<%@ Page Title=" All Settings" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="AllSettings.aspx.cs" Inherits="Web.NewHR.AllSettings" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .itemStatus
        {
            background-image: none !important;
            background-color: #428BCA;
            color: #FFF;
            padding: 7px 10px 7px 10px;
            border-bottom: 1px solid #fff !important;
            font-size: 14px;
        }
        .x-toolbar-default a
        {
            color: White;
            font-family: Arial, 'DejaVu Sans' , 'Liberation Sans' ,Freesans,sans-serif !important;
            font-size: 13px;
        }
        .itemStatus .x-panel-header-text-default
        {
            color: White !important;
        }
        .dbPanelBodyStyle
        {
            background-color: White !important;
        }
        .tdStyle
        {
            width: 480px;
            padding-right: 30px;
        }
        .description
        {
            font-family: Open Sans;
            font-size: 12px;
            margin-top: 6px;F
            margin-bottom: 6px;
            margin-left: 4px;
            font-style: italic;
        }
        .innerTable
        {
            width: 100%;
            margin-bottom: 20px;
        }
        .innerTable .maintd
        {
            width: 50%;
            padding: 4px;
            margin-bottom: 4px;
            border-bottom: solid 1px #EAEAEA;
            cursor: pointer;
        }
        .innerTable a
        {
            display: block;
            height: 15px; /*background-image: url(../Styles/images/report.png);*/
            background-repeat: no-repeat;
            padding-left: 25px;
            padding-top: 5px;
        }
    </style>
    <script type="text/javascript">
        function MyFavouriteItemStat() {
            alert('You can not choose more than seven item as favourite');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    All Settings
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <asp:Panel ID="pnlContainer" runat="server" class="contentArea1">
            <div style='clear: both'>
            </div>
            <table class="contentTable">
                <tr>
                    <td class="tdStyle" valign="top">
                        <div class="itemStatus">
                            Grade and Position
                        </div>
                        <table class="innerTable" border="0">
                            <tr>
                                <td id="Td24" class="maintd" runat="server" visible='<%# IsAccessible("newhr/GroupManager.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton15" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink39" ToolTip="Levels and Positions" runat="server" Text="Groups"
                                        NavigateUrl='~/newhr/GroupManager.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td16" class="maintd" runat="server" visible='<%# IsAccessible("newhr/PositionGradeManager.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton5" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink6" ToolTip="Levels/Positions and Designations" runat="server"
                                        Text="Levels/Positions and Designations" NavigateUrl='~/newhr/PositionGradeManager.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td class="maintd" runat="server" visible='<%# IsAccessible("newhr/SalaryScaleManager.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton6" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink31" ToolTip="Change salary scale." runat="server" Text="Salary Scale"
                                        NavigateUrl='~/newhr/SalaryScaleManager.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td class="maintd" runat="server" visible='<%# IsAccessible("newhr/DefinedSalaryImport.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton7" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink32" ToolTip="Change the leave encashment months." runat="server"
                                        Text="Other Defined Salary" NavigateUrl='~/newhr/DefinedSalaryImport.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div class="itemStatus">
                            Deputation
                        </div>
                        <table class="innerTable" border="0">
                            <tr>
                                <td id="Td17" class="maintd" runat="server" visible='<%# IsAccessible("newhr/DeputationSettings.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton8" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink33" ToolTip="Levels and Positions" runat="server" Text="Depution Types"
                                        NavigateUrl='~/newhr/DeputationSettings.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div class="itemStatus">
                            Leave Settings
                        </div>
                        <table class="innerTable" border="0">
                            <tr>
                                <td class="maintd" runat="server" visible='<%# IsAccessible("CP/LeaveSetting.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A7" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink11" ToolTip="Change the leave encashment months." runat="server"
                                        Text="Leave / Attendance settings" NavigateUrl='~/CP/LeaveSetting.aspx' />
                                </td>
                            </tr>
                            <tr runat="server" id="trLeaveFareSettings">
                                <td id="Td42" class="maintd" runat="server" visible='<%# IsAccessible("CP/LeaveFareSetting.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton37" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink30" ToolTip="Change the leave fare settings." runat="server"
                                        Text="Leave Fare settings" NavigateUrl='~/CP/LeaveFareSetting.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td class="maintd" runat="server" visible='<%# IsAccessible("CP/Extension/ManageLeaveProjects.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A8" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink12" ToolTip="Select teams and its manager for leave approval."
                                        runat="server" Text="Manage Leave Team" NavigateUrl='~/CP/Extension/ManageLeaveProjects.aspx' />
                                </td>
                            </tr>
                            <tr runat="server" id="rowLeaveSettingByTeam">
                                <td class="maintd" runat="server" visible='<%# IsAccessible("CP/LeaveApprovalNotification.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A10" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink14" runat="server" ToolTip="Set up the leave request and leave approval."
                                        Text="Leave Request Setting" NavigateUrl='~/CP/LeaveApprovalNotification.aspx' />
                                </td>
                            </tr>
                            <tr runat="server" id="rowLeaveSettingByDefinedType">
                                <td id="Td34" class="maintd" runat="server" visible='<%# IsAccessible("CP/LeaveApprovalNotification.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton29" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink21" runat="server" ToolTip="Set up the leave request and leave approval."
                                        Text="Pre-Defined Authority List" NavigateUrl='~/newhr/LeaveRequestPredefinedList.aspx' />
                                </td>
                            </tr>
                            <tr runat="server" id="rowLeaveSpecificIndividualLeaveAuthority">
                                <td id="Td29" class="maintd" runat="server" visible='<%# IsAccessible("NewHr/LeaveSpecificFlow.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton36" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink44" runat="server" ToolTip="Individual Leave Authority."
                                        Text="Individual Leave Authority" NavigateUrl='~/NewHr/LeaveSpecificFlow.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div class="itemStatus">
                            Company Settings
                        </div>
                        <%-- <div class="description">
                        &nbsp;
                    </div>--%>
                        <table class="innerTable" border="0">
                            <tr>
                                <td id="Td6" class="maintd" runat="server" visible='<%# IsAccessible("CP/ManageCompany.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A11" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink20" ToolTip="" runat="server" Text="Manage Company" NavigateUrl='~/CP/ManageCompany.aspx' />
                                </td>
                                <td id="Td20" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/Extension/MonetoryChangeLog.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton11" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink23" runat="server" Text="Clear Cache" NavigateUrl='~/CP/ClearCache.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td7" class="maintd" runat="server" visible='<%# IsAccessible("CP/ManageBranch.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A12" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink24" ToolTip="" runat="server" Text="Branches" NavigateUrl='~/CP/ManageBranch.aspx' />
                                </td>
                                <td id="Td18" class="maintd" runat="server" visible='<%# IsAccessible("CP/ManageBranchDepartment.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton9" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink34" ToolTip="" runat="server" Text="Branch Departments"
                                        NavigateUrl='~/CP/ManageBranchDepartment.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td8" class="maintd" runat="server" visible='<%# IsAccessible("CP/ManageDep.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A13" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink25" ToolTip="" runat="server" Text="Departments" NavigateUrl='~/CP/ManageDep.aspx' />
                                </td>
                                <td id="Td10" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/ManageBranchDepartmentHeads.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton1" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink27" ToolTip="" runat="server" Text="Set Department Head"
                                        NavigateUrl='~/CP/ManageBranchDepartmentHeads.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td9" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/Extension/MonetoryChangeLog.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A14" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink26" runat="server" Text="Change Logs" NavigateUrl='~/CP/Extension/MonetoryChangeLog.aspx' />
                                </td>
                                <td id="Td41" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/ManageBanksBranches.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton39" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink56" ToolTip="" runat="server" Text="Bank Branches" NavigateUrl='~/CP/ManageBanksBranches.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td26" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/ManageBanks.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton17" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink41" ToolTip="" runat="server" Text="Banks" NavigateUrl='~/CP/ManageBanks.aspx' />
                                </td>
                                <td id="Td11" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/ManageVoucherDepartment.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton2" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink28" runat="server" Text="Voucher Department" NavigateUrl='~/CP/ManageVoucherDepartment.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td12" class="maintd" runat="server" visible='<%# IsAccessible("CP/ManageVoucherGroup.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton3" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink29" runat="server" Text="Voucher Settings" NavigateUrl='~/CP/ManageVoucherGroup.aspx' />
                                </td>
                                <td id="Td48" class="maintd" runat="server" visible='<%# IsAccessible("NewHr/LogList.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton47" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink62" runat="server" Text="Log List" NavigateUrl='~/NewHr/LogList.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td25" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/ManageCostCode.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton28" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink9" runat="server" Text="Manage Cost Code" NavigateUrl='~/CP/ManageCostCode.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div class="itemStatus">
                            Shift
                        </div>
                        <%-- <div class="description">
                        &nbsp;
                    </div>--%>
                        <table class="innerTable" border="0">
                            <tr>
                                <td id="Td28" class="maintd" runat="server" visible='<%# IsAccessible("newhr/shift/ShiftList.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton19" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink43" ToolTip="" runat="server" Text="Manage Shift" NavigateUrl='~/newhr/shift/ShiftList.aspx' />
                                </td>
                                <td id="Td30" class="maintd" runat="server" visible='<%# IsAccessible("newhr/shift/ShiftEmployeeRelation.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton24" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink48" ToolTip="" runat="server" Text="Employee Shift" NavigateUrl='~/newhr/shift/ShiftEmployeeRelation.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td31" class="maintd" runat="server" visible='<%# IsAccessible("NewHR/Shift/AssignShiftHr.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton25" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink49" ToolTip="" runat="server" Text="Shift Planning" NavigateUrl='~/NewHR/Shift/AssignShiftHr.aspx' />
                                </td>
                                <td id="Td32" class="maintd" runat="server" visible='<%# IsAccessible("newhr/shift/ChangeShiftType.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton26" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink50" ToolTip="" runat="server" Text="Shift Schedule" NavigateUrl='~/newhr/shift/ChangeShiftType.aspx' />
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                    <td class="tdStyle" valign="top">
                        <div class="itemStatus">
                            Initial Settings
                        </div>
                        <table class="innerTable" border="0">
                            <tr id="Tr1" runat="server">
                                <td id="Td1" class="maintd" runat="server" visible='<%# IsAccessible("CP/ManageGratuityRule.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A1" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink1" runat="server" ToolTip="Setup Gratuity/Severance rules."
                                        Text="Gratuity/Severance Rule" NavigateUrl='~/CP/ManageGratuityRule.aspx' />
                                </td>
                                <%-- </tr>
                        <tr id="Tr2" runat="server">--%>
                                <td id="Td2" class="maintd" runat="server" visible='<%# IsAccessible("CP/LeaveOpeningBal.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A2" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink2" runat="server" ToolTip="Enter opening leave day/hours."
                                        Text="Opening Leaves" NavigateUrl='~/CP/LeaveOpeningBal.aspx' />
                                </td>
                            </tr>
                            <tr id="Tr3" runat="server">
                                <td id="Td3" class="maintd" runat="server" visible='<%# IsAccessible("CP/OpeningWorkdaysEntry.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A3" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink3" runat="server" ToolTip="Enter opening workdays." Text="Opening workdays"
                                        NavigateUrl='~/CP/OpeningWorkdaysEntry.aspx' />
                                </td>
                                <td id="Td43" class="maintd" runat="server" visible='<%# IsAccessible("CP/CustomGratuityStartDate.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton38" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink57" runat="server" Text="Gratuity Custom Date" NavigateUrl='~/CP/CustomGratuityStartDate.aspx' />
                                </td>
                            </tr>
                            <tr id="Tr5" runat="server">
                                <td id="Td5" class="maintd" runat="server" visible='<%# IsAccessible("CP/ChangeStatus.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A5" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink18" runat="server" ToolTip="Change the employee status names."
                                        Text="Change Employee Status" NavigateUrl='~/CP/ChangeStatus.aspx' />
                                </td>
                                <td id="Td4" class="maintd" runat="server" visible='<%# IsAccessible("CP/HistoryForTax.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A4" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink5" runat="server" ToolTip="Enter YTD taxable incomes and taxes paid."
                                        Text="Tax History" NavigateUrl='~/CP/HistoryForTax.aspx' />
                                </td>
                            </tr>
                            <tr id="Tr6" runat="server">
                                <td id="Td19" class="maintd" runat="server" visible='<%# IsAccessible("CP/Extension/EmailContentDisplay.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton10" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink35" ToolTip="Content of the Email sent to the employees"
                                        runat="server" Text="Change Email Content" NavigateUrl='~/CP/Extension/EmailContentDisplay.aspx' />
                                </td>
                                <td id="Td15" class="maintd" runat="server" visible='<%# IsAccessible("CP/Extension/ChangeEmailSetting.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A6" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink19" runat="server" ToolTip="Email configuration change."
                                        Text="Test Email Setting" NavigateUrl='~/CP/Extension/ChangeEmailSetting.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div class="itemStatus">
                            Regular Settings
                        </div>
                        <%-- <div class="description">
                        &nbsp;
                    </div>--%>
                        <table class="innerTable" border="0">
                            <tr>
                                <td class="maintd" runat="server" visible='<%# IsAccessible("~/CP/FinancialDateDetails.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A18" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink7" ToolTip="Create new Payroll Periods (Automatically created)."
                                        runat="server" Text="Payroll Periods" NavigateUrl='~/CP/PayrollPeriodPG.aspx' />
                                </td>
                                <td class="maintd" runat="server" visible='<%# IsAccessible("~/CP/FinancialDateDetails.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A19" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink8" ToolTip="Create new Fiancial Year." runat="server"
                                        Text="Financial Year" NavigateUrl='~/CP/FinancialDateDetails.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td44" class="maintd" runat="server" visible='<%# IsAccessible("~/cp/backupandrestore.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton41" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink58" runat="server" Text="Backup" NavigateUrl='~/cp/backupandrestore.aspx' />
                                </td>
                                <td id="Td46" class="maintd" runat="server" visible='<%# IsAccessible("~/cp/TaxSlabList.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton42" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink59" runat="server" Text="Tax Slabs" NavigateUrl='~/cp/TaxSlabList.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div class="itemStatus">
                            Project Settings
                        </div>
                        <table class="innerTable" border="0">
                            <tr>
                                <td class="maintd" runat="server" visible='<%# IsAccessible("~/CP/Extension/ManageProjects.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A22" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink15" ToolTip="Create and edit projects for project contribution."
                                        runat="server" Text="Manage Projects" NavigateUrl='~/CP/Extension/ManageProjects.aspx' />
                                </td>
                                <td id="Td27" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/Extension/ProjectContribution.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A23" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink16" ToolTip="Setup project salary assignment setting for project report."
                                        runat="server" Text="Project Income Contribution " NavigateUrl='~/CP/Extension/ProjectContribution.aspx' />
                                </td>
                            </tr>
                            <tr>
                            </tr>
                            <tr>
                                <td class="maintd" runat="server" visible='<%# IsAccessible("~/CP/Extension/ProjectPayInput.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="A24" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink17" ToolTip="Enter or import project hours for project report."
                                        runat="server" Text="Manual Project Hours/Percent Input" NavigateUrl='~/CP/Extension/ProjectPayInput.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div class="itemStatus">
                            Other Settings
                        </div>
                        <table class="innerTable" border="0">
                            <tr>
                                <td id="Td45" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/ManageCITIncomes") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton40" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink60" ToolTip="Select Income Heads for % of CIT contribution."
                                        runat="server" Text="CIT Incomes" NavigateUrl='~/CP/ManageCITIncomes.aspx' />
                                </td>
                                <td id="Td49" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/Extension/ManageCheckList.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton44" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink64" ToolTip="Content of the Email sent to the employees"
                                        runat="server" Text="Payroll Checklist" NavigateUrl='~/CP/Extension/ManageCheckList.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td50" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/ManageDepositList.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton45" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink65" ToolTip="Content of the Email sent to the employees"
                                        runat="server" Text="Deposit Document List" NavigateUrl='~/CP/ManageDepositList.aspx' />
                                </td>
                                <td id="Td51" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/ManageSettings/ManageSettingList.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton46" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink66" ToolTip="Manage Setting List" runat="server" Text="Manage Setting List"
                                        NavigateUrl='~/CP/ManageSettings/ManageSettingList.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="AwardList" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/AwardList.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton12" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink36" ToolTip="" runat="server" Text="Award List" NavigateUrl='~/cp/AwardList.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div class="itemStatus">
                            Users and Roles
                        </div>
                        <%-- <div class="description">
                        &nbsp;
                    </div>--%>
                        <table class="innerTable" border="0">
                            <tr>
                                <td id="Td22" class="maintd" runat="server" visible='<%# IsAccessible("User/ManageUsers.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton13" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink10" ToolTip="" runat="server" Text="Manage Users" NavigateUrl='~/User/ManageUsers.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td23" class="maintd" runat="server" visible='<%# IsAccessible("~/User/ManageRoles.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton14" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink38" runat="server" Text="Manage Roles" NavigateUrl='~/User/ManageRoles.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td33" class="maintd" runat="server" visible='<%# IsAccessible("~/User/MGPermission.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton27" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink51" runat="server" Text="Manage Permission" NavigateUrl='~/User/MGPermission.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td21" class="maintd" runat="server" visible='<%# IsAccessible("~/CP/PasswordChangeRule.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton20" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink37" runat="server" Text="Password Change Rules" NavigateUrl='~/CP/PasswordChangeRule.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div class="itemStatus">
                            Income / Pay Settings
                        </div>
                        <%--   <div class="description">
                        &nbsp;
                    </div>--%>
                        <table class="innerTable" border="0">
                            <tr>
                                <td class="maintd" runat="server" visible='<%# IsAccessible("~/cp/AAOvertimeSettings") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton18" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink42" ToolTip="Change the leave encashment months." runat="server"
                                        Text="Income / Pay setting" NavigateUrl='~/CP/AAOvertimeSettings.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td13" class="maintd" runat="server" visible='<%# IsAccessible("~/cp/ManageAutoOTGroup") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton4" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink13" ToolTip="Change the leave encashment months." runat="server"
                                        Text="Auto Overtime setting" NavigateUrl='~/CP/ManageAutoOTGroup.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td14" class="maintd" runat="server" visible='<%# IsAccessible("~/cp/ContractSetting") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton16" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink4" ToolTip="Change the leave encashment months." runat="server"
                                        Text="UnitRate Day Setting" NavigateUrl='~/CP/ContractSetting.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td47" class="maintd" runat="server" visible='<%# IsAccessible("~/NewHr/YearlyIncomeSettings") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton43" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink61" ToolTip="Yearly Income Setting" runat="server" Text="Yearly Income Setting"
                                        NavigateUrl='~/NewHr/YearlyIncomeSettings.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div class="itemStatus">
                            PF/CIT
                        </div>
                        <%--   <div class="description">
                        &nbsp;
                    </div>--%>
                        <table class="innerTable" border="0">
                            <tr>
                                <td id="Td35" class="maintd" runat="server" visible='<%# IsAccessible("~/cp/OpeningImportPage.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton30" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink22" ToolTip="Change the leave encashment months." runat="server"
                                        Text="PF/CIT Opening Import" NavigateUrl='~/CP/OpeningImportPage.aspx' />
                                </td>
                                <td id="Td37" class="maintd" runat="server" visible='<%# IsAccessible("~/cp/YearlyInterestImportPage.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton32" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink52" ToolTip="Change the leave encashment months." runat="server"
                                        Text="PF/CIT Yearly Interest Import" NavigateUrl='~/CP/YearlyInterestImportPage.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td36" class="maintd" runat="server" visible='<%# IsAccessible("~/cp/PFBalanceReport.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton31" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink40" ToolTip="Change the leave encashment months." runat="server"
                                        Text="PF Balance Report" NavigateUrl='~/CP/PFBalanceReport.aspx' />
                                </td>
                                <td id="Td38" class="maintd" runat="server" visible='<%# IsAccessible("~/cp/CITBalanceReport.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton33" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink53" ToolTip="Change the leave encashment months." runat="server"
                                        Text="CIT Balance Report" NavigateUrl='~/CP/CITBalanceReport.aspx' />
                                </td>
                            </tr>
                            <tr>
                                <td id="Td39" class="maintd" runat="server" visible='<%# IsAccessible("~/cp/CITBalanceReport.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton34" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink54" ToolTip="Change the leave encashment months." runat="server"
                                        Text="PF Yearwise Balance Report" NavigateUrl='~/CP/PFBalanceYearwiseReport.aspx' />
                                </td>
                                <td id="Td40" class="maintd" runat="server" visible='<%# IsAccessible("~/cp/CITBalanceYearwiseReport.aspx") %>'>
                                    <span style="float: left;">
                                        <ext:ImageButton ID="ImageButton35" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                            OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                            PressedImageUrl="~/images/favEnable.gif">
                                            <DirectEvents>
                                                <Click OnEvent="FavouriteItemBtn_Click">
                                                </Click>
                                            </DirectEvents>
                                        </ext:ImageButton>
                                    </span>
                                    <asp:HyperLink ID="HyperLink55" ToolTip="Change the leave encashment months." runat="server"
                                        Text="CIT Yearwise Balance Report" NavigateUrl='~/CP/CITBalanceYearwiseReport.aspx' />
                                </td>
                            </tr>
                        </table>
                        <div runat="server" id="mgAllowance">
                            <div class="itemStatus">
                                Allowance Settings
                            </div>
                            <%--   <div class="description">
                        &nbsp;
                    </div>--%>
                            <table class="innerTable" border="0">
                                <tr>
                                    <td class="maintd" runat="server" visible='<%# IsAccessible("NewHR/MGAllowance/IncomeSetting.aspx") %>'>
                                        <span style="float: left;">
                                            <ext:ImageButton ID="ImageButton21" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                                OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                                PressedImageUrl="~/images/favEnable.gif">
                                                <DirectEvents>
                                                    <Click OnEvent="FavouriteItemBtn_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:ImageButton>
                                        </span>
                                        <asp:HyperLink ID="HyperLink45" ToolTip="Change the leave encashment months." runat="server"
                                            Text="Allowance Income List" NavigateUrl='~/NewHR/MGAllowance/IncomeSetting.aspx' />
                                    </td>
                                    <td class="maintd" runat="server" visible='<%# IsAccessible("NewHR/MGAllowance/AllowanceOpening.aspx") %>'>
                                        <span style="float: left;">
                                            <ext:ImageButton ID="ImageButton22" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                                OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                                PressedImageUrl="~/images/favEnable.gif">
                                                <DirectEvents>
                                                    <Click OnEvent="FavouriteItemBtn_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:ImageButton>
                                        </span>
                                        <asp:HyperLink ID="HyperLink46" ToolTip="Change the leave encashment months." runat="server"
                                            Text="Allowance Opening" NavigateUrl='~/NewHR/MGAllowance/AllowanceOpening.aspx' />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="maintd" runat="server" visible='<%# IsAccessible("cp/extension/MGAllowanceSalaryImport.aspx") %>'>
                                        <span style="float: left;">
                                            <ext:ImageButton ID="ImageButton23" runat="server" EnableToggle="true" ImageUrl="~/images/favDisable.gif"
                                                OverImageUrl="~/images/favEnable.gif" DisabledImageUrl="~/images/favDisable.gif"
                                                PressedImageUrl="~/images/favEnable.gif">
                                                <DirectEvents>
                                                    <Click OnEvent="FavouriteItemBtn_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:ImageButton>
                                        </span>
                                        <asp:HyperLink ID="HyperLink47" ToolTip="Change the leave encashment months." runat="server"
                                            Text="Allowance List/Import" NavigateUrl='~/cp/extension/MGAllowanceSalaryImport.aspx' />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
