﻿<%@ Page Title="Group Setting" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="GroupManager.aspx.cs" Inherits="Web.NewHR.GroupManager" %>

<%@ Register Src="~/Controls/BreadCrumbCtl.ascx" TagName="BreadCrumbCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
      var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.LevelGroupId);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }

    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 0px !important;
        }
        #menu
        {
            display: none;
        }
        .centerButtons
        {
            padding-left: 15px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="hdnDropMode" />
    <ext:Hidden runat="server" ID="hdnSource" />
    <ext:Hidden runat="server" ID="hdnDest" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteLevel_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Group?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Groups
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="width: 560px;">
        <div>
            <div>
                <!-- panel-btns -->
                <h4 class="panel-title sectionHeading" style="margin-left: 20px; margin-bottom: -20px;">
                    <i></i>Employee Groups in the Organization
                </h4>
            </div>
            <div class="panel-body" style="width: 540px">
                <ext:GridPanel ID="GridLevels" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store3" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="LevelGroupId">
                                    <Fields>
                                        <ext:ModelField Name="LevelGroupId" Type="String" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Order" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column8" Width="300" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Name" Align="Left" DataIndex="Name" />
                            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Order"
                                Align="Center" DataIndex="Order">
                            </ext:Column>
                            <ext:CommandColumn TdCls="centerButtons" Width="100" ID="CommandColumn2" Align="Center"
                                runat="server" Text="Actions">
                                <Commands>
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                        CommandName="Edit" />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div class="buttonBlockSection">
                    <ext:Button runat="server" ID="btnAddLevel" Cls="btn btn-primary btn-sm btn-sect"
                        Text="<i></i>Add New Group">
                        <DirectEvents>
                            <Click OnEvent="btnAddLevel_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </div>
            </div>
        </div>
        <ext:Window ID="WindowLevel" runat="server" Title="Employee Group" Icon="Application"
            Width="350" Height="250" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtLevelName" LabelSeparator="" MinValue="0" runat="server" FieldLabel="Name"
                                LabelAlign="Top">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtLevelName" ErrorMessage="Please type a Group name." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:NumberField ID="txtLevelOrder" LabelSeparator="" MinValue="1" runat="server"
                                FieldLabel="Order" LabelAlign="Top">
                            </ext:NumberField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtLevelOrder" ErrorMessage="Please define the group order." />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button runat="server" ID="btnLevelSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save"
                                    runat="server">
                                    <DirectEvents>
                                        <Click OnEvent="btnLevelSaveUpdate_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel"
                                    runat="server">
                                    <Listeners>
                                        <Click Handler="#{WindowLevel}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
    <br />
</asp:Content>
