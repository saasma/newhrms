﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;

namespace Web.NewHR
{
    public partial class EmpRetiringDetailsList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments();
            cmbDepartment.Store[0].DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int branchId = -1, departmentId = -1, employeeId = -1, pageSize = 20;
            string name = "";

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            name = cmbEmpSearch.Text;

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    name = "";
            }

            if (employeeId == 0)
                employeeId = -1;

            if(cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);   

            List<GetEmployeeRetirementDetailsListResult> list = EmployeeManager.GetEmpRetirementDetails(e.Start/pageSize, pageSize, branchId, departmentId, name, employeeId);
            gridRetDetails.GetStore().DataSource = list;
            gridRetDetails.GetStore().DataBind();

            if (list.Count > 0)
                e.Total = list[0].TotalRows.Value;
        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            int branchId = -1, departmentId = -1, employeeId = -1, pageSize = 20;
            string name = "";

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            name = cmbEmpSearch.Text;

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    name = "";
            }

            if (employeeId == 0)
                employeeId = -1;

            if (cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            List<GetEmployeeRetirementDetailsListResult> list = EmployeeManager.GetEmpRetirementDetails(0, 999999, branchId, departmentId, name, employeeId);

            Bll.ExcelHelper.ExportToExcel("Employee Retirement Details", list,
                new List<String>() { "RowNumber", "TotalRows", "StatusID" },
            new List<String>() { },
            new Dictionary<string, string>() { {"EmployeeId","EIN"}, {"RetirementBy","Retirement By"}, 
                    {"RetirementDate","Retirement Date"}, {"JoinDate","Start Date/Join Date"}, { "StatusName", "Status" }, { "RetirementByAge", "Retirement By Age" }, { "RetirementByService", "Retirement By Service" } },
            new List<string>() { }
            , new List<string> { }
            , new List<string> { }
            , new Dictionary<string, string>() { }
            , new List<string> { "EmployeeId", "Name", "Branch", "Department", "Level", "StatusName", "DOB", "JoinDate","RetirementByAge","RetirementByService", "RetirementBy", "RetirementDate" });


        }

    }
}