﻿<%@ Page Title="Employee Photo Approval" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmpIdentificationApproval.aspx.cs" Inherits="Web.NewHR.EmpIdentificationApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="../js/jquery.Jcrop.js"></script>
    <script type="text/javascript" src="../js/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.10.3.min.js"></script>
    <link id="Link1" rel="stylesheet" type="text/css" href="../css/jquery.Jcrop.css" />
    <link id="Link2" rel="stylesheet" type="text/css" href="../css/jquery.Jcrop.min.css" />

    <script type="text/javascript">
        /* A header Checkbox of CheckboxSelectionModel deals with the current page only.
        This override demonstrates how to take into account all the pages.
        It works with local paging only. It is not going to work with remote paging.
        */
        Ext.selection.CheckboxModel.override({
            selectAll: function (suppressEvent) {
                var me = this,
                    selections = me.store.getAllRange(), // instead of the getRange call
                    i = 0,
                    len = selections.length,
                    start = me.getSelection().length;

                me.suspendChanges();

                for (; i < len; i++) {
                    me.doSelect(selections[i], true, suppressEvent);
                }

                me.resumeChanges();
                if (!suppressEvent) {
                    me.maybeFireSelectionChange(me.getSelection().length !== start);
                }
            },

            deselectAll: Ext.Function.createSequence(Ext.selection.CheckboxModel.prototype.deselectAll, function () {
                this.view.panel.getSelectionMemory().clearMemory();
            }),

            updateHeaderState: function () {
                var me = this,
                    store = me.store,
                    storeCount = store.getTotalCount(),
                    views = me.views,
                    hdSelectStatus = false,
                    selectedCount = 0,
                    selected, len, i;

                if (!store.buffered && storeCount > 0) {
                    selected = me.view.panel.getSelectionMemory().selectedIds;
                    hdSelectStatus = true;
                    for (s in selected) {
                        ++selectedCount;
                    }

                    hdSelectStatus = storeCount === selectedCount;
                }

                if (views && views.length) {
                    me.toggleUiHeader(hdSelectStatus);
                }
            }
        });

        Ext.grid.plugin.SelectionMemory.override({
            memoryRestoreState: function (records) {
                if (this.store !== null && !this.store.buffered && !this.grid.view.bufferedRenderer) {
                    var i = 0,
                        ind,
                        sel = [],
                        len,
                        all = true,
                        cm = this.headerCt;

                    if (!records) {
                        records = this.store.getAllRange(); // instead of getRange
                    }

                    if (!Ext.isArray(records)) {
                        records = [records];
                    }

                    if (this.selModel.isLocked()) {
                        this.wasLocked = true;
                        this.selModel.setLocked(false);
                    }

                    if (this.selModel instanceof Ext.selection.RowModel) {
                        for (ind = 0, len = records.length; ind < len; ind++) {
                            var rec = records[ind],
                                id = rec.getId();

                            if ((id || id === 0) && !Ext.isEmpty(this.selectedIds[id])) {
                                sel.push(rec);
                            } else {
                                all = false;
                            }

                            ++i;
                        }

                        if (sel.length > 0) {
                            this.surpressDeselection = true;
                            this.selModel.select(sel, false, !this.grid.selectionMemoryEvents);
                            this.surpressDeselection = false;
                        }
                    } else {
                        for (ind = 0, len = records.length; ind < len; ind++) {
                            var rec = records[ind],
                                id = rec.getId();

                            if ((id || id === 0) && !Ext.isEmpty(this.selectedIds[id])) {
                                if (this.selectedIds[id].dataIndex) {
                                    var colIndex = cm.getHeaderIndex(cm.down('gridcolumn[dataIndex=' + this.selectedIds[id].dataIndex + ']'))
                                    this.selModel.setCurrentPosition({
                                        row: i,
                                        column: colIndex
                                    });
                                }
                                return false;
                            }

                            ++i;
                        }
                    }

                    if (this.selModel instanceof Ext.selection.CheckboxModel) {
                        if (all) {
                            this.selModel.toggleUiHeader(true);
                        } else {
                            this.selModel.toggleUiHeader(false);
                        }
                    }

                    if (this.wasLocked) {
                        this.selModel.setLocked(true);
                    }
                }
            }
        });

        var CommandHandler = function (command, record) {

            
            if(command == 'View')
            { 
                 <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);                
                 //<%= btnView.ClientID %>.fireEvent('click');
                 document.getElementById("<%= btnView1.ClientID %>").click();
            }
           
        };

          function myFunction() {
            //some code here
            $("#popupdiv").dialog({
                modal: true,
                draggable: false,
                resizable: false,
                width: 'auto',
                height: 'auto',
                position: ['center', 'center'],
                show: 'blind',
                hide: 'blind',
                dialogClass: 'ui-dialog-osx',
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

        function AfterSaveCall()
        {
            document.getElementById("<%= btnSaveCall.ClientID %>").click();
        }

        

    </script>


    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        
        .ui-dialog-titlebar-close
    {
        visibility: hidden;
    }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

    <div style="display:none;">
        <asp:Button ID="btnView1" OnClick="btnView1_Click" Text="text" runat="server" />
    </div>

     <div style="display:none;">
        <asp:Button ID="btnSaveCall" OnClick="btnSaveCall_Click" Text="text" runat="server" />
    </div>

<ext:Hidden ID="hdnEmployeeId" runat="server" />
 <ext:LinkButton ID="btnView" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnView_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>

    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Photograph Approval
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Container ID="lblMsg" runat="server" />
        <ext:GridPanel ID="gridIdenApproval" runat="server" Cls="itemgrid" Width="490" Scroll="None">
            <Store>
                <ext:Store ID="Store1" runat="server">
                    <Model>
                        <ext:Model ID="Model2" runat="server" IDProperty="EmployeeId">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" />
                                <ext:ModelField Name="Name" />
                                <ext:ModelField Name="imageUrl" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel2" runat="server">
                <Columns>
                    <ext:Column ID="Column7" runat="server" Text="Employee Id" Width="100" DataIndex="EmployeeId"
                        Align="Center">
                    </ext:Column>
                    <ext:Column ID="Column8" runat="server" Text="Employee Name" Width="200" DataIndex="Name">
                    </ext:Column>
                    <ext:TemplateColumn ID="TemplateColumn2" runat="server" Text="Photo" DataIndex="url"
                        TemplateString='<img style="width:60px;height:45px;" src="{imageUrl}" />' />

                    <ext:CommandColumn ID="CommandColumn2" runat="server" Width="70" Sortable="false"
                                MenuDisabled="true" Text="Actions" Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Text="<i></i>View" CommandName="View" ToolTip-Text="View Photo" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi" />
            </SelectionModel>
        </ext:GridPanel>
        <div class="buttonBlock" style="width: 68%;">

        <table>
            <tr>
                <td style="width:300px;">
                    <ext:Button ID="btnApprove" runat="server" Cls="btn btn-success" Text="Approve" Width="120">
                        <DirectEvents>
                            <Click OnEvent="btnApprove_Click">
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the Photo?" />
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridIdenApproval}.getRowsValues({selectedOnly:true}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>

                <td>
                    <ext:Button ID="btnDelete" runat="server" Cls="btn btn-save" Text="Delete" Width="120">
                        <DirectEvents>
                            <Click OnEvent="btnDelete_Click">
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Photo?" />
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridIdenApproval}.getRowsValues({selectedOnly:true}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
        </table>

            
        </div>
        <br />
    </div>


<div id="popupdiv" title="" style="display: none; background-color: Silver;">
    <asp:Image ID="Image1" runat="server" />
    <br />
    <ext:Button runat="server" Text="Crop" ID="btnCrop" Cls="bluebutton left">
        <DirectEvents>
            <Click OnEvent="btnCropImage_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
        <Listeners>
        </Listeners>
    </ext:Button>
</div>


<input type="hidden" runat="server" id="_xField" />
<input type="hidden" runat="server" id="_yField" />
<input type="hidden" runat="server" id="_widthField" />
<input type="hidden" runat="server" id="_heightField" />


<ext:Window ID="WPhotoView" runat="server" Title="" Icon="Application"
            Width="410" Height="390" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
             <div style="border: 1px solid grey; width: 380px; height: 340px;">
                <ext:Image ID="ImgEmployee" runat="Server" Width="380" Height="340">
                </ext:Image>
            </div>
        </Content>
    </ext:Window>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">

<script type="text/javascript">
    $(function () {
        $('#btnclick').click(function () {
            $("#popupdiv").dialog({
                modal: true,
                draggable: false,
                resizable: false,
                width: 'auto',
                height: 'auto',
                position: ['center', 'center'],
                show: 'blind',
                hide: 'blind',
                dialogClass: 'ui-dialog-osx',
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });

        });


    })

      

</script>
<script type="text/javascript">

    function hideDiv() {
        document.getElementById('popupdiv').style.display = "none";
    }

    var editorID = '<%= Image1.ClientID %>';

    jQuery(function () {
        jQuery('#' + editorID).Jcrop({
            onChange: showCoords,
            onSelect: showCoords,
            aspectRatio: 1
        });
    });

    function showCoords(c) {

        var xField = document.getElementById('<%= _xField.ClientID %>');
        var yField = document.getElementById('<%= _yField.ClientID %>');
        var widthField = document.getElementById('<%= _widthField.ClientID %>');
        var heightField = document.getElementById('<%= _heightField.ClientID %>');

        xField.value = c.x;
        yField.value = c.y;
        widthField.value = c.w;
        heightField.value = c.h;
    }

    
    
</script>

</asp:Content>


