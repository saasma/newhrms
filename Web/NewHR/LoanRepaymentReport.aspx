﻿<%@ Page Title="Loan Repayment Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="LoanRepaymentReport.aspx.cs" Inherits="Web.CP.LoanRepaymentReport" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 
   
    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Loan Repayment Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                        QueryMode="Local" ID="cmbYear" ForceSelection="true" DisplayField="Year" ValueField="Year"
                                        runat="server" FieldLabel="Year">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model2" IDProperty="Year" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Year" Type="String" />
                                                            <ext:ModelField Name="Year" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox Width="100" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                        QueryMode="Local" ID="cmbMonth" ForceSelection="true" DisplayField="Value" ValueField="Key"
                                        runat="server" FieldLabel="Month">
                                        <Store>
                                            <ext:Store ID="Store2" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model3" IDProperty="Key" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Key" Type="String" />
                                                            <ext:ModelField Name="Value" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                        QueryMode="Local" ID="cmbLoanDeduction" Width="200px" ForceSelection="true" DisplayField="Title" ValueField="DeductionId"
                                        runat="server" FieldLabel="Deduction">
                                        <Store>
                                            <ext:Store ID="Store3" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model4" IDProperty="DeductionId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DeductionId" Type="String" />
                                                            <ext:ModelField Name="Title" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                          <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox Width="150" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                        QueryMode="Local" ID="cmbType" ForceSelection="true" runat="server" FieldLabel="Type">
                                        <Items>
                                            <ext:ListItem Text="All" Value="2">
                                            </ext:ListItem>
                                            <ext:ListItem Text="New This Month" Value="1">
                                            </ext:ListItem>
                                        </Items>
                                        <SelectedItems>
                                            <ext:ListItem Value="2" />
                                        </SelectedItems>
                                    </ext:ComboBox>
                                </td>
                                <td style="width: 310px;">
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="250" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                OnReadData="Store_ReadData" AutoScroll="true">
                <Store>
                    <ext:Store PageSize="25" ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="SN" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="AccountNo" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Deduction" Type="String" />
                                    <ext:ModelField Name="TakenOnText" Type="String" />
                                    <ext:ModelField Name="TakenOnEngText" Type="String" />
                                    <ext:ModelField Name="PrincipleAmount" Type="String" />
                                    <ext:ModelField Name="NoOfInstallment" Type="String" />
                                    <ext:ModelField Name="RemainingInstallment" Type="String" />
                                    <ext:ModelField Name="PPMT" Type="String" />
                                    <ext:ModelField Name="EndDateText" Type="String" />
                                    <ext:ModelField Name="EndDateEngText" Type="String" />
                                    <ext:ModelField Name="RemInstallmentIncludingCurrentMonth" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="EmployeeId" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colFromDate" runat="server" Text="SN" Locked="true" DataIndex="SN"
                            MenuDisabled="false" Sortable="false" Align="Right" Width="50" />
                        <ext:Column ID="Column1" runat="server" Text="EIN" Locked="true" DataIndex="EmployeeId"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
                        <ext:Column ID="Column2" runat="server" Text="AccountNo" Locked="true" DataIndex="AccountNo"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="100" />
                        <ext:Column ID="Column3" runat="server" Text="Name" Locked="true" DataIndex="Name"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="Column6" runat="server" Text="Deduction" DataIndex="Deduction" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column7" runat="server" Text="Taken On" DataIndex="TakenOnText"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column4"  runat="server" Text="Taken On Eng" DataIndex="TakenOnEngText" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="100" />
                        <ext:Column ID="Column5" runat="server" Text="Principle" DataIndex="PrincipleAmount"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                            </ext:Column>
                        <ext:Column ID="income1" runat="server" Text="No Of Installment" DataIndex="NoOfInstallment"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="120">
                        </ext:Column>
                        <ext:Column ID="Column8" runat="server" Text="Rem Installment" DataIndex="RemainingInstallment"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="100">
                        </ext:Column>
                        <ext:Column ID="Column9" runat="server" Text="PPMT" DataIndex="PPMT"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="90">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Column10" runat="server" Text="End Date" DataIndex="EndDateText"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100">
                        </ext:Column>
                        <ext:Column ID="Column11"  runat="server" Text="End Date Eng" DataIndex="EndDateEngText"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="100">
                        </ext:Column>
                        <ext:Column ID="Column12" runat="server" Text="Rem Including Current Month" DataIndex="RemInstallmentIncludingCurrentMonth"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
