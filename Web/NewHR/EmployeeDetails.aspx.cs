﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils;
using System.IO;
using Web.Helper;

namespace Web.NewHR
{
    public partial class EmployeeDetails : BasePage
    {
        int employeeId = 0;

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                Response.Redirect("EmployeeSummary.aspx?ID=" + Request.QueryString["ID"]);

            this.DisableViewStateForExtResourceManager();

            employeeId = int.Parse(Request.QueryString["ID"]);

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                //gender1.InnerHtml = string.Format(gender.InnerHtml, CommonManager.GetGenderName);
                gender1.InnerHtml = string.Format("{0}", CommonManager.GetGenderName);
                LoadEditData();

            }
        }

        public void btnLoadHistory_Click(object sender, DirectEventArgs e)
        {
            List<GetServiceHistoryListResult> dataTerminations = EmployeeManager.GetServiceHistoryList
               (employeeId, null,null,0,99999,null);

            ServiceHistory.Store[0].DataSource = dataTerminations;
            ServiceHistory.Store[0].DataBind();
        }

        private void LoadEditData()
        {
            foreach (Ext.Net.HyperLink item in btnEditDetails.Menu[0].Items)
            {
                item.NavigateUrl += employeeId;
            }

            foreach (Ext.Net.HyperLink item in slitBtnQuickTasks.Menu[0].Items)
            {
                item.NavigateUrl += employeeId;
            }

            linkRewardAddition.Visible = CommonManager.CompanySetting.HasLevelGradeSalary;
           

            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.RBB)
                divRBBReward.Visible = false;

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            if (emp != null)
            {

                LoadBasicInfo(emp);
                LoadPersonalInfo(emp);
                LoadContactInfo(emp);
                LoadHireDetails(emp);
                LoadLevelGradeHistory(emp);

                health.HRef += emp.EmployeeId;
                family.HRef += emp.EmployeeId;
                prevEmployment.HRef += emp.EmployeeId;

                skill9.HRef += emp.EmployeeId;
                skill10.HRef += emp.EmployeeId;
                skill11.HRef += emp.EmployeeId;
                skill12.HRef += emp.EmployeeId;
                skill13.HRef += emp.EmployeeId;
                skill14.HRef += emp.EmployeeId;

                if (emp.IsRetired != null && emp.IsRetired.Value)
                {
                    if (emp.EHumanResources[0].DateOfRetirementEng != null && emp.EHumanResources[0].DateOfRetirementEng < DateTime.Now)
                    {
                        spanRetiredDate.InnerHtml = "Retired : " + emp.EHumanResources[0].DateOfRetirement + " (" + emp.EHumanResources[0].DateOfRetirementEng.Value.ToLongDateString() + ")";
                    }
                }
                if (emp.IsResigned != null && emp.IsResigned.Value)
                {
                    if (emp.EHumanResources[0].DateOfResignationEng != null && emp.EHumanResources[0].DateOfResignationEng < DateTime.Now)
                    {
                        spanRetiredDate.InnerHtml = "Retired : " + emp.EHumanResources[0].DateOfResignation + " (" + emp.EHumanResources[0].DateOfResignationEng.Value.ToLongDateString() + ")";
                    }
                }
            }
        }

        private void LoadLevelGradeHistory(EEmployee emp)
        {
            gridLevelGradeHistory.Store[0].DataSource = NewHRManager.GetLevelGradeHistory(emp.EmployeeId);
            gridLevelGradeHistory.Store[0].DataBind();
        }

        private void LoadHireDetails(EEmployee emp)
        {

            //empPostionAndGrade.HRef += emp.EmployeeId;
            // Group/Level
            int? levelId = null; 
            double grade = 0;
            NewHRManager.SetEmployeeJoiningLevelGrade(ref levelId, ref grade, emp.EmployeeId);
            if (levelId != null)
            {
                BLevel levelEntity = NewPayrollManager.GetLevelById(levelId.Value);
                if (levelEntity.BLevelGroup.Name == levelEntity.Name)
                    level.InnerHtml = levelEntity.Name;
                else
                    level.InnerHtml = levelEntity.BLevelGroup.Name + " - " + levelEntity.Name;
                gradeDisplay.InnerHtml = grade.ToString();

                designationChange.Visible = false;
                salaryChange.Visible = false;
            }
            else
            {
                if (emp.EDesignation.BLevel != null)
                {
                    if (emp.EDesignation.BLevel.BLevelGroup.Name == emp.EDesignation.BLevel.BLevelGroup.Name)
                        level.InnerHtml = emp.EDesignation.BLevel.BLevelGroup.Name;
                    else
                        level.InnerHtml = emp.EDesignation.BLevel.BLevelGroup.Name + " - " + emp.EDesignation.BLevel.Name;
                }
                if (CommonManager.Setting.ShowGradeStep != null && CommonManager.Setting.ShowGradeStep.Value)
                {
                    if (emp.EGrade != null)
                        gradeDisplay.InnerHtml = emp.EGrade.Name;
                }
                else
                {
                    trGrade.Visible = false;
                }
                divLevelGradeHistory.Visible = false;

            }
            

            if (emp.EHumanResources.Count > 0)
            {
                if (emp.EHumanResources[0].HireMethod != null)
                    hireMethod.InnerHtml = EmployeeManager.GetHireMethod(emp.EHumanResources[0].HireMethod.Value).HireMethodName;

            }

            ServicePeroid first = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            if (first != null)
                hireDate.InnerHtml = first.FromDate;

            ECurrentStatus empLastStatus = new EmployeeManager().GetCurrentLastStatus(emp.EmployeeId);
            if (empLastStatus != null)
            {
                currentJobStatus.InnerHtml = empLastStatus.CurrentStatusText;

                if (!string.IsNullOrEmpty(empLastStatus.ToDate))
                    contractEndDate.InnerHtml = empLastStatus.ToDate;

            }
        }

        private void LoadContactInfo(EEmployee emp)
        {
            editContact.HRef += emp.EmployeeId;
            editAddress.HRef += emp.EmployeeId;

            
            if (emp.EAddresses.Count > 0)
            {
                // Address
                EAddress entity = emp.EAddresses[0];

                string present = "", permanent = "";
                WebHelper.GetAddressHTML(entity, ref present, ref permanent);
                addressTemporary.InnerHtml = present;
                addressPermanent.InnerHtml = permanent;

                if (!string.IsNullOrEmpty(entity.PSCitIssDis))
                {
                    rowCitizenshipIssueAddress.Visible = true;
                    spanCitizenshipIssueAddress.InnerHtml = entity.PSCitIssDis;
                }

                // Contact
                extNo.InnerHtml = entity.Extension;
                noPersonalNo.InnerHtml = entity.PersonalPhone;
                noPersonalMobile.InnerHtml = entity.PersonalMobile;
                contactEmergency.InnerHtml = entity.EmergencyName + (!string.IsNullOrEmpty(entity.EmergencyRelation) ? ", " + entity.EmergencyRelation : "");
                contactEmergencyNo.InnerHtml = entity.EmergencyPhone + (!string.IsNullOrEmpty(entity.EmergencyMobile) ? " / " + entity.EmergencyMobile : "");
            }
        }
        private void LoadPersonalInfo(EEmployee emp)
        {
            ein.InnerHtml = emp.EmployeeId.ToString();
            gender.InnerHtml = new Gender().GetText(emp.Gender.Value);
            dob.InnerHtml = emp.DateOfBirth;
            maritalStatus.InnerHtml = emp.MaritalStatus;
            fatherName.InnerHtml = emp.FatherName;

            editPersonalInfo.HRef += emp.EmployeeId;

            if (emp.FunctionalTitleId != null)
            {
                FunctionalTitle title = ListManager.GetFunctionalTitle(emp.FunctionalTitleId.Value);
                if (title != null)
                    spanFunctionalTitle.InnerHtml = title.Name;
            }
            else
                rowFunctionalTitle.Visible = false;

            
        }
        private void LoadBasicInfo(EEmployee emp)
        {

            title.InnerHtml = emp.Title + " " + emp.Name + " - " + emp.EmployeeId;
            if (!string.IsNullOrEmpty(emp.EHumanResources[0].IdCardNo))
            {
                title.InnerHtml += " (I No : " + emp.EHumanResources[0].IdCardNo + ")";
            }
            // Designation/Position
            if (emp.EDesignation != null)
                positionDesignation.InnerHtml = emp.EDesignation.Name;
            // Group/Level
            if (emp.PEmployeeIncomes.Count > 0 && emp.PEmployeeIncomes[0].LevelId != null)
            {
                BLevel level = NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value);
                BLevel actingLevel = NewPayrollManager.GetActingLevelInTodayDate(emp.EmployeeId);

                if (level != null)
                    groupLevel.InnerHtml = level.BLevelGroup.Name + " (Level - " + level.Name +
                        (actingLevel == null ? "" : ", Acting Level - " + actingLevel.Name)
                        + ")";
            }
            else
                groupLevelImg.Visible = false;

            // Branch
            GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);
            if (currentBranchDep != null)
            {
                branch.InnerHtml = currentBranchDep.Department + ", " + currentBranchDep.Branch;
            }

            // Contact
            if (emp.EAddresses.Count > 0)
            {
                EAddress entity = emp.EAddresses[0];

                //if (!string.IsNullOrEmpty(entity.CIPhoneNo) || !string.IsNullOrEmpty(entity.CIMobileNo) || !string.IsNullOrEmpty(entity.Extension))
                //    contactMobile.InnerHtml += string.Format("{0} / {1} ({2})", entity.CIPhoneNo, entity.CIMobileNo, entity.Extension);


                // Mobile
                //if (EmployeeManager.IsBranchOrDepartmentOrRegionalHead(emp.EmployeeId))
                //{
                    if (!string.IsNullOrEmpty(entity.CIMobileNo))
                    {
                        contactMobile.InnerHtml = entity.CIMobileNo + " (O)";

                        if (!string.IsNullOrEmpty(entity.PersonalMobile))
                        {
                            contactMobile.InnerHtml += " " + entity.PersonalMobile + " (P)";
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(entity.PersonalMobile))
                        {
                            contactMobile.InnerHtml += entity.PersonalMobile + " (P)";
                        }
                    }
                    contactMobile.InnerHtml += "&nbsp;";
                //}
                //else
                //{
                //    contactMobile.Visible = false;
                //    imgMobile.Visible = false;
                //}

                // Phone
                if (!string.IsNullOrEmpty(entity.CIPhoneNo))
                    contactPhone.InnerHtml = entity.CIPhoneNo;
                if( !string.IsNullOrEmpty(entity.Extension))
                    contactPhone.InnerHtml += " - " + entity.Extension;
                if (contactPhone.InnerHtml != "")
                    contactPhone.InnerHtml += " (O)";

                if (!string.IsNullOrEmpty(entity.PersonalPhone))
                    contactPhone.InnerHtml += " " + entity.PersonalPhone + " (R)";

                contactPhone.InnerHtml += "&nbsp;";
            

                email.InnerHtml += string.Format("<a href='mailto:{0}'>{0}</a> ", entity.CIEmail);
            }

            // Working for since
            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid servicePeriod = EmployeeManager.GetServicePeroidDate(employeeId);
            if (servicePeriod != null)
            {
                NewHelper.GetElapsedTime(servicePeriod.FromDateEng, servicePeriod.ToDateEng.Value, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " years";
                if(months != 0)
                    text += " " + months + " months";
                if (days != 0)
                    text += " " + days + " days";

                workingFor.InnerHtml += text;

                workingFor.InnerHtml += ", Since " +
                    (IsEnglish ? servicePeriod.FromDateEng.ToShortDateString() : servicePeriod.FromDate + " (" + servicePeriod.FromDateEng.ToShortDateString() + ")");
                
            }

            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }
        }

        private void HideButtonsAndColumns()
        {
           
        }

     

        protected void btnEditEmployee_Click(object sender, DirectEventArgs e)
        {
            if(string.IsNullOrEmpty(Request.QueryString["ID"]))
                return;
            string EmployeeId = Request.QueryString["ID"].ToString();
            Response.Redirect("~/newhr/PersonalDetail.aspx?ID="+ employeeId);
        }

       

    }
}