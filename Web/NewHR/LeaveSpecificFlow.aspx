﻿<%@ Page Title="Leave Specific Flow" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="LeaveSpecificFlow.aspx.cs" Inherits="Web.NewHR.LeaveSpecificFlow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }
        
    var CommandHandler = function(command, record){
            <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
            
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             };
   
   function searchList() {
             <%=gridLeaveSpecificFlow.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    function ImportPopup()
    {       
        if(<%=cmbTypeFilter.ClientID %>.getValue() == null || ctl00_ContentPlaceHolder_Main_cmbTypeFilter.getValue() == '')
        {
            alert('Type is required.');
            return;
        }
        leaveSpecImport('Type=' + <%=cmbTypeFilter.ClientID %>.getValue());
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the record?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
        <Proxy>
            <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                <ActionMethods Read="GET" />
                <Reader>
                    <ext:JsonReader Root="plants" TotalProperty="total" />
                </Reader>
            </ext:AjaxProxy>
        </Proxy>
        <Model>
            <ext:Model ID="Model5" IDProperty="Name" Name="ItemLineModel" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store ID="storeEmployee" runat="server">
        <Model>
            <ext:Model ID="modelEmployee" runat="server">
                <Fields>
                    <ext:ModelField Name="NameEIN" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store ID="storeEmployeeAdd" runat="server">
        <Model>
            <ext:Model ID="model1" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Specific Leave/Overtime Authority
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <table>
                <tr>
                    <td>
                        <ext:Button ID="btnAddNew" MarginSpec="25 10 10 10" Width="150" Height="30" runat="server"
                            Text="Add New Definition">
                            <DirectEvents>
                                <Click OnEvent="btnAddNew_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="Button1" MarginSpec="25 10 10 10" Width="150" Height="30" OnClientClick="ImportPopup();return false;"
                            runat="server" Text="Import from Excel">
                        </ext:Button>
                    </td>
                </tr>
            </table>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td style="width: 210px;">
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" EmptyText="Search Employee" runat="server"
                                        FieldLabel="Employee" LabelAlign="Top" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="250" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <%--<td style="width: 160px;">
                                    <ext:ComboBox ID="cmbBranchFilter"  runat="server" ValueField="BranchId" DisplayField="Name" EmptyText="Branch Filter" FieldLabel="Branch" LabelAlign="Top"
                                        Width="150" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                        <Store>
                                            <ext:Store ID="Store2" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model2" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" Type="String" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                       <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                   this.clearValue(); 
                                                   this.getTrigger(0).hide();
                                               }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="width: 160px;">
                                    <ext:ComboBox ID="cmbDepartmentFilter" runat="server" ValueField="DepartmentId" DisplayField="Name" EmptyText="Department Filter" FieldLabel="Department" LabelAlign="Top"
                                        Width="150" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model3" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" Type="String" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                   this.clearValue(); 
                                                   this.getTrigger(0).hide();
                                               }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>--%>
                                <td style="width: 210px;">
                                    <ext:ComboBox ID="cmbRecommenderFilter" runat="server" ValueField="EmployeeId" DisplayField="NameEIN"
                                        EmptyText="Recommender Filter" FieldLabel="Recommender" LabelAlign="Top" Width="200"
                                        LabelSeparator="" ForceSelection="true" QueryMode="Local" StoreID="storeEmployee">
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                   this.clearValue(); 
                                                   this.getTrigger(0).hide();
                                               }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="width: 210px;">
                                    <ext:ComboBox ID="cmbApprovalFilter" runat="server" ValueField="EmployeeId" DisplayField="NameEIN"
                                        EmptyText="Approval Filter" FieldLabel="Approval" LabelAlign="Top" Width="200"
                                        LabelSeparator="" ForceSelection="true" QueryMode="Local" StoreID="storeEmployee">
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                   this.clearValue(); 
                                                   this.getTrigger(0).hide();
                                               }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td style="width: 210px;">
                                    <ext:ComboBox ID="cmbTypeFilter" runat="server" FieldLabel="Type" LabelAlign="Top"
                                        Width="200" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Text="Leave" Value="1" />
                                            <ext:ListItem Text="Overtime" Value="2" />
                                        </Items>
                                        <SelectedItems>
                                            <ext:ListItem Text="Leave" Value="1" />
                                        </SelectedItems>
                                        <Listeners>
                                            <Select Handler="searchList();" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" StyleSpec="padding-top:12px;" runat="server" Text="Load"
                                        Width="100" Cls="btn btn-default" MarginSpec="10 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridLeaveSpecificFlow" runat="server"
                Cls="itemgrid">
                <Store>
                    <ext:Store ID="storeLeaveSpecEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model6" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="Int" />
                                    <ext:ModelField Name="Name" Type="string" />
                                    <ext:ModelField Name="Branch" Type="string" />
                                    <ext:ModelField Name="Department" Type="string" />
                                    <ext:ModelField Name="R1Name" Type="string" />
                                    <ext:ModelField Name="R2Name" Type="string" />
                                    <ext:ModelField Name="A1Name" Type="string" />
                                    <ext:ModelField Name="A2Name" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="EmployeeId" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colEmployeeId" Sortable="false" MenuDisabled="true" runat="server"
                            Text="EID" Align="Center" Width="60" DataIndex="EmployeeId" />
                        <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Name" Align="Left" Width="160" DataIndex="Name" />
                        <ext:Column ID="colBranchName" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Branch" Align="Left" Width="120" DataIndex="Branch" />
                        <ext:Column ID="colDepartmentName" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Deparment" Align="Left" Width="120" DataIndex="Department" />
                        <ext:Column ID="colR1Name" Sortable="false" MenuDisabled="true" runat="server" Text="Recommender1"
                            Align="Left" Width="160" DataIndex="R1Name" />
                        <ext:Column ID="colR2Name" Sortable="false" MenuDisabled="true" runat="server" Text="Recommender2"
                            Align="Left" Width="160" DataIndex="R2Name" />
                        <ext:Column ID="colA1Name" Sortable="false" MenuDisabled="true" runat="server" Text="Approval1"
                            Align="Left" Width="160" DataIndex="A1Name" />
                        <ext:Column ID="colA2Name" Sortable="false" MenuDisabled="true" runat="server" Text="Approval2"
                            Align="Left" Width="160" DataIndex="A2Name" />
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="50" Text="Edit" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                    CommandName="Edit" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                        <ext:CommandColumn ID="CommandColumn2" runat="server" Width="50" Text="Delete" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                    CommandName="Delete" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeLeaveSpecEmpList"
                        DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchList()" />
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                    <ext:ListItem Value="300" Text="300" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
    </div>
    <ext:Window ID="winLeaveSpec" runat="server" Title="Employee Leave Approval Details"
        Width="500" Height="400" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:DisplayField ID="dispType" runat="server" FieldLabel="Type" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbEmployee" runat="server" ValueField="EmployeeId" DisplayField="Name" TypeAhead="true" 
                            FieldLabel="Employee Name" LabelAlign="Top" Text="Employee" Width="200" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local" StoreID="storeEmployeeAdd">
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                            </Listeners>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="cmbEmployee"
                            Display="None" ErrorMessage="Employee is required." ValidationGroup="SaveLeaveSp"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbEmployeeRecommender1" runat="server" ValueField="EmployeeId" TypeAhead="true"
                            DisplayField="Name" Text="Employee" LabelAlign="Top" FieldLabel="Leave Recommendation"
                            Width="200" LabelSeparator="" ForceSelection="true" QueryMode="Local" StoreID="storeEmployeeAdd">
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="vertical-align: bottom;">
                        <ext:ComboBox ID="cmbEmployeeRecommender2" runat="server" ValueField="EmployeeId" TypeAhead="true"
                            DisplayField="Name" Text="Employee" LabelAlign="Top" FieldLabel="" Width="200"
                            LabelSeparator="" ForceSelection="true" QueryMode="Local" StoreID="storeEmployeeAdd">
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbEmployeeApproval1" runat="server" ValueField="EmployeeId" DisplayField="Name" TypeAhead="true"
                            Text="Employee" LabelAlign="Top" FieldLabel="Leave Approval" Width="200" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local" StoreID="storeEmployeeAdd">
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="vertical-align: bottom;">
                        <ext:ComboBox ID="cmbEmployeeApproval2" runat="server" ValueField="EmployeeId" DisplayField="Name" TypeAhead="true"
                            Text="Employee" LabelAlign="Top" FieldLabel="" Width="200" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local" StoreID="storeEmployeeAdd">
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Focus Handler="this.lastQuery = '';this.store.clearFilter();" />
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveLeaveSp';">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" Cls="btnFlatLeftGap" ID="LinkButton1"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{winLeaveSpec}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
