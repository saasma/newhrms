﻿<%@ Page Title="SI Loan Import" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="SILoanImport.aspx.cs" Inherits="Web.NewHR.SILoanImport" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

   
    var blockMultipleFormSubmit = true;
    
    function refreshWindow() {
        <%=btnLoad.ClientID %>.fireEvent('click');
    }

    function isPayrollSelected(btn) {
      
       
        if(<%=cmbLoan.ClientID %>.value == null)
        {
            return;
        }
        var ret = shiftPopup("DeductionId=" + <%=cmbLoan.ClientID %>.value);
        return false;
    }

    var columnRenderer = function (value, meta, record) {
        if (value == null)
            return "";

        if (value == "")
            return "";
        return value.toFixed(2);
    }



    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">            <div class="media-body">
                <h4 runat="server" id="title">
                    Simple Interest Loans</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding: 10px">
            <table>
                <tr>
                    <td style='width: 800px'>
                        <ext:ComboBox ValueField="DeductionId" FieldLabel="Loan" Width="200" LabelWidth="50"
                            LabelAlign="Left" DisplayField="Title" ID="cmbLoan" runat="server">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="modelLoan" runat="server">
                                            <Fields>
                                                <ext:ModelField runat="server" Name="DeductionId" Type="Int" />
                                                <ext:ModelField runat="server" Name="Title" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Change OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Change>
                            </DirectEvents>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                            ValidationGroup="SaveUpdate" ControlToValidate="cmbLoan" ErrorMessage="Loan is required." />
                    </td>
                    <td>
                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-default btn-sect btn-sm" Width="100"
                            Height="30" Text="Show">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style='padding-left: 10px'>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Import from Excel" OnClientClick="valGroup = 'SaveUpdate'; if(CheckValidation()) return isPayrollSelected(this); else return false;"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div style="clear: both" runat="server" id="gridContainer">
        </div>
        <br />
        <ext:GridPanel ID="gridLoanRepayment" ClicksToEdit="1" Border="true" StripeRows="true"
            Header="false" runat="server" Height="500" Width="1550px" Title="Title" Cls="gridtbl">
            <Store>
                <ext:Store ID="storeLoanRepayment" runat="server">
                    <Model>
                        <ext:Model ID="Model1" runat="server">
                            <Fields>
                                <ext:ModelField Name="EmployeeDeductionId" Type="Int" />
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="AdvanceLoanAmount" Type="Float" />
                                <ext:ModelField Name="TakenOnEng" Type="Date" />
                                <ext:ModelField Name="ToBePaidOver" Type="String" />
                                <ext:ModelField Name="TakenOnEng" Type="String" />
                                 <ext:ModelField Name="InterestRate" Type="String" />
                                <ext:ModelField Name="MarketRate" Type="String" />
                                <ext:ModelField Name="PaymentTerms" Type="String" />
                                <ext:ModelField Name="StartingFromEng" Type="Date" />                                                           
                                <ext:ModelField Name="INo" Type="String" /> 
                                <ext:ModelField Name="DateFrom" Type="String" />
                                <ext:ModelField Name="DateTo" Type="String" />   
                                <ext:ModelField Name="LastPaymentEng" Type="Date" />     
                                <ext:ModelField Name="TakenDate" Type="String" />                              
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <Listeners>
            </Listeners>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel11" runat="server">
                </ext:RowSelectionModel>
            </SelectionModel>
            <ColumnModel runat="server" ID="columnModel">
                <Columns>
                    <ext:Column ID="Column1" runat="server" Sortable="false" MenuDisabled="true" Header="EIN" Align="Center"
                        Width="50" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="Column4" runat="server" Sortable="false" MenuDisabled="true" Header="I No" Align="Center" Hidden="true"
                        Width="50" DataIndex="INo">
                    </ext:Column>
                    <ext:Column ID="ColumnEmployeeName" runat="server" Sortable="false" MenuDisabled="true" Header="Name" Width="160" Align="Left"
                        ColumnID="EmployeeName" DataIndex="EmployeeName">
                    </ext:Column>

                    <ext:NumberColumn ID="NumberColumn1" runat="server" Sortable="false" Align="Right"
                        MenuDisabled="true" Header="Loan Amount" Width="110" ColumnID="AdvanceLoanAmount"
                        DataIndex="AdvanceLoanAmount">
                    </ext:NumberColumn>  
                    
                    <ext:DateColumn Format="yyyy/MM/dd" ID="DateColumn2" runat="server" Sortable="false" Align="Right" MenuDisabled="true"
                        Header="Loan Taken On" Width="110" DataIndex="TakenOnEng">
                    </ext:DateColumn>                 

                     <ext:Column ID="colStartingFrom" runat="server" Sortable="false" Align="Left" MenuDisabled="true"
                        Header="Taken On Year/Month" Width="140" DataIndex="TakenDate">
                    </ext:Column>                    

                    <ext:NumberColumn ID="NumberColumn2" runat="server" Sortable="false" Align="Right"
                        MenuDisabled="true" Header="Interest Rate" Width="90" ColumnID="AdvanceLoanAmount"
                        DataIndex="InterestRate">
                    </ext:NumberColumn>

                    <ext:NumberColumn ID="NumberColumn3" runat="server" Sortable="false" Align="Right"
                        MenuDisabled="true" Header="Market Rate" Width="90" ColumnID="AdvanceLoanAmount"
                        DataIndex="MarketRate">
                    </ext:NumberColumn>
                    
                    <ext:Column ID="Column5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="Installment No" Width="100" DataIndex="ToBePaidOver">
                    </ext:Column>

                    <ext:Column ID="Column6" runat="server" Sortable="false" Align="Left" MenuDisabled="true" 
                        Header="Payment Terms" Width="110" DataIndex="PaymentTerms">
                    </ext:Column>

                    <ext:DateColumn Format="yyyy/MM/dd" ID="Column7" runat="server" Sortable="false" Align="Right" MenuDisabled="true"
                        Header="Deduction Start Date" Width="140" DataIndex="StartingFromEng">
                    </ext:DateColumn>

                    <ext:Column ID="Column2" runat="server" Sortable="false" Align="Left" MenuDisabled="true"
                        Header="Ded. Start Year/Month" Width="140" DataIndex="DateFrom">
                    </ext:Column>

                    <ext:DateColumn Format="yyyy/MM/dd" ID="DateColumn1" runat="server" Sortable="false" Align="Right" MenuDisabled="true"
                        Header="Deduction End Date" Width="140" DataIndex="StartingFromEng">
                    </ext:DateColumn>

                    <ext:Column ID="Column3" runat="server" Sortable="false" Align="Left" MenuDisabled="true"
                        Header="Ded. End Year/Month" Width="140" DataIndex="DateTo">
                    </ext:Column>
                   
                    

                </Columns>
            </ColumnModel>
        </ext:GridPanel>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
