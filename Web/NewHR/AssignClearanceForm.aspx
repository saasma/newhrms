﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true"
    CodeBehind="AssignClearanceForm.aspx.cs" Inherits="Web.NewHR.AssignClearanceForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #ctl00_ContentPlaceHolder_Main_TextFieldNote-bodyEl {
            padding-top: 25px;
            padding-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">


    <ext:Panel
        ID="Resigned"
        runat="server"
        Width="900"
        Height="500"
        BodyPadding="10">
        <Content>
            <ext:FormPanel
                ID="panel"
                runat="server"
                Width="500"
                Height="410"
                Frame="true"
                Title="Clearance Form"
                BodyPadding="13">

                <Items>
                    <ext:Label runat="server" Text="Form Name*"></ext:Label>
                    <ext:TextField ID="txtFormName" Width="460px" runat="server"></ext:TextField>
                    <ext:FieldSet
                        ID="chkFormDetail"
                        runat="server"
                        Flex="1"
                        Title="Show*"
                        Layout="AnchorLayout"
                        DefaultAnchor="100%">
                        <Items>
                            <ext:Checkbox ID="chkLoanDetail" runat="server" BoxLabel="Loan Details" />
                            <ext:Checkbox ID="chkAdvanceDetail" runat="server" BoxLabel="Advance Details" />
                            <ext:Checkbox ID="chkRefundableDeposit" runat="server" BoxLabel="Refundable Deposits" />
                            <ext:Checkbox ID="chkAssetInCustody" runat="server" BoxLabel="Assets In Custody" />
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldContainer Layout="HBoxLayout" runat="server">
                        <Items>
                            <ext:ComboBox runat="server" SelectedIndex="0" Width="120" ID="cmbType" ForceSelection="true"
                                FieldLabel="Send this form to*" LabelAlign="Top" DisplayField="UserName"
                                ValueField="UserID" QueryMode="Local"
                                LabelSeparator="">
                                <Store>
                                    <ext:Store ID="store1" runat="server">
                                        <Model>
                                            <ext:Model runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="UserName" />
                                                    <ext:ModelField Name="UserID" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:TextField Width="335px" ID="TextFieldNote" runat="server"></ext:TextField>
                        </Items>
                    </ext:FieldContainer>
                </Items>
                <Items>
                    <ext:Button Cls="btn btn-primary" runat="server" Text="Save">
                        <DirectEvents>
                            <Click OnEvent="btnSave">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Items>
            </ext:FormPanel>
        </Content>
    </ext:Panel>
</asp:Content>
