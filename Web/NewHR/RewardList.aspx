﻿<%@ Page Title="Reward List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="RewardList.aspx.cs" Inherits="Web.NewHR.RewardList" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/RewardListCtl.ascx" TagName="FamilyCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Reward List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                </h4>
            </div>
            <div class="widget-body">
                <uc1:FamilyCtl Id="EmployeeDetailsCtl2" runat="server" />
            </div>
        </div>
    </div>
    <br />
</asp:Content>
