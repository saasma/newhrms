﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using DAL;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class Reward : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }
        protected void Initialise()
        {

            
            storeEmployee.DataSource = EmployeeManager.GetAllEmployees();
            storeEmployee.DataBind();


            cmbCurrentLevel.Store[0].DataSource = NewPayrollManager.GetAllParentLevels();
            cmbCurrentLevel.Store[0].DataBind();


            cmbCurrentPost.Store[0].DataSource = new CommonManager().GetAllDesignations();
            cmbCurrentPost.Store[0].DataBind();

            LoadEmpFromQueryString();
        }
        private void LoadEmpFromQueryString()
        {
            int deputaionId = UrlHelper.GetIdFromQueryString("RewardID");
            if (deputaionId != 0)
                hdnID.Value = deputaionId.ToString();

            int did = GetActingID();
            if (did != 0)
            {
                RewardEmployee ded = NewPayrollManager.GetRewardEmployeeById(did);
                               

                Process(ded);
                EEmployee emp = EmployeeManager.GetEmployeeById(ded.EmployeeId);
                employeeBlock.SetEmployeeDetailsWithPhoto(emp);

                ddlEmployee.Disable();
            }
        }
        private RewardEmployee Process(RewardEmployee entity)
        {
            if (entity == null)
            {
                entity = new RewardEmployee();

                entity.EmployeeId = int.Parse(ddlEmployee.SelectedItem.Value);
                entity.LetterNumber = txtLetterNo.Text.Trim();
                entity.LetterDate = calLetterDate.Text.Trim();
                entity.LetterDateEng = GetEngDate(entity.LetterDate);
                entity.RewardDate = calRewardDate.Text.Trim();
                entity.RewardDateEng = GetEngDate(entity.RewardDate);
                entity.RecommendedByEmployeeId = int.Parse(cmbRecommendedBy.SelectedItem.Value);
                entity.Type = int.Parse(cmbType.SelectedItem.Value);
                if (entity.Type == 1)
                    entity.CashAmount = (decimal)(txtReward.Number);
                else
                {
                    entity.Grade = (int)(txtReward.Number);
                    entity.GradeEffectiveFrom = calEffectiveFrom.Text.Trim();
                    entity.GradeEffectiveFromEng = GetEngDate(entity.GradeEffectiveFrom);
                }
                entity.LevelId = int.Parse(cmbCurrentLevel.SelectedItem.Value);
                entity.DesignationId = int.Parse(cmbCurrentPost.SelectedItem.Value);

                entity.Notes = txtNotes.Text.Trim();
                entity.RewardGrantedFor = txtRewardGrantedFor.Text.Trim();
                return entity;
            }
            else
            {
                ExtControlHelper.ComboBoxSetSelected(entity.EmployeeId.ToString(), ddlEmployee);
               
                txtLetterNo.Text = entity.LetterNumber;
                calLetterDate.Text = entity.LetterDate;
                calRewardDate.Text = entity.RewardDate;
                calEffectiveFrom.Text = entity.GradeEffectiveFrom;

                ExtControlHelper.ComboBoxSetSelected(entity.RecommendedByEmployeeId.ToString(), cmbRecommendedBy);
                ExtControlHelper.ComboBoxSetSelected(entity.Type.ToString(), cmbType);
                if (entity.Type == 1)
                    txtReward.Number = (double)entity.CashAmount;
                else
                    txtReward.Number =(double) entity.Grade;

                ExtControlHelper.ComboBoxSetSelected(entity.LevelId.ToString(), cmbCurrentLevel);
                ExtControlHelper.ComboBoxSetSelected(entity.DesignationId.ToString(), cmbCurrentPost);

                txtRewardGrantedFor.Text = entity.RewardGrantedFor;
                txtNotes.Text = entity.Notes;
               
            }
            return null;
        }
        protected void cmbActionTypes_Select(object sender, DirectEventArgs e)
        {

        }

        protected void ddlEmployee_Select(object sender, DirectEventArgs e)
        {
            EEmployee emp = EmployeeManager.GetEmployeeById(int.Parse(ddlEmployee.SelectedItem.Value));

            BLevel currentLevel = NewHRManager.GetEmployeeLastestLevel(emp.EmployeeId);
            if (currentLevel != null)
            {
                cmbCurrentLevel.SetValue(currentLevel.LevelId.ToString());
            }
            else
            {
                cmbCurrentLevel.ClearValue();
            }

            EDesignation currentPost = NewHRManager.GetEmployeeCurrentPostDesitionPosition(emp.EmployeeId);
            if (currentPost != null)
            {

                cmbCurrentPost.SetValue(currentPost.DesignationId.ToString());
            }
            else
            {
                cmbCurrentPost.ClearValue();
            }
            employeeBlock.SetEmployeeDetailsWithPhoto(emp);
        }

        
       

        

        private void LoadList()
        {
           
        }
        public int GetActingID()
        {
            if (hdnID.Text != "" && hdnID.Text != null)
                return int.Parse(hdnID.Text);
            return 0;
        }
        public void btnSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                //this.Value = "true";

                RewardEmployee history = Process(null);
                history.EmployeeId = int.Parse(ddlEmployee.SelectedItem.Value);


                if (GetActingID() != 0)
                {
                    history.RewardID = GetActingID();
                    Status status = NewPayrollManager.InsertUpdateEmployeeReward(history, false);

                    //if (IsBranchTransfer)
                    //divMsgCtl.InnerHtml = "Acting updated.";
                    //else
                    //    divMsgCtl.InnerHtml = "Department transfer updated.";
                }
                else
                {
                    Status status = NewPayrollManager.InsertUpdateEmployeeReward(history, true);
                    //if (IsBranchTransfer)
                    //divMsgCtl.InnerHtml = "Acting saved.";

                    hdnID.Value = history.RewardID.ToString();
                    
                    //else
                    //    divMsgCtl.InnerHtml = "Department transfer saved.";
                }
                //divMsgCtl.Hide = false;

                // Prepare Sub-Department List
                //StringBuilder str = new StringBuilder("");
                //bool first = true;
                //List<SubDepartment> deps = DepartmentManager.GetAllSubDepartmentsByDepartment(history.DepeartmentId.Value);
                //foreach (SubDepartment obj in deps)
                //{
                //    if (first == false)
                //        str.Append(",");
                //    str.Append("\"" + obj.SubDepartmentId + "$$" + obj.Name + "\"");
                //    first = false;
                //}
                ////Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());

                // Register to set Branch/Subdepartment from js in Employee page


                Response.Redirect("~/newhr/RewardList.aspx");

                ClearFields();
                //btnCancel_Click(null, null);
            }
        }
        protected void ClearFields()
        {
            this.hdnID.Text = "";
         
        }

       
    }
}