﻿<%@ Page Title="Employee OT Calculation" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeOTCalculation.aspx.cs" Inherits="Web.CP.EmployeeOTCalculation" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 
   
    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee OT Calculation
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td style="width: 160px">
                                    <pr:CalendarExtControl Width="150px" FieldLabel="From" ID="calFromDate" runat="server"
                                        LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                                <td>
                                    <pr:CalendarExtControl Width="150px" FieldLabel="To" ID="calToDate" runat="server"
                                        LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                                <td>
                                    <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                        LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                                        runat="server" FieldLabel="Branch">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store6" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                                        Hidden="true" Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId"
                                        QueryMode="Local" LabelSeparator="" LabelAlign="Top">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="storeLevel" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelLevel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="LevelId" Type="Int" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DesignationId" DisplayField="LevelAndDesignation"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store7" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model9" IDProperty="DesignationId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DesignationId" />
                                                            <ext:ModelField Name="LevelAndDesignation" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td style="width: 310px;">
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="250" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                Cls="itemgrid1" OnReadData="Store_ReadData"   AutoScroll="true">
                <Store>
                    <ext:Store PageSize="50" ID="storeEmpList" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="RowNumber">
                                <Fields>
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="AccountNo" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="Position" Type="String" />
                                    <ext:ModelField Name="Designation" Type="String" />
                                    <ext:ModelField Name="AccountNo" Type="String" />
                                    <ext:ModelField Name="OTHoursRounded" Type="String" />
                                    <ext:ModelField Name="Rate" Type="String" />
                                    <ext:ModelField Name="OTName" Type="String" />
                                    <ext:ModelField Name="Calculation" Type="String" />
                                    <ext:ModelField Name="EligibleAmount" Type="String" />
                                    <ext:ModelField Name="OTAmount" Type="String" />
                                    <ext:ModelField Name="Period" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="EmployeeId" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                       
                        <ext:Column ID="Column1" Locked="true"  runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="50" />
                        <ext:Column ID="Column3" Locked="true"  runat="server" Text="Name" DataIndex="Name" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="Column4" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="110" />
                        <ext:Column ID="Column5" runat="server" Text="Department" DataIndex="Department"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="110" />
                        <ext:Column ID="Column6" runat="server" Text="Position/Level" DataIndex="Position" MenuDisabled="false"
                            Hidden="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column7" runat="server" Text="Designation" DataIndex="Designation"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="110" />
                        <ext:Column ID="OTHours" runat="server" Text="OT Hours" DataIndex="OTHoursRounded" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="70">
                        </ext:Column>
                        <ext:Column ID="Raasdfate" runat="server" Text="Rate per hour" DataIndex="Rate" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="50">
                        </ext:Column>
                        <ext:Column ID="Column2" runat="server" Text="OT Type" DataIndex="OTName" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="100">
                        </ext:Column>
                        <ext:Column ID="Calculation" runat="server" Text="Calculation" DataIndex="Calculation"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100">
                        </ext:Column>
                        <ext:Column ID="income5" runat="server" Text="Eligible Salary" DataIndex="EligibleAmount"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="110">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Column13" runat="server" Text="OT Amount" DataIndex="OTAmount" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="80">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                         <ext:Column ID="Column8" runat="server" Text="Period" DataIndex="Period"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                    </Columns>
                </ColumnModel>
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
