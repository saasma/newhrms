﻿<%@ Page Title="PreDefined Approval List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="LeaveRequestPredefinedList.aspx.cs" Inherits="Web.CP.LeaveRequestPredefinedList" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<%--#TODO: Group the Branc-Department-Department Head with Branch Column--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
            

            var TARenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeTravelOrder.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.StepName;
                };

            var AuthorityRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeAuthorityType.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.AuthorityTypeName;
                };

                var EmpRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                    var r = <%= storeEmployee.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Name;
                };
               
               var GroupRenderer = function(value)
               {
                    var r = <%= StoreGroup.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Name;
               } 
               
                 var LeaveRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                    var r = <%= storeLeaves.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Title;
                };

                var DesignationRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                    var r = <%= storeDesignation.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Name;
                };

                var BranchRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                    var r = <%= storeBranch.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Name;
                };
                var LeaveNotifTypeRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                    var r = <%= storeLeaveNotifyType.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.AuthorityTypeName;
                };

                var LeaveNotifBranchRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                    var r = <%= storeLeaveNotifBranch.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Name;
                };



            var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };
        
        var addNewRow = function (grid) {
            var newRow = new SettingModel();

       

            var rowIndex = grid.getStore().data.items.length;

            //gridProjectSplitter.stopEditing();
            grid.getStore().insert(rowIndex, newRow);
        }
        
        var addNewRow1 = function (grid) {
            var newRow = new SettingModel1();

       

            var rowIndex = grid.getStore().data.items.length;

            //gridProjectSplitter.stopEditing();
            grid.getStore().insert(rowIndex, newRow);
        }

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

       

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

        var RemoveItemLineLeave = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };

         var beforeEditApproval = function (e1, e, e2, e3) {


            if ((e.field == "EmployeeId" ) && e.record.data.AuthorityType != "6") 
            {
                    e.cancel = true;
                    return;
            }
             if ((e.field == "DesignationId" ) && e.record.data.AuthorityType != "4") 
            {
                    e.cancel = true;
                    return;
            }
            

             if ((e.field == "ApplyToBranchId" ) && e.record.data.AuthorityType != "4"  && e.record.data.AuthorityType != "6") 
            {
                    e.cancel = true;
                    return;
            }

            <%=storeDesignation.ClientID %>.clearFilter();
            <%=storeAuthorityType.ClientID %>.clearFilter();
            <%=storeEmployee.ClientID %>.clearFilter();
             <%=storeBranch.ClientID %>.clearFilter();
        }

        var beforeEdit = function (e1, e, e2, e3) {


            if ((e.field == "EmployeeId" ) && e.record.data.AuthorityType != "6") 
            {
                    e.cancel = true;
                    return;
            }
               
              if ((e.field == "DesignationId" ) && e.record.data.AuthorityType != "4") 
            {
                    e.cancel = true;
                    return;
            }
             if ((e.field == "ApplyToBranchId" ) && e.record.data.AuthorityType != "4"  && e.record.data.AuthorityType != "6") 
            {
                    e.cancel = true;
                    return;
            }

            <%=storeDesignation.ClientID %>.clearFilter();
            <%=storeAuthorityType.ClientID %>.clearFilter();
            <%=storeEmployee.ClientID %>.clearFilter();
            <%=storeBranch.ClientID %>.clearFilter();
        }


        var beforeEditLeaveNotify = function (e1, e, e2, e3) {

            if((e.field == "BranchId") && e.record.data.AuthorityType != "4")
            {   
                e.cancel = true;
                return;
            }

             if((e.field == "LevelGroupId") && e.record.data.AuthorityType != "5")
            {   
                e.cancel = true;
                return;
            }

            <%=storeBranch.ClientID %>.clearFilter();
            <%=storeEmployee.ClientID %>.clearFilter();
        }

    </script>
    <style>
        table#albums
        {
            border-collapse: separate;
            border-spacing: 0 10px;
        }
        
        table#albums1
        {
            border-collapse: separate;
            border-spacing: 0 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="hiddenValueDept">
    </ext:Hidden>
    <ext:Store runat="server" ID="storeTravelOrder">
        <Model>
            <ext:Model ID="Model2" IDProperty="StepID" runat="server">
                <Fields>
                    <ext:ModelField Name="StepName" />
                    <ext:ModelField Name="StepID" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store runat="server" ID="storeAuthorityType">
        <Model>
            <ext:Model ID="Model3" IDProperty="AuthorityType" runat="server">
                <Fields>
                    <ext:ModelField Name="AuthorityTypeName" />
                    <ext:ModelField Name="AuthorityType" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store runat="server" ID="storeEmployee">
        <Model>
            <ext:Model ID="Model4" IDProperty="EmployeeId" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" />
                    <ext:ModelField Name="EmployeeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
     <ext:Store runat="server" ID="storeLeaves">
        <Model>
            <ext:Model ID="Model11" IDProperty="LeaveTypeId" runat="server">
                <Fields>
                    <ext:ModelField Name="Title" />
                    <ext:ModelField Name="LeaveTypeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store runat="server" ID="storeDesignation">
        <Model>
            <ext:Model ID="Model5" IDProperty="DesignationId" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" />
                    <ext:ModelField Name="DesignationId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store runat="server" ID="storeBranch">
        <Model>
            <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" />
                    <ext:ModelField Name="BranchId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store runat="server" ID="storeLeaveNotifyType">
        <Model>
            <ext:Model ID="Model8" IDProperty="AuthorityType" runat="server">
                <Fields>
                    <ext:ModelField Name="AuthorityTypeName" />
                    <ext:ModelField Name="AuthorityType" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store runat="server" ID="storeLeaveNotifBranch">
        <Model>
            <ext:Model ID="Model9" IDProperty="BranchId" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" />
                    <ext:ModelField Name="BranchId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store ID="StoreGroup" runat="server">
        <Model>
            <ext:Model IDProperty="LevelGroupId" ID="Model10" runat="server">
                <Fields>
                    <ext:ModelField Name="LevelGroupId" Type="String" />
                    <ext:ModelField Name="Name" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Pre-Defined Approval Authorities
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <div class="alert alert-info">
                <ext:ComboBox LabelSeparator="" StyleSpec="clear:both;margin-bottom:15px;margin-left:10px;"
                    ID="cmbType" runat="server" FieldLabel="Process" LabelAlign="Top" Width="200"
                    QueryMode="Local" ForceSelection="true">
                    <Items>
                        <ext:ListItem Text="Leave" Value="1" />
                        <ext:ListItem Text="Overtime/Allowance/Attendance" Value="2" />
                        <%--<ext:ListItem Text="Evening Counter" Value="3" />--%>
                    </Items>
                    <DirectEvents>
                        <Select OnEvent="cmbType_Change">
                            <EventMask ShowMask="true" />
                        </Select>
                    </DirectEvents>
                </ext:ComboBox>
            </div>
            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                ValidationGroup="SaveUpdate" ControlToValidate="cmbType" ErrorMessage="Type is required." />
            <div>
                <div>
                    <!-- panel-btns -->
                    <h4 class="panel-title sectionHeading" style="width: 1122px; margin-left: 0px; margin-bottom: -20px;">
                        <i></i>Recommend List</h4>
                </div>
                <!-- panel-heading -->
                <div class="panel-body" style="padding-left: 0px">
                    <ext:GridPanel Title="" StyleSpec="margin-top:0px;" ID="gridAllowances" runat="server">
                        <Store>
                            <ext:Store ID="storeAllowances" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" Name="SettingModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="AuthorityType" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                            <ext:ModelField Name="DesignationId" Type="String" />
                                            <ext:ModelField Name="AuthorityTypeDisplayName" Type="String" />
                                            <ext:ModelField Name="ApplyToBranchId" Type="String" />
                                            <ext:ModelField Name="ApplyToBranchHeadAlso" Type="String" NullConvert="false" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                            </ext:CellEditing>
                        </Plugins>
                        <Listeners>
                            <BeforeEdit Fn="beforeEdit" />
                        </Listeners>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" DataIndex="SequenceNo" runat="server"
                                    Width="35" />
                                <ext:Column ID="Column1" runat="server" Text="Authority" Sortable="false" MenuDisabled="true"
                                    Width="150" DataIndex="AuthorityType" Border="false">
                                    <Renderer Fn="AuthorityRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="AuthorityTypeName" QueryMode="Local" ForceSelection="true"
                                            ValueField="AuthorityType" StoreID="storeAuthorityType" ID="ComboBox1" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="Authority Display Name" Sortable="false"
                                    MenuDisabled="true" Width="170" DataIndex="AuthorityTypeDisplayName" Border="false">
                                    <Editor>
                                        <ext:TextField ID="TextField1" runat="server" />
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column4" ToolTip="Select Designation for Specific Designation Authority"
                                    runat="server" Text="Designation" Sortable="false" MenuDisabled="true" Width="150"
                                    DataIndex="DesignationId" Border="false">
                                    <Renderer Fn="DesignationRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="Name" ForceSelection="true" QueryMode="Local" ValueField="DesignationId"
                                            StoreID="storeDesignation" ID="ComboBox4" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column2" runat="server" ToolTip="Select Employee for Specific Employee Authority"
                                    Text="Employee" Sortable="false" MenuDisabled="true" Width="180" DataIndex="EmployeeId"
                                    Border="false">
                                    <Renderer Fn="EmpRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="Name" ForceSelection="true" QueryMode="Local" ValueField="EmployeeId"
                                            StoreID="storeEmployee" ID="ComboBox2" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" ToolTip="Apply To This Branch Employee Only"
                                    Text="Apply To This Branch Only" Sortable="false" MenuDisabled="true" Width="200"
                                    DataIndex="ApplyToBranchId" Border="false">
                                    <Renderer Fn="BranchRenderer" />
                                    <Editor>
                                        <ext:ComboBox ForceSelection="true" DisplayField="Name" QueryMode="Local" ValueField="BranchId"
                                            StoreID="storeBranch" ID="ComboBox3" runat="server">
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                            </Triggers>
                                            <Listeners>
                                                <Select Handler="this.getTrigger(0).show();" />
                                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column11" runat="server" Text="Include Branch Head Also" Sortable="false"
                                    MenuDisabled="true" Width="170" DataIndex="ApplyToBranchHeadAlso" Border="false">
                                    <Renderer Handler="if(value=='true') return 'Yes';" />
                                    <Editor>
                                        <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="ComboBox9" runat="server">
                                            <Items>
                                                <ext:ListItem Value="true" Text="Yes" />
                                            </Items>
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                            </Triggers>
                                            <Listeners>
                                                <Select Handler="this.getTrigger(0).show();" />
                                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                    Width="30" Align="Center">
                                    <Commands>
                                        <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                    </Commands>
                                    <Listeners>
                                        <Command Fn="RemoveItemLine">
                                        </Command>
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <ext:Button ID="btnAddNewLineToMain" StyleSpec="margin-top:10px;" runat="server"
                        Icon="Add" Height="26" Text="Add New Line" Cls="btn btn-primary btn-sect btn-sm">
                        <Listeners>
                            <Click Handler="addNewRow(#{gridAllowances});" />
                        </Listeners>
                    </ext:Button>
                </div>
            </div>
            <div>
                <div>
                    <!-- panel-btns -->
                    <h4 class="panel-title sectionHeading" style="width: 1122px; margin-left: 0px; margin-bottom: -20px;">
                        <i></i>Approval List</h4>
                </div>
                <!-- panel-heading -->
                <div class="panel-body" style="padding-left: 0px">
                    <ext:GridPanel Title="" ID="gridApprovalList" runat="server">
                        <Store>
                            <ext:Store ID="store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model6" Name="SettingModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="AuthorityType" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                            <ext:ModelField Name="DesignationId" Type="String" />
                                            <ext:ModelField Name="AuthorityTypeDisplayName" Type="String" />
                                            <ext:ModelField Name="ApplyToBranchId" Type="String" />
                                            <ext:ModelField Name="ApplyToBranchHeadAlso" Type="String" NullConvert="false" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing2" ClicksToEdit="1" runat="server">
                            </ext:CellEditing>
                        </Plugins>
                        <Listeners>
                            <BeforeEdit Fn="beforeEditApproval" />
                        </Listeners>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn2" DataIndex="SequenceNo" runat="server"
                                    Width="35" />
                                <ext:Column ID="Column5" runat="server" Text="Authority" Sortable="false" MenuDisabled="true"
                                    Width="150" DataIndex="AuthorityType" Border="false">
                                    <Renderer Fn="AuthorityRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="AuthorityTypeName" QueryMode="Local" ForceSelection="true"
                                            ValueField="AuthorityType" StoreID="storeAuthorityType" ID="ComboBox5" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column7" runat="server" Text="Authority Display Name" Sortable="false"
                                    MenuDisabled="true" Width="170" DataIndex="AuthorityTypeDisplayName" Border="false">
                                    <Editor>
                                        <ext:TextField ID="TextField2" runat="server" />
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column8" ToolTip="Select Designation for Specific Designation Authority"
                                    runat="server" Text="Designation" Sortable="false" MenuDisabled="true" Width="150"
                                    DataIndex="DesignationId" Border="false">
                                    <Renderer Fn="DesignationRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="Name" QueryMode="Local" ValueField="DesignationId" StoreID="storeDesignation"
                                            ID="ComboBox6" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column9" ToolTip="Select Employee for Specific Employee Authority"
                                    runat="server" Text="Employee" Sortable="false" MenuDisabled="true" Width="180"
                                    DataIndex="EmployeeId" Border="false">
                                    <Renderer Fn="EmpRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="Name" ForceSelection="true" QueryMode="Local" ValueField="EmployeeId"
                                            StoreID="storeEmployee" ID="ComboBox7" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column10" ToolTip="Apply To This Branch Employee Only" runat="server"
                                    Text="Apply To This Branch Only" Sortable="false" MenuDisabled="true" Width="200"
                                    DataIndex="ApplyToBranchId" Border="false">
                                    <Renderer Fn="BranchRenderer" />
                                    <Editor>
                                        <ext:ComboBox ForceSelection="true" DisplayField="Name" QueryMode="Local" ValueField="BranchId"
                                            StoreID="storeBranch" ID="ComboBox8" runat="server">
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                            </Triggers>
                                            <Listeners>
                                                <Select Handler="this.getTrigger(0).show();" />
                                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column12" runat="server" Text="Include Branch Head Also" Sortable="false"
                                    MenuDisabled="true" Width="170" DataIndex="ApplyToBranchHeadAlso" Border="false">
                                    <Renderer Handler="if(value=='true') return 'Yes';" />
                                    <Editor>
                                        <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="ComboBox10" runat="server">
                                            <Items>
                                                <ext:ListItem Value="true" Text="Yes" />
                                            </Items>
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                            </Triggers>
                                            <Listeners>
                                                <Select Handler="this.getTrigger(0).show();" />
                                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Sortable="false" MenuDisabled="true"
                                    Width="30" Align="Center">
                                    <Commands>
                                        <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                    </Commands>
                                    <Listeners>
                                        <Command Fn="RemoveItemLine">
                                        </Command>
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <ext:Button ID="Button2" StyleSpec="margin-top:10px;" runat="server" Icon="Add" Height="26"
                        Text="Add New Line" Cls="btn btn-primary btn-sm btn-sect">
                        <Listeners>
                            <Click Handler="addNewRow(#{gridApprovalList});" />
                        </Listeners>
                    </ext:Button>
                </div>
            </div>
            <div>
                <div>
                    <!-- panel-btns -->
                    <h4 class="panel-title sectionHeading" style="width: 1122px; margin-left: 0px; margin-bottom: -20px;">
                        <i></i>Leave Approval Notification</h4>
                </div>
                <!-- panel-heading -->
                <div class="panel-body" style="padding-left: 0px">
                    <ext:GridPanel Title="" ID="gridLeavePredNotif" runat="server">
                        <Store>
                            <ext:Store ID="store2" runat="server">
                                <Model>
                                    <ext:Model ID="leavePredNotifModel" Name="SettingModel1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="AuthorityType" Type="String" />
                                            <ext:ModelField Name="BranchId" Type="String" />
                                            <ext:ModelField Name="LevelGroupId" Type="String" />
                                            <ext:ModelField Name="EmployeeId1" Type="String" />
                                            <ext:ModelField Name="EmployeeId2" Type="String" />
                                            <ext:ModelField Name="LeaveTypeId" Type="String" />
                                            <ext:ModelField Name="Emails" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing3" ClicksToEdit="1" runat="server">
                            </ext:CellEditing>
                        </Plugins>
                        <Listeners>
                            <BeforeEdit Fn="beforeEditLeaveNotify" />
                        </Listeners>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn3" DataIndex="SequenceNo" runat="server"
                                    Width="35" />
                                <ext:Column ID="colLeaveNotifRule" runat="server" Text="Leave Notification Rule"
                                    Sortable="false" MenuDisabled="true" Width="150" DataIndex="AuthorityType" Border="false">
                                    <Renderer Fn="LeaveNotifTypeRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="AuthorityTypeName" QueryMode="Local" ForceSelection="true"
                                            ValueField="AuthorityType" StoreID="storeLeaveNotifyType" ID="cmbTypeLeaveNotif"
                                            runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="colSelectBranch" ToolTip="Select for 'Branch Employee Approval'"
                                    runat="server" Text="Select Branch" Sortable="false" MenuDisabled="true" Width="150"
                                    DataIndex="BranchId" Border="false">
                                    <Renderer Fn="LeaveNotifBranchRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="Name" QueryMode="Local" ForceSelection="true" ValueField="BranchId"
                                            StoreID="storeLeaveNotifBranch" ID="cmbBranchLeaveNotify" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column13" runat="server" Text="Select Group" Sortable="false" MenuDisabled="true"
                                    Width="120" DataIndex="LevelGroupId" Border="false">
                                    <Renderer Fn="GroupRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="Name" QueryMode="Local" ForceSelection="true" ValueField="LevelGroupId"
                                            StoreID="StoreGroup" ID="ComboBox11" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="colOfficer1" runat="server" Text="Employee 1" Sortable="false" MenuDisabled="true"
                                    Width="150" DataIndex="EmployeeId1" Border="false">
                                    <Renderer Fn="EmpRenderer" />
                                    <Editor>
                                        <ext:ComboBox ForceSelection="true" DisplayField="Name" QueryMode="Local" ValueField="EmployeeId"
                                            StoreID="storeEmployee" ID="cmbEmployee1LeaveNotify" runat="server">
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                            </Triggers>
                                            <Listeners>
                                                <Select Handler="this.getTrigger(0).show();" />
                                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="colOfficer2" runat="server" Text="Employee 2" Sortable="false" MenuDisabled="true"
                                    Width="150" DataIndex="EmployeeId2" Border="false">
                                    <Renderer Fn="EmpRenderer" />
                                    <Editor>
                                        <ext:ComboBox ForceSelection="true" DisplayField="Name" QueryMode="Local" ValueField="EmployeeId"
                                            StoreID="storeEmployee" ID="cmbEmployee2LeaveNotify" runat="server">
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                            </Triggers>
                                            <Listeners>
                                                <Select Handler="this.getTrigger(0).show();" />
                                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                 <ext:Column ID="Column15" runat="server" Text="Leave" Sortable="false" MenuDisabled="true"
                                    Width="150" DataIndex="LeaveTypeId" Border="false">
                                    <Renderer Fn="LeaveRenderer" />
                                    <Editor>
                                        <ext:ComboBox ForceSelection="true" DisplayField="Title" QueryMode="Local" ValueField="LeaveTypeId"
                                            StoreID="storeLeaves" ID="ComboBox12" runat="server">
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                            </Triggers>
                                            <Listeners>
                                                <Select Handler="this.getTrigger(0).show();" />
                                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column14" ToolTip="Separate with comma for multiple" runat="server"
                                    Text="Emails" Sortable="false" MenuDisabled="true" Width="250" DataIndex="Emails"
                                    Border="false">
                                    <Editor>
                                        <ext:TextField runat="server" />
                                    </Editor>
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn3" runat="server" Sortable="false" MenuDisabled="true"
                                    Width="30" Align="Center">
                                    <Commands>
                                        <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                    </Commands>
                                    <Listeners>
                                        <Command Fn="RemoveItemLineLeave">
                                        </Command>
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <ext:Button ID="Button4" StyleSpec="margin-top:10px;" runat="server" Icon="Add" Height="26"
                        Text="Add New Line" Cls="btn btn-primary btn-sm btn-sect">
                        <Listeners>
                            <Click Handler="addNewRow1(#{gridLeavePredNotif});" />
                        </Listeners>
                    </ext:Button>
                </div>
            </div>
            <div class="alert alert-info">
                <div style="margin-top: 0px; width: 200px;">
                    <ext:Button ID="Button1" runat="server" Height="30" Width="120" StyleSpec="margin-left:10px;"
                        Text="Save Changes" Cls="btn btn-success btn-sect">
                        <DirectEvents>
                            <Click OnEvent="ButtonNext_Click">
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="GridValues" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                        Mode="Raw" />
                                    <ext:Parameter Name="GridValuesApproval" Value="Ext.encode(#{gridApprovalList}.getRowsValues({selectedOnly : false}))"
                                        Mode="Raw" />
                                    <ext:Parameter Name="GridLeavePredNot" Value="Ext.encode(#{gridLeavePredNotif}.getRowsValues({selectedOnly : false}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
