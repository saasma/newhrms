﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;
using System.Collections;

namespace Web.NewHR
{
    public partial class EmpEducationList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "EducationImport", "../ExcelWindow/EmpEducationImport.aspx", 450, 500);
        }

        private void Initialise()
        {
            storeBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeBranch.DataBind();

            storeLevel.DataSource = NewPayrollManager.GetAllParentLevelList().OrderBy(x => x.Name).ToList();
            storeLevel.DataBind();

            cmbEducationLevel.Store[0].DataSource = CommonManager.GetEducationLevelList();
            cmbEducationLevel.Store[0].DataBind();

            cmbFaculty.Store[0].DataSource = CommonManager.GetEducationFacultyList();
            cmbFaculty.Store[0].DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int total = 0, pageSize = 0, employeeId = -1, branchId = -1, levelId = -1, educationLevelId = -1, facultyId = -1;
            string employeeName = cmbSearch.Text;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            if (cmbEducationLevel.SelectedItem != null && cmbEducationLevel.SelectedItem.Value != null)
                educationLevelId = int.Parse(cmbEducationLevel.SelectedItem.Value);

            if (cmbFaculty.SelectedItem != null && cmbFaculty.SelectedItem.Value != null)
                facultyId = int.Parse(cmbFaculty.SelectedItem.Value);

            if (cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);


            List<GetEmployeeEducationListResult> list = NewHRManager.GetEmployeeEducations(e.Page - 1, e.Limit, employeeId, employeeName, branchId, levelId, educationLevelId, facultyId);

            //-- start filtering -----------------------------------------------------------
            FilterHeaderConditions fhc = new FilterHeaderConditions(e.Parameters["filterheader"]);

            foreach (FilterHeaderCondition condition in fhc.Conditions)
            {
                string dataIndex = condition.DataIndex;
                FilterType type = condition.Type;
                string op = condition.Operator;
                object value = null;

                switch (condition.Type)
                {
                    case FilterType.Boolean:
                        value = condition.Value<bool>();
                        break;

                    case FilterType.Date:
                        switch (condition.Operator)
                        {
                            case "=":
                                value = condition.Value<DateTime>();
                                break;

                            case "compare":
                                value = FilterHeaderComparator<DateTime>.Parse(condition.JsonValue);
                                break;
                        }
                        break;
                    case FilterType.String:
                        value = condition.Value<string>();
                        break;
                    case FilterType.Numeric:
                        value = condition.Value<int>();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                list.RemoveAll(item =>
                {
                    object oValue = item.GetType().GetProperty(dataIndex).GetValue(item, null);
                    string matchValue = null;
                    string itemValue = null;

                    if (type == FilterType.String)
                    {
                        matchValue = (string)value;

                        if (dataIndex == "EmployeeId")
                            itemValue = oValue.ToString();
                        else
                            itemValue = oValue as string;

                        matchValue = matchValue.ToLower();
                        itemValue = (itemValue == null ? "" : itemValue.ToLower());
                    }

                    switch (op)
                    {
                        case "=":
                            return oValue == null || !oValue.Equals(value);
                        case "compare":
                            return !((IEquatable<IComparable>)value).Equals((IComparable)oValue);
                        case "+":
                            return itemValue == null || !itemValue.Contains(matchValue); //return itemValue == null || !itemValue.StartsWith(matchValue);
                        case "-":
                            return itemValue == null || !itemValue.EndsWith(matchValue);
                        case "!":
                            return itemValue == null || itemValue.IndexOf(matchValue) >= 0;
                        case "*":
                            return itemValue == null || itemValue.IndexOf(matchValue) < 0;
                        default:
                            throw new Exception("Not supported operator");
                    }
                });
            }
            //-- end filtering ------------------------------------------------------------

            //-- start sorting ------------------------------------------------------------

            if (e.Sort.Length > 0)
            {
                list.Sort(delegate (GetEmployeeEducationListResult obj1, GetEmployeeEducationListResult obj2)
                {
                    object a;
                    object b;

                    int direction = e.Sort[0].Direction == Ext.Net.SortDirection.DESC ? -1 : 1;

                    a = obj1.GetType().GetProperty(e.Sort[0].Property).GetValue(obj1, null);
                    b = obj2.GetType().GetProperty(e.Sort[0].Property).GetValue(obj2, null);
                    return CaseInsensitiveComparer.Default.Compare(a, b) * direction;
                });
            }

            //Session["ExportEducationList"] = list;

            storeEducation.DataSource = list;

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            e.Total = total;
        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            ((TextField)defaultField).Icon = Icon.Magnifier;

            return defaultField;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            //if (Session["ExportEducationList"] == null)
            //{
            //    NewMessage.ShowWarningMessage("Please reload the data for export.");
            //    return;
            //}

            //List<GetEmployeeEducationListResult> list = (List<GetEmployeeEducationListResult>)Session["ExportEducationList"];

            //Session["ExportEducationList"] = null;


            int pageSize = 0, employeeId = -1, branchId = -1, levelId = -1, educationLevelId = -1, facultyId = -1;
            string employeeName = cmbSearch.Text;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            if (cmbEducationLevel.SelectedItem != null && cmbEducationLevel.SelectedItem.Value != null)
                educationLevelId = int.Parse(cmbEducationLevel.SelectedItem.Value);

            if (cmbFaculty.SelectedItem != null && cmbFaculty.SelectedItem.Value != null)
                facultyId = int.Parse(cmbFaculty.SelectedItem.Value);

            if (cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);


            List<GetEmployeeEducationListResult> list = NewHRManager.GetEmployeeEducations(0, 999999, employeeId, employeeName, branchId, levelId, educationLevelId, facultyId);



            string template = ("~/App_Data/ExcelTemplate/EmpEducationImport.xlsx");

            List<FixedValueEducationLevel> listEducationLevel = CommonManager.GetEducationLevelList().ToList();
            List<FixedValueEducationFaculty> listEducationFaculty = CommonManager.GetEducationFacultyList().ToList();
            List<FixedValueDivision> listDivision = ListManager.GetAllDivisions().ToList();
            List<CountryList> listCountries = new CommonManager().GetAllCountries().ToList();

            ExcelGenerator.ExportEmployeeEducation(template, list, listEducationLevel, listEducationFaculty, listDivision, listCountries);


            //List<string> hiddenColumnList = new List<string>();
            //hiddenColumnList = new List<string> { "TotalRows", "RowNumber", "EductionId", "Place" };

            //Bll.ExcelHelper.ExportToExcel("Employee Education", list,
            //       hiddenColumnList,
            //   new List<String>() { },
            //   new Dictionary<string, string>() { {"EmployeeId","EIN"}, {"Name","Employee Name"}, {"Course","Course Name"}, {"Percentage","Percentage/Grade" },
            //        {"Institution","Institute"}, {"MajorSubjects","Major Subjects"}, { "PassedYear", "Passed Year" } },
            //   new List<string>() { }
            //   , new List<string> { }
            //   , new List<string> { }
            //   , new Dictionary<string, string>() { }
            //   , new List<string> { "EmployeeId", "Name", "Course", "Level", "Faculty", "Institution", "University", "Country", "Percentage", "Division", "PassedYear", "MajorSubjects" });



        }

    }
}