﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.CP.Report.Templates.HR;
using DevExpress.XtraReports.UI;
using BLL.BO;

namespace Web.NewHr
{
    public partial class LeaveTakenDetails : BasePage
    {
     

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
               Initialize();

                              

            }
        }

        private void Initialize()
        {

            int empId = -1;
            int yearId = -1;


            string emp = Request.QueryString["id"];
            string year = Request.QueryString["year"];
            string yearid = Request.QueryString["yearid"];

            if (int.TryParse(emp, out empId))
            {
                //GetLeaveApprovalApplyToCCForEmployeeResult lae = LeaveAttendanceManager.GetLeaveApprovalApplyToAndCCForEmployee(empId);

                //if (lae == null)
                //    return;

                //if (empId == SessionManager.CurrentLoggedInEmployeeId ||
                //    lae.ApplyTo == SessionManager.CurrentLoggedInEmployeeId ||
                //    lae.CC1 == SessionManager.CurrentLoggedInEmployeeId ||
                //    lae.CC2 == SessionManager.CurrentLoggedInEmployeeId ||
                //    lae.CC3 == SessionManager.CurrentLoggedInEmployeeId ||
                //    lae.CC4 == SessionManager.CurrentLoggedInEmployeeId ||
                //    lae.CC5 == SessionManager.CurrentLoggedInEmployeeId)
                {

                    int yearidValue = 0;
                    int.TryParse(yearid, out yearidValue);


                    if (SessionManager.CurrentCompany.LeaveStartMonth == (int)EnglishMonthEnum.April && SessionManager.CurrentCompany.IsEnglishDate)
                    {
                        DateTime start = DateTime.Now, end = DateTime.Now;

                        int yearValue = int.Parse(yearid);

                        //if (IsEnglish)
                        //{
                            start = new DateTime(yearValue, (int)EnglishMonthEnum.April, 1);
                            end = new DateTime(yearValue + 1, ((int)EnglishMonthEnum.April-1), DateTime.DaysInMonth(yearValue + 1, ((int)EnglishMonthEnum.April - 1)));
                        //}
                        //else
                        //{
                        //    start = new CustomDate(1, 1, yearValue, false).EnglishDate;
                        //    end = new CustomDate(DateHelper.GetTotalDaysInTheMonth(yearValue, 12, false), 12, yearValue, false).EnglishDate;
                        //}

                        //FinancialDate yearE = CommonManager.GetFiancialDateById(yearId);
                        //if (year != null)
                        {

                            lbl.InnerHtml += " for " + EmployeeManager.GetEmployeeById(empId).Name +
                                " from " + start.ToShortDateString() +
                                " to " + end.ToShortDateString();

                            List<Report_HRLeaveTakenNewReportResult> list =
                                BLL.BaseBiz.PayrollDataContext.Report_HRLeaveTakenNewReport(start,
                                end, empId).ToList();

                            gridBranchTransfer.Store[0].DataSource = list;
                            gridBranchTransfer.Store[0].DataBind();
                        }
                    }
                        // for fiscal yearly leave period case
                    else if (yearidValue != 0)
                    {
                        FinancialDate yearE = CommonManager.GetFiancialDateById(yearId);
                        if (year != null)
                        {

                            lbl.InnerHtml += " for " + EmployeeManager.GetEmployeeById(empId).Name +
                                " from " + yearE.StartingDateEng.Value.ToShortDateString() +
                                " to " + yearE.EndingDateEng.Value.ToShortDateString();

                            List<Report_HRLeaveTakenNewReportResult> list =
                                BLL.BaseBiz.PayrollDataContext.Report_HRLeaveTakenNewReport(yearE.StartingDateEng.Value,
                                yearE.EndingDateEng.Value, empId).ToList();

                            gridBranchTransfer.Store[0].DataSource = list;
                            gridBranchTransfer.Store[0].DataBind();
                        }

                    }
                    else
                    // for baisakh leave start month case
                    {

                        DateTime start = DateTime.Now, end = DateTime.Now;

                        int yearValue = int.Parse(year);

                        if(IsEnglish)
                        {
                            start = new DateTime(yearValue, 1, 1);
                            end = new DateTime(yearValue, 12, DateTime.DaysInMonth(yearValue, 12));
                        }
                        else
                        {
                            start = new CustomDate(1, 1, yearValue, false).EnglishDate;
                            end = new CustomDate(DateHelper.GetTotalDaysInTheMonth(yearValue, 12, false), 12, yearValue, false).EnglishDate;
                        }

                        FinancialDate yearE = CommonManager.GetFiancialDateById(yearId);
                        if (year != null)
                        {

                            lbl.InnerHtml += " for " + EmployeeManager.GetEmployeeById(empId).Name +
                                " from " + start.ToShortDateString() +
                                " to " + end.ToShortDateString();

                            List<Report_HRLeaveTakenNewReportResult> list =
                                BLL.BaseBiz.PayrollDataContext.Report_HRLeaveTakenNewReport(start,
                                end, empId).ToList();

                            gridBranchTransfer.Store[0].DataSource = list;
                            gridBranchTransfer.Store[0].DataBind();
                        }
                    }
                }
            }

        }

        private void Clear()
        {
            
        }

       
    
        public void btnExport_Click(object sender, EventArgs e)
        {

        }


    }
}