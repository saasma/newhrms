﻿<%@ Page Title="Employee Marital and Tax Status" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmpMaritalAndTaxStatusDetails.aspx.cs" Inherits="Web.NewHR.EmpMaritalAndTaxStatusDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .disabledIncomeDeduction, .disabledIncomeDeduction a, .disabledIncomeDeduction td
    {
        background-color: #ddd !important;
    }
        
    .unappliedIncomeDeduction, .unappliedIncomeDeduction a, .unappliedIncomeDeduction td
    {
        color: #E6E6E6;
    }

</style>

<script type="text/javascript">
    var CommandHandler = function(value, command,record,p2) {
        var employeeId = record.data.EmployeeId;
        var INo = record.data.IDCardNo;
        var branchName = record.data.Branch;
        var departmentName = record.data.Department;

        <%= hdnEmployeeId.ClientID %>.setValue(employeeId);
        <%= hdnINo.ClientID %>.setValue(INo);
        <%= hdnBranch.ClientID %>.setValue(branchName);
        <%= hdnDepartment.ClientID %>.setValue(departmentName);
        <%= btnEdit.ClientID %>.fireEvent('click');        
        
    };

    function refreshGrid() {
        <%= btnLoad.ClientID %>.fireEvent('click');
    }

     var getRowClass = function (record) {
    
            var clsName = record.data.RowBackColor;
            if(clsName != '')
                return clsName;

            return "";

        };

    
    function refreshWindow()
    {
        <%= btnLoad.ClientID %>.fireEvent('click');
    }

    function cmbMartialStatusEdit_Change()
    {
        var maritalStatus = <%= cmbMartialStatusEdit.ClientID %>.getValue();

        if(maritalStatus != null && maritalStatus.toLowerCase() == 'married')
            <%= chkHasCoupleTaxStatus.ClientID %>.enable();
        else
        {
            <%= chkHasCoupleTaxStatus.ClientID %>.setValue(false);
            <%= chkHasCoupleTaxStatus.ClientID %>.disable();

        }
    }

    
</script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>

    <ext:Button ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Hidden ID="hdnEmployeeId" runat="server" /> 
    
    <ext:Hidden ID="hdnBranch" runat="server" /> 
    <ext:Hidden ID="hdnDepartment" runat="server" /> 
    <ext:Hidden ID="hdnINo" runat="server" /> 

    
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Marital and Tax Status
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbBranch" Width="160" runat="server" ValueField="BranchId" DisplayField="BranchName"
                            FieldLabel="Branch" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" />
                                                <ext:ModelField Name="BranchName" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                </td>
                <td>
                     <ext:ComboBox ID="cmbDepartment" Width="160" runat="server" ValueField="DepartmentId" MarginSpec="0 0 0 10"
                            DisplayField="Name" FieldLabel="Department" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                </td>
                <td >
                    <ext:ComboBox ID="cmbSubDepartment" Width="160" runat="server" ValueField="SubDepartmentId" MarginSpec="0 0 0 10"
                        DisplayField="Name" FieldLabel="Sub Department" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store11" runat="server">
                                <Model>
                                    <ext:Model ID="Model11" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="SubDepartmentId" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                </td>
                <td>
                     <ext:ComboBox FieldLabel="Designation" ID="cmbDesignation"  MarginSpec="0 0 0 10"
                        Width="160"  runat="server" ValueField="DesignationId" DisplayField="Name"
                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store4" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" IDProperty="DesignationId" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="DesignationId" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:ComboBox ID="cmbLevel" runat="server" FieldLabel="Level"  MarginSpec="0 0 0 10"
                        Width="160"  DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                        LabelSeparator="" LabelAlign="Top">
                        <Store>
                            <ext:Store ID="storeLevel" runat="server">
                                <Model>
                                    <ext:Model ID="modelLevel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="LevelId" Type="Int" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:MultiCombo Width="160" LabelSeparator="" SelectionMode="All" LabelAlign="Top"  MarginSpec="0 0 0 10"
                        ID="cmbJobStatus" DisplayField="Value" ValueField="Key"
                        runat="server" FieldLabel="Job Status">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model8" IDProperty="Key" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Key" />
                                            <ext:ModelField Name="Value" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                        </Listeners>
                    </ext:MultiCombo>


                    

                </td>
            </tr>
            <tr>
                
                <td colspan="2">
                    <ext:Store ID="storeEmployee" runat="server">
                        <Model>
                            <ext:Model ID="Model7" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="NameEIN" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:TagField ID="tfEmployee" MarginSpec="5 0 0 0" QueryMode="Local" MinChars="2" StoreID="storeEmployee" DisplayField="NameEIN" ValueField="EmployeeId" runat="server" Width="330" TypeAhead="true" FieldLabel="Employee" LabelSeparator="" LabelAlign="Top">
                        <Listeners>
                            <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                queryEvent.query = new RegExp(q, 'ig');
                                queryEvent.query.length = q.length;" />
                        </Listeners>
                    </ext:TagField>

                </td>
                
               
                <td>
                    <ext:ComboBox ID="cmbMaritalStatus" ForceSelection="true"  MarginSpec="0 0 0 10"
                            QueryMode="Local" FieldLabel="Marital Status" runat="server" LabelAlign="Top"
                            DisplayField="Key" ValueField="Value" Width="160" LabelSeparator="">
                            <Store>
                                <ext:Store ID="storeMarritalStatus" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Key" />
                                        <ext:ModelField Name="Value" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                </td>
                <td>
                     <ext:ComboBox ID="cmbTaxStatus" Width="160" runat="server" ValueField="Value"  MarginSpec="5 0 0 10"
                            DisplayField="Text" FieldLabel="Tax Status" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="All" Value="-1" />
                                <ext:ListItem Text="Single" Value="1" />
                                <ext:ListItem Text="Couple" Value="2" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Text="All" Value="-1" />
                            </SelectedItems>
                        </ext:ComboBox>
                </td>
                 <td>
                    <ext:ComboBox ID="cmbUnit" Width="160" runat="server" ValueField="UnitId" MarginSpec="0 0 0 10" Hidden="true"
                            DisplayField="Name" FieldLabel="Unit" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                                              
                            <Store>
                                <ext:Store ID="Store15" runat="server">
                                    <Model>
                                        <ext:Model ID="Model16" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="UnitId" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                </td>
                <td style="padding-top: 16px;" >
                    <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Load" Width="100" MarginSpec="5 0 0 10">
                        <DirectEvents>
                            <Click OnEvent="btnLoad_Click"  >
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'LoadData'; if(CheckValidation()) return ''; else return false;">
                            </Click>
                        </Listeners>
                    </ext:Button>
               
               </td>
               <td style="padding-top: 16px;" >
                    <ext:Button ID="Button1" runat="server" Cls="btn btn-save" Text="Import" Width="100" MarginSpec="5 0 0 10" OnClientClick="importPopup">
                    </ext:Button>
                </td>

                
            </tr>
        </table>
        <br />        

        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridMaritalTaxStatus" runat="server" Cls="itemgrid"
            Scroll="None" Width="980">
            <Store>
                <ext:Store ID="storeMaritalTaxStatus" runat="server" >
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="EmployeeId">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="IDCardNo" Type="String" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="Branch" Type="String" />
                                <ext:ModelField Name="Department" Type="String" />
                                <ext:ModelField Name="MaritalStatus" Type="String" />
                                <ext:ModelField Name="HasCoupleTaxStatus" Type="Boolean" />
                                <ext:ModelField Name="HasCoupleTaxStatusValue" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>  
                    <ext:Column ID="colEmployeeId" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                        Width="60" Align="Left" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="INo"
                        Width="60" Align="Left" DataIndex="IDCardNo">
                    </ext:Column>
                    <ext:Column ID="colName" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                        Width="160" Align="Left" DataIndex="Name">
                    </ext:Column>
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                        Width="160" Align="Left" DataIndex="Branch">
                    </ext:Column>
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                        Width="160" Align="Left" DataIndex="Department">
                    </ext:Column>
                    <ext:Column ID="colIncomeType" Sortable="false" MenuDisabled="true" runat="server" Text="Marital Status"
                        Width="100" Align="Left" DataIndex="MaritalStatus">
                    </ext:Column>
                    <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Has Couple Tax Status"
                        Width="200" Align="Left" DataIndex="HasCoupleTaxStatusValue">
                    </ext:Column>
                     <ext:ImageCommandColumn ID="CommandColumnChange" runat="server" Width="70" Text=""
                            MenuDisabled="true" Sortable="false" Align="Center">
                            <Commands>
                                <ext:ImageCommand CommandName="Edit" Text="Edit">
                                    <ToolTip Text="Edit" />
                                </ext:ImageCommand>
                            </Commands>
                            <Listeners>
                                <Command Fn="CommandHandler">
                                </Command>
                            </Listeners>
                        </ext:ImageCommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                    <ext:CellSelectionModel ID="CellSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <%--<GetRowClass Fn="getRowClass" />--%>
                </ext:GridView>
            </View>
        </ext:GridPanel>
        <br />

    </div>




    <ext:Window ID="WDetails" runat="server" Title="Tax and Marital Status" Icon="Application" 
        Resizable="false" Width="500" Height="410" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table style="margin-left: 20px; margin-top: 20px;">
                <tr>
                    <td colspan="2">
                        <ext:TextField ID="txtName" ReadOnly="true" runat="server" LabelSeparator="" FieldLabel="Name" Text="" LabelAlign="Left" Width="400" LabelWidth="130">
                        </ext:TextField>
                    </td>
                    
                </tr>
                <tr>
                   <td colspan="2">
                        <ext:TextField ID="txtINo" ReadOnly="true" runat="server" LabelSeparator="" FieldLabel="I No" Text="" LabelAlign="Left" Width="400" LabelWidth="130" MarginSpec="15 0 0 0">
                        </ext:TextField>
                    </td>
                    
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextField ID="txtBranch" ReadOnly="true" runat="server" LabelSeparator="" FieldLabel="Branch" Text="" LabelAlign="Left" Width="400" LabelWidth="130" MarginSpec="15 0 0 0">
                        </ext:TextField>
                    </td>
                    
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextField ID="txtDepartment" ReadOnly="true" runat="server" LabelSeparator="" FieldLabel="Department" Text="" LabelAlign="Left" Width="400" LabelWidth="130" MarginSpec="15 0 0 0">
                        </ext:TextField>
                    </td>
                    
                </tr>
                <tr>
                   <td colspan="2">
                        <ext:ComboBox ID="cmbMartialStatusEdit" ForceSelection="true" MarginSpec="15 0 0 0"
                            QueryMode="Local" FieldLabel="Marital Status *" runat="server" LabelAlign="Left" LabelWidth="130"
                            DisplayField="Key" ValueField="Value" Width="300" LabelSeparator="">
                            <Store>
                                <ext:Store ID="store2" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Key" />
                                        <ext:ModelField Name="Value" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <Listeners>
                                <Change Fn="cmbMartialStatusEdit_Change" />
                            </Listeners>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="rfvMaritalStatus" runat="server" ValidationGroup="SaveUpdDetails"
                                        ControlToValidate="cmbMartialStatusEdit" ErrorMessage="Marital Status is required." />
                    </td>
                     
                </tr>

                <tr>
                    <td colspan="2">
                        
                        <ext:Checkbox ID="chkHasCoupleTaxStatus" runat="server" LabelSeparator="" BoxLabel="Has Couple Tax Status" MarginSpec="15 0 0 0"
                            BoxLabelAlign="Before" LabelWidth="130" Enabled="false">
                        </ext:Checkbox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Update" MarginSpec="20 0 0 0" >
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdDetails'; if(CheckValidation()) return ''; else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>

                    <td>
                        <ext:Button runat="server" ID="Button2" Cls="btn btn-primary" Text="<i></i>Cancel" MarginSpec="20 0 0 0" >
                            <Listeners>
                                <Click Handler="#{WDetails}.hide();" />
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>

            </table>
        </Content>
    </ext:Window>
    
    

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
