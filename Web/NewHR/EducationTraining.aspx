﻿<%@ Page Title="Education and training" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EducationTraining.aspx.cs" Inherits="Web.NewHR.EducationTraining1" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EducationCtl.ascx" TagName="EducationCtl"
    TagPrefix="ucEducation" %>
<%@ Register Src="~/NewHR/UserControls/TrainingCtl.ascx" TagName="TrainingCtl" TagPrefix="ucTraining" %>
<%@ Register Src="~/NewHR/UserControls/SeminarCtl.ascx" TagName="SeminarCtl" TagPrefix="ucSeminar" %>
<%@ Register Src="~/NewHR/UserControls/SkillSetsCtl.ascx" TagName="SkillSetsCtl"
    TagPrefix="ucSkillSets" %>
<%@ Register Src="~/NewHR/UserControls/PublicationCtl.ascx" TagName="PublicationCtl"
    TagPrefix="ucPublication" %>
<%@ Register Src="~/NewHR/UserControls/LanguageSetsCtl.ascx" TagName="LanguageSetsCtl"
    TagPrefix="ucLanguageSets" %>
<%@ Register Src="~/NewHR/UserControls/EduCtrlTest.ascx" TagName="ECtrl" TagPrefix="ucE" %>
<%@ Register Src="~/NewHR/UserControls/UTrainingCtrl.ascx" TagName="UCUTrainingCtrl"
    TagPrefix="uUTrainingCtrl" %>
<%@ Register Src="~/NewHR/UserControls/USeminarCtrl.ascx" TagName="ucUSeminarCtrl"
    TagPrefix="uUSeminarCtrl" %>
<%@ Register Src="~/NewHR/UserControls/USkillSetCtrl.ascx" TagName="UCUSkillSetCtrl"
    TagPrefix="uUSkillSetCtrl" %>
<%@ Register Src="~/NewHR/UserControls/ULanguageCtrl.ascx" TagName="UCULanguageCtrl"
    TagPrefix="uULanguageCtrl" %>
<%@ Register Src="~/NewHR/UserControls/UPublicationCtrl.ascx" TagName="UCUPublicationCtrl"
    TagPrefix="uUPublicationCtrl" %>
<%@ Register Src="~/NewHR/UserControls/ExtraActivitiesCtrl.ascx" TagName="ExtraActivity"
    TagPrefix="EA" %>

<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .userDetailsClass
        {
            background-color:#D8E7F3; 
            height:50px; text-align:center; 
            padding-top:10px; 
            margin-bottom:10px;
            margin-left:18px;
        }
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body" style="margin-left:145px;">
                <h4>
                    Education and Training
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            
            <%--<uc2:EmployeeWizard Id="EmployeeWizard1" runat="server" />--%>
            <table>
                <tr>
                    <td valign="top" style="border-right: 1px solid #e7e7e7;">
                        <div style="float: left; margin-right: 10px;">
                            <ucEW:EmpDetailsWizard Id="EmployeeWizard1" runat="server" />
                        </div>
                    </td>
                    <td valign="top"  style="width:100%;">
                        <div class="userDetailsClass">
                            <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                          </div>

                        <div id="divEducation">
                            <div>
                                <!-- panel-btns -->
                                <span style="margin-left: 20px;"><i>Note: Drag and drop to reorder the education list.</i></span>
                                <h4 class="sectionHeading" style="width: 1001px">
                                    <i></i>Education
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body" style="padding-bottom: 0px;">
                                <ucE:ECtrl ID="ECtrl" runat="server" />
                            </div>
                        </div>
                        <div id="divTraining">
                            <div>
                                <div>
                                    <!-- panel-btns -->
                                    <h4 class="sectionHeading" style="width: 1001px">
                                        <i></i>Training
                                    </h4>
                                </div>
                                <!-- panel-heading -->
                                <div class="panel-body">
                                    <uUTrainingCtrl:UCUTrainingCtrl Id="TrainingCtl1" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div id="divSeminar">
                            <div>
                                <div>
                                    <!-- panel-btns -->
                                    <h4 class="sectionHeading" style="width: 1001px">
                                        <i></i>Seminar
                                    </h4>
                                </div>
                                <!-- panel-heading -->
                                <div class="panel-body">
                                    <uUSeminarCtrl:ucUSeminarCtrl Id="SeminarCtl1" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div id="divSkill">
                            <div>
                                <div>
                                    <!-- panel-btns -->
                                    <h4 class="sectionHeading" style="width: 1001px">
                                        <i></i>Skill Sets
                                    </h4>
                                </div>
                                <!-- panel-heading -->
                                <div class="panel-body">
                                    <uUSkillSetCtrl:UCUSkillSetCtrl Id="SkillSetsCtl1" runat="server" />
                                    <%--<ucSkillSets:SkillSetsCtl Id="SkillSetsCtl1" runat="server" />--%>
                                </div>
                            </div>
                        </div>
                        <div id="divLanguageSets">
                            <div>
                                <div>
                                    <!-- panel-btns -->
                                    <h4 class="sectionHeading" style="width: 1001px">
                                        <i></i>Language
                                    </h4>
                                </div>
                                <!-- panel-heading -->
                                <div class="panel-body">
                                    <%--<ucLanguageSets:LanguageSetsCtl Id="LanguageSetsCtl1" runat="server" />--%>
                                    <uULanguageCtrl:UCULanguageCtrl Id="LanguageSetsCtl1" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div id="divPublication">
                            <div>
                                <div>
                                    <!-- panel-btns -->
                                    <h4 class="sectionHeading" style="width: 1001px">
                                        <i></i>Publication
                                    </h4>
                                </div>
                                <!-- panel-heading -->
                                <div class="panel-body">
                                    <%--<ucPublication:PublicationCtl Id="PublicationCtl1" runat="server" />--%>
                                    <uUPublicationCtrl:UCUPublicationCtrl Id="PublicationCtl1" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div id="divExtraActivity">
                            <div>
                                <!-- panel-btns -->
                                <h4 class="sectionHeading" style="width: 1001px">
                                    <i></i>Extra Activity
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body" style="padding-bottom: 0px;">
                                <EA:ExtraActivity Id="ExtAct" runat="server" />
                            </div>
                        </div>
                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
