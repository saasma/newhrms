﻿<%@ Page Title="Employee List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeCatalogueList.aspx.cs" Inherits="Web.NewHR.EmployeeCatalogueList" %>

<%@ Register Src="~/NewHR/UserControls/EmployeeListCtrl.ascx" TagName="EmpList" TagPrefix="ucEL" %>
<%@ Register Src="~/NewHR/UserControls/EmpCatalogueCtrl.ascx" TagName="EmpCal" TagPrefix="ucEC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchListParent() {
     
        var val = <%=hdnSwitch.ClientID %>.getValue();
        if(val == 1)
        {
            <%=btnEmpCatDetl.ClientID %>.fireEvent('click');            
        }
        else
        {
            searchListEL();
        }
    }

    Ext.onReady(
        function()
        {
             

              Ext.defer(function(){
                       
                        document.getElementById('divEmpCat').style.display = 'none';
                        
                        searchListEL();
                    }, 50);   


        }
    );

    function EmpListCall()
    {
        <%=hdnSwitch.ClientID %>.setValue('2');
        document.getElementById('divEmpList').style.display = 'block';
        document.getElementById('divEmpCat').style.display = 'none';
        searchListEL();
    }

    function EmpCatFn()
    {
        <%=hdnSwitch.ClientID %>.setValue('1');
        document.getElementById('divEmpList').style.display = 'none';
        document.getElementById('divEmpCat').style.display = 'block';

        <%=btnEmpCatDetl.ClientID %>.fireEvent('click');
    }

    

    </script>
    <style type="text/css">
        .contentpanel
        {
            padding: 8px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnSwitch" runat="server" Text="2" />
    <ext:LinkButton ID="btnEmpCatDetl" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEmpCatDetl_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <div class="alert alert-info" style="margin-top: 10px; padding: 2px;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                        LabelAlign="Top" ForceSelection="true" QueryMode="Local" ID="cmbBranch" DisplayField="Name"
                                        ValueField="BranchId" runat="server" FieldLabel="Branch">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store1" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" IDProperty="BranchId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Listeners>
                                            <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                      queryEvent.query = new RegExp('\\b' + q, 'i');
                                      queryEvent.query.length = q.length;" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                        Width="140" ForceSelection="true" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId"
                                        DisplayField="Name" LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store2" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model2" IDProperty="DepartmentId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Listeners>
                                            <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                      queryEvent.query = new RegExp('\\b' + q, 'i');
                                      queryEvent.query.length = q.length;" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                                        Width="140" ForceSelection="true" MarginSpec="0 5 0 5" DisplayField="GroupLevel"
                                        ValueField="LevelId" QueryMode="Local" LabelSeparator="" LabelAlign="Top">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="storeLevel" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelLevel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="LevelId" Type="Int" />
                                                            <ext:ModelField Name="GroupLevel" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Listeners>
                                            <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                      queryEvent.query = new RegExp('\\b' + q, 'i');
                                      queryEvent.query.length = q.length;" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                        Width="140" ForceSelection="true" MarginSpec="0 5 0 5" runat="server" ValueField="DesignationId"
                                        DisplayField="LevelAndDesignation" LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store4" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model5" IDProperty="DesignationId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DesignationId" />
                                                            <ext:ModelField Name="LevelAndDesignation" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Listeners>
                                            <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                      queryEvent.query = new RegExp('\\b' + q, 'i');
                                      queryEvent.query.length = q.length;" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ForceSelection="true" MatchFieldWidth="false" FieldLabel="Status" ID="cmbEmpStatus"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="Key" DisplayField="Value"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="storeEmpStatus" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model3" IDProperty="Key" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Key" />
                                                            <ext:ModelField Name="Value" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:TextField ID="txtINO" EmptyText="Search" Width="90" MarginSpec="0 5 0 5" FieldLabel="I No"
                                        runat="server" LabelAlign="Top" LabelSeparator="">
                                    </ext:TextField>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbEmployee" EmptyText="Search" runat="server" Width="180" FieldLabel="Employee"
                                        LabelAlign="Top" HideBaseTrigger="true" LabelSeparator="" QueryMode="Local" DisplayField="Name"
                                        ValueField="EmployeeId">
                                        <Store>
                                            <ext:Store ID="storeEmployeeList" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchListParent">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <div style="float: right;">
                <table>
                    <tr>
                        <td style="width: 40px;">
                            <ext:Button ID="btnEmployeeList" runat="server" Text="<i class='glyphicon glyphicon-list'></i>"
                                ToolTip="Employee List">
                                <Listeners>
                                    <Click Fn="EmpListCall">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>
                            <ext:Button ID="btnEmpCatl" runat="server" Text="<i class='glyphicon glyphicon-th'></i>"
                                ToolTip="Employee Catalogue">
                                <Listeners>
                                    <Click Fn="EmpCatFn">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div id="divEmpList">
                <ucEL:EmpList Id="ucEmpList" runat="server" />
            </div>
            <div id="divEmpCat">
                <ucEC:EmpCal Id="ucEmpCat" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
