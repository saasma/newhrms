﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.CP
{
    public partial class LeaveRequestPredefinedList : BasePage
    {

        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }     

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        protected void cmbType_Change(object sender, DirectEventArgs e)
        {
            int selectedItem = int.Parse(cmbType.SelectedItem.Value);
            LoadLevels((PreDefindFlowType)selectedItem);
        }


        public void Initialise()
        {

            storeLeaves.DataSource = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
            storeLeaves.DataBind();

            storeAuthorityType.DataSource = TravelAllowanceManager.getLeaveAuthorityTypeListForCombo();
            storeAuthorityType.DataBind();

            storeEmployee.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name).ToList();
            storeEmployee.DataBind();

            storeDesignation.DataSource = new CommonManager().GetAllDesignations();
            storeDesignation.DataBind();

            storeBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Name).ToList();
            storeBranch.DataBind();

            
            storeLeaveNotifyType.DataSource = TravelAllowanceManager.GetLeavePredefinedAuthorityTypeList();
            storeLeaveNotifyType.DataBind();


            StoreGroup.DataSource = NewPayrollManager.GetLevelGroup();
            StoreGroup.DataBind();


            storeLeaveNotifBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeLeaveNotifBranch.DataBind();

            BindLeaveNotification();
        }

        private void BindLeaveNotification()
        {
            gridLeavePredNotif.Store[0].DataSource = TravelAllowanceManager.GetLeavePredefinedNotificationList();
            gridLeavePredNotif.Store[0].DataBind();
        }

        private void LoadLevels(PreDefindFlowType type)
        {
            storeAllowances.DataSource = TravelAllowanceManager.GetLeaveApprovalFlowList(true, type);
            storeAllowances.DataBind();

            gridApprovalList.Store[0].DataSource = TravelAllowanceManager.GetLeaveApprovalFlowList(false, type);
            gridApprovalList.Store[0].DataBind();            
        }




        
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
        }

      

      



        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            string json = e.ExtraParams["GridValues"];
            string jsonGridValuesApproval = e.ExtraParams["GridValuesApproval"];

            string jsonLeavePredNot = e.ExtraParams["GridLeavePredNot"];

            if (string.IsNullOrEmpty(json) || string.IsNullOrEmpty(jsonGridValuesApproval) || string.IsNullOrEmpty(jsonLeavePredNot))
            {
                return;
            }
            Status isValidToInsert = new Status();

            List<DAL.LeaveApprovalPreDefineEmployee> lines = JSON.Deserialize<List<DAL.LeaveApprovalPreDefineEmployee>>(json);
            List<DAL.LeaveApprovalPreDefineEmployee> linesApproval = JSON.Deserialize<List<DAL.LeaveApprovalPreDefineEmployee>>(jsonGridValuesApproval);

            List<DAL.LeavePredefinedNotification> linesLeavePredefinedNotif = JSON.Deserialize<List<DAL.LeavePredefinedNotification>>(jsonLeavePredNot);

            for (int i = 0; i < lines.Count; i++)
            {
                if (string.IsNullOrEmpty(lines[i].AuthorityType.ToString().Trim()))
                {
                    isValidToInsert.IsSuccess = false;
                    isValidToInsert.ErrorMessage = "Authority can not be empty for the row " + (i + 1) + ". Please remove empty rows.";
                }           


            }
            int row =1;
            foreach (DAL.LeaveApprovalPreDefineEmployee item in lines)
            {
                item.IsRecommend = true;
                if (item.AuthorityType != 6)
                {
                    item.EmployeeId = null;
                }

                if (item.AuthorityType == 6 &&
                    (item.EmployeeId == null))
                {
                    isValidToInsert.ErrorMessage = "For \"Specific Person\", Employee must be selected for the row " + row+ ".";
                    isValidToInsert.IsSuccess = false;
                }

                item.Sequence = lines.IndexOf(item) + 1;

                row += 1;
            }

            for (int i = 0; i < linesApproval.Count; i++)
            {
                if (string.IsNullOrEmpty(linesApproval[i].AuthorityType.ToString().Trim()))
                {
                    isValidToInsert.IsSuccess = false;
                    isValidToInsert.ErrorMessage = "Authority can not be empty for the row " + (i + 1) + ". Please remove empty rows.";
                }

              


            }
            int row1 = 1;
            foreach (DAL.LeaveApprovalPreDefineEmployee item in linesApproval)
            {
                item.IsRecommend = false;
                if (item.AuthorityType != 6)
                {
                    item.EmployeeId = null;
                }

                if (item.AuthorityType == 6 &&
                    (item.EmployeeId == null))
                {
                    isValidToInsert.ErrorMessage = "For \"Specific Person\", Employee must be selected for the row " + row + ".";
                    isValidToInsert.IsSuccess = false;
                }

                item.Sequence = linesApproval.IndexOf(item) + 1;

                row += 1;
            }

            if ((lines.Count == 0 && linesApproval.Count != 0) || (linesApproval.Count == 0 && lines.Count != 0))
            {
                isValidToInsert.ErrorMessage = "Recommender and Approval list is required.";
                isValidToInsert.IsSuccess = false;
            }

          
            

            int row3 = 1;
            foreach (DAL.LeavePredefinedNotification item in linesLeavePredefinedNotif)
            {               
                if (item.AuthorityType == 4 && item.BranchId == null)
                {
                    isValidToInsert.ErrorMessage = "For \"Branch Employee Approval\", Branch must be selected for the row " + row3 + ".";
                    isValidToInsert.IsSuccess = false;
                }

                if (item.AuthorityType == 5 && item.LevelGroupId == null)
                {
                    isValidToInsert.ErrorMessage = "For \"Group type\", Group must be selected for the row " + row3 + ".";
                    isValidToInsert.IsSuccess = false;
                }    

                if (item.AuthorityType != null &&
                    (item.EmployeeId1 == null && item.EmployeeId2 == null && string.IsNullOrEmpty(item.Emails)))
                {
                    isValidToInsert.ErrorMessage = "Any Employee must be selected for the row " + row3 + ".";
                    isValidToInsert.IsSuccess = false;
                }

                row3 += 1;
            }

            if (isValidToInsert.IsSuccess)
            {
                Status status = TravelAllowanceManager.UpdateLeaveOrder
                    (lines,linesApproval, linesLeavePredefinedNotif, int.Parse( cmbType.SelectedItem.Value));
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Approval Flow saved");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                    //NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
            else
            {
               // NewMessage.ShowWarningMessage(isValidToInsert.ErrorMessage);
                NewMessage.ShowWarningMessage(isValidToInsert.ErrorMessage);
            }

        }

    }
}
