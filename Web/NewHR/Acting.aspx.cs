﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using DAL;
using Utils;
using BLL.Base;
using System.IO;
using Utils.Helper;

namespace Web.NewHR.UserControls
{
    public partial class Acting : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }
        protected void Initialise()
        {

            
            storeEmployee.DataSource = EmployeeManager.GetAllActiveEmployees();
            storeEmployee.DataBind();


            cmbCurrentLevel.Store[0].DataSource = NewPayrollManager.GetAllParentLevels();
            cmbCurrentLevel.Store[0].DataBind();

            cmbToLevel.Store[0].DataSource = NewPayrollManager.GetAllParentLevels();
            cmbToLevel.Store[0].DataBind();

            cmbCurrentPost.Store[0].DataSource = new CommonManager().GetAllDesignations();
            cmbCurrentPost.Store[0].DataBind();

            LoadEmpFromQueryString();
        }
        private void LoadEmpFromQueryString()
        {
            int deputaionId = UrlHelper.GetIdFromQueryString("ActingID");
            if (deputaionId != 0)
                hdnID.Value = deputaionId.ToString();

            int did = GetActingID();
            if (did != 0)
            {
                ActingEmployee ded = NewPayrollManager.GetActingEmployeeById(did);
                               

                Process(ded);
                EEmployee emp = EmployeeManager.GetEmployeeById(ded.EmployeeId);
                employeeBlock.SetEmployeeDetailsWithPhoto(emp);

                ddlEmployee.Disable();
            }
        }
        private ActingEmployee Process(ActingEmployee entity)
        {
            if (entity == null)
            {
                entity = new ActingEmployee();

                entity.EmployeeId = int.Parse(ddlEmployee.SelectedItem.Value);

                entity.LetterNumber = txtLetterNo.Text.Trim();
                entity.LetterDate = calLetterDate.Text.Trim();
                entity.LetterDateEng = GetEngDate(entity.LetterDate);
                entity.ApplicableFrom = calApplicableFrom.Text.Trim();
                entity.ApplicableFromEng = GetEngDate(entity.ApplicableFrom);
                entity.ApplicableTill = calApplicableTill.Text.Trim();

                if (!string.IsNullOrEmpty(entity.ApplicableTill))
                    entity.ApplicableTillEng = GetEngDate(entity.ApplicableTill);

                entity.DesignationId = int.Parse(cmbCurrentPost.SelectedItem.Value);
                entity.FromLevelId = int.Parse(cmbCurrentLevel.SelectedItem.Value);
                entity.ToLevelId = int.Parse(cmbToLevel.SelectedItem.Value);
                entity.Notes = txtNotes.Text.Trim();
                //entity.Amount = decimal.Parse(txtAmount.Text.Trim());

                return entity;
            }
            else
            {
                ExtControlHelper.ComboBoxSetSelected(entity.EmployeeId.ToString(), ddlEmployee);
               
                txtLetterNo.Text = entity.LetterNumber;
                calLetterDate.Text = entity.LetterDate;

               
                calApplicableFrom.Text = entity.ApplicableFrom;
                calApplicableTill.Text = entity.ApplicableTill;
                ExtControlHelper.ComboBoxSetSelected(entity.FromLevelId.ToString(), cmbCurrentLevel);
                ExtControlHelper.ComboBoxSetSelected(entity.ToLevelId.ToString(), cmbToLevel);
                ExtControlHelper.ComboBoxSetSelected(entity.DesignationId.ToString(), cmbCurrentPost);
                //if (entity.Amount != null)
                //    txtAmount.Text = GetCurrency(entity.Amount);
                //else
                //    txtAmount.Text = "0";
                txtNotes.Text = entity.Notes;
               
            }
            return null;
        }
        protected void cmbActionTypes_Select(object sender, DirectEventArgs e)
        {

        }

        protected void ddlEmployee_Select(object sender, DirectEventArgs e)
        {
            EEmployee emp = EmployeeManager.GetEmployeeById(int.Parse(ddlEmployee.SelectedItem.Value));

            BLevel currentLevel = NewHRManager.GetEmployeeCurrentLevel(emp.EmployeeId);
           
            if (currentLevel != null)
            {              
                cmbCurrentLevel.SetValue(currentLevel.LevelId.ToString());

                BLevel nextActingLevel = NewHRManager.GetEmployeeNextLevelForActing(emp.EmployeeId);
                
                if (nextActingLevel != null)
                {
                    BLevelRate date = NewPayrollManager.GetLevelRate(nextActingLevel.LevelId, DateTime.Now);
                    cmbToLevel.SetValue(nextActingLevel.LevelId.ToString());
                    //txtAmount.Text = GetCurrency(date.StepGradeRate);
                }
                else
                {
                    cmbToLevel.ClearValue();
                }
            }
            else
            {
                cmbCurrentLevel.ClearValue();
            }

            EDesignation currentPost = NewHRManager.GetEmployeeCurrentPostDesitionPosition(emp.EmployeeId);
            if (currentPost != null)
            {

                if (currentLevel == null)
                    cmbCurrentLevel.SetValue(currentPost.BLevel.LevelId.ToString());

                cmbCurrentPost.SetValue(currentPost.DesignationId.ToString());
            }
            else
            {
                cmbCurrentPost.ClearValue();
            }
            employeeBlock.SetEmployeeDetailsWithPhoto(emp);
        }

        
       

        

        private void LoadList()
        {
           
        }
        public int GetActingID()
        {
            if (hdnID.Text != "" && hdnID.Text != null)
                return int.Parse(hdnID.Text);
            return 0;
        }
        public void btnSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                //this.Value = "true";

                ActingEmployee history = Process(null);
                history.EmployeeId = int.Parse(ddlEmployee.SelectedItem.Value);

                Status status = null;

                if (GetActingID() != 0)
                {
                    history.ActingID = GetActingID();
                    status = NewPayrollManager.InsertUpdateEmployeeActing(history, false);

                    //if (IsBranchTransfer)
                    //divMsgCtl.InnerHtml = "Acting updated.";
                    //else
                    //    divMsgCtl.InnerHtml = "Department transfer updated.";
                }
                else
                {
                    status = NewPayrollManager.InsertUpdateEmployeeActing(history, true);
                    //if (IsBranchTransfer)
                    //divMsgCtl.InnerHtml = "Acting saved.";

                    hdnID.Value = history.ActingID.ToString();
                    
                    //else
                    //    divMsgCtl.InnerHtml = "Department transfer saved.";
                }
                //divMsgCtl.Hide = false;

                if (status.IsSuccess == false)
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                    return;
                }

                // Prepare Sub-Department List
                //StringBuilder str = new StringBuilder("");
                //bool first = true;
                //List<SubDepartment> deps = DepartmentManager.GetAllSubDepartmentsByDepartment(history.DepeartmentId.Value);
                //foreach (SubDepartment obj in deps)
                //{
                //    if (first == false)
                //        str.Append(",");
                //    str.Append("\"" + obj.SubDepartmentId + "$$" + obj.Name + "\"");
                //    first = false;
                //}
                ////Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());

                // Register to set Branch/Subdepartment from js in Employee page


                Response.Redirect("~/newhr/ActingList.aspx");

                ClearFields();
                //btnCancel_Click(null, null);
            }
        }
        protected void ClearFields()
        {
            this.hdnID.Text = "";
         
        }

       
    }
}