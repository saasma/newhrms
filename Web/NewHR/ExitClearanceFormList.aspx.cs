﻿using BLL;
using BLL.Manager;
using Ext.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.NewHR
{
    public partial class ExitClearanceFormList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();
            }
        }

        protected void Initialize()
        {
            txtFormName.Text = "";
            chkLoanDetail.Value = false;
            chkAdvanceDetail.Value = false;
            chkRefundableDeposit.Value = false;
            chkAssetInCustody.Value = false;
            var userList = CommonManager.GetAllUsers();
            cmbType.Store[0].DataSource = userList;
            cmbType.Store[0].DataBind();

            //var clearanceDetailTypes = CommonManager.GetAllClearanceDetailTypes();
            //foreach (var detail in clearanceDetailTypes)
            //{
            //    X.GetCmp<FieldSet>("chkFormDetail").Add(new Ext.Net.Checkbox
            //    {
            //        BoxLabel = detail.ClearanceDetailType,
            //        ID = detail.ID.ToString(),
            //    });
            //}
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            storeExitClearanceForm.DataSource = CommonManager.GetAllExitClearanceFormType();
            storeExitClearanceForm.DataBind();
        }

        protected void btnSave(object sender, DirectEventArgs e)
        {
            string id = hiddenValue.Text;
           
            List<int> detailToShowInClearanceFormID = new List<int>();
            if (chkLoanDetail.Value.Equals(true))
            {
                detailToShowInClearanceFormID.Add((int)ClearanceDetailToShow.LoanDetail);
            }
            if (chkAdvanceDetail.Value.Equals(true))
            {
                detailToShowInClearanceFormID.Add((int)ClearanceDetailToShow.AdvanceDetail);
            }
            if (chkRefundableDeposit.Value.Equals(true))
            {
                detailToShowInClearanceFormID.Add((int)ClearanceDetailToShow.RefundableDeposit);
            }
            if (chkAssetInCustody.Value.Equals(true))
            {
                detailToShowInClearanceFormID.Add((int)ClearanceDetailToShow.AssetInCustody);
            }
            CommonManager.SaveClearanceFormDetail(txtFormName.Text, detailToShowInClearanceFormID,cmbType.SelectedItem.Text, cmbType.Value.ToString());
            Initialize();
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {
            var list = CommonManager.GetClearanceFormDetailById(Convert.ToInt32(hiddenValue.Text));
            txtFormName.Text = list.ClearanceFormName;
            cmbType.Text = list.PersonInPositionId.ToString();
            List<string> ids = list.ExitClearanceDetailId.Split(',').Reverse().ToList();
            foreach(var id in ids)
            {
                if(Convert.ToInt32(id) == (int)ClearanceDetailToShow.LoanDetail)
                {
                    chkLoanDetail.Value = true;
                }
                if (Convert.ToInt32(id) == (int)ClearanceDetailToShow.AdvanceDetail)
                {
                    chkAdvanceDetail.Value = true;
                }
                if (Convert.ToInt32(id) == (int)ClearanceDetailToShow.RefundableDeposit)
                {
                    chkRefundableDeposit.Value = true;
                }
                if (Convert.ToInt32(id) == (int)ClearanceDetailToShow.AssetInCustody)
                {
                    chkAssetInCustody.Value = true;
                }
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {

        }
        protected void btnArchive_Click(object sender, DirectEventArgs e)
        {

        }
    }
}