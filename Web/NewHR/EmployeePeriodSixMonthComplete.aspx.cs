﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Calendar;
using Web.Helper;
using Bll;

namespace Web.NewHR
{
    public partial class EmployeePeriodSixMonthComplete : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

      

        public void Initialise()
        {
            X.AddScript("#{btnThisMonth}.fireEvent('click');");
            
        }

        public void btnThisMonth_Change(object sender, DirectEventArgs e)
        {
            hdnFilterValue.Text = "ThisMonth";
            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;

            DateTime FromDate;// DateManager.GetStartDate(Utils.Helper.Util.GetCurrentDateAndTime().AddDays(-days));
            DateTime ToDate;// DateManager.GetEndDate(FromDate.AddDays(6));

            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);

            List<EmployeeServiceBo> CompeteThisMonth = DashboardManager.GetPeriodCompletionListing(6, FromDate, ToDate);

            gridEmployee.GetStore().DataSource = CompeteThisMonth;
            gridEmployee.GetStore().DataBind();

            if (IsEnglish)
                ColumnsBS.Hide();

        }




        public void btnNextMonth_Change(object sender, DirectEventArgs e)
        {
            hdnFilterValue.Text = "NextMonth";
            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;

            DateTime FromNextDate;// DateManager.GetStartDate(Utils.Helper.Util.GetCurrentDateAndTime().AddDays(-days));
            DateTime ToNextDate;// DateManager.GetEndDate(FromDate.AddDays(6));

            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            CustomDate firstDateOfNextMonth = lastDateOfThisMonth.IncrementByOneDay();
            CustomDate lastDateOfNextMonth = firstDateOfNextMonth.GetLastDateOfThisMonth();


            FromNextDate = DateManager.GetStartDate(firstDateOfNextMonth.EnglishDate);
            ToNextDate = DateManager.GetEndDate(lastDateOfNextMonth.EnglishDate);


            List<EmployeeServiceBo> CompleteThisYear = DashboardManager.GetPeriodCompletionListing(6, FromNextDate, ToNextDate);

            gridEmployee.GetStore().DataSource = CompleteThisYear;
            gridEmployee.GetStore().DataBind();
            if (IsEnglish)
                ColumnsBS.Hide();

        }

       

        public void btnLastMonth_Change(object sender, DirectEventArgs e)
        {

            hdnFilterValue.Text = "LastMonth";
            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;

            DateTime FromDate;// DateManager.GetStartDate(Utils.Helper.Util.GetCurrentDateAndTime().AddDays(-days));
            DateTime ToDate;// DateManager.GetEndDate(FromDate.AddDays(6));

            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            CustomDate LastDayOfLastMonth = firstDateOfThisMonth.DecrementByOneDay();
            CustomDate firstDateOfLastMonth = new CustomDate(1, LastDayOfLastMonth.Month, currentYear, IsEnglish);

            FromDate = DateManager.GetStartDate(firstDateOfLastMonth.EnglishDate);
            ToDate = DateManager.GetEndDate(firstDateOfLastMonth.EnglishDate);


            List<EmployeeServiceBo> CompeteLastMonth = DashboardManager.GetPeriodCompletionListing(6, FromDate, ToDate);

            gridEmployee.GetStore().DataSource = CompeteLastMonth;
            gridEmployee.GetStore().DataBind();
            if (IsEnglish)
                ColumnsBS.Hide();

        }
        public void btnExport_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(hdnFilterValue.Text.Trim()))
                return;

            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;

            DateTime FromDateThisMonth, FromDateNextMonth, FromDateThisYear, FromDateNextSixMonth, FromDateNextYear, FromDateLastMonth;
            DateTime ToDateThisMonth, ToDateNextMonth, ToDateThisYear, ToDateNextSixMonth, ToDateNextYear, ToDateLastMonth;


            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromDateThisMonth = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToDateThisMonth = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);



            CustomDate firstDateOfNextMonth = lastDateOfThisMonth.IncrementByOneDay();
            CustomDate lastDateOfNextMonth = firstDateOfNextMonth.GetLastDateOfThisMonth();


            FromDateNextMonth = DateManager.GetStartDate(firstDateOfNextMonth.EnglishDate);
            ToDateNextMonth = DateManager.GetEndDate(lastDateOfNextMonth.EnglishDate);

            FinancialDate currentFinancialYear = CommonManager.GetCurrentFinancialYear();
            FromDateThisYear = currentFinancialYear.StartingDateEng.Value;
            ToDateThisYear = currentFinancialYear.EndingDateEng.Value;

            CustomDate sixMonthStartDate = firstDateOfNextMonth.AddMonthIfFirstDay(6);
            CustomDate sixMonthLastDate = sixMonthStartDate.GetLastDateOfThisMonth();


            FromDateNextSixMonth = DateManager.GetStartDate(sixMonthStartDate.EnglishDate);
            ToDateNextSixMonth = DateManager.GetEndDate(sixMonthLastDate.EnglishDate);



            CustomDate NextYearStartDate = firstDateOfNextMonth.AddMonthIfFirstDay(12 - (today.Month));
            CustomDate NextYearLastDate = (NextYearStartDate.AddMonthIfFirstDay(11)).GetLastDateOfThisMonth();



            FromDateNextYear = DateManager.GetStartDate(NextYearStartDate.EnglishDate);
            ToDateNextYear = DateManager.GetEndDate(NextYearLastDate.EnglishDate);


            CustomDate LastDayOfLastMonth = firstDateOfThisMonth.DecrementByOneDay();
            CustomDate firstDateOfLastMonth = new CustomDate(1, LastDayOfLastMonth.Month, currentYear, IsEnglish);

            FromDateLastMonth = DateManager.GetStartDate(firstDateOfLastMonth.EnglishDate);
            ToDateLastMonth = DateManager.GetEndDate(firstDateOfLastMonth.EnglishDate);

            List<EmployeeServiceBo> _ListResult = new List<EmployeeServiceBo>();
            if (hdnFilterValue.Text.ToLower() == "thismonth")
            {
                _ListResult = DashboardManager.GetPeriodCompletionListing(6,FromDateThisMonth, ToDateThisMonth);
            }

            else if (hdnFilterValue.Text.ToLower() == "nextmonth")
            {
                _ListResult = DashboardManager.GetPeriodCompletionListing(6, FromDateNextMonth, ToDateNextMonth);
            }

            else if (hdnFilterValue.Text.ToLower() == "nextsixmonth")
            {
                _ListResult = DashboardManager.GetPeriodCompletionListing(6, FromDateNextSixMonth, ToDateNextSixMonth);
            }


            else if (hdnFilterValue.Text.ToLower() == "thisyear")
            {
                _ListResult = DashboardManager.GetPeriodCompletionListing(6, FromDateThisYear, ToDateThisYear);
            }


            else if (hdnFilterValue.Text.ToLower() == "nextyear")
            {
                _ListResult = DashboardManager.GetPeriodCompletionListing(6, FromDateNextYear, ToDateNextYear);
            }

            else if (hdnFilterValue.Text.ToLower() == "lastmonth")
            {
                _ListResult = DashboardManager.GetPeriodCompletionListing(6, FromDateLastMonth, ToDateLastMonth);
            }


            List<string> _hideColumnList = new List<string>();
            string[] _hideColumn = { "DepartmentID", "BranchID", "DesignationID" };
            _hideColumnList.AddRange(_hideColumn.ToList());
            if (IsEnglish)
                _hideColumnList.Add("JoinDateNepali");

             ExcelHelper.ExportToExcel("6 Month Complete List", _ListResult,

            _hideColumnList,
            new List<String>() { },
            new Dictionary<string, string>() { { "EmployeeId", "EIN" }, { "IdCardNo", "I No" }, { "JoinDateEng", "Join Date(A.D)" }, { "JoinDateNepali", "Join Date(B.S)" }, { "ServicePeriod", "Service Period" } },
            new List<string>() { }
            , new Dictionary<string, string>() { }
            , new List<string> { "EmployeeId", "IdCardNo", "Name", "Designation", "Level", "Branch", "Department", "JoinDateEng", "JoinDateNepali", "ServicePeriod" });
        }
    
        
     
    }
}