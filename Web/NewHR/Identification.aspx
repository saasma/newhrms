﻿<%@ Page Title="Identification" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="Identification.aspx.cs" Inherits="Web.NewHR.Identification" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/PhotographCtl.ascx" TagName="PhotographCtl"
    TagPrefix="ucPhotographCtl" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/UCitizenshipCtl.ascx" TagName="CtzenshipCtrl"
    TagPrefix="ucCtzn" %>
<%@ Register Src="~/NewHR/UserControls/UDrivingLiscenceCtl.ascx" TagName="DrivingLiscenceCtl"
    TagPrefix="ucDrivingLiscenceCtl" %>
<%@ Register Src="~/NewHR/UserControls/UPassportCtrl.ascx" TagName="PassportCtl" TagPrefix="ucPassportCtl" %>
<%@ Register Src="~/NewHR/UserControls/EmpSignatureCtrl.ascx" TagName="SignatureCtl"
    TagPrefix="ucSig" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>



<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../js/jquery.Jcrop.js"></script>
    <script type="text/javascript" src="../js/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.10.3.min.js"></script>
    <link id="Link1" rel="stylesheet" type="text/css" href="../css/jquery.Jcrop.css" />
    <link id="Link2" rel="stylesheet" type="text/css" href="../css/jquery.Jcrop.min.css" />
    <style type="text/css">
        .userDetailsClass
        {
            background-color: #D8E7F3;
            height: 50px;
            text-align: center;
            padding-top: 10px;
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body" style="margin-left: 145px;">
                <h4>
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <%--<uc2:EmployeeWizard Id="EmployeeWizard1" runat="server" />--%>
            <table>
                <tr>
                    <td valign="top">
                        <div style="float: left; margin-right: 25px;">
                            <ucEW:EmpDetailsWizard Id="EmployeeWizard1" runat="server" />
                        </div>
                    </td>
                    <td valign="top" style="width: 100%;">
                        <div class="userDetailsClass">
                            <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                        </div>
                        <div style="width: 805px; border: 1px solid #428BCA; margin-left: 0px">
                            <div>
                                <!-- panel-btns -->
                                <h4 class="panel-title sectionHeading" style="margin-left: 0px">
                                    <i></i>Identification
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body" style="width: 600px; margin-top: 10px;">
                                <table>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtINo" LabelAlign="Top" FieldLabel="Current I No" runat="server"
                                                Width="200px" />
                                            <asp:RequiredFieldValidator ID="valReqd" ControlToValidate="txtINo" Display="None"
                                                ErrorMessage="Current I No is required." runat="server" ValidationGroup="savecode" />
                                        </td>
                                        <td style="width: 10px">
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtContractINo" LabelAlign="Top" FieldLabel="Contract I No" runat="server"
                                                Width="200px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtCardSerialNo" LabelAlign="Top" FieldLabel="Card Serial No"
                                                runat="server" Width="200px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 10px;">
                                            <ext:Button runat="server" Text="Save Changes" Height="30" ID="btnUpload" Cls="btn btn-primary btn-sect">
                                                <DirectEvents>
                                                    <Click OnEvent="btnUpload_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                                <Listeners>
                                                    <Click Handler="valGroup= 'savecode'; return CheckValidation();">
                                                    </Click>
                                                </Listeners>
                                            </ext:Button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="width: 805px; border: 1px solid #428BCA; margin-left: 0px; margin-top: 15px;">
                            <div>
                                <!-- panel-btns -->
                                <h4 class="panel-title sectionHeading" style="margin-left: 0px">
                                    <i></i>Photograph and Signature
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body">
                                <table>
                                    <tr>
                                        <td>
                                            <ucPhotographCtl:PhotographCtl Id="PhotographCtl2" runat="server" />
                                        </td>
                                        <td style="width: 50px;">
                                        </td>
                                        <td>
                                            <ucSig:SignatureCtl Id="ESigCtlr" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="widget">
                            <div id="divCitizenship">
                                <div style="width:1019px; margin-left:-19px">
                                    <div>
                                        <!-- panel-btns -->
                                        <h4 class="sectionHeading">
                                            <i></i>Citizenship
                                        </h4>
                                    </div>
                                    <!-- panel-heading -->
                                    <div class="panel-body">
                                        <table class="fieldTable firsttdskip">
                                            <tr>
                                                <td>
                                                    <ucCtzn:CtzenshipCtrl Id="CitznCtrl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            
                            </div>
                            <div id="div1">
                                
                                <div style="width:1019px; margin-left:-19px">
                                    <div>
                                        <!-- panel-btns -->
                                        <h4 class="panel-title sectionHeading">
                                            <i></i>Driving Licence
                                        </h4>
                                    </div>
                                    <!-- panel-heading -->
                                    <div class="panel-body">
                                        <table class="fieldTable firsttdskip">
                                            <tr>
                                                <td>
                                                    <ucDrivingLiscenceCtl:DrivingLiscenceCtl Id="DrivingLiscenceCtl1" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                
                                
                            </div>
                            <div id="divPassport">
                                <div style="width:1019px; margin-left:-19px;">
                                    <div>
                                        <!-- panel-btns -->
                                        <h4 class="panel-title sectionHeading">
                                            <i></i>Passport
                                        </h4>
                                    </div>
                                    <!-- panel-heading -->
                                    <div class="panel-body">
                                        <table class="fieldTable firsttdskip">
                                            <tr>
                                                <td>
                                                    <ucPassportCtl:PassportCtl Id="PassportCtl1" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
