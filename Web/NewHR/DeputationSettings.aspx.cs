﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using DAL;
using BLL.Base;

namespace Web.NewHR.UserControls
{
    public partial class DeputationSettings :BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }
        protected void Initialise()
        {
            LoadList();

            cmbAttendance.Store[0].DataSource = ExtControlHelper.GetTextValues(typeof(DeputationAttendanceIn));
            cmbAttendance.Store[0].DataBind();

            cmbTA.Store[0].DataSource = ExtControlHelper.GetTextValues(typeof(DeputationTA));
            cmbTA.Store[0].DataBind();

            cmbDA.Store[0].DataSource = ExtControlHelper.GetTextValues(typeof(DeputationDA));
            cmbDA.Store[0].DataBind();

            cmbSalary.Store[0].DataSource = ExtControlHelper.GetTextValues(typeof(DeputationApplicableSalaryAllowance));
            cmbSalary.Store[0].DataBind();

        }

        private void LoadList()
        {
            Grid.Store[0].DataSource = NewPayrollManager.GetDeputationList();
            Grid.Store[0].DataBind();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }
        protected void ClearFields()
        {
            this.hdnID.Text = "";
            txtName.Text = "";
            txtDescription.Text = "";
            cmbTA.ClearValue();
            cmbDA.ClearValue();
            cmbSalary.ClearValue();
            cmbAttendance.ClearValue();

            btnSave.Text = Resources.Messages.Save;
        }

        public void EditDeputation(int deputationId)
        {

            Deputation entity = NewPayrollManager.GetDeputationById(deputationId);


            this.hdnID.Text = entity.DeputationId.ToString();
            txtName.Text = entity.Name;
            txtDescription.Text = entity.Description;
            cmbAttendance.SetValue(entity.AttendanceIn.ToString());
            cmbDA.SetValue(entity.DailyAllowance.ToString());
            cmbTA.SetValue(entity.TravellingAllowance.ToString());
            cmbSalary.SetValue(entity.ApplicableSalaryAllowance.ToString());




            btnSave.Text = Resources.Messages.Update;
            this.AEWindow.Show();
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AEWindow.Show();

        }
        protected void GridHealth_Command(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            int deputationId = int.Parse(e.ExtraParams["ID"]);
           

            switch (commandName)
            {
                case "Delete":
                    this.DeleteData(deputationId);
                    break;

                case "Edit":
                    this.EditDeputation(deputationId);
                    break;
            }

        }
        protected void DeleteData(int ID)
        {

            Status status = NewPayrollManager.DeleteDeputation(ID);

            if (status.IsSuccess)

                LoadList();
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }


        private void SaveFormData()
        {

            Status myStatus = new Status();
            Deputation entity = new Deputation();

            bool isSave = bool.Parse(this.hdnID.Text == "" ? "true" : "false");
            if (!isSave)
                entity.DeputationId = int.Parse(this.hdnID.Text);

            entity.Name = txtName.Text.Trim();
            entity.Description = txtDescription.Text.Trim();
            entity.AttendanceIn = int.Parse(cmbAttendance.SelectedItem.Value);
            entity.DailyAllowance = int.Parse(cmbDA.SelectedItem.Value);
            entity.ApplicableSalaryAllowance = int.Parse(cmbSalary.SelectedItem.Value);
            entity.TravellingAllowance = int.Parse(cmbTA.SelectedItem.Value);

            myStatus = NewPayrollManager.InsertUpdateDeputation(entity, isSave);

            if (myStatus.IsSuccess)
            {
                if (isSave)
                    NewMessage.ShowNormalMessage("Record Save successfully");
                else
                    NewMessage.ShowNormalMessage("Record Updated successfully");

                //  this.ClearFields();
                this.AEWindow.Close();
                LoadList();

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }
    }
}