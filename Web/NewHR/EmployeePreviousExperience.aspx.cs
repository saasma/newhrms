﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;
using System.Collections;


namespace Web.NewHR
{
    public partial class EmployeePreviousExperience : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "WorkExpImport", "../ExcelWindow/EPreviousExpImport.aspx", 450, 500);
        }

        private void Initialise()
        {
            storeBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeBranch.DataBind();

            storeLevel.DataSource = NewPayrollManager.GetAllParentLevelList().OrderBy(x => x.Name).ToList();
            storeLevel.DataBind();

            cmbExpCategory.Store[0].DataSource = NewHRManager.GetAllExperienceCategory();
            cmbExpCategory.Store[0].DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int total = 0, pageSize = 0, employeeId = -1, branchId = -1, levelId = -1, experienceCategoryId = -1;
            string employeeName = cmbSearch.Text;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            if (cmbExpCategory.SelectedItem != null && cmbExpCategory.SelectedItem.Value != null)
                experienceCategoryId = int.Parse(cmbExpCategory.SelectedItem.Value);

            if (cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            List<GetEmployeePrevExperienceListResult> list = NewHRManager.GetEmployeePreviousExpiriences(e.Page - 1, e.Limit, employeeId, employeeName, branchId, levelId, experienceCategoryId);

            //-- start filtering -----------------------------------------------------------
            FilterHeaderConditions fhc = new FilterHeaderConditions(e.Parameters["filterheader"]);

            foreach (FilterHeaderCondition condition in fhc.Conditions)
            {
                string dataIndex = condition.DataIndex;
                FilterType type = condition.Type;
                string op = condition.Operator;
                object value = null;

                switch (condition.Type)
                {
                    case FilterType.Boolean:
                        value = condition.Value<bool>();
                        break;

                    case FilterType.Date:
                        switch (condition.Operator)
                        {
                            case "=":
                                value = condition.Value<DateTime>();
                                break;

                            case "compare":
                                value = FilterHeaderComparator<DateTime>.Parse(condition.JsonValue);
                                break;
                        }
                        break;
                    case FilterType.String:
                        value = condition.Value<string>();
                        break;
                    case FilterType.Numeric:
                        value = condition.Value<int>();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                list.RemoveAll(item =>
                {
                    object oValue = item.GetType().GetProperty(dataIndex).GetValue(item, null);
                    string matchValue = null;
                    string itemValue = null;

                    if (type == FilterType.String)
                    {
                        matchValue = (string)value;

                        if (dataIndex == "EmployeeId")
                            itemValue = oValue.ToString();
                        else
                            itemValue = oValue as string;

                        matchValue = matchValue.ToLower();
                        itemValue = (itemValue == null ? "" : itemValue.ToLower());
                    }

                    switch (op)
                    {
                        case "=":
                            return oValue == null || !oValue.Equals(value);
                        case "compare":
                            return !((IEquatable<IComparable>)value).Equals((IComparable)oValue);
                        case "+":
                            return itemValue == null || !itemValue.Contains(matchValue); //return itemValue == null || !itemValue.StartsWith(matchValue);
                        case "-":
                            return itemValue == null || !itemValue.EndsWith(matchValue);
                        case "!":
                            return itemValue == null || itemValue.IndexOf(matchValue) >= 0;
                        case "*":
                            return itemValue == null || itemValue.IndexOf(matchValue) < 0;
                        default:
                            throw new Exception("Not supported operator");
                    }
                });
            }
            //-- end filtering ------------------------------------------------------------

            //-- start sorting ------------------------------------------------------------

            if (e.Sort.Length > 0)
            {
                list.Sort(delegate (GetEmployeePrevExperienceListResult obj1, GetEmployeePrevExperienceListResult obj2)
                {
                    object a;
                    object b;

                    int direction = e.Sort[0].Direction == Ext.Net.SortDirection.DESC ? -1 : 1;

                    a = obj1.GetType().GetProperty(e.Sort[0].Property).GetValue(obj1, null);
                    b = obj2.GetType().GetProperty(e.Sort[0].Property).GetValue(obj2, null);
                    return CaseInsensitiveComparer.Default.Compare(a, b) * direction;
                });
            }

            //Session["ExportPrevExp"] = list;

            storeExperience.DataSource = list;

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            e.Total = total;
        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            ((TextField)defaultField).Icon = Icon.Magnifier;

            return defaultField;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            int pageSize = 0, employeeId = -1, branchId = -1, levelId = -1, experienceCategoryId = -1;
            string employeeName = cmbSearch.Text;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            if (cmbExpCategory.SelectedItem != null && cmbExpCategory.SelectedItem.Value != null)
                experienceCategoryId = int.Parse(cmbExpCategory.SelectedItem.Value);

            if (cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            List<GetEmployeePrevExperienceListResult> list = NewHRManager.GetEmployeePreviousExpiriences(0, 9999999, employeeId, employeeName, branchId, levelId, experienceCategoryId);


            //if (Session["ExportPrevExp"] == null)
            //{
            //    NewMessage.ShowWarningMessage("Please reload the data for export.");
            //    return;
            //}

            //List<GetEmployeePrevExperienceListResult> list = (List<GetEmployeePrevExperienceListResult>)Session["ExportPrevExp"];

            //Session["ExportPrevExp"] = null;


            string template = ("~/App_Data/ExcelTemplate/EmpExperienceImport.xlsx");
            List<ExperienceCategory> experienceCategories = ListManager.GetExperienceCategory();
            ExcelGenerator.ExportEmployeePrevExperience(template, list, experienceCategories);

            //List<string> hiddenColumnList = new List<string>();
            //hiddenColumnList = new List<string> { "TotalRows", "RowNumber", "PrevEmploymentId" };

            //Bll.ExcelHelper.ExportToExcel("Employee Previous Work Experience", list,
            //       hiddenColumnList,
            //   new List<String>() { },
            //   new Dictionary<string, string>() { {"EmployeeId","EIN"}, {"Name","Employee Name"},{"Category","Experience Category"}, {"JobResponsibility","Job Responsibility"}, { "JoinDate", "Join Date" },{"ResignDate", "Resign Date"}, {"ReasonForLeaving", "Reason for leaving"} },
            //   new List<string>() { }
            //   , new List<string> { }
            //   , new List<string> { }
            //   , new Dictionary<string, string>() { }
            //   , new List<string> { "EmployeeId", "Name", "Category", "Organization", "Place", "Position", "JobResponsibility", "ReasonForLeaving", "JoinDate", "ResignDate" });



        }

    }
}