﻿<%@ Page Title="Employee Seniority Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeSeniority.aspx.cs" Inherits="Web.NewHR.EmployeeSeniority" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .x-tab-bar-default {
            background-color: inherit;
        }

        .hideLeftBlockCssInPage {
            margin: 0px !important;
            padding-left: 20px !important;
        }

        #menu {
            display: none;
        }

        #content {
            margin: inherit;
        }
    </style>
    <script type="text/javascript">
        function searchList() {
            <%=gridEmp.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

        var CommandHandlerMainGrid = function(command, record) {
            <%= hiddenValueSeniority.ClientID %>.setValue(record.data.Seniority);
            <%= hiddenValueEmpId.ClientID %>.setValue(record.data.EmployeeId);
            <%= hiddenValueEmpName.ClientID %>.setValue(record.data.Name);
            if(command=="Edit")
                <%= btnEditGroup.ClientID %>.fireEvent('click');
        };

        function PassQueryString(btn) {
            var EmpID = <%=cmbEmpSearch.ClientID %>.getValue();
            if(EmpID==null)
                EmpID="";
            var ret = EmpShiftPopup("EmpID=" + EmpID);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Employee Seniority List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden ID="hiddenValueEmpId" runat="server" />
        <ext:Hidden ID="hiddenValueSeniority" runat="server" />
        <ext:Hidden ID="hiddenValueEmpName" runat="server" />
        <ext:LinkButton runat="server" ID="btnEditGroup" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnEditGroup_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <table style="margin-top: 10px;">
                <tr>
                    <td style="width: 250px">
                        <ext:Store runat="server" ID="storeEmpSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model9" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Employee" LabelWidth="70"
                            EmptyText="Search Employee" LabelAlign="Top" runat="server" DisplayField="Name"
                            ValueField="EmployeeId" StoreID="storeEmpSearch" TypeAhead="false" Width="200"
                            PageSize="9999" HideBaseTrigger="true" MinChars="1" Hidden="false" TriggerAction="All"
                            ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl2" runat="server">
                                    <Html>
                                        <tpl>
                                                <div class="search-item">
                                                                <span>{Name}</span>  
                                                 </div>
					                    </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                               this.clearValue(); 
                                               this.getTrigger(0).hide();
                                           }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td valign="bottom">
                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-primary btn-sect" Width="100"
                            Text="Load">
                            <Listeners>
                                <Click Fn="searchList">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style='padding-left: 10px'>
                        <asp:LinkButton ID="btnExport" runat="server" Text="Import from Excel" OnClientClick="return PassQueryString(this)"
                            CssClass=" excel marginRight tiptip" Style="float: left;" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="clear: both; padding-top: 30px" runat="server" id="gridContainer">
        </div>
        <ext:GridPanel ID="gridEmp" Border="true" Header="false" runat="server" Width="700" Title="Title" Cls="gridtbl">
            <Store>
                <ext:Store runat="server" ID="gridEmpStore" PageSize="100" AutoLoad="true" OnReadData="Store_ReadData" RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model1" Name="gridEmpStoreModel" runat="server">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="Seniority" />
                                <ext:ModelField Name="Designation" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel runat="server" ID="ColumnModel2">
                <Columns>
                    <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="EIN" Width="50" ColumnID="ColEmployeeId"
                        Locked="true" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Name" Width="350" ColumnID="ColName"
                        Locked="true" DataIndex="Name">
                    </ext:Column>
                    <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Seniority" Width="100"
                        Locked="true" ColumnID="colSeniority" DataIndex="Seniority">
                    </ext:Column>
                      <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Designation" Width="200"
                        Locked="true" ColumnID="colSeniority" DataIndex="Designation">
                    </ext:Column>
                    <ext:CommandColumn runat="server" Width="30" Align="Left">
                        <Commands>
                            <ext:GridCommand Icon="NoteEdit" CommandName="Edit" ToolTip-Text="Edit">
                            </ext:GridCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandlerMainGrid(command, record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="gridEmpStore"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="2">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <ext:Window ID="WindowEmp" Hidden="true" runat="server" Title="Edit" Icon="ApplicationEdit" Width="500" Height="200"
            AutoScroll="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtEmpName" ReadOnly="true" Width="200" runat="server" FieldLabel="Employee Name"
                                LabelAlign="Top" LabelSeparator="">
                            </ext:TextField>
                        </td>
                        <td>
                            <ext:TextField ID="txtSeniorityValue" runat="server" Width="200" FieldLabel="Seniority" LabelAlign="Top" LabelSeparator=""></ext:TextField>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnSave_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    or
                                </div>
                                <ext:LinkButton runat="server" ID="btnCancel" StyleSpec="padding:0px;" Cls="btnFlatLeftGap"
                                    Text="<i></i>Cancel">
                                    <Listeners>
                                        <Click Handler="#{WindowEmp}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
