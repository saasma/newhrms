﻿<%@ Page Title="Manage employees" Language="C#" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" CodeBehind="PREmployeeListing.aspx.cs" Inherits="Web.NewHR.PREmployeeListing" %>

<%@ Register Src="~/NewHR/UserControls/EmployeeHRList.ascx" TagName="EmployeeList"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <uc1:EmployeeList ID="EmployeeList1" runat="server" />
        </div>
    </div>
</asp:Content>
