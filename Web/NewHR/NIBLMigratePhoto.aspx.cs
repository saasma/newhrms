﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using Utils.Calendar;
using BLL.Base;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using Web.Helper;
using BLL.Manager;

namespace Web.NewHR
{
    public partial class NIBLMigratePhoto : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["OldConnection"].ConnectionString);

            SqlCommand cmd = new SqlCommand("SELECT p.*,e.FULL_NAME,e.EMPLOYEE_CD FROM dbo.HR_EMPLOYEE_PHOTO p left JOIN dbo.HR_EMPLOYEE e ON e.EMPLOYEE_CD = p.EMPLOYEE_CD", con);
            con.Open();
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();

            NewHRManager mgr = new NewHRManager();
            EEmployee emp  = null;
            EmployeeManager empMgr = new EmployeeManager();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    try
                    {
                        if (dr["PHOTO_DATA"].Equals(DBNull.Value))
                            continue;

                        byte[] img = (byte[])dr["PHOTO_DATA"];
                        string empCode = dr["EMPLOYEE_CD"].ToString();
                        int ein = int.Parse(empCode);

                        emp = EmployeeManager.GetEmployeeById(ein);
                        if (emp == null || img == null || img.Length == 0)
                            continue;

                        //if (emp.HHumanResources.Count > 0)
                        //{
                        //    if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhoto))
                        //        continue;
                        //}


                        MemoryStream ms = new MemoryStream(img);
                        System.Drawing.Image i = System.Drawing.Image.FromStream(ms);



                        string location = Server.MapPath("~/Uploads/" + ein.ToString() + Path.GetExtension(dr["PHOTO_Name"].ToString()));
                        //string thumbnailLoc = Server.MapPath("~/Uploads/" + ein.ToString() + Path.GetExtension(dr["PHOTO_Name"].ToString()));
                        i.Save(location);


                        //try
                        //{

                        //    WebHelper.CreateThumbnail(location, 200, 200, thumbnailLoc);
                        //    thumbnailLoc = Path.GetFileName(thumbnailLoc);
                        //}
                        //catch { }




                        //mgr.SaveOrUpdateImage(UploadType.Photo,
                        //    "photo" + ein.ToString() + Path.GetExtension(dr["PHOTO_Name"].ToString())
                        //    , ein,
                        //     ein.ToString() + Path.GetExtension(dr["PHOTO_Name"].ToString()));


                        //ImgEmployee.ImageUrl = filename;
                        //Image1.ImageUrl = filename;

                        //ImgEmployeeThumbprint.ImageUrl = "../Uploads/" + thumbnailLocation;





                    }
                    catch(Exception ee1)
                    
                    {

                        Response.Write(ee1.ToString());
                    }
                }
            }
            con.Close();
        }

     
    }
}