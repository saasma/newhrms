﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class EmployeeIncomeDetails : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachPopUpCode(Page, "popupUpdateIncome", "../cp/AEIncome.aspx", 650, 700);
        }

        private void Initialise()
        {
            storeIncomeType.DataSource = PayManager.GetIncomeListByCompany(SessionManager.CurrentCompanyId);
            storeIncomeType.DataBind();

            storeEmployee.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport();
            storeEmployee.DataBind();

            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments();
            cmbDepartment.Store[0].DataBind();

            storeLevel.DataSource = ActivityManager.GetLevels();
            storeLevel.DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.Name).ToList();
            cmbDesignation.Store[0].DataBind();

            List<KeyValue> list = new JobStatus().GetMembers();
            list.RemoveAt(0);
            cmbJobStatus.Store[0].DataSource = list;
            cmbJobStatus.Store[0].DataBind();

            if (SessionManager.IsReadOnlyUser)
            {
                CommandColumnChange.Hide();
                btnApply.Hide();
                btnUnapply.Hide();
            }
        }

        

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            if (cmbIncomeType.SelectedItem == null || cmbIncomeType.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Please select income type.");
                cmbIncomeType.Focus();
                return;
            }

            int incomeId = int.Parse(cmbIncomeType.SelectedItem.Value);
            string employeeIdList = "";

            foreach (Tag tag in tfEmployee.Tags)
            {
                employeeIdList += tag.Value + ",";
            }

            if (!string.IsNullOrEmpty(employeeIdList))
                employeeIdList = employeeIdList.TrimEnd(',');

            int branchId = -1, departmentId = -1, designationId = -1, levelId = -1, applySatus = 0;
            string statusList = "";

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            foreach (Ext.Net.ListItem item in cmbJobStatus.SelectedItems)
                statusList += item.Value.ToString() + ",";

            if (statusList != "")
                statusList = statusList.TrimEnd(',');

            if (cmbStatus.SelectedItem != null && cmbStatus.SelectedItem.Value != null)
                applySatus = int.Parse(cmbStatus.SelectedItem.Value);

            storeEmpIncome.DataSource = NewHRManager.GetEmployeeIncomeDetailsList(employeeIdList, incomeId, branchId, departmentId, designationId, levelId,
                statusList, applySatus);
            storeEmpIncome.DataBind();           
                        
        }

        protected void btnApply_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetEmployeeIncomeDetailsListResult> list = JSON.Deserialize<List<GetEmployeeIncomeDetailsListResult>>(gridItemsJson);
            if (list.Count <= 0)
            {
                NewMessage.ShowWarningMessage("Please select employees to apply income.");
                return;
            }

            List<int> employeeIds = new List<int>();
            foreach (var item in list)
            {
                employeeIds.Add(item.EmployeeId);
            }

            int incomeId = list[0].IncomeId;
            Status status = NewHRManager.AddIncomeToEmployees(employeeIds, incomeId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Income applied to the employees successfully.");
                btnLoad_Click(null, null);

                CheckboxSelectionModel1.SelectedRows.Clear();
                CheckboxSelectionModel1.UpdateSelection();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }

        protected void btnUnapply_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetEmployeeIncomeDetailsListResult> list = JSON.Deserialize<List<GetEmployeeIncomeDetailsListResult>>(gridItemsJson);

            if (list.Count <= 0)
            {
                NewMessage.ShowWarningMessage("Please select employees to unapply income.");
                return;
            }

            List<int> empIncomeIds = new List<int>();
            foreach (var item in list)
            {
                if (item.EmployeeIncomeId.Equals(0))
                    continue;

                empIncomeIds.Add(item.EmployeeIncomeId);
            }

            int incomeId = list[0].IncomeId;
            string sourceType = list[0].Source;

            Status status = NewHRManager.DeleteIncomeFromEmployees(empIncomeIds, incomeId, sourceType);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Income unapplied to the employees successfully.");
                btnLoad_Click(null, null);

                CheckboxSelectionModel1.SelectedRows.Clear();
                CheckboxSelectionModel1.UpdateSelection();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);


        }


    }
}