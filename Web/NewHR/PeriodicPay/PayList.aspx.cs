﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class PayList : BasePage
    {


        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
     
        protected void Page_Load(object sender, EventArgs e)
        {
           


            if (!X.IsAjaxRequest && !IsPostBack)
            {

               
               Initialize();

                              

            }
        }

    
        private void Initialize()
        {

            cmbPayGroup.Store[0].DataSource = PeriodicPayManager.GetPayGroup();
            cmbPayGroup.Store[0].DataBind();


        }

        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            ClearControls();
            window.Show();
        }

        protected void ClearControls()
        {



            txtStartDate.Text = "";
            txtEndDate.Text = "";
            hiddenValue.Text = "";


        }

        public void cmbGroup_Select(object sender, DirectEventArgs e)
        {
            int groupId = int.Parse(cmbPayGroup.SelectedItem.Value);

            PayGroupCalculation lastPay = PeriodicPayManager.GetLastPay(groupId);
            if (lastPay != null)
            {

                txtStartDate.Text = lastPay.EndDateEng.Value.AddDays(1).ToString();
                txtEndDate.Text = lastPay.EndDateEng.Value.AddDays(7).ToString();
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdProjectAssociation");
            if (Page.IsValid)
            {
                PayGroupCalculation obj = new PayGroupCalculation();


                obj.StartDateEng  = DateTime.Parse(txtStartDate.Text.Trim());
                obj.EndDateEng = DateTime.Parse(txtEndDate.Text.Trim());
                obj.StartDate = BLL.BaseBiz.GetAppropriateDate(obj.StartDateEng.Value);
                obj.EndDate = BLL.BaseBiz.GetAppropriateDate(obj.EndDateEng.Value);
                obj.PayGroupRef_ID = int.Parse(cmbPayGroup.SelectedItem.Value);
                obj.CreatedBy = SessionManager.CurrentLoggedInUserID;
                obj.CreatedOn = DateTime.Now;

                if (!string.IsNullOrEmpty(hiddenValue.Text))
                    obj.CalculationID = int.Parse(hiddenValue.Text);


                CustomDate d1 = CustomDate.GetCustomDateFromString(obj.StartDate, IsEnglish);
                CustomDate d2 = CustomDate.GetCustomDateFromString(obj.EndDate, IsEnglish);
                obj.NepName = DateHelper.GetMonthShortNameForBoth(d1.Month, IsEnglish) + " " + d1.Day + " - " +
                                DateHelper.GetMonthShortNameForBoth(d2.Month, IsEnglish) + " " + d2.Day;

                obj.EngName = DateHelper.GetMonthShortNameForBoth(obj.StartDateEng.Value.Month, true) + "  " + obj.StartDateEng.Value.Day + " - " +
                        DateHelper.GetMonthShortNameForBoth(obj.EndDateEng.Value.Month, true) + "  " + obj.EndDateEng.Value.Day;

                Status status = PeriodicPayManager.SaveUpdatePeriodicPay(obj);
                if (status.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(hiddenValue.Text))
                        NewMessage.ShowNormalMessage("Record updated successfully.");
                    else
                        NewMessage.ShowNormalMessage("Record saved successfully.");
                    window.Close();
                    storeGrid.Reload();

                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {

            int id = int.Parse(hiddenValue.Text);


            PayGroupCalculation obj = PeriodicPayManager.GetDate(id);
            if (obj != null)
            {
                
                txtStartDate.Text = obj.StartDateEng.ToString();
                txtEndDate.Text = obj.EndDateEng.ToString();
                cmbPayGroup.SetValue(obj.PayGroupRef_ID.ToString());


                window.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int id = int.Parse(hiddenValue.Text);

            PeriodicPayManager.DeleteSkipDate(new PayGroupCalculation { CalculationID = id });

            NewMessage.ShowNormalMessage("Record deleted successfully.");
            storeGrid.Reload();

        }



        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;


            DateTime? startdate = null;
            DateTime? todate = null;

            if (!string.IsNullOrEmpty(calFromDate.RawText))
                startdate = calFromDate.SelectedDate.Date;

            if (!string.IsNullOrEmpty(calToDate.RawText))
                todate = calToDate.SelectedDate.Date;


            List<PayGroupCalculation> list = PeriodicPayManager.GetPayList(startdate, todate);

            e.Total = totalRecords;

            //if (list.Any())
            storeGrid.DataSource = list;
            storeGrid.DataBind();

        }

        public void btnExport_Click(object sender, EventArgs e)
        {

            DateTime? startdate = null;
            DateTime? todate = null;

            if (!string.IsNullOrEmpty(calFromDate.RawText))
                startdate = calFromDate.SelectedDate.Date;

            if (!string.IsNullOrEmpty(calToDate.RawText))
                todate = calToDate.SelectedDate.Date;


            List<PayGroupCalculation> list = PeriodicPayManager.GetPayList(startdate, todate);



            Bll.ExcelHelper.ExportToExcel("Pay List", list,
                new List<String> { },
            new List<String>() { },
            new Dictionary<string, string>() {},
            new List<string>() { }
            , new Dictionary<string, string>() {
              
            }
            , new List<string> { });


        }


    }
}