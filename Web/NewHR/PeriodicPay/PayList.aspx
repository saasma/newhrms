﻿<%@ Page Title="Pay List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="PayList.aspx.cs" Inherits="Web.CP.PayList" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

  
    var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.CalculationID);
            
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }
    var renderName = function(e1,e2,e3,e4)
    {
        return "<a href='../../CP/PeriodicPayDetails.aspx?id=" + e3.data.CalculationID + "'>"+ e3.data.PayName +"</a>";
    }

    </script>
    <style type="text/css">
        .holiday, .holiday a, .holiday td
        {
            color: #469146;
            background-color: #F0FFF0 !important;
        }
        .leave, .leave a, .leave td
        {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }
        
        .weeklyholiday, .weeklyholiday a, .weeklyholiday td
        {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Pay List
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <div style="clear: both">
            </div>
            <span runat='server' style="font-size: 14px; padding-top: 15px; margin-bottom: 10px"
                id="details"></span>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="From"
                                        ID="calFromDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:DateField LabelWidth="120" MarginSpec="0 5 0 5" Width="100" FieldLabel="To"
                                        ID="calToDate" runat="server" LabelAlign="Top" LabelSeparator="">
                                        <Plugins>
                                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                        </Plugins>
                                    </ext:DateField>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                Cls="itemgrid" OnReadData="Store_ReadData">
                <Store>
                    <ext:Store PageSize="1000" ID="storeGrid" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="false">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="CalculationID" Type="String" />
                                    <ext:ModelField Name="StartDateEng" Type="Date" />
                                    <ext:ModelField Name="StartDate" Type="String" />
                                    <ext:ModelField Name="EndDateEng" Type="Date" />
                                    <ext:ModelField Name="EndDate" Type="String" />
                                    <ext:ModelField Name="PayName" Type="String" />
                                    <ext:ModelField Name="TotalEmployees" Type="String" />
                                    <ext:ModelField Name="TotalGrosss" Type="String" />
                                    <ext:ModelField Name="TotalTax" Type="String" />
                                    <ext:ModelField Name="TotalNet" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="Column1" runat="server" Text="Pay Name" DataIndex="PayName" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120">
                            
                            <Renderer Fn="renderName" />
                            
                            </ext:Column>
                        <ext:DateColumn ID="DateColumn1" runat="server" Text="Start Date(AD)" DataIndex="StartDateEng"
                            Format="dd-MMM-yyyy" MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                       
                        <ext:DateColumn ID="DateColumn2" runat="server" Text="End Date(AD)" DataIndex="EndDateEng"
                            Format="dd-MMM-yyyy" MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                         <ext:Column ID="Column4" runat="server" Text="Start Date" DataIndex="StartDate" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column3" runat="server" Text="End Date" DataIndex="EndDate" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column2" runat="server" Text="Total" DataIndex="TotalEmployees" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="100" />
                        <ext:Column ID="Column5" runat="server" Text="Total Grosss" DataIndex="TotalGrosss"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Column6" runat="server" Text="Total Tax" DataIndex="TotalTax" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Column7" runat="server" Text="Total Net" DataIndex="TotalNet" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                       

                        <ext:CommandColumn ID="CommandColumn2" Align="Center" Cls="gridc" runat="server"
                            Width="40">
                            <Commands>
                                <ext:GridCommand>
                                    <Menu EnableScrolling="false">
                                        <Items>
                                            <ext:MenuCommand Text="Edit" CommandName="Edit" />
                                            <ext:MenuCommand Text="Delete" CommandName="Delete" />
                                        </Items>
                                    </Menu>
                                </ext:GridCommand>
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>

                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="buttonBlockSection" style="margin-top: 10px" runat="server" id="buttonBlock">
            <ext:Button runat="server" Cls="btn btn-primary btn-sect" Height="30px" ID="btnAddNew"
                Text="<i></i>Add New">
                <DirectEvents>
                    <Click OnEvent="btnAddNew_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
        <ext:Hidden ID="hiddenValue" runat="server" />
        <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnEdit_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDelete_Click">
                    <EventMask ShowMask="true" />
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete ?" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Window ID="window" runat="server" ButtonAlign="Left" Title="Add Period" Icon="Application"
            Width="400" Height="320" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                     <tr>
                        <td>
                            <ext:ComboBox ID="cmbPayGroup" ValueField="PayGroupID" DisplayField="Name" LabelSeparator=""
                                runat="server" FieldLabel="Pay Group *"  Width="300"
                                 >
                                <Store>
                                    <ext:Store ID="store1" runat="server" AutoLoad="true">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="PayGroupID" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                 <DirectEvents>
                                    <Select OnEvent="cmbGroup_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                ValidationGroup="SaveUpdProjectAssociation" ControlToValidate="cmbPayGroup" ErrorMessage="Pay group is required." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:DateField ID="txtStartDate" runat="server" FieldLabel="Start Date *" EmptyText="" LabelAlign="Left"
                                LabelSeparator="" Width="300" StyleSpec="margin-top:7px;">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdProjectAssociation" ControlToValidate="txtStartDate" ErrorMessage="Star date is required." />
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <ext:DateField ID="txtEndDate" runat="server" FieldLabel="End Date *" EmptyText="" LabelAlign="Left"
                                LabelSeparator="" Width="300" StyleSpec="margin-top:7px;">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin3" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                ValidationGroup="SaveUpdProjectAssociation" ControlToValidate="txtEndDate" ErrorMessage="End date is required." />
                        </td>
                    </tr>
                   
                    

                  
                </table>
            </Content>
            <Buttons>
                     <ext:Button runat="server" ID="btnSave" Height="30px" Width="80px" Cls="btn btn-primary"
                                                Text="<i></i>Save">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSave_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                                <Listeners>
                                                    <Click Handler="valGroup = 'SaveUpdProjectAssociation'; if(CheckValidation()) return true;">
                                                    </Click>
                                                </Listeners>
                                            </ext:Button>

                                            <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel">
                                            <Listeners>
                                                <Click Handler="#{window}.hide();">
                                                </Click>
                                            </Listeners>
                                        </ext:LinkButton>


            </Buttons>
        </ext:Window>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
