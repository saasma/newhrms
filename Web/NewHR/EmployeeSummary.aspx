﻿<%@ Page Title="Employee Details" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeSummary.aspx.cs" Inherits="Web.NewHR.EmployeeSummary" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/newhr/UserControls/UserProfileCtrl.ascx" TagName="UserProfile"
    TagPrefix="ucUserProfile" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<%@ Register Src="UserControls/EmployeeSummaryCtl.ascx" TagName="EmployeeSummaryCtl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        

        .items
        {
            padding-left: 20px;
        }
        .items span
        {
            
            display: block;
            font-family: 'Open Sans' ,sans-serif;
            padding-bottom: 2px;
        }
        .items img
        {
            float:left;
            width:18px;height:18px;
            
margin-right: 10px;
        }
        
     
        
        /*ext grid to nomal grid*/


        .buttonBlock
        {
            display: none;
        }
        #content
        {
            background: white;
        }

        }
        .accordion-edit:hover, .accordion-edit
        {
            font-weight: normal;
        }
        a:hover
        {
            
            font-weight: normal;
        }
        .accordion {
margin-bottom: 5px;

}
 .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
.accordion-inner
{
    background:none;
}

  #divEmployeeProfile table td
{
    
background-color: transparent;
padding-left:5px;
border-style: none;
color: #3b3838;
font-size: 10pt;
font-style: normal;
font-weight: normal;
margin-left: 0in;
text-shadow: none;
font-family: Calibri;
text-decoration: none ! important;
vertical-align: top;
writing-mode: page;
border-bottom-width: 0.0349cm;
border-bottom-style: solid;
border-bottom-color: rgba(206, 222, 236, 1);
border-left-style: none;
border-right-style: none;
border-top-width: 0.0349cm;
border-top-style: solid;
border-top-color: #bfbfbf;
}

.firstcell tr td:first-child {
background-color: rgba(239, 246, 251, 1)!important;
border-top:none!important;

}

.fa:before{margin-right:5px;}

#divEmployeeProfile .tableTitle {
background-color: #428BCA!important;
border-style: none;
color: White;
font-style: normal;
font-weight: normal;
margin-left: 0in;
text-decoration: none ! important;
vertical-align: middle;

border-bottom-style: solid;

border-left-style: none;
border-right-style: none;
border-top-style: none;
padding-top:0px!important;
min-width:400px;
padding-top: 5px!important;
padding-bottom: 5px;
border-bottom-width: 2px;
}

.tableSectionBreak
{
background-color: transparent!important;
height: 20px;
}

    .x-tab-bar-default{background-color:Transparent!important;}
    #content{padding-left:0px!important;}
    .innerLR{padding-left:0px;}
    .colorGrey{color:#636E7B;}
    
    .defaultfont{
font-size: 15px;
}

.x-tab-default .x-tab-inner
{
    font-weight:normal;
}
.x-grid-cell-inner{padding: 5px 5px 5px 5px;}
.x-column-header-inner {padding: 7px 5px 7px 5px;}


   </style>
    <script type="text/javascript">

        function expandAllCollapseAllHandler(btn) {
            if (btn.getText() == "EXPAND") {
                btn.setText("COLLAPSE");

                var list = Ext.query(".accordion-toggle");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];
                    item.className = "accordion-toggle collapsed";
                }


                list = Ext.query(".accordion-body");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];

                    item.className = "accordion-body in collapse";
                    item.style.height = "";
                }
            }
            else {
                btn.setText("EXPAND");


                var list = Ext.query(".accordion-toggle");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];
                    item.className = "accordion-toggle";
                }

                list = Ext.query(".accordion-body");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];


                    item.className = "accordion-body collapse";
                    item.style.height = "";
                }
            }
        }

        
    var CommandHandler = function(command, record){
            <%= hdnBranchDepartmentId.ClientID %>.setValue(record.data.EventSourceID);
               if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             };

               var setLink = function(e1,e2,e3)
        {
            return '<a href="javscript:void(0)" onclick="showDetailsNew(' + e3.data.EventSourceID + ',0);">Details</a>' 
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="Hidden_CustomerID" runat="server">
    </ext:Hidden>
    <ext:Hidden ID='hdnBranchDepartmentId' runat='server' />
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure,you want to delete the event?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body" style="margin-left: 0px;">
                <h4>
                    Employee Profile</h4>
            </div>
        </div>
        <!-- media -->
    </div>
    <div class="contentpanel" style="padding-top: 10px;">
        <%-- <uc2:EmployeeWizard Id="EmployeeWizard1" runat="server" />--%>
        <%-- <div style="float: left; margin-right: 20px;">
                            <ucEW:EmpDetailsWizard Id="EmployeeWizard1" runat="server" />
                        </div>--%>
        <table>
            <tr>
                <td valign="top">
                    <div class="text-center" style="width: 200px">
                        <img src="~/images/sample.jpg" class="img-circle img-offline img-responsive img-profile"
                            width="150" alt="" id="image" runat="server" />
                        <%--   <asp:Image ID="image" CssClass="img-circle img-offline img-responsive img-profile" runat="server" ImageUrl="~/images/sample.jpg"
                            Width="125px" Height="125px" />--%>
                        <h4 class="profile-name mb5" id="title" runat="server">
                        </h4>
                        <div runat="server">
                            <span class="fa fa-map-marker colorGrey defaultfont" style="padding: 1px" runat="server"
                                id="spnAddress"></span>
                        </div>
                        <div>
                            <i class="fa fa-briefcase colorGrey defaultfont defaultfont" style="padding-top: 15px"
                                id="positionDesignation" runat="server"></i>
                        </div>
                        <div id="Div1" runat="server">
                            <i class="fa" style="color: #428bca; font-size: 14px;" id="SpnPositionDesignationDepartment"
                                runat="server"></i>
                        </div>
                        <div style="margin-bottom: 0px;">
                            <div class="items">
                                <%--  <span runat="server" style="font-size: 15px; color: rgb(132, 132, 132)" id="spnAddress" />--%>
                                <div style="min-width: 100px">
                                    <%--  <span runat="server" style="font-size: 12px; color: rgb(132, 132, 132); float: left"
                                    id="positionDesignation"></span><span runat="server" style="font-size: 12px; color: #47759e;
                                        min-width: 100px;" id="SpnPositionDesignationDepartment"></span>--%>
                                </div>
                            </div>
                            <div id="divAddress" style="margin-top: 40px">
                                <address style="text-align: center;" class="fa colorGrey defaultfont">
                                    <span id="spnPermanentAddress" runat="server"></span><span runat="server" id="contactPhone">
                                    </span><span runat="server" id="contactMobile"></span>
                                    <br />
                                    <i class="fa fa-envelope-o"></i>&nbsp;<span runat="server" id="email"></span>
                                </address>
                                <br />
                            </div>
                            <img runat="server" id="groupLevelImg" src="../Styles/images/emp_position.png" style="margin-top: 8px;
                                display: none" />
                            <span runat="server" style="padding-top: 10px; display: none" id="groupLevel"></span>
                            <span runat="server" id="branch" style="display: none"></span>
                            <%--<span runat="server"
                                id="contactMobile" style="color: rgb(132, 132, 132)"></span><span runat="server"
                                    id="contactPhone" style="color: rgb(132, 132, 132)"></span>--%><%--<span runat="server" id="email">
                                    </span>--%><span runat="server" id="workingFor" style="width: 500px; display: none">
                                    </span>
                        </div>
                        <div style="clear: both">
                        </div>
                        <ext:SplitButton ID="btnEditDetails" Cls="btn btn-primary" Width="120px" runat="server"
                            Text="Edit Info">
                            <DirectEvents>
                                <Click OnEvent="btnEditDetails_Click" />
                            </DirectEvents>
                            <Menu>
                                <ext:Menu>
                                    <Items>
                                        <ext:HyperLink NavigateUrl="~/newhr/PersonalDetail.aspx?ID=" runat="server" Text="Main Info" />
                                        <ext:HyperLink ID="HyperLink1" NavigateUrl="~/newhr/address.aspx?ID=" runat="server"
                                            Text="Address" />
                                               <ext:HyperLink Target="_blank" ID="HyperLink13" NavigateUrl="~/newhr/jd.aspx?ID="
                                                            runat="server" Text="JD" />
                                        <ext:HyperLink ID="HyperLink2" NavigateUrl="~/newhr/Health.aspx?ID=" runat="server"
                                            Text="Health" />
                                        <ext:HyperLink ID="HyperLink3" NavigateUrl="~/newhr/DocumentListing.aspx?ID=" runat="server"
                                            Text="Documents" />
                                        <ext:HyperLink ID="HyperLink4" NavigateUrl="~/newhr/Family.aspx?ID=" runat="server"
                                            Text="Family" />
                                        <ext:HyperLink ID="HyperLink5" NavigateUrl="~/newhr/Identification.aspx?ID=" runat="server"
                                            Text="Identification" />
                                        <ext:HyperLink ID="HyperLink6" NavigateUrl="~/newhr/PreviousEmployment.aspx?ID="
                                            runat="server" Text="Experience" />
                                        <ext:HyperLink ID="HyperLink7" NavigateUrl="~/newhr/EducationTraining.aspx?ID=" runat="server"
                                            Text="Education" />
                                        <ext:HyperLink ID="HyperLink8" NavigateUrl="~/newhr/EmployeeLeave.aspx?ID=" runat="server"
                                            Text="Leaves" />
                                        <ext:HyperLink ID="HyperLink9" NavigateUrl="~/newhr/EmployeePayroll.aspx?ID=" runat="server"
                                            Text="Payroll" />
                                        <ext:HyperLink ID="linkRewardAddition" NavigateUrl="~/newhr/EmployeeGradeReward.aspx?ID="
                                            runat="server" Text="Reward / Additional" />
                                        <ext:HyperLink ID="HyperLink10" NavigateUrl="~/newhr/OtherHRDetails.aspx?ID=" runat="server"
                                            Text="Other" />
                                    </Items>
                                </ext:Menu>
                            </Menu>
                        </ext:SplitButton>
                    </div>
                </td>
                <td valign="top">
                    <div>
                        <span runat="server" id="spanRetiredDate" style="width: 1050px; color: Red"></span>
                        <ext:TabPanel ID="Panel2" runat="server" MinHeight="800" AutoHeight="true" StyleSpec="padding-top:10px"
                            Width="1000" AutoWidth="true" DefaultBorder="false">
                            <Items>
                                <ext:Panel Title="Summary" runat="server">
                                    <Content>
                                        <div id="collapse-1" class="accordion-body">
                                            <div id="divEmployeeProfile">
                                                <div class="accordion-inner">
                                                    <table>
                                                        <tr>
                                                            <td valign="top">
                                                                <table class="fieldTable firstcell" style="margin-left: -5px;">
                                                                    <tr>
                                                                        <td colspan="2" class="tableTitle">
                                                                            PERSONAL INFORMATION
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 140px;">
                                                                            EIN
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="ein"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            ID Card Number
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnIdcardNumber"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Card Serial Number
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnCardSeriallNumber"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="background-color: transparent!important; height: 20px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Date of Birth
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="dob"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td runat="server" id="gender1">
                                                                            {0}
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="gender"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Marital Status
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="maritalStatus"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            No of Children
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnNoOfChildren"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Spouse Name
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="SpnSpouseName"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Marriage Anniversary
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="SpnMarriageAnniversary"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Reference
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spanRefernce"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="background-color: transparent!important; height: 20px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Father's Name
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="fatherName"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Mother's Name
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnMotherName"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Grandfather's Name
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnGrandFatherName"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="background-color: transparent!important; height: 20px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Citizenship No
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnCitizenshipNo"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Passport No
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnPasswordNo"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Driving License No
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnDrivingLicenseNo"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            License Type
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnLicenseType"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="background-color: transparent!important; height: 20px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            PF Start Date
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnPFStartDate"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            PF No
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnPFNo"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            PAN No
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="SpnPanNo"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Gratuity Start Date
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnGratuityStartDate"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            CIT No
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnCITNo"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="background-color: transparent!important; height: 20px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Payment Bank
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnPaymentBank"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Account Number
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnAccountNumber"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="background-color: transparent!important; height: 20px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Nationality
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnNationality"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Ethnicity
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnEthinicity"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Religion
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnReligion"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Birthmark
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnBrithMark"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Hobby
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnHobby"></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="width: 20px">
                                                            </td>
                                                            <td valign="top">
                                                                <table class="fieldTable  firstcell">
                                                                    <tr>
                                                                        <td colspan="2" class="tableTitle">
                                                                            STARTING JOB POSITION
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 115px">
                                                                            Hire Method
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="hireMethod"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Appointment Date
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spanAppointmentDate"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Hire Date
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="hireDate"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Join Job Status
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnJobStatus"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Contract End Date
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="contractEndDate"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="trLevel">
                                                                        <td style="width: 140px;">
                                                                            Level/Position
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="level"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="trGrade">
                                                                        <td style="width: 140px;">
                                                                            Grade
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="gradeDisplay"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Branch
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnStartingBarnch"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Department
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnStartingDepartment"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Level/Position
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnStartingLevel"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Designation
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnStartingPosition"></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table class="fieldTable firstcell" style="margin-top: 20px">
                                                                    <tr>
                                                                        <td colspan="2" class="tableTitle">
                                                                            CURRENT JOB POSITION
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 140px">
                                                                            Last Promotion
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnLastPromotion"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Status
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnCurrentStatus"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Branch
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnCurrentBranch"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Department
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnCurrentDepartment"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Level/Position
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnCurrentLevel"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Designation
                                                                        </td>
                                                                        <td>
                                                                            <span runat="server" id="spnCurrentPosition"></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </td> </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div style="float: right">
                                                <table>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </Content>
                                </ext:Panel>
                                <ext:Panel ID="Panel1" Title="Profile" runat="server">
                                    <Content>
                                        <div style="margin-left: -5px;">
                                            <ucUserProfile:UserProfile Id="userProfile1" runat="server" />
                                        </div>
                                    </Content>
                                </ext:Panel>
                                <ext:Panel ID="pnl3" Title="Service History" runat="server">
                                    <Content>
                                        <ext:Button ID="btnNewEvent" MarginSpec="25 10 10 0" Width="100" Height="30" runat="server"
                                            Text="New Event">
                                            <DirectEvents>
                                                <Click OnEvent="btnNewEvent_Click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                        <ext:GridPanel Width="1000" StyleSpec="margin-top:15px;" ID="gridEventList" runat="server"
                                            OnReadData="Store_ReadData" Scroll="Horizontal">
                                            <Store>
                                                <ext:Store ID="storeEmpList" runat="server" AutoLoad="true" RemotePaging="true" RemoteSort="true">
                                                    <Model>
                                                        <ext:Model ID="Model1" runat="server" IDProperty="EventSourceID">
                                                            <Fields>
                                                                <ext:ModelField Name="RowNumber" Type="Int" />
                                                                <ext:ModelField Name="EventSourceID" Type="Int" />
                                                                <ext:ModelField Name="BranchId" Type="Int" />
                                                                <ext:ModelField Name="Date" Type="String" />
                                                                <ext:ModelField Name="DateEng" Type="Date" />
                                                                <ext:ModelField Name="EmployeeId" Type="String" />
                                                                <ext:ModelField Name="Name" Type="String" />
                                                                <ext:ModelField Name="EventName" Type="String" />
                                                                <ext:ModelField Name="Branch" Type="String" />
                                                                <ext:ModelField Name="Department" Type="String" />
                                                                <ext:ModelField Name="ServiceStatus" Type="String" />
                                                                <ext:ModelField Name="Designation" Type="String" />
                                                                <ext:ModelField Name="Level" Type="String" />
                                                                <ext:ModelField Name="IdCardNo" Type="String" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                    <Sorters>
                                                        <ext:DataSorter Property="DateEng" Direction="ASC" />
                                                    </Sorters>
                                                </ext:Store>
                                            </Store>
                                            <SelectionModel>
                                                <ext:CellSelectionModel runat="server" EnableKeyNav="true" />
                                            </SelectionModel>
                                            <View>
                                                <ext:GridView EnableTextSelection="true"   runat="server" AllowCopy="true" Copy="true" />
                                            </View>
                                            <%--<Plugins>
                                                <ext:CellEditing ClicksToEdit="1" runat="server" />
                                            </Plugins>--%>
                                            <ColumnModel ID="ColumnModel1" runat="server">
                                                <Columns>
                                                    <ext:Column ID="Column5" runat="server" Text="SN" DataIndex="RowNumber" MenuDisabled="true"
                                                        Sortable="false" Align="Left" Width="30" />
                                                    <ext:Column ID="Column3" runat="server" Text="Event" DataIndex="EventName" MenuDisabled="true"
                                                        Sortable="false" Align="Left" Width="120" />
                                                    <ext:Column ID="colFromDate" runat="server" Text="Date" DataIndex="Date" MenuDisabled="true"
                                                        Sortable="false" Align="Left" Width="90">
                                                        <%--<Editor>
                                                            <ext:TextField ID="TextField1" runat="server" />
                                                        </Editor>--%>
                                                    </ext:Column>
                                                    <ext:DateColumn ID="colFromDateEng" runat="server" Text="Eng Date" DataIndex="DateEng"
                                                        Format="yyyy-MMM-dd" MenuDisabled="true" Sortable="false" Align="Left" Width="90">
                                                       <%-- <Editor>
                                                            <ext:DateField runat="server" />
                                                        </Editor>--%>
                                                    </ext:DateColumn>
                                                    <ext:Column ID="colFromBranch" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="true"
                                                        Sortable="false" Align="Left" Width="150" />
                                                    <ext:Column ID="Column2" runat="server" Text="Department" DataIndex="Department"
                                                        MenuDisabled="true" Sortable="false" Align="Left" Width="150" />
                                                    <ext:Column ID="colToBranch" runat="server" Text="Service Status" DataIndex="ServiceStatus"
                                                        MenuDisabled="true" Sortable="false" Align="Left" Width="90" />
                                                    <ext:Column ID="Column211" runat="server" Text="Position" DataIndex="Designation"
                                                        MenuDisabled="true" Sortable="false" Align="Left" Width="140" />
                                                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="" Align="Center">
                                                        <Commands>
                                                            <ext:CommandSeparator />
                                                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                                                CommandName="Edit" />
                                                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                                                CommandName="Delete" />
                                                        </Commands>
                                                        <Listeners>
                                                            <Command Handler="CommandHandler(command,record);" />
                                                        </Listeners>
                                                    </ext:CommandColumn>
                                                    <ext:Column ID="Column4" runat="server" Text="Details" MenuDisabled="true" Sortable="false"
                                                        Align="Left" Width="60">
                                                        <Renderer Fn="setLink" />
                                                    </ext:Column>
                                                </Columns>
                                            </ColumnModel>
                                        </ext:GridPanel>
                                    </Content>
                                </ext:Panel>
                            </Items>
                        </ext:TabPanel>
                        <uc1:EmployeeSummaryCtl Id="empSummaryDetails" runat="server" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <ext:Window ID="eventAddEditWindow" runat="server" Title="Event Details" Icon="Application"
        Width="800" Height="480" BodyPadding="5" Hidden="true" Modal="false">
        <Content>
            <table class="fieldTable" style="margin-left: 20px;">
                <tr>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbEventType" runat="server" LabelStyle="font-weight:bold;text-align:right"
                            ValueField="EventID" DisplayField="Name" Width="320" LabelWidth="120" LabelSeparator=""
                            FieldLabel="Event Type *" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EventID" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="reqdEventType" runat="server" ControlToValidate="cmbEventType"
                            Display="None" ErrorMessage="Event is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="tblTD">
                        <pr:CalendarExtControl LabelStyle="font-weight:bold;text-align:right" FieldLabel="Applicable Date *"
                            Width="320" LabelWidth="120" ID="calFromDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="calFromDate"
                            Display="None" ErrorMessage="Applicable date is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td class="tblTD">
                        <pr:CalendarExtControl LabelStyle="font-weight:bold;text-align:right" FieldLabel="To Date"
                            Width="320" LabelWidth="120" ID="calToDate" runat="server" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <%-- <td style="width: 120px;">
                        Letter Number
                    </td>--%>
                    <td class="tblTD">
                        <ext:TextField FieldLabel="Letter Number" LabelStyle="font-weight:bold;text-align:right"
                            Width="320" LabelWidth="120" ID="txtLetterNumber" runat="server" LabelAlign="Left"
                            LabelSeparator="">
                        </ext:TextField>
                    </td>
                    <%--<td style="width: 120px;">
                        
                    </td>--%>
                    <td class="tblTD">
                        <pr:CalendarExtControl FieldLabel="Letter Date *" LabelStyle="font-weight:bold;text-align:right"
                            Width="320" LabelWidth="120" ID="calLetterDate" runat="server" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="rfvLetterDate" runat="server" ControlToValidate="calLetterDate"
                            Display="None" ErrorMessage="Letter date is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <%-- <td style="width: 120px;">
                        Branch *
                    </td>--%>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbBranch" LabelStyle="font-weight:bold;text-align:right" runat="server"
                            ValueField="BranchId" DisplayField="Name" Width="320" LabelWidth="120" FieldLabel="Branch *"
                            LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="Branch_Select" />
                            </DirectEvents>
                          
                            <%-- <Listeners>
                                <AfterRender Fn="onAfterRender" />
                            </Listeners>--%>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvBranch" runat="server" ControlToValidate="cmbBranch"
                            Display="None" ErrorMessage="Branch is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <%-- <td style="width: 120px;">
                        Department *
                    </td>--%>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbDepartment" LabelStyle="font-weight:bold;text-align:right" runat="server"
                            ValueField="DepartmentId" DisplayField="Name" Width="320" LabelWidth="120" FieldLabel="Department *"
                            LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="rfvDepartment" runat="server" ControlToValidate="cmbDepartment"
                            Display="None" ErrorMessage="Department is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbSubDepartment" LabelStyle="font-weight:bold;text-align:right"
                            runat="server" ValueField="SubDepartmentId" DisplayField="Name" Width="320" LabelWidth="120"
                            FieldLabel="Sub Department" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="SubDepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td class="tblTD">
                    </td>
                </tr>
                <tr>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbLevel" LabelStyle="font-weight:bold;text-align:right" runat="server"
                            LabelAlign="Left" FieldLabel="Level/Position *" ValueField="LevelId" DisplayField="GroupLevel"
                            Width="320" LabelWidth="120" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model8" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LevelId" Type="String" />
                                                <ext:ModelField Name="GroupLevel" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbLevel_Select">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="cmbLevel"
                            Display="None" ErrorMessage="Level/Position is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td class="tblTD">
                        <ext:ComboBox ID="cmbDesignation" LabelStyle="font-weight:bold;text-align:right"
                            runat="server" LabelAlign="Left" FieldLabel=" Designation *" ValueField="DesignationId"
                            DisplayField="Name" Width="320" LabelWidth="120" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store7" runat="server">
                                    <Model>
                                        <ext:Model ID="Model9" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DesignationId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="cmbDesignation"
                            Display="None" ErrorMessage="Designation is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbStatus" LabelStyle="font-weight:bold;text-align:right" runat="server"
                            LabelAlign="Left" FieldLabel=" Service Status *" ValueField="Key" DisplayField="Value"
                            Width="320" LabelWidth="120" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store8" runat="server">
                                    <Model>
                                        <ext:Model ID="Model10" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" Type="String" />
                                                <ext:ModelField Name="Key" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cmbStatus"
                            Display="None" ErrorMessage="Service status is required." ValidationGroup="SaveBrTran"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <%--<asp:DropDownList Style="margin-top: 10px" CssClass='statusWidth' ID="ddlStatus"
                            runat="server" AppendDataBoundItems="true" Width="150px">
                            <asp:ListItem Selected="true" Value="-1" Text="--Status--"></asp:ListItem>
                        </asp:DropDownList>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="tblTD">
                        <ext:TextArea LabelWidth="120" LabelStyle="font-weight:bold;text-align:right" FieldLabel="Note"
                            ID="txtNote" runat="server" LabelSeparator="" Rows="3" Width="650px" Height="80" />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div id="buttonsBar" runat="server" class="popupButtonDiv" style="float: right;">
                            <div style="width: 100px;">
                                <ext:Button runat="server" ID="btnSave" Height="32" Width="90" Text="<i></i>Save">
                                    <DirectEvents>
                                        <Click OnEvent="btnSave_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveBrTran'; if(CheckValidation()) return this.disable(); else return false;">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                            </div>
                            <ext:Button runat="server" ID="btnCancel" Cls="btn btn-default" Text="Cancel">
                                <Listeners>
                                    <Click Handler="#{eventAddEditWindow}.hide();" />
                                </Listeners>
                            </ext:Button>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
