﻿<%@ Page Title="Employee Status Change" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmpChangeStatusDetails.aspx.cs" Inherits="Web.NewHR.EmpChangeStatusDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .disabledIncomeDeduction, .disabledIncomeDeduction a, .disabledIncomeDeduction td
    {
        background-color: #ddd !important;
    }
        
    .unappliedIncomeDeduction, .unappliedIncomeDeduction a, .unappliedIncomeDeduction td
    {
        color: #E6E6E6;
    }

</style>

<script type="text/javascript">
    var CommandHandler = function(value, command,record,p2) {
        var employeeId = record.data.EmployeeId;
        var eCurrentStatusId = record.data.ECurrentStatusId;
        
        var ret = promote("EId=" + employeeId + "&statusId=" + eCurrentStatusId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    //refreshIncomeList(0);
                }
            }     
        
    };

    function refreshGrid() {
       searchList();
    }

     var getRowClass = function (record) {
    
            var clsName = record.data.RowBackColor;
            if(clsName != '')
                return clsName;

            return "";

        };

    
    function refreshWindow()
    {
        searchList();
    }

    function loadAndHide(win) {
        win.close();
        showLoading();
        searchList();
    }

    
    function searchList() {
        <%=gridChangeStatus.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
        <%=PagingToolbar1.ClientID %>.doRefresh();
    }

    function addChangeStatus()
    {
        var employeeId = <%= cmbEmployee.ClientID %>.getValue();
        if(employeeId == null || employeeId == 'undefined')
        {
            alert('Please select employee.');
            <%= cmbEmployee.ClientID %>.focus();
            return;
        }

         var ret = promote("EId=" + employeeId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    //refreshIncomeList(0);
                }
            }   

    }
</script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>

   
    <ext:Hidden ID="hdnEmployeeId" runat="server" /> 
  
    
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Status Change
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbBranch" Width="160" runat="server" ValueField="BranchId" DisplayField="BranchName"
                            FieldLabel="Branch" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" />
                                                <ext:ModelField Name="BranchName" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                </td>
                <td>
                     <ext:ComboBox ID="cmbDepartment" Width="160" runat="server" ValueField="DepartmentId" MarginSpec="0 0 0 10"
                            DisplayField="Name" FieldLabel="Department" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                </td>
                <td >
                    <ext:ComboBox ID="cmbSubDepartment" Width="160" runat="server" ValueField="SubDepartmentId" MarginSpec="0 0 0 10"
                        DisplayField="Name" FieldLabel="Sub Department" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store11" runat="server">
                                <Model>
                                    <ext:Model ID="Model11" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="SubDepartmentId" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                </td>
                <td>
                     <ext:ComboBox FieldLabel="Designation" ID="cmbDesignation"  MarginSpec="0 0 0 10"
                        Width="160"  runat="server" ValueField="DesignationId" DisplayField="Name"
                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store4" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" IDProperty="DesignationId" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="DesignationId" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:ComboBox ID="cmbLevel" runat="server" FieldLabel="Level"  MarginSpec="0 0 0 10"
                        Width="160"  DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                        LabelSeparator="" LabelAlign="Top">
                        <Store>
                            <ext:Store ID="storeLevel" runat="server">
                                <Model>
                                    <ext:Model ID="modelLevel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="LevelId" Type="Int" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:MultiCombo Width="160" LabelSeparator="" SelectionMode="All" LabelAlign="Top"  MarginSpec="0 0 0 10"
                        ID="cmbJobStatus" DisplayField="Value" ValueField="Key"
                        runat="server" FieldLabel="Job Status">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model8" IDProperty="Key" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Key" />
                                            <ext:ModelField Name="Value" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                        </Listeners>
                    </ext:MultiCombo>


                    

                </td>
            </tr>
            <tr>
                
                <td>
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>

                    <ext:ComboBox LabelSeparator="" ID="cmbEmployee" runat="server" DisplayField="Name"
                        FieldLabel="Employee" LabelAlign="Top" ValueField="EmployeeId" EmptyText="Search Employee"
                        StoreID="storeSearch" TypeAhead="false" Width="160" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="true">
                        <ListConfig LoadingText="Searching..." MinWidth="160" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>

                </td>
                
                <td>
                    <ext:ComboBox ID="cmbUnit" Width="160" runat="server" ValueField="UnitId" MarginSpec="0 0 0 10"
                            DisplayField="Name" FieldLabel="Unit" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                                              
                            <Store>
                                <ext:Store ID="Store15" runat="server">
                                    <Model>
                                        <ext:Model ID="Model16" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="UnitId" Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                </td>

                
                <td colspan="3" style="padding-top: 16px;" >
                  <table>
                    <tr>
                        <td>
                            <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Load" Width="100" MarginSpec="5 0 0 15">
                                <Listeners>
                                    <Click Fn="searchList">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>
                            <ext:Button ID="btnAddStatus" runat="server" Cls="btn btn-save" Text="Add Status" Width="100" MarginSpec="5 0 0 15">
                                <Listeners>
                                    <Click Fn="addChangeStatus" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>
                            <ext:Button ID="Button1" runat="server" Cls="btn btn-save" Text="Import" Width="100" MarginSpec="5 0 0 15" OnClientClick="importPopup">
                                </ext:Button>
                        </td>
                    </tr>
                  </table>
                    
               
             
                   
                    
               
                    
                </td>

                
            </tr>
        </table>
        <br />        

        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridChangeStatus" runat="server" Cls="itemgrid"
            Scroll="None" Width="1100">
            <Store>
                <ext:Store ID="storeChangeStatus" runat="server" AutoLoad="true" OnReadData="Store_ReadData" PageSize="20"
                    RemotePaging="true" RemoteSort="false">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="ECurrentStatusId">
                            <Fields>
                                <ext:ModelField Name="ECurrentStatusId" Type="Int" />
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="IDCardNo" Type="String" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="Branch" Type="String" />
                                <ext:ModelField Name="Department" Type="String" />
                                <ext:ModelField Name="CurrentStatusName" Type="String" />
                                <ext:ModelField Name="EventType" Type="String" />
                                <ext:ModelField Name="FromDate" Type="String" />
                                <ext:ModelField Name="ToDate" Type="String" />
                                <ext:ModelField Name="Note" Type="String" />
                                <ext:ModelField Name="CurrentStatus" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>  
                    <ext:Column ID="colEmployeeId" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                        Width="50" Align="Left" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="INo" Hidden="true"
                        Width="60" Align="Left" DataIndex="IDCardNo">
                    </ext:Column>
                    <ext:Column ID="colName" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                        Width="160" Align="Left" DataIndex="Name">
                    </ext:Column>
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                        Width="160" Align="Left" DataIndex="Branch">
                    </ext:Column>
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                        Width="160" Align="Left" DataIndex="Department">
                    </ext:Column>
                    <ext:Column ID="colIncomeType" Sortable="false" MenuDisabled="true" runat="server" Text="Current Status"
                        Width="100" Align="Left" DataIndex="CurrentStatusName">
                    </ext:Column>
                    <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Event Type"
                        Width="120" Align="Left" DataIndex="EventType">
                    </ext:Column>
                    <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="From Date"
                        Width="90" Align="Left" DataIndex="FromDate">
                    </ext:Column>
                    <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="To Date"
                        Width="90" Align="Left" DataIndex="ToDate">
                    </ext:Column>
                    <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Note"
                        Width="120" Align="Left" DataIndex="Note">
                    </ext:Column>
                    <ext:ImageCommandColumn ID="CommandColumnChange" runat="server" Width="70" Text=""
                            MenuDisabled="true" Sortable="false" Align="Center">
                            <Commands>
                                <ext:ImageCommand CommandName="Edit" Text="Edit">
                                    <ToolTip Text="Edit" />
                                </ext:ImageCommand>
                            </Commands>
                            <Listeners>
                                <Command Fn="CommandHandler">
                                </Command>
                            </Listeners>
                        </ext:ImageCommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                    <ext:CellSelectionModel ID="CellSelectionModel2" runat="server" Mode="Single" />
                </SelectionModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <%--<GetRowClass Fn="getRowClass" />--%>
                </ext:GridView>
            </View>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeChangeStatus"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0" />
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <br />

    </div>



</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
