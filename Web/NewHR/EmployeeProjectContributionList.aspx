﻿<%@ Page Title="Employee Project Contribution List." Language="C#" AutoEventWireup="true"
    CodeBehind="EmployeeProjectContributionList.aspx.cs" MasterPageFile="~/Master/NewDetails.Master"
    Inherits="Web.NewHR.EmployeeProjectContributionList" %>

<%@ Register Src="~/Controls/FooterCtl.ascx" TagName="FooterCtl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/HeaderCtl.ascx" TagName="HeaderCtl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript">
  
                  function processBefore(btnId) {
     var startDate =  <%=txtStartDate.ClientID%>.getValue();
     
      <%=hdnStartDate.ClientID%>.setValue(startDate);
      
}

    function searchList() {
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
           

           var prepareCommand = function (grid, toolbar, rowIndex, record) {
            if(record.data.Status ==-1) {
                toolbar.hide();
            }

        };

    </script>
    <ext:Hidden ID="hdnTimeSheetId" runat="server" />
    <ext:Hidden ID="hdnEmployeeID" runat="server" />
    <ext:Hidden ID="hdnStartDate" runat="server" />
    <ext:Hidden ID="hdnWeekNo" runat="server" />
    <%--  <ext:LinkButton ID="btnExport" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnExport_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    --%>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 id="header" runat="server">
                    Employee Project Contribution List</h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="alert alert-info">
            <table class="fieldTable">
                <tr>
                    <td style="width: 30px;">
                        Date
                    </td>
                    <td>
                    <%--    <ext:DateField Width="120px" ID="txtStartDate" runat="server" LabelSeparator="" Format="MM/dd/yyyy">
                        </ext:DateField>
--%>

                           <pr:CalendarExtControl Width="150px" ID="txtStartDate" runat="server" LabelSeparator="" />

                        <asp:RequiredFieldValidator Display="None" ID="val1" runat="server" ValidationGroup="LoadTimeSheet"
                            ControlToValidate="txtStartDate" ErrorMessage="Start Date is required." />
                    </td>
                    <td style="width: 10px;">
                    </td>
                    <td style="width: 60px; padding-left: 10px">
                        Employee
                    </td>
                    <td style="width: 165px;">
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbSearch" LabelWidth="70" runat="server" DisplayField="Name"
                            ValueField="EmployeeId" StoreID="storeSearch" TypeAhead="false" Width="180" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="width: 20px">
                    </td>
                    <td>
                        <ext:Button runat="server" Width="100" Height="30" ID="btnLoad" Cls="btn btn-default btn-sm btn-sect"
                            Text="Load" OnClientClick="searchList();">
                            <Listeners>
                                <Click Handler="valGroup = 'LoadTimeSheet'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td>
                        <ext:LinkButton ID="lnkExcelPrint" Icon="PageExcel" runat="server" Text="Export"
                            OnClick="btnExcelPrint_Click" Hidden="false" AutoPostBack="true">
                            <Listeners>
                                <Click Fn="processBefore" />
                            </Listeners>
                        </ext:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <table>
            <tr>
                <td>
                    <ext:GridPanel ID="gridTimeSheet" runat="server" Cls="itemgrid" Width="1200" Scroll="None">
                        <Store>
                            <ext:Store ID="Store3" runat="server" AutoLoad="true" PageSize="50">
                                <Proxy>
                                    <ext:AjaxProxy Json="true" Url="../Handler/EmployeeProjectContributionHandler.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="data" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <AutoLoadParams>
                                    <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                    <ext:Parameter Name="IsEmpView" Value="-1" Mode="Raw" />
                                </AutoLoadParams>
                                <Parameters>
                                    <ext:StoreParameter Name="EmployeeId" Value="#{cmbSearch}.getValue()" Mode="Raw"
                                        ApplyMode="Always" />
                                    <ext:StoreParameter Name="StartDate" Value="#{txtStartDate}.getValue()" Mode="Raw"
                                        ApplyMode="Always" />
                                </Parameters>
                                <Model>
                                    <ext:Model ID="Model1" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="EmployeeID" />
                                            <ext:ModelField Name="EmployeeName" />
                                            <ext:ModelField Name="DeptID" />
                                            <ext:ModelField Name="FundCode" />
                                            <ext:ModelField Name="BusinessUnit" />
                                            <ext:ModelField Name="ProjectName" />
                                            <ext:ModelField Name="ProjectNumber" />
                                            <ext:ModelField Name="Activity" />
                                            <ext:ModelField Name="SourceType" />
                                            <ext:ModelField Name="Categoty" />
                                            <ext:ModelField Name="Percentage" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel2" runat="server">
                            <Columns>
                                <ext:Column ID="colEId" runat="server" Text="EIN" Width="50" DataIndex="EmployeeID"
                                    Align="Center" Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column1" runat="server" Text="Staff Name" Width="180" DataIndex="EmployeeName"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column7" runat="server" Text="Dept ID" Width="100" DataIndex="DeptID"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column2" runat="server" Text="Fund Code" Width="100" DataIndex="FundCode"
                                    Align="Left" Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Align="Center" Text="Business Unit" Width="100"
                                    DataIndex="BusinessUnit" Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="Project Id" Width="100" DataIndex="ProjectName"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column5" runat="server" Text="Activity" Width="100" DataIndex="Activity"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column8" runat="server" Text="Source Type" Width="150" DataIndex="SourceType"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:Column ID="Column9" runat="server" Text="Category" Width="150" DataIndex="Categoty"
                                    Sortable="false" MenuDisabled="true">
                                </ext:Column>
                                <ext:NumberColumn ID="Column1N" runat="server" Text="Distribution" Width="150" DataIndex="Percentage"
                                    Sortable="false" MenuDisabled="true">
                                </ext:NumberColumn>
                            </Columns>
                        </ColumnModel>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="50" DisplayInfo="true"
                                DisplayMsg="Displaying Time Sheet {0} - {1} of {2}" EmptyMsg="No Records to display">
                            </ext:PagingToolbar>
                        </BottomBar>
                    </ext:GridPanel>
                </td>
            </tr>
        </table>
        <br />
    </div>
</asp:Content>
