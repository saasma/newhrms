﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Calendar;
using Web.Helper;
using Bll;
using System.Data;

namespace Web.NewHR
{
    public partial class EmployeeCountReport : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }

      

        public void Initialise()
        {
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.LevelAndDesignation).ToList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            this.storeLevel.DataBind();

            BindDataToGrid();


            btnLoad_Change(null, null);
            
        }



        public void btnLoad_Change(object sender, DirectEventArgs e)
        {

            int branchId = -1, depId = -1, levelId = -1, designId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designId = int.Parse(cmbDesignation.SelectedItem.Value);

            bool? retiredOnly = false;

            if (cmbRetType.SelectedItem != null && cmbRetType.SelectedItem.Value != null)
            {
                if (cmbRetType.SelectedItem.Equals("-1"))
                    retiredOnly = null;
                else
                    retiredOnly = bool.Parse(cmbRetType.SelectedItem.Value);
            }

            if (hdnFilterValue.Text == "Position-Branch")
            {
                gridBranchLevel.Show();
                gridEmployee.Hide();
                BindDataToGrid();
            }
            else
            {
                gridBranchLevel.Hide();
                gridEmployee.Show();

                gridEmployee.GetStore().DataSource =
                    EmployeeManager.GetEmployeeCount(hdnFilterValue.Text, branchId, depId, levelId, designId, retiredOnly).ToList();
                gridEmployee.GetStore().DataBind();
            }
        }


        public void btnExport_Click(object sender, EventArgs e)
        {

            int branchId = -1, depId = -1, levelId = -1, designId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designId = int.Parse(cmbDesignation.SelectedItem.Value);

            bool? retiredOnly = null;

            if (cmbRetType.SelectedItem.Equals("-1"))
                retiredOnly = null;
            else
                retiredOnly = bool.Parse(cmbRetType.SelectedItem.Value);

            List<Report_GetEmployeeCountResult> list =
                EmployeeManager.GetEmployeeCount(hdnFilterValue.Text, branchId, depId, levelId, designId,retiredOnly).ToList();

            string title = "";
         
            if (hdnFilterValue.Text.ToLower() == "thismonth")
            {
                title = "";
            }




            if (hdnFilterValue.Text == "Position-Branch")
            {
                DataTable dt = GetDataTableForExcel();
                WebHelper.ExportDtToExcel(dt, "Branch levevl List");
            }
            else
            {

                ExcelHelper.ExportToExcel("Employee Count", list,
                      new List<string>() { },
                         new List<String>() { },
                         new Dictionary<string, string>() { { "Total", "Count" } },
                         new List<string>() { }
                         , new Dictionary<string, string>() { }
                         , new List<string> { });
            }
        }



        private DataTable GetDataTableForExcel()
        {
            bool? retiredOnly = null;

            if (cmbRetType.SelectedItem != null && cmbRetType.SelectedItem.Equals("-1") == false)
            {
                retiredOnly = bool.Parse(cmbRetType.SelectedItem.Value);
            }

            List<Report_GetEmployeeCountResult> itemInSiteList =
                BLL.BaseBiz.PayrollDataContext.Report_GetEmployeeCount("Position-Branch", null, null, null, null, retiredOnly).ToList();
            //InventoryManager.GetItemBranchConsumptionReport
            //(from, to, uomType);

            // Create Columns for the Site 
            //Model SiteModel = (this.Store_ItemInSiteDetails.Model[0] as Model);
            //string columnId = "ItemInSite_{0}";
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            List<BLevel> levelList = NewPayrollManager.GetAllParentLevels();

            DataTable table = new DataTable();

            table.Columns.Add("Level", typeof(string));

            foreach (Branch s in branchList)
            {
                //string gridIndexId = string.Format(columnId, s.Name);
                //ModelField field = new ModelField(gridIndexId, ModelFieldType.String);
                //field.UseNull = true;
                //SiteModel.Fields.Add(field);
                //GridPanel_ItemInSite.ColumnModel.Columns.Add(new Column { Align = Alignment.Right, DataIndex = gridIndexId, Text = s.Name, Width = 110 });

                DataColumn colString = new DataColumn();

                colString.AllowDBNull = true;
                //  colString.ColumnName = NewMessage.GetTextValue(_fields.ServerMappingFields);// HttpContext.GetGlobalResourceObject("ResourceEnums", _fields.ServerMappingFields).ToString();
                colString.ColumnName = s.Name;
                colString.Caption = s.Name;
                colString.DataType = typeof(string);
                table.Columns.Add(colString);

                //DataColumn colStringValue = new DataColumn();

                //colStringValue.AllowDBNull = true;
                ////  colString.ColumnName = NewMessage.GetTextValue(_fields.ServerMappingFields);// HttpContext.GetGlobalResourceObject("ResourceEnums", _fields.ServerMappingFields).ToString();
                //colStringValue.ColumnName = s.Name + "Value";
                //colStringValue.Caption = s.Name + " Value";
                //colStringValue.DataType = typeof(string);
                //table.Columns.Add(colStringValue);
            }


            //object[] rowTotal = new object[(branchList.Count) + 1];


            //Dictionary<string, decimal> branchTotal = new Dictionary<string, decimal>();
            //Dictionary<string, decimal> branchTotalValue = new Dictionary<string, decimal>();

            object[] data = new object[levelList.Count];
            int row = 0;
            foreach (BLevel entity in levelList)
            {
                object[] rowData = new object[(branchList.Count) + 1]; //except site there are other more three column(item name,total,unit of measure)
                decimal totalQty = 0;
                decimal totalValue = 0;

                //rowData[0] = entity.Category;
                rowData[0] = entity.Name;
                int index = 1;

                //Value of Item in each Site
                int i = 0;
                foreach (Branch site in branchList)
                {
                    Report_GetEmployeeCountResult itemBranch = itemInSiteList.FirstOrDefault(x =>
                        x.Name == entity.Name && x.Branch == site.Name);

                    i++;
                    string strQty = itemBranch != null && itemBranch.Total != null ? itemBranch.Total.ToString() : "";

                    if (!string.IsNullOrEmpty(strQty))
                        totalQty += decimal.Parse(strQty);

                    decimal quantity = 0;
                    if (decimal.TryParse(strQty, out quantity))
                        rowData[index] = quantity;
                    else
                        rowData[index] = "-";


                    index += 1;

                    //if (branchTotalValue.ContainsKey(site.Name))
                    //    branchTotalValue[site.Name] += Convert.ToDecimal(itemBranch == null || itemBranch.Amount == null ? 0 : itemBranch.Amount.Value);
                    //else
                    //    branchTotalValue[site.Name] = Convert.ToDecimal(itemBranch == null || itemBranch.Amount == null ? 0 : itemBranch.Amount.Value);

                    //if (branchTotal.ContainsKey(site.Name))
                    //    branchTotal[site.Name] += Convert.ToDecimal(quantity);
                    //else
                    //    branchTotal[site.Name] = Convert.ToDecimal(quantity);

                }
                //Total Column
                //rowData[index++] = totalQty;
                //rowData[index++] = totalValue;

                //Unit of Measure column
                //rowData[index] = entity.UOMName;

                //Add a Row
                data[row++] = rowData;
            }

            // add Grand Total
            //Value of Item in each Site
            int totalIndex = 1;
            decimal grandTotal = 0;
            decimal grandTotalValue = 0;
            //foreach (Branch site in branchList)
            //{
            //    decimal value;
            //    if (branchTotal.ContainsKey(site.Name))
            //        value = branchTotal[site.Name];
            //    else
            //        value = 0;
            //    grandTotal += value;
            //    rowTotal[totalIndex++] = value;


            //    if (branchTotalValue.ContainsKey(site.Name))
            //        value = branchTotalValue[site.Name];
            //    else
            //        value = 0;
            //    grandTotalValue += value;
            //    rowTotal[totalIndex++] = value;
            //}
            //rowTotal[totalIndex++] = grandTotal;
            //rowTotal[totalIndex++] = grandTotalValue;
            //data[row++] = rowTotal;


            foreach (object[] obj in data)
            {
                table.Rows.Add(obj);
            }
            return table;
        }






  

        private void BindDataToGrid()
        {

            bool? retiredOnly = null;

            if (cmbRetType.SelectedItem != null && cmbRetType.SelectedItem.Value != null && cmbRetType.SelectedItem.Equals("-1") == false)
            {
                retiredOnly = bool.Parse(cmbRetType.SelectedItem.Value);
            }

            List<Report_GetEmployeeCountResult> itemInSiteList =
              BLL.BaseBiz.PayrollDataContext.Report_GetEmployeeCount("Position-Branch", null, null, null, null,retiredOnly).ToList();


            // Create Columns for the Site 
            Model SiteModel = (this.Store_ItemInSiteDetails.Model[0] as Model);
            string columnId = "ItemInSite_{0}";
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            List<BLevel> levelList = NewPayrollManager.GetAllParentLevels();

            foreach (Branch s in branchList)
            {
                string gridIndexId = string.Format(columnId, s.Name);
                ModelField field = new ModelField(gridIndexId, ModelFieldType.String);
                field.UseNull = true;
                SiteModel.Fields.Add(field);
                gridBranchLevel.ColumnModel.Columns.Add(new Column { Align = Alignment.Right, DataIndex = gridIndexId, Text = s.Name, Width = 110 });

                //string gridIndexId1 = string.Format(columnId, s.Name + "Value");
                //ModelField field1 = new ModelField(gridIndexId1, ModelFieldType.String);
                //field1.UseNull = true;
                //SiteModel.Fields.Add(field1);
                //gridBranchLevel.ColumnModel.Columns.Add(new Column { Align = Alignment.Right, DataIndex = gridIndexId1, Text = s.Name + " Value", Width = 110 });

            }

            //Add Total Column
            //SiteModel.Fields.Add(new ModelField { Name = "Total", Type = ModelFieldType.String });
            //GridPanel_ItemInSite.ColumnModel.Columns.Add(new Column { Width= new Unit(100), Align=Alignment.Right, DataIndex = "Total", Text = "Total" });

            //SiteModel.Fields.Add(new ModelField { Name = "Total Value", Type = ModelFieldType.String });
            //GridPanel_ItemInSite.ColumnModel.Columns.Add(new Column { Width = new Unit(100), Align = Alignment.Right, DataIndex = "TotalValue", Text = "Total Value" });

         

            //object[] rowTotal = new object[(branchList.Count ) + 1];
            //rowTotal[0] = "Total";

            //Dictionary<string, decimal> branchTotal = new Dictionary<string, decimal>();
            //Dictionary<string, decimal> branchTotalValue = new Dictionary<string, decimal>();

            object[] data = new object[levelList.Count];
            int row = 0;
            foreach (BLevel entity in levelList)
            {
                object[] rowData = new object[(branchList.Count) + 1]; //except site there are other more three column(item name,total,unit of measure)
                decimal totalQty = 0;
                decimal totalValue = 0;

                rowData[0] = entity.Name;

                int index = 1;

                //Value of Item in each Site
                int i = 0;
                foreach (Branch site in branchList)
                {

                    Report_GetEmployeeCountResult itemBranch = itemInSiteList.FirstOrDefault(x =>
                        x.Name == entity.Name && x.Branch == site.Name);

                    i++;
                    string strQty = itemBranch != null && itemBranch.Total != null ? itemBranch.Total.ToString() : "";

                    if (!string.IsNullOrEmpty(strQty))
                        totalQty += decimal.Parse(strQty);

                    decimal quantity = 0;
                    if (decimal.TryParse(strQty, out quantity))
                        rowData[index] = quantity;
                    else
                        rowData[index] = "-";

                  
                    index += 1;

                    //if (branchTotal.ContainsKey(site.Name))
                    //    branchTotal[site.Name] += Convert.ToDecimal(quantity);
                    //else
                    //    branchTotal[site.Name] = Convert.ToDecimal(quantity);

                    //if (branchTotalValue.ContainsKey(site.Name))
                    //    branchTotalValue[site.Name] += Convert.ToDecimal((itemBranch == null || itemBranch.Amount == null ? 0 : itemBranch.Amount));
                    //else
                    //    branchTotalValue[site.Name] = Convert.ToDecimal((itemBranch == null || itemBranch.Amount == null ? 0 : itemBranch.Amount));

                }
                //Total Column
                //rowData[index++] = totalQty;
                //rowData[index++] = totalValue;

                //Unit of Measure column
                

                //Add a Row
                data[row++] = rowData;
            }

            // add Grand Total
            //Value of Item in each Site
            int totalIndex = 1;
            decimal grandTotal = 0;
            decimal grandTotalValue = 0;
            //foreach (Branch site in branchList)
            //{
            //    decimal value;
            //    if (branchTotal.ContainsKey(site.Name))
            //        value = branchTotal[site.Name];
            //    else
            //        value = 0;
            //    grandTotal += value;
            //    rowTotal[totalIndex++] = value;


            //    if (branchTotalValue.ContainsKey(site.Name))
            //        value = branchTotalValue[site.Name];
            //    else
            //        value = 0;
            //    grandTotalValue += value;
            //    rowTotal[totalIndex++] = value;
            //}
            //rowTotal[totalIndex++] = grandTotal;
            //rowTotal[totalIndex++] = grandTotalValue;
            //data[row++] = rowTotal;

            Store_ItemInSiteDetails.DataSource = data;
            Store_ItemInSiteDetails.DataBind();
        }

        protected void ExportExcel(object sender, EventArgs e)
        {
            DataTable dt = GetDataTableForExcel();
            //ExcelHelper.ExportDtToExcel(dt, "Branch levevl List");
        }

    }
}