﻿<%@ Page Title="Position Grade Setting" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="PositionGradeManager.aspx.cs" Inherits="Web.NewHR.PositionGradeManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
      var CommandHandler = function(command, record){
            <%=isSubLevel.ClientID %>.setValue('false');
            <%= hiddenValue.ClientID %>.setValue(record.data.LevelId);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
            var CommandHandlerSubLevel = function(command, record){
            <%=isSubLevel.ClientID %>.setValue('true');
            <%= hiddenValue.ClientID %>.setValue(record.data.LevelId);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
        var PositionCommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.DesignationId);
                if(command=="Edit")
                {
                    <%= btnEditPosition.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDeletePosition.ClientID %>.fireEvent('click');
                }

             }
        var amRenderere = function(value)
        {
            if(value == null || value==false)  
                return "";
            return "Yes";
        }

         var notifyDrop = function(ddSource, e, data) {
            var index = ddSource.grid.getView().findRowIndex(e.target),
                store = ddSource.grid.getStore();
            store.remove(ddSource.dragData.selections);
            index = index > store.getCount() ? store.getCount() : index;
            store.insert(index, ddSource.dragData.selections);
            return true;
        };

        function process(e1,e2,e3,e4,e5)
        {
            var sourceId = e2.records[0].data.DesignationId;
            <%=hdnSource.ClientID %>.setValue(sourceId);
            var destinationId = e3.data.DesignationId;
            <%=hdnDest.ClientID %>.setValue(destinationId);
            <%=hdnDropMode.ClientID %>.setValue(e4);
//             Ext.net.Notification.show({title:e2.records[0].data.Name,html: e3.data.Name});
            <%=btnChangeDesignationOrder.ClientID %>.fireEvent('click');
        }


        function renderClericalType(e1,e2,record)
        {           
            if(record.data.ClericalType == 1)
            {
                return 'Clerical';
            }
            else if(record.data.ClericalType == 2) 
            {
                return 'Non Clerical';
            }
        }

        function refreshWindow(popupWindow)
        {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

            <%=btnLoadDesignations.ClientID %>.fireEvent('click');
            //alert("ehllo");
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="hdnDropMode" />
    <ext:Hidden runat="server" ID="hdnSource" />
    <ext:Hidden runat="server" ID="hdnDest" />
    <ext:Hidden runat="server" ID="isSubLevel" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnChangeDesignationOrder" runat="server">
        <DirectEvents>
            <Click OnEvent="btnChangeDesignationOrder_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel" runat="server">
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteLevel_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the level?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:LinkButton runat="server" Hidden="true" ID="btnEditPosition" runat="server">
        <DirectEvents>
            <Click OnEvent="btnEditPosition_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDeletePosition" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeletePosition_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Position?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnLoadDesignations" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnLoadDesignations_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Levels, Positions and Designations
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div style="width: 710px">
            <div>
                <!-- panel-btns -->
                <h4 class="panel-title sectionHeading" style="margin-left: 20px; margin-bottom: -20px;
                    width: 770px;">
                    <i></i>Levels and Positions in the organization</h4>
            </div>
            <!-- panel-heading -->
            <div class="panel-body" style="width: 810px">
                <ext:GridPanel ID="GridLevels" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeLevel" runat="server" OnReadData="LevelData_Refresh" PageSize="20">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="LevelId">
                                    <Fields>
                                        <ext:ModelField Name="LevelId" Type="String" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Code" Type="string" />
                                        <ext:ModelField Name="Order" Type="string" />
                                        <ext:ModelField Name="GroupName" Type="string" />
                                        <ext:ModelField Name="IsAM" Type="Boolean" />
                                        <ext:ModelField Name="ClericalType" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column8" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Group" Align="Left" DataIndex="GroupName" />
                            <ext:Column ID="Column5" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Level/Position" Align="Left" DataIndex="Name" />
                            <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Code"
                                Align="Left" DataIndex="Code">
                            </ext:Column>
                            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Order"
                                Align="Center" DataIndex="Order">
                            </ext:Column>
                            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Clerical Type"
                                Align="Left" DataIndex="ClericalType">
                                <Renderer Fn="renderClericalType" />
                            </ext:Column>
                            <ext:CommandColumn TdCls="centerButtons" ID="CommandColumn2" runat="server" Text="Actions">
                                <Commands>
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                        CommandName="Edit" />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <%--<View>
                            <ext:GridView ID="GridView1" runat="server">
                                <Plugins>
                                    <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragText="Drag and drop to reorder level">
                                    </ext:GridDragDrop>
                                </Plugins>
                            </ext:GridView>
                        </View>--%>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                    <BottomBar>
                        <ext:PagingToolbar ID="paging1" runat="server">
                        </ext:PagingToolbar>
                    </BottomBar>
                </ext:GridPanel>
                <div class="buttonBlockSection">
                    <table>
                        <tr>
                            <td>
                                <ext:Button runat="server" ID="btnAddLevel" Cls="btn btn-primary btn-sm btn-sect"
                                    Text="<i></i>Add Level and Position">
                                    <DirectEvents>
                                        <Click OnEvent="btnAddLevel_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                            </td>
                            <td style="padding-left: 410px;">
                                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                                    Text="<i></i>Export To Excel">
                                </ext:Button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div>
            <div>
                <!-- panel-btns -->
                <h4 class="panel-title sectionHeading" style="margin-left: 20px; margin-bottom: -20px;
                    width: 970px;">
                    <i></i>Designations</h4>
            </div>
            <!-- panel-heading -->
            <div class="panel-body" style="width: 1010px">
                <ext:GridPanel ID="GridPositionDesignations" runat="server" Cls="itemgrid" EnableDragDrop="true"
                    DDGroup="ddGroup">
                    <Store>
                        <ext:Store ID="storePosition" runat="server" OnReadData="PositionData_Refresh" PageSize="20">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="DesignationId">
                                    <Fields>
                                        <ext:ModelField Name="DesignationId" Type="String" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Code" Type="string" />
                                        <ext:ModelField Name="Order" Type="string" />
                                        <ext:ModelField Name="LevelName" Type="string" />
                                        <ext:ModelField Name="Category" Type="string" />
                                        <ext:ModelField Name="Group" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column1" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Designation" Align="Left" DataIndex="Name" />
                            <ext:Column ID="Column9" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Category" Align="Left" DataIndex="Category" />
                            <ext:Column ID="Column10" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Group" Align="Left" DataIndex="Group" />
                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Code"
                                Align="Center" DataIndex="Code" />
                            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Order"
                                Align="Center" DataIndex="Order">
                            </ext:Column>
                            <ext:Column ID="Column4" Sortable="false" Width="300" MenuDisabled="true" runat="server"
                                Text="Level/Position" Align="Left" DataIndex="LevelName">
                            </ext:Column>
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="">
                                <Commands>
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                        CommandName="Edit" />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="PositionCommandHandler(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                    </SelectionModel>
                    <View>
                        <ext:GridView ID="GridView1" runat="server">
                            <Plugins>
                                <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                            </Plugins>
                            <Listeners>
                                <BeforeItemClick Fn="process" />
                                <Drop Fn="process" />
                                <%-- <AfterRender Handler="this.plugins[0].dragZone.getDragText = getDragDropText;" />--%>
                                <%--  <Drop Handler="var dropOn = overModel ? ' ' + dropPosition + ' ' + overModel.get('Name') : ' on empty view'; 
                                               Ext.net.Notification.show({title:'Drag from right to left', html:'Dropped ' + data.records[0].get('Name') + dropOn});" />--%>
                            </Listeners>
                        </ext:GridView>
                    </View>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolbar1" runat="server">
                        </ext:PagingToolbar>
                    </BottomBar>
                </ext:GridPanel>
                <%--<ext:DropTarget ID="DropTarget1" runat="server" Target="={GridPositionDesignations.view.scroller.dom}"
                    Group="ddGroup">
                    <NotifyDrop Fn="notifyDrop" />
                </ext:DropTarget>--%>
                <div class="buttonBlockSection">
                    <table>
                        <tr>
                            <td>
                                <ext:Button runat="server" ID="btnAddPosition" Cls="btn btn-primary btn-sm btn-sect"
                                    Text="<i></i>Add Designation">
                                    <DirectEvents>
                                        <Click OnEvent="btnAddPosition_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                            </td>
                            <td style="padding-left: 640px;">
                                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExportDesignation_Click"
                                    ID="btnExportDesignation" Text="<i></i>Export To Excel">
                                </ext:Button>
                            </td>
                            <td style='    padding-left: 28px;'>
                                <asp:LinkButton ID="LinkButton3" runat="server" Text="Excel import" OnClientClick="taxImportPopup();return false;"
                                    CssClass=" excel marginRight tiptip" Style="float: left;" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <!-- col-sm-6 -->
        <div class="innerLR">
            <ext:Window ID="WindowLevel" runat="server" Title="Employee Level/Position" Icon="Application"
                Width="420" Height="300" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbGroup" Width="200px" runat="server" ValueField="LevelGroupId"
                                    DisplayField="Name" FieldLabel="Group" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store1" runat="server">
                                            <Model>
                                                <ext:Model ID="Model2" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelGroupId" Type="String" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="valcmbFaculty" runat="server" ValidationGroup="SaveUpdateLevel"
                                    ControlToValidate="cmbGroup" ErrorMessage="Please select a Group name." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:NumberField ID="txtLevelOrder" LabelSeparator="" MinValue="1" runat="server"
                                    FieldLabel="Order" LabelAlign="Top">
                                </ext:NumberField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="txtLevelOrder" ErrorMessage="Please define the Level order." />
                            </td>
                            <td>
                                <ext:ComboBox ID="cmbClericalType" runat="server" ValueField="Value" DisplayField="Text"
                                    FieldLabel="Clerical Type" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Text="Clerical" Value="1" />
                                        <ext:ListItem Text="Non Clerical" Value="2" />
                                    </Items>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="rfvClericalType" runat="server" ValidationGroup="SaveUpdateLevel"
                                    ControlToValidate="cmbClericalType" ErrorMessage="Please select a Clerical Type." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:TextField ID="txtLevelName" Width="200px" LabelSeparator="" MinValue="0" runat="server"
                                    FieldLabel="Level/Position Name" LabelAlign="Top">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="txtLevelName" ErrorMessage="Please type a Level name." />
                            </td>
                            <td>
                                <ext:TextField ID="txtLevelCode" LabelSeparator="" MinValue="0" runat="server" FieldLabel="Code"
                                    LabelAlign="Top">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator7" runat="server"
                                    ValidationGroup="SaveUpdateLevel" ControlToValidate="txtLevelCode" ErrorMessage="Please type a Level code." />
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" ID="btnLevelSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save"
                                        runat="server">
                                        <DirectEvents>
                                            <Click OnEvent="btnLevelSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdateLevel'; if(CheckValidation()) return this.disable(); else return false;">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{WindowLevel}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
            <ext:Window ID="WindowSubLevel" runat="server" Title="Employee Sub-Level" Icon="Application"
                Width="420" Height="350" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbLevel2" Width="200px" runat="server" ValueField="LevelId" DisplayField="Name"
                                    FieldLabel="Parent Level" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store8" runat="server">
                                            <Model>
                                                <ext:Model ID="Model8" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelId" Type="String" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator12" runat="server"
                                    ValidationGroup="SaveUpdateSubLevel" ControlToValidate="cmbLevel2" ErrorMessage="Please select a parent level." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:NumberField ID="txtLevelOrder2" LabelSeparator="" MinValue="1" runat="server"
                                    FieldLabel="Order" LabelAlign="Top">
                                </ext:NumberField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator9" runat="server"
                                    ValidationGroup="SaveUpdateSubLevel" ControlToValidate="txtLevelOrder2" ErrorMessage="Please define the Level order." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:TextField ID="txtLevelName2" Width="200px" LabelSeparator="" MinValue="0" runat="server"
                                    FieldLabel="Sub-Level Name" LabelAlign="Top">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator10" runat="server"
                                    ValidationGroup="SaveUpdateSubLevel" ControlToValidate="txtLevelName2" ErrorMessage="Please type a Level name." />
                            </td>
                            <td>
                                <ext:TextField ID="txtLevelCode2" LabelSeparator="" MinValue="0" runat="server" FieldLabel="Level Code"
                                    LabelAlign="Top">
                                </ext:TextField>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator11" runat="server"
                                    ValidationGroup="SaveUpdateSubLevel" ControlToValidate="txtLevelCode2" ErrorMessage="Please type a Level code." />
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" ID="btnSubLevelSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save"
                                        runat="server">
                                        <DirectEvents>
                                            <Click OnEvent="btnSubLevelSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdateSubLevel'; if(CheckValidation()) return this.disable(); else return false;">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton2" Text="<i></i>Cancel"
                                        runat="server">
                                        <Listeners>
                                            <Click Handler="#{WindowSubLevel}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
            <ext:Window ID="WindowPosition" runat="server" Title="Employee Designation" Icon="Application"
                Width="480" Height="350" BodyPadding="5" Hidden="true" Modal="true">
                <Content>
                    <table class="fieldTable">
                        <tr>
                            <td>
                                <ext:TextField ID="txtPositionName" FieldLabel="Name" LabelAlign="Top" runat="server"
                                    Width="200px" />
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator6" runat="server"
                                    ValidationGroup="SaveUpdatePosition" ControlToValidate="txtPositionName" ErrorMessage="Please type the Designation name." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbLevel" Width="200px" runat="server" ValueField="LevelId" DisplayField="Name"
                                    FieldLabel="Level/Position" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store4" runat="server">
                                            <Model>
                                                <ext:Model ID="Model3" runat="server" IDProperty="LevelId">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelId" Type="String" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Listeners>
                                        <Focus Handler="this.lastQuery='';this.store.clearFilter();" />
                                    </Listeners>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                    ValidationGroup="SaveUpdatePosition" ControlToValidate="cmbLevel" ErrorMessage="Please select a Level/Position." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ext:TextField ID="txtPositionCode" FieldLabel="Code" LabelAlign="Top" runat="server"
                                    Width="200px">
                                </ext:TextField>
                            </td>
                            <%--  <td>
                            <ext:NumberField FieldLabel="Order" MinValue="0" LabelAlign="Top" ID="txtPositionOrder"
                                runat="server" Width="180px" />
                        </td>--%>
                        </tr>
                        <tr>
                            <td>
                                <ext:ComboBox ID="cmbPositionCategory" Width="200px" runat="server" ValueField="Value"
                                    DisplayField="Text" FieldLabel="Category" LabelAlign="top" LabelSeparator=""
                                    ForceSelection="true" QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store5" runat="server">
                                            <Model>
                                                <ext:Model ID="Model5" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="Value" Type="String" />
                                                        <ext:ModelField Name="Text" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                                    ValidationGroup="SaveUpdatePosition" ControlToValidate="cmbPositionCategory"
                                    ErrorMessage="Please select a Category." />
                            </td>
                            <td>
                                <ext:ComboBox ID="cmbPositionGroup" Width="200px" runat="server" ValueField="Value"
                                    DisplayField="Text" FieldLabel="Group" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                    QueryMode="Local">
                                    <Store>
                                        <ext:Store ID="Store6" runat="server">
                                            <Model>
                                                <ext:Model ID="Model6" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="Value" Type="String" />
                                                        <ext:ModelField Name="Text" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server"
                                    ValidationGroup="SaveUpdatePosition" ControlToValidate="cmbPositionGroup" ErrorMessage="Please select a Group." />
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2">
                                <div class="popupButtonDiv">
                                    <ext:Button runat="server" ID="btnPositionSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save"
                                        runat="server">
                                        <DirectEvents>
                                            <Click OnEvent="btnPositionSaveUpdate_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveUpdatePosition'; if(CheckValidation()) return this.disable(); else return false;">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                    <div class="btnFlatOr">
                                        or</div>
                                    <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton4" Text="<i></i>Cancel"
                                        runat="server">
                                        <Listeners>
                                            <Click Handler="#{WindowPosition}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </ext:Window>
        </div>
    </div>
</asp:Content>
