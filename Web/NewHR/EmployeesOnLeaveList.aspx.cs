﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Base;
using BLL;
using Utils;


namespace Web.NewHR
{
    public partial class EmployeesOnLeaveList : BasePage
    {
        public void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                DateField1.SelectedDate = DateTime.Now;

                Load1_Click(null, null);
            }
        }

        public void LoadLeaveGrid2(DateTime GivenDay)
        {


            if (TabPanel1.ActiveTabIndex == 0)
            {
                storeOnLeaveToday.DataSource = DashboardManager.GetDayLeaveList(GivenDay);
                storeOnLeaveToday.DataBind();
            }

            if (TabPanel1.ActiveTabIndex == 1)
            {
                List<DashboardOnLeaveThisWeekResult> list1 = DashboardManager.getEmpOnLeaveThisWeek(GivenDay);
                storeOnLeaveThisWeek.DataSource = list1.OrderBy(x => x.BranchName).ThenBy(x => x.EmployeeName).ToList();
                storeOnLeaveThisWeek.DataBind();
            }

            if (TabPanel1.ActiveTabIndex == 2)
            {
                List<DashboardOnLeaveNextWeekResult> list2 = DashboardManager.getEmpOnLeaveNextMonth(GivenDay);
                storeOnLeaveNextWeek.DataSource = list2.OrderBy(x => x.BranchName).ThenBy(x => x.EmployeeName).ToList();
                storeOnLeaveNextWeek.DataBind();
            }

            List<DashboardOnLeaveThisMonthResult> list = DashboardManager.getEmpOnLeaveThisMonth(GivenDay);
            list = list.OrderBy(x => x.BranchName).ThenBy(x => x.EmployeeName).Where(y => y.DateOnEng.Date != GivenDay.Date).ToList();

            if (TabPanel1.ActiveTabIndex == 3)
            {
                storeOnLeaveThisMonth.DataSource = list;
                storeOnLeaveThisMonth.DataBind();
            }

         



        }

        protected void Tab_Change(object sender, DirectEventArgs e)
        {
            Load1_Click(null, null);
        }

        protected void Load1_Click(object sender, DirectEventArgs e)
        {
            LoadLeaveGrid2(DateField1.SelectedDate);
        }

    }
}