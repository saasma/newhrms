﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;
using System.Collections;

namespace Web.NewHR
{
    public partial class EmpFamilyList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "EmpFamilyImport", "../ExcelWindow/EmployeeFamilyImport.aspx", 450, 500);
        }

        private void Initialise()
        {
            storeBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeBranch.DataBind();

            storeLevel.DataSource = NewPayrollManager.GetAllParentLevelList().OrderBy(x => x.Name).ToList();
            storeLevel.DataBind();

            cmbRelation.Store[0].DataSource = CommonManager.GetRelationList();
            cmbRelation.Store[0].DataBind();

            if (IsPostBack == false)
            {
                List<FixedValueFamilyRelation> statusList = CommonManager.GetRelationList();
                //statusList.RemoveAt(0);
                foreach (var item in statusList)
                {
                    multiRelation.AddItems(item.Name, item.ID.ToString());
                }

            }
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int total = 0, pageSize = 0, employeeId = -1, branchId = -1, levelId = -1, relationId = -1;
            string employeeName = cmbSearch.Text;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            if (cmbRelation.SelectedItem != null && cmbRelation.SelectedItem.Value != null)
                relationId = int.Parse(cmbRelation.SelectedItem.Value);


            JobStatus statues = new JobStatus();
            List<KeyValue> statusList = statues.GetMembers();

            List<FixedValueFamilyRelation> RelList = CommonManager.GetRelationList();
            string[] RelationTexts = multiRelation.Text.Split(new char[] { ',' });
            string RelationIDTexts = "";
            foreach (string item in RelationTexts)
            {
                string text = item.ToString().ToLower().Trim();
                FixedValueFamilyRelation status = RelList.FirstOrDefault(x => x.Name.ToString().ToLower().Trim() == text);
                if (status != null)
                {
                    if (RelationIDTexts == "")
                        RelationIDTexts = status.ID.ToString();
                    else
                        RelationIDTexts += "," + status.ID;
                }
            }

            List<GetEmployeeFamilyMemberListResult> list = NewHRManager.GetEmployeeFamilyMembers(e.Page - 1, e.Limit, employeeId, employeeName, branchId, levelId, relationId, RelationIDTexts);

            //-- start filtering -----------------------------------------------------------
            FilterHeaderConditions fhc = new FilterHeaderConditions(e.Parameters["filterheader"]);

            foreach (FilterHeaderCondition condition in fhc.Conditions)
            {
                string dataIndex = condition.DataIndex;
                FilterType type = condition.Type;
                string op = condition.Operator;
                object value = null;

                switch (condition.Type)
                {
                    case FilterType.Boolean:
                        value = condition.Value<bool>();
                        break;

                    case FilterType.Date:
                        switch (condition.Operator)
                        {
                            case "=":
                                value = condition.Value<DateTime>();
                                break;

                            case "compare":
                                value = FilterHeaderComparator<DateTime>.Parse(condition.JsonValue);
                                break;
                        }
                        break;
                    case FilterType.String:
                        value = condition.Value<string>();
                        break;
                    case FilterType.Numeric:
                        value = condition.Value<int>();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                list.RemoveAll(item =>
                {
                    object oValue = item.GetType().GetProperty(dataIndex).GetValue(item, null);
                    string matchValue = null;
                    string itemValue = null;

                    if (type == FilterType.String)
                    {
                        matchValue = (string)value;

                        if (dataIndex == "EmployeeId")
                            itemValue = oValue.ToString();
                        else
                            itemValue = oValue as string;

                        matchValue = matchValue.ToLower();
                        itemValue = (itemValue == null ? "" : itemValue.ToLower());
                    }

                    switch (op)
                    {
                        case "=":
                            return oValue == null || !oValue.Equals(value);
                        case "compare":
                            return !((IEquatable<IComparable>)value).Equals((IComparable)oValue);
                        case "+":
                            return itemValue == null || !itemValue.Contains(matchValue); //return itemValue == null || !itemValue.StartsWith(matchValue);
                        case "-":
                            return itemValue == null || !itemValue.EndsWith(matchValue);
                        case "!":
                            return itemValue == null || itemValue.IndexOf(matchValue) >= 0;
                        case "*":
                            return itemValue == null || itemValue.IndexOf(matchValue) < 0;
                        default:
                            throw new Exception("Not supported operator");
                    }
                });
            }
            //-- end filtering ------------------------------------------------------------

            //-- start sorting ------------------------------------------------------------

            if (e.Sort.Length > 0)
            {
                list.Sort(delegate (GetEmployeeFamilyMemberListResult obj1, GetEmployeeFamilyMemberListResult obj2)
                {
                    object a;
                    object b;

                    int direction = e.Sort[0].Direction == Ext.Net.SortDirection.DESC ? -1 : 1;

                    a = obj1.GetType().GetProperty(e.Sort[0].Property).GetValue(obj1, null);
                    b = obj2.GetType().GetProperty(e.Sort[0].Property).GetValue(obj2, null);
                    return CaseInsensitiveComparer.Default.Compare(a, b) * direction;
                });
            }

            //Session["ExportEmpFamily"] = list;

            storeFamilyMem.DataSource = list;

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            e.Total = total;
        }

        protected Field OnCreateFilterableField(object sender, ColumnBase column, Field defaultField)
        {
            ((TextField)defaultField).Icon = Icon.Magnifier;

            return defaultField;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            //if (Session["ExportEmpFamily"] == null)
            //{
            //    NewMessage.ShowWarningMessage("Please reload the data for export.");
            //    return;
            //}

            //List<GetEmployeeFamilyMemberListResult> list = (List<GetEmployeeFamilyMemberListResult>)Session["ExportEmpFamily"];

            //Session["ExportEmpFamily"] = null;

            int employeeId = -1, branchId = -1, levelId = -1, relationId = -1;
            string employeeName = cmbSearch.Text;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            if (cmbRelation.SelectedItem != null && cmbRelation.SelectedItem.Value != null)
                relationId = int.Parse(cmbRelation.SelectedItem.Value);


            JobStatus statues = new JobStatus();
            List<KeyValue> statusList = statues.GetMembers();

            List<FixedValueFamilyRelation> RelList = CommonManager.GetRelationList();
            string[] RelationTexts = multiRelation.Text.Split(new char[] { ',' });
            string RelationIDTexts = "";
            foreach (string item in RelationTexts)
            {
                string text = item.ToString().ToLower().Trim();
                FixedValueFamilyRelation status = RelList.FirstOrDefault(x => x.Name.ToString().ToLower().Trim() == text);
                if (status != null)
                {
                    if (RelationIDTexts == "")
                        RelationIDTexts = status.ID.ToString();
                    else
                        RelationIDTexts += "," + status.ID;
                }
            }

            List<GetEmployeeFamilyMemberListResult> list = NewHRManager.GetEmployeeFamilyMembers(0, 999999, employeeId, employeeName, branchId, levelId, relationId, RelationIDTexts);




            string template = ("~/App_Data/ExcelTemplate/EmpFamilyImport.xlsx");

            List<FixedValueFamilyRelation> listFamilyRelations = CommonManager.GetRelationList().ToList();
            List<HrFamilyOccupation> occupations = ListManager.GetAllOccuations().ToList();
            List<HBloodGroup> BloodGroup = CommonManager.GetAllBloodGroup().ToList();
            List<CitizenNationality> NationalityList = CommonManager.GetCitizenshipNationaliyList().ToList();
            List<DocumentDocumentType> DocumentTypeList = CommonManager.GetDocumentDocumentTypeList().ToList();

            ExcelGenerator.ExportEmployeeFamilyLists(template, list, listFamilyRelations, occupations, BloodGroup, NationalityList, DocumentTypeList);

            //List<string> hiddenColumnList = new List<string>();
            //hiddenColumnList = new List<string> { "TotalRows", "RowNumber", "FamilyId", "DocumentIssueDateEng" };

            //Bll.ExcelHelper.ExportToExcel("Employee Family Members", list,
            //       hiddenColumnList,
            //   new List<String>() { },
            //   new Dictionary<string, string>() { {"EmployeeId","EIN"}, {"IdCardNo","INO" }, {"BloodGroup","Blood Group" }, {"DocumentIdType","Document Type" },
            //       { "DocumentIssueDate","Doc. Issue Date" }, {"DocumentIssuePlace","Doc. Issue Place"},
            //        {"MemberName","Member Name"}, {"DateOfBirth","Date Of Birth"}, { "IsDependent", "Dependent" }, {"AgeOnSpecifiedSPDate", "Age on specified date"},
            //        {"ContactNumber", "Contact Number"} },
            //   new List<string>() { }
            //   , new List<string> { }
            //   , new List<string> { }
            //   , new Dictionary<string, string>() { }
            //   , new List<string> { "Name", "EmployeeId", "IdCardNo", "MemberName", "Relation", "DateOfBirth", "IsDependent", "Occupation", "AgeOnSpecifiedSPDate", "ContactNumber" });



        }


    }
}