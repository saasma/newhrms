﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.NewHR
{
    public partial class GroupManager : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            LoadGroups();
        }
        private void LoadGroups()
        {
            GridLevels.Store[0].DataSource = NewPayrollManager.GetLevelGroup();
            GridLevels.Store[0].DataBind();
        }

        public void ClearGroupFields()
        {
            hiddenValue.Text = string.Empty;
            txtLevelName.Text = string.Empty;
            txtLevelOrder.Number = 0;
        }

        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            ClearGroupFields();
            WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            var levelId = int.Parse(hiddenValue.Text.Trim());
            var status = NewPayrollManager.DeleteGroup(levelId);
            if (status.IsSuccess)
            {
                LoadGroups();
                NewMessage.ShowNormalMessage("Group deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            var levelId = int.Parse(hiddenValue.Text.Trim());
            var entity = NewPayrollManager.GetGroupById(levelId);
            WindowLevel.Show();

            txtLevelName.Text = entity.Name;
            txtLevelOrder.Number = entity.Order;
        }

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                var entity = new BLevelGroup();
                var isInsert = true;
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    isInsert = false;
                    entity.LevelGroupId = int.Parse(hiddenValue.Text.Trim());
                }
                entity.Name = txtLevelName.Text.Trim();
                entity.Order = (int)txtLevelOrder.Number;

                bool IsOrderExits = NewPayrollManager.IsGroupOrderExists(entity.LevelGroupId,entity.Order);
                if (IsOrderExits)
                {
                    NewMessage.ShowNormalMessage("Order already exists.");
                    return;
                }

                var status = NewPayrollManager.InsertUpdateGroup(entity, isInsert);

                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadGroups();
                    NewMessage.ShowNormalMessage("Group saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }
    }
}
