﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using Utils.Calendar;
using BLL.BO;


namespace Web.NewHR
{
    public partial class EmpDutySchedule : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
                Initialise();
        }

        private void Initialise()
        {
            List<TextValue> list = new List<TextValue>();
            int year = DateTime.Now.Year;
            list.Add(new TextValue() { Text = (year - 2).ToString(), Value = (year - 2).ToString() });
            list.Add(new TextValue() { Text = (year - 1).ToString(), Value = (year - 1).ToString() });
            list.Add(new TextValue() { Text = (year).ToString(), Value = (year).ToString() });
            list.Add(new TextValue() { Text = (year + 1).ToString(), Value = (year + 1).ToString() });
            list.Add(new TextValue() { Text = (year + 2).ToString(), Value = (year + 2).ToString() });
            list.Add(new TextValue() { Text = (year + 3).ToString(), Value = (year + 3).ToString() });

            storeYear.DataSource = list;
            storeYear.DataBind();

            btnLoad_Click(btnThisYear, null);
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            DateTime fromDate, toDate;
            int daysInMonth = 0;
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;

            CustomDate cdFromDate= null, cdToDate=null;

            if (sender == btnThisMonth)
            {
                cdFromDate = CustomDate.GetCustomDateFromString(string.Format("{0}/{1}/{2}", year, month, 1), true);
                daysInMonth = DateHelper.GetTotalDaysInTheMonth(year, month, true);
                cdToDate = CustomDate.GetCustomDateFromString(string.Format("{0}/{1}/{2}", year, month, daysInMonth), true);

                dfDetails.Text = string.Format("From {0} {1}, {2} to {3} {4}, {5}", DateHelper.GetMonthName(month, true), 1, year,
                    DateHelper.GetMonthName(month, true), daysInMonth, year);

            }
            else if (sender == btnNextMonth)
            {
                if (month == 12)
                {
                    month = 1;
                    year++;
                }
                else
                    month++;

                cdFromDate = CustomDate.GetCustomDateFromString(string.Format("{0}/{1}/{2}", year, month, 1), true);
                daysInMonth = DateHelper.GetTotalDaysInTheMonth(year, month, true);
                cdToDate = CustomDate.GetCustomDateFromString(string.Format("{0}/{1}/{2}", year, month, daysInMonth), true);

                dfDetails.Text = string.Format("From {0} {1}, {2} to {3} {4}, {5}", DateHelper.GetMonthName(month, true), 1, year,
                    DateHelper.GetMonthName(month, true), daysInMonth, year);

            }
            else if (sender == btnLastMonth)
            {
                if (month == 1)
                {
                    month = 12;
                    year--;
                }
                else
                    month--;

                cdFromDate = CustomDate.GetCustomDateFromString(string.Format("{0}/{1}/{2}", year, month, 1), true);
                daysInMonth = DateHelper.GetTotalDaysInTheMonth(year, month, true);
                cdToDate = CustomDate.GetCustomDateFromString(string.Format("{0}/{1}/{2}", year, month, daysInMonth), true);

                dfDetails.Text = string.Format("From {0} {1}, {2} to {3} {4}, {5}", DateHelper.GetMonthName(month, true), 1, year,
                    DateHelper.GetMonthName(month, true), daysInMonth, year);
            }
            else if (sender == btnThisYear)
            {
                cdFromDate = CustomDate.GetCustomDateFromString(string.Format("{0}/{1}/{2}", year, 1, 1), true);
                daysInMonth = DateHelper.GetTotalDaysInTheMonth(year, 12, true);
                cdToDate = CustomDate.GetCustomDateFromString(string.Format("{0}/{1}/{2}", year, 12, daysInMonth), true);

                dfDetails.Text = string.Format("From {0} {1} to {2} {3}", DateHelper.GetMonthName(1, true), year,
                    DateHelper.GetMonthName(12, true), year);

                colEdit.Show();
            }
            else if (sender == btnDateRange)
            {              
                DateTime dtStart = calStartDate.SelectedDate;
                DateTime dtEnd = calEndDate.SelectedDate;

                cdFromDate = CustomDate.GetCustomDateFromString(string.Format("{0}/{1}/{2}", dtStart.Year, dtStart.Month, dtStart.Day), true);
                cdToDate = CustomDate.GetCustomDateFromString(string.Format("{0}/{1}/{2}", dtEnd.Year, dtEnd.Month, dtEnd.Day), true);

                dfDetails.Text = string.Format("From {0} {1}, {2} to {3} {4}, {5}", DateHelper.GetMonthName(dtStart.Month, true), dtStart.Day, dtStart.Year,
                    DateHelper.GetMonthName(dtEnd.Month, true), dtEnd.Day, dtEnd.Year);
            }

            fromDate = cdFromDate.EnglishDate;
            toDate = cdToDate.EnglishDate;

            int columnWidth = 60;           

            Model SiteModel = (this.gridSchedule.Store[0].Model[0] as Model);
            string columnId = "Item_{0}";

            int weekNumber = AttendanceManager.GetWeekNumber(DateTime.Today);

            List<HPLDutyPeriod> listPeriods = new List<HPLDutyPeriod>();

            if (sender == null || sender == btnThisYear)
                listPeriods = NewHRManager.GetHPLDutyPeriodsForYear(year);
            else
                listPeriods = NewHRManager.GetHPLDutyPeriodsForDateRange(fromDate, toDate);

            int listcount = listPeriods.Count;

            int k = 0;

            foreach(var item in listPeriods)
            {
                string gridIndexIdMonth = string.Format(columnId, ++k);

                ModelField field = new ModelField(gridIndexIdMonth, ModelFieldType.String);
                field.UseNull = true;
                SiteModel.Fields.Add(field);

                Column column = new Column();
                column.Text = item.MonthName;
                column.Sortable = false;
                column.Draggable = false;
                column.MenuDisabled = true;

                Column column1 = new Column();
                column1.Text = item.PeriodName;
                column1.Sortable = false;
                column1.Draggable = false;
                column1.MenuDisabled = true;

                column.Columns.Add(column1);

                Column dataColumn = new Column { DataIndex = gridIndexIdMonth, Align = Alignment.Center, Text = "WK " + item.WeekNo.ToString().PadLeft(2, '0'), Width = columnWidth, MenuDisabled = true, Sortable = false, Draggable = false };
                dataColumn.Renderer.Fn = "myRenderer1";
                column1.Columns.Add(dataColumn);

                gridSchedule.ColumnModel.Columns.Add(column);  
                
            }

            List<int> employeeIds = NewHRManager.GetScheduleEmployeesByYear(cdFromDate.Year);

            object[] data = new object[employeeIds.Count];

            int sn = 0, l = 2;
            int row = 0;
            foreach (int employeeId in employeeIds)
            {
                sn++;
                object[] rowData = new object[listcount  + 3];
                rowData[0] = sn.ToString();
                rowData[1] = employeeId.ToString();
                rowData[2] = EmployeeManager.GetEmployeeById(employeeId).Name;
                l = 2;
                foreach (var item in listPeriods)
                {
                    HPLDutySchedule obj = NewHRManager.GetEmployeeDutyScheduleForPeriod(employeeId, item.PeriodId);
                    if (obj != null)
                    {
                        if (obj.Value != null && obj.Value.Value)
                            rowData[++l] = "On";
                        else
                            rowData[++l] = "Off";
                    }
                    else
                        rowData[++l] = null;
                }

                //Add a Row
                data[row++] = rowData;

            }

            gridSchedule.Store[0].DataSource = data;
            gridSchedule.Store[0].DataBind();

        }

        protected void btnPopup_Click(object sender, DirectEventArgs e)
        {
            int year = DateTime.Today.Year;
            int employeeId = int.Parse(hdnEmployeeId.Text);

            cmbSchedule1.ClearValue();
            cmbSchedule2.ClearValue();
            cmbSchedule3.ClearValue();

            List<HPLDutySchedule> list = NewHRManager.GetTopThreeHPLDutySchedules(employeeId, year);
            if (list.Count > 0)
            {
                if (list[0].Value != null && list[0].Value.Value == true)
                    cmbSchedule1.SetValue("On");
                else
                    cmbSchedule1.SetValue("Off");

                if (list[1].Value != null && list[1].Value.Value == true)
                    cmbSchedule2.SetValue("On");
                else
                    cmbSchedule2.SetValue("Off");

                if (list[2].Value != null && list[2].Value.Value == true)
                    cmbSchedule3.SetValue("On");
                else
                    cmbSchedule3.SetValue("Off");

            }

            wSchedulePopup.Center();
            wSchedulePopup.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveSchedule");
            if (Page.IsValid)
            {
                List<HPLDutySchedule> list = new List<HPLDutySchedule>();

                bool val1 = false, val2 = false, val3 = false, val4 = false;
                int offCount = 0;

                if (cmbSchedule1.SelectedItem != null && cmbSchedule1.SelectedItem.Value != null)
                    val1 = (cmbSchedule1.SelectedItem.Value == "On" ? true : false);
                else
                {
                    NewMessage.ShowWarningMessage("Please select Schedule 1.");
                    cmbSchedule1.Focus();
                    return;
                }

                if (cmbSchedule2.SelectedItem != null && cmbSchedule2.SelectedItem.Value != null)
                    val2 = (cmbSchedule2.SelectedItem.Value == "On" ? true : false);
                else
                {
                    NewMessage.ShowWarningMessage("Please select Schedule 2.");
                    cmbSchedule2.Focus();
                    return;
                }

                if (cmbSchedule3.SelectedItem != null && cmbSchedule3.SelectedItem.Value != null)
                    val3 = (cmbSchedule3.SelectedItem.Value == "On" ? true : false);
                else
                {
                    NewMessage.ShowWarningMessage("Please select Schedule 3.");
                    cmbSchedule3.Focus();
                    return;
                }

                if (val1 == false)
                    offCount++;
                if (val2 == false)
                    offCount++;
                if (val3 == false)
                    offCount++;

                if (offCount > 1)
                {
                    NewMessage.ShowWarningMessage("Please select only one \"Off\" value.");
                    return;
                }

                if (val1 == true && val2 == true && val3 == true)
                    val4 = false;
                else
                    val4 = true;

                int k = 1;
                List<HPLDutyPeriod> listDutyPeriods = NewHRManager.GetHPLDutyPeriodsForYear(DateTime.Today.Year);
                foreach (var item in listDutyPeriods)
                {
                    HPLDutySchedule obj = new HPLDutySchedule();
                    obj.EmployeeId = int.Parse(hdnEmployeeId.Text);
                    obj.PeriodId = item.PeriodId;
                    obj.Year = item.Year;

                    if (k % 4 == 1)
                        obj.Value = val1;
                    else if (k % 4 == 2)
                        obj.Value = val2;
                    else if (k % 4 == 3)
                        obj.Value = val3;
                    else
                        obj.Value = val4;

                    list.Add(obj);
                    k++;
                }

                Status status = NewHRManager.SaveUpdateEmpDutySchedule(list);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Employee duty schedule saved successfully.");
                    wSchedulePopup.Close();
                    btnLoad_Click(btnThisYear, null);
                }
                else
                    NewMessage.ShowWarningMessage("Error while saving employee duty schedule.");          

            }

        }

        protected void btnAddEmployee_Click(object sender, DirectEventArgs e)
        {
            cmbEmployee.ClearValue();
            cmbYear.ClearValue();
            WAddEmployee.Center();
            WAddEmployee.Show();
        }

        protected void btnSaveEmployee_Click(object sener, DirectEventArgs e)
        {
            Page.Validate("AddEmployee");
            if (Page.IsValid)
            {
                if (cmbEmployee.SelectedItem == null || cmbEmployee.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Please select employee.");
                    cmbEmployee.Focus();
                    return;
                }

                if (cmbYear.SelectedItem == null || cmbYear.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Please select year.");
                    cmbYear.Focus();
                    return;
                }

                HPLScheduleEmployee obj = new HPLScheduleEmployee();
                obj.EmployeeId = int.Parse(cmbEmployee.SelectedItem.Value);
                obj.Year = int.Parse(cmbYear.SelectedItem.Value);

                Status status = NewHRManager.SaveUpdateScheduleEmployee(obj);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Employee added successfully to the schedule year.");
                    WAddEmployee.Close();
                    btnLoad_Click(btnThisYear, null);
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);

            }

        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int year = DateTime.Today.Year;
            int employeeId = int.Parse(hdnEmployeeId.Text);

            Status status = NewHRManager.DeleteHPLEmpSchedule(employeeId, year);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Employee duty schedule deleted successfully.");
                wSchedulePopup.Close();

                btnLoad_Click(btnThisYear, null);
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }




    }
}