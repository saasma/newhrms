﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true"
    CodeBehind="ResignationRequestList.aspx.cs" Inherits="Web.NewHR.ResignationRequestList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
    <style type="text/css">
        .holiday, .holiday a, .holiday td {
            color: #469146;
            background-color: #F0FFF0 !important;
        }

        .leave, .leave a, .leave td {
            color: #D95CA9;
            background-color: #FFF0F5 !important;
        }

        .weeklyholiday, .weeklyholiday a, .weeklyholiday td {
            color: #8F8F1A;
            background-color: #FAFAD2 !important;
        }
    </style>
    <style type="text/css">
        .x-grid-row, .x-grid-data-row {
            height: 25px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript">       
        function clearControls(){
            <%= FinanceClearance.ClientID %>.clear();
            <%= AssetCustodian.ClientID %>.clear();
            <%= LibraryClearance.ClientID %>.clear();
           
        }

        function showWindow(command){
            if(command=="Start")
            {
                <%= StartExitProcess.ClientID %>.show();
            }
            if(command=="Reject")
            {
                <%= Reject.ClientID %>.show();
            } 
            if(command=="View")
            {
                <%= View.ClientID %>.show();
                <%= btnView.ClientID %>.fireEvent('click');
            }   
        }

        var setValue = function(record){
            <%= hiddenValue.ClientID %>.setValue(record.data.ID);
        }
        var changeStatus = function (command) 
        { 
            if(command=="Start")
            {
                <%= btnStartExit.ClientID %>.fireEvent('click');
            }
            if(command=="Reject")
            {
                <%= btnReject.ClientID %>.fireEvent('click');
            }             
        }

        function searchList(data) { 
            <%= tabChange.ClientID %>.setValue(data);
            <%= btnTabChange.ClientID %>.fireEvent('click');

        }

        var prepare = function (grid, toolbar, rowIndex, record) {
            var firstButton = toolbar.items.get(1);
            var a = <%= tabChange.ClientID %>.getValue();
            if (a > 0) {                
                firstButton.setDisabled(true);
                firstButton.setTooltip("Disabled");
                if(a > 1){ 
                    var button = toolbar.items.get(2);                     
                    button.setDisabled(true);
                    button.setTooltip("Disabled");
                }
            }
        };
        var DaysCount = function(value){
            if(value <0){
                return "Date expired";
                
            }
            if (value==0){
                return "Today";
            }
           else  {
                return value;
            }
        }

        var companyRenderer = function (value, record) {
            var tpl = "<img src='{0}' />";
            if (r.data.pctChange > 0) {
                return String.format(tpl, "GOOD.GIF");
            }
            else if (r.data.pctChange == 0) {
                return String.format(tpl, "OK.GIF");
            }
            else if (r.data.pctChange < 0) {
                return String.format(tpl, "BAD.GIF");
            }
            else {
                return "";
            }
        };

    </script>
    <%-- <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>--%>

    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="tabChange" />
    <ext:Button ID="btnView" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnView_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnStartExit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnStartExit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnReject" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnReject_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnTabChange" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnTab_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

    <div class="contentpanel" style='padding-top: 0px;'>
        <div class="innerLR">
            <h3>Resignation Requests</h3>
            <br />
            <table>
                <tr>
                    <td>
                        <ext:TabPanel ID="tabPanel" runat="server" MinHeight="30" ActiveTabIndex="0" Width="400">
                            <Items>
                                <ext:Panel ID="tabPending" runat="server" Title="Pending">
                                </ext:Panel>
                                <ext:Panel ID="tabExitProcess" runat="server" Title="Exit process">
                                </ext:Panel>
                                <ext:Panel ID="tabApproved" runat="server" Title="Approved">
                                </ext:Panel>
                                <ext:Panel ID="tabRejected" runat="server" Title="Rejected">
                                </ext:Panel>
                            </Items>
                            <%-- <DirectEvents>
                                <TabChange OnEvent="btnTab_Click" ></TabChange>                                 
                            </DirectEvents>--%>
                            <Listeners>
                                <TabChange Handler="searchList(#{tabPanel}.items.indexOf(#{tabPanel}.getActiveTab()));" />
                            </Listeners>
                        </ext:TabPanel>
                    </td>
                    <td style="padding-left: 20px;">
                        <span id='spanText' style="color: blue"></span>
                    </td>
                </tr>
            </table>
            <ext:GridPanel ID="gridReqList" runat="server" Header="true" AutoScroll="true" Width="650">
                <Store>
                    <ext:Store ID="Store3" runat="server" OnReadData="Store_ReadData" RemoteSort="true"
                        PageSize="20" GroupField="EmployeeName">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Sorters>
                            <ext:DataSorter Direction="ASC" Property="EmployeeName" />
                        </Sorters>

                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="RequestID">
                                <Fields>
                                    <ext:ModelField Name="ID" />
                                    <ext:ModelField Name="EmployeeName" />
                                    <ext:ModelField Name="ExpectedDate" />
                                    <ext:ModelField Name="DaysCount" />
                                    <ext:ModelField Name="Action" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>


                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="SN" Width="40" Align="Center"/>
                        <ext:Column ID="colEId" Locked="true" runat="server" Text="Employee Name" Width="130" DataIndex="EmployeeName"
                             Sortable="true" MenuDisabled="false">
                        </ext:Column>
                        <ext:DateColumn ID="Column10" Format="yyyy-MMM-dd" runat="server" Text="Expected Date"
                            Width="100" DataIndex="ExpectedDate" Sortable="true" MenuDisabled="false">
                        </ext:DateColumn>
                        <ext:Column ID="Column11" runat="server"  Text="Days Count" Width="100"
                            DataIndex="DaysCount" Sortable="true"  MenuDisabled="false">
                                                <Renderer Fn="DaysCount" />                       

                        </ext:Column>
                       
                        <ext:CommandColumn runat="server" Text="Action" Width="260">
                            <Commands>
                                <ext:GridCommand CommandName="View" Text="View" />
                                <ext:GridCommand CommandName="Start" Text="Start Exit" />
                                <ext:GridCommand Icon="Delete" CommandName="Reject" Text="Reject" />
                            </Commands>
                            <PrepareToolbar Fn="prepare" />
                            <Listeners>
                                <Command Handler="showWindow(command); setValue(record); " />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
               
                <Listeners>
                    <SelectionChange Handler="document.getElementById('spanText').innerHTML = this.getSelectionModel().selected.items.length + ' requests selected';" />
                </Listeners>
                <%-- <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple">
                        <Listeners>
                            <BeforeSelect Fn="gridRowBeforeSelect" />
                        </Listeners>
                        <Renderer Fn="renderSelect">
                        </Renderer>
                    </ext:CheckboxSelectionModel>
                </SelectionModel>--%>
               
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="#{Store3}.pageSize = this.getValue();searchList()" />
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <br />
        </div>
    </div>

    <ext:Window
        ID="StartExitProcess"
        runat="server"
        Title="Start Exit Process"
        Icon="Application"
        Width="950"
        Height="500"
        Closable="false"
        BodyStyle="background-color: #fff;"
        BodyPadding="5"
        Hidden="true"
        Modal="true">
        <Content>
            <div>
                <div class="panel">
                    <content>
                    <div class="position">
                        <label>
                            <p><strong>You are about to start employee clearnernce. </strong></p>
                            <p><strong>The following clearnce are required. Remove the selection if you want to skip the exempt some forms.</strong></p>
                        </label>
                        <br />
                        <ext:FieldSet
                            runat="server"
                            Flex="1"
                            Title="Clearance Form"
                            Layout="AnchorLayout"
                            DefaultAnchor="100%">
                            <Items>
                                <ext:Checkbox ID="FinanceClearance" runat="server" BoxLabel="Finance Clearance Form" />
                                <ext:Checkbox ID="AssetCustodian" runat="server" BoxLabel="Asset Custodian Form" />
                                <ext:Checkbox ID="LibraryClearance" runat="server" BoxLabel="Library Clearance Form" />
                            </Items>
                        </ext:FieldSet>
                        <buttons>
                            <ext:Button runat="server" Text="Start exit Process" OnClientClick="changeStatus('Start');"  height="40"/>                              
                            <ext:Button runat="server" Text="Cancle" OnClientClick="#{StartExitProcess}.close(); clearControls();"  height="40"/>                              
                        </buttons>
                </content>
                </div>
            </div>
        </Content>
    </ext:Window>

    <ext:Window
        ID="Reject"
        runat="server"
        Title="Reject"
        Icon="Application"
        Width="900"
        Height="500"
        Closable="false"
        BodyStyle="background-color: #fff;"
        BodyPadding="5"
        Hidden="true"
        Modal="true">
        <Content>
            <div>
                <div class="panel">
                    <content>
                    <div class="position">
                        <label>
                            <p><strong>You are about to reject the request. </strong></p>
                            <p><strong>The following clearnce are required. Remove the selection if you want to skip the exempt some forms.</strong></p>
                        </label>
                        <br />
                        <ext:Button runat="server" Text="Reject Request"  OnClientClick="changeStatus('Reject');" height="40"/>
                        <ext:Button runat="server" Text="Cancel" OnClientClick="#{Reject}.close();" height="40"/>                      
                </content>
                </div>
            </div>
        </Content>
    </ext:Window>

    <ext:Window
        ID="View"
        runat="server"
        Title="Detail"
        Icon="Application"
        Width="900"
        Height="500"
        Closable="false"
        BodyStyle="background-color: #fff;"
        BodyPadding="5"
        Hidden="true"
        Modal="true">
        <Content>
            <div class="position">
                <ext:DateField
                    ID="ExpectedDate"
                    runat="server"
                    Vtype="daterange"
                    ReadOnly="true"
                    FieldLabel="Expected Date">
                </ext:DateField>
                <br />
                <div class="position1">
                    <ext:TextArea runat="server" ID="Reason" ReadOnly="true" FieldLabel="Reason for Resignation" bottompadding="5px" Width="800" Height="200" />
                </div>
            </div>
            <ext:Button runat="server" Text="Start exit Process" OnClientClick="changeStatus('Start');" Height="40" />
            <ext:Button runat="server" Text="Reject Request" OnClientClick="changeStatus('Reject');" Height="40" />
            <ext:Button runat="server" Text="Close" OnClientClick="#{View}.close();" Height="40" />
        </Content>
    </ext:Window>
</asp:Content>
