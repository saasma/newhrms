﻿<%@ Page Title="Promotion List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="PromotionListGeneral.aspx.cs" Inherits="Web.NewHR.PromotionListGeneral" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/ActingListCtl.ascx" TagName="FamilyCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        

        var levelRenderer = function (value) {
            if (value == "")
                return "";

            var level = <%=StoreLevel.ClientID %>.getById(value);
            if (Ext.isEmpty(level))
                return "";
            return level.data.GroupLevel;
        }
        var designationRenderer = function (value) {
            if (value == "" || value == null)
                return "";
            var StorePostDesignation = <%=StorePostDesignation.ClientID %>;
            var level = StorePostDesignation.getById(value);
            if (Ext.isEmpty(level))
                return "";
            return level.data.Name;
        }
        var record = null;
        var beforeLevelId = null;
        var beforeEdit = function (e1, e, e2, e3) {
            // add code to filter by UOM
            beforeLevelId = e.record.data.LevelId;

            if (e.field == "DesignationId") {
                var cmbDesignation = <%=cmbDesignation.ClientID %>;
                cmbDesignation.lastQuery = "";
                cmbDesignation.store.clearFilter();
                cmbDesignation.store.filter({ property: 'LevelId', value: e.record.data.LevelId, exactMatch: true });
                //cmbUOM.setValue(e.record.data.UOMRef_ID);
            }
        }
        var afterEdit = function (editor, e) {
            if (e.field == "FromDate") 
            {
                if(e.record.data.FromDate != "" && isValidDate(e.record.data.FromDate) == false)
                {
                    e.record.data.FromDate = "";
                }
            }
            if (e.field == "LevelId") 
            {

//                Ext.getBody().mask("Loading...");
//                Ext.net.DirectMethods.GetNewGrade(
//                    e.record.data.PrevLevelId,e.record.data.PrevGrade,e.record.data.LevelId,
//                {
//                            success: function (result) {  Ext.getBody().unmask();promotionCallback(result);  },
//                            failure: function (result) {   Ext.getBody().unmask();promotionCallback(""); }
//                }
//                );

//                if(beforeLevelId != e.record.data.LevelId)
//                    e.record.data.DesignationId = ""; 

                record = e.record;
            }
            if(e.record.data.StepGrade=="")
                e.record.data.StepGrade="0";
            e.record.commit();
        }

        var promotionCallback = function(result)
        {
            record.data.StepGrade=result;
            record.commit();
        }

        var CommandHandler = function(command, record){
            <%= hdn.ClientID %>.setValue(record.data.DetailId);
            
            
            if(record.data.DetailId == 0)
            {
                <%=gridPromotion.ClientID %>.getStore().remove(record);
                return;
            }

                <%= btnDelete.ClientID %>.fireEvent('click');
            

        }

        var prepareCommand = function (grid, command, record, row) {
            // you can prepare group command
            if (record.data.Status == '2')
            {
                command.hidden = true;
                command.hideMode = 'visibility';    //you can try 'display' also
            }


           
        };

        function refreshWindow(popupWindow)
        {
            if (typeof (popupWindow) != 'undefined')
                popupWindow.close();

            <%=btnLoadQuestion.ClientID %>.fireEvent('click');
            //alert("ehllo");
        }


    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 10px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <style type="text/css">
        .innerLR
        {
            padding: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Promotion List
                </h4>
            </div>
        </div>
    </div>
    <div style="color:#800000;    margin-left: 20px;
    margin-top: 5px;">
        Note : First Save the list and then only Approve the promotion, do not improt level/grade change history from this page
    </div>
    <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnLoadQuestion">
        <DirectEvents>
            <Click OnEvent="btnLoadQuestion_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="contentpanel" style="padding-top: 0px;">
        <ext:Hidden runat="server" ID="hdn">
        </ext:Hidden>
        <ext:Button ID="btnDelete" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDelete_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Promotion?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <ext:Store ID="StoreLevel" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="LevelId">
                    <Fields>
                        <ext:ModelField Name="LevelId" Type="String" />
                        <ext:ModelField Name="GroupLevel" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="StorePostDesignation" runat="server">
            <Model>
                <ext:Model ID="Model3" runat="server" IDProperty="DesignationId">
                    <Fields>
                        <ext:ModelField Name="DesignationId" Type="String" />
                        <ext:ModelField Name="Name" />
                        <ext:ModelField Name="LevelId" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="StoreAllDesignation" runat="server">
            <Model>
                <ext:Model ID="Model5" runat="server" IDProperty="DesignationId">
                    <Fields>
                        <ext:ModelField Name="DesignationId" Type="String" />
                        <ext:ModelField Name="Name" />
                        <ext:ModelField Name="LevelId" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div class="innerLR">
            <ext:Label ID="lblMsg" runat="server" />
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model2" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info">
                <table class="fieldTable firsttdskip">
                    <tr>
                        <td>
                            <pr:CalendarExtControl Width="180px" FieldLabel="From Date" ID="calFromDate" runat="server"
                                LabelAlign="Top" LabelSeparator="" />
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="calFromDate" ErrorMessage="Date is required." />
                        </td>
                        <td>
                            <ext:ComboBox ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70" LabelAlign="Top"
                                runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <DirectEvents>
                                    <Select OnEvent="cmbSearch_Select">
                                        <ExtraParams>
                                            <ext:Parameter Name="List" Value="Ext.encode(#{gridPromotion}.getRowsValues({selectedOnly : false}))"
                                                Mode="Raw" />
                                        </ExtraParams>
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:TextField ID="txtName" runat="server" Width="180px" LabelAlign="Top" FieldLabel="Name">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="txtName" ErrorMessage="Name is required." />
                        </td>
                         <td>
                            <asp:LinkButton ID="btnExport" runat="server" Text="Excel import" OnClientClick="taxImportPopup();return false;"
                                CssClass=" excel marginRight tiptip" Style="float: left;" />
                        </td>
                    </tr>
                </table>
            </div>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridPromotion" runat="server" Cls="itemgrid">
                <Store>
                    <ext:Store ID="Store3" runat="server">
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="DetailId" Type="string" />
                                    <ext:ModelField Name="EmployeeId" Type="string" />
                                    <ext:ModelField Name="EmployeeName" Type="string" />
                                    <%-- <ext:ModelField Name="Branch" Type="string" />--%>
                                    <ext:ModelField Name="PrevLevelId" Type="string" />
                                    <ext:ModelField Name="PrevGrade" Type="string" />
                                    <ext:ModelField Name="PrevDesignationId" Type="string" />
                                    <ext:ModelField Name="LevelId" Type="string" />
                                    <ext:ModelField Name="StepGrade" Type="string" />
                                    <ext:ModelField Name="DesignationId" Type="string" />
                                    <ext:ModelField Name="FromDate" Type="string" />
                                    <ext:ModelField Name="StatusText" Type="string" />
                                    <ext:ModelField Name="Status" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                        <Listeners>
                            <BeforeEdit Fn="beforeEdit" />
                            <Edit Fn="afterEdit" />
                        </Listeners>
                    </ext:CellEditing>
                </Plugins>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                            Align="Left" Width="60" DataIndex="EmployeeId" />
                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                            Align="Left" Width="150" DataIndex="EmployeeName" />
                        <ext:Column ID="Column6" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Post" Align="Left" DataIndex="PrevDesignationId">
                            <Renderer Fn="designationRenderer" />
                        </ext:Column>
                        <ext:Column ID="Column2" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Level" Align="Left" DataIndex="PrevLevelId">
                            <Renderer Fn="levelRenderer" />
                        </ext:Column>
                        <ext:Column ID="Column3" Width="80" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Grade" Align="Left" DataIndex="PrevGrade">
                        </ext:Column>
                        <ext:Column ID="Column7" Width="90" Sortable="false" MenuDisabled="true" runat="server"
                            Text="From Date" Align="Left" DataIndex="FromDate">
                            <Editor>
                                <ext:TextField runat="server" />
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column1" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                            Text="New Level" Align="Left" DataIndex="LevelId">
                            <Renderer Fn="levelRenderer" />
                            <Editor>
                                <ext:ComboBox ID="cmbCurrentLevel" Width="180px" runat="server" ValueField="LevelId"
                                    DisplayField="GroupLevel" StoreID="StoreLevel" LabelAlign="top" LabelSeparator=""
                                    ForceSelection="true" QueryMode="Local">
                                    <Listeners>
                                        <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                      queryEvent.query = new RegExp('\\b' + q, 'i');
                                      queryEvent.query.length = q.length;" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column4" Width="90" Sortable="false" MenuDisabled="true" runat="server"
                            Text="New Grade" Align="Left" DataIndex="StepGrade">
                            <Editor>
                                <ext:NumberField runat="server" MinValue="0" />
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column10" Width="140" Sortable="false" MenuDisabled="true" runat="server"
                            Text="New Post" Align="Left" DataIndex="DesignationId">
                            <Renderer Fn="designationRenderer" />
                            <Editor>
                                <ext:ComboBox ID="cmbDesignation" Width="180px" runat="server" ValueField="DesignationId"
                                    DisplayField="Name" StoreID="StorePostDesignation" LabelAlign="top" LabelSeparator=""
                                    ForceSelection="true" QueryMode="Local">
                                </ext:ComboBox>
                            </Editor>
                        </ext:Column>
                        <ext:Column ID="Column5" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Status" Align="Left" DataIndex="StatusText">
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40" Text="" Align="Center"
                            MenuDisabled="true" Sortable="false">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                    CommandName="Delete" />
                            </Commands>
                            <PrepareGroupToolbar Fn="prepareCommand" />
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
                </SelectionModel>
            </ext:GridPanel>
            <table style="margin-top: 20px;">
                <tr>
                    <td style="width: 95%;">
                        <ext:Button runat="server"  ID="btnSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSaveUpdate_Save" Timeout="999999">
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the Promotion?" />
                                    <ExtraParams>
                                        <ext:Parameter Name="List" Value="Ext.encode(#{gridPromotion}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdate'; if(CheckValidation()) return this.disable(); else return false;">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td>
                        <ext:Button ID="btnApprove" Cls="btn btn-success" Disabled="true" runat="server" Text="Approve">
                            <Listeners>
                                <Click Handler="valGroup = 'saveupdate'; if(CheckValidation()) return this.disable(); else return false;" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="lnkBtnApprove_Click" Timeout="492000">
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the Promotion?" />
                                    <ExtraParams>
                                        <ext:Parameter Name="List" Value="Ext.encode(#{gridPromotion}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
            </table>
            <div style="clear: both">
            </div>
        </div>
    </div>
    <br />
</asp:Content>
