﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Web.CP.Report.Templates.HR;
using BLL.BO;
using System.IO;
using Utils;
using System.Data;
using Utils.Calendar;
using DevExpress.XtraPrinting;
using System.Security.AccessControl;
using Utils.Helper;
using System.Text;
using Bll;


namespace Web.NewHR
{
    public partial class EmployeeProjectContributionList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.ProjectAndTimesheet;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {

            }
            
           
        }


        protected void btnExcelPrint_Click(object sender, EventArgs e)
        {
            
            int EmployeeId = -1;
            if(!string.IsNullOrEmpty(cmbSearch.SelectedItem.Value))
                EmployeeId = int.Parse(cmbSearch.SelectedItem.Value);

            List<GetEmployeProjectContributionListResult> resultSet = NewTimeSheetManager.GetEmployeProjectContributionList(0, int.MaxValue, EmployeeId, hdnStartDate.Text.Trim());

                ExcelHelper.ExportToExcel("Employee Project Contribution List", resultSet,
                    new List<String>() { "Distribution", "TimeSheetID", "ProjectID","ProjectNumber", "TotalRows", "RowNumber" },
              new List<String>() { },
                    new Dictionary<string, string>() { { "EmployeeID","EIN" }, { "EmployeeName","Staff Name" },{"DeptID"," Dept ID"},{"FundCode","Fund Code"},
                    {"BusinessUnit","Business Unit"},{"ProjectName","Project ID"},{"SourceType","Source Type"},{"Categoty","Category"},{"Percentage","Distribution"} },
              new List<string>() { "Percentage" }
              , new Dictionary<string, string>() { },
              new List<string> { "EmployeeID", "EmployeeName", "DeptID", "FundCode", "BusinessUnit", "ProjectName", "ProjectID", "Activity", "SourceType", "Categoty", "Percentage" }
             );
            }

        }

      
  

    }
