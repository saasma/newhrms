﻿<%@ Page Title="Duty Schedule" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmpDutySchedule.aspx.cs" Inherits="Web.NewHR.EmpDutySchedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">

    .x-grid3-cell-inner {
            padding-top: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            padding-right: 0px;
        }
        
           .x-grid-componentcolumn .x-grid-cell .x-grid-cell-inner {
    border:none;
}
    
</style>

<script type="text/javascript">

    function linkClick(EmployeeId){
            <%= hdnEmployeeId.ClientID %>.setValue(EmployeeId);
             <%= btnPopup.ClientID %>.fireEvent('click');
        }
        
        function linkRenderer(value, meta, record){                        
            return String.format("<a class='company-link' href='#' onclick='linkClick(\"{0}\");'>Edit</a>", value);
        }

    var CommandHandler = function(command, record){
            <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
                <%= btnPopup.ClientID %>.fireEvent('click');
        }

        function enableDateRange() {
            <%= calStartDate.ClientID %>.enable();
            <%= calEndDate.ClientID %>.enable();
        }

        var myRenderer1 = function (value, metadata) {

            if(value != null && value != '')
            {
                if(value == 'On')
                    metadata.style = "background-color: #92d050; color:#006100;";
                else
                    metadata.style = "background-color: lightsalmon; color:#9c0006;";
            }

            return value;
        };

</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>

<ext:Hidden ID="hdnEmployeeId" runat="server" />

<ext:Button ID="btnPopup" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnPopup_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:Button>

    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Duty Schedule
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
       
        <table style="display:none;">
            <tr>
                <td>
                    <ext:Button ID="btnThisMonth" runat="server" Cls="btn btn-save" Text="This Month" Width="120" OnClick="btnLoad_Click" AutoPostBack="true">
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnNextMonth" runat="server" Cls="btn btn-save" Text="Next Month" Width="120" MarginSpec="0 0 0 15" OnClick="btnLoad_Click" AutoPostBack="true" >
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnLastMonth" runat="server" Cls="btn btn-save" Text="Last Month" Width="120"  MarginSpec="0 0 0 15" OnClick="btnLoad_Click" AutoPostBack="true">
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnThisYear" runat="server" Cls="btn btn-save" Text="This Year" Width="120"  MarginSpec="0 0 0 15" OnClick="btnLoad_Click" AutoPostBack="true">
                    </ext:Button>
                </td>
                <td> 
                    <ext:Button ID="btnDateRange" runat="server" Cls="btn btn-save" Text="Date Range" Width="120"  MarginSpec="0 0 0 15" OnClick="btnLoad_Click" AutoPostBack="true">
                        <Listeners>
                            <Click Handler="valGroup = 'LoadData'; if(CheckValidation()) return ''; else return false;">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td>
                    <ext:DateField ID="calStartDate" runat="server" Width="120px"  EmptyText="From" Enabled="false"  MarginSpec="0 0 0 15"
                        LabelSeparator="">
                        <Plugins>
                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                    <asp:RequiredFieldValidator Display="None" ID="rfvStartDate" runat="server" ValidationGroup="LoadData"
                                ControlToValidate="calStartDate" ErrorMessage="From Date is required." />
                </td>
                <td>
                    <ext:DateField ID="calEndDate" runat="server" Width="120px" EmptyText="To" Enabled="false"  MarginSpec="0 0 0 15"
                        LabelSeparator="">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server" ValidationGroup="LoadData"
                                ControlToValidate="calEndDate" ErrorMessage="To Date is required." />
                </td>
            </tr>
        </table>
       
        <ext:DisplayField ID="dfDetails" runat="server" StyleSpec="font-style:italic;" FieldStyle="font-style:italic;" />

        <ext:GridPanel StyleSpec="margin-top:5px;" ID="gridSchedule" runat="server" Cls="itemgrid"
            Scroll="Horizontal">
            <Store>
                <ext:Store ID="storeSchedule" runat="server">
                    <Model>
                        <ext:Model ID="ModelSchedule" runat="server">
                            <Fields>
                                <ext:ModelField Name="SN" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="String" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="SN" Draggable="false"
                        Width="60" Align="Center" DataIndex="SN" Locked="true">
                    </ext:Column>
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="EmployeeId" Hidden="true"
                        Width="60" Align="Left" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server" Locked="true"
                        Text="Employee Name" Width="180" Align="Left" DataIndex="EmployeeName">
                    </ext:Column>                    
                    <ext:Column ID="colEdit" Header="" DataIndex="EmployeeId" runat="server" Width="50" Hidden="true">
                            <Renderer Fn="linkRenderer" />
                        </ext:Column>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
            </SelectionModel>           
            <Plugins>
                <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                    <%--<Listeners>                              
                        <Edit Fn="afterEdit" />
                    </Listeners>--%>
                </ext:CellEditing>
            </Plugins>
           
        </ext:GridPanel>
        <br />

        <ext:Button ID="btnAddEmployee" runat="server" Cls="btn btn-save" Text="Add Employee" MarginSpec="20 0 0 0" >
            <DirectEvents>
                <Click OnEvent="btnAddEmployee_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>

        <br />
    </div>


    <ext:Window ID="wSchedulePopup" Width="400" Height="230" Title="Change Employee Schedule"
        Closable="true" Resizable="false" Hidden="true" runat="server" AutoScroll="false">
        <Content>
            <div class="windowContentWrapper">
                <table style="margin-left: 20px;">
                    <tr>
                        <td>
                            <ext:ComboBox ID="cmbSchedule1" runat="server" FieldLabel="Schedule 1" LabelAlign="Top" Width="100px" DisplayField="Text" LabelSeparator=""
                                ValueField="Value" EmptyText="" QueryMode="Local" MarginSpec="10 0 0 0">
                                <Items>
                                    <ext:ListItem Text="On" Value="On" />
                                    <ext:ListItem Text="Off" Value="Off" />    
                                </Items>
                            </ext:ComboBox>
                             <asp:RequiredFieldValidator Display="None" ID="rfvSchedule1" runat="server" ValidationGroup="SaveSchedule"
                                ControlToValidate="cmbSchedule1" ErrorMessage="Please select Schedule 1." />
                        </td>
                        <td>
                            <ext:ComboBox ID="cmbSchedule2" runat="server" FieldLabel="Schedule 2" MarginSpec="10 0 0 20" LabelAlign="Top" Width="100px" DisplayField="Text" LabelSeparator=""
                                ValueField="Value" EmptyText="" QueryMode="Local">
                                <Items>
                                    <ext:ListItem Text="On" Value="On" />
                                    <ext:ListItem Text="Off" Value="Off" />    
                                </Items>
                            </ext:ComboBox>
                             <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server" ValidationGroup="SaveSchedule"
                                ControlToValidate="cmbSchedule2" ErrorMessage="Please select Schedule 2." />
                        </td>
                        <td>
                            <ext:ComboBox ID="cmbSchedule3" runat="server" FieldLabel="Schedule 3" LabelAlign="Top" MarginSpec="10 0 0 20" Width="100px" DisplayField="Text" LabelSeparator=""
                                ValueField="Value" EmptyText="" QueryMode="Local">
                                <Items>
                                    <ext:ListItem Text="On" Value="On" />
                                    <ext:ListItem Text="Off" Value="Off" />    
                                </Items>
                            </ext:ComboBox>
                             <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server" ValidationGroup="SaveSchedule"
                                ControlToValidate="cmbSchedule3" ErrorMessage="Please select Schedule 3." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Button ID="btnSave" runat="server" Cls="btn btn-save" Text="Save" MarginSpec="20 0 0 0" >
                               <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                               </DirectEvents>
                               <Listeners>
                                    <Click Handler="valGroup = 'SaveSchedule'; if(CheckValidation()) return ''; else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td>
                            <ext:Button ID="btnDelete" runat="server" Cls="btn btn-save" Text="Delete" MarginSpec="20 0 0 0" >
                               <DirectEvents>
                                    <Click OnEvent="btnDelete_Click">
                                        <EventMask ShowMask="true" />
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete?" />
                                    </Click>
                               </DirectEvents>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </Content>
    </ext:Window>


    <ext:Window ID="WAddEmployee" Width="280" Height="200" Title="Add Employee"
        Closable="true" Resizable="false" Hidden="true" runat="server" AutoScroll="false">
        <Content>
            <div class="windowContentWrapper">
                <table style="margin-left: 20px;">
                    <tr>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model3" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" ID="cmbEmployee" runat="server" DisplayField="Name" MarginSpec="20 0 0 0"
                                FieldLabel="" LabelAlign="Top" ValueField="EmployeeId" EmptyText="Search Employee"
                                StoreID="storeSearch" TypeAhead="false" Width="220" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="false">
                                <ListConfig LoadingText="Searching..." MinWidth="220" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                                    <div class="search-item">
                                                                    <span>{Name}</span>  
                                                     </div>
					                        </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                                   this.clearValue(); 
                                                   this.getTrigger(0).hide();
                                               }" />
                                </Listeners>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="rfvEmployee" runat="server" ValidationGroup="AddEmployee"
                                ControlToValidate="cmbEmployee" ErrorMessage="Please select employee." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Store ID="storeYear" runat="server">
                                <Model>
                                    <ext:Model ID="modelYear" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Text" Type="String" />
                                            <ext:ModelField Name="Value" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox ID="cmbYear" runat="server" FieldLabel="" Width="100" DisplayField="Text" EmptyText="Year" MarginSpec="13 0 0 0"
                                ValueField="Value" StoreID="storeYear" Editable="false"  QueryMode="Local">
                            </ext:ComboBox>
                             <asp:RequiredFieldValidator Display="None" ID="rfvYear" runat="server" ValidationGroup="AddEmployee"
                                    ControlToValidate="cmbYear" ErrorMessage="Please select year." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:Button ID="btnSaveEmployee" runat="server" Cls="btn btn-save" Text="Add" MarginSpec="20 0 0 0" Width="100" >
                               <DirectEvents>
                                    <Click OnEvent="btnSaveEmployee_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                               </DirectEvents>
                               <Listeners>
                                    <Click Handler="valGroup = 'AddEmployee'; if(CheckValidation()) return ''; else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </Content>
    </ext:Window>
        

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
