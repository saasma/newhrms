﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using DAL;
using BLL.Manager;
using Utils.Helper;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;
using BLL.Base;

namespace Web.NewHR
{
    public partial class EmployeeAddressReport : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "EmpPopup", "../ExcelWindow/EmpAddressImportExcel.aspx", 450, 500);
        }
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int total = 0, pageSize = 0, employeeId = -1, LevelId = -1, BranchId = -1;
            string employeeName = cmbSearch.Text;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);
                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                BranchId = Convert.ToInt32(cmbBranch.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                LevelId = Convert.ToInt32(cmbLevel.SelectedItem.Value);

            pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            List<GetEmployeeAddressResult> list = NewHRManager.GetEmployeeAddressResult(e.Page - 1, e.Limit, employeeId, employeeName, BranchId, LevelId);

            gridEmpStore.DataSource = list;
            gridEmpStore.DataBind();

            if (list.Count > 0)
                total = list[0].TOTALROWS.Value;
            e.Total = total;
        }
        private void Initialise()
        {
            storeBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            storeBranch.DataBind();

            storeLevel.DataSource = NewPayrollManager.GetAllParentLevelList().OrderBy(x => x.Name).ToList();
            storeLevel.DataBind();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/EmpAddressImport.xlsx");
            int employeeId = -1, BranchId = -1, LevelId = -1;
            string employeeName = "";
            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);
                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                BranchId = Convert.ToInt32(cmbBranch.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                LevelId = Convert.ToInt32(cmbLevel.SelectedItem.Value);
            List<GetEmployeeAddressResult> list = NewHRManager.GetEmployeeAddressResult(0, 9999999, employeeId, employeeName, BranchId, LevelId);
            List<CountryName> CountryList = CommonManager.GetCountryNames();
            List<DistrictList> DistrictList = CommonManager.GetDistrictNames();
            List<ZoneList> ZoneList = CommonManager.GetZoneList();
            ExcelGenerator.ExportEmployeeAddress(template, list, CountryList, ZoneList, DistrictList);
        }

    }
}
