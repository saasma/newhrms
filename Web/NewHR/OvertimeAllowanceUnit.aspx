﻿<%@ Page Title="Overtime Allowance Unit" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="OvertimeAllowanceUnit.aspx.cs" Inherits="Web.CP.OvertimeAllowanceUnit" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Overtime Allowance Unit
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                               <td>
                                    <pr:CalendarExtControl Width="150px" FieldLabel="Month" ID="txtFromDate" runat="server"
                                        LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                                <td>
                                    <pr:CalendarExtControl Width="150px" FieldLabel="To" ID="txtToDate" runat="server" Hidden="true"
                                        LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                                <td>
                                    <ext:ComboBox Width="140" MarginSpec="0 5 0 5" LabelSeparator="" LabelAlign="Top"
                                        QueryMode="Local" ID="cmbShowOnly" DisplayField="Value" ValueField="Text"
                                        runat="server" FieldLabel="Show Only">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                            <ext:ListItem Index="0" Value="1" Text="Overtime">
                                            </ext:ListItem>
                                            <ext:ListItem Index="0" Value="2" Text="Allowance">
                                            </ext:ListItem>
                                        </Items>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name" Hidden="true"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="250" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <%--<Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>--%>
                                          <DirectEvents>
                                            <Click OnEvent="btnSearch_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOverTimeAllowance" runat="server"
                Cls="itemgrid1" OnReadData="Store_ReadData" AutoScroll="true">
                <Store>
                    <ext:Store PageSize="50" ID="storeEmpList" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="Allowance1" Type="String" />
                                    <ext:ModelField Name="Allowance2" Type="String" />
                                    <ext:ModelField Name="Allowance3" Type="String" />
                                    <ext:ModelField Name="Allowance4" Type="String" />
                                    <ext:ModelField Name="Allowance5" Type="String" />
                                    <ext:ModelField Name="Overtime1" Type="String" />
                                    <ext:ModelField Name="Overtime2" Type="String" />
                                    <ext:ModelField Name="Overtime3" Type="String" />
                                    <ext:ModelField Name="Overtime4" Type="String" />
                                    <ext:ModelField Name="Overtime5" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="EmployeeId" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colFromDate" runat="server" Text="SN" Locked="true" DataIndex="RowNumber"
                            MenuDisabled="false" Sortable="false" Align="Right" Width="50" />
                        <ext:Column ID="Column1" runat="server" Text="EIN" Locked="true" DataIndex="EmployeeId"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="50" />
                        <ext:Column ID="Column3" runat="server" Text="Name" Locked="true" DataIndex="Name"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="Column4" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column5" runat="server" Text="Department" DataIndex="Department"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="ColumnGroupOvertime" Text="Overtime" StyleSpec="background-color:#DDEBF7" runat="server">
                            <Columns>
                                <ext:Column ID="ColumnOvertime1" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    Hidden="true" DataIndex="Overtime1" MenuDisabled="false" Sortable="false" Align="Center"
                                    Width="120">
                                </ext:Column>
                                <ext:Column ID="ColumnOvertime2" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    Hidden="true" DataIndex="Overtime2" MenuDisabled="false" Sortable="false" Align="Right"
                                    Width="120">
                                </ext:Column>
                                <ext:Column ID="ColumnOvertime3" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    Hidden="true" DataIndex="Overtime3" MenuDisabled="false" Sortable="false" Align="Right"
                                    Width="120">
                                </ext:Column>
                                <ext:Column ID="ColumnOvertime4" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    Hidden="true" DataIndex="Overtime4" MenuDisabled="false" Sortable="false" Align="Right"
                                    Width="120">
                                </ext:Column>
                                <ext:Column ID="ColumnOvertime5" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    Hidden="true" DataIndex="Overtime5" MenuDisabled="false" Sortable="false" Align="Right"
                                    Width="120">
                                </ext:Column>
                            </Columns>
                        </ext:Column>
                        <ext:Column ID="ColumnGroupAllowances" Text="Allowances" StyleSpec="background-color:#DDEBF7"
                            runat="server">
                            <Columns>
                                <ext:Column ID="ColumnAllowance1" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    Hidden="true" DataIndex="Allowance1" MenuDisabled="false" Sortable="false" Align="Center"
                                    Width="120">
                                    <%--<Renderer Fn="getFormattedAmount" />--%>
                                </ext:Column>
                                <ext:Column ID="ColumnAllowance2" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    Hidden="true" DataIndex="Allowance2" MenuDisabled="false" Sortable="false" Align="Right"
                                    Width="120">
                                </ext:Column>
                                <ext:Column ID="ColumnAllowance3" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    Hidden="true" DataIndex="Allowance3" MenuDisabled="false" Sortable="false" Align="Right"
                                    Width="120">
                                </ext:Column>
                                <ext:Column ID="ColumnAllowance4" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    Hidden="true" DataIndex="Allowance4" MenuDisabled="false" Sortable="false" Align="Right"
                                    Width="120">
                                </ext:Column>
                                <ext:Column ID="ColumnAllowance5" runat="server" Text="" StyleSpec="background-color:#DDEBF7"
                                    Hidden="true" DataIndex="Allowance5" MenuDisabled="false" Sortable="false" Align="Right"
                                    Width="120">
                                </ext:Column>
                            </Columns>
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
