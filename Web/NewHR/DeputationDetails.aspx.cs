﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using Utils.Helper;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Calendar;
using System.IO;
using Utils;

namespace Web.NewHR
{
    public partial class DeputationDetails : BasePage
    {
        BranchManager branchMgr = new BranchManager();
        EmployeeManager empMgr = new EmployeeManager();
        InsuranceManager insMgr = new InsuranceManager();
        CommonManager cmpMgr = new CommonManager();
        List<BranchDepartmentHistory> list = new List<BranchDepartmentHistory>();

        protected void Page_Load(object sender, EventArgs e)
        {
           


            if (!IsPostBack)
            {
                Initialise();
            }
            JavascriptHelper.AttachPopUpCode(Page, "popup", "../cp/BranchTransferLeaveSetting.aspx", 1200, 700);

        }

        public void ddlDeputationList_IndexChanged(object sender, EventArgs e)
        {
            Deputation entity = NewPayrollManager.GetDeputationById(int.Parse(ddlDeputationList.SelectedValue));
            if (entity != null)
            {
                desc.InnerHtml = entity.Description;
                UIHelper.SetSelectedInDropDown(ddlAttendance, entity.AttendanceIn);
                UIHelper.SetSelectedInDropDown(ddlSalary, entity.ApplicableSalaryAllowance);
                UIHelper.SetSelectedInDropDown(ddlTA, entity.TravellingAllowance);
                UIHelper.SetSelectedInDropDown(ddlDA, entity.DailyAllowance);
            }
        }

        void Initialise()
        {
            //calFromDate.IsEnglishCalendar = this.IsEnglish;
            //calFromDate.SelectTodayDate();

            int deputaionId = UrlHelper.GetIdFromQueryString("did");
            if (deputaionId != 0)
                hdn.Value = deputaionId.ToString();

            ddlDeputationList.DataSource = NewPayrollManager.GetDeputationList();
            ddlDeputationList.DataBind();


            ddlAttendance.DataSource = ExtControlHelper.GetTextValues(typeof(DeputationAttendanceIn));
            ddlAttendance.DataBind();

            ddlTA.DataSource = ExtControlHelper.GetTextValues(typeof(DeputationTA));
            ddlTA.DataBind();

            ddlDA.DataSource = ExtControlHelper.GetTextValues(typeof(DeputationDA));
            ddlDA.DataBind();

            ddlSalary.DataSource = ExtControlHelper.GetTextValues(typeof(DeputationApplicableSalaryAllowance));
            ddlSalary.DataBind();

            //calDepartureDate.IsEnglishCalendar = this.IsEnglish;
            //calDepartureDate.SelectTodayDate();

            //calLetterDate.IsEnglishCalendar = this.IsEnglish;
            //calLetterDate.SelectTodayDate();

            List<FinancialDate> years = new CommonManager().GetAllFinancialDates();
            foreach (FinancialDate date in years)
            {
                date.SetName(IsEnglish);
            }

            LoadBranches();

            //load for emp from querystring
            LoadEmpFromQueryString();

            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.RBB)
            {
                HideDeputationForOther();
            }
           
        }
        
        public void HideDeputationForOther()
        {
            rowDeputation1.Visible = false;
            rowDeputation2.Visible = false;
            rowDeputation3.Visible = false;
            rowDeputation4.Visible = false;
            rowDeputationType.Visible = false;
        }

        public int GetDeputationID()
        {
            if (hdn.Value != "")
                return int.Parse(hdn.Value);
            return 0;
        }

        private void LoadEmpFromQueryString()
        {
            int did = GetDeputationID();
            if (did != 0)
            {
                DeputationEmployee ded = NewPayrollManager.GetDeputationEmployeeById(did);

                EEmployee emp = new EmployeeManager().GetById(ded.EmployeeId.Value);

                UIHelper.SetSelectedInDropDown(ddlFilterByBranch, emp.BranchId);

                UIHelper.SetSelectedInDropDown(ddlEmployeeList, emp.EmployeeId.ToString());

                ddlFilterByBranch.Enabled = false;
                ddlEmployeeList.Enabled = false;

                LoadEmployeeDetails(null, null);

                Process(ded);

                btnReview.Visible = true;
                btnReview.OnClientClick = "popupUpdateLeaveCall(" + did + ");return false;";
            }
        }



        public void LoadDepartments(object sender, EventArgs e)
        {
            ListItem item = ddlTransferToDepartment.Items[0];

            ddlTransferToDepartment.DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(ddlTransferToBranch.SelectedValue));
            ddlTransferToDepartment.DataBind();

            ddlTransferToDepartment.Items.Insert(0, item);

            LoadSubDepartments(null, null);
        }
        void LoadBranches()
        {
            ddlFilterByBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlFilterByBranch.DataBind();

            ddlTransferToBranch.DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlTransferToBranch.DataBind();

        
            //if( ddlFilterByBranch.Items.Count >0)
            //{
            //    ddlFilterByBranch.Items[1].Selected = true;
            //    LoadEmployeesByBranch();
            //}
            LoadEmployeesByBranch();
        }

        protected void LoadEmployees(object sender, EventArgs e)
        {
            LoadEmployeesByBranch();
        }

        void LoadEmployeesByBranch()
        {
            //if (ddlFilterByBranch.SelectedValue.Equals("-1") == false)
            {
                ListItem first = ddlEmployeeList.Items[0];
                ddlEmployeeList.DataSource = empMgr.GetEmployeesByBranch(int.Parse(ddlFilterByBranch.SelectedValue), -1);
                ddlEmployeeList.DataBind();
                ddlEmployeeList.Items.Insert(0, first);
            }
        }

        void LoadInsuranceName()
        {
            //ddlInsuranceName.DataSource = insMgr.GetInsuranceName();
            //ddlInsuranceName.DataBind();
        }



        protected void ddlFilterByValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployees();
        }
        void LoadEmployees()
        {
            string type = string.Empty; int id = 0;

        }

        private void BindHistory(int employeeId)
        {
            //PayManager mgr = new PayManager();


            //list = CommonManager.GetBranchDepartmentHistory(employeeId);
            //gvw.DataSource = list;
            //gvw.DataBind();



        }

        private DeputationEmployee Process(DeputationEmployee entity)
        {
            if (entity == null)
            {
                entity = new DeputationEmployee();

                entity.DeputationId = int.Parse(ddlDeputationList.SelectedValue);
                entity.Description = txtDescription.Text.Trim();
                entity.LetterNumber = txtLetterNumber.Text.Trim();
                entity.LetterDate = calLetterDate.Text.Trim();
                entity.LetterDateEng = GetEngDate(entity.LetterDate);
                entity.DeputationDate = calDeputationDate.Text.Trim();
                entity.DeputationDateEng = GetEngDate(entity.DeputationDate);
                entity.BranchId = int.Parse(ddlTransferToBranch.SelectedValue);
                entity.DepeartmentId = int.Parse(ddlTransferToDepartment.SelectedValue);
                entity.CommuteDays = double.Parse(txtCommuteDays.Text.Trim());

                entity.FromDate = calFrom.Text.Trim();
                entity.FromDateEng = GetEngDate(entity.FromDate);
                entity.ToDate = calTo.Text.Trim();
                if (!string.IsNullOrEmpty(entity.ToDate))
                    entity.ToDateEng = GetEngDate(entity.ToDate);

                entity.EmployeeId = int.Parse(ddlEmployeeList.SelectedValue);

                entity.AttendanceIn = int.Parse(ddlAttendance.SelectedValue);
                entity.ApplicableSalaryAllowance = int.Parse(ddlSalary.SelectedValue);
                entity.TravellingAllowance = int.Parse(ddlTA.SelectedValue);
                entity.DailyAllowance = int.Parse(ddlDA.SelectedValue);

                if (!string.IsNullOrEmpty(txtTAAmount.Text.Trim()))
                    entity.TravellingAllowanceAmount = decimal.Parse(txtTAAmount.Text.Trim());
                if (!string.IsNullOrEmpty(txtDAAmount.Text.Trim()))
                    entity.DailyAllowanceAmount = decimal.Parse(txtDAAmount.Text.Trim());


                return entity;
            }
            else
            {
                UIHelper.SetSelectedInDropDown(ddlTransferToBranch, entity.BranchId);

                LoadDepartments(null, null);
                UIHelper.SetSelectedInDropDown(ddlTransferToDepartment, entity.DepeartmentId);

                LoadSubDepartments(null, null);

                UIHelper.SetSelectedInDropDown(ddlDeputationList, entity.DeputationId);
                txtDescription.Text=entity.Description;
                txtLetterNumber.Text=entity.LetterNumber;
                calLetterDate.Text = entity.LetterDate;

                calDeputationDate.Text=entity.DeputationDate;


                txtCommuteDays.Text = entity.CommuteDays.ToString();

                calFrom.Text = entity.FromDate;
                calTo.Text=entity.ToDate;
                UIHelper.SetSelectedInDropDown(ddlEmployeeList, entity.EmployeeId);

                UIHelper.SetSelectedInDropDown(ddlAttendance, entity.AttendanceIn);
                UIHelper.SetSelectedInDropDown(ddlSalary, entity.ApplicableSalaryAllowance);
                UIHelper.SetSelectedInDropDown(ddlTA, entity.TravellingAllowance);
                UIHelper.SetSelectedInDropDown(ddlDA, entity.DailyAllowance);


                if (entity.TravellingAllowanceAmount != null)
                    txtTAAmount.Text = GetCurrency(entity.TravellingAllowanceAmount);
                if (entity.DailyAllowanceAmount != null)
                    txtDAAmount.Text = GetCurrency(entity.DailyAllowanceAmount);
            }
            return null;
        }

        public void LoadSubDepartments(object sender, EventArgs e)
        {
           

        }


        //protected void gvw_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    //first record 
        //    int currentGradeHistory = (int)gvw.DataKeys[gvw.SelectedIndex][0];

        //    //EGradeHistory prevGradeHistory = CommonManager.GetPreviousGradeHistory(currentGradeHistory);

        //    //if (prevGradeHistory != null)
        //    //{
        //    //    RegisterLastFromDateScript(prevGradeHistory);
        //    //}


        //    BranchDepartmentHistory history = CommonManager.GetBranchDepartmentHistoryById(currentGradeHistory);
        //    Process(history);
        //    btnSave.Text = Resources.Messages.Update;

        //    //if (history.IsFirst.Value)
        //    //    calFromDate.Enabled = false;
        //    //else
        //    //    calFromDate.Enabled = true;

        //    int employeeId = int.Parse(ddlEmployeeList.SelectedValue);

        //    //if first one then disable from date as it should be the same of emp first status from date
        //    if (CommonManager.GetBranchDepartmentHistory(employeeId)[0].BranchDepartmentId == history.BranchDepartmentId)
        //    {
        //        calFromDate.ToolTip = Resources.Messages.HistoryFirstFromDateNotEditable;
        //        //calFromDate.Enabled = false;
        //    }
        //    else
        //    {
        //        calFromDate.Enabled = true; ;
        //        calFromDate.ToolTip = "";
        //    }

        //}

        public string GetStatus(object value)
        {
            if (value == null)
                return "To be confirmed";
            if (int.Parse(value.ToString()) == 0)
                return "To be confirmed";
            return "Confirmed";
        }
        public bool IsEditable(int index)
        {
            if (list != null && list.Count > 0)
            {
                if (list.Count - 1 == index)
                    return true;
            }
            return false;
        }

        public void LoadEmployeeDetails(object sender, EventArgs e)
        {
            int employeeId = int.Parse(ddlEmployeeList.SelectedValue);

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            if (emp == null)
            {
                buttonsBar.Visible = false;
                divWarningMsg.InnerHtml = "Please select an employee.";
                divWarningMsg.Hide = false;
                return;
            }
            else
                buttonsBar.Visible = true;

            CommonManager.SaveFirstIBranchfNotExists(employeeId);
            BindHistory(employeeId);


            

            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.ImageUrl)))
                        hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
                image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));
            }

            lblName.Text = emp.Title + " " + emp.Name;
            lblBranch.Text = emp.Branch.Name;
            lblDepartment.Text = emp.Department.Name;

            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            GetElapsedTime(firstStatus.FromDateEng, BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);

            lblSince.Text = "Since " +
                (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");
            lblTime.Text  = string.Format("For {0} years {1} months {2} days",
                years,months,days);


           
        }

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        private void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //this.Value = "true";

                DeputationEmployee history = Process(null);
                history.EmployeeId = int.Parse(ddlEmployeeList.SelectedValue);


                if (GetDeputationID() != 0)
                {

                    history.DeputationEmployeeId = GetDeputationID();
                    Status status = NewPayrollManager.InsertUpdateEmployeeDeputation(history, false);

                    //if (IsBranchTransfer)
                    divMsgCtl.InnerHtml = "Deputation updated.";
                    //else
                    //    divMsgCtl.InnerHtml = "Department transfer updated.";
                }
                else
                {
                    Status status = NewPayrollManager.InsertUpdateEmployeeDeputation(history, true);
                    //if (IsBranchTransfer)
                    divMsgCtl.InnerHtml = "Deputation saved.";

                    hdn.Value = history.DeputationEmployeeId.ToString();
                    ddlFilterByBranch.Enabled = false;
                    ddlEmployeeList.Enabled = false;
                    btnSave.Text = "Update";
                    //else
                    //    divMsgCtl.InnerHtml = "Department transfer saved.";
                }
                divMsgCtl.Hide = false;

                // Prepare Sub-Department List
                //StringBuilder str = new StringBuilder("");
                //bool first = true;
                //List<SubDepartment> deps = DepartmentManager.GetAllSubDepartmentsByDepartment(history.DepeartmentId.Value);
                //foreach (SubDepartment obj in deps)
                //{
                //    if (first == false)
                //        str.Append(",");
                //    str.Append("\"" + obj.SubDepartmentId + "$$" + obj.Name + "\"");
                //    first = false;
                //}
                ////Page.ClientScript.RegisterArrayDeclaration("texts", str.ToString());

                // Register to set Branch/Subdepartment from js in Employee page

                btnReview.Visible = true;
                btnReview.OnClientClick = "popupUpdateLeaveCall(" + history.DeputationEmployeeId + ");return false;";


                ClearFields();
                btnCancel_Click(null, null);
            }
        }

        IIndividualInsurance ProcessInsurance(IIndividualInsurance indiv)
        {

            return null;
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //btnSave.Text = "Save";

            //int employeeId = int.Parse(ddlEmployeeList.SelectedValue);

            //if (employeeId != -1)
            //{

            //    gvw.SelectedIndex = -1;
            //    CommonManager.SaveFirstIBranchfNotExists(employeeId);
            //    BindHistory(employeeId);

            //    ClearFields();
            //}
        }

        void ClearFields()
        {
            //UIHelper.SetSelectedInDropDown(ddlTransferToBranch, -1);
            //UIHelper.SetSelectedInDropDown(ddlTransferToDepartment, -1);
            //txtNote.Text = "";
            //calFromDate.Text = "";  
            //calDepartureDate.Text = "";
            //calLetterDate.Text = "";
            //calFromDate.Enabled = true;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Button is hidden as for internal use only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
        }

    }
}

