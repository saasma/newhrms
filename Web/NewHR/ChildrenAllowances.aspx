﻿<%@ Page Title="Children Allowances" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ChildrenAllowances.aspx.cs" Inherits="Web.NewHR.ChildrenAllowances" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/newhr/UserControls/ChildrenAllowanceCtl.ascx" TagName="ChildrenAllowanceCtl"
    TagPrefix="ucChildrenAllowance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
     <script type="text/javascript" src="../Employee/override.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    var blockMultipleFormSubmit = true;
    function refreshWindow() {
        <%=btnLoad.ClientID %>.fireEvent('click');
        <%=windowAllowance.ClientID %>.hide();
    }

      function searchList() {
             <%=gridLoanRepayment.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

            var ChildAllowanceCommandHandler = function(command, record){
                <%= hdn.ClientID %>.setValue(record.data.ChildrenAllowanceID);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }
            }
            

        function focusEvent(e1, tab) {
        if (tab.title.toString().toLowerCase() == "active allowances") {
         <%= hdnTabStatus.ClientID %>.setValue(tab.title.toString().toLowerCase());
           <%= btnTabChange.ClientID %>.fireEvent('click');
          
        }
           else if (tab.title.toString().toLowerCase() == "started this month") {
             <%= hdnTabStatus.ClientID %>.setValue(tab.title.toString().toLowerCase());
             <%= btnTabChange.ClientID %>.fireEvent('click');
        }
         
            else if (tab.title.toString().toLowerCase() == "ending this month") {
             <%= hdnTabStatus.ClientID %>.setValue(tab.title.toString().toLowerCase());
             <%= btnTabChange.ClientID %>.fireEvent('click');
        }
           else if (tab.title.toString().toLowerCase() == "inactive allowances") {
             <%= hdnTabStatus.ClientID %>.setValue(tab.title.toString().toLowerCase());
             <%= btnTabChange.ClientID %>.fireEvent('click');
        }

    };

    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4 runat="server" id="title">
                    Children Allowances</h4>
            </div>
        </div>
    </div>
    <ext:Hidden ID="hdnTabStatus" runat="server" Text="active allowances" />
    <ext:LinkButton ID="btnTabChange" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnTabChange_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Hidden runat="server" Hidden="true" ID="hdn">
    </ext:Hidden>
    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
        <Proxy>
            <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                <ActionMethods Read="GET" />
                <Reader>
                    <ext:JsonReader Root="plants" TotalProperty="total" />
                </Reader>
            </ext:AjaxProxy>
        </Proxy>
        <Model>
            <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                <Fields>
                    <ext:ModelField Name="Name" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <div class="contentpanel">
        <div class="attribute" style="">
            <div style="clear: both;">
                <ext:Button ID="btnAssignAllowance" Cls="btn btn-primary" Height="30" runat="server"
                    Text="Assign Allowance">
                    <DirectEvents>
                        <Click OnEvent="btnAssignAllowance_Click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <br />
                <ext:TabPanel ID="TabPanel_Main" ActiveIndex="0" Border="false" Unstyled="true" runat="server"
                    Width="1150" Plain="true" OverflowY="Auto">
                    <Items>
                        <ext:Panel ID="Panel1" runat="server" AutoHeight="true" Title="Active Allowances"
                            Header="False" Border="false" OverflowY="Auto">
                        </ext:Panel>
                        <ext:Panel ID="Panel2" AutoHeight="true" runat="server" Title="Started This Month"
                            Header="False" Border="false">
                        </ext:Panel>
                        <ext:Panel ID="Panel3" AutoHeight="true" runat="server" Title="Ending This Month"
                            Header="False" Border="false">
                            <Content>
                            </Content>
                        </ext:Panel>
                        <ext:Panel ID="Panel4" AutoHeight="true" runat="server" Title="Inactive Allowances"
                            Header="False" Border="false">
                            <Content>
                            </Content>
                        </ext:Panel>
                    </Items>
                    <Listeners>
                        <TabChange Fn="focusEvent">
                        </TabChange>
                    </Listeners>
                </ext:TabPanel>
            </div>
            <table class="fieldTable firsttdskip">
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbSearch" FieldLabel="Employee" LabelWidth="70" LabelAlign="Top"
                            LabelSeparator="" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" EmptyText="Search Employee" TypeAhead="false" Width="180"
                            PageSize="9999" HideBaseTrigger="true" MinChars="1" TriggerAction="All" ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:ComboBox ID="cmbAllowance" runat="server" ValueField="IncomeId" DisplayField="Title"
                            FieldLabel="Allowance" Width="180" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="storeAllowance" runat="server">
                                    <Model>
                                        <ext:Model ID="modelall" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="IncomeId" Type="String" />
                                                <ext:ModelField Name="Title" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue();
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style='width: 500px; padding-top: 25px'>
                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-default btn-sect btn-sm" Width="100"
                            Height="30" Text="Show">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                    <td style="padding-top: 25px">
                        <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                            Height="30" Text="<i></i>Export To Excel">
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
        <div style="clear: both" runat="server" id="gridContainer">
        </div>
        <br />
        <ext:GridPanel ID="gridLoanRepayment" ClicksToEdit="1" Border="true" StripeRows="true"
            Header="false" runat="server" Width="1000" Title="Title" Cls="gridtbl">
            <Store>
                <ext:Store ID="storeLoanRepayment" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model1" runat="server" IDProperty="ChildrenAllowanceID">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="ChildrenAllowanceID" Type="Int" />
                                <ext:ModelField Name="ChildrenName" Type="String" />
                                <ext:ModelField Name="Relation" Type="String" />
                                <ext:ModelField Name="Income" Type="String" />
                                <ext:ModelField Name="Dob" Type="Date" />
                                <ext:ModelField Name="Amount" Type="Date" />
                                <ext:ModelField Name="StartDate" Type="Date" />
                                <ext:ModelField Name="EndDate" Type="Date" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <Listeners>
            </Listeners>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel11" runat="server">
                </ext:RowSelectionModel>
            </SelectionModel>
            <ColumnModel runat="server" ID="columnModel">
                <Columns>
                    <ext:Column ID="Column1" runat="server" Sortable="false" MenuDisabled="true" Header="EIN"
                        Width="50" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="Column4" runat="server" Sortable="false" MenuDisabled="true" Header="Employee Name"
                        Width="150" DataIndex="EmployeeName">
                    </ext:Column>
                    <ext:Column runat="server" Sortable="false" MenuDisabled="true" Header="Children Name"
                        Width="130" ColumnID="EmployeeName" DataIndex="ChildrenName">
                    </ext:Column>
                    <ext:Column ID="Column2" runat="server" Sortable="false" MenuDisabled="true" Header="Relation"
                        Width="90" ColumnID="EmployeeName" DataIndex="Relation">
                    </ext:Column>
                    <ext:Column ID="Column6" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="Income" Width="150" DataIndex="Income">
                    </ext:Column>
                    <ext:DateColumn ID="colAmount" runat="server" Sortable="false" Align="Right" MenuDisabled="true"
                        Header="DOB" Width="100" ColumnID="DOB" DataIndex="Dob">
                    </ext:DateColumn>
                    <ext:DateColumn ID="NumberColumn1" runat="server" Sortable="false" Align="Right"
                        MenuDisabled="true" Header="Start Date" Width="100" ColumnID="AdvanceLoanAmount"
                        DataIndex="StartDate">
                    </ext:DateColumn>
                    <ext:DateColumn ID="Column5" runat="server" Sortable="false" Align="Center" MenuDisabled="true"
                        Header="End Date" Width="100" DataIndex="EndDate">
                    </ext:DateColumn>
                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="120" Text="Actions"
                        Align="Center">
                        <Commands>
                            <ext:CommandSeparator />
                            <ext:GridCommand Text="Edit" CommandName="Edit" />
                            <ext:GridCommand Text="Delete" CommandName="Delete" />
                        </Commands>
                        <Listeners>
                            <Command Handler="ChildAllowanceCommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeLoanRepayment"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
    </div>
    <ext:Window ID="windowAllowance" runat="server" Title="Add Children Allowance"
        Icon="Application" Height="500" Width="650" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <ucChildrenAllowance:ChildrenAllowanceCtl Id="ChildrenAllowanceCtl1" EnableViewState="false"
                Hide="false" runat="server" />
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
