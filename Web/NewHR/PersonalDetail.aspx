﻿<%@ Page Title="Personal Details" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="PersonalDetail.aspx.cs" Inherits="Web.NewHR.PersonalDetail" %>

<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .groupLevelUL
        {
            padding: 0px;
            margin: 0px;
        }
        .groupLevelUL li
        {
            float: left;
            width: 180px;
            padding-right: 15px;
            list-style: none;
        }
        .userDetailsClass
        {
            background-color: #D8E7F3;
            height: 50px;
            text-align: center;
            padding-top: 10px;
            margin-bottom: 10px;
        }
    </style>
    <script type="text/javascript">
        function titleChanged(title, cmbGender) {
            //if (cmbGender.getValue() == null) 
            {
                if (title == "Mr")
                    cmbGender.setValue("1");
                else if (title == "Mrs" || title == "Miss")
                    cmbGender.setValue("0");
            }
        }


        function promoteCall(statusId) {
            if (typeof (EmployeeId) != 'undefined') {
                var ret = promote("EId=" + parseInt(EmployeeId) + "&statusId=" + statusId);
                if (typeof (ret) != 'undefined') {
                    if (ret == 'ReloadIncome') {
                        //refreshIncomeList(0);
                    }
                }
            }
            return false;
        }

        function onStatusDeleteCallback(result) {
            if (result) {



                editId = 0;

                //document.getElementById(ddlStatusId).selectedIndex = 0;
                $('#<%= empStatusList.ClientID %>').html(result);




            }
            hideLoading();
        }

        function loadAndHide(win) {
            win.close();
            showLoading();
            Web.PayrollService.GetStatusList(parseInt(EmployeeId), onStatusDeleteCallback);

        }

        var editId = 0;


        function deleteStatus(statusId) {
            if (confirm("Confirm delete status?")) {

                showLoading();
                Web.PayrollService.DeleteEmployeeStatus(statusId, onStatusDeleteCallback);


            }
            return false;
        }
        function elementsEnabled() {

            return true;
        }

        Ext.onReady(
            function () {
                document.getElementById('tdChangeStatus').style.display = 'block';
            }
        );


        function ChangeProbation(statusId) {
            if (typeof (EmployeeId) != 'undefined') {
                var ret = changeProbation("EId=" + parseInt(EmployeeId) + "&statusId=" + statusId);
                if (typeof (ret) != 'undefined') {
                    if (ret == 'ReloadIncome') {
                        //refreshIncomeList(0);
                    }
                }
            }
            return false;
        }

        function probationCallBack(result) {
            if (result) {
                editId = 0;

                $('#<%= empProbationList.ClientID %>').html(result);
            }
        }

        function deleteProbation(statusId) {
            if (confirm("Confirm delete the probation?")) {

                showLoading();
                Web.PayrollService.DeleteEmployeeProbation(statusId, probationCallBack);


            }
            return false;
        }

        function loadAndHideProbation(win) {
            win.close();
            showLoading();

            Web.PayrollService.GetProbationList(parseInt(EmployeeId), probationCallBack);

        }

        function confirmDoNotCalculateSalary(value) {
            if (value == "true") {
                Ext.Msg.show({
                    title: 'Do Not Calculate Salary Confirmation', //<- the dialog's title  
                    msg: 'Are you sure, to set Not to calculate Salary for this Employee, if not change the setting to "No"?', //<- the message  
                    buttons: Ext.Msg.OK, //<- YES and NO buttons  
                    icon: Ext.Msg.WARNING, // <- error icon  
                    minWidth: 300
                });
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body" runat="server" id="divTitle" style="margin-left: 145px;">
                <h4>
                    Employee Personal Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <%--<uc2:EmployeeWizard runat="server" Id="EmployeeWizard1" />--%>
            <table>
                <tr>
                    <td valign="top">
                        <%--<uc2:EmployeeWizard runat="server"  runat="server" id="EmployeeWizard1"/>--%>
                        <div style="float: left; margin-right: 25px;">
                            <ucEW:EmpDetailsWizard Id="EmployeeWizard1" runat="server" />
                        </div>
                    </td>
                    <td valign="top">
                        <div class="userDetailsClass" runat="server" id="divEmployeeDetails">
                            <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                        </div>
                        <ext:Container ID="lblMsg" runat="server" />
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <!-- panel-btns -->
                                <h4 class="panel-title">
                                    <i></i>Personal Details
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body">
                                <div runat="server" id="navigation" style="float: right; margin-bottom: -100px; display: none">
                                    &nbsp;&nbsp;
                                    <ext:HyperLink ID="btnFirst" StyleSpec="font-size: 20px;" runat="server" Text="<<"
                                        Font-Bold="true" />
                                    &nbsp;&nbsp;
                                    <ext:HyperLink ID="btnPrevious" StyleSpec="font-size: 20px;" runat="server" Text="<"
                                        Font-Bold="true" />
                                    &nbsp;&nbsp;
                                    <ext:HyperLink ID="btnNextEmployee" StyleSpec="font-size: 20px;" runat="server" Text=">"
                                        Font-Bold="true" />
                                    &nbsp;&nbsp;
                                    <ext:HyperLink ID="btnLast" StyleSpec="font-size: 20px;" runat="server" Text=">>"
                                        Font-Bold="true" />
                                </div>
                                <span runat="server" id="spanRetiredDate" style="width: 500px; color: Red"></span>
                                <table class="fieldTable firsttdskip">
                                    <tr>
                                        <td>
                                            <ext:TextField Hidden="true" ID="txtEIN" FieldLabel="EIN *" Disabled="true" runat="server"
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:TextField>
                                            <asp:CompareValidator ID="RequiredFieldValidator4" Operator="GreaterThanEqual" ValueToCompare="1"
                                                Type="Integer" ErrorMessage="EIN number must be integer type." Display="None"
                                                ControlToValidate="txtEIN" ValidationGroup="InsertUpdate" runat="server">
                                            </asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbTitle" FieldLabel="Title *"
                                                runat="server" LabelAlign="Top" DisplayField="Key" Width="180" LabelSeparator=""
                                                ValueField="Value">
                                                <Store>
                                                    <ext:Store ID="Store1" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Key" />
                                                            <ext:ModelField Name="Value" />
                                                        </Fields>
                                                    </ext:Store>
                                                </Store>
                                                <Listeners>
                                                    <Select Handler="titleChanged(this.getValue(),#{cmbGender});">
                                                    </Select>
                                                </Listeners>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select employee title."
                                                Display="None" ControlToValidate="cmbTitle" ValidationGroup="InsertUpdate" runat="server">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <ext:ComboBox ForceSelection="true" QueryMode="Local" ID="cmbGender" FieldLabel="Gender *"
                                                runat="server" LabelAlign="Top" Width="180" LabelSeparator="">
                                                <Items>
                                                    <ext:ListItem Text="Male" Value="1" />
                                                    <ext:ListItem Text="Female" Value="0" />
                                                    <ext:ListItem Text="Third Gender" Value="2" />
                                                </Items>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator ErrorMessage="Please select a Gender." Display="None"
                                                ID="RequiredFieldValidator2" ControlToValidate="cmbGender" ValidationGroup="InsertUpdate"
                                                runat="server">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtFirstNameEng" FieldLabel="First Name *" runat="server" LabelAlign="Top"
                                                Width="180" LabelSeparator="" StyleSpec="margin-top:7px;">
                                            </ext:TextField>
                                            <asp:RequiredFieldValidator ErrorMessage="First name is required." Display="None"
                                                ID="RequiredFieldValidator9" ControlToValidate="txtFirstNameEng" ValidationGroup="InsertUpdate"
                                                runat="server">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtMiddleNameEng" FieldLabel="Middle Name" runat="server" LabelAlign="Top"
                                                Width="180" LabelSeparator="" StyleSpec="margin-top:7px;">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtLastNameEng" FieldLabel="Last Name *" runat="server" LabelAlign="Top"
                                                Width="180" LabelSeparator="" StyleSpec="margin-top:7px;">
                                            </ext:TextField>
                                            <asp:RequiredFieldValidator ErrorMessage="Last name is required." Display="None"
                                                ID="RequiredFieldValidator10" ControlToValidate="txtLastNameEng" ValidationGroup="InsertUpdate"
                                                runat="server">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <ext:TextField ID="txtNameNep" FieldCls="preetifont" FieldLabel="नाम [नेपालीमा]"
                                                runat="server" LabelAlign="Top" Width="375" LabelSeparator="" StyleSpec="margin-top:7px;font-family: MYPREETI">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtFatherName" FieldLabel="Father's Name" runat="server" LabelAlign="Top"
                                                Width="180" LabelSeparator="" StyleSpec="margin-top:7px;">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtMotherName" FieldLabel="Mother's Name" runat="server" LabelAlign="Top"
                                                Width="180" LabelSeparator="" StyleSpec="margin-top:7px;">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:DateField ID="txtDOB" runat="server" FieldLabel="Date of Birth(English) *" EmptyText=""
                                                LabelAlign="Top" Width="180" LabelSeparator="">
                                                <DirectEvents>
                                                    <Blur OnEvent="txtDOBEng_Change">
                                                    </Blur>
                                                </DirectEvents>
                                            </ext:DateField>
                                            <asp:RequiredFieldValidator ErrorMessage="Please enter the date of birth." Display="None"
                                                ID="RequiredFieldValidator6" ControlToValidate="txtDOB" ValidationGroup="InsertUpdate"
                                                runat="server">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <ext:TextField Width="180px" FieldLabel="Nepali DOB (yyyy/mm/dd) *" ID="txtDOBNep"
                                                runat="server" LabelAlign="Top" LabelSeparator="">
                                                <DirectEvents>
                                                    <Blur OnEvent="txtDOBNep_Change">
                                                        <EventMask ShowMask="true" />
                                                    </Blur>
                                                </DirectEvents>
                                            </ext:TextField>
                                            <%-- <asp:RequiredFieldValidator ErrorMessage="DOB is required." Display="None" ID="RequiredFieldValidator8"
                                ControlToValidate="txtDOBEng" ValidationGroup="InsertUpdate" runat="server">
                            </asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtPOB" FieldLabel="Place Of Birth" runat="server" LabelAlign="Top"
                                                Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtAge" FieldLabel="Age" runat="server" LabelAlign="Top" Disabled="true"
                                                Width="180" LabelSeparator="">
                                            </ext:TextField>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:ComboBox ID="cmbMarried" ForceSelection="true" Note="For Couple tax set from Payroll -> Tax Status"
                                                QueryMode="Local" FieldLabel="Marital Status *" runat="server" LabelAlign="Top"
                                                DisplayField="Key" ValueField="Value" Width="180" LabelSeparator="">
                                                <Store>
                                                    <ext:Store ID="storeMarritalStatus" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Key" />
                                                            <ext:ModelField Name="Value" />
                                                        </Fields>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator ErrorMessage="Please select a Marital Status." Display="None"
                                                ID="RequiredFieldValidator5" ControlToValidate="cmbMarried" ValidationGroup="InsertUpdate"
                                                runat="server">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td valign="top">
                                            <ext:DateField ID="txtMarriageAnniversary" runat="server" FieldLabel="Marriage Anniversary"
                                                EmptyText="" LabelAlign="Top" Width="180" LabelSeparator="">
                                            </ext:DateField>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:ComboBox ID="cmbReligion" ForceSelection="true" QueryMode="Local" FieldLabel="Religion"
                                                runat="server" LabelAlign="Top" DisplayField="ReligionName" Width="180" LabelSeparator=""
                                                ValueField="ReligionName">
                                                <Store>
                                                    <ext:Store ID="storeReligion" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="ReligionName" Type="String" />
                                                            <ext:ModelField Name="ReligionName" Type="String" />
                                                        </Fields>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                            <%-- <asp:RequiredFieldValidator ErrorMessage="Religion is required." Display="None" ID="RequiredFieldValidator7"
                                                    ControlToValidate="cmbReligion" ValidationGroup="InsertUpdate" runat="server">
                                                </asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td>
                                            <ext:ComboBox ID="cmbEthnicity" QueryMode="Local" FieldLabel="Ethnicity" runat="server"
                                                ForceSelection="true" LabelAlign="Top" DisplayField="CasteName" ValueField="CasteId"
                                                Width="180" LabelSeparator="">
                                                <Store>
                                                    <ext:Store ID="storeEthnicity" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model12" IDProperty="CasteId" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="CasteId" Type="String" />
                                                                    <ext:ModelField Name="CasteName" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </td>
                                        <td>
                                            <ext:ComboBox ID="cmbHolidayGroup" QueryMode="Local" FieldLabel="Holiday Group" runat="server"
                                                ForceSelection="true" LabelAlign="Top" DisplayField="Name" ValueField="GroupId"
                                                Width="180" LabelSeparator="">
                                                <Store>
                                                    <ext:Store ID="store14" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model15" IDProperty="GroupId" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="GroupId" Type="String" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                 <Triggers>
                                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                </Triggers>
                                                <Listeners>
                                                    <Select Handler="this.getTrigger(0).show();" />
                                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                    <TriggerClick Handler="if (index == 0) { 
                                                           this.clearValue(); 
                                                           this.getTrigger(0).hide();
                                                       }" />
                                                </Listeners>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtMotherTongue" FieldLabel="Mother Tongue" runat="server" LabelAlign="Top"
                                                Width="180" LabelSeparator="" StyleSpec="margin-top:7px;">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtChildren" FieldLabel="Children" runat="server" LabelAlign="Top"
                                                Width="180" LabelSeparator="" StyleSpec="margin-top:7px;">
                                            </ext:TextField>
                                            <asp:CompareValidator ErrorMessage="Invalid children value." Display="None" Type="Integer"
                                                Operator="DataTypeCheck" ID="RequiredFieldValidator11" ControlToValidate="txtChildren"
                                                ValidationGroup="InsertUpdate" runat="server">
                                            </asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtHobby" FieldLabel="Hobby" runat="server" LabelAlign="Top" Width="180"
                                                LabelSeparator="" StyleSpec="margin-top:7px;">
                                            </ext:TextField>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                                <asp:RequiredFieldValidator ErrorMessage="You have not entered employee's First Name."
                                    Display="None" ID="RequiredFieldValidator3" ControlToValidate="txtFirstNameEng"
                                    ValidationGroup="InsertUpdate" runat="server">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <!-- panel-btns -->
                                <h4 class="panel-title">
                                    <i></i>Position and Job
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body">
                                <table>
                                    <tr>
                                        <td valign="top">
                                            <table class="fieldTable firsttdskip">
                                                <tr>
                                                    <td>
                                                        <ext:ComboBox ID="cmbHireMethod" Width="180px" runat="server" ValueField="HireMethodId"
                                                            DisplayField="HireMethodName" FieldLabel="Hire Method" LabelAlign="top" LabelSeparator=""
                                                            ForceSelection="true" QueryMode="Local">
                                                            <Store>
                                                                <ext:Store ID="Store7" runat="server">
                                                                    <Model>
                                                                        <ext:Model ID="Model7" runat="server">
                                                                            <Fields>
                                                                                <ext:ModelField Name="HireMethodId" Type="String" />
                                                                                <ext:ModelField Name="HireMethodName" Type="String" />
                                                                            </Fields>
                                                                        </ext:Model>
                                                                    </Model>
                                                                </ext:Store>
                                                            </Store>
                                                        </ext:ComboBox>
                                                        <%-- <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator17" runat="server"
                                            ValidationGroup="InsertUpdate" ControlToValidate="cmbHireMethod" ErrorMessage="Hire method is required." />--%>
                                                    </td>
                                                    <td>
                                                        <pr:CalendarExtControl Width="180px" FieldLabel="Appointment/Hire date *" ID="calHireDate"
                                                            runat="server" LabelAlign="Top" LabelSeparator="">
                                                            <%--   <DirectEvents>
                                                                <Blur OnEvent="calHireDate_Blur">
                                                                    <EventMask ShowMask="true" />
                                                                </Blur>
                                                            </DirectEvents>--%>
                                                        </pr:CalendarExtControl>
                                                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator18" runat="server"
                                                            ValidationGroup="InsertUpdate" ControlToValidate="calHireDate" ErrorMessage="Please enter employee Hire Date." />
                                                    </td>
                                                    <td>
                                                        <ext:ComboBox ID="cmbStatus" Width="180px" runat="server" ValueField="Key" DisplayField="Value"
                                                            FieldLabel="Job Status *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                            QueryMode="Local">
                                                            <Store>
                                                                <ext:Store ID="Store8" runat="server">
                                                                    <Model>
                                                                        <ext:Model ID="Model8" runat="server">
                                                                            <Fields>
                                                                                <ext:ModelField Name="Key" />
                                                                                <ext:ModelField Name="Value" />
                                                                            </Fields>
                                                                        </ext:Model>
                                                                    </Model>
                                                                </ext:Store>
                                                            </Store>
                                                            <DirectEvents>
                                                                <Select OnEvent="cmbStatus_Change">
                                                                    <EventMask ShowMask="true" />
                                                                </Select>
                                                            </DirectEvents>
                                                        </ext:ComboBox>
                                                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator19" runat="server"
                                                            ValidationGroup="InsertUpdate" ControlToValidate="cmbStatus" ErrorMessage="Please select a Job Status." />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <ext:TextField ID="txtAppointmentLetterNo" FieldLabel="Appointment Letter No" runat="server"
                                                            LabelAlign="Top" Width="180" LabelSeparator="">
                                                        </ext:TextField>
                                                    </td>
                                                    <td>
                                                        <ext:Checkbox ID="chkContractEndDate" runat="server" LabelSeparator="" BoxLabel="Hire Contract End Date"
                                                            BoxLabelAlign="After" LabelWidth="150">
                                                            <Listeners>
                                                                <Change Handler="if(this.getValue()==true) #{calContractEndDate}.enable(); else #{calContractEndDate}.disable();" />
                                                            </Listeners>
                                                        </ext:Checkbox>
                                                        <pr:CalendarExtControl Disabled="true" Width="180px" ID="calContractEndDate" runat="server"
                                                            LabelAlign="Top" LabelSeparator="" />
                                                    </td>
                                                    <td>
                                                        <ext:TextField ID="txtHireReference" FieldLabel="Reference" runat="server" LabelAlign="Top"
                                                            Width="180" LabelSeparator="">
                                                        </ext:TextField>
                                                        <asp:RequiredFieldValidator Display="None" ID="reqdRef" runat="server" ValidationGroup="InsertUpdate"
                                                            ControlToValidate="txtHireReference" ErrorMessage="Reference is required." />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <pr:CalendarExtControl Width="180px" FieldLabel="Appointment Letter Date" ID="calAppointLetterDate"
                                                            runat="server" LabelAlign="Top" LabelSeparator="" />
                                                    </td>
                                                    <td>
                                                        <ext:Checkbox ID="chkSalaryCalculateDate" runat="server" LabelSeparator="" BoxLabel="Salary Calculation Date"
                                                            BoxLabelAlign="After" LabelWidth="150">
                                                            <Listeners>
                                                                <Change Handler="if(this.getValue()==true) {#{calSalaryCalculationDate}.enable();#{calSalaryCalculationDate}.readOnly=false;} else #{calSalaryCalculationDate}.disable();" />
                                                            </Listeners>
                                                        </ext:Checkbox>
                                                        <pr:CalendarExtControl Disabled="true" Width="180px" ID="calSalaryCalculationDate"
                                                            runat="server" LabelSeparator="" LabelAlign="Top" />
                                                    </td>
                                                    <td>
                                                       <ext:ComboBox ID="cmbPayGroup" Hidden="true" Width="180px" runat="server" ValueField="PayGroupID" DisplayField="Name"
                                                            FieldLabel="Pay Group" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                            QueryMode="Local">
                                                            <Store>
                                                                <ext:Store ID="Store13" runat="server">
                                                                    <Model>
                                                                        <ext:Model ID="Model14" runat="server">
                                                                            <Fields>
                                                                                <ext:ModelField Name="PayGroupID" Type="String" />
                                                                                <ext:ModelField Name="Name" />
                                                                            </Fields>
                                                                        </ext:Model>
                                                                    </Model>
                                                                </ext:Store>
                                                            </Store>
                                                            <SelectedItems>
                                                                <ext:ListItem Index="0" />
                                                            </SelectedItems>
                                                        </ext:ComboBox>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trExtendProbation1" visible="false">
                                                    <td>
                                                        <ext:Checkbox ID="chkHasProbation" runat="server" LabelSeparator="" BoxLabel="Define Probation Period"
                                                            BoxLabelAlign="After" LabelWidth="150">
                                                            <Listeners>
                                                                <Change Handler="if(this.getValue()==true) {#{calProbationStart}.enable();#{calProbationEnd}.enable();} else {#{calProbationStart}.disable();#{calProbationEnd}.disable();}" />
                                                            </Listeners>
                                                        </ext:Checkbox>
                                                        <pr:CalendarExtControl Width="180px" Disabled="true" FieldLabel="Probation Start Date"
                                                            ID="calProbationStart" runat="server" LabelAlign="Top" LabelSeparator="" />
                                                    </td>
                                                    <td style="padding-top: 29px;">
                                                        <pr:CalendarExtControl Disabled="true" FieldLabel="Probation End Date" Width="180px"
                                                            ID="calProbationEnd" runat="server" LabelSeparator="" LabelAlign="Top" />
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trExtendProbation2" visible="false">
                                                    <td colspan="3">
                                                        <div class="removePadding" id="empProbationList" runat="server">
                                                        </div>
                                                        <asp:Button ID="btnProbation" Width="120px" CssClass="btn btn-primary btn-sm" Style="margin-top: 10px"
                                                            runat="server" OnClientClick="return ChangeProbation(0);" Text="Extend Probation" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" style="padding-left: 15px; display: none" id="tdChangeStatus">
                                            <span>Change Status</span>
                                            <div class="removePadding" id="empStatusList" runat="server">
                                            </div>
                                            <asp:Button ID="btnPromote" Width="100px" CssClass="btn btn-primary btn-sm" Style="margin-top: 10px"
                                                runat="server" OnClientClick="return promoteCall(0);" Text="Change Status" />
                                        </td>
                                    </tr>
                                </table>
                                <hr style="margin-top: 10px;" />
                                <table class="fieldTable firsttdskip">
                                    <tr>
                                        <td>
                                            <ext:Checkbox runat="server" ID="chkExcludeNonMatrixSalary"  
                                                BoxLabel="Exclude Salary Matrix">
                                                <DirectEvents>
                                                    <Change OnEvent="cmbStatus_Change" >
                                                        <EventMask ShowMask="true" />
                                                    </Change>
                                                </DirectEvents>
                                                </ext:Checkbox>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="padding-left: 0px;" runat="server" id="td1">
                                            <ext:ComboBox ID="cmbUnit" Hidden="true" Width="180px" runat="server" ValueField="UnitId"
                                                DisplayField="Name" FieldLabel="Unit *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                              
                                                <Store>
                                                    <ext:Store ID="Store15" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model16" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="UnitId" Type="String" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                          
                                        </td>

                                    </tr>
                                    <tr>
                                        <td style="padding-left: 0px;" runat="server" id="tdGroup">
                                            <ext:ComboBox ID="cmbGroup" Width="180px" runat="server" ValueField="LevelGroupId"
                                                DisplayField="Name" FieldLabel="Group *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                                <DirectEvents>
                                                    <Select OnEvent="cmbGroup_Change">
                                                        <EventMask ShowMask="true" />
                                                    </Select>
                                                </DirectEvents>
                                                <Store>
                                                    <ext:Store ID="Store2" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="LevelGroupId" Type="String" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator Display="None" ID="valGroup" runat="server" ValidationGroup="InsertUpdate"
                                                ControlToValidate="cmbGroup" ErrorMessage="Please select employee Group." />
                                        </td>
                                        <td runat="server" id="tdLevel">
                                            <ext:ComboBox ID="cmbLevel" Width="180px" runat="server" ValueField="LevelId" DisplayField="Name"
                                                FieldLabel="Level *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store3" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model1" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="LevelId" Type="String" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="cmbLevel_Change">
                                                        <EventMask ShowMask="true" />
                                                    </Select>
                                                </DirectEvents>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator Display="None" ID="valLevel" runat="server" ValidationGroup="InsertUpdate"
                                                ControlToValidate="cmbLevel" ErrorMessage="Please select employee Level." />
                                        </td>
                                        <td>
                                            <ext:ComboBox ID="cmbPosition" Width="180px" runat="server" ValueField="DesignationId"
                                                DisplayField="Name" FieldLabel="Designation *" LabelAlign="top" LabelSeparator=""
                                                ForceSelection="true" QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store41" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model4" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="DesignationId" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator13" runat="server"
                                                ValidationGroup="InsertUpdate" ControlToValidate="cmbPosition" ErrorMessage="Please select employee Designation." />
                                        </td>
                                        <td runat="server" id="tdStep">
                                            <ext:ComboBox ID="cmbStep" Width="180px" runat="server" DisplayField="Text" ValueField="Value"
                                                FieldLabel="Grade *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store4" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model3" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="Text" />
                                                                    <ext:ModelField Name="Value" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator Display="None" ID="valGradeStep" runat="server" ValidationGroup="InsertUpdate"
                                                ControlToValidate="cmbStep" ErrorMessage="Please select employee Grade." />
                                        </td>
                                        
                                       <td>
                                            <ext:ComboBox ID="cmbFunctionTitle" Width="180px" runat="server" ValueField="FunctionalTitleId"
                                                DisplayField="Name" FieldLabel="Functional Title" LabelAlign="top" LabelSeparator=""
                                                ForceSelection="true" QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store12" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model13" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="FunctionalTitleId" Type="String" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                          
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='width: 180px'>
                                            <ext:ComboBox ID="cmbBranch" Width="180px" runat="server" ValueField="BranchId" DisplayField="BranchName"
                                                FieldLabel="Branch *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                                <DirectEvents>
                                                    <Select OnEvent="cmbBranch_Change">
                                                        <EventMask ShowMask="true" />
                                                    </Select>
                                                </DirectEvents>
                                                <Store>
                                                    <ext:Store ID="Store5" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model5" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="BranchId" />
                                                                    <ext:ModelField Name="BranchName" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator15" runat="server"
                                                ValidationGroup="InsertUpdate" ControlToValidate="cmbBranch" ErrorMessage="Please select a Branch for the employee." />
                                        </td>
                                        <td style='width: 180px'>
                                            <ext:ComboBox ID="cmbDepartment" Width="180px" runat="server" ValueField="DepartmentId"
                                                DisplayField="Name" FieldLabel="Department *" LabelAlign="top" LabelSeparator=""
                                                ForceSelection="true" QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store6" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model6" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="DepartmentId" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator16" runat="server"
                                                ValidationGroup="InsertUpdate" ControlToValidate="cmbDepartment" ErrorMessage="Please select a Department for the employee." />
                                        </td>
                                        <td style='width: 180px'>
                                            <ext:ComboBox ID="cmbSubDepartment" Width="180px" runat="server" ValueField="SubDepartmentId"
                                                DisplayField="Name" FieldLabel="Sub Department" LabelAlign="top" LabelSeparator=""
                                                ForceSelection="true" QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store11" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model11" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="SubDepartmentId" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </td>
                                        <td style='width: 180px'>
                                            <ext:ComboBox ID="cmbELocation" Width="180px" runat="server" ValueField="LocationId"
                                                DisplayField="Name" FieldLabel="Location" LabelAlign="top" LabelSeparator=""
                                                ForceSelection="true" QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store9" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model9" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="LocationId" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </td>
                                        <td>
                                            <ext:ComboBox ID="cmbGrade" Width="180px" runat="server" ValueField="GradeId" DisplayField="Name"
                                                FieldLabel="Grade/Step" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store10" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model10" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="GradeId" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                </table>
                                <%--<hr style="margin-top: 10px; margin-top: 20px" />
                                <table runat="server" id="tableRetirement2" style="width: inherit; margin-top: 25px;
                                    display: none; margin-bottom: 15px;">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <ext:Checkbox ID="chkRetirement" runat="server" LabelSeparator="" BoxLabel="Retirement Date"
                                                    BoxLabelAlign="After" LabelWidth="150">
                                                    <Listeners>
                                                        <Change Handler="if(this.getValue()==true) {#{calRetirementDate}.enable();#{cmbTypes}.enable();} else {#{calRetirementDate}.disable();#{cmbTypes}.disable();}" />
                                                    </Listeners>
                                                </ext:Checkbox>
                                                <pr:CalendarExtControl Disabled="true" Width="180px" ID="calRetirementDate" runat="server"
                                                    LabelAlign="Top" LabelSeparator="" />
                                            </td>
                                            <td style="padding-left: 10px">
                                                <ext:ComboBox Disabled="true" FieldLabel="Retirement Type" ID="cmbTypes" Width="180px"
                                                    runat="server" ValueField="EventID" ForceSelection="true" DisplayField="Name"
                                                    LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                                    <Store>
                                                        <ext:Store ID="Store12" runat="server">
                                                            <Model>
                                                                <ext:Model ID="Model13" IDProperty="EventID" runat="server">
                                                                    <Fields>
                                                                        <ext:ModelField Name="EventID" />
                                                                        <ext:ModelField Name="Name" />
                                                                    </Fields>
                                                                </ext:Model>
                                                            </Model>
                                                        </ext:Store>
                                                    </Store>
                                                </ext:ComboBox>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>--%>
                            </div>
                        </div>
                        <div class="buttonBlock">
                            <ext:Button ID="btnNext" Cls="btn btn-primary" Width="100px" runat="server" Text="&nbsp;&nbsp;Save&nbsp;&nbsp;"
                                ValidationGroup="InsertUpdate">
                                <DirectEvents>
                                    <Click OnEvent="ButtonNext_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'InsertUpdate';  if(CheckValidation()) return true; else return false;">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
