﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="ExitFlow.aspx.cs" Inherits="Web.NewHR.ExitFlow" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

     <div class="contentpanel" style='padding-top: 0px;'>
        <div class="innerLR">
            <h3>Exit Flow</h3>
           
            <ext:GridPanel ID="gridReqList" runat="server" Header="true" AutoScroll="true" Width="650">
                <Store>
                    <ext:Store ID="Store3" runat="server" OnReadData="Store_ReadData" RemoteSort="true"
                        PageSize="20" GroupField="EmployeeName">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Sorters>
                            <ext:DataSorter Direction="ASC" Property="EmployeeName" />
                        </Sorters>

                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="RequestID">
                                <Fields>
                                    <ext:ModelField Name="ID" />
                                    <ext:ModelField Name="EmployeeName" />
                                    <ext:ModelField Name="ExpectedDate" />
                                    <ext:ModelField Name="DaysCount" />
                                    <ext:ModelField Name="Action" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>


                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="Step" Width="40" Align="Center"/>
                        <ext:Column ID="colEId" Locked="true" runat="server" Text="Description" Width="130" DataIndex="EmployeeName"
                             Sortable="true" MenuDisabled="false">
                        </ext:Column>
                        <ext:DateColumn ID="Column10" Format="yyyy-MMM-dd" runat="server" Text="Authority"
                            Width="100" DataIndex="ExpectedDate" Sortable="true" MenuDisabled="false">
                        </ext:DateColumn>
                        <ext:Column ID="Column" runat="server" Text="Person 1" Width="100"
                            DataIndex="DaysCount" Sortable="true" MenuDisabled="false">
                        </ext:Column>
                       <ext:Column ID="Column1" runat="server" Text="Person 2" Width="100"
                            DataIndex="DaysCount" Sortable="true" MenuDisabled="false">
                        </ext:Column>                        
                    </Columns>
                </ColumnModel>
               <%-- <Listeners>
                    <SelectionChange Handler="document.getElementById('spanText').innerHTML = this.getSelectionModel().selected.items.length + ' requests selected';" />
                </Listeners>--%>
               
                <View>
                    <ext:GridView ID="GridView1" EnableTextSelection="true" runat="server">
                        <Listeners>
                        </Listeners>
                    </ext:GridView>
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="#{Store3}.pageSize = this.getValue();searchList()" />
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <br />
        </div>
    </div>

</asp:Content>

