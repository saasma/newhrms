﻿<%@ Page Title="Acting" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true"
    CodeBehind="Acting.aspx.cs" Inherits="Web.NewHR.UserControls.Acting" %>

<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentHistoryCtl.ascx" TagName="EmploymentHistoryCtl"
    TagPrefix="ucEmploymentHistoryCtl" %>
<%@ Register Src="UserControls/EmployeeBlockDetails.ascx" TagName="EmployeeBlockDetails"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
 <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hdnID" Text="">
    </ext:Hidden>

<div class="pageheader">
    <div class="media">
        <div class="media-body">
            <h4>
                Acting
            </h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div class="innerLR">
        <div class="separator bottom">
        </div>
        
        <ext:Store ID="storeEmployee" runat="server">
            <Model>
                <ext:Model IDProperty="EmployeeId" runat="server">
                    <Fields>
                        <ext:ModelField Name="NameEIN" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:ComboBox QueryMode="Local" LabelWidth="120" FieldLabel="Select Employee" ID="ddlEmployee"
            Width="400px" ForceSelection="true" runat="server" StoreID="storeEmployee" Editable="true"
            DisplayField="NameEIN" ValueField="EmployeeId" Mode="Local" EmptyText="">
            <DirectEvents>
                <Select OnEvent="ddlEmployee_Select">
                    <EventMask ShowMask="true" />
                </Select>
            </DirectEvents>
            <Listeners>
                <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                      queryEvent.query = new RegExp('\\b' + q, 'i');
                                      queryEvent.query.length = q.length;" />
            </Listeners>
        </ext:ComboBox>
        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
            ValidationGroup="SaveUpdate" ControlToValidate="ddlEmployee" ErrorMessage="Employee is required." />
        <div class="widget" style="margin-top: 15px">
            <div class="widget-body">
                <uc2:EmployeeBlockDetails Id="employeeBlock" runat="server" />
                <table style="margin-top: 10px;" class="fieldTable firsttdskip">
                    <tr>
                        <td>
                            <ext:TextField ID="txtLetterNo" FieldLabel="Letter No" runat="server" LabelAlign="Top"
                                Width="180" LabelSeparator="">
                            </ext:TextField>
                            <%-- <asp:RequiredFieldValidator ErrorMessage="DOB is required." Display="None" ID="RequiredFieldValidator9"
                                ControlToValidate="txtDOBEng" ValidationGroup="InsertUpdate" runat="server">
                            </asp:RequiredFieldValidator>--%>
                        </td>
                        <td>
                            <pr:CalendarExtControl Width="180px" FieldLabel="Letter Date *" ID="calLetterDate"
                                runat="server" LabelAlign="Top" LabelSeparator="" />
                            <asp:RequiredFieldValidator ID="val1" runat="server" ErrorMessage="Date is required."
                                ControlToValidate="calLetterDate" ValidationGroup="SaveUpdate" Display="None" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <pr:CalendarExtControl Width="180px" FieldLabel="Applicable From *" ID="calApplicableFrom"
                                runat="server" LabelAlign="Top" LabelSeparator="" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Date is required."
                                ControlToValidate="calApplicableFrom" ValidationGroup="SaveUpdate" Display="None" />
                        </td>
                        <td>
                            <pr:CalendarExtControl Width="180px" FieldLabel="Applicable Till" ID="calApplicableTill"
                                runat="server" LabelAlign="Top" LabelSeparator="" />
                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Date is required."
                                ControlToValidate="calApplicableTill" ValidationGroup="SaveUpdate" Display="None" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:ComboBox Disabled="true" ID="cmbCurrentLevel" Width="180px" runat="server" ValueField="LevelId"
                                DisplayField="Name" FieldLabel="Level *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store4" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LevelId" Type="String" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="cmbCurrentLevel" ErrorMessage="Current level is required." />
                        </td>
                        <td>
                            <ext:ComboBox Disabled="true" ID="cmbCurrentPost" Width="180px" runat="server" ValueField="DesignationId"
                                DisplayField="Name" FieldLabel="Current Post *" LabelAlign="top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="DesignationId" Type="String" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="cmbCurrentPost" ErrorMessage="Current post is required." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:ComboBox ID="cmbToLevel" Width="180px" runat="server" ValueField="LevelId" DisplayField="Name"
                                FieldLabel="Acting level *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LevelId" Type="String" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="cmbToLevel" ErrorMessage="Acting level is required." />
                        </td>
                        <td style="display:none">
                           <%-- <ext:TextField FieldLabel="Amount *" Width="180px" ID="txtAmount" runat="server"
                                LabelSeparator="" LabelAlign="Top" />
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator6" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="txtAmount" ErrorMessage="Amount is required." />
                            <asp:CompareValidator Display="None" ID="CompareValidator1" runat="server" Type="Currency"
                                Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="SaveUpdate" ControlToValidate="txtAmount"
                                ErrorMessage="Invalid amount." />--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <ext:TextArea ID="txtNotes" FieldLabel="Notes " runat="server" LabelAlign="Top" LabelSeparator=""
                                Width="375">
                            </ext:TextArea>
                        </td>
                    </tr>
                </table>
                <div class="buttonBlock buttonLeft">
                    <ext:Button runat="server" Cls="btn btn-primary" ID="btnSaveUpdate"
                         Text="<i></i>Save" >
                        <DirectEvents>
                            <Click OnEvent="btnSaveUpdate_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                            </Click>
                        </Listeners>
                    </ext:Button>
                    <div class="btnFlatOr">
                        or</div>
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                        Text="<i></i>Cancel">
                        <Listeners>
                            <Click Handler="window.location = 'ActingList.aspx';">
                            </Click>
                        </Listeners>
                    </ext:LinkButton>
                </div>
                <div style="clear: both">
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
