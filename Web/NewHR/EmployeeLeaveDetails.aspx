﻿<%@ Page Title="Employee Leaves" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmployeeLeaveDetails.aspx.cs" Inherits="Web.NewHR.EmployeeLeaveDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .disabledIncomeDeduction, .disabledIncomeDeduction a, .disabledIncomeDeduction td
    {
        background-color: #ddd !important;
    }
        
    .unappliedIncomeDeduction, .unappliedIncomeDeduction a, .unappliedIncomeDeduction td
    {
        color: #E6E6E6;
    }

</style>

<script type="text/javascript">
    var CommandHandler = function(value, command,record,p2) {
        var employeeId = record.data.EmployeeId;
        var leaveTypeId = record.data.LeaveTypeId;
        var freqOfAccrual = record.data.FreqOfAccrual;

        if(freqOfAccrual == 'Manually')
        {
            var ret = popupUpdateMannualLeave('Id=' + leaveTypeId + "&EId=" + employeeId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshGrid();
                }
            }
        

        }
        else
        {
             var ret = popupUpdateLeave('Id=' + leaveTypeId + "&EId=" + employeeId);
                if (typeof (ret) != 'undefined') {
                    if (ret == 'Reload') {
                        refreshLeaveList(0);
                    }
                }
                
        }

        return false;
        
    };

    function refreshGrid() {
        <%= btnLoad.ClientID %>.fireEvent('click');
    }

     var getRowClass = function (record) {
    
            var clsName = record.data.RowBackColor;
            if(clsName != '')
                return clsName;

            return "";

        };


    var prepareCommand = function (grid, command, record, row) {
            
            if (record.data.LEEmployeeId == 0 )
            {
                command.hidden = true;
                command.hideMode = 'visibility';   
            }


           
        };
</script>

<script type="text/javascript">  

    var checkboxRenderer = function (v, p, record) {
        if (record.data.IsDeletable == true || record.data.EmployeeDeductionId == 0) {
            return '<div class="x-grid-row-checker">&nbsp;</div>';
        }

        return "";



    };

</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>

    
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Leaves
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbBranch" Width="180" runat="server" ValueField="BranchId" DisplayField="BranchName"
                            FieldLabel="Branch" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" />
                                                <ext:ModelField Name="BranchName" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                </td>
                <td>
                     <ext:ComboBox ID="cmbDepartment" Width="180" runat="server" ValueField="DepartmentId" MarginSpec="0 0 0 10"
                            DisplayField="Name" FieldLabel="Department" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                </td>
                <td>
                     <ext:ComboBox FieldLabel="Designation" ID="cmbDesignation"  MarginSpec="0 0 0 10"
                        Width="180"  runat="server" ValueField="DesignationId" DisplayField="Name"
                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store4" runat="server">
                                <Model>
                                    <ext:Model ID="Model3" IDProperty="DesignationId" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="DesignationId" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:ComboBox ID="cmbLevel" runat="server" FieldLabel="Level"  MarginSpec="0 0 0 10"
                        Width="180"  DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                        LabelSeparator="" LabelAlign="Top">
                        <Store>
                            <ext:Store ID="storeLevel" runat="server">
                                <Model>
                                    <ext:Model ID="modelLevel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="LevelId" Type="Int" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:MultiCombo Width="200" LabelSeparator="" SelectionMode="All" LabelWidth="50" LabelAlign="Top"  MarginSpec="0 0 0 10"
                        ID="cmbJobStatus" DisplayField="Value" ValueField="Key"
                        runat="server" FieldLabel="Job Status">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model8" IDProperty="Key" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Key" />
                                            <ext:ModelField Name="Value" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                        </Listeners>
                    </ext:MultiCombo>


                    

                </td>
            </tr>
            <tr>
                
                <td colspan="2">
                    <ext:Store ID="storeEmployee" runat="server">
                        <Model>
                            <ext:Model ID="Model7" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="NameEIN" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:TagField ID="tfEmployee" MarginSpec="5 0 0 0" QueryMode="Local" MinChars="2" StoreID="storeEmployee" DisplayField="NameEIN" ValueField="EmployeeId" runat="server" Width="370" TypeAhead="true" FieldLabel="Employee" LabelSeparator="" LabelAlign="Top">
                        <Listeners>
                            <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                queryEvent.query = new RegExp(q, 'ig');
                                queryEvent.query.length = q.length;" />
                        </Listeners>
                    </ext:TagField>

                </td>
                <td >
                    <ext:ComboBox ID="cmbLeaveType" Width="180" runat="server" ValueField="LeaveTypeId"  MarginSpec="5 0 0 10"
                        DisplayField="Title" FieldLabel="Leave Type" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="storeLeaveType" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Title" />
                                            <ext:ModelField Name="LeaveTypeId" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator runat="server" ID="rfvLeaveType" ValidationGroup="LoadData"
                         Display="None" ErrorMessage="Please select Leave Type." ControlToValidate="cmbLeaveType"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <ext:ComboBox ID="cmbStatus" Width="180" runat="server" ValueField="Value"  MarginSpec="5 0 0 10"
                        DisplayField="Text" FieldLabel="Status" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <Items>
                            <ext:ListItem Text="All" Value="-1" />
                            <ext:ListItem Text="Applied" Value="1" />
                            <ext:ListItem Text="Unapplied" Value="2" />
                        </Items>
                        <SelectedItems>
                            <ext:ListItem Text="All" Value="-1" />
                        </SelectedItems>
                    </ext:ComboBox>
                </td>
                <td style="padding-top: 16px;">
                    <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Load" Width="120" MarginSpec="5 0 0 10">
                        <DirectEvents>
                            <Click OnEvent="btnLoad_Click"  >
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'LoadData'; if(CheckValidation()) return ''; else return false;">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <br />        

        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridEmpLeaves" runat="server" Cls="itemgrid"
            Scroll="None" Width="1050">
            <Store>
                <ext:Store ID="storeEmpLeave" runat="server" >
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="EmployeeId">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="LeaveTypeId" Type="Int" />
                                <ext:ModelField Name="IDCardNo" Type="String" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="Branch" Type="String" />
                                <ext:ModelField Name="Department" Type="String" />
                                <ext:ModelField Name="Title" Type="String" />
                                <ext:ModelField Name="IsActive" Type="String" />
                                <ext:ModelField Name="CurrentStatus" Type="String" />
                                <ext:ModelField Name="Balance" Type="Int" />
                                <ext:ModelField Name="FreqOfAccrual" Type="String" />
                                <ext:ModelField Name="CurrentStatusValue" Type="Int" />
                                <ext:ModelField Name="LEEmployeeId" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>  
                    <ext:Column ID="colEmployeeId" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                        Width="60" Align="Left" DataIndex="EmployeeId">
                    </ext:Column>
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="INo"
                        Width="60" Align="Left" DataIndex="IDCardNo">
                    </ext:Column>
                    <ext:Column ID="colName" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                        Width="180" Align="Left" DataIndex="Name">
                    </ext:Column>
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                        Width="160" Align="Left" DataIndex="Branch">
                    </ext:Column>
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                        Width="160" Align="Left" DataIndex="Department">
                    </ext:Column>
                    <ext:Column ID="colIncomeType" Sortable="false" MenuDisabled="true" runat="server" Text="Leave Type"
                        Width="140" Align="Left" DataIndex="Title">
                    </ext:Column>
                    <ext:NumberColumn ID="colBalance" runat="server" Sortable="true" Align="Right"
                        MenuDisabled="true" Header="Total Days" Width="120" DataIndex="Balance">
                    </ext:NumberColumn>
                    <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                        Width="80" Align="Left" DataIndex="CurrentStatus">
                    </ext:Column>
                    <ext:ImageCommandColumn ID="CommandColumnChange" runat="server" Width="70" Text=""
                            MenuDisabled="true" Sortable="false" Align="Center">
                            <Commands>
                                <ext:ImageCommand CommandName="Change" Text="Change">
                                    <ToolTip Text="Change" />
                                </ext:ImageCommand>
                            </Commands>
                            <Listeners>
                                <Command Fn="CommandHandler">
                                </Command>
                            </Listeners>
                             <PrepareCommand Fn="prepareCommand" />
                        </ext:ImageCommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi">
                   <%-- <CustomConfig>
                        <ext:ConfigItem Name="renderer" Value="checkboxRenderer" Mode="Raw" />
                    </CustomConfig>--%>
                </ext:CheckboxSelectionModel>
            </SelectionModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <%--<GetRowClass Fn="getRowClass" />--%>
                </ext:GridView>
            </View>
        </ext:GridPanel>
        <br />

        <table>
            <tr>
                <td>
                    <ext:Button ID="btnApply" runat="server" Cls="btn btn-save" Text="Apply">
                        <DirectEvents>
                            <Click OnEvent="btnApply_Click" >
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to apply the leave to the employees?" />
                                <ExtraParams>
                                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridEmpLeaves}.getRowsValues({selectedOnly:true}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnUnapply" runat="server" Cls="btn btn-save" Text="Unapply" MarginSpec="0 0 0 20">
                        <DirectEvents>
                            <Click OnEvent="btnUnapply_Click" >
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to unapply the leave to the employees?" />
                                <ExtraParams>
                                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridEmpLeaves}.getRowsValues({selectedOnly:true}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
        </table>
    </div>
    
    

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
