﻿<%@ Page Title="Employee Information Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmpInfoList.aspx.cs" Inherits="Web.NewHR.EmpInfoList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
    
        function searchList() {
            <%=gridEmpInfo.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

        function refreshWindow()
        {
            searchList();
        }


    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Employee Information Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Store ID="storeBranch" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="BranchId">
                    <Fields>
                        <ext:ModelField Name="BranchId" Type="String" />
                        <ext:ModelField Name="Name" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <table>
            <tr>
                <td style="width: 160px;">
                    <ext:ComboBox ID="cmbBranch" runat="server" ValueField="BranchId" DisplayField="Name" EmptyText="Branch Filter"
                        FieldLabel="" Width="150" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local" StoreID="storeBranch">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 210px;">
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model3" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" DisplayField="Name"
                        ValueField="EmployeeId" EmptyText="Search Employee"
                        StoreID="storeSearch" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:Button ID="btnLoad" runat="server" Text="Load">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnExport" runat="server" Text="Export" AutoPostBack="true" OnClick="btnExport_Click" MarginSpec="0 0 0 10">
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnImportExcel" runat="server" OnClientClick="EEmployeeInfoImport();return false;" Text="<i></i>Import from Excel" MarginSpec="0 0 0 10">
                    </ext:Button>
                </td>
            </tr>
        </table>
        <br />

        <ext:GridPanel StyleSpec="margin-top:10px;" ID="gridEmpInfo" runat="server"
            Cls="itemgrid" OnReadData="Store_ReadData" Scroll="Horizontal">
            <Store>
                <ext:Store ID="storeEmpInfo" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="PrevEmploymentId">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="String" />
                                <ext:ModelField Name="IdCardNo" Type="String" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="BloodGroupName" Type="String" />
                                <ext:ModelField Name="Nationality" Type="String" />
                                <ext:ModelField Name="CitizenshipNo" Type="String" />
                                <ext:ModelField Name="IssueDate" Type="String" />
                                <ext:ModelField Name="Place" Type="String" />
                                <ext:ModelField Name="LiscenceTypeName" Type="String" />
                                <ext:ModelField Name="DrivingLicenceNo" Type="String" />
                                <ext:ModelField Name="DrivingLicenceIssueDate" Type="String" />
                                <ext:ModelField Name="IssuingCountry" Type="String" />
                                <ext:ModelField Name="PassportNo" Type="String" />
                                <ext:ModelField Name="IssuingDate" Type="String" />
                                <ext:ModelField Name="ValidUpto" Type="String" />
                                <ext:ModelField Name="Birthmark" Type="String" />
                                <ext:ModelField Name="MarriageAniversary" Type="String" />
                                <ext:ModelField Name="MotherTongue" Type="String" />
                                <ext:ModelField Name="ReligionName" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:Column ID="colEmployeeId" DataIndex="EmployeeId" runat="server" Text="EIN" MenuDisabled="true" Sortable="false" Align="Left" Width="60" Locked="true" />
                    <ext:Column ID="colINO" runat="server" DataIndex="IdCardNo" Text="INO" MenuDisabled="true" Sortable="false" Align="Left" Width="80" Locked="true" />
                    <ext:Column ID="colEmplooyeeName" runat="server" DataIndex="Name" Text="Name" MenuDisabled="true" Sortable="false" Align="Left" Width="150" Locked="true" />
                    <ext:Column ID="colBloodGroup" runat="server" DataIndex="BloodGroupName" Text="Blood Group" MenuDisabled="true" Sortable="false" Align="Left" Width="80" />
                    <ext:Column ID="colNationality" runat="server" DataIndex="Nationality" Text="Nationality" MenuDisabled="true" Sortable="false" Align="Left" Width="120" />
                    <ext:Column ID="colCitizenshipNo" runat="server" DataIndex="CitizenshipNo" Text="Cit. No." MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colCitIssueDates" runat="server" Text="Cit. Issue Date" MenuDisabled="true" Sortable="false" Align="Left" Width="100" DataIndex="IssueDate">
                    </ext:Column>
                    <ext:Column ID="colCitIssuePlace" runat="server" DataIndex="Place" Text=" Cit. Issue Place" MenuDisabled="true" Sortable="false" Align="Left" Width="110" />
                    <ext:Column ID="colLicType" runat="server" DataIndex="LiscenceTypeName" Text="License Type" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colLiscenseNo" runat="server" DataIndex="DrivingLicenceNo" Text="Liscense No" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colLisIssueDate" runat="server" Text="Lis. Issue Date" MenuDisabled="true" Sortable="false" Align="Left" Width="100" DataIndex="DrivingLicenceIssueDate">
                    </ext:Column>
                    <ext:Column ID="colLisIssueCountry" runat="server" DataIndex="IssuingCountry" Text="Lis. Issuing Country" MenuDisabled="true" Sortable="false" Align="Left" Width="130" />
                    <ext:Column ID="colPassportNo" runat="server" DataIndex="PassportNo" Text="Passport No." MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colPIssuingDate" runat="server" Text="Pass. Issuing Date" MenuDisabled="true" Sortable="false" Align="Left" Width="120" DataIndex="IssuingDate">
                    </ext:Column>
                    <ext:Column ID="colPValidUptoss" runat="server" Text="Pass. Valid Upto" MenuDisabled="true" Sortable="false" Align="Left" Width="120" DataIndex="ValidUpto">
                    </ext:Column>
                    <ext:Column ID="colBirthmark" runat="server" DataIndex="Birthmark" Text="Birthmark" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colMarriageAnneversary" runat="server" DataIndex="MarriageAniversary" Text="Marriage Anneversary" MenuDisabled="true" Sortable="false" Align="Left" Width="150" />
                    <ext:Column ID="colReligion" runat="server" DataIndex="ReligionName" Text="Religion" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colEthnicity" runat="server" DataIndex="" Text="Ethnicity" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colMotherTongue" runat="server" DataIndex="MotherTongue" Text="Mother Tongue" MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                </Columns>
            </ColumnModel>
            <Plugins>
                <ext:FilterHeader ID="FilterHeader2" runat="server" Remote="true" OnCreateFilterableField="OnCreateFilterableField" />
            </Plugins>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeEmpInfo"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                            </Listeners>
                            <Items>
                                 <ext:ListItem Value="25" Text="25" />
                                <ext:ListItem Value="100" Text="100" />
                                <ext:ListItem Value="500" Text="500" />
                                <ext:ListItem Value="100000" Text="All" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <br />
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
