﻿<%@ Page Title="Employee Activity" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeActivityList.aspx.cs" Inherits="Web.NewHR.EmployeeActivityList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
  

    <style type="text/css">
        .x-grid-cell-inner
        {
            margin-left: -5px;
        }
        
        .blueCls
        {
            color:Blue !important;
        }
        .grid1 .x-panel-header
        {
            background-color:Transparent !important;        
        }
        .grid1 .x-header-text
        {
            color:Blue !important;       
        }
        .commentCls td
        {
            background-color: #FFF3C3 !important;
        }
        .employeeCommentedCls td
        {
            background-color: #C6E0B4 !important;
        }
        
        .trHeaderCls td
        {
            background-color:#BDD7EE;
            color:#4077A7;
            padding:3px;
            margin-top:10px !important;
        }
        .trRowClass td
        {
            background-color:White;
            padding:3px;
        }
        .spacerRowCls
        {
            height:15px;
        }
        .ActivityCls
        {
            cursor : pointer;
            background-color: #3892D3 !important;
        }
        .ActivitySelectedCls
        {
            cursor : pointer;
            background-color: #0d47a1 !important;
        }
        .ActivityCls : hover
        {
            cursor : pointer;
            background-color: #3071A9 !important;
        }
        .ActivitySelectedCls : hover
        {
            cursor : pointer;
            background-color: #3071A9 !important;
        }
        
        .x-view-over {
            font-style : italic;
            font-weight:bold;
            font-size:15px;
        }
       
    </style>
    <script type="text/javascript">


     /* A header Checkbox of CheckboxSelectionModel deals with the current page only.
     This override demonstrates how to take into account all the pages.
     It works with local paging only. It is not going to work with remote paging.
     */
     Ext.selection.CheckboxModel.override({
         selectAll: function (suppressEvent) {
             var me = this,
                    selections = me.store.getAllRange(), // instead of the getRange call
                    i = 0,
                    len = selections.length,
                    start = me.getSelection().length;

             me.suspendChanges();

             for (; i < len; i++) {
                 me.doSelect(selections[i], true, suppressEvent);
             }

             me.resumeChanges();
             if (!suppressEvent) {
                 me.maybeFireSelectionChange(me.getSelection().length !== start);
             }
         },

         deselectAll: Ext.Function.createSequence(Ext.selection.CheckboxModel.prototype.deselectAll, function () {
             this.view.panel.getSelectionMemory().clearMemory();
         }),

         updateHeaderState: function () {
             var me = this,
                    store = me.store,
                    storeCount = store.getTotalCount(),
                    views = me.views,
                    hdSelectStatus = false,
                    selectedCount = 0,
                    selected, len, i;

             if (!store.buffered && storeCount > 0) {
                 selected = me.view.panel.getSelectionMemory().selectedIds;
                 hdSelectStatus = true;
                 for (s in selected) {
                     ++selectedCount;
                 }

                 hdSelectStatus = storeCount === selectedCount;
             }

             if (views && views.length) {
                 me.toggleUiHeader(hdSelectStatus);
             }
         }
     });

     Ext.grid.plugin.SelectionMemory.override({
         memoryRestoreState: function (records) {
             if (this.store !== null && !this.store.buffered && !this.grid.view.bufferedRenderer) {
                 var i = 0,
                        ind,
                        sel = [],
                        len,
                        all = true,
                        cm = this.headerCt;

                 if (!records) {
                     records = this.store.getAllRange(); // instead of getRange
                 }

                 if (!Ext.isArray(records)) {
                     records = [records];
                 }

                 if (this.selModel.isLocked()) {
                     this.wasLocked = true;
                     this.selModel.setLocked(false);
                 }

                 if (this.selModel instanceof Ext.selection.RowModel) {
                     for (ind = 0, len = records.length; ind < len; ind++) {
                         var rec = records[ind],
                                id = rec.getId();

                         if ((id || id === 0) && !Ext.isEmpty(this.selectedIds[id])) {
                             sel.push(rec);
                         } else {
                             all = false;
                         }

                         ++i;
                     }

                     if (sel.length > 0) {
                         this.surpressDeselection = true;
                         this.selModel.select(sel, false, !this.grid.selectionMemoryEvents);
                         this.surpressDeselection = false;
                     }
                 } else {
                     for (ind = 0, len = records.length; ind < len; ind++) {
                         var rec = records[ind],
                                id = rec.getId();

                         if ((id || id === 0) && !Ext.isEmpty(this.selectedIds[id])) {
                             if (this.selectedIds[id].dataIndex) {
                                 var colIndex = cm.getHeaderIndex(cm.down('gridcolumn[dataIndex=' + this.selectedIds[id].dataIndex + ']'))
                                 this.selModel.setCurrentPosition({
                                     row: i,
                                     column: colIndex
                                 });
                             }
                             return false;
                         }

                         ++i;
                     }
                 }

                 if (this.selModel instanceof Ext.selection.CheckboxModel) {
                     if (all) {
                         this.selModel.toggleUiHeader(true);
                     } else {
                         this.selModel.toggleUiHeader(false);
                     }
                 }

                 if (this.wasLocked) {
                     this.selModel.setLocked(true);
                 }
             }
         }
     });


     var prepareActivity = function (grid, toolbar, rowIndex, record) {
         var markButton = toolbar.items.get(0);
         if (record.data.Status > 2) {
             markButton.setVisible(false);            
         }
     }

    var CommandHandler = function(command, record){
        
            <%= hdnOldActivityId.ClientID %>.setValue(record.data.ActivityId);
            <%= hdnActivityId.ClientID %>.setValue(record.data.DetailId);
            <%= hdnActivityType.ClientID %>.setValue(record.data.ActivityType);
            <%= hdnClients.ClientID %>.setValue(record.data.Clients);
            <%= hdnActivityDuration.ClientID %>.setValue(record.data.ActivityDurationPeriod);
            
            if(command=="MarkRead")
            {
                <%= btnMarkRead.ClientID %>.fireEvent('click');
            }
            else if(command == "View")
            {
                <%= btnView.ClientID %>.fireEvent('click');
            }
        }

    
        function searchList() {
            <%=gridActivity.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

       var checkboxRenderer = function (v, p, record) {
            if(record.data.Status == 3){            
                return "";
            }
            
          return '<div class="x-grid-row-checker">&nbsp;</div>'
        };

     var clientRenderer = function (value) {
            if(value == null || value == '')
                return "";

            var r = <%=storeClient.ClientID %>.getById(value);

            if (Ext.isEmpty(r)) {
                return "";
            }
            return r.data.ClientName;
        };
        
        var typeRenderer = function (value) {
            if(value == null || value == '')
                return "";

            var r = <%=storeType.ClientID %>.getByInternalId(value);

            if (Ext.isEmpty(r)) {
                return "";
            }
            return r.data.Name;
        };

        var rendererClass = function(value, meta, record) {
            meta.tdCls = 'blueCls';
            return value;
        };
        
        var getRowClass = function (record) {
            if (record.data.NoOfEmpComments > 0) {
                return "employeeCommentedCls";
            }
            else if(record.data.NoOfComments > 0) {
                return "commentCls";
            }
            else{
                return "new-row";
            }
        };

        var enterKeyPressHandler = function (f, e) {
            if (e.getKey() == e.ENTER) {
                
                <%= btnSave.ClientID %>.fireEvent('click');
                e.stopEvent();
            }
        };

        var commentRenderer = function (value, metaData, record, rowIndex, colIndex, store) {

           if(value != null && value > 0)
                return 'Yes';
            else
             {
                if(record.data.NoOfComments > 0)
                    return 'No';
                else
                    return '';

              }
        };



        var itemClick = function (view, record, item, index, e) {
            <%= hdnOldActivityId.ClientID %>.setValue(record.data.ActivityId);
            <%= hdnActivityId.ClientID %>.setValue(record.data.DetailId);
            <%= hdnActivityType.ClientID %>.setValue(record.data.ActivityType);
            <%= hdnClients.ClientID %>.setValue(record.data.Clients);
            <%= hdnActivityDuration.ClientID %>.setValue(record.data.ActivityDurationPeriod);
            <%= btnView.ClientID %>.fireEvent('click');

        };

       
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <ext:Hidden runat="server" ID="hdnActivityId" />
     <ext:Hidden runat="server" ID="hdnOldActivityId" />
    <ext:Hidden runat="server" ID="hdnActivityType" />
    <ext:Hidden runat="server" ID="hdnClients" />
     <ext:Hidden runat="server" ID="hdnActivityDuration" />
     

    <ext:LinkButton ID="btnMarkRead" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnMarkRead_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the activity?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnView" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnView_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>



    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Activity
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Store ID="storeClient" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="ClientId">
                    <Fields>
                        <ext:ModelField Name="ClientId" Type="String" />
                        <ext:ModelField Name="ClientName" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="storeType" runat="server">
            <Model>
                <ext:Model ID="Model2" runat="server" IDProperty="ActivityTypeId">
                    <Fields>
                        <ext:ModelField Name="ActivityTypeId" Type="String" />
                        <ext:ModelField Name="Name" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <table>
            <tr>
                <td style="width: 140px;">
                    <ext:DateField ID="txtStartDate" runat="server" Width="120px" LabelAlign="Top" FieldLabel="From Date"
                        LabelSeparator="">
                        <Plugins>
                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td style="width: 140px;">
                    <ext:DateField ID="txtEndDate" runat="server" Width="120px" LabelAlign="Top" FieldLabel="To Date"
                        LabelSeparator="">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td style="width: 220px; display:none;">
                    <ext:ComboBox ID="cmbTypeFilter" runat="server" ValueField="ActivityTypeId" DisplayField="Name"
                        FieldLabel="Type" Width="200" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local" StoreID="storeType">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 220px; display:none;">
                    <ext:ComboBox ID="cmbClientFilter" runat="server" ValueField="ClientId" DisplayField="ClientName"
                        FieldLabel="Client" Width="200" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local" StoreID="storeClient">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 220px;">
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model3" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" DisplayField="Name"
                        FieldLabel="Employee" LabelAlign="Top" ValueField="EmployeeId" EmptyText="Search Employee"
                        StoreID="storeSearch" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="padding-top: 14px;">
                    <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Load" Width="120">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td style="width: 150px;">
                    <ext:Button ID="btnMarkAllAsRead" runat="server" Cls="btn btn-save" Text="<i></i>Approve"
                        Width="130">
                        <DirectEvents>
                            <Click OnEvent="btnMarkAllAsRead_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the activities?" />
                                <ExtraParams>
                                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridActivity}.getRowsValues({selectedOnly:true}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
                <td>
                    <ext:Button runat="server" Cls="btn btn-save" Width="150" ID="btnSaveAndSend" Text="<i></i>Save And Send">
                        <DirectEvents>
                            <Click OnEvent="btnSaveAndSend_Click">
                                <EventMask ShowMask="true" />
                                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save and send the activities?" />
                                <ExtraParams>
                                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridActivity}.getRowsValues({selectedOnly:true}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <br />
        <ext:TabPanel ID="TabPanel1" runat="server" Width="1160">
            <Items>
                <ext:Panel ID="pnlToday" runat="server" Title="Today">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlYesterday" runat="server" Title="Yesterday">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlThisWeek" runat="server" Title="This Week">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlLastWeek" runat="server" Title="Last Week">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlThisMonth" runat="server" Title="This Month">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlAll" runat="server" Title="All">
                    <Content>
                    </Content>
                </ext:Panel>
            </Items>
            <Listeners>
                <TabChange Fn="searchList" />
            </Listeners>
        </ext:TabPanel>
        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridActivity" runat="server" Cls="itemgrid"
            Scroll="None" Width="1160">
            <Store>
                <ext:Store ID="storeActivity" runat="server" AutoLoad="true" OnReadData="Store_ReadData" GroupField="EmployeeName"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="DetailId">
                            <Fields>
                                <ext:ModelField Name="DetailId" Type="Int" />
                                <ext:ModelField Name="ActivityId" Type="Int" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="Status" Type="Int" />
                                <ext:ModelField Name="NoOfActivities" Type="Int" />
                                <ext:ModelField Name="StatusName" Type="String" />
                                <ext:ModelField Name="DateString" Type="String" />
                                <ext:ModelField Name="NoOfComments" Type="Int" />
                                <ext:ModelField Name="NoOfEmpComments" Type="Int" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="Clients" Type="String" />
                                <ext:ModelField Name="Description" Type="String" />
                                <ext:ModelField Name="ActivityDurationPeriod" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    
                    <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Date"
                        Width="100" Align="Left" DataIndex="DateString">
                    </ext:Column>                    
                    <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text="Activity"
                        Width="140" Align="Left" DataIndex="ActivityType">
                    </ext:Column>
                    <ext:Column ID="ColClients" Sortable="false" MenuDisabled="true" runat="server" Text="Clients"
                        Width="180" Align="Left" DataIndex="Clients">
                    </ext:Column>
                    <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Details" Width="220" Align="Left" DataIndex="Description">
                    </ext:Column>
                    <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server"
                        Text="No of Comments" Width="100" Align="Center" DataIndex="NoOfComments">
                    </ext:Column>
                    <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Emp Comment" Width="100" Align="Center" DataIndex="NoOfEmpComments">
                        <Renderer Fn="commentRenderer" />
                    </ext:Column>
                    <ext:Column ID="colStatusName" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Status" Width="80" Align="Left" DataIndex="StatusName">
                    </ext:Column> 
                    <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Hours:Minutes" Width="90" Align="Left" DataIndex="ActivityDurationPeriod">
                    </ext:Column>                    
                    <ext:CommandColumn ID="CommandColumn3" runat="server" Width="70" Text="" Align="Center">
                        <Commands>
                            <ext:GridCommand Text="Approve" CommandName="MarkRead" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="50" Text="" Align="Center">
                        <Commands>
                            <ext:GridCommand Text="View" CommandName="View" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi">
                    <CustomConfig>
                        <ext:ConfigItem Name="renderer" Value="checkboxRenderer" Mode="Raw" />
                    </CustomConfig>
                </ext:CheckboxSelectionModel>
            </SelectionModel>
            <View>
                <ext:GridView ID="GridView1" runat="server">
                    <GetRowClass Fn="getRowClass" />
                </ext:GridView>
            </View>
             <Features>
                <ext:GroupingSummary ID="Group1" runat="server" GroupHeaderTplString="{name}" HideGroupedHeader="true"
                    EnableGroupingMenu="false" />
            </Features>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeEmpList"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <br />
    </div>
    <ext:Window ID="WActivity" runat="server" Title="User Daily Activity" Icon="Application" OverflowY="Scroll"
        Resizable="false" Width="1080" Height="450" BodyPadding="5" Hidden="true" Modal="true" ButtonAlign="Left">
        <Content>

          <table style="margin-left:20px; margin-top:20px;"> 
            <tr>
                <td style="vertical-align:top;">
                    <table>
                        <tr>
                            <td>
                                <ext:Label ID="lblActivityDate" runat="server" StyleSpec="color:#356FA2; font-weight:bold;" />
                            </td>
                            <td>
                                <ext:Label ID="lblActivityDateDetls" runat="server" StyleSpec="font-style: italic !important;" />
                              
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr class="spacerRowCls"></tr>
                                    <tr class="trHeaderCls">
                                        <td style="width:150px;">Type</td>
                                        <td style="width:350px;" id="tdClients" runat="server">Client</td>
                                        <td style="width:100px;">Hours:Minutes</td>
                                    </tr>
                                    <tr class="trRowClass">
                                        <td style="width:100px;">
                                            <ext:Label ID="lblActivityType" runat="server" />
                                        </td>
                                        <td style="width:300px;">
                                            <ext:Label ID="lblActivityClients" runat="server" />
                                        </td>
                                        <td style="width:100px;">
                                            <ext:Label ID="lblActivityDuration" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="spacerRowCls"></tr>
                        <tr class="trHeaderCls">
                            <td colspan="2" style="width:600px;">Details</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width:600px;">
                                <ext:TextArea ID="txtActivityDetails" FieldLabel="" runat="server" LabelAlign="Top"
                                    LabelSeparator="" Width="600" Rows="10">
                                </ext:TextArea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:Store ID="storeActivityDV" runat="server" >
                                        <Model>
                                            <ext:Model ID="Model7" runat="server" IDProperty="name">
                                                <Fields>
                                                    <ext:ModelField Name="DetailId" Type="Int" />
                                                    <ext:ModelField Name="ActivityId" Type="Int" />
                                                    <ext:ModelField Name="EmployeeName" Type="String" />
                                                    <ext:ModelField Name="DateEng" Type="Date" />
                                                    <ext:ModelField Name="Status" Type="Int" />
                                                    <ext:ModelField Name="NoOfActivities" Type="Int" />
                                                    <ext:ModelField Name="StatusName" Type="String" />
                                                    <ext:ModelField Name="DateString" Type="String" />
                                                    <ext:ModelField Name="NoOfComments" Type="Int" />
                                                    <ext:ModelField Name="NoOfEmpComments" Type="Int" />
                                                    <ext:ModelField Name="ActivityType" Type="String" />
                                                    <ext:ModelField Name="Clients" Type="String" />
                                                    <ext:ModelField Name="Description" Type="String" />
                                                    <ext:ModelField Name="ActivityDurationPeriod" Type="String" />
                                                    <ext:ModelField Name="SN" Type="Int" />
                                                    <ext:ModelField Name="ClsName" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                <ext:DataView
                                    ID="dvActivity"
                                    runat="server"
                                    StoreID="storeActivityDV"
                                    MultiSelect="false"
                                    OverItemCls="x-view-over"
                                    ItemSelector="div.thumb-wrap"
                                    EmptyText="" StyleSpec="margin-left:-7px;"
                                    TrackOver="true">
                                    
                                        <Tpl ID="Tpl1" runat="server">
                                            <Html>
                                                <tpl for=".">
                                                    <div class="thumb-wrap" id="{DetailId}">
                                                        <div class="{ClsName}" style="height:30px; width:50px; text-align:center; color:White; background-color:#3892D3; padding:5px; margin-left:7px; float:left;"><span class="x-editable" style="padding:10px;">{SN}</span></div>
                                                        
                                                    </div>
                                                </tpl>
                                                <div class="x-clear"></div>
                                            </html>                    </Tpl>
                                             <Listeners>
                                                <ItemClick Fn="itemClick" />
                                            </Listeners>
                    
                                </ext:DataView>
                        </tr>
                    </table>
                </td>

                <td style="vertical-align:top;">
                    <table style="margin-left:20px;">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td style="width:110px;">
                                            <ext:Image ID="img" runat="server" Width="80" Height="94" />
                                        </td>
                                        <td style="vertical-align:bottom;">
                                            <ext:Label ID="lblEmpName" runat="server" StyleSpec="color:#356FA2; font-size:large;" /><br />
                                            <ext:Label ID="lblDesignation" runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="spacerRowCls"></tr>
                                     
                                    <tr class="trHeaderCls">
                                        <td colspan="2">Comments</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:TextArea ID="txtComment" FieldLabel="" runat="server" LabelAlign="Top"
                                                LabelSeparator="" Width="400" Rows="4">
                                            </ext:TextArea>
                                            <asp:RequiredFieldValidator Display="None" ID="rfvComment" runat="server" ValidationGroup="SaveComment"
                                                ControlToValidate="txtComment" ErrorMessage="Comment is required." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ext:Button runat="server" ID="btnSaveAndSendAct" Cls="btn btn-primary" Text="<i></i>Send" MarginSpec="10 0 0 0">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSaveAndSendAct_Click">
                                                        <EventMask ShowMask="true" />                               
                                                    </Click>
                                                </DirectEvents>
                                                <Listeners>
                                                    <Click Handler="valGroup = 'SaveComment'; if(CheckValidation()) return ''; else return false;">
                                                    </Click>
                                                </Listeners>
                                            </ext:Button>
                                            <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary" Text="<i></i>Save Comment and Send" Hidden="true">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSave_Click">
                                                        <EventMask ShowMask="true" />                               
                                                    </Click>
                                                </DirectEvents>
                                                <Listeners>
                                                    <Click Handler="valGroup = 'SaveComment'; if(CheckValidation()) return ''; else return false;">
                                                    </Click>
                                                </Listeners>
                                            </ext:Button>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">
                                            <ext:DisplayField ID="dfComments" runat="server" MarginSpec="20 0 0 0" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
          </table>

           
            
            <br />
            <ext:Portal ID="portalPlannedActivities" runat="server" Border="false" Width="960"
                StyleSpec="margin-left:15px;">
                <Items>
                    <ext:PortalColumn ID="portalPlannedActivities1" runat="server" Cls="x-column-padding">
                        <Items>
                            <ext:Portlet StyleSpec="border:1px solid #157FCC;padding:5px;" ID="Portlet1" runat="server"
                                Title="Planned Activities" Icon="Accept" Collapsed="true" Closable="false">
                                <Items>
                                    <ext:GridPanel StyleSpec="margin-top:5px; margin-left:20px;" ID="gridPlan" runat="server"
                                        Cls="itemgrid" Scroll="Vertical" Width="900">
                                        <Store>
                                            <ext:Store ID="storePlan" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelPlan" Name="SettingModel" runat="server" IDProperty="SN">
                                                        <Fields>
                                                            <ext:ModelField Name="SN" Type="Int" />
                                                            <ext:ModelField Name="Description" Type="String"/>
                                                            <ext:ModelField Name="TotalTime" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel>
                                            <Columns>
                                                <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="SN"
                                                    Align="Center" Width="50" DataIndex="SN">
                                                </ext:Column>
                                                <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                                                    Align="Left" Width="750" DataIndex="Description">
                                                </ext:Column>
                                                <ext:Column ID="colTime" Sortable="false" MenuDisabled="true" runat="server" Text="Estimated Time"
                                                    Align="Left" Width="100" DataIndex="TotalTime">
                                                </ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <SelectionModel>
                                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                        </SelectionModel>
                                        <Plugins>
                                            <ext:CellEditing ID="CellEditing2" runat="server" ClicksToEdit="1">
                                            </ext:CellEditing>
                                        </Plugins>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Portlet>
                        </Items>
                    </ext:PortalColumn>
                </Items>
            </ext:Portal>
            <br />

            
        </Content>
        <Buttons>
            <ext:Button runat="server" Cls="btn btn-save" ID="Button1" Text="<i></i>Close" MarginSpec="0 0 0 20" Width="90">
                <Listeners>
                    <Click Handler="#{WActivity}.hide();" />
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">



</asp:Content>
