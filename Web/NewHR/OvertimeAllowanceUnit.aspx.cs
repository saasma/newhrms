﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class OvertimeAllowanceUnit : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
               Initialize();

                              

            }
        }

        private void Initialize()
        {

            SetGridHeader();

        }

        private void Clear()
        {
            
        }

        protected void SetGridHeader()
        {
            List<DAL.OvertimeType> _OvertimeTypes = BLL.Manager.OvertimeManager.GetOvertimeList();
            List<EveningCounterType> _AllowanceTypes = AllowanceManager.GetAllEveningCounterList();

            int i = 1;
            foreach (DAL.OvertimeType _item in _OvertimeTypes)
            {
                if (i == 1)
                {
                    ColumnOvertime1.Text = _item.Name;
                    ColumnOvertime1.Show();
                }
                if (i == 2)
                {
                    ColumnOvertime2.Text = _item.Name;
                    ColumnOvertime2.Show();
                }
                if (i == 3)
                {
                    ColumnOvertime3.Text = _item.Name;
                    ColumnOvertime3.Show();
                }
                if (i == 4)
                {
                    ColumnOvertime4.Text = _item.Name;
                    ColumnOvertime4.Show();
                }
                if (i == 5)
                {
                    ColumnOvertime5.Text = _item.Name;
                    ColumnOvertime5.Show();
                }
                i++;
            }

            int j = 1;
            foreach (EveningCounterType _item in _AllowanceTypes)
            {
                if (j == 1)
                {
                    ColumnAllowance1.Text = _item.Name;
                    ColumnAllowance1.Show();
                }
                if (j == 2)
                {
                    ColumnAllowance2.Text = _item.Name;
                    ColumnAllowance2.Show();
                }
                if (j == 3)
                {
                    ColumnAllowance3.Text = _item.Name;
                    ColumnAllowance3.Show();
                }
                if (j == 4)
                {
                    ColumnAllowance4.Text = _item.Name;
                    ColumnAllowance4.Show();
                }
                if (j == 5)
                {
                    ColumnAllowance5.Text = _item.Name;
                    ColumnAllowance5.Show();
                }
                j++;
            } 
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int? totalRecords = 0;
            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();
            CustomDate date1 = CustomDate.GetCustomDateFromString(txtFromDate.Text.Trim(), IsEnglish);
            PayrollPeriod startPeriod = CommonManager.GetPayrollPeriod(date1.Month, date1.Year);

            int PayrollPeriodId = 0;

            if (startPeriod == null)
            {
                NewMessage.ShowNormalMessage("Date " + txtFromDate.Text +" "+ "is not in payroll period");
                return;
            }
            else
                PayrollPeriodId = startPeriod.PayrollPeriodId;
            

            List<DAL.OvertimeType> _OvertimeTypes = BLL.Manager.OvertimeManager.GetOvertimeList();
            List<EveningCounterType> _AllowanceTypes = AllowanceManager.GetAllEveningCounterList();

            List<Report_GetOverTimeAllowanceSumResult> listSumOvertimeAllowance = EmployeeManager.GetOverTimeAllowanceSum(PayrollPeriodId);
            List<Report_GetOverTimeAllowanceEmployeesResult> list = EmployeeManager.GetOverTimeEmployees
              (PayrollPeriodId, e.Page - 1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords);

            e.Total = totalRecords.Value;

            if (list.Any())
            {
                AttendanceManager atteManager = new AttendanceManager();
                foreach (Report_GetOverTimeAllowanceEmployeesResult OvertimeEmployee in list)
                {
                    int overtimeCnt = 1;
                    foreach (DAL.OvertimeType _Overtime in _OvertimeTypes)
                    {

                        if (overtimeCnt == 1)

                            OvertimeEmployee.Overtime1 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "overtime", _Overtime.OvertimeTypeId);
                        if (overtimeCnt == 2)
                            OvertimeEmployee.Overtime2 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "overtime", _Overtime.OvertimeTypeId);
                        if (overtimeCnt == 3)
                            OvertimeEmployee.Overtime3 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "overtime", _Overtime.OvertimeTypeId);
                        if (overtimeCnt == 4)
                            OvertimeEmployee.Overtime4 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "overtime", _Overtime.OvertimeTypeId);
                        if (overtimeCnt == 5)
                            OvertimeEmployee.Overtime5 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "overtime", _Overtime.OvertimeTypeId);

                        overtimeCnt++;
                    }

                    int AllowanceCnt = 1;
                    foreach (DAL.EveningCounterType _Allowance in _AllowanceTypes)
                    {

                        if (AllowanceCnt == 1)
                            OvertimeEmployee.Allowance1 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "allowance", _Allowance.EveningCounterTypeId);
                        if (AllowanceCnt == 2)
                            OvertimeEmployee.Allowance2 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "allowance", _Allowance.EveningCounterTypeId);
                        if (AllowanceCnt == 3)
                            OvertimeEmployee.Allowance3 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "allowance", _Allowance.EveningCounterTypeId);
                        if (AllowanceCnt == 4)
                            OvertimeEmployee.Allowance4 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "allowance", _Allowance.EveningCounterTypeId);
                        if (AllowanceCnt == 5)
                            OvertimeEmployee.Allowance5 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "allowance", _Allowance.EveningCounterTypeId);
                        AllowanceCnt++;
                    }

                }
                storeEmpList.DataSource = list;
                storeEmpList.DataBind();

            }
        }



        protected void btnSearch_Click(object sender, DirectEventArgs e)
        {

            if (string.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowNormalMessage("Month is required.");
                return;
            }

           
            if (!string.IsNullOrEmpty(cmbShowOnly.SelectedItem.Value))
            {

                string ShowOnly = cmbShowOnly.SelectedItem.Text.ToLower();
                if (ShowOnly == "allowance")
                {
                    ColumnGroupOvertime.Hide();
                    ColumnGroupAllowances.Show();
                    if (string.IsNullOrEmpty(ColumnAllowance1.Text))
                        ColumnAllowance1.Hide();
                    if (string.IsNullOrEmpty(ColumnAllowance2.Text))
                        ColumnAllowance2.Hide();
                    if (string.IsNullOrEmpty(ColumnAllowance3.Text))
                        ColumnAllowance3.Hide();
                    if (string.IsNullOrEmpty(ColumnAllowance4.Text))
                        ColumnAllowance4.Hide();
                    if (string.IsNullOrEmpty(ColumnAllowance5.Text))
                        ColumnAllowance5.Hide();
                    
                }

                else if (ShowOnly == "overtime")
                {
                    ColumnGroupOvertime.Show();
                    ColumnGroupAllowances.Hide();
                    if (string.IsNullOrEmpty(ColumnOvertime1.Text))
                        ColumnOvertime1.Hide();
                    if (string.IsNullOrEmpty(ColumnOvertime2.Text))
                        ColumnOvertime2.Hide();
                    if (string.IsNullOrEmpty(ColumnOvertime3.Text))
                        ColumnOvertime3.Hide();
                    if (string.IsNullOrEmpty(ColumnOvertime4.Text))
                        ColumnOvertime4.Hide();
                    if (string.IsNullOrEmpty(ColumnOvertime5.Text))
                        ColumnOvertime5.Hide();
                   
                }
                else
                {
                    ColumnGroupOvertime.Show();
                    ColumnGroupAllowances.Show();
                    if (string.IsNullOrEmpty(ColumnAllowance1.Text))
                        ColumnAllowance1.Hide();
                    if (string.IsNullOrEmpty(ColumnAllowance2.Text))
                        ColumnAllowance2.Hide();
                    if (string.IsNullOrEmpty(ColumnAllowance3.Text))
                        ColumnAllowance3.Hide();
                    if (string.IsNullOrEmpty(ColumnAllowance4.Text))
                        ColumnAllowance4.Hide();
                    if (string.IsNullOrEmpty(ColumnAllowance5.Text))
                        ColumnAllowance5.Hide();
                    if (string.IsNullOrEmpty(ColumnOvertime1.Text))
                        ColumnOvertime1.Hide();
                    if (string.IsNullOrEmpty(ColumnOvertime2.Text))
                        ColumnOvertime2.Hide();
                    if (string.IsNullOrEmpty(ColumnOvertime3.Text))
                        ColumnOvertime3.Hide();
                    if (string.IsNullOrEmpty(ColumnOvertime4.Text))
                        ColumnOvertime4.Hide();
                    if (string.IsNullOrEmpty(ColumnOvertime5.Text))
                        ColumnOvertime5.Hide();
                    
                }

            }
            gridOverTimeAllowance.GetStore().Reload();
        }

        protected string GetOverTimeAllowanceSum(List<Report_GetOverTimeAllowanceSumResult> listSumOvertimeAllowance, int EmployeeID, string Type, int TypeID)
        {
            Report_GetOverTimeAllowanceSumResult _Result = listSumOvertimeAllowance.SingleOrDefault(x => x.Type.ToLower() == Type && x.TypeID == TypeID && x.EmployeeID == EmployeeID);

            if (_Result == null || _Result.Duration == null)
                return "";
            
            
            decimal value = decimal.Parse(_Result.Duration.Value.ToString());
            //string RetrnValue = "-";

            //if (_Result != null)
            //{
            //    value = 

            //    return value.ToString();
            //}



            if (Type.ToLower() == "overtime")
            {
                if (value > 0)
                {
                    return TimeSpan.FromMinutes((double)value).TotalHours.ToString("n1");
                }
            }
            else
            {
                if (value > 0)
                    return value.ToString();
            }

            return "";
        }


        public void btnExport_Click(object sender, EventArgs e)
        {
            int? totalRecords = 0;
            CustomDate date1 = CustomDate.GetCustomDateFromString(txtFromDate.Text.Trim(), IsEnglish);
            PayrollPeriod startPeriod = CommonManager.GetPayrollPeriod(date1.Month, date1.Year);
            int PayrollPeriodId = 0;
            if (startPeriod == null)
            {
                NewMessage.ShowNormalMessage("Date " + txtFromDate.Text + " " + "is not in payroll period");
                return;
            }
            else
                PayrollPeriodId = startPeriod.PayrollPeriodId;

            List<DAL.OvertimeType> _OvertimeTypes = BLL.Manager.OvertimeManager.GetOvertimeList();
            List<EveningCounterType> _AllowanceTypes = AllowanceManager.GetAllEveningCounterList();

            List<Report_GetOverTimeAllowanceSumResult> listSumOvertimeAllowance = EmployeeManager.GetOverTimeAllowanceSum(PayrollPeriodId);
            List<Report_GetOverTimeAllowanceEmployeesResult> list = EmployeeManager.GetOverTimeEmployees(PayrollPeriodId, 0, 99999, "", ref totalRecords);

            if (list.Any())
            {
                AttendanceManager atteManager = new AttendanceManager();
                foreach (Report_GetOverTimeAllowanceEmployeesResult OvertimeEmployee in list)
                {
                    int overtimeCnt = 1;
                    foreach (DAL.OvertimeType _Overtime in _OvertimeTypes)
                    {

                        if (overtimeCnt == 1)

                            OvertimeEmployee.Overtime1 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "overtime", _Overtime.OvertimeTypeId);
                        if (overtimeCnt == 2)
                            OvertimeEmployee.Overtime2 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "overtime", _Overtime.OvertimeTypeId);
                        if (overtimeCnt == 3)
                            OvertimeEmployee.Overtime3 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "overtime", _Overtime.OvertimeTypeId);
                        if (overtimeCnt == 4)
                            OvertimeEmployee.Overtime4 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "overtime", _Overtime.OvertimeTypeId);
                        if (overtimeCnt == 5)
                            OvertimeEmployee.Overtime5 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "overtime", _Overtime.OvertimeTypeId);

                        overtimeCnt++;
                    }

                    int AllowanceCnt = 1;
                    foreach (DAL.EveningCounterType _Allowance in _AllowanceTypes)
                    {

                        if (AllowanceCnt == 1)
                            OvertimeEmployee.Allowance1 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "allowance", _Allowance.EveningCounterTypeId);
                        if (AllowanceCnt == 2)
                            OvertimeEmployee.Allowance2 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "allowance", _Allowance.EveningCounterTypeId);
                        if (AllowanceCnt == 3)
                            OvertimeEmployee.Allowance3 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "allowance", _Allowance.EveningCounterTypeId);
                        if (AllowanceCnt == 4)
                            OvertimeEmployee.Allowance4 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "allowance", _Allowance.EveningCounterTypeId);
                        if (AllowanceCnt == 5)
                            OvertimeEmployee.Allowance5 = this.GetOverTimeAllowanceSum(listSumOvertimeAllowance, OvertimeEmployee.EmployeeId, "allowance", _Allowance.EveningCounterTypeId);
                        AllowanceCnt++;
                    }
                }

                List<string> hiddenList = new List<string> { "Title", "TotalRows" };
                string ShowOnly = "";
                
                if(cmbShowOnly.SelectedItem != null && cmbShowOnly.SelectedItem.Value != null)
                ShowOnly =  cmbShowOnly.SelectedItem.Text.ToLower();

                if (ShowOnly == "allowance")
                {
                    for (int i =1;  i <= 5; i++)
                    {
                        hiddenList.Add("Overtime" + i);
                    }
                }

               if (ShowOnly == "overtime")
                {
                   for (int i=1;i <= 5; i++)
                    {
                        hiddenList.Add("Allowance" + i);
                    }
                }

                //assuming 5 is the max type

                int OverTimeTypeCount = _OvertimeTypes.Count;
                int AllowanceTypeCount =_AllowanceTypes.Count;

                for (int i = (OverTimeTypeCount + 1); i <= 5; i++)
                {
                    hiddenList.Add("Overtime" + i);
                }

                for (int i = (AllowanceTypeCount+1); i <= 5; i++)
                {
                    hiddenList.Add("Allowance" + i);
                }

                Dictionary<string, string> renameList = new Dictionary<string, string>();
                renameList.Add("RowNumber", "SN");

                int k = 1;
                foreach (DAL.OvertimeType _item in _OvertimeTypes)
                {
                    if (k == 1)
                    {
                        renameList.Add("Overtime1", _item.Name);
                    }
                    if (k == 2)
                    {
                        renameList.Add("Overtime2", _item.Name);
                    }
                    if (k == 3)
                    {
                        renameList.Add("Overtime3", _item.Name);
                    }
                    if (k == 4)
                    {
                        renameList.Add("Overtime4", _item.Name);
                    }
                    if (k == 5)
                    {
                        renameList.Add("Overtime5", _item.Name);
                    }
                    k++;
                }

                int j = 1;
                foreach (EveningCounterType _item in _AllowanceTypes)
                {
                    if (j == 1)
                    {
                        renameList.Add("Allowance1", _item.Name);
                    }
                    if (j == 2)
                    {
                        renameList.Add("Allowance2", _item.Name);
                    }
                    if (j == 3)
                    {
                        renameList.Add("Allowance3", _item.Name);
                    }
                    if (j == 4)
                    {
                        renameList.Add("Allowance4", _item.Name);
                    }
                    if (j == 5)
                    {
                        renameList.Add("Allowance5", _item.Name);
                    }
                    j++;
                }

                Bll.ExcelHelper.ExportToExcel("Employee OvertimeAllowance Unit", list,
                    hiddenList,
                new List<String>() { },
                renameList,
                new List<string>()
                {

                }
                , new Dictionary<string, string>()
                {
                    {"Overtime Allowance Unit"," for " + startPeriod.Name}
                }
                , new List<string> { "RowNumber", "EmployeeId", "Name", "Branch", "Department", "Overtime1", "Overtime2","Overtime3","Overtime4","Overtime4" ,"Allowance1","Allowance2"
                ,"Allowance3","Allowance4","Allowance5"
                  });


            }
        }


    }
}