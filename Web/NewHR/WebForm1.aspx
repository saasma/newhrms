﻿<%@ Page Language="C#" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!X.IsAjaxRequest)
        {


        }
    }

    public class Employee
    {
        public int EIN { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
    }
    private object[] Data
    {
        get
        {
            return new object[]
            {
                new Employee{ EIN=1,Company="Swift Broters" , Name="Ram Kumar"},
                new Employee{ EIN=2,Company="ABC Company" , Name="Shyam Kumar"}               
                
            };
        }
    }

    protected void GridSkillSets_Command(object sender, DirectEventArgs e)
    {

        int ein = int.Parse(e.ExtraParams["ID"]);



    }
</script>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>Simple Array Grid - Ext.NET Examples</title>
    <style>
        #CalendarPanel2-tb-day{display:none;}
        #CalendarPanel2-tb-month{display:none;}
        .ext-cal-dayview .ext-cal-gutter-rt
        {
            width: 20px;
        }
    </style>
</head>
<body>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <ext:Store ID="storeEmployeeList" runat="server">
        <Model>
            <ext:Model ID="modelEmpShift" runat="server" IDProperty="EmployeeId">
                <Fields>
                    <ext:ModelField Name="Branch" Type="String" />
                    <ext:ModelField Name="Department" Type="String" />
                    <ext:ModelField Name="EmployeeId" Type="Int" />
                    <ext:ModelField Name="Name" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Viewport ID="Viewport2" runat="server" Layout="Border">
        <Items>
            <%--<ext:Panel ID="Panel1" runat="server" Height="35" Border="false" Region="North" Cls="app-header"
                BodyCls="app-header-content">
                <Content>
                    <div id="app-logo">
                        <div class="logo-top">
                            &nbsp;</div>
                        <div id="logo-body">
                            &nbsp;</div>
                        <div class="logo-bottom">
                            &nbsp;</div>
                    </div>
                    <h1>
                        My Calendar</h1>
                    <span id="app-msg" class="x-hidden"></span>
                </Content>
            </ext:Panel>--%>
            <ext:Panel ID="Panel2" runat="server" Layout="Border" Region="Center"
                Cls="app-center">
                <Items>
                    <ext:Panel ID="Panel3" runat="server" Width="176" Region="West" Border="false" Cls="app-west">
                        <Items>
                            <ext:GridPanel Region="West" ID="gridEmployeeList" runat="server" StoreID="storeEmployeeList"
                                MinHeight="0">
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="colBranch" Width="90" runat="server" Text="Branch" DataIndex="Branch">
                                        </ext:Column>
                                        <ext:Column ID="colEmployee" Width="200" runat="server" Text="Employee" DataIndex="Name">
                                        </ext:Column>
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </Items>
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <%--<Items>
                                    <ext:Button ID="Button1" runat="server" Text="Save All Events" Icon="Disk" Handler="CompanyX.record.saveAll();" />
                                </Items>--%>
                            </ext:Toolbar>
                        </TopBar>
                    </ext:Panel>
                    <ext:Panel runat="server" Region="Center" ID="pnl">
                    </ext:Panel>
                    <ext:CalendarPanel ID="CalendarPanel2" RenderTo="pnl" runat="server" Region="Center"
                        ActiveIndex="1" Border="false">
                        <CalendarStore ID="CalendarStore1" runat="server">
                            <Calendars>
                                <ext:CalendarModel CalendarId="1" Title="Home" />
                                <ext:CalendarModel CalendarId="2" Title="Work" />
                                <ext:CalendarModel CalendarId="3" Title="School" />
                            </Calendars>
                        </CalendarStore>
                        <EventStore ID="EventStore2" runat="server" NoMappings="true">
                            <Proxy>
                                <ext:AjaxProxy Url="shift/ShiftService.asmx/GetShiftList" Json="true">
                                    <ActionMethods Read="POST" />
                                    <Reader>
                                        <ext:JsonReader Root="d" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Mappings>
                                <ext:ModelField Name="StartDate" Type="Date" DateFormat="M$" />
                                <ext:ModelField Name="EndDate" Type="Date" DateFormat="M$" />
                            </Mappings>
                            <Parameters>
                                <ext:StoreParameter Name="StartDate" Mode="Value" Value="1/1/2000" />
                                <ext:StoreParameter Name="EndDate" Mode="Value" Value="1/1/2015" />
                                <ext:StoreParameter Name="Employee" Value="0" Mode="Raw" />
                            </Parameters>
                            <Listeners>
                                <BeforeLoad Handler="Ext.net.Mask.show();" />
                                <Load Handler="Ext.net.Mask.hide();" />
                            </Listeners>
                        </EventStore>
                        <DayView runat="server" Hidden="true" Disabled="true" />
                        <MonthView ID="MonthView1" Hidden="true" Disabled="true" runat="server" ShowHeader="true" ShowWeekLinks="true"
                            ShowWeekNumbers="true" />
                        <%--     <Listeners>
                            <ViewChange Fn="CompanyX.viewChange" Scope="CompanyX" />
                            <EventClick Fn="CompanyX.record.show" Scope="CompanyX" />
                            <DayClick Fn="CompanyX.dayClick" Scope="CompanyX" />
                            <RangeSelect Fn="CompanyX.rangeSelect" Scope="CompanyX" />
                            <EventMove Fn="CompanyX.record.move" Scope="CompanyX" />
                            <EventResize Fn="CompanyX.record.resize" Scope="CompanyX" />
                            <EventAdd Fn="CompanyX.record.addFromEventDetailsForm" Scope="CompanyX" />
                            <EventUpdate Fn="CompanyX.record.updateFromEventDetailsForm" Scope="CompanyX" />
                            <EventDelete Fn="CompanyX.record.removeFromEventDetailsForm" Scope="CompanyX" />
                        </Listeners>--%>
                    </ext:CalendarPanel>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
</body>
</html>
