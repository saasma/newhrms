﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.Base;

namespace Web.NewHR
{
    public partial class Identification : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                EEmployee emp = new EmployeeManager().GetById(GetEmployeeID());
                if (emp != null && emp.EHumanResources[0].IdCardNo != null && emp.EHumanResources.Count() > 0)
                    txtINo.Text = emp.EHumanResources[0].IdCardNo;

                txtContractINo.Text = emp.EHumanResources[0].ContractIdCardNo == null ? "" :
                    emp.EHumanResources[0].ContractIdCardNo;

               txtCardSerialNo.Text = emp.EHumanResources[0].CardSerialNo == null ? "" :
                    emp.EHumanResources[0].CardSerialNo;

            }
        }


        protected void btnUpload_Click(object sender, DirectEventArgs e)
        {

            Status status = EmployeeManager.UpdateINo(GetEmployeeID(), txtINo.Text.Trim(), txtContractINo.Text.Trim(), txtCardSerialNo.Text.Trim());
            if (status.IsSuccess)
                NewMessage.ShowNormalPopup("I No updated.");
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        protected int GetEmployeeID()
        {
            //Guid? AssetID = null;
            int EmpID = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                EmpID = int.Parse(Request.QueryString["ID"]);
            }
            return EmpID;
        }

    }
}