﻿<%@ Page Title="Attendance Requests Approval" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="AttRequestsApproval.aspx.cs" Inherits="Web.NewHR.AttRequestsApproval" %>

<%@ Register Src="~/Employee/UserControls/TimeAttendCtrl.ascx" TagName="TimeAttCtrl" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Approve Time Attendance
                </h4>
            </div>  
        </div>
    </div>

<div style="padding-top:30px; margin-left:20px;">
    <uc:TimeAttCtrl Id="ucTimeAttend" runat="server" />
</div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
