﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using Web.Master;
using BLL.Base;
using DAL;
using Bll;

namespace Web.NewHR
{
    public partial class ManageOvernightShift : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {


            HR master = this.Master as HR;
            if (master != null)
                master.DisableViewState = true;

            if (!IsPostBack && !X.IsAjaxRequest)
            {
                DispDate();
            }
            
        }

        public void DispDate()
        {
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();


            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.LevelAndDesignation).ToList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            this.storeLevel.DataBind();


        }


        public void btnUnMark_Click(object sender, DirectEventArgs e)
        {
            int ein = int.Parse(hdnEIN.Text.Trim());
            DateTime date = Convert.ToDateTime(hdnDate.Text.Trim());

            AttendanceMapping map = BLL.BaseBiz.PayrollDataContext.AttendanceMappings
                .FirstOrDefault(x => x.EmployeeId == ein);

            if (map == null)
            {
                NewMessage.ShowWarningMessage("Mapping not defined.");
                return;
            }
           
            List<AttendanceCheckInCheckOut> List =
                BLL.BaseBiz.PayrollDataContext.AttendanceCheckInCheckOuts
                .Where(x => x.DeviceID == map.DeviceId && x.Date == date.Date)
                .Where(x => x.DateTime.Date != x.Date).OrderBy(x => x.DateTime).ToList();

            foreach (AttendanceCheckInCheckOut item in List)
            {
                item.Date = item.DateTime.Date;
            }

            BLL.BaseBiz.PayrollDataContext.SubmitChanges();


            NewMessage.ShowNormalMessage("Attendance updated.", "searchList");
        }



        public void btnDetails_Click(object sender, DirectEventArgs e)
        {
            int ein = int.Parse(hdnEIN.Text.Trim());
            DateTime date = Convert.ToDateTime(hdnDate.Text.Trim());

            dispEmployee.Text = EmployeeManager.GetEmployeeName(ein) + " , " +  date.ToLongDateString();
            //dispDate.Text = date.ToLongDateString();

            List<AttendanceCheckInCheckOutView> List =
                BLL.BaseBiz.PayrollDataContext.AttendanceCheckInCheckOutViews
                .Where(x => x.EmployeeId == ein && x.Date == date.Date)
                .OrderBy(x => x.DateTime).ToList();

            foreach (AttendanceCheckInCheckOutView item in List)
            {
                item.Time = item.DateTime.ToString("yyyy-MMM-dd") +  "  " +  item.DateTime.ToShortTimeString();
            }

            gridMarkingList.Store[0].DataSource = List;
            gridMarkingList.Store[0].DataBind();

            btnSaveNightShift.Hide();
            windowMark.Title = "Attendance Details";
            windowMark.Center();
            windowMark.Show();
        }

        public void btnMark_Click(object sender, DirectEventArgs e)
        {
            int ein = int.Parse(hdnEIN.Text.Trim());
            DateTime date = Convert.ToDateTime(hdnDate.Text.Trim());

            dispEmployee.Text = EmployeeManager.GetEmployeeName(ein) + " , " +  date.ToLongDateString();
            //dispDate.Text = date.ToLongDateString();

            List<AttendanceCheckInCheckOutView> List =
                BLL.BaseBiz.PayrollDataContext.AttendanceCheckInCheckOutViews
                .Where(x => x.EmployeeId == ein && x.Date == date.Date)
                .Where(x => x.DateTime.Date == x.Date).OrderBy(x => x.DateTime).ToList();

            foreach (AttendanceCheckInCheckOutView item in List)
            {
                item.Time = item.DateTime.ToShortTimeString();
            }

            gridMarkingList.Store[0].DataSource = List;
            gridMarkingList.Store[0].DataBind();

            btnSaveNightShift.Show();
            windowMark.Title = "Marking List";
            windowMark.Center();
            windowMark.Show();
        }

        public void btnSaveNightShift_Save(object sender, DirectEventArgs e)
        {
            CheckboxSelectionModel selection = gridMarkingList.GetSelectionModel() as CheckboxSelectionModel;

            string guidList = "";

            foreach (var item in selection.SelectedRows)
            {
                if (guidList == "")
                    guidList = item.RecordID.ToString();
                else
                    guidList += "," + item.RecordID.ToString();
            }


            BLL.BaseBiz.PayrollDataContext.MarkOvernightShift(guidList,SessionManager.CurrentLoggedInUserID);
            windowMark.Hide();
            NewMessage.ShowNormalMessage("Attendance updated.", "searchList");

        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            DateTime? start = null, end = null;
            


            if (txtFromDate.SelectedDate != DateTime.MinValue)
                start = txtFromDate.SelectedDate;
            if (txtToDate.SelectedDate != DateTime.MinValue)
                end = txtToDate.SelectedDate;

            //hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            List<GetAttendanceListForOvernightMarkingNewResult> list = AttendanceManager.GetListForOvernightShiftMarkingNew
                (branchId, depId, employeeId, start, end, e.Page - 1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords
                , levelId, designationId);

            e.Total = totalRecords;

            //if (list.Any())
            storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

   
    }
}