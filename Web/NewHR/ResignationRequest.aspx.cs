﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class ResignationRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                //var a = CommonManager.RequestCheck();
                //if (a != null)
                //{
                //    Resigned.Hidden = false;
                //    Panel.Hidden = true;
                //    ExpectedDate1.Text = a.ExpectedDate.ToString();
                //    Reason1.Text = a.Reason;
                //    Status.Text = CommonManager.FilterResignationStatus(a.CurrentStatus);

                //    if (a.CurrentStatus != (int)BLL.ResignationRequestStatus.Pending)
                //    {
                //        CancelProcess.Visible = false;
                //    }
                //}
            }
        }

        protected void Initialize()
        {
            ExpectedDate.Clear();
            Reason.Clear();
            lblDaysCount.Text = "";
        }

        protected void Send_Click(object sender, DirectEventArgs e)
        {
            string expectedDate = ExpectedDate.Text;
            string reason = Reason.Text;
            bool status = CommonManager.SaveResignation(expectedDate, reason);
            if (status)
            {
                Resigned.Hidden = false;
                Panel.Hidden = true;
                ExpectedDate1.Text = expectedDate;
                Reason1.Text = reason;
                Status.Text = "Pending";
            }
        }

        protected void btnDaysCount_Click(object sender, DirectEventArgs e)
        {
            int daysCount = CommonManager.DaysCount(ExpectedDate.Text);
            lblDaysCount.Text = "In " +daysCount.ToString() +" days";
        }

        protected void Cancel_Click(object sender, DirectEventArgs e)
        {
            bool status = CommonManager.DeleteResignationReq();
        }
    }
}