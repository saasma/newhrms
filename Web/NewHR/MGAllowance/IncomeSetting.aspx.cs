﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.NewHR
{
    public partial class IncomeSetting : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {

            cmbIncome.Store[0].DataSource = PayManager.GetVariableIncomeList(SessionManager.CurrentCompanyId, false);
            cmbIncome.Store[0].DataBind();


            Grid.Store[0].DataSource = AllowanceManager.GetAllowanceIncomeList();
            Grid.Store[0].DataBind();
          


        }
        
       

        public void ClearLevelFields()
        {
            cmbIncome.ClearValue();
            txtAllocatedAmount.Text = "";
            hiddenValue.Text = "";
        }

       

     

        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            ClearLevelFields();
            window.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            //Status status = NewPayrollManager.DeleteLevel(levelId);
            //if (status.IsSuccess)
            //{
            //    LoadLevels();
            //    NewMessage.ShowNormalMessage("Level deleted.");
            //}
            //else
            //{
            //    NewMessage.ShowWarningMessage(status.ErrorMessage);
            //}

        }

    


      


        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            MG_AllowanceSetting entity = AllowanceManager.GetAllowance(levelId);
            window.Show();

            cmbIncome.Store[0].ClearFilter();
            cmbIncome.SetValue(entity.IncomeId.ToString());
            txtAllocatedAmount.Text = GetCurrency(entity.AllocatedAmount.ToString());

          
        }

       

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {

                MG_AllowanceSetting entity = new MG_AllowanceSetting();
                bool isInsert = true;
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    isInsert = false;
                    entity.IncomeId = int.Parse(hiddenValue.Text.Trim());
                }
                entity.IncomeId = int.Parse(cmbIncome.SelectedItem.Value);
                entity.AllocatedAmount = decimal.Parse(txtAllocatedAmount.Text.Trim());

                Status status = AllowanceManager.InsertUpdateAllowance(entity, isInsert);

                if (status.IsSuccess)
                {
                    window.Hide();
                    Initialise();
                    NewMessage.ShowNormalMessage("Allowance saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
                
            }
        }


   

    }
}