﻿<%@ Page Title="Income Setting" Language="C#" MasterPageFile="~/Master/HR.Master"
    AutoEventWireup="true" CodeBehind="IncomeSetting.aspx.cs" Inherits="Web.NewHR.IncomeSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
      var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.IncomeId);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
       



    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="hdnDropMode" />
    <ext:Hidden runat="server" ID="hdnSource" />
    <ext:Hidden runat="server" ID="hdnDest" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel" runat="server">
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteLevel_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the allowance?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <%--  <ul class="breadcrumb">
        <li><a href="index.html?lang=en" class="glyphicons home"><i></i>Smashing</a></li>
        <li class="divider"></li>
        <li>Bookings</li>
    </ul>--%>
    <div class="separator bottom">
    </div>
    <h3>
        Allowance Income List</h3>
    <div class="innerLR">
        <div class="widget">
            <div class="widget-body">
                <div>
                    <ext:GridPanel ID="Grid" runat="server" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="Store3" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server" IDProperty="IncomeId">
                                        <Fields>
                                            <ext:ModelField Name="IncomeId" Type="String" />
                                            <ext:ModelField Name="Name" Type="string" />
                                            <ext:ModelField Name="AllocatedAmount" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column8" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                    Text="Name" Align="Left" DataIndex="Name" />
                                <ext:Column ID="Column5" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                    Text="Allocated Amount" Align="Left" DataIndex="AllocatedAmount">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Actions">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                                        <%-- <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />--%>
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <div class="buttonBlock">
                        <ext:Button runat="server" ID="btnAddLevel" Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Add Income"
                            runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnAddLevel_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </div>
                </div>
            </div>
        </div>
        <ext:Window ID="window" runat="server" Title="Income Setting" Icon="Application"
            Width="420" Height="300" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:ComboBox ID="cmbIncome" Width="250px" runat="server" ValueField="IncomeId" DisplayField="Title"
                                FieldLabel="Income" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="IncomeId" Type="String" />
                                                    <ext:ModelField Name="Title" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="valcmbFaculty" runat="server" ValidationGroup="SaveUpdateLevel"
                                ControlToValidate="cmbIncome" ErrorMessage="Income is required." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextField ID="txtAllocatedAmount" LabelSeparator="" MinValue="0" runat="server"
                                FieldLabel="Allocated Amount" LabelAlign="Top">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtAllocatedAmount" ErrorMessage="Amount is required." />
                            <asp:CompareValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                Type="Currency" Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="SaveUpdateLevel"
                                ControlToValidate="txtAllocatedAmount" ErrorMessage="Invalid amount." />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button runat="server" ID="btnLevelSaveUpdate" Cls="btnFlat" BaseCls="btnFlat"
                                    Text="<i></i>Save" runat="server">
                                    <DirectEvents>
                                        <Click OnEvent="btnLevelSaveUpdate_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel"
                                    runat="server">
                                    <Listeners>
                                        <Click Handler="#{window}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
    <br />
</asp:Content>
