﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.NewHR
{
    public partial class AllowanceOpening : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        protected void cmbAllowanceIncome_Select(object sender, DirectEventArgs e)
        {
            int incomeId = int.Parse(cmbIncome.SelectedItem.Value);

            Grid.Store[0].DataSource = AllowanceManager.GetAllowanceOpeningIncomeList(incomeId);
            Grid.Store[0].DataBind();


            cmbEmployee.Store[0].DataSource = AllowanceManager.GetIncomeEmployee(incomeId);
            cmbEmployee.Store[0].DataBind();
        }

        public void Initialise()
        {

            cmbIncome.Store[0].DataSource = AllowanceManager.GetAllowanceIncomes();
            cmbIncome.Store[0].DataBind();




        }
        
       

        public void ClearLevelFields()
        {
            //cmbIncome.ClearValue();
            //txtAllocatedAmount.Text = "";
            txtTotalAllocatedAmount.Text = "";
            txtTotalAmountPaid.Text = "";
            txtTotalBillReceived.Text = "";
            cmbEmployee.ClearValue();
            hiddenValue.Text = "";
            cmbEmployee.Enable(true);
        }

       

     

        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            ClearLevelFields();
            window.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            int levelId = int.Parse(hiddenValue.Text.Trim());
            //Status status = NewPayrollManager.DeleteLevel(levelId);
            //if (status.IsSuccess)
            //{
            //    LoadLevels();
            //    NewMessage.ShowNormalMessage("Level deleted.");
            //}
            //else
            //{
            //    NewMessage.ShowWarningMessage(status.ErrorMessage);
            //}

        }

    


      


        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(hiddenValue.Text.Trim());
            MG_Opening entity = AllowanceManager.GetOpening(int.Parse(cmbIncome.SelectedItem.Value), employeeId);
            window.Show();

            cmbEmployee.Store[0].ClearFilter();
            cmbEmployee.SetValue(entity.EmployeeId.ToString());


            txtTotalAllocatedAmount.Text = GetCurrency(entity.TotalAllocatedAmount);

            txtTotalAmountPaid.Text = GetCurrency(entity.TotalAmountPaid);

            txtTotalBillReceived.Text = GetCurrency(entity.TotalBillReceived);

            cmbEmployee.Disable(true);
        }

       

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {

                MG_Opening entity = new MG_Opening();
                bool isInsert = true;
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    isInsert = false;
                    
                }
                entity.EmployeeId = int.Parse(cmbEmployee.SelectedItem.Value);
                entity.IncomeId = int.Parse(cmbIncome.SelectedItem.Value);
                entity.TotalAllocatedAmount = decimal.Parse(txtTotalAllocatedAmount.Text.Trim());
                entity.TotalAmountPaid = decimal.Parse(txtTotalAmountPaid.Text.Trim());
                entity.TotalBillReceived = decimal.Parse(txtTotalBillReceived.Text.Trim());

                Status status = AllowanceManager.InsertUpdateOpening(entity, isInsert);

                if (status.IsSuccess)
                {
                    window.Hide();
                    cmbAllowanceIncome_Select(null,null);
                    NewMessage.ShowNormalMessage("Opening saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
                
            }
        }


   

    }
}