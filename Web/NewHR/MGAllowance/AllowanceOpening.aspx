﻿<%@ Page Title="Allowance Opening" Language="C#" MasterPageFile="~/Master/HR.Master"
    AutoEventWireup="true" CodeBehind="AllowanceOpening.aspx.cs" Inherits="Web.NewHR.AllowanceOpening" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
      var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.EmployeeId);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }
       



    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="hdnDropMode" />
    <ext:Hidden runat="server" ID="hdnSource" />
    <ext:Hidden runat="server" ID="hdnDest" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel" runat="server">
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteLevel_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the opening?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <%--  <ul class="breadcrumb">
        <li><a href="index.html?lang=en" class="glyphicons home"><i></i>Smashing</a></li>
        <li class="divider"></li>
        <li>Bookings</li>
    </ul>--%>
    <div class="separator bottom">
    </div>
    <h3>
        Allowance Opening List</h3>
    <div class="innerLR">
        <ext:ComboBox ID="cmbIncome" Width="250px" runat="server" ValueField="IncomeId" DisplayField="Title"
            FieldLabel="Income" LabelAlign="top" LabelSeparator="" ForceSelection="true"
            QueryMode="Local">
            <DirectEvents>
                <Select OnEvent="cmbAllowanceIncome_Select">
                    <EventMask ShowMask="true" />
                </Select>
            </DirectEvents>
            <Store>
                <ext:Store ID="Store4" runat="server">
                    <Model>
                        <ext:Model ID="Model3" runat="server">
                            <Fields>
                                <ext:ModelField Name="IncomeId" Type="String" />
                                <ext:ModelField Name="Title" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
        </ext:ComboBox>
        <div class="widget" style="margin-top:15px">
            <div class="widget-body">
                <div>
                    <ext:GridPanel ID="Grid" runat="server" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="Store3" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="IncomeId" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                            <ext:ModelField Name="Name" Type="string" />
                                            <ext:ModelField Name="TotalAllocatedAmount" Type="string" />
                                            <ext:ModelField Name="TotalBillReceived" Type="string" />
                                            <ext:ModelField Name="TotalAmountPaid" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column8" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                    Text="Employee" Align="Left" DataIndex="Name" />
                                <ext:Column ID="Column5" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                    Text="Allocated Amount" Align="Left" DataIndex="TotalAllocatedAmount">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column1" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                    Text="Total Bill Received" Align="Left" DataIndex="TotalBillReceived">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:Column ID="Column2" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                                    Text="Total Amount Paid" Align="Left" DataIndex="TotalAmountPaid">
                                    <Renderer Fn="getFormattedAmount" />
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Text="Actions">
                                    <Commands>
                                        <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                                        <%-- <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />--%>
                                    </Commands>
                                    <Listeners>
                                        <Command Handler="CommandHandler(command,record);" />
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <div class="buttonBlock">
                        <ext:Button runat="server" ID="btnAddLevel" Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Add Employee"
                            runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnAddLevel_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </div>
                </div>
            </div>
        </div>
        <ext:Window ID="window" runat="server" Title="Opening" Icon="Application"
            Width="420" Height="400" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:ComboBox ID="cmbEmployee" Width="250px" runat="server" ValueField="Value"
                                DisplayField="Text" FieldLabel="Employee" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="Value" Type="String" />
                                                    <ext:ModelField Name="Text" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbEmployee" ErrorMessage="Employee is required." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextField ID="txtTotalAllocatedAmount" LabelSeparator="" MinValue="0" runat="server"
                                FieldLabel="Total Allocated Amount" LabelAlign="Top">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtTotalAllocatedAmount"
                                ErrorMessage="Total allocated amount is required." />
                            <asp:CompareValidator Display="None" ID="CompareValidator1" runat="server" Type="Currency"
                                Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="SaveUpdateLevel"
                                ControlToValidate="txtTotalAllocatedAmount" ErrorMessage="Invalid amount." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextField ID="txtTotalBillReceived" LabelSeparator="" MinValue="0" runat="server"
                                FieldLabel="Total Bill Received" LabelAlign="Top">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtTotalBillReceived" ErrorMessage="Amount is required." />
                            <asp:CompareValidator Display="None" ID="CompareValidator2" runat="server" Type="Currency"
                                Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="SaveUpdateLevel"
                                ControlToValidate="txtTotalBillReceived" ErrorMessage="Invalid amount." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextField ID="txtTotalAmountPaid" LabelSeparator="" MinValue="0" runat="server"
                                FieldLabel="Total Amount Paid" LabelAlign="Top">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtTotalAmountPaid" ErrorMessage="Amount is required." />
                            <asp:CompareValidator Display="None" ID="CompareValidator3" runat="server" Type="Currency"
                                Operator="GreaterThanEqual" ValueToCompare="0" ValidationGroup="SaveUpdateLevel"
                                ControlToValidate="txtTotalAmountPaid" ErrorMessage="Invalid amount." />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button runat="server" ID="btnLevelSaveUpdate" Cls="btnFlat" BaseCls="btnFlat"
                                    Text="<i></i>Save" runat="server">
                                    <DirectEvents>
                                        <Click OnEvent="btnLevelSaveUpdate_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                                <div class="btnFlatOr">
                                    or</div>
                                <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel"
                                    runat="server">
                                    <Listeners>
                                        <Click Handler="#{window}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
    <br />
</asp:Content>
