﻿<%@ Page Title="Pay Group Manager" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="PayGroupManager.aspx.cs" Inherits="Web.NewHR.PayGroupManager" %>

<%@ Register Src="~/Controls/BreadCrumbCtl.ascx" TagName="BreadCrumbCtl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
      var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.PayGroupID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDeleteLevel.ClientID %>.fireEvent('click');
                }

             }

    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 0px !important;
        }
        #menu
        {
            display: none;
        }
        .centerButtons
        {
            padding-left: 15px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="hdnDropMode" />
    <ext:Hidden runat="server" ID="hdnSource" />
    <ext:Hidden runat="server" ID="hdnDest" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDeleteLevel" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteLevel_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Group?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Pay Groups
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" >
        <div>
            
            <div  >
                <ext:GridPanel ID="GridLevels" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store3" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="PayGroupID">
                                    <Fields>
                                        <ext:ModelField Name="PayGroupID" Type="String" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Description" Type="string" />
                                        <ext:ModelField Name="TDSRate" Type="string" />
                                        <ext:ModelField Name="Income" Type="string"  />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column8" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Name" Align="Left" DataIndex="Name" />
                            <ext:Column ID="Column1" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Description" Align="Left" DataIndex="Description" />
                            <ext:Column ID="Column2" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                                Text="TDS Rate" Align="Left" DataIndex="TDSRate" />
                           <ext:Column ID="Column3" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Income" Align="Left" DataIndex="Income" />
                            <ext:CommandColumn TdCls="centerButtons" Width="100" ID="CommandColumn2" Align="Center"
                                runat="server" Text="Actions">
                                <Commands>
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                        CommandName="Edit" />
                                    <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                        CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                            <ext:CommandColumn Flex="1" />
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div class="buttonBlockSection">
                    <ext:Button runat="server" ID="btnAddLevel" Cls="btn btn-primary btn-sm btn-sect"
                        Text="<i></i>Add New Group">
                        <DirectEvents>
                            <Click OnEvent="btnAddLevel_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </div>
            </div>
        </div>
        <ext:Window ID="WindowLevel" runat="server" Title="Pay Group" Icon="Application"
            Width="450" Height="350" BodyPadding="5" Hidden="true" Modal="true">
            <Content>
                <table class="fieldTable">
                    <tr>
                        <td>
                            <ext:TextField ID="txtLevelName" LabelSeparator="" Width="200"  MinValue="0" runat="server" FieldLabel="Name"
                                LabelAlign="Top">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtLevelName" ErrorMessage="Please type a name." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextField ID="txtDesc" LabelSeparator="" Width="400" MinValue="0" runat="server" FieldLabel="Description"
                                LabelAlign="Top">
                            </ext:TextField>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:TextField ID="txtTDSRate" LabelSeparator="" Width="200" MinValue="0" runat="server" FieldLabel="TDS Rate"
                                LabelAlign="Top">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="txtTDSRate" ErrorMessage="TDS rate is required." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ext:ComboBox ID="cmbUnitIncomes" Width="200" runat="server" ValueField="IncomeId"
                                DisplayField="Title" FieldLabel="Unit Rate Income" LabelAlign="top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store13" runat="server">
                                        <Model>
                                            <ext:Model ID="Model14" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="IncomeId" Type="String" />
                                                    <ext:ModelField Name="Title" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                              <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                                ValidationGroup="SaveUpdateLevel" ControlToValidate="cmbUnitIncomes" ErrorMessage="Income is required." />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="2">
                            <div class="popupButtonDiv">
                                <ext:Button runat="server" ID="btnLevelSaveUpdate" Cls="btn btn-primary" Text="<i></i>Save"
                                    runat="server">
                                    <DirectEvents>
                                        <Click OnEvent="btnLevelSaveUpdate_Click">
                                            <EventMask ShowMask="true" />
                                        </Click>
                                    </DirectEvents>
                                    <Listeners>
                                        <Click Handler="valGroup = 'SaveUpdateLevel'; return CheckValidation();">
                                        </Click>
                                    </Listeners>
                                </ext:Button>
                               
                                <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel"
                                    runat="server">
                                    <Listeners>
                                        <Click Handler="#{WindowLevel}.hide();">
                                        </Click>
                                    </Listeners>
                                </ext:LinkButton>
                            </div>
                        </td>
                    </tr>
                </table>
            </Content>
        </ext:Window>
    </div>
    <br />
</asp:Content>
