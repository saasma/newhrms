﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true"
    CodeBehind="ExitClearanceFormList.aspx.cs" Inherits="Web.NewHR.ExitClearanceFormList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
    <style>
        #ctl00_ContentPlaceHolder_Main_TextFieldNote-bodyEl {
            padding-top: 25px;
            padding-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script>
        
        function OpenNewForm() {
            <%= hiddenValue.ClientID %>.clear();
            <%= NewForm.ClientID %>.show();
        }
        var setValue = function(record){
            <%= hiddenValue.ClientID %>.setValue(record.data.ID);
        }   

        function ClearControls(){
            <%= txtFormName.ClientID %>.setValue("");
            <%= cmbType.ClientID %>.setValue("");

            <%= chkLoanDetail.ClientID %>.setValue("");
            <%= chkAdvanceDetail.ClientID %>.setValue("");
            <%= chkRefundableDeposit.ClientID %>.setValue("");
            <%= chkAssetInCustody.ClientID %>.setValue("");

             <%= NewForm.ClientID %>.close();
        }

        function showWindow(command){
            ClearControls();
            if(command=="Edit")
            {
                <%= NewForm.ClientID %>.show();
                <%= btnView.ClientID %>.fireEvent('click');
            }
            if(command=="Delete")
            {
                <%= btnDelete.ClientID %>.fireEvent('click');
            } 
            if(command=="Archive")
            {
                <%= btnArchive.ClientID %>.fireEvent('click');
            }   
        }
    </script>
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Button runat="server" Hidden="true" ID="btnView">
        <DirectEvents>
            <Click OnEvent="btnView_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button runat="server" Hidden="true" ID="btnDelete">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button runat="server" Hidden="true" ID="btnArchive">
        <DirectEvents>
            <Click OnEvent="btnArchive_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="contentpanel" style='padding-top: 0px;'>
        <div class="innerLR">
            <h3>Resignation Requests</h3>
            <br />
            <table>
                <tr>
                    <td>
                        <ext:Button runat="server" Cls="btn btn-primary" Text="Add Form" OnClientClick="OpenNewForm()">
                        </ext:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TabPanel ID="tabPanel" runat="server" MinHeight="30" Width="260" ActiveTabIndex="0">
                            <Items>
                                <ext:Panel ID="tabPending" runat="server" Title="Current Forms">
                                </ext:Panel>
                                <ext:Panel ID="tabExitProcess" runat="server" Title="Old Unused Forms">
                                </ext:Panel>
                            </Items>
                            <Listeners>
                                <%--<TabChange Handler="searchList(#{tabPanel}.items.indexOf(#{tabPanel}.getActiveTab()));" />--%>
                            </Listeners>
                        </ext:TabPanel>
                    </td>
                    <td style="padding-left: 20px;">
                        <span id='spanText' style="color: blue"></span>
                    </td>
                </tr>
            </table>
            <ext:GridPanel ID="gridReqList" runat="server" Header="true" AutoScroll="true" Width="650">
                <Store>
                    <ext:Store ID="storeExitClearanceForm" runat="server" OnReadData="Store_ReadData" RemoteSort="true"
                        PageSize="20" GroupField="EmployeeName">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Sorters>
                            <ext:DataSorter Direction="ASC" Property="EmployeeName" />
                        </Sorters>

                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="RequestID">
                                <Fields>
                                    <ext:ModelField Name="ID" />
                                    <ext:ModelField Name="ClearanceFormName" />
                                    <ext:ModelField Name="Mandatory" />
                                    <ext:ModelField Name="Action" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>


                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Text="SN" Width="40" Align="Center" />
                        <ext:Column ID="colEId" Locked="true" runat="server" Text="Form Name" Width="130" DataIndex="ClearanceFormName"
                            Sortable="true" MenuDisabled="false">
                        </ext:Column>
                        <ext:DateColumn ID="Column10" Format="yyyy-MMM-dd" runat="server" Text="Mandatory"
                            Width="100" DataIndex="ExpectedDate" Sortable="true" MenuDisabled="false">
                        </ext:DateColumn>

                        <ext:CommandColumn runat="server" Text="Action" Width="260">
                            <Commands>
                                <ext:GridCommand CommandName="Edit" Text="Edit" />
                                <ext:GridCommand Icon="Delete" CommandName="Delete" Text="Delete" />
                                <ext:GridCommand CommandName="Archive" Text="Archive" />
                            </Commands>
                            <%-- <PrepareToolbar Fn="prepare" />--%>
                            <Listeners>
                                 <Command Handler="setValue(record);showWindow(command);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <Listeners>
                    <SelectionChange Handler="document.getElementById('spanText').innerHTML = this.getSelectionModel().selected.items.length + ' requests selected';" />
                </Listeners>
                <%-- <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Simple">
                        <Listeners>
                            <BeforeSelect Fn="gridRowBeforeSelect" />
                        </Listeners>
                        <Renderer Fn="renderSelect">
                        </Renderer>
                    </ext:CheckboxSelectionModel>
                </SelectionModel>--%>
                <View>
                    <ext:GridView ID="GridView1" EnableTextSelection="true" runat="server">
                        <Listeners>
                        </Listeners>
                    </ext:GridView>
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <%--<Select Handler="#{Store3}.pageSize = this.getValue();searchList()" />--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <br />
        </div>
    </div>
    <ext:Window
        ID="NewForm"
        runat="server"
        Title="Clearance Form"
        Icon="Application"
        Width="500"
        Height="400"
        Closable="false"
        BodyStyle="background-color: #fff;"
        BodyPadding="5"
        Hidden="true"
        Modal="true">
        <Content>
            <ext:FormPanel runat="server">
                <Items>
                    <ext:Label runat="server" Text="Form Name*"></ext:Label>
                    <ext:TextField ID="txtFormName" Width="460px" runat="server"></ext:TextField>
                    <ext:FieldSet
                        ID="chkFormDetail"
                        runat="server"
                        Flex="1"
                        Title="Show*"
                        Layout="AnchorLayout"
                        DefaultAnchor="100%">
                        <Items>
                            <ext:Checkbox ID="chkLoanDetail" runat="server" BoxLabel="Loan Details" />
                            <ext:Checkbox ID="chkAdvanceDetail" runat="server" BoxLabel="Advance Details" />
                            <ext:Checkbox ID="chkRefundableDeposit" runat="server" BoxLabel="Refundable Deposits" />
                            <ext:Checkbox ID="chkAssetInCustody" runat="server" BoxLabel="Assets In Custody" />
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldContainer Layout="HBoxLayout" runat="server">
                        <Items>
                            <ext:ComboBox runat="server" SelectedIndex="0" Width="120" ID="cmbType" ForceSelection="true"
                                FieldLabel="Send this form to*" LabelAlign="Top" DisplayField="UserName"
                                ValueField="UserID" QueryMode="Local"
                                LabelSeparator="">
                                <Store>
                                    <ext:Store ID="store1" runat="server">
                                        <Model>
                                            <ext:Model runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="UserName" />
                                                    <ext:ModelField Name="UserID" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:TextField Width="335px" ID="TextFieldNote" runat="server"></ext:TextField>
                        </Items>
                    </ext:FieldContainer>
                </Items>
                <Items>
                    <ext:Button Cls="btn btn-primary" runat="server" Text="Save">
                        <DirectEvents>
                            <Click OnEvent="btnSave">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button Cls="btn btn-primary" runat="server" OnClientClick="ClearControls()" Text="Cancel">
                        
                    </ext:Button>
                </Items>
            </ext:FormPanel>
        </Content>
    </ext:Window>
</asp:Content>
