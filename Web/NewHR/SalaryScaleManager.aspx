﻿<%@ Page Title="Salary Scale Setting" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="SalaryScaleManager.aspx.cs" Inherits="Web.NewHR.SalaryScaleManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var afterEdit = function (editor, e) {
            if (e.record.data.NoOfSteps == '')
                e.record.data.NoOfSteps = 0;
            if (e.record.data.StepRate == '')
                e.record.data.StepRate = 0;
            if (e.record.data.PayScale == '')
                e.record.data.PayScale = 0;
            e.record.commit();
        }
        var forcePostBack = function () {
            window.location = 'SalaryScaleManager.aspx';
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Pay Scale and Grades
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden ID="hdn" runat="server" Text="" />
        <table>
            <tr>
                <td>
                    <ext:ComboBox ID="cmbSalaryChangeDate" Width="180px" runat="server" ValueField="DateID"
                        DisplayField="Name" FieldLabel="Salary Scale History" LabelAlign="top" LabelSeparator=""
                        ForceSelection="true" QueryMode="Local">
                        <DirectEvents>
                            <Change OnEvent="btnGenerateScale_Click">
                                <EventMask ShowMask="true" />
                            </Change>
                        </DirectEvents>
                        <Store>
                            <ext:Store ID="Store3" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="DateID" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                    </ext:ComboBox>
                </td>
                <td style="vertical-align: bottom; padding-left: 10px;">
                    <ext:Button runat="server" ID="btnChange" Cls="btn btn-warning btn-sm btn-sect" Text="<i></i>Change Salary Scale"
                        runat="server">
                        <DirectEvents>
                            <Click OnEvent="btnChangeSalaryScale_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
                <td style="vertical-align: bottom; padding-left: 10px;">
                    <ext:Button runat="server" ID="btnAdd" Cls="btn btn-success btn-sm btn-sect" Text="<i></i>New Salary Scale"
                        runat="server">
                        <DirectEvents>
                            <Click OnEvent="btnAddSalaryScale_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <ext:Window ID="WindowLevel" runat="server" Title="Salary Scale and Grades" Icon="Application"
            Width="800" Height="630" BodyPadding="10" Hidden="true" Modal="true" AutoScroll="true">
            <Items>
                <ext:Container runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:TextField ID="txtName" FieldLabel="Name" runat="server" LabelAlign="Top" Width="180"
                                        LabelSeparator="">
                                    </ext:TextField>
                                    <asp:RequiredFieldValidator ErrorMessage="Please type a Name." Display="None" ID="RequiredFieldValidator10"
                                        ControlToValidate="txtName" ValidationGroup="InsertUpdate" runat="server">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td style="padding-left: 10px">
                                    <pr:CalendarExtControl Width="180px" FieldLabel="Effective From" ID="calEffectiveFrom"
                                        runat="server" LabelAlign="Top" LabelSeparator="">
                                    </pr:CalendarExtControl>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
                <ext:GridPanel Width="600" StyleSpec="margin-top:15px;margin-bottom:20px!important;"
                    ID="gridSalaryScale" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store1" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="LevelId" Type="String" />
                                        <ext:ModelField Name="GroupLevel" Type="String" />
                                        <ext:ModelField Name="LevelCode" Type="string" />
                                        <ext:ModelField Name="PayScale" Type="string" />
                                        <ext:ModelField Name="StepRate" Type="string" />
                                        <ext:ModelField Name="NoOfSteps" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <Plugins>
                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                            <Listeners>
                                <%-- <BeforeEdit Fn="beforeEdit" />--%>
                                <Edit Fn="afterEdit" />
                            </Listeners>
                        </ext:CellEditing>
                    </Plugins>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column5" Sortable="false" Width="140" MenuDisabled="true" runat="server"
                                Text="Group" Align="Left" DataIndex="GroupLevel" />
                            <ext:Column ID="Column6" Sortable="false" Width="100" MenuDisabled="true" runat="server"
                                Text="Level Code" Align="Left" DataIndex="LevelCode">
                            </ext:Column>
                            <ext:Column ID="Column7" Sortable="false" Width="110" MenuDisabled="true" runat="server"
                                Text="Pay Scale" Align="Center" DataIndex="PayScale">
                                <Renderer Fn="getFormattedAmount" />
                                <Editor>
                                    <ext:TextField MaskRe="[0-9]|\.|%" runat="server" MinValue="0">
                                    </ext:TextField>
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column8" Sortable="false" Width="110" MenuDisabled="true" runat="server"
                                Text="Grade Rate" Align="Center" DataIndex="StepRate">
                                <Renderer Fn="getFormattedAmount" />
                                <Editor>
                                    <ext:TextField MaskRe="[0-9]|\.|%" ID="NumberField11" runat="server" MinValue="0" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column9" Sortable="false" Width="110" MenuDisabled="true" runat="server"
                                Text="No of Grades" Align="Center" DataIndex="NoOfSteps">
                                <%-- <Renderer Fn="getFormattedAmount" />--%>
                                <Editor>
                                    <ext:TextField MaskRe="[0-9]" ID="NumberField21" runat="server" MinValue="0" />
                                </Editor>
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:CellSelectionModel ID="CellSelectionModel1" runat="server" Mode="Simple" />
                    </SelectionModel>
                  
                </ext:GridPanel>
                <ext:Button runat="server" Cls="btn btn-primary" ID="btnChangeScale" Text="<i></i>Save Scale">
                    <DirectEvents>
                        <Click OnEvent="btnChangeScale_Click">
                            <ExtraParams>
                                <ext:Parameter Name="ScaleList" Value="Ext.encode(#{gridSalaryScale}.getRowsValues({selectedOnly : false}))"
                                    Mode="Raw" />
                            </ExtraParams>
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                    <Listeners>
                        <Click Handler="valGroup='InsertUpdate';return CheckValidation();" />
                    </Listeners>
                </ext:Button>
            </Items>
        </ext:Window>
        <div>
            <ext:GridPanel Height="600" StyleSpec="margin-top:15px;" ID="GridPayScale" runat="server"
                Cls="itemgrid">
                <Store>
                    <ext:Store ID="StoreEducation" runat="server">
                        <Model>
                            <ext:Model ID="Model3" runat="server">
                                <Fields>
                                    <ext:ModelField Name="GroupLevel" Type="String" />
                                    <ext:ModelField Name="Level" Type="String" />
                                    <ext:ModelField Name="LevelCode" Type="string" />
                                    <ext:ModelField Name="PayScale" Type="string" />
                                    <ext:ModelField Name="StepRate" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column1" Sortable="false" Width="150" MenuDisabled="true" runat="server"
                            Text="Group" Align="Left" DataIndex="GroupLevel" />
                        <ext:Column ID="Column10" Sortable="false" Width="100" MenuDisabled="true" runat="server"
                            Text="Level" Align="Left" DataIndex="Level" />
                        <ext:Column ID="Column2" Sortable="false" Width="100" MenuDisabled="true" runat="server"
                            Text="Level Code" Align="Left" DataIndex="LevelCode">
                        </ext:Column>
                        <ext:Column ID="Column3" Sortable="false" Width="80" MenuDisabled="true" runat="server"
                            Text="Pay Scale" Align="Right" DataIndex="PayScale">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Column4" Sortable="false" Width="85" MenuDisabled="true" runat="server"
                            Text="Grade Rate" Align="Right" DataIndex="StepRate">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:CellSelectionModel ID="RowSelectionModel1" runat="server" Mode="Simple" />
                </SelectionModel>
                  <View>
                        <ext:GridView ID="GridView1" runat="server" EnableTextSelection="true" />
                    </View>
            </ext:GridPanel>
        </div>
    </div>
</asp:Content>
