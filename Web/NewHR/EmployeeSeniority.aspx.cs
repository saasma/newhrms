﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL;
using DAL;
using BLL.Manager;
using Utils.Helper;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using Utils;
using System.Xml.Linq;
using Utils.Calendar;
using BLL.Entity;
using BLL.BO;

namespace Web.NewHR
{
    public partial class EmployeeSeniority : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                // Initialize();
            }
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "EmpShiftPopup", "../ExcelWindow/SeniorityImportExcel.aspx", 450, 500);
        }
        public void Initialize()
        {
            //storeGroup.DataSource = AppraisalManager.GetAllRolloutGroup();
            //storeGroup.DataBind();
        }

        protected void btnEditGroup_Click(object sender, DirectEventArgs e)
        {
            clearPopup();
            txtEmpName.Text = hiddenValueEmpName.Text.Trim();
            txtSeniorityValue.Text = hiddenValueSeniority.Text.Trim();
            WindowEmp.Show();
        }

        protected void clearPopup()
        {
            txtEmpName.Text = "";
            txtSeniorityValue.Text = "";
        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (txtEmpName.Text == "")
            {
                NewMessage.ShowWarningMessage("Employee name cannot be empty.");
                txtEmpName.Focus();
                return;
            }
            if (txtSeniorityValue.Text == "")
            {
                NewMessage.ShowWarningMessage("Seniority value cannot be empty.");
                txtSeniorityValue.Focus();
                return;
            }

            EEmployee _dbobj = new EEmployee();
            if (!string.IsNullOrEmpty(hiddenValueEmpId.Text))
            {
                _dbobj.EmployeeId = int.Parse(hiddenValueEmpId.Text.Trim());
            }

            int sen;
            if (int.TryParse(txtSeniorityValue.Text.Trim(), out sen) == true)
            {
                _dbobj.Seniority = sen;
            }
            else
            {
                NewMessage.ShowWarningMessage("Please enter valid seniority.");
                txtSeniorityValue.Focus();
                return;
            }

            Status status = AppraisalManager.UpdateEmpSeniority(_dbobj);
            if (status.IsSuccess == true)
            {
                NewMessage.ShowNormalMessage("Sucessfully updated !");
                WindowEmp.Hide();
                PagingToolbar1.DoRefresh();
                return;
            }
            else
            {
                NewMessage.ShowWarningMessage("Unable to edit !");
                return;
            }


        }
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int total = 0, pageSize = 0, employeeId = -1, GroupId = -1;
            string employeeName = cmbEmpSearch.Text;

            if (cmbEmpSearch.SelectedItem != null && cmbEmpSearch.SelectedItem.Value != null)
            {
                int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);
                if (employeeId != 0)
                    employeeName = "";
                else
                    employeeId = -1;
            }
            pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            List<GetEmployeeSeniorityListResult> list = AppraisalManager.GetEmployeeSeniorityList(e.Start / pageSize, pageSize, employeeId, employeeName);

            gridEmpStore.DataSource = list;
            gridEmpStore.DataBind();

            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            e.Total = total;
        }

    }
}
