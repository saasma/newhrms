﻿<%@ Page Title="Employee Leave Information" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeLeave.aspx.cs" Inherits="Web.NewHR.EmployeeLeave" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function popupLeaveCall() {
            if (typeof (EmployeeId) != 'undefined') {
                var ret = popupLeave("EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'ReloadLeave') {
                        refreshLeaveList(0);
                    }
                }
            }
            return false;
        }

        function reloadLeave(closingWindow) {
            closingWindow.close();
            refreshLeaveList(0); ;
        }

        function popupUpdateLeaveCall(leaveId) {
            if (typeof (EmployeeId) != 'undefined') {

                var ret = popupUpdateLeave('Id=' + leaveId + "&EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'Reload') {
                        refreshLeaveList(0);
                    }
                }
            }
            return false;
        }
        function popupUpdateMannualLeaveCall(leaveId) {
            if (typeof (EmployeeId) != 'undefined') {

                var ret = popupUpdateMannualLeave('Id=' + leaveId + "&EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'Reload') {
                        refreshLeaveList(0);
                    }
                }
            }
            return false;
        }
        //call to delete income
        function deleteLeaveCall(leaveId) {
            if (confirm("Do you want to delete the leave for the employee?")) {
                refreshLeaveList(leaveId);
            }
            return false;
        }

        function refreshLeaveList(LeaveId) {
            if (typeof (EmployeeId) != 'undefined') {
                showLoading();
                Web.PayrollService.GetLeaveList(LeaveId, parseInt(EmployeeId), refreshLeaveListCallback);
            }
        }

        function refreshLeaveListCallback(result) {
            if (result) {
                document.getElementById('<%=leaves.ClientID %>').innerHTML
                = result;
            }
            hideLoading();
        }

    </script>
    <style type="text/css">
        .balanceTD
        {
            text-align: right !important;
            padding-right: 5px !important;
        }
        .table-condensed td
        {
            padding: 2px;
            padding-left: 4px;
            padding-right: 2px;
        }
        .userDetailsClass
        {
            background-color: #D8E7F3;
            height: 50px;
            text-align: center;
            padding-top: 10px;
            margin-bottom: 10px;
            margin-left: 18px;
        }
        
        .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td
        {
            padding:3px;
            line-height:0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <%--   <ul class="breadcrumb">
        <li><a href="index.html?lang=en" class="glyphicons home"><i></i>Smashing</a></li>
        <li class="divider"></li>
        <li>Bookings</li>
    </ul>--%>
    <div class="pageheader">
        <div class="media">
            <div class="media-body" style="margin-left: 145px;">
                <h4>
                    Employee Leave
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <%--<uc2:EmployeeWizard Id="EmployeeWizard1" runat="server" />--%>
            <div class="separator bottom">
            </div>
            <table>
                <tr>
                    <td valign="top">
                        <div style="float: left; margin-right: 10px;">
                            <ucEW:EmpDetailsWizard Id="EmpDetailsWizard1" runat="server" />
                        </div>
                    </td>
                    <td valign="top">
                        <div class="userDetailsClass" style="width: 1110px;">
                            <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                        </div>
                        <div>
                            <div style="">
                                <!-- panel-btns -->
                                <h4 class="sectionHeading" style="margin-bottom: 0px; width: 1110px">
                                    <i></i>Leaves
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body">
                                <div id="leaves" runat="server" style="width: 1110px">
                                </div>
                                <div style="margin-top: 20px; margin-bottom: 20px;">
                                    <ext:Button runat="server" Cls="btn btn-primary btn-sect" runat="server" Width="120"
                                        StyleSpec="margin-top:10px" Height="30" ID="btnSaveUpdate" Text="<i></i>Add Leave">
                                        <Listeners>
                                            <Click Handler="return popupLeaveCall();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
</asp:Content>
