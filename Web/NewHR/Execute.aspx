﻿<%@ Page MaintainScrollPositionOnPostback="true" Title="Execute.aspx" Language="C#"
    MasterPageFile="~/Master/Details.Master" ValidateRequest="false" AutoEventWireup="true"
    CodeBehind="Execute.aspx.cs" Inherits="Web.CP.Execute" %>

<%--    ValidateRequest="false" must be set if we are going to execute the whole sql containing all
functions & sp as there are values for Potiential html error
--%>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="contentArea">
        <asp:TextBox runat="server" ID="txt1" TextMode="MultiLine" Rows="17" Width="700px"
            Columns="5">
            

        </asp:TextBox>
        <br />
        <br />
        <asp:Button runat="server" OnClick="btn1_Click" ID="btn1" CssClass="save" Text="Run" />
        <br />
        <br />
          <h3>
            Key:</h3>
        &nbsp;<asp:TextBox runat="server" ID="pwd" Width="500"></asp:TextBox>
        <br />
        <br />
        <h3>
            Status:
        </h3>
        <asp:Label ID="lblStatus" runat="server" EnableViewState="False" />
    </div>
    <div style="display: none">
        <asp:GridView runat="server" ID="gvw">
        </asp:GridView>
    </div>
</asp:Content>
