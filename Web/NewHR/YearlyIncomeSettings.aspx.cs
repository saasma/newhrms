﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using DAL;
using BLL.Base;

namespace Web.NewHR.UserControls
{
    public partial class YearlyIncomeSettings : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }
        protected void Initialise()
        {
            LoadList();
            storeIncomes.DataSource = VouccherManager.GetTaxCertificateIncomeList().OrderBy
                (x => x.HeaderName).ToList();
            storeIncomes.DataBind();
        }

        private void LoadList()
        {
            int Type = -1;
            Grid.Store[0].DataSource = VouccherManager.GetTaxCertificateIncomeList().OrderBy
                (x => x.HeaderName).ToList();
            Grid.Store[0].DataBind();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {

            this.SaveFormData();

        }
        protected void ClearFields()
        {
            this.hdnID.Text = "";
            cmbType.ClearValue();

            btnSave.Text = Resources.Messages.Save;
        }


        [DirectMethod]
        public object SaveRow(string typesourceid, bool isPhantom, JsonObject values)
        {

            //string typesourceid = cmbIncomes.SelectedItem.Value;

            int type = int.Parse(typesourceid.Substring(0, typesourceid.IndexOf(":")));
            int sourceId = int.Parse(typesourceid.Substring(typesourceid.IndexOf(":") + 1));

            string selectedValue = values["Value"].ToString();

             int typeenum=0;

             if (int.TryParse(selectedValue, out typeenum))
             {


                 

                 YearlyIncome entity = new YearlyIncome();
                 entity.Type = type;
                 entity.SourceID = sourceId;
                 entity.TypeEnum = typeenum;


                 Status myStatus = NewPayrollManager.InsertUpdateYearlyIncome(entity);

                 if (myStatus.IsSuccess)
                 {

                    // NewMessage.ShowNormalPopup("Record Save successfully");


                     //  this.ClearFields();
                     //this.AEWindow.Close();

                     return new { valid = true };

                 }
                 else
                     return new { valid = false, Message = myStatus.ErrorMessage };
                 // NewMessage.ShowWarningMessage(myStatus.ErrorMessage);

             }
             return new { valid = false };
            //var myJsonString = values.ToString();
            //var strJObject = JObject.Parse(myJsonString);
            //List<GetEmployeesIncomeForVariableIncomesResult> projectPays = new List<GetEmployeesIncomeForVariableIncomesResult>();
            //int indx = 0;
            //foreach (System.Web.UI.WebControls.ListItem item in chkVariableIncomes.Items)
            //{
            //    if (chkVariableIncomes.Items[indx].Selected == true)
            //    {
            //        GetEmployeesIncomeForVariableIncomesResult projectPay = new GetEmployeesIncomeForVariableIncomesResult();
            //        projectPay.IncomeId = int.Parse(item.Value);
            //        projectPay.EmployeeId = int.Parse(EmployeeID);
            //        if (values["P_" + item.Value] != null)
            //            projectPay.Amount = decimal.Parse(values["P_" + item.Value].ToString());
            //        projectPays.Add(projectPay);
            //    }
            //    indx++;
            //}

            //int count = 0;
            //ResponseStatus status = PayManager.SaveVariableIncome(projectPays, isDeduction, ref count);
            //if (status.IsSuccessType == true)
            //{
            //    // NewMessage.ShowNormalMessage("The Information has been updated");
            //    Notification.Show(new NotificationConfig
            //    {
            //        Title = "Notification",
            //        Icon = Icon.Information,
            //        AutoHide = true,
            //        Html = "The Information has been updated",
            //    });
            //    return new { valid = true };
            //}
            //else
            //    return new { valid = false, Message = status.ErrorMessage };

        }

        public void EditYearlyIncome(int type, int sourceId)
        {
            YearlyIncome entity = NewPayrollManager.GetYearlyIncomeByIncomeId(type, sourceId);
            ClearFields();
            this.hdnID.Text = type.ToString();

            cmbType.ClearValue();

            if (entity != null)
            {
                X.AddScript("#{cmbType}.getStore().clearFilter();");
                cmbType.SetValue(entity.TypeEnum.ToString());
            }
            else
                cmbType.ClearValue();

            cmbIncomes.ClearValue();
            cmbIncomes.SetValue(type + ":" + sourceId);

            //ExtControlHelper.ComboBoxSetSelected(entity.Type.ToString(), cmbType);
            //ExtControlHelper.ComboBoxSetSelected(entity.IncomeId.ToString(), cmbIncomes);
            btnSave.Text = Resources.Messages.Update;
            this.AEWindow.Show();
        }

        protected void btnAddNewLine_Click(object sender, DirectEventArgs e)
        {
            this.ClearFields();
            this.AEWindow.Show();

        }
        protected void GridCommand(object sender, DirectEventArgs e)
        {
            string commandName = e.ExtraParams["command"];
            string typesourceid = (e.ExtraParams["ID"]).ToString();

            int type = int.Parse(typesourceid.Substring(0, typesourceid.IndexOf(":")));
            int sourceId = int.Parse(typesourceid.Substring(typesourceid.IndexOf(":") +1));


            this.EditYearlyIncome(type, sourceId);


        }
        protected void DeleteData(int ID)
        {

            Status status = NewPayrollManager.DeleteDeputation(ID);

            if (status.IsSuccess)

                LoadList();
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }


        private void SaveFormData()
        {

            Status myStatus = new Status();

            string typesourceid = cmbIncomes.SelectedItem.Value;

            int type = int.Parse(typesourceid.Substring(0, typesourceid.IndexOf(":")));
            int sourceId = int.Parse(typesourceid.Substring(typesourceid.IndexOf(":") + 1));
            int typeenum = int.Parse(cmbType.SelectedItem.Value);

            YearlyIncome entity = new YearlyIncome();
            entity.Type = type;
            entity.SourceID = sourceId;
            entity.TypeEnum = typeenum;


            myStatus = NewPayrollManager.InsertUpdateYearlyIncome(entity);

            if (myStatus.IsSuccess)
            {

                NewMessage.ShowNormalPopup("Record Save successfully");


                //  this.ClearFields();
                this.AEWindow.Close();
                LoadList();

            }
            else
                NewMessage.ShowWarningMessage(myStatus.ErrorMessage);
        }
    }
}