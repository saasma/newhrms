﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.CP.Report.Templates.HR;
using DevExpress.XtraReports.UI;
using BLL.BO;

namespace Web.NewHr
{
    public partial class PeriodicLeaveTakenReport : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
               Initialize();

                              

            }
        }

        private void Initialize()
        {
                        

            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            //cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.LevelAndDesignation).ToList();
            //cmbDesignation.Store[0].DataBind();

            //this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            //this.storeLevel.DataBind();


            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .OrderBy(x => x.Order).ToList();

            leaveList.Add(new LLeaveType { LeaveTypeId = 0, Title = "Unpaid Leave" });

            int index = 0;
            foreach (LLeaveType item in leaveList)
            {
                index += 1;

                Column col = gridBranchTransfer.ColumnModel.FindControl("ColumnLeave" + index) as Column;
                if (col != null)
                {
                    col.Text = item.Title;
                    col.Visible = true;
                }
            }


        }

        private void Clear()
        {
            
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            //if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
            //    levelId = int.Parse(cmbLevel.SelectedItem.Value);
            //if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
            //    designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            DateTime start = txtFromDate.SelectedDate;
            DateTime end = txtToDate.SelectedDate;

            DateTime? PayrollStartDate = null; DateTime? PayrollEndDate = null;
            DateTime? startDateLeaveTaken = null; DateTime? endDateLeaveTaken = null;

            CommonManager.SetPeriodForPeriodicLeaveTaken(start, end, ref PayrollStartDate, ref PayrollEndDate, ref startDateLeaveTaken, ref endDateLeaveTaken);


            List<Report_HR_YearlyLeaveBalanceResult> list1 = EmployeeManager.GetPeriodicLeaveTakenReport
                (branchId, depId, levelId, designationId, employeeId, e.Page - 1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords
                , start, end, PayrollStartDate, PayrollEndDate, startDateLeaveTaken, endDateLeaveTaken);

            e.Total = totalRecords;

            //if (list1.Any())
            storeEmpList.DataSource = list1;
            storeEmpList.DataBind();

        }

     
        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            //if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
            //    levelId = int.Parse(cmbLevel.SelectedItem.Value);
            //if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
            //    designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;


            DateTime start = txtFromDate.SelectedDate;
            DateTime end = txtToDate.SelectedDate;

            DateTime? PayrollStartDate = null; DateTime? PayrollEndDate = null;
            DateTime? startDateLeaveTaken = null; DateTime? endDateLeaveTaken = null;

            CommonManager.SetPeriodForPeriodicLeaveTaken(start, end, ref PayrollStartDate, ref PayrollEndDate, ref startDateLeaveTaken, ref endDateLeaveTaken);



            List<Report_HR_YearlyLeaveBalanceResult> list1 = EmployeeManager.GetPeriodicLeaveTakenReport
                (branchId, depId, levelId, designationId, employeeId, 0, 99999, "", ref totalRecords
                , start, end, PayrollStartDate, PayrollEndDate, startDateLeaveTaken, endDateLeaveTaken);



            ReportDataSet.YearlyReportTableDataTable table = new ReportDataSet.YearlyReportTableDataTable();

            foreach (Report_HR_YearlyLeaveBalanceResult item in list1)
            {
                ReportDataSet.YearlyReportTableRow row = table.NewYearlyReportTableRow();
                row.EIN = item.EmployeeId.ToString();
                row.Name = item.Name;

                
               
                row.L1Taken = item.L1Taken;
                if (item.L1Taken == 0)
                    row.SetL1TakenNull();
               
                row.L2Taken = item.L2Taken;
                if (item.L2Taken == 0)
                    row.SetL2TakenNull();
              
                row.L3Taken = item.L3Taken;
                if (item.L3Taken == 0)
                    row.SetL3TakenNull();
               
                row.L4Taken = item.L4Taken;
                if (item.L4Taken == 0)
                    row.SetL4TakenNull();
               
                row.L5Taken = item.L5Taken;
                if (item.L5Taken == 0)
                    row.SetL5TakenNull();
               
                row.L6Taken = item.L6Taken;
                if (item.L6Taken == 0)
                    row.SetL6TakenNull();
              
                row.L7Taken = item.L7Taken;
                if (item.L7Taken == 0)
                    row.SetL7TakenNull();
                
                row.L8Taken = item.L8Taken;
                if (item.L8Taken == 0)
                    row.SetL8TakenNull();
                
                row.L9Taken = item.L9Taken;
                if (item.L9Taken == 0)
                    row.SetL9TakenNull();
                
                row.L10Taken = item.L10Taken;
                if (item.L10Taken == 0)
                    row.SetL10TakenNull();
                
                row.L11Taken = item.L11Taken;
                if (item.L11Taken == 0)
                    row.SetL11TakenNull();
                

                table.Rows.Add(row);
            }




            PeriodicLeaveTaken report1 = new PeriodicLeaveTaken();

            report1.DataSource = table;
            report1.DataMember = "Report";

            if (IsEnglish == false)
            {
               

                
                    report1.labelTitle.Text += start.ToString("yyyy-MMM-dd") + "/" + end.ToString("yyyy-MMM-dd");
            }


            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
              .OrderBy(x => x.Order).ToList();
            leaveList.Add(new LLeaveType { LeaveTypeId = 0, Title = "Unpaid Leave" });
            int index = 0;
            foreach (LLeaveType leave in leaveList)
            {
                index += 1;

                XRTableCell leaveTitle = report1.FindControl("leave" + index, true) as XRTableCell;
                if (leaveTitle != null)
                {
                    leaveTitle.Text = leave.Title;
                    leaveTitle.Visible = true;
                    //(report1.Bands[BandKind.PageHeader] .FindControl("panel" + index, true) as XRPanel).Visible = true;
                    (report1.Bands[BandKind.Detail].FindControl("paneldata" + index, true) as XRLabel).Visible = true;
                }
            }

            using (MemoryStream stream = new MemoryStream())
            {
                
                report1.ExportToXls(stream);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AppendHeader("Content-Type", "application/pdf");
                HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=PeriodicLeaveTakenReport" + ".xls");
                HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                HttpContext.Current.Response.End();
            }

        }


    }
}