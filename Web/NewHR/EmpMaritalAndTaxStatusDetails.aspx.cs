﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class EmpMaritalAndTaxStatusDetails : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/EmpMaritalTaxStatusImportExcel.aspx", 450, 500);
        }

        private void Initialise()
        {
            cmbSubDepartment.Store[0].DataSource = new DepartmentManager().GetSubDepartmentsByCompany(SessionManager.CurrentCompanyId);
            cmbSubDepartment.Store[0].DataBind();

            storeEmployee.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport();
            storeEmployee.DataBind();

            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments();
            cmbDepartment.Store[0].DataBind();

            storeLevel.DataSource = ActivityManager.GetLevels();
            storeLevel.DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.Name).ToList();
            cmbDesignation.Store[0].DataBind();

            List<KeyValue> list = new JobStatus().GetMembers();
            list.RemoveAt(0);
            cmbJobStatus.Store[0].DataSource = list;
            cmbJobStatus.Store[0].DataBind();

            if (CommonManager.IsUnitEnabled)
            {
                cmbUnit.Show();
                cmbUnit.Store[0].DataSource = CommonManager.GetAllUnitList();
                cmbUnit.Store[0].DataBind();
            }

            List<KeyValue> maritalStatusKV = new MaritalStatus().GetMembers();
            KeyValue objKV = new KeyValue() { Key = "All", Value = "All" };
            maritalStatusKV.Insert(0, objKV);
            cmbMaritalStatus.Store[0].DataSource = maritalStatusKV;
            cmbMaritalStatus.Store[0].DataBind();

            cmbMartialStatusEdit.Store[0].DataSource = new MaritalStatus().GetMembers();
            cmbMartialStatusEdit.Store[0].DataBind();            

            if (SessionManager.IsReadOnlyUser)
            {
                CommandColumnChange.Hide();
            }
        }
                

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            string employeeIdList = "";

            foreach (Tag tag in tfEmployee.Tags)
            {
                employeeIdList += tag.Value + ",";
            }

            if (!string.IsNullOrEmpty(employeeIdList))
                employeeIdList = employeeIdList.TrimEnd(',');

            int branchId = -1, departmentId = -1, designationId = -1, levelId = -1, subDepartmentId = -1, unitId = -1;
            string statusList = "", maritalStatus = "";
            bool? hasCoupleTaxStatus = null;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            foreach (Ext.Net.ListItem item in cmbJobStatus.SelectedItems)
                statusList += item.Value.ToString() + ",";

            if (statusList != "")
                statusList = statusList.TrimEnd(',');

            if (cmbSubDepartment.SelectedItem != null && cmbSubDepartment.SelectedItem.Value != null)
                subDepartmentId = int.Parse(cmbSubDepartment.SelectedItem.Value);

            if (cmbUnit.SelectedItem != null && cmbUnit.SelectedItem.Value != null)
                unitId = int.Parse(cmbUnit.SelectedItem.Value);

            if (cmbMaritalStatus.SelectedItem != null && cmbMaritalStatus.SelectedItem.Value != null)
            {
                maritalStatus = cmbMaritalStatus.SelectedItem.Value;
                if (maritalStatus == "All")
                    maritalStatus = "";
            }

            if (cmbTaxStatus.SelectedItem != null && cmbTaxStatus.SelectedItem.Value != null)
            {
                if (cmbTaxStatus.SelectedItem.Value == "1")
                    hasCoupleTaxStatus = false;
                else if (cmbTaxStatus.SelectedItem.Value == "2")
                    hasCoupleTaxStatus = true;
            }


            storeMaritalTaxStatus.DataSource = NewHRManager.GetEmployeeMaritalAndTaxStatusList(employeeIdList, branchId, departmentId, designationId, levelId,
                statusList, subDepartmentId, unitId, maritalStatus, hasCoupleTaxStatus);
            storeMaritalTaxStatus.DataBind();           
                        
        }

        private void ClearFields()
        {
            txtName.Clear();
            txtINo.Clear();
            txtBranch.Clear();
            txtDepartment.Clear();
            cmbMartialStatusEdit.ClearValue();
            chkHasCoupleTaxStatus.Checked = false;
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(hdnEmployeeId.Text);

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            txtName.Text = emp.Name;
            txtINo.Text = hdnINo.Text;
            txtBranch.Text = hdnBranch.Text;
            txtDepartment.Text = hdnDepartment.Text;

            cmbMartialStatusEdit.ClearValue();
            chkHasCoupleTaxStatus.Checked = false;
            chkHasCoupleTaxStatus.Disable();

            if (!string.IsNullOrEmpty(emp.MaritalStatus))
            {
                cmbMartialStatusEdit.SetValue(emp.MaritalStatus);

                if(emp.MaritalStatus == MaritalStatus.MARRIED)
                    chkHasCoupleTaxStatus.Enable();
            }
                

            if (emp.HasCoupleTaxStatus != null && emp.HasCoupleTaxStatus.Value)
                chkHasCoupleTaxStatus.Checked = true;

            WDetails.Center();
            WDetails.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdDetails");
            if (this.IsValid)
            {
                int employeeId = int.Parse(hdnEmployeeId.Text);
                string maritalStatus = cmbMartialStatusEdit.SelectedItem.Value;
                bool hasCoupleTaxStatus = false;

                if (chkHasCoupleTaxStatus.Checked)
                    hasCoupleTaxStatus = true;

                Status status = NewHRManager.UpdateEmployeeTaxMaritalStatus(employeeId, maritalStatus, hasCoupleTaxStatus);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Status updated successfully.");

                    WDetails.Close();
                    btnLoad_Click(null, null);
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);


            }
        }

    }

    
}