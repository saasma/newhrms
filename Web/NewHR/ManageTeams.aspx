﻿<%@ Page Title="Manage Teams" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true"
    CodeBehind="ManageTeams.aspx.cs" Inherits="Web.NewHR.ManageTeams" %>

<%@ Register Src="~/newhr/UserControls/GradeRewardCtl.ascx" TagName="GradeRewardCtl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <script type="text/javascript">

        var targetTeamId = "";
        var sourceIDList = "";

        var dropHandler = function (e1, e2, e3, e4, e5) {
            <%=hdnTeam.ClientID %>.setValue(targetTeamId);
            <%=hdnList.ClientID %>.setValue(sourceIDList);
            <%=btnUpdate.ClientID %>.fireEvent('click');
        }
        var nodeDragOver = function (targetNode, position, dragData) {

            // target node should be 2 (Team)
            if (targetNode.data.depth != 2) {
                return false;
            }

            targetTeamId = targetNode.data.id;

            sourceIDList = "";

            for (i = 0; i < dragData.records.length; i++) {
                var record = dragData.records[i];
                // if dragged Depth is not 3(Employee) then cancel the drag & drop
                if (record.data.depth != 3)
                    return false;

                if (sourceIDList == "")
                    sourceIDList = record.data.id;
                else
                    sourceIDList += "," + record.data.id;

            }

            return true;
        };

         function expandAllCollapse1(btn) {
            if (btn.getText() == "Expand") {
                btn.setText("Collapse");

                <%=treePanelEmpStruct.ClientID %>.expandAll();
            }
            else {
                btn.setText("Expand");


               
               <%=treePanelEmpStruct.ClientID %>.collapseAll();
            }
        }
         function expandAllCollapse2(btn) {
            if (btn.getText() == "Expand") {
                btn.setText("Collapse");

                <%=treePanelLeaveTeams.ClientID %>.expandAll();
            }
            else {
                btn.setText("Expand");


               
               <%=treePanelLeaveTeams.ClientID %>.collapseAll();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

     <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Manage Teams
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
    
    <div class="separator bottom">
    </div>
    <ext:Hidden ID="hdnTeam" Text="" runat="server" />
    <ext:Hidden ID="hdnList" Text="" runat="server" />
    <ext:Button ID="btnUpdate" runat="server" Hidden="true" Cls="btn btn-primary">
        <DirectEvents>
            <Click OnEvent="btnUpdate_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    
    <div style="margin-bottom: 10px; font-weight: 10px;">
        You can drag the employee name from the Employee Structure (left panel) to the Leave
        Team on the right panel to assign the selected employee to a team.
    </div>
    <table>
        <tr>
            <td valign="top">
                <ext:Button runat="server"  ID="expandAllCollapseAll" Cls="btn btn-primary"
                    Text="Expand">
                    <Listeners>
                        <Click Handler="expandAllCollapse1(this);" />
                    </Listeners>
                </ext:Button>
                <ext:TreePanel MultiSelect="true" Icon="ApplicationGet" ID="treePanelEmpStruct" RootVisible="false"
                    AutoScroll="true" Animate="true" Mode="Local" runat="server" Collapsible="false"
                    UseArrows="true" Title="Employee Structure">
                    <Fields>
                        <ext:ModelField Name="projectName" />
                    </Fields>
                    <ColumnModel>
                        <Columns>
                            <ext:TreeColumn ID="TreeColumn1" Width="300" runat="server" Text="Employee" DataIndex="text"
                                Flex="1">
                            </ext:TreeColumn>
                            <ext:Column ID="Column1" Width="300" runat="server" Text="Associated Team" Flex="1"
                                DataIndex="projectName">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <%-- <Listeners>
                        <beforenodedrop fn="beforenodedrop" />
                    </Listeners>--%>
                    <Listeners>
                    </Listeners>
                    <View>
                        <ext:TreeView ID="TreeView1" runat="server">
                            <Plugins>
                                <ext:TreeViewDragDrop ID="TreeViewDragDrop1" EnableDrag="true" EnableDrop="false"
                                    runat="server" AppendOnly="true" />
                            </Plugins>
                        </ext:TreeView>
                    </View>
                </ext:TreePanel>
            </td>
            <td style="width: 50px">
            </td>
            <td valign="top">
                <ext:Button runat="server"  ID="LinkButton1" Text="Expand" Cls="btn btn-primary">
                    <Listeners>
                        <Click Handler="expandAllCollapse2(this);" />
                    </Listeners>
                </ext:Button>
                <ext:TreePanel ID="treePanelLeaveTeams" RootVisible="false" Width="400" AutoScroll="true"
                    Animate="true" Mode="Local" runat="server" Collapsible="false" UseArrows="true"
                    Title="Leave Teams">
                    <ColumnModel>
                        <Columns>
                            <ext:TreeColumn ID="TreeColumn2" Width="300" runat="server" Text="Teams" DataIndex="text"
                                Flex="1">
                            </ext:TreeColumn>
                        </Columns>
                    </ColumnModel>
                    <View>
                        <ext:TreeView ID="treeViewLeaveTeams" runat="server">
                            <Listeners>
                                <Drop Fn="dropHandler" />
                            </Listeners>
                            <Plugins>
                                <ext:TreeViewDragDrop Icon="Folder" EnableDrag="false" EnableDrop="true" Expanded="true"
                                    ID="leaveTeamsDragDrop" runat="server" ContainerScroll="true">
                                </ext:TreeViewDragDrop>
                            </Plugins>
                        </ext:TreeView>
                    </View>
                    <Listeners>
                        <NodeDragOver Fn="nodeDragOver" />
                    </Listeners>
                </ext:TreePanel>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>
