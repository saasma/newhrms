﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Web.CP.Report.Templates.HR;
using BLL.BO;
using System.IO;
using Utils;
using System.Data;
using Utils.Calendar;
using DevExpress.XtraPrinting;
using System.Security.AccessControl;
using Utils.Helper;
using System.Text;


namespace Web.NewHR
{
    public partial class TimesheetList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.ProjectAndTimesheet;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {

                CustomDate date = CustomDate.GetTodayDate(true).GetFirstDateOfThisMonth();
                CustomDate date2 = CustomDate.GetTodayDate(true).GetLastDateOfThisMonth();
                txtStartDate.Text = date.ToString();
                txtEndDate.Text = date2.ToString();

            }
            if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"].ToString().ToLower().Equals("reload"))
            {
                Export();
            }
           
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {

            int timesheetid = int.Parse(hdnTimeSheetId.Text);
            windowTimesheetDetails.Center();
            windowTimesheetDetails.Show();
            DAL.Timesheet timesheet = NewTimeSheetManager.GetTimeSheetById(timesheetid);
            if (timesheet == null)
            {
                NewMessage.ShowWarningMessage("Time sheet does not exists.");
                return;
            }
            else
            {
                windowTimesheetDetails.Title = "Timeheet of " +
                    new EmployeeManager().GetById(timesheet.EmployeeId).Name;
            }
            TimesheetCtl1.LoadTimesheet(timesheetid);
        }

        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetTimeSheetListForApprovalResult> list = JSON.Deserialize<List<GetTimeSheetListForApprovalResult>>(gridItemsJson);
            List<Timesheet> timesheetList = new List<Timesheet>();

            foreach (var item in list)
            {
                Timesheet obj = new Timesheet()
                {
                    EmployeeId = item.EmployeeId,
                    TimesheetId = item.TimesheetId
                };
                timesheetList.Add(obj);
            }

            if (list.Count == 0)
                return;

            Status status = NewTimeSheetManager.ApproveTimeSheet(timesheetList);
            if (status.IsSuccess)
            {
                //CheckboxSelectionModel1.ClearSelection();
                NewMessage.ShowNormalMessage( "Time sheets approved successfully.");
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage( status.ErrorMessage);
            }
        }

        private void ClearFields()
        {
            txtEmployeeName.Text = "";
            txtWeekNo.Text = "";
            txtWeekDate.Text = "";
            txtTotalHours.Text = "";
            txtNotes.Text = "";
        }

        protected void btnDeleteDraft_Click(object sender, DirectEventArgs e)
        {
            int timeSheetId = int.Parse(hdnTimeSheetId.Text);

            if (NewTimeSheetManager.DeleteDarftTimeSheet(timeSheetId))
            {
                NewMessage.ShowNormalMessage("Time sheet deleted successfully.");
                gridTimeSheet.GetStore().Reload();
              
            }
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            ClearFields();

            int timeSheetId = int.Parse(hdnTimeSheetId.Text);

            Timesheet obj = NewTimeSheetManager.GetTimeSheetById(timeSheetId);
            if (obj != null && NewTimeSheetManager.IsTimesheetViewableForManager(obj.TimesheetId))
            {
                txtEmployeeName.Text = new EmployeeManager().GetById(obj.EmployeeId).Name;
                if (!string.IsNullOrEmpty(hdnWeekNo.Text))
                    txtWeekNo.Text = hdnWeekNo.Text;
                if (obj.StartDate != null)
                    txtWeekDate.Text = NewTimeSheetManager.GetWeekRangeName(
                        obj.StartDate.Value, obj.EndDate.Value);

                if (obj.TotalHours != null)
                    txtTotalHours.Text = obj.TotalHours.Value.ToString();
                WTimeSheet.Show();               
            }
        }


        protected void btnRejectSave_Click(object sender, DirectEventArgs e)
        {
            int timeSheetId = int.Parse(hdnTimeSheetId.Text);

            Timesheet obj = new Timesheet()
            {
                TimesheetId = timeSheetId,
                Status = (int)TimeSheetStatus.Rejected,
                Notes = string.IsNullOrEmpty(txtNotes.Text.Trim()) ? null : txtNotes.Text.Trim()
            };

            Status status = NewTimeSheetManager.RejectTimeSheet(obj);
            if(status.IsSuccess)
            {
                WTimeSheet.Close();
                NewMessage.ShowNormalMessage( "Time sheet rejected successfully.");
                PagingToolbar1.DoRefresh();
            }
            else
            {
                NewMessage.ShowWarningMessage( status.ErrorMessage);
            }
        }


        protected void btnExcelPrint_Click(object sender, DirectEventArgs e)
        {

            //date validation--
            if (Convert.ToDateTime(txtStartDate.Text.Trim()).Month != Convert.ToDateTime(txtEndDate.Text.Trim()).Month)
            {
                NewMessage.ShowNormalMessage("Please select single month.");
                return;
            }

            //-----------------
            string strTimeSheetID = "";
            RowSelectionModel sm = this.gridTimeSheet.GetSelectionModel() as RowSelectionModel;
            foreach (SelectedRow row in sm.SelectedRows)
            {

                if (!string.IsNullOrEmpty(strTimeSheetID))
                    strTimeSheetID += "," + row.RecordID.Split(',').ToArray()[0].ToString();
                else
                    strTimeSheetID = row.RecordID.Split(',').ToArray()[0].ToString();
            }


            if (strTimeSheetID == "")
            {
                NewMessage.ShowNormalMessage("Please select Time Sheet to export");
                return;
            }

            if (int.Parse(strTimeSheetID) <= 0)
            {
                NewMessage.ShowNormalMessage("Not Filled Status can not be exported");
                return;
            }

                DateTime weekStart = Convert.ToDateTime(txtStartDate.Text.Trim());//Convert.ToDateTime(hdnStartDate.Text);
                weekStart = new DateTime(weekStart.Year, weekStart.Month, 1);

                DateTime monthEndDate = new DateTime(weekStart.Year, weekStart.Month,
                DateTime.DaysInMonth(weekStart.Year, weekStart.Month));

                DateTime week1 = weekStart;
                DateTime week2 = week1.AddDays(7);
                DateTime week3 = week2.AddDays(7);
                DateTime week4 = week3.AddDays(7);
                DateTime week5 = week4.AddDays(7);

                if (week4 < monthEndDate)
                {
                    week5 = week4.AddDays(7);
                }



                ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter adap =
                new ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter();
                //  ReportDataSet ds = new ReportDataSet();

                adap.Connection.ConnectionString = Config.ConnectionString;
                ReportDataSet.GetPAREmployeeHeaderDataTable tableEmployee = adap.GetData(strTimeSheetID);



                ReportDataSetTableAdapters.GetTimeAllocationInfoByWeekTableAdapter adap1 =
                    new ReportDataSetTableAdapters.GetTimeAllocationInfoByWeekTableAdapter();
                //  ReportDataSet ds1 = new ReportDataSet();


                adap.Connection.ConnectionString = Config.ConnectionString;
                ReportDataSet.GetTimeAllocationInfoByWeekDataTable tableTimeAllocation = adap1.GetData(week1, week2, week3, week4, week5, monthEndDate, strTimeSheetID);


                //store Employee Leave information

                ReportDataSetTableAdapters.ReportGetEmployeeLeaveListTableAdapter adap2 =
                    new ReportDataSetTableAdapters.ReportGetEmployeeLeaveListTableAdapter();
                // ReportDataSet ds2= new ReportDataSet();


                adap.Connection.ConnectionString = Config.ConnectionString;
                ReportDataSet.ReportGetEmployeeLeaveListDataTable tableLeaveInformation = adap2.GetData(week1, week2, week3, week4, week5, monthEndDate, strTimeSheetID);

                string[] arrayTimeSheetID = strTimeSheetID.Split(',').ToArray();
                string filePath = Server.MapPath(@"~/Uploads/PARExportFiles");
                foreach (string TimeSheetID in arrayTimeSheetID)
                {
                    if (int.Parse(TimeSheetID) > 0)
                    {

                        string filterExp = "TimeSheetID = '" + TimeSheetID + "'";

                        PAReport report = GetMainReport(tableEmployee, TimeSheetID);
                        report.DataSource = tableEmployee.Select(filterExp, "").CopyToDataTable();
                        report.DataMember = "Report";

                        CreateDirectory(filePath);

                        GenerateReport(tableTimeAllocation, tableLeaveInformation, tableEmployee, TimeSheetID);
                        XlsExportOptions opt = new XlsExportOptions();

                        Timesheet _Timesheet = NewTimeSheetManager.GetTimeSheetById(int.Parse(TimeSheetID));

                        string EmplolyeeName = EmployeeManager.GetEmployeeById(_Timesheet.EmployeeId).Name;
                        string MonthName = _Timesheet.StartDate.Value.Year + " - " +
                        DateHelper.GetMonthName(_Timesheet.StartDate.Value.Month, true);
                        string filename = EmplolyeeName + "-" + MonthName + ".xls";


                        report.ExportToXls(filePath + "/" + filename);
                    }

                }

                // ZipFile zipFile = new ZipFile(filePath + "/" + "Timesheet");

            }

        public void btnSendMail(object sender, DirectEventArgs e)
        {

            if (Convert.ToDateTime(txtStartDate.Text.Trim()).Month != Convert.ToDateTime(txtEndDate.Text.Trim()).Month)
            {
                NewMessage.ShowNormalMessage("Please select single month.");
                return;
            }

            string strEmployeeID = "";
            RowSelectionModel sm = this.gridTimeSheet.GetSelectionModel() as RowSelectionModel;


            foreach (SelectedRow row in sm.SelectedRows)
            {

                //timesheet id is zero for not filled status
                if (int.Parse(row.RecordID.Split(',').ToArray()[0])>0)
                {
                    NewMessage.ShowNormalMessage("Please send mail for not filled status only");
                    return;
                }

                if (!string.IsNullOrEmpty(strEmployeeID))
                    strEmployeeID += "," + row.RecordID.Split(',').ToArray()[1].ToString();
                
                else
                    strEmployeeID = row.RecordID.Split(',').ToArray()[1].ToString();
            }

            if (strEmployeeID == "")
            {
                NewMessage.ShowNormalMessage("Please select Time Sheet");
                return;
            }

            string[] arrayEmployeeID = strEmployeeID.Split(',').ToArray();
            SendMail(arrayEmployeeID);

        }


        public void btnSetApprove_Click(object sender, DirectEventArgs e)
        {

            string strTimeSheetID = "";
            RowSelectionModel sm = this.gridTimeSheet.GetSelectionModel() as RowSelectionModel;

            foreach (SelectedRow row in sm.SelectedRows)
            {

                int timesheetId = int.Parse(row.RecordID.Split(',').ToArray()[0]);

                Timesheet _Timesheet = NewTimeSheetManager.GetTimeSheetById(timesheetId);
                if (_Timesheet == null)
                {
                    NewMessage.ShowNormalMessage("You can only approve Timesheet with status Awaiting For Approval");
                    return;
                }

                if (_Timesheet.Status!= (int)TimeSheetStatus.AwaitingApproval)
                {
                    NewMessage.ShowNormalMessage("You can only approve Timesheet with status Awaiting For Approval");
                    return;
                }

                if (timesheetId > 0)
                {
                    if (!string.IsNullOrEmpty(strTimeSheetID))
                        strTimeSheetID += "," + timesheetId;
                    else
                        strTimeSheetID = timesheetId.ToString();
                }
            }

            if (strTimeSheetID == "")
            {
                NewMessage.ShowNormalMessage("Please select Time Sheet");
                return;
            }

            string[] arrayTimeSheetID = strTimeSheetID.Split(',').ToArray();
            ApproveTimeSheet(arrayTimeSheetID);

        }

        public void btnSetAsDraft(object sender, DirectEventArgs e)
        {
            string strEmployeeID = "";
            RowSelectionModel sm = this.gridTimeSheet.GetSelectionModel() as RowSelectionModel;
            foreach (SelectedRow row in sm.SelectedRows)
            {

                int timesheetId = int.Parse(row.RecordID.Split(',').ToArray()[0]);
                if (timesheetId > 0)
                {
                    if (!string.IsNullOrEmpty(strEmployeeID))
                        strEmployeeID += "," + timesheetId;
                    else
                        strEmployeeID = timesheetId.ToString();
                }
            }

            if (strEmployeeID == "")
            {
                NewMessage.ShowNormalMessage("Please select Time Sheet");
                return;
            }

            string[] arrayEmployeeID = strEmployeeID.Split(',').ToArray();
            ChangeStatus(arrayEmployeeID);

        }

        protected void SendMail(string[] EmployeeIDs)
        {

            SMTPHelper mailSender = new SMTPHelper();
            EmailContent dbMailContent = CommonManager.GetMailContent((int)EmailContentType.ProjectNotfill);
         
            string MonthName = Convert.ToDateTime(txtStartDate.Text).Year + " - " +
            DateHelper.GetMonthName(Convert.ToDateTime(txtStartDate.Text).Month, true);

            string subject = dbMailContent.Subject.Replace("#YearMonth#", MonthName);
        
            string body =dbMailContent.Body.Replace("#YearMonth#", MonthName);

            string to = "";
            foreach (string EmployeeID in EmployeeIDs)
            {

                //string filterExp = "TimeSheetID = '" + TimeSheetID + "'";

                //Timesheet _Timesheet = NewTimeSheetManager.GetTimeSheetById(int.Parse(TimeSheetID));
                EEmployee _Employeee = EmployeeManager.GetEmployeeById(int.Parse(EmployeeID));


                //   string StatusText = HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)_Timesheet.Status).ToString()) == null ?
                //   ((TimeSheetStatus)_Timesheet.Status).ToString() :
                //   HttpContext.GetGlobalResourceObject("FixedValues", ((TimeSheetStatus)_Timesheet.Status).ToString()).ToString();

                //   if (StatusText.ToLower() != "not filled")
                //   {
                //       NewMessage.ShowNormalMessage("please send mail for not filled status only");
                //       return;
                //   }

                if (string.IsNullOrEmpty(_Employeee.EAddresses[0].CIEmail))
                {
                    NewMessage.ShowNormalMessage("Email address not found for Employee " + _Employeee.Name ) ;
                    return;
                }

                if(!string.IsNullOrEmpty(to))
                to+="," +_Employeee.EAddresses[0].CIEmail;
                else
                    to = _Employeee.EAddresses[0].CIEmail;

            }


            string[] ToEmailIDs = to.Split(',');
            int count = 0;
            foreach (string ToEMailId in ToEmailIDs)
            {
                bool isSendSuccess = SMTPHelper.SendMail(ToEMailId, body, subject, "","");
                if (isSendSuccess)

                    //NewMessage.ShowNormalMessage(Resources.Messages.SentMailSuccessMsg);
                    count++;
            }

            if (count > 0)
            {
                NewMessage.ShowNormalMessage(string.Format(Resources.Messages.SentMailSuccessMsg, count));
                return;
            }
            else
            {
                NewMessage.ShowWarningMessage("error while sending email");
                return;
            }


        }

        protected void ApproveTimeSheet(string[] timesheetIDs)
        {
            int count = 0;
            foreach (string timesheetId in timesheetIDs)
            {

                if (SessionManager.User.UserMappedEmployeeId == null)
                {
                    NewMessage.ShowNormalMessage("Employee has not been mapped to this User");
                    return;
                }

                Timesheet timesheet = BLL.BaseBiz.PayrollDataContext.Timesheets.FirstOrDefault(
                    x => x.TimesheetId == int.Parse(timesheetId));
                if (timesheet != null)
                {
                    
                    timesheet.Status = (int)TimeSheetStatus.Approved;
                    timesheet.ApprovedBy = SessionManager.User.UserMappedEmployeeId;
                    //timesheet.ApprovalEmployeeId = SessionManager.User.UserMappedEmployeeId;
                    timesheet.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();
                    count+= 1;
                }
            }

            BLL.BaseBiz.PayrollDataContext.SubmitChanges();
            if (count > 0)
            {
                NewMessage.ShowNormalMessage((count) + " Timesheet has been approved successfully");
                X.Js.AddScript("searchList();");
            }

        }

        protected void ChangeStatus(string[] timesheetIDs)
        {
            string to = "";
            int count = 0;
            foreach (string timesheetId in timesheetIDs)
            {
                Timesheet timesheet =  BLL.BaseBiz.PayrollDataContext.Timesheets.FirstOrDefault(
                    x => x.TimesheetId == int.Parse(timesheetId));
                if (timesheet != null)
                {
                    timesheet.Status = (int)TimeSheetStatus.Draft;

                    count += 1;
                }
            }

            BLL.BaseBiz.PayrollDataContext.SubmitChanges();
            if (count > 0)
            {
                NewMessage.ShowNormalMessage("Status set to draft for " + (count - 1) + " timesheet.");

                X.Js.AddScript("searchList();");
            }
        }
            
       public static void CreateDirectory(string path) 
    {
        // Specify the directory you want to manipulate. 
     

        try 
        {
            // Determine whether the directory exists. 
            if (Directory.Exists(path))
            {

                return;
            }
            else
            {
                // Try to create the directory.
            //DirectorySecurity securityRules = new DirectorySecurity();
            //securityRules.AddAccessRule(new FileSystemAccessRule("", FileSystemRights.Read, AccessControlType.Allow));
            //securityRules.AddAccessRule(new FileSystemAccessRule("", FileSystemRights.FullControl, AccessControlType.Allow));
            DirectoryInfo di = Directory.CreateDirectory(path);

            }
        } 
        catch (Exception e) 
        {
            Console.WriteLine("The process failed: {0}", e.ToString());
        } 
        finally {}
    }


        protected void Export()
        {
            bool isPDF =  false;
            DAL.Timesheet timesheet = null;
            string rejectionNotes = "";
            DateTime weekStart = Convert.ToDateTime(hdnStartDate.Text);
            weekStart = new DateTime(weekStart.Year, weekStart.Month, 1);

            TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(weekStart,
               int.Parse(hdnEmployeeID.Value.ToString()), ref rejectionNotes, ref timesheet);

            if (timesheet == null)
            {
                NewMessage.ShowNormalMessage("Timesheet not saved.");
                return;
            }

            PAReport report = GetPAR(timesheet.TimesheetId.ToString());
            GetSubReportPAR();
            

            using (MemoryStream stream = new MemoryStream())
            {
                if (isPDF)
                    report.ExportToPdf(stream);
                else
                    report.ExportToXls(stream);

                HttpContext.Current.Response.Clear();
                if (isPDF)
                {

                    HttpContext.Current.Response.ContentType = "application/pdf";
                    HttpContext.Current.Response.AppendHeader("Content-Type", "application/pdf");

                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + report.labelTitle.Text + " Statement.pdf\"");

                }
                else
                {
                    
                    report.ExportToXls(stream);
                    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                    HttpContext.Current.Response.AppendHeader("Content-Type", "application/vnd.ms-excel");

                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + report.labelTitle.Text + " Statement.xls\"");
                }

                HttpContext.Current.Response.AddHeader("Content-Length", stream.ToArray().Length.ToString());

                HttpContext.Current.Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length);
                HttpContext.Current.Response.End();
            }
        }



        private PAReport GetMainReport( ReportDataSet.GetPAREmployeeHeaderDataTable EmployeeInfoTable ,string TimeSheetID)
        {

            //List<GetPAREmployeeHeaderResult> data = EmployeeManager.GetPAREmployeeHeader(hdnEmployeeID.Value.ToString()).ToList();

            //ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter adap =
            //    new ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter();
            //ReportDataSet ds = new ReportDataSet();

            //adap.Connection.ConnectionString = Config.ConnectionString;
            //ReportDataSet.GetPAREmployeeHeaderDataTable table = adap.GetData(TimeSheetID);


            // Process for report
               string filterExp = "TimeSheetID = '" + TimeSheetID + "'";
            PAReport report = new PAReport();
            report.DataSource = EmployeeInfoTable.Select(filterExp);
            report.DataMember = "Report";
            //            report.labelTitle.Text = "";



            return report;
        }

        private PAReport GetPAR(string TimeSheetID)
        {

            //List<GetPAREmployeeHeaderResult> data = EmployeeManager.GetPAREmployeeHeader(hdnEmployeeID.Value.ToString()).ToList();

            ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter adap =
                new ReportDataSetTableAdapters.GetPAREmployeeHeaderTableAdapter();
            ReportDataSet ds = new ReportDataSet();

            adap.Connection.ConnectionString = Config.ConnectionString;
            ReportDataSet.GetPAREmployeeHeaderDataTable table = adap.GetData(TimeSheetID);


            // Process for report
            PAReport report = new PAReport();
            report.DataSource = table;
            report.DataMember = "Report";
            //            report.labelTitle.Text = "";



            return report;
        }


        private void GenerateReport(ReportDataSet.GetTimeAllocationInfoByWeekDataTable TimeInfoTable, ReportDataSet.ReportGetEmployeeLeaveListDataTable LeaveInfoTable, ReportDataSet.GetPAREmployeeHeaderDataTable EmployeeInfoTable, string TimeSheetID)
        {


            // Process for report

            string filterExp = "TimeSheetID = '" + TimeSheetID + "'";

            //PAReport report = new PAReport();
            //report.DataSource = EmployeeInfoTable.Select(filterExp);

            HttpContext.Current.Items["PARTimeAllocationInfo"] = TimeInfoTable.Select(filterExp).CopyToDataTable();

            //store Employee Leave information

            HttpContext.Current.Items["PAREmployeeLeaveInfo"] = LeaveInfoTable.Select(filterExp).CopyToDataTable();

        }


        private void GetSubReportPAR()
        {



            DateTime weekStart = Convert.ToDateTime(hdnStartDate.Text);
            weekStart = new DateTime(weekStart.Year, weekStart.Month, 1);


            DAL.Timesheet timesheet = null;
            string rejectionNotes = "";
            TimeSheetStatus status = NewTimeSheetManager.GetTimesheetStatus(weekStart,
                int.Parse(hdnEmployeeID.Value.ToString()), ref rejectionNotes, ref timesheet);


            DateTime monthEndDate = new DateTime(weekStart.Year, weekStart.Month,
            DateTime.DaysInMonth(weekStart.Year, weekStart.Month));

            DateTime week1 = weekStart;
            DateTime week2 = week1.AddDays(7);
            DateTime week3 = week2.AddDays(7);
            DateTime week4 = week3.AddDays(7);
            DateTime week5 = week4.AddDays(7);

            if (week4 < monthEndDate)
            {
                week5 = week4.AddDays(7);
            }

            //  List<GetTimeAllocationInfoByWeekResult> data = EmployeeManager.ReportGetTimeAllocationInfoByWeek(this.ReadonlyEmployeeId == 0 ? this.EmployeeId : this.ReadonlyEmployeeId,week1,week2,week3,week4,week5);

            ReportDataSetTableAdapters.GetTimeAllocationInfoByWeekTableAdapter adap =
                new ReportDataSetTableAdapters.GetTimeAllocationInfoByWeekTableAdapter();
            ReportDataSet ds = new ReportDataSet();


            adap.Connection.ConnectionString = Config.ConnectionString;
            ReportDataSet.GetTimeAllocationInfoByWeekDataTable table = adap.GetData(week1, week2, week3, week4, week5, monthEndDate, timesheet.TimesheetId.ToString());
            HttpContext.Current.Items["PARTimeAllocationInfo"] = table;

            //store Employee Leave information

            ReportDataSetTableAdapters.ReportGetEmployeeLeaveListTableAdapter adap1 =
                new ReportDataSetTableAdapters.ReportGetEmployeeLeaveListTableAdapter();
            ReportDataSet ds1 = new ReportDataSet();


            adap.Connection.ConnectionString = Config.ConnectionString;
            ReportDataSet.ReportGetEmployeeLeaveListDataTable table1 = adap1.GetData( week1, week2, week3, week4, week5, monthEndDate, timesheet.TimesheetId.ToString());
            HttpContext.Current.Items["PAREmployeeLeaveInfo"] = table1;

            //HttpContext.Current.Items["MonthCovered"] = DateTime.Parse(hdnStartDate.Text.Trim()).Year + " - " +
            //       DateHelper.GetMonthName(DateTime.Parse(hdnStartDate.Text.Trim()).Month, true);

            // Declare an object variable. 
         



            

            //-----------------------------



        }


    }
}