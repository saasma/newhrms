﻿<%@ Page Title="Training Administration" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="TrainingAdministration.aspx.cs" Inherits="Web.NewHR.TrainingMod.TrainingAdministration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        .innerLR
        {
            padding: 0px;
        }
        .blockBottomBorder
        {
            border-bottom: 4px solid #EAF3FA;
            padding-bottom: 1px;
            width: 350px;
        }
        .blockTitle
        {
            font-size: 14px;
            padding-left: 2px;
            margin-bottom: 5px;
        }
        .blockTraining
        {
            width: 350px;
            background-color: #EAF3FA;
            margin-top: 5px;
        }
        
       
        
        .textCls
        {
            color: #47759E;
            padding-top: 3px;
            font-weight: bold;
        }
        .txtColor
        {
            color: #47759E;
        }
        .bodyWhite
        {
            background-color: White;
        }
        
        .fontRed
        {
            color: Red;
        }
        .blockBottomBorder1
        {
            border-bottom: 4px solid #EAF3FA;
            width: 1287px;
        }
        
        .pending-row
        {
            color: #A8552C;
        }
        .approved-row
        {
            color: #308530;
        }
        .houseful-row
        {
            color: #5B5C58;
        }
        .waiting-row
        {
            color: #003399;
        }
        .clsCursorHand
        {
            cursor: pointer;
        }
        
        .commentCls td
        {
            background-color: #EFEFEF !important;
        }
        
        .total-field .x-form-display-field
        {
            font-style: italic;
            font-weight: bold !important;
            background: #E8F1FF !important;
            color: #428BCA;
            padding: 0px 0px 0px 5px;
        }
        #tblBookedInfo td
        {
            padding: 5px;
            color: White;
        }
        
         .blockTrainingName
        {
            width: 350px;
            background-color: #EAF3FA;
            margin-top: 5px;
            padding: 5px 5px 5px 0px;
            border-bottom: 1px solid #428BCA;
        }
        
    </style>
    <script type="text/javascript">

    function DetailsClick(TrainingId)
    {                 
        <%= hdnTrainingId.ClientID %>.setValue(TrainingId);            
        <%= btnTrainngDetails.ClientID %>.fireEvent('click');
    }

     var CommandHandler = function(command, record){
        <%= hdnTrainingRequestId.ClientID %>.setValue(record.data.TrainingRequestId);
        <%= hdnTrainingId.ClientID %>.setValue(record.data.TrainingID);
            
            if(command=="Approve")
            {
                <%= btnApprove.ClientID%>.fireEvent('click');
            }
            else if(command=="WaitingList")
            {
                <%= btnWaiting.ClientID%>.fireEvent('click');
            }
            else if(command=='HouseFull')
            {
                <%= btnHouseFull.ClientID%>.fireEvent('click');
            }
          else if(command=='View')
            {
              DetailsClick(record.data.TrainingID);
            }
         
              
      };
      var IsTrainingRender = function (value, metaData, record, rowIndex, colIndex, store) {

            var Name = "";
    if (typeof (value) == 'undefined' || value == '')
       Name = "No";

    
    if (value == false)
        Name = "No";
    else if (value == "1")
        Name = "Yes";

    // setTotal();

    return Name;
}

      var getRowClassTrainingLn = function (record) {
            if (record.data.IsTraining==false) {
                return "commentCls";
            }
            else{
                return "new-row";
            }
        };

      var template = '<span style="color:{0};">{1}</span>';

        var change1 = function (value) {
            return Ext.String.format(template, "blue", value);
        };
        
        var change2 = function(value){
            return Ext.String.format(template, "#1DC36B", value);
        };

        var getRowClass = function (record) {
                if (record.data.Status == 1) {
                    return "pending-row";
                }                
                else if (record.data.Status == 2) {
                    return "approved-row";
                }
                else if (record.data.Status == 3) {
                    return "houseful-row";
                }
                else{
                    return "waiting-row";
                }
            };


             var CommandHandlerEmployeeGrid = function(command, record) {
            if(command=='deleterow')
            {
                <%= hdnSelectedEmployee.ClientID %>.setValue(record.data.EmployeeId);
                <%= lnkRemoveRow.ClientID %>.fireEvent('click');
            }
             };

        var showSelectedTraining = function()
        {
            var trainingid = <%= cmbTraining.ClientID %>.getValue();

            if(trainingid == null || trainingid == "")
                return;


            DetailsClick(trainingid);
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnTrainingId" runat="server" />
    <ext:Hidden ID="hdnTrainingRequestId" runat="server" />
    <ext:Hidden ID="hdnSelectedEmployee" runat="server" />
    <ext:LinkButton runat="server" ID="lnkRemoveRow" Hidden="true">
        <DirectEvents>
            <Click OnEvent="lnkRemoveRow_Click">
                <EventMask ShowMask="true" />
                <ExtraParams>
                    <ext:Parameter Name="gridItems" Value="Ext.encode(#{GridEmployee}.getRowsValues({selectedOnly:false}))"
                        Mode="Raw" />
                </ExtraParams>
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnTrainngDetails" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnTrainngDetails_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnApprove" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnApprove_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve training request?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnHouseFull" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnHouseFull_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save training request as house full?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnWaiting" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnWaiting_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save training request as waiting?" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Training Administration
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <%-- <h4 class="heading">
        <div class="blockBottomBorder1">
            Training Administration</div></h4>--%>
            <div style="padding-bottom: 15px">
                <table>
                </table>
            </div>
            <table>
                <tr>
                    <td valign="top" style="padding-bottom: 10px">
                        <ext:ComboBox ID="cmbTraining" ForceSelection="true" StyleSpec="float:left" runat="server" Width="250" FieldLabel="" LabelAlign="Top"
                            EmptyText="training filter" LabelSeparator="" QueryMode="Local" DisplayField="Name"
                            ValueField="TrainingID">
                            <Store>
                                <ext:Store ID="store9" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="TrainingID" />
                                        <ext:ModelField Name="Name" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Change OnEvent="cmbTraining_Change">
                                </Change>
                            </DirectEvents>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();document.getElementById('anchorView').style.display = '';" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();
                                    var q = queryEvent.query;if(q=='') return;
                                      queryEvent.query = new RegExp('\\b' + q, 'i');
                                      queryEvent.query.length = q.length;" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue();#{storeTRParticipation}.reload(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                        </ext:ComboBox>
                          <span style="padding-left: 15px;float:left;padding-top: 4px;"><a id="anchorView" style="display:none" href="javascript:void(0)" onclick="showSelectedTraining();">View
                            </a></span>
                    </td>
                 
                </tr>
                <tr>
                    <td valign="top" style="width: 8%">
                        <div class="">
                            <span class="blockTitle" style="color: #47759E;">Training Participation Requests</span>
                        </div>
                        <ext:DisplayField ID="dispTrainingReqPart" runat="server" Hidden="true" />
                        <ext:Store ID="storeTRParticipation" runat="server" PageSize="20" OnReadData="BindTrainingReqOnRead">
                            <Model>
                                <ext:Model ID="model10" runat="server" IDProperty="TrainingRequestId">
                                    <Fields>
                                        <ext:ModelField Name="TrainingID" Type="String" />
                                        <ext:ModelField Name="EmployeeName" Type="String" />
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="Branch" Type="String" />
                                        <ext:ModelField Name="Department" Type="String" />
                                        <ext:ModelField Name="TrainingType" Type="String" />
                                        <ext:ModelField Name="ApprovedByName" Type="String" />
                                        <ext:ModelField Name="Status" Type="Int" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:GridPanel ID="gridTrainingRequestPart" StoreID="storeTRParticipation" Header="false"
                            StyleSpec="margin-top:5px" Margins="False" runat="server" MinHeight="0" Width="700">
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column44" Width="150" runat="server" DataIndex="EmployeeName" MenuDisabled="true"
                                        Text="Employee" Align="Left">
                                    </ext:Column>
                                    <ext:Column ID="Column39" Width="150" runat="server" DataIndex="Name" MenuDisabled="true"
                                        Flex="1" Text="Training" Align="Left">
                                    </ext:Column>
                                    <ext:Column ID="Column40" Width="100" runat="server" DataIndex="Branch" MenuDisabled="true"
                                        Text="Branch" Align="Left">
                                    </ext:Column>
                                    <ext:Column ID="Column41" Width="100" runat="server" DataIndex="Department" MenuDisabled="true"
                                        Text="Department" Align="Left">
                                    </ext:Column>
                                    <ext:Column ID="Column42" Width="65" runat="server" DataIndex="TrainingType" MenuDisabled="true"
                                        Hidden="true" Align="Left">
                                    </ext:Column>
                                    <ext:Column ID="Column43" Width="90" runat="server" DataIndex="ApprovedByName" MenuDisabled="true"
                                        Align="Left">
                                    </ext:Column>
                                    <ext:CommandColumn ID="cmdColumn" runat="server" Width="50" MenuDisabled="true" Sortable="false">
                                        <Commands>
                                            <ext:GridCommand Icon="ApplicationFormEdit" ToolTip-Text="Action">
                                                <Menu EnableScrolling="false">
                                                    <Items>
                                                        <ext:MenuCommand Text="Approve" CommandName="Approve" />
                                                        <ext:MenuCommand Text="House Full" CommandName="HouseFull" />
                                                        <ext:MenuCommand Text="Waiting List" CommandName="WaitingList" />

                                                    </Items>
                                                </Menu>
                                            </ext:GridCommand>
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandHandler(command, record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <View>
                                <ext:GridView ID="GridView1" runat="server">
                                    <GetRowClass Fn="getRowClass" />
                                </ext:GridView>
                            </View>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel8" runat="server" Mode="Single" />
                            </SelectionModel>
                            <BottomBar>
                                <ext:PagingToolbar ID="paging1" runat="server">
                                </ext:PagingToolbar>
                            </BottomBar>
                        </ext:GridPanel>
                    </td>
                    <td valign="top" style="width: 14%;">
                        <div class="">
                            <span class="blockTitle" style="color: #47759E;">Available Trainings</span>
                        </div>
                        <div class="blockTraining">
                            <ext:DisplayField ID="dispThisMonthTitle" FieldCls="textCls" runat="server" />
                        </div>
                        <%-- <ext:DisplayField ID="dispThisMonthDetails" runat="server" />--%>
                        <asp:Repeater runat="server" ID="rptTrainingList">
                            <ItemTemplate>
                                <li style="list-style: none; padding-bottom: 10px">
                                    <div class="blockTrainingName ">
                                        <span style="width: 150px;">
                                            <asp:HyperLink Width="150px" ID="Label2" runat="server" NavigateUrl='' Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                                            <%--(<%# Eval("StatusName")%>) --%>
                                        </span>
                                    </div>
                                    <div style="background-color: #f5f5f5; margin-top: 1px; width: 350px">
                                        <br />
                                        <span style="font-style: italic; padding-left: 4px">
                                            <%# Eval("DateInfo")%>
                                        </span>
                                        <br />
                                        <span style="font-style: italic; padding-left: 4px; color: #808080">
                                            <%# Eval("Host")%>
                                        </span>
                                        <br />
                                        <span>
                                            <%--    <%# Eval("BookingInfo")%>--%>
                                            <table id="tblBookedInfo">
                                                <tr>
                                                    <td style="background-color: #808080; width: 70px">
                                                        Seats
                                                    </td>
                                                    <td style="background-color: #DADADA; width: 20px; color: Black">
                                                        <%# Eval("Seats")%>
                                                    </td>
                                                    <td>
                                                        &nbsp
                                                    </td>
                                                    <td style="background-color: #5B9BD5; width: 70px">
                                                        Booked
                                                    </td>
                                                    <td style="background-color: #BED7EE; width: 20px; color: Black">
                                                        <%# Eval("Booked")%>
                                                    </td>
                                                    <td>
                                                        &nbsp
                                                    </td>
                                                    <td style="background-color: #538334; width: 70px">
                                                        Avilable
                                                    </td>
                                                    <td style="background-color: #C6E0B4; width: 20px; color: Black">
                                                        <%# Eval("Avilable")%>
                                                    </td>
                                                    <td>
                                                        <%# Eval("ViewLink")%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%-- <div style="background-color:grey;width:20px">Seats</div><div style="background-color:Green;width:20px">12</div>  <div style="background-color:Green;width:20px">Booked</div>
                                            <div style="background-color:Green;width:20px">8</div>   <div style="background-color:Green;width:20px">Avilable</div><div style="background-color:Green;width:20px">4</div>--%>
                                        </span>
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <span style="padding-left: 5px"><a href="TrainingEmployee.aspx?filter=thismonth">view
                            all</a></span>
                        
                        <br />
                        <div class="blockTraining" style="display: none">
                            <ext:DisplayField ID="dispNextMonthTitle" runat="server" FieldCls="textCls" />
                        </div>
                        <ext:DisplayField ID="dispNextMonthDetails" runat="server" Hidden="true">
                        </ext:DisplayField>
                        <br />
                        <div class="blockTraining" style="display: none">
                            <ext:DisplayField ID="dispNearFuture" Text="Near Future" runat="server" FieldCls="textCls"
                                Hidden="true" />
                        </div>
                        <ext:DisplayField ID="dispNearFutureDetails" runat="server" Hidden="true" />
                    </td>
                    <%-- <td valign="top" style="width: 400px;">
                        <div class="blockBottomBorder">
                            <span class="blockTitle" style="color: #47759E;">Additional Training Requests</span>
                        </div>
                        <br />
                    </td>--%>
                </tr>
            </table>
            <br />
            <br />
            <ext:TabPanel ID="TabPanel2" runat="server" MinHeight="200" Width="1155">
                <Items>
                    <ext:Panel ID="Panel5" runat="server" Title="Waiting">
                        <Content>
                            <br />
                            <ext:GridPanel ID="gridTRWaiting" runat="server" MinHeight="0" Width="1155">
                                <Store>
                                    <ext:Store ID="store6" runat="server" OnReadData="BindTRWainingOnRead" PageSize="20">
                                        <Model>
                                            <ext:Model ID="model6" runat="server" IDProperty="TrainingRequestId">
                                                <Fields>
                                                    <ext:ModelField Name="TrainingID" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="EmployeeName" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                    <ext:ModelField Name="Branch" Type="String" />
                                                    <ext:ModelField Name="Department" Type="String" />
                                                    <ext:ModelField Name="ApprovedByName" Type="String" />
                                                    <ext:ModelField Name="ApprovedOn" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Column19" Width="300" runat="server" Text="Training Name" DataIndex="Name"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column20" Width="185" runat="server" Text="Employee Name" DataIndex="EmployeeName"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column21" Width="60" runat="server" Text="EID" DataIndex="EmployeeId"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                        </ext:Column>
                                        <ext:Column ID="Column22" Width="130" runat="server" Text="Branch" DataIndex="Branch"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column23" Width="130" runat="server" Text="Department" DataIndex="Department"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column36" Width="180" runat="server" Text="Approved By" DataIndex="ApprovedByName"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:DateColumn ID="DateColumn2" Width="90" runat="server" Text="Approved On" DataIndex="ApprovedOn"
                                            Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                        </ext:DateColumn>
                                        <ext:CommandColumn ID="CommandColumn5" runat="server" Width="80" MenuDisabled="true"
                                            Sortable="false">
                                            <Commands>
                                                <ext:GridCommand Text="Change">
                                                    <Menu EnableScrolling="false">
                                                        <Items>
                                                            <ext:MenuCommand Text="Approve" CommandName="Approve" />
                                                            <ext:MenuCommand Text="House Full" CommandName="HouseFull" />
                                                            <ext:MenuCommand Text="Waiting List" CommandName="WaitingList" />
                                                        </Items>
                                                    </Menu>
                                                </ext:GridCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command, record);" />
                                            </Listeners>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel5" runat="server" Mode="Single" />
                                </SelectionModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar3" runat="server">
                                    </ext:PagingToolbar>
                                </BottomBar>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel3" runat="server" Title="Approved">
                        <Content>
                            <br />
                            <ext:GridPanel ID="gridTRApproved" runat="server" MinHeight="0" Width="1155">
                                <Store>
                                    <ext:Store ID="storeTRApproved" runat="server" OnReadData="BindTRApprovedOnReadData"
                                        PageSize="20">
                                        <Model>
                                            <ext:Model ID="modelTRApproved" runat="server" IDProperty="TrainingRequestId">
                                                <Fields>
                                                    <ext:ModelField Name="TrainingID" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="EmployeeName" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                    <ext:ModelField Name="Branch" Type="String" />
                                                    <ext:ModelField Name="Department" Type="String" />
                                                    <ext:ModelField Name="ApprovedByName" Type="String" />
                                                    <ext:ModelField Name="ApprovedOn" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Column9" Width="300" runat="server" Text="Training Name" DataIndex="Name"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column13" Width="185" runat="server" Text="Employee Name" DataIndex="EmployeeName"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column14" Width="60" runat="server" Text="EID" DataIndex="EmployeeId"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                        </ext:Column>
                                        <ext:Column ID="Column15" Width="130" runat="server" Text="Branch" DataIndex="Branch"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column16" Width="130" runat="server" Text="Department" DataIndex="Department"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column34" Width="180" runat="server" Text="Approved By" DataIndex="ApprovedByName"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:DateColumn ID="colFromDate" Width="90" runat="server" Text="Approved On" DataIndex="ApprovedOn"
                                            Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                        </ext:DateColumn>
                                        <ext:CommandColumn ID="CommandColumn2" runat="server" Width="80" MenuDisabled="true"
                                            Sortable="false">
                                            <Commands>
                                                <ext:GridCommand Text="Change">
                                                    <Menu EnableScrolling="false">
                                                        <Items>
                                                            <ext:MenuCommand Text="Approve" CommandName="Approve" />
                                                            <ext:MenuCommand Text="House Full" CommandName="HouseFull" />
                                                            <ext:MenuCommand Text="Waiting List" CommandName="WaitingList" />
                                                        </Items>
                                                    </Menu>
                                                </ext:GridCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command, record);" />
                                            </Listeners>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                                </SelectionModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar1" runat="server">
                                    </ext:PagingToolbar>
                                </BottomBar>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel4" runat="server" Title="House Full">
                        <Content>
                            <br />
                            <ext:GridPanel ID="gridTRHouseFull" runat="server" MinHeight="0" Width="1155">
                                <Store>
                                    <ext:Store ID="store5" runat="server" OnReadData="BindTRHouseFulOnRead" PageSize="20">
                                        <Model>
                                            <ext:Model ID="model5" runat="server" IDProperty="TrainingRequestId">
                                                <Fields>
                                                    <ext:ModelField Name="TrainingID" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="EmployeeName" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                    <ext:ModelField Name="Branch" Type="String" />
                                                    <ext:ModelField Name="Department" Type="String" />
                                                    <ext:ModelField Name="ApprovedByName" Type="String" />
                                                    <ext:ModelField Name="ApprovedOn" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Column10" Width="300" runat="server" Text="Training Name" DataIndex="Name"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column11" Width="185" runat="server" Text="Employee Name" DataIndex="EmployeeName"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column12" Width="60" runat="server" Text="EID" DataIndex="EmployeeId"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                        </ext:Column>
                                        <ext:Column ID="Column17" Width="130" runat="server" Text="Branch" DataIndex="Branch"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column18" Width="130" runat="server" Text="Department" DataIndex="Department"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column35" Width="180" runat="server" Text="Approved By" DataIndex="ApprovedByName"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:DateColumn ID="DateColumn1" Width="90" runat="server" Text="Approved On" DataIndex="ApprovedOn"
                                            Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                        </ext:DateColumn>
                                        <ext:CommandColumn ID="CommandColumn4" runat="server" Width="80" MenuDisabled="true"
                                            Sortable="false">
                                            <Commands>
                                                <ext:GridCommand Text="Change">
                                                    <Menu EnableScrolling="false">
                                                        <Items>
                                                            <ext:MenuCommand Text="Approve" CommandName="Approve" />
                                                            <ext:MenuCommand Text="House Full" CommandName="HouseFull" />
                                                            <ext:MenuCommand Text="Waiting List" CommandName="WaitingList" />
                                                        </Items>
                                                    </Menu>
                                                </ext:GridCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command, record);" />
                                            </Listeners>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel4" runat="server" Mode="Single" />
                                </SelectionModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar2" runat="server">
                                    </ext:PagingToolbar>
                                </BottomBar>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel6" runat="server" Title="History">
                        <Content>
                            <br />
                            <table>
                                <tr>
                                    <td style="width: 140px;">
                                        <ext:DateField ID="txtDateFilter" runat="server" Width="120">
                                        </ext:DateField>
                                    </td>
                                    <td>
                                        <ext:Button ID="btnLoad" runat="server" Cls="btn btn-primary" Text="Load">
                                            <DirectEvents>
                                                <Click OnEvent="btnLoad_Click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                </tr>
                            </table>
                            </br>
                            <ext:GridPanel ID="gridTRHistory" runat="server" MinHeight="0" Width="1155">
                                <Store>
                                    <ext:Store ID="store7" runat="server" OnReadData="BindTRHistoryOnRead" PageSize="20">
                                        <Model>
                                            <ext:Model ID="model7" runat="server" IDProperty="TrainingRequestId">
                                                <Fields>
                                                    <ext:ModelField Name="TrainingID" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="EmployeeName" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                    <ext:ModelField Name="Branch" Type="String" />
                                                    <ext:ModelField Name="Department" Type="String" />
                                                    <ext:ModelField Name="ApprovedByName" Type="String" />
                                                    <ext:ModelField Name="ApprovedOn" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Column24" Width="300" runat="server" Text="Training Name" DataIndex="Name"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column25" Width="185" runat="server" Text="Employee Name" DataIndex="EmployeeName"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column26" Width="60" runat="server" Text="EID" DataIndex="EmployeeId"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                        </ext:Column>
                                        <ext:Column ID="Column27" Width="130" runat="server" Text="Branch" DataIndex="Branch"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column28" Width="130" runat="server" Text="Department" DataIndex="Department"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column37" Width="180" runat="server" Text="Approved By" DataIndex="ApprovedByName"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:DateColumn ID="DateColumn3" Width="90" runat="server" Text="Approved On" DataIndex="ApprovedOn"
                                            Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                        </ext:DateColumn>
                                        <ext:CommandColumn ID="CommandColumn6" runat="server" Width="80" MenuDisabled="true"
                                            Sortable="false">
                                            <Commands>
                                                <ext:GridCommand Text="Change">
                                                    <Menu EnableScrolling="false">
                                                        <Items>
                                                            <ext:MenuCommand Text="Approve" CommandName="Approve" />
                                                            <ext:MenuCommand Text="House Full" CommandName="HouseFull" />
                                                            <ext:MenuCommand Text="Waiting List" CommandName="WaitingList" />
                                                        </Items>
                                                    </Menu>
                                                </ext:GridCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command, record);" />
                                            </Listeners>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel6" runat="server" Mode="Single" />
                                </SelectionModel>
                                <BottomBar>
                                    <ext:PagingToolbar ID="PagingToolbar4" runat="server">
                                    </ext:PagingToolbar>
                                </BottomBar>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                    <ext:Panel ID="Panel7" runat="server" Title="Additional Requests">
                        <Content>
                            <br />
                            <ext:GridPanel ID="gridAdditionalRequests" runat="server" MinHeight="0" Width="1155">
                                <Store>
                                    <ext:Store ID="store8" runat="server">
                                        <Model>
                                            <ext:Model ID="model8" runat="server" IDProperty="TrainingRequestId">
                                                <Fields>
                                                    <ext:ModelField Name="TrainingID" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                    <ext:ModelField Name="EmployeeName" Type="String" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                    <ext:ModelField Name="Branch" Type="String" />
                                                    <ext:ModelField Name="Department" Type="String" />
                                                    <ext:ModelField Name="ApprovedByName" Type="String" />
                                                    <ext:ModelField Name="ApprovedOn" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Column29" Width="300" runat="server" Text="Training Name" DataIndex="Name"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column30" Width="185" runat="server" Text="Employee Name" DataIndex="EmployeeName"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column31" Width="60" runat="server" Text="EID" DataIndex="EmployeeId"
                                            MenuDisabled="true" Sortable="false" Align="Center">
                                        </ext:Column>
                                        <ext:Column ID="Column32" Width="130" runat="server" Text="Branch" DataIndex="Branch"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column33" Width="130" runat="server" Text="Department" DataIndex="Department"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:Column ID="Column38" Width="180" runat="server" Text="Approved By" DataIndex="ApprovedByName"
                                            MenuDisabled="true" Sortable="false" Align="Left">
                                        </ext:Column>
                                        <ext:DateColumn ID="DateColumn4" Width="90" runat="server" Text="Approved On" DataIndex="ApprovedOn"
                                            Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                        </ext:DateColumn>
                                        <ext:CommandColumn ID="CommandColumn7" runat="server" Width="80" MenuDisabled="true"
                                            Sortable="false">
                                            <Commands>
                                                <ext:GridCommand Text="Change">
                                                    <Menu EnableScrolling="false">
                                                        <Items>
                                                            <ext:MenuCommand Text="Approve" CommandName="Approve" />
                                                            <ext:MenuCommand Text="House Full" CommandName="HouseFull" />
                                                            <ext:MenuCommand Text="Waiting List" CommandName="WaitingList" />
                                                        </Items>
                                                    </Menu>
                                                </ext:GridCommand>
                                            </Commands>
                                            <Listeners>
                                                <Command Handler="CommandHandler(command, record);" />
                                            </Listeners>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel7" runat="server" Mode="Single" />
                                </SelectionModel>
                            </ext:GridPanel>
                        </Content>
                    </ext:Panel>
                </Items>
            </ext:TabPanel>
        </div>
    </div>
    <ext:Window ID="windowTPR" runat="server" Title="Training Participation Request"
        ButtonAlign="Left" AutoScroll="true" Icon="None" Height="650" Width="900" BodyPadding="5"
        Hidden="true" Modal="false">
        <Content>
            <div>
                <table class="fieldTable" border="0">
                    <tr>
                        <td valign="top">
                            <ext:DisplayField ID="dispTrainingName" runat="server" Style="color: #47759E; font-size: 15px;
                                font-weight: bold;" />
                            <ext:DisplayField ID="dispTrainingDetails" runat="server" Style="color: #47759E;">
                            </ext:DisplayField>
                            <div style="    margin-top: 10px;">
                              Venue
                            <ext:DisplayField ID="dispVenue" Text="" runat="server" Style="color: #47759E;" /></div>
                        </td>
                        <td style="width: 135px;">
                            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridTotalSeats" runat="server" Title="Total Seats"
                                TitleAlign="Center" Width="130" Cls="itemgrid">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model4" runat="server" IDProperty="TrainingID">
                                                <Fields>
                                                    <ext:ModelField Name="AvailableSeats" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="colID" Sortable="false" MenuDisabled="true" runat="server" HideTitleEl="true"
                                            Align="Center" Width="100" DataIndex="AvailableSeats" />
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </td>
                        <td style="width: 135px;">
                            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridSeatsBooked" runat="server" Title="Seats Booked"
                                TitleAlign="Center" Width="130" Cls="itemgrid">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server" IDProperty="TrainingID">
                                                <Fields>
                                                    <ext:ModelField Name="BookedSeats" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" HideTitleEl="true"
                                            Align="Center" Width="100" DataIndex="BookedSeats">
                                            <Renderer Fn="change1" />
                                        </ext:Column>
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </td>
                        <td style="width: 150px;">
                            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridAvailabelSeats" runat="server"
                                Title="Available Seats" TitleAlign="Center" Width="130" Cls="itemgrid">
                                <Store>
                                    <ext:Store ID="Store3" runat="server">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server" IDProperty="TrainingID">
                                                <Fields>
                                                    <ext:ModelField Name="LeftSeats" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" HideTitleEl="true"
                                            Align="Center" Width="100" DataIndex="LeftSeats">
                                            <Renderer Fn="change2" />
                                        </ext:Column>
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 40px;">
                            Hosted by
                            <ext:DisplayField ID="dispHostedBy" runat="server" Style="color: #47759E;" />
                        </td>
                        <td style="padding-bottom: 17px;">
                            <ext:Button runat="server" Cls="btn btn-primary" Width="130" StyleSpec="margin-top:10px"
                                ID="btnAllocateSeats" Text="<i></i>Allocate Seats">
                                <DirectEvents>
                                    <Click OnEvent="btnAllocateSeats_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                        <td style="padding-bottom: 17px; padding-left: 10px;" colspan="2">
                            <ext:Button runat="server" StyleSpec="margin-top:10px" ID="btnInviteEmp" Cls="btn btn-primary"
                                Width="130" Text="<i></i>Invite Employee">
                                <DirectEvents>
                                    <Click OnEvent="btnInviteEmp_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 40px;" colspan="4">
                            Resource Person:
                            <ext:DisplayField ID="dispResourcePerson" runat="server" Style="color: #47759E;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 40px;" colspan="4">
                            Note:
                            <ext:DisplayField ID="dispNotes" runat="server" Style="color: #47759E;" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <ext:GridPanel StyleSpec="margin-top:5px; border-color: transparent;" ID="gridTrainingLines"
                                runat="server" Cls="itemgrid" Width="840" MinHeight="150" AutoScroll="true">
                                <Store>
                                    <ext:Store ID="StoreTrainingLines" runat="server">
                                        <Model>
                                            <ext:Model ID="Model11" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LineID" />
                                                    <ext:ModelField Name="DateEng" Type="Date" />
                                                    <ext:ModelField Name="Day" Type="string" />
                                                    <ext:ModelField Name="InTimeDT" Type="Date" />
                                                    <ext:ModelField Name="OutTimeDT" Type="Date" />
                                                    <ext:ModelField Name="Hours" />
                                                    <ext:ModelField Name="IsTraining" />
                                                    <ext:ModelField Name="ResourcePerson" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>
                                        <ext:DateColumn ID="colDate" runat="server" Align="Left" Text="Date" Width="80" MenuDisabled="true"
                                            Sortable="false" Format="yyyy/MM/dd" DataIndex="DateEng">
                                        </ext:DateColumn>
                                        <ext:Column ID="Column45" Sortable="false" MenuDisabled="true" runat="server" Text="Day"
                                            Width="90" Align="Left" DataIndex="Day">
                                        </ext:Column>
                                        <ext:Column ID="asdfas" runat="server" Text="" Sortable="false" MenuDisabled="true"
                                            Width="50" DataIndex="IsTraining" Border="false">
                                            <Renderer Fn="IsTrainingRender">
                                            </Renderer>
                                        </ext:Column>
                                        <ext:DateColumn ID="colInTime1" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Start" Align="Left" Width="95" DataIndex="InTimeDT" Format="hh:mm tt">
                                        </ext:DateColumn>
                                        <ext:DateColumn ID="colOutTime1" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="End" Align="Left" Width="95" DataIndex="OutTimeDT" Format="hh:mm tt">
                                        </ext:DateColumn>
                                        <ext:Column ID="colWorkHours" Sortable="false" MenuDisabled="true" runat="server"
                                            Text="Hours" Align="Left" Width="100" DataIndex="Hours">
                                        </ext:Column>
                                        <ext:TagColumn ID="TagColumn1" runat="server" Text="Resource Person(s)" DataIndex="ResourcePerson"
                                            Flex="1">
                                        </ext:TagColumn>
                                    </Columns>
                                </ColumnModel>
                                <DockedItems>
                                    <ext:FieldContainer ID="Container1" runat="server" Dock="Bottom" Layout="HBoxLayout"
                                        StyleSpec="margin-top:2px;">
                                        <Defaults>
                                            <ext:Parameter Name="height" Value="32" />
                                        </Defaults>
                                        <Items>
                                            <ext:DisplayField ID="lblDaysTotal" Cls="total-field" runat="server" Width="80" />
                                            <ext:DisplayField ID="DisplayField1" Text="" Cls="total-field" runat="server" Width="90" />
                                            <ext:DisplayField ID="DisplayField2" Text="" Cls="total-field" runat="server" Width="50" />
                                            <ext:DisplayField ID="DisplayField3" Text="" Cls="total-field" runat="server" Width="95" />
                                            <ext:DisplayField ID="DisplayField4" Text="" Cls="total-field" runat="server" Width="95" />
                                            <ext:DisplayField ID="lblTotalHours" Cls="total-field" runat="server" Width="100" />
                                            <ext:DisplayField ID="lblTotalRP" Cls="total-field" runat="server" Width="550" />
                                        </Items>
                                    </ext:FieldContainer>
                                </DockedItems>
                                <View>
                                    <ext:GridView ID="GridView2" runat="server" StripeRows="false">
                                        <GetRowClass Fn="getRowClassTrainingLn" />
                                    </ext:GridView>
                                </View>
                            </ext:GridPanel>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding-top: 10px; padding-left: 10px">
                <table>
                    <tr>
                        <td colspan="2">
                            <ext:TabPanel ID="TabPanel1" runat="server" MinHeight="150" Width="840">
                                <Items>
                                    <ext:Panel ID="Panel1" runat="server" Title="Seats Allocated">
                                        <Content>
                                            <ext:GridPanel ID="gridTRSeatsAllocated" runat="server" MinHeight="0" Width="700">
                                                <Store>
                                                    <ext:Store ID="storeTRSeatsAllocated" runat="server">
                                                        <Model>
                                                            <ext:Model ID="modelTRSeatsAllocated" runat="server" IDProperty="TrainingRequestId">
                                                                <Fields>
                                                                    <ext:ModelField Name="TrainingID" Type="String" />
                                                                    <ext:ModelField Name="Name" Type="String" />
                                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                                    <ext:ModelField Name="Branch" Type="String" />
                                                                    <ext:ModelField Name="Department" Type="String" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:Column ID="colName" Width="180" runat="server" Text="Employee Name" DataIndex="Name"
                                                            Sortable="false" MenuDisabled="true" Align="Left">
                                                        </ext:Column>
                                                        <ext:Column ID="colEIN" Width="60" runat="server" Text="EIN" DataIndex="EmployeeId"
                                                            Sortable="false" MenuDisabled="true" Align="Center">
                                                        </ext:Column>
                                                        <ext:Column ID="Column5" Width="120" runat="server" Text="Branch" DataIndex="Branch"
                                                            Sortable="false" MenuDisabled="true" Align="Left">
                                                        </ext:Column>
                                                        <ext:Column ID="Column6" Width="120" runat="server" Text="Department" DataIndex="Department"
                                                            Sortable="false" MenuDisabled="true" Align="Left">
                                                        </ext:Column>
                                                        <ext:CommandColumn ID="CommandColumn3" runat="server" Width="80" MenuDisabled="true"
                                                            Sortable="false">
                                                            <Commands>
                                                                <ext:GridCommand Text="Change">
                                                                    <Menu EnableScrolling="false">
                                                                        <Items>
                                                                            <ext:MenuCommand Text="Approve" CommandName="Approve" />
                                                                            <ext:MenuCommand Text="House Full" CommandName="HouseFull" />
                                                                            <ext:MenuCommand Text="Waiting List" CommandName="WaitingList" />
                                                                        </Items>
                                                                    </Menu>
                                                                </ext:GridCommand>
                                                            </Commands>
                                                            <Listeners>
                                                                <Command Handler="CommandHandler(command, record);" />
                                                            </Listeners>
                                                        </ext:CommandColumn>
                                                    </Columns>
                                                </ColumnModel>
                                                <SelectionModel>
                                                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                                </SelectionModel>
                                            </ext:GridPanel>
                                        </Content>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel2" runat="server" Title="Pending Requests">
                                        <Content>
                                            <ext:GridPanel ID="gridTRPendingRequest" runat="server" MinHeight="0" Width="700">
                                                <Store>
                                                    <ext:Store ID="store4" runat="server">
                                                        <Model>
                                                            <ext:Model ID="model3" runat="server" IDProperty="TrainingRequestId">
                                                                <Fields>
                                                                    <ext:ModelField Name="TrainingID" Type="String" />
                                                                    <ext:ModelField Name="Name" Type="String" />
                                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                                    <ext:ModelField Name="Branch" Type="String" />
                                                                    <ext:ModelField Name="Department" Type="String" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel>
                                                    <Columns>
                                                        <ext:Column ID="Column3" Width="180" runat="server" Text="Employee Name" Sortable="false"
                                                            DataIndex="Name" MenuDisabled="true" Align="Left">
                                                        </ext:Column>
                                                        <ext:Column ID="Column4" Width="60" runat="server" Text="EIN" DataIndex="EmployeeId"
                                                            Sortable="false" MenuDisabled="true" Align="Center">
                                                        </ext:Column>
                                                        <ext:Column ID="Column7" Width="120" runat="server" Text="Branch" DataIndex="Branch"
                                                            Sortable="false" MenuDisabled="true" Align="Left">
                                                        </ext:Column>
                                                        <ext:Column ID="Column8" Width="120" runat="server" Text="Department" DataIndex="Department"
                                                            Sortable="false" MenuDisabled="true" Align="Left">
                                                        </ext:Column>
                                                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" MenuDisabled="true"
                                                            Sortable="false">
                                                            <Commands>
                                                                <ext:GridCommand Text="Change">
                                                                    <Menu EnableScrolling="false">
                                                                        <Items>
                                                                            <ext:MenuCommand Text="Approve" CommandName="Approve" />
                                                                            <ext:MenuCommand Text="House Full" CommandName="HouseFull" />
                                                                            <ext:MenuCommand Text="Waiting List" CommandName="WaitingList" />
                                                                        </Items>
                                                                    </Menu>
                                                                </ext:GridCommand>
                                                            </Commands>
                                                            <Listeners>
                                                                <Command Handler="CommandHandler(command, record);" />
                                                            </Listeners>
                                                        </ext:CommandColumn>
                                                    </Columns>
                                                </ColumnModel>
                                                <SelectionModel>
                                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                                </SelectionModel>
                                            </ext:GridPanel>
                                        </Content>
                                    </ext:Panel>
                                </Items>
                            </ext:TabPanel>
                        </td>
                    </tr>
                </table>
            </div>
        </Content>
        <Buttons>
            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnClose" Cls="btnFlat"
                BaseCls="btnFlat" Text="<i></i>Close">
                <Listeners>
                    <Click Handler="#{windowTPR}.hide();">
                    </Click>
                </Listeners>
            </ext:LinkButton>
        </Buttons>
    </ext:Window>
    <ext:Window ID="windowAllocateSeat" runat="server" Title="Allocate Seat" Icon="None"
        ButtonAlign="Left" AutoScroll="true" Height="550" Width="600" BodyPadding="5"
        Hidden="true" Modal="false">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:DisplayField ID="dispASTrainingName" runat="server" Style="color: #47759E; font-size: 15px;
                            font-weight: bold;" />
                        <%--  <ext:DisplayField ID="dispASTrainingDetails" runat="server" Style="color: #47759E;">
                        </ext:DisplayField>--%>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="height: 40px;">
                        Hosted by
                        <ext:DisplayField ID="dispASHostedBy" runat="server" Style="color: #47759E;" />
                    </td>
                </tr>
                <%-- <tr>
                    <td style="height: 40px;">
                        Resource Person:
                        <ext:DisplayField ID="dispASResourcePerson" runat="server" Style="color: #47759E;" />
                    </td>
                </tr>--%>
                <tr>
                    <td style="padding-top: 10px">
                        <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model9" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox ID="cmbEmployee" FieldLabel="Select an employee to add" LabelWidth="150"
                            LabelAlign="Top" LabelSeparator="" runat="server" DisplayField="Name" ValueField="EmployeeId"
                            StoreID="storeSearch" TypeAhead="false" Width="300" PageSize="9999" HideBaseTrigger="true"
                            MinChars="1" TriggerAction="All" ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;margin-top:20px">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                            </Listeners>
                            <DirectEvents>
                                <Select OnEvent="btnAdddEmployee_Click">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="gridItems" Value="Ext.encode(#{GridEmployee}.getRowsValues({selectedOnly:false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                        <%--<asp:RequiredFieldValidator Display="None" ID="rfvEmployee" runat="server" ValidationGroup="SaveAS"
                            ControlToValidate="cmbEmployee" ErrorMessage="Please select employee" />--%>
                    </td>
                    <td>
                        <ext:Button runat="server" Cls="btn btn-primary btn-sect" Height="30px" ID="btnAdddEmployee"
                            Hidden="true" StyleSpec="margin-top:15px;" Text="<i></i>Add">
                            <DirectEvents>
                                <Click OnEvent="btnAdddEmployee_Click">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="gridItems" Value="Ext.encode(#{GridEmployee}.getRowsValues({selectedOnly:false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:GridPanel ID="GridEmployee" runat="server" Width="500" Cls="itemgrid" Scroll="None"
                            MinHeight="120" Title="Selected Employee" StyleSpec="margin-top:15px;clear:both;">
                            <Store>
                                <ext:Store ID="storeEmpAssign" runat="server">
                                    <Model>
                                        <ext:Model ID="Model12" runat="server" IDProperty="EmployeeId">
                                            <Fields>
                                                <ext:ModelField Name="EmployeeId" />
                                                <ext:ModelField Name="Name" />
                                                <ext:ModelField Name="Branch" />
                                                <ext:ModelField Name="Department" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:Column ID="Column46" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                                        Width="50" Align="Center" DataIndex="EmployeeId">
                                    </ext:Column>
                                    <ext:Column ID="Column47" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                                        Flex="1" Width="140" Align="Left" DataIndex="Name">
                                    </ext:Column>
                                    <ext:Column ID="Column48" Width="100" runat="server" DataIndex="Branch" MenuDisabled="true"
                                        Text="Branch" Align="Left">
                                    </ext:Column>
                                    <ext:Column ID="Column49" Width="100" runat="server" DataIndex="Department" MenuDisabled="true"
                                        Text="Department" Align="Left">
                                    </ext:Column>
                                    <ext:CommandColumn ID="CommandColumn8" runat="server" Width="30" Text="" Align="Center">
                                        <Commands>
                                            <ext:CommandSeparator />
                                            <ext:GridCommand Cls="editGridButton" ToolTip-Text="remove" Text="" Icon="Delete"
                                                CommandName="deleterow" />
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandHandlerEmployeeGrid(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <View>
                                <ext:GridView ID="GridView3" runat="server" StripeRows="true" />
                            </View>
                        </ext:GridPanel>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 10px">
                        <ext:Checkbox  ID="cbSendEmail" BoxLabel="Send e-mail to selected employees on seat allocation"
                            LabelWidth="200" LabelSeparator="" BoxLabelAlign="After" Hidden="false" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextArea ID="txtASNotes" runat="server" Height="70" Width="400" FieldLabel="Notes"
                            Hidden="true" LabelAlign="Top" LabelSeparator="" />
                    </td>
                </tr>
                <%-- <tr>
                    <td style="height: 10px;">
                    </td>
                </tr>--%>
            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" ID="btnAllocateSeatSave" Cls="btn btn-primary" Text="<i></i>Allocate Seat">
                <DirectEvents>
                    <Click OnEvent="btnAllocateSeatSave_Click">
                        <EventMask ShowMask="true" />
                        <ExtraParams>
                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{GridEmployee}.getRowsValues({selectedOnly:false}))"
                                Mode="Raw" />
                        </ExtraParams>
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'SaveAS'; return CheckValidation();">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:LinkButton runat="server" ID="btnCancelAllS" StyleSpec="padding:0px;" Text="Cancel"
                Cls="btnFlatLeftGap">
                <Listeners>
                    <Click Handler="#{windowAllocateSeat}.hide();">
                    </Click>
                </Listeners>
            </ext:LinkButton>
        </Buttons>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
