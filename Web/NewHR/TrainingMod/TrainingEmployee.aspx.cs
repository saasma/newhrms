﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;

namespace Web.NewHR.TrainingMod
{
    public partial class TrainingEmployee : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["filter"]) && Request.QueryString["filter"] == "thismonth")
            {
                txtDateFrom.Text = DateTime.Now.Date.ToString();
                txtDateTo.Text = DateTime.Now.Date.ToString();
                gridTrainingEmployee.GetStore().DataSource = TrainingManager.GetTrainingEmployeeList(DateTime.Parse(txtDateFrom.Text.Trim()), DateTime.Parse(txtDateTo.Text.Trim()));
                gridTrainingEmployee.GetStore().DataBind();
            }
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(txtDateFrom.Text.Trim()) || string.IsNullOrEmpty(txtDateTo.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("From date and to date is required");
                return;
            }

            gridTrainingEmployee.GetStore().DataSource = TrainingManager.GetTrainingEmployeeList(DateTime.Parse(txtDateFrom.Text.Trim()), DateTime.Parse(txtDateTo.Text.Trim()));
            gridTrainingEmployee.GetStore().DataBind();
        }

    }
}