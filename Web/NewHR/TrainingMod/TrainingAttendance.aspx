﻿<%@ Page Title="Training Attendance" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="TrainingAttendance.aspx.cs" Inherits="Web.NewHR.TrainingMod.TrainingAttendance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">


<style type="text/css">

    .clsBold
{
    color:#47759E;
    font-weight:bold;
    padding-left:3px;
}

.clsItalic
{
    color:#47759E;
    font-style:italic;
}

.clsBlue
{
    color:#47759E;
}

</style>

<script type="text/javascript">

    var afterEdit = function (e1, e2, e3, e4, e5, e6) {
        
        var val = e2.value;
         var GridPanelStore = <%=gridTrainingAttendance.ClientID %>.getStore();
        var isTrue = <%=chkCopyMode.ClientID %>.getValue();
        var colCount = <%=gridTrainingAttendance.ClientID %>.columns.length - 1;
        var records = <%=gridTrainingAttendance.ClientID %>.getStore();
        var record=null,
        length = records.data.length,
        cols = 0,
        SNo = 0,
        i = 0;

        SNo = e2.record.data.SN;

        if(SNo % 2 === 0)
        {
            i = 1;
        }
        else
        {
            i = 0;
        }       

        if(isTrue)
        {
            for (; i < length; i = i +2) 
            {
                record = records.data.items[i];
              
                SNo = record.data.SN;

                var k = 0;
                for(var j = 0; j < colCount; ++j)
                {
                    if(record.get('Day_' + j) != 'WH')
                    {
                        record.set('Day_' + j,val);
                    }
                    k = k +1;
                    
                }
                record.commit();
                
            }

             GridPanelStore.commitChanges;
                  <%=gridTrainingAttendance.ClientID %>.getView().refresh(false);
        }

    };


    function chkCopyModeChange()
    {       

        if(<%=chkCopyMode.ClientID %>.getValue() == true)
        {
            <%=chkSingleMode.ClientID %>.setValue(false);
        }
    }

    function chkSingleModeChange()
    {
        if(<%=chkSingleMode.ClientID %>.getValue() == true)
        {
            <%=chkCopyMode.ClientID %>.setValue(false);
        }        
    }


</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Training Attendance Management
                </h4>
            </div>
        </div>
    </div>


 <div class="contentpanel">

 <ext:Hidden ID="hdnTrainingId" runat="server" />
 <ext:Hidden ID="hdnTotalDays" runat="server" />
 <table>
    <tr>
        <td>
           <ext:DisplayField ID="dfTrainingName"  FieldCls="clsBold" runat="server" Width="300" />
        </td>
        <td>
            <ext:DisplayField ID="dfTrainingDetails" FieldCls="clsItalic" runat="server" Width="350" />
        </td>
        <td style="padding-bottom:7px;">
            <ext:Checkbox ID="chkCopyMode" runat="server" LabelAlign="Left" LabelWidth="70" FieldLabel="Copy Mode" LabelSeparator="" LabelCls="clsBlue" Width="100">
                <Listeners>
                    <Change Fn="chkCopyModeChange" />
                </Listeners>
            </ext:Checkbox>
        </td>
        <td style="padding-bottom:7px;">
            <ext:Checkbox ID="chkSingleMode" runat="server" LabelAlign="Left"  LabelWidth="80" LabelSeparator="" LabelCls="clsBlue" FieldLabel="Single Mode" Width="100">
                <Listeners>
                    <Change Fn="chkSingleModeChange" />
                </Listeners>
            </ext:Checkbox>
        </td>
    </tr>
 </table>

                         <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridTrainingAttendance" runat="server" Cls="itemgrid"
                                Scroll="None" Width="1075">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Reader>
                                            <ext:ArrayReader>
                                            </ext:ArrayReader>
                                        </Reader>
                                        <Model>
                                            <ext:Model ID="Model1" runat="server" IDProperty="SN">
                                                <Fields>
                                                    <ext:ModelField Name="SN" Type="Int" />
                                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel>
                                    <Columns>                                       
                                        <ext:Column ID="colName" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                            Width="200" Align="Left" DataIndex="Name">
                                        </ext:Column>                                        
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                </SelectionModel>
                                <Plugins>
                                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                                        <Listeners>
                                            <Edit Fn="afterEdit" />
                                        </Listeners>
                                    </ext:CellEditing>
                                </Plugins>
                            </ext:GridPanel>


        <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
                    Height="30" ID="btnSave" Text="<i></i>Save Attendance">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" />
                            <ExtraParams>
                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridTrainingAttendance}.getRowsValues({ selectedOnly: false }))"
                                    Mode="Raw" />
                            </ExtraParams>
                        </Click>
                    </DirectEvents>
                </ext:Button>


 </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
