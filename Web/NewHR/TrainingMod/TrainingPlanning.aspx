﻿<%@ Page Title="Training Planning" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="TrainingPlanning.aspx.cs" Inherits="Web.NewHR.TrainingMod.TrainingPlanning" %>

<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage {
            margin: 0px !important;
            padding-left: 20px !important;
        }

        .CommandCls {
            background: transparent !important;
            border: none !important;
        }

        #menu {
            display: none;
        }

        .innerLR {
            padding: 0px;
        }
        /*hide DEMO of DayPilot*/
        .scheduler_default_corner {
            visibility: hidden;
        }

        .itemList {
            margin: 0px;
            padding: 0px;
        }

            .itemList li {
                float: left;
                list-style-type: none;
            }

        .blockBottomBorder {
            border-bottom: 4px solid #EAF3FA;
            padding-bottom: 1px;
        }

        .blockBottomBorder1 {
            border-bottom: 4px solid #EAF3FA;
            width: 1253px;
        }

        .blockTitle {
            font-size: 14px;
            padding-left: 2px;
            margin-bottom: 5px;
        }

        .blockTraining {
            background-color: #EAF3FA;
            padding: 5px 5px 5px 2px;
        }

        .textCls x-form-display-field {
            color: #157fcc !important;
            padding-top: 3px;
            font-weight: bold;
            background: red !important;
        }

        #ctl00_ContentPlaceHolder_Main_CalendarPanel1-tb-day {
            visibility: hidden;
        }

        #ctl00_ContentPlaceHolder_Main_CalendarPanel1-tb-week {
            visibility: hidden;
        }

        #ctl00_ContentPlaceHolder_Main_CalendarPanel1-tb-month {
            visibility: hidden;
        }

        .commentCls td {
            background-color: #EFEFEF !important;
        }

        .clsCursorHand {
            cursor: pointer;
        }

        #tblBookedInfo td {
            padding: 5px;
            color: White;
        }

        .blockTrainingName {
            width: 350px;
            background-color: #EAF3FA;
            margin-top: 5px;
            padding: 5px 5px 5px 0px;
            border-bottom: 1px solid #428BCA;
        }
    </style>
    <style>
        .total-field .x-form-display-field {
            font-style: italic;
            font-weight: bold !important;
            background: #E8F1FF !important;
            color: #428BCA;
            padding: 0px 0px 0px 5px;
        }
    </style>
     <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
  
    <script type="text/javascript">


        /* Fast filter helpers */

        function clear() {
            var filterBox = document.getElementById("TextBoxFilter");
            filterBox.value = '';
            filter();
        }
        function filter() {
            var filterBox = document.getElementById("TextBoxFilter");
            var filterText = filterBox.value;

            dps1.clientState = { "filter": filterText };
            dps1.commandCallBack("filter");
        }

        function key(e) {
            var keynum = (window.event) ? event.keyCode : e.keyCode;

            if (keynum === 13) {
                filter();
                return false;
            }

            return true;
        }

        var CommandHandlerClear = function(command, record){
            var GridPanelStore = <%=gridAttRequest.ClientID %>.getStore();
                if(command=="Clear")
                {

                    for(i=0;i<GridPanelStore.data.items.length;i++)
                    {
                        if(record.data.ResourcePerson!="") 
                        { 
                            if(GridPanelStore.data.items[i].data.IsTraining==true)
                                GridPanelStore.data.items[i].data.ResourcePerson=record.data.ResourcePerson;
                        }
                    }

                    GridPanelStore.commitChanges;
                    <%=gridAttRequest.ClientID %>.getView().refresh(false);
                }
            };

            var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
                var store = this.grid.store;
                store.remove(record);
            };


            var IsTrainingRender = function (value, metaData, record, rowIndex, colIndex, store) {
                var Name = "";
                if (typeof (value) == 'undefined' || value == '')
                    Name = "No";
                    
                if (value == false)
                    Name = "No";
                else if (value == "1")
                    Name = "Yes";

                // setTotal();

                return Name;
            }


            var getRowClass = function (record) {
                if (record.data.IsTraining==false) {
                    return "commentCls";
                }
                else{
                    return "new-row";
                }
            };


            var beforeEdit = function(e1,e2,record)
            {
               
                if(e2.field == 'IsTraining')
                {
                    if(e2.record.data.IsTraining==true)
                    {
                        e2.record.data.IsTraining=false;
                        e2.record.data.InTimeDT=''; 
                        e2.record.data.OutTimeDT='';
                        e2.record.data.Hours='';
                        e2.record.data.ResourcePerson='';
                        e2.record.commit();
                        e2.cancel = true;
                    }
                    else
                    {
                        e2.record.data.IsTraining=true; 
                        e2.record.commit();
                        e2.cancel = true;
                    }

                }


                if(e2.field!='IsTraining')
                {
                    if(e2.record.data.IsTraining == null || e2.record.data.IsTraining==false)
                    {
                        e2.cancel = true;
                    }
                }

                ReCalculateGridTotals();
            };

            function cellSelectionChanged(e1, e2, e3, e4) {
                
                
            }

            function showAddNewRP()
            {
                Ext.getBody().mask("Loading...");

                Ext.net.DirectMethods.ShowAddNewRP(
                           {
                               success: function (result) { Ext.getBody().unmask(); },
                               failure: function (result) { Ext.getBody().unmask();  }
                           }
                           );             
            }
        
            var RenderColor = function(value, meta, record, rowIndex, colIndex, store) {
                //debugger;
                //or can set css tyle to meta.css
                if(record.data.IsTraining==false)
                    meta.attr = 'style="background-color:red';
            }
                     
            var CalculateWorkHours = function(e1,e2,record)
            {

                if(record.data.InTimeDT == null || record.data.OutTimeDT == null)
                {
                    return;
                }


                if(record.data.InTimeDT != '' && record.data.OutTimeDT != '')
                {
                  
                    var diffTime = getTotalMinutes(record.data.InTimeDT,record.data.OutTimeDT);

                    if(diffTime <= 0)
                    {
                        alert('End time must be greater than Start time.');
                        return "";
                    }

                    return (Math.floor(diffTime/60) + ":" + (diffTime%60));
              
                }
            };



            Array.prototype.contains = function(v) {
                for(var c = 0; c < this.length; c++) {
                    if(this[c] === v) return true;
                }
                return false;
            };

            Array.prototype.unique = function() {
                var arr = [];
                for(var t = 0; t < this.length; t++) {
                    if(!arr.contains(this[t])) {
                        arr.push(this[t]);
                    }
                }
                return arr; 
            }

            var afterEdit = function (editor, e) {
                var GridPanelStore = <%=gridAttRequest.ClientID %>.getStore();
              //            var RP = e.store.data.items[i].data.ResourcePerson;
              //            var AddedRP = <%=hdnAddedRP.ClientID%>.getValue();
              //            alert(AddedRP);
              //            GridPanelStore.data.items[e.rowIdx].data.ResourcePerson=(RP + ',' +  AddedRP);
            

              debugger;
              var TotalDays = 0;
              var TotalRP = '';
              var TotalHrs = 0;
              var TotalMins = 0;
              //debugger;

              for(i=0;i<e.store.data.items.length;i++)
              {
                  if(e.store.data.items[i].data.IsTraining == true)
                      TotalDays = TotalDays + 1;

                

                  if(e.store.data.items[i].data.InTimeDT != '' && e.store.data.items[i].data.OutTimeDT != '' && 
                    e.store.data.items[i].data.InTimeDT!= null && e.store.data.items[i].data.OutTimeDT != null)
                  {
                   
                      var diffTime = getTotalMinutes(e.store.data.items[i].data.InTimeDT,e.store.data.items[i].data.OutTimeDT); //calculateTotalMinutes(eTime,false,true) - calculateTotalMinutes(strTime, false, false)
                      TotalHrs = TotalHrs +  Math.floor(diffTime/60) ;
                      TotalMins = TotalMins+  (diffTime%60);
                  }

               
                  
                  if(e.store.data.items[i].data.ResourcePerson!='' && e.store.data.items[i].data.IsTraining == true)
                  {
                      if(TotalRP!='')
                          TotalRP = TotalRP + ',' + e.store.data.items[i].data.ResourcePerson;
                      else
                          TotalRP =  e.store.data.items[i].data.ResourcePerson;
                  }
               
              }

              var str_array=  TotalRP.split(',');
              var strTime = "";
              if(TotalHrs==0 && TotalMins!=0)
                  strTime = TotalMins +' Mins';

              if(TotalMins==0 && TotalHrs!=0)
                  strTime = TotalHrs +' Hrs';

              if(TotalHrs!=0 && TotalMins!=0)
                  strTime = TotalHrs +' Hrs ' + TotalMins +' Mins';

              var uniquesRP = str_array.unique();
              if(TotalDays>0)
                  <%=lblDaysTotal.ClientID %>.setValue(TotalDays +' Days' );
            else  <%=lblDaysTotal.ClientID %>.setValue('');
              if(TotalRP!='')
                  <%=lblTotalRP.ClientID %>.setValue(uniquesRP.length +' resource persons' );
               else <%=lblTotalRP.ClientID %>.setValue('');
              <%=lblTotalHours.ClientID %>.setValue(strTime);
          };


          var afterEdit1 = function (e1, e2) {
              var ResourePerson =  e2.record.data.ResourcePerson;

              var str_array=  ResourePerson.split(',');

          };

          function ReCalculateGridTotals()
          {
       
              var TotalDays = 0;
              var TotalRP = '';
              var TotalHrs = 0;
              var TotalMins = 0;

              var GridPanelStore = <%=gridAttRequest.ClientID %>.getStore();

            for(i=0;i<GridPanelStore.data.items.length;i++)
            {
                if(GridPanelStore.data.items[i].data.IsTraining == true)
                    TotalDays = TotalDays + 1;

              


                if(GridPanelStore.data.items[i].data.InTimeDT != '' && GridPanelStore.data.items[i].data.OutTimeDT != '' && 
                GridPanelStore.data.items[i].data.InTimeDT!= null &&GridPanelStore.data.items[i].data.OutTimeDT != null)
                {
                 
                    var diffTime = getTotalMinutes(GridPanelStore.data.items[i].data.InTimeDT,GridPanelStore.data.items[i].data.OutTimeDT); //calculateTotalMinutes(eTime,false,true) - calculateTotalMinutes(strTime, false, false)
                    TotalHrs = TotalHrs +  Math.floor(diffTime/60) ;
                    TotalMins = TotalMins+  (diffTime%60);
                }

             
                
                if(GridPanelStore.data.items[i].data.ResourcePerson!='' && GridPanelStore.data.items[i].data.IsTraining == true)
                {
                    if(TotalRP!='')
                        TotalRP = TotalRP + ',' + GridPanelStore.data.items[i].data.ResourcePerson;
                    else
                        TotalRP =  GridPanelStore.data.items[i].data.ResourcePerson;
                }
               
            }

            var str_array=  TotalRP.split(',');
            var strTime = "";
            if(TotalHrs==0 && TotalMins!=0)
                strTime = TotalMins +' Mins';

            if(TotalMins==0 && TotalHrs!=0)
                strTime = TotalHrs +' Hrs';

            if(TotalHrs!=0 && TotalMins!=0)
                strTime = TotalHrs +' Hrs ' + TotalMins +' Mins';

            var uniquesRP = str_array.unique();

            if(TotalDays>0)
                <%=lblDaysTotal.ClientID %>.setValue(TotalDays +' Days' );
            else  <%=lblDaysTotal.ClientID %>.setValue('');

            if(TotalRP!='')
                <%=lblTotalRP.ClientID %>.setValue(uniquesRP.length +' resource persons' );
            else <%=lblTotalRP.ClientID %>.setValue('');
           <%=lblTotalHours.ClientID %>.setValue(strTime);
       }

        /// http://stackoverflow.com/questions/7709803/javascript-get-minutes-between-two-dates
       function getTotalMinutes(start,end)
       {
           
           start = new Date(2016,1,1,start.getHours(),start.getMinutes(),0);
           end = new Date(2016,1,1,end.getHours(),end.getMinutes(),0);

           var diffMs = (end - start); // milliseconds between now & Christmas
           var diffDays = Math.round(diffMs / 86400000); // days
           var diffHrs = Math.round((diffMs % 86400000) / 3600000); // hours
           var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
           return (diffHrs * 60) + diffMins;
       }

       

       var renderResourcePersons= function (value, metaData, record, rowIndex, colIndex, store) {
           if(typeof(value)=='undefined' || value==0)
               return;
           var str_array=  value.split(',');
           var ResourcePersons = '';

           for(var i = 0; i < str_array.length; i++) {
               var id= str_array[i];
               var r = <%= storeResoucrePerson.ClientID %>.getById(id.toString());

        if (Ext.isEmpty(r)) {
            return "";
        }

        if(ResourcePersons!='')
            ResourcePersons = ResourcePersons +',' + r.data.Name;
        else
            ResourcePersons = r.data.Name;
          
    }

            return ResourcePersons;

        };


// Fix for Group panel Expand/Collapse or row adding case : Uncaught TypeError: Cannot read property 'isCollapsedPlaceholder' of undefined

Ext.view.Table.override({
    indexInStore: function(node) {
        node = (node && node.isCollapsedPlaceholder) ? this.getNode(node) : this.getNode(node, false);

        if (!node && node !== 0) {
            return -1;
        }

        var recordIndex = node.getAttribute('data-recordIndex');

        if (recordIndex) {
            return parseInt(recordIndex, 10);
        }

        return this.dataSource.indexOf(this.getRecord(node));
    }
});
    </script>
    <script type="text/javascript">
        var txtStartDate = null;
        var txtEndDate = null;
       
        var lblTotalDays = null;
        var lblTotalHours = null;
        
        var tfEndTime = null;
        var hdnTotalDays = null;
        var hdnTotalHours = null;

        Ext.onReady(
        function () {
            

        });

        var CompanyX = {


            setStartDate: function (picker, date) {

                this.getCalendar().setStartDate(date);
                //CompanyX.hdFromDateEng.value = date.toDateString();

                //reloadGrids();
            }
        }
       

        function datechange()
        {

            txtStartDate=<%=txtStartDate.ClientID %>;
            txtEndDate=<%=txtEndDate.ClientID %>;

            hdnTotalDays=<%=hdnTotalDays.ClientID %>;
            

            var start = txtStartDate.getRawValue();
            var end = txtEndDate.getRawValue();
            if(start != "" && end != "")
            {
                var date1 = new Date(start);
                var date2 = new Date(end);
                var one_day=1000*60*60*24; 
                var  _Diff=Math.ceil((date2.getTime()-date1.getTime())/(one_day)); 

                if(_Diff>=0)
                    _Diff += 1;
                
                if(isNaN(_Diff))
                    _Diff = 0;

                //            lblTotalDays.setValue(_Diff);
                hdnTotalDays.setValue(_Diff);
            }
        }

        var CommandHandler = function(command, record){
            <%= hdnTrainingId.ClientID %>.setValue(record.data.TrainingID);
            
        if(command=="Edit")
        {
            <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else if(command=="Delete")
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }
                else if(command=="Publish")
                {
                    <%= btnPublish.ClientID %>.fireEvent('click');
                }
                else if(command=="Unpublish")
                {
                    <%= btnUnpublish.ClientID %>.fireEvent('click');
                }

    }
       
var CalculateEffectiveHour = function(){
    Ext.net.DirectMethods.CalculateEffeciveHours();
};

var loadHoliday = function (month, year) {
    Ext.net.Mask.show();
    Ext.net.DirectMethod.request({
        url: "../LeaveRequestService.asmx/GetHolidays",
        cleanRequest: true,
        json: true,
        params: {
            month: month,
            year: year
        },
        success: function (result) {
            holidayList = result;
            //Ext.Msg.alert("Json Message", result);
            CompanyX.DatePicker1.highlightDates(result);
            Ext.net.Mask.hide();

            //find the range case
            for (i = 0; i < result.length; i++) {
                if (result[i].Id == 0) {
                           
                    break;
                }
            }
        },
        failure: function (result) { Ext.net.Mask.hide(); }
    });
};

function trainingSelected(cal, rec, el) {
            
    <%= hdnTrainingId.ClientID %>.setValue(rec.data.EventId);
            <%= btnEdit.ClientID %>.fireEvent('click');
        }

        function dayClickCl(cal, dt, el)
        {
            <%= txtTrainingName.ClientID %>.setValue('');
            <%= txtSeats.ClientID %>.setValue('');
            <%= txtVenue.ClientID %>.setValue('');
            <%= txtHost.ClientID %>.setValue('');
            <%= txtResourcePerson.ClientID %>.setValue('');
            <%= mcmbLevel.ClientID %>.setValue('');
            <%= hdnTrainingId.ClientID %>.setValue('');
            <%=txtStartDate.ClientID %>.setValue(dt);
            <%=txtEndDate.ClientID %>.setValue(dt);
            <%=windowTraining.ClientID %>.center();
            <%=windowTraining.ClientID %>.show();
            <%=StoreTrainingLine.ClientID %>.removeAll();
            <%= cmbCountry.ClientID %>.setValue('');
            <%= txtCity.ClientID %>.setValue('');
            <%=dispRP.ClientID%>.setValue('');
            <%= cmbTrainingType.ClientID %>.setValue('');
            <%=cbChangeNotification.ClientID %>.hide();
            <%=cbSendEmail.ClientID %>.hide();
            <%=lblRP.ClientID %>.hide();
            <%=dispTrainingRequestCount.ClientID%>.setValue('');
            <%=dispTrainingApprovedCount.ClientID%>.setValue('');
            <%=lblDaysTotal.ClientID%>.setValue('');
            <%=lblTotalHours.ClientID%>.setValue('');
            <%=lblTotalRP.ClientID%>.setValue('');

        }


        function rangeSelectCl(cal, dates, callback)
        {
            
            <%= txtTrainingName.ClientID %>.setValue('');
            <%= txtSeats.ClientID %>.setValue('');
            <%= txtVenue.ClientID %>.setValue('');
            <%= txtHost.ClientID %>.setValue('');
            <%= txtResourcePerson.ClientID %>.setValue('');
            
            
            <%= mcmbLevel.ClientID %>.setValue('');
            
            
            <%= hdnTrainingId.ClientID %>.setValue('');
            <%=txtStartDate.ClientID %>.setValue(dates.StartDate);
            <%=txtEndDate.ClientID %>.setValue(dates.EndDate);
            <%=windowTraining.ClientID %>.center();
            <%=windowTraining.ClientID %>.show();
          
        }


    </script>
</asp:Content>
<asp:Content ID="content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder_Main">


    <ext:Hidden ID="hdnTrainingId" runat="server" Text="" />
    <ext:Hidden ID="hdnTotalDays" runat="server" />
    <ext:Hidden ID="hdnAddedRP" runat="server" />
    <ext:Hidden ID="hdnTotalHours" runat="server" />
    <ext:LinkButton ID="btnEdit" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the training." />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnPublish" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnPublish_Click">
                <EventMask ShowMask="true" />
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to publish the training." />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton ID="btnUnpublish" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnUnpublish_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Training Planning
                </h4>
            </div>
        </div>
    </div>
    <ext:Store ID="storeResoucrePerson" runat="server">
        <Model>
            <ext:Model ID="Model7" runat="server" IDProperty="ResourcePersonId">
                <Fields>
                    <ext:ModelField Name="ResourcePersonId" Type="String" />
                    <ext:ModelField Name="Name" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <div class="contentpanel">
        <div class="innerLR" style="margin-top: -30px;">
            <%-- <h4 class="heading">
            <div class="blockBottomBorder1">Training Planning
            </div>
         </h4>   --%>

            <div>
                <div id="toolbar">
                    <ext:Button runat="server" ID="btnAddNew1" Cls="btn btn-primary" StyleSpec="margin-top:20px;margin-left:10px"
                        Text="Add New Training">
                        <DirectEvents>
                            <Click OnEvent="btnAddNew_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <br />
                    <%--<a style="font-size: 18px" title="Previous" href="javascript:setCurrentDate('previous');dps1.commandCallBack('previous');">
                &#x25c4;</a> <a style="font-size: 18px" href="javascript:setCurrentDate('next');dps1.commandCallBack('next');">
                    &#x25ba;</a> <a title="Next" href="javascript:setCurrentDate('this');dps1.commandCallBack('this');">
                        This Month</a>--%>
                </div>
                <table>
                    <tr>
                        <td valign="top">
                            <table>
                                <tr>
                                    <td valign="top" style="display: none">
                                        <ext:DatePicker ID="DatePicker1" runat="server" StyleSpec='StyleSpec="border:1px solid #A3BAD9;'
                                            Width="215">
                                            <Listeners>
                                                <%-- <Select Fn="CompanyX.setStartDate"/>--%>
                                            </Listeners>
                                        </ext:DatePicker>
                                    </td>
                                    <td style="width: 10px"></td>
                                    <td valign="top">
                                        <ext:CalendarPanel ID="CalendarPanel1" Width="700" ShowDayView="false" ShowWeekView="true"
                                            HeaderAsText="true" Header="true" Height="450" runat="server" ActiveIndex="2"
                                            Border="true" Margins="0">
                                            <CalendarStore ID="GroupStore1" runat="server">
                                            </CalendarStore>
                                            <EventStore ID="EventStore1" runat="server" NoMappings="true">
                                                <Proxy>
                                                    <ext:AjaxProxy Url="../../LeaveRequestService.asmx/GetTrainings" Json="true">
                                                        <ActionMethods Read="POST" />
                                                        <Reader>
                                                            <ext:JsonReader Root="d" />
                                                        </Reader>
                                                    </ext:AjaxProxy>
                                                </Proxy>
                                                <Mappings>
                                                    <ext:ModelField Name="StartDate" Type="Date" DateFormat="M$" />
                                                    <ext:ModelField Name="EndDate" Type="Date" DateFormat="M$" />
                                                </Mappings>
                                                <Parameters>
                                                    <ext:StoreParameter Name="StartDate" Mode="Raw" Value="#{CalendarPanel1}.layout.activeItem.lastRenderStart" />
                                                    <ext:StoreParameter Name="EndDate" Mode="Raw" Value="#{CalendarPanel1}.layout.activeItem.lastRenderEnd" />
                                                </Parameters>
                                                <Listeners>
                                                    <BeforeLoad Handler="Ext.net.Mask.show();" />
                                                    <Load Handler="Ext.net.Mask.hide();" />
                                                </Listeners>
                                            </EventStore>
                                            <%-- <WeekView ID="WeekView1" runat="server" ShowTime="false" />--%>
                                            <MonthView ID="MonthView1" runat="server" ShowHeader="true" ShowWeekLinks="false"
                                                ShowWeekNumbers="true">
                                            </MonthView>
                                            <Listeners>
                                                <EventClick Fn="trainingSelected" />
                                                <DayClick Fn="dayClickCl" />
                                                <RangeSelect Fn="rangeSelectCl" />
                                            </Listeners>
                                        </ext:CalendarPanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="padding-left: 20px;">
                            <%--  <div class="blockBottomBorder" style="margin-top: -50px;">
                                <span class="blockTitle" style="color: #47759E;">Training List</span>
                            </div>--%>
                            <div class="blockTraining">
                                <ext:DisplayField ID="dispThisMonthTitle" Cls="textCls" runat="server" />
                                <span id="spnTrainingName" runat="server" style="color: Red"></span>
                            </div>
                            <%-- <ext:DisplayField ID="dispThisMonthDetails" Cls="textCls" runat="server" />--%>
                            <asp:Repeater runat="server" ID="rptTrainingList">
                                <ItemTemplate>
                                    <li style="list-style: none; padding-bottom: 10px">
                                        <div class="blockTrainingName ">
                                            <span style="width: 150px;">
                                                <asp:HyperLink Width="150px" ID="Label2" runat="server" NavigateUrl='' Text='<%# "&nbsp;" + Eval("Name") %>'></asp:HyperLink>
                                                <%--(<%# Eval("StatusName")%>) --%>
                                            </span>
                                        </div>
                                        <div style="background-color: #f5f5f5; margin-top: 1px; width: 350px">
                                            <span style="font-style: italic; padding-left: 4px">
                                                <%# Eval("DateInfo")%>
                                            </span>
                                            <br />
                                            <span style="font-style: italic; padding-left: 4px; color: #808080">
                                                <%# Eval("Host")%>
                                            </span>
                                            <br />
                                            <span>
                                                <%--    <%# Eval("BookingInfo")%>--%>
                                                <table id="tblBookedInfo">
                                                    <tr>
                                                        <td style="background-color: #808080; width: 70px">Seats
                                                        </td>
                                                        <td style="background-color: #DADADA; width: 20px; color: Black">
                                                            <%# Eval("Seats")%>
                                                        </td>
                                                        <td>&nbsp
                                                        </td>
                                                        <td style="background-color: #5B9BD5; width: 70px">Booked
                                                        </td>
                                                        <td style="background-color: #BED7EE; width: 20px; color: Black">
                                                            <%# Eval("Booked")%>
                                                        </td>
                                                        <td>&nbsp
                                                        </td>
                                                        <td style="background-color: #538334; width: 70px">Available
                                                        </td>
                                                        <td style="background-color: #C6E0B4; width: 20px; color: Black">
                                                            <%# Eval("Avilable")%>
                                                        </td>
                                                        <td>
                                                            <%# Eval("ViewLink")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%-- <div style="background-color:grey;width:20px">Seats</div><div style="background-color:Green;width:20px">12</div>  <div style="background-color:Green;width:20px">Booked</div>
                                            <div style="background-color:Green;width:20px">8</div>   <div style="background-color:Green;width:20px">Avilable</div><div style="background-color:Green;width:20px">4</div>--%>
                                            </span>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>

                            <span style="padding-left: 5px"><a href="TrainingEmployee.aspx?filter=thismonth">view
                            all</a></span>
                            <br />
                        </td>
                    </tr>
                </table>
                <br />
                <ext:TabPanel ID="TabPanel1" runat="server" MinHeight="500" Width="1253">
                    <Items>
                        <ext:Panel ID="Panel1" runat="server" Title="Training Events">
                            <Content>
                                <br />
                                <ext:Store ID="storeTraining" runat="server" OnReadData="BindTrainingEventsStore"
                                    PageSize="20">
                                    <Model>
                                        <ext:Model ID="modelTraining" runat="server" IDProperty="TrainingID">
                                            <Fields>
                                                <ext:ModelField Name="TrainingID" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                                <ext:ModelField Name="FromDate" Type="String" />
                                                <ext:ModelField Name="ToDate" Type="String" />
                                                <ext:ModelField Name="Days" Type="String" />
                                                <ext:ModelField Name="TrainingType" Type="String" />
                                                <ext:ModelField Name="Host" Type="String" />
                                                <ext:ModelField Name="ResourcePerson" Type="String" />
                                                <ext:ModelField Name="Venue" Type="String" />
                                                <ext:ModelField Name="AvailableSeats" Type="String" />
                                                <ext:ModelField Name="BookedSeats" Type="String" />
                                                <ext:ModelField Name="LeftSeats" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                                <ext:GridPanel ID="gridTraining" runat="server" StoreID="storeTraining" MinHeight="0"
                                    Width="1253">
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="colName" Width="200" runat="server" Text="Training Name" DataIndex="Name"
                                                MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:DateColumn ID="colFromDate" Width="95" runat="server" Text="From Date" DataIndex="FromDate"
                                                Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                            </ext:DateColumn>
                                            <ext:DateColumn ID="DateColumn1" Width="95" runat="server" Text="To Date" DataIndex="ToDate"
                                                Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                            </ext:DateColumn>
                                            <ext:Column ID="Column1" Width="80" runat="server" Text="Type" DataIndex="TrainingType"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column2" Width="160" runat="server" Text="Host" DataIndex="Host"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column3" Width="203" runat="server" Text="Resource Person" DataIndex="ResourcePerson"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column4" Width="130" runat="server" Text="Venue" DataIndex="Venue"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column5" Width="70" runat="server" Text="Capacity" DataIndex="AvailableSeats"
                                                Sortable="false" MenuDisabled="true" Align="Center">
                                            </ext:Column>
                                            <ext:Column ID="Column6" Width="75" runat="server" Text="Enrollment" DataIndex="BookedSeats"
                                                Sortable="false" MenuDisabled="true" Align="Center">
                                            </ext:Column>
                                            <ext:Column ID="Column7" Width="70" runat="server" Text="Available" DataIndex="LeftSeats"
                                                Sortable="false" MenuDisabled="true" Align="Center">
                                            </ext:Column>
                                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" MenuDisabled="true"
                                                Sortable="false">
                                                <Commands>
                                                    <ext:GridCommand Text="Action">
                                                        <Menu EnableScrolling="false">
                                                            <Items>
                                                                <ext:MenuCommand Text="Edit" CommandName="Edit" />
                                                                <ext:MenuCommand Text="Delete" CommandName="Delete" />
                                                                <ext:MenuCommand Text="Publish" CommandName="Publish" />
                                                                <ext:MenuCommand Text="Unpublish" CommandName="Unpublish" />
                                                            </Items>
                                                        </Menu>
                                                    </ext:GridCommand>
                                                </Commands>
                                                <Listeners>
                                                    <Command Handler="CommandHandler(command, record);" />
                                                </Listeners>
                                            </ext:CommandColumn>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                    </SelectionModel>
                                    <BottomBar>
                                        <ext:PagingToolbar ID="paging1" runat="server">
                                        </ext:PagingToolbar>
                                    </BottomBar>
                                </ext:GridPanel>
                            </Content>
                        </ext:Panel>
                        <ext:Panel ID="Panel2" runat="server" Title="Event History">
                            <Content>
                                <br />
                                <table>
                                    <tr>
                                        <td style="width: 140px;">
                                            <ext:DateField ID="txtDateFilterFrom" FieldLabel="From" LabelSeparator="" LabelAlign="Top"
                                                runat="server" Width="120">
                                            </ext:DateField>
                                        </td>
                                        <td style="width: 140px;">
                                            <ext:DateField ID="txtDateFilterTo" FieldLabel="To" LabelSeparator="" LabelAlign="Top"
                                                runat="server" Width="120">
                                            </ext:DateField>
                                        </td>
                                        <td style="padding-top: 15px;">
                                            <ext:Button ID="btnLoad" runat="server" Cls="btn btn-primary" Text="Load">
                                                <DirectEvents>
                                                    <Click OnEvent="btnLoad_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <ext:Store ID="storeTrainingHistory" runat="server" OnReadData="BindTrainingEventsHistoryStore"
                                    PageSize="20">
                                    <Model>
                                        <ext:Model ID="model1" runat="server" IDProperty="TrainingID">
                                            <Fields>
                                                <ext:ModelField Name="TrainingID" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                                <ext:ModelField Name="FromDate" Type="String" />
                                                <ext:ModelField Name="ToDate" Type="String" />
                                                <ext:ModelField Name="Days" Type="String" />
                                                <ext:ModelField Name="TrainingType" Type="String" />
                                                <ext:ModelField Name="Host" Type="String" />
                                                <ext:ModelField Name="ResourcePerson" Type="String" />
                                                <ext:ModelField Name="Venue" Type="String" />
                                                <ext:ModelField Name="AvailableSeats" Type="String" />
                                                <ext:ModelField Name="BookedSeats" Type="String" />
                                                <ext:ModelField Name="LeftSeats" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                                <ext:GridPanel ID="gridTrainingHistory" runat="server" StoreID="storeTrainingHistory"
                                    MinHeight="0" Width="1253">
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column8" Width="200" runat="server" Text="Training Name" DataIndex="Name"
                                                MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:DateColumn ID="DateColumn2" Width="95" runat="server" Text="From Date" DataIndex="FromDate"
                                                Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                            </ext:DateColumn>
                                            <ext:DateColumn ID="DateColumn3" Width="95" runat="server" Text="To Date" DataIndex="ToDate"
                                                Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                            </ext:DateColumn>
                                            <ext:Column ID="Column9" Width="80" runat="server" Text="Type" DataIndex="TrainingType"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column10" Width="160" runat="server" Text="Host" DataIndex="Host"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column11" Width="203" runat="server" Text="Resource Person" DataIndex="ResourcePerson"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column12" Width="130" runat="server" Text="Venue" DataIndex="Venue"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column13" Width="70" runat="server" Text="Capacity" DataIndex="AvailableSeats"
                                                Sortable="false" MenuDisabled="true" Align="Center">
                                            </ext:Column>
                                            <ext:Column ID="Column14" Width="75" runat="server" Text="Enrollment" DataIndex="BookedSeats"
                                                Sortable="false" MenuDisabled="true" Align="Center">
                                            </ext:Column>
                                            <ext:Column ID="Column15" Width="70" runat="server" Text="Available" DataIndex="LeftSeats"
                                                Sortable="false" MenuDisabled="true" Align="Center">
                                            </ext:Column>
                                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="80" MenuDisabled="true"
                                                Sortable="false">
                                                <Commands>
                                                    <ext:GridCommand Text="Action">
                                                        <Menu EnableScrolling="false">
                                                            <Items>
                                                                <ext:MenuCommand Text="Edit" CommandName="Edit" />
                                                                <ext:MenuCommand Text="Delete" CommandName="Delete" />
                                                                <ext:MenuCommand Text="Publish" CommandName="Publish" />
                                                                <ext:MenuCommand Text="Unpublish" CommandName="Unpublish" />
                                                            </Items>
                                                        </Menu>
                                                    </ext:GridCommand>
                                                </Commands>
                                                <Listeners>
                                                    <Command Handler="CommandHandler(command, record);" />
                                                </Listeners>
                                            </ext:CommandColumn>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                    </SelectionModel>
                                    <BottomBar>
                                        <ext:PagingToolbar ID="PagingToolbar1" runat="server">
                                        </ext:PagingToolbar>
                                    </BottomBar>
                                </ext:GridPanel>
                                <br />
                                <div style="float: right;">
                                    <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                                        Cls="btn btn-primary" Text="<i></i>Export">
                                    </ext:Button>
                                    <br />
                            </Content>
                        </ext:Panel>
                        <ext:Panel ID="Panel3" runat="server" Title="Drafts">
                            <Content>
                                <br />
                                <ext:Store ID="storeUnpublishedTrainings" runat="server" OnReadData="BindTrainingUnpublishedStore"
                                    PageSize="20">
                                    <Model>
                                        <ext:Model ID="model2" runat="server" IDProperty="TrainingID">
                                            <Fields>
                                                <ext:ModelField Name="TrainingID" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                                <ext:ModelField Name="FromDate" Type="String" />
                                                <ext:ModelField Name="ToDate" Type="String" />
                                                <ext:ModelField Name="Days" Type="String" />
                                                <ext:ModelField Name="TrainingType" Type="String" />
                                                <ext:ModelField Name="Host" Type="String" />
                                                <ext:ModelField Name="ResourcePerson" Type="String" />
                                                <ext:ModelField Name="Venue" Type="String" />
                                                <ext:ModelField Name="AvailableSeats" Type="String" />
                                                <ext:ModelField Name="BookedSeats" Type="String" />
                                                <ext:ModelField Name="LeftSeats" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                                <ext:GridPanel ID="gridTrainingUnpublished" runat="server" StoreID="storeUnpublishedTrainings"
                                    MinHeight="0" Width="1253">
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column16" Width="200" runat="server" Text="Training Name" DataIndex="Name"
                                                MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:DateColumn ID="DateColumn4" Width="95" runat="server" Text="From Date" DataIndex="FromDate"
                                                Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                            </ext:DateColumn>
                                            <ext:DateColumn ID="DateColumn5" Width="95" runat="server" Text="To Date" DataIndex="ToDate"
                                                Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                                            </ext:DateColumn>
                                            <ext:Column ID="Column17" Width="80" runat="server" Text="Type" DataIndex="TrainingType"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column18" Width="160" runat="server" Text="Host" DataIndex="Host"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column19" Width="203" runat="server" Text="Resource Person" DataIndex="ResourcePerson"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column20" Width="130" runat="server" Text="Venue" DataIndex="Venue"
                                                Sortable="false" MenuDisabled="true" Align="Left">
                                            </ext:Column>
                                            <ext:Column ID="Column21" Width="70" runat="server" Text="Capacity" DataIndex="AvailableSeats"
                                                Sortable="false" MenuDisabled="true" Align="Center">
                                            </ext:Column>
                                            <ext:Column ID="Column22" Width="75" runat="server" Text="Enrollment" DataIndex="BookedSeats"
                                                Sortable="false" MenuDisabled="true" Align="Center">
                                            </ext:Column>
                                            <ext:Column ID="Column23" Width="70" runat="server" Text="Available" DataIndex="LeftSeats"
                                                Sortable="false" MenuDisabled="true" Align="Center">
                                            </ext:Column>
                                            <ext:CommandColumn ID="CommandColumn3" runat="server" Width="80" MenuDisabled="true"
                                                Sortable="false">
                                                <Commands>
                                                    <ext:GridCommand Text="Action">
                                                        <Menu EnableScrolling="false">
                                                            <Items>
                                                                <ext:MenuCommand Text="Edit" CommandName="Edit" />
                                                                <ext:MenuCommand Text="Delete" CommandName="Delete" />
                                                                <ext:MenuCommand Text="Publish" CommandName="Publish" />
                                                                <ext:MenuCommand Text="Unpublish" CommandName="Unpublish" />
                                                            </Items>
                                                        </Menu>
                                                    </ext:GridCommand>
                                                </Commands>
                                                <Listeners>
                                                    <Command Handler="CommandHandler(command, record);" />
                                                </Listeners>
                                            </ext:CommandColumn>
                                        </Columns>
                                    </ColumnModel>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
                                    </SelectionModel>
                                    <BottomBar>
                                        <ext:PagingToolbar ID="PagingToolbar2" runat="server">
                                        </ext:PagingToolbar>
                                    </BottomBar>
                                </ext:GridPanel>
                            </Content>
                        </ext:Panel>
                    </Items>
                </ext:TabPanel>
            </div>
            <div class="colRight">
            </div>
        </div>
    </div>
    <ext:Window ID="windowRP" runat="server" Title="Add Resource Person" Icon="Application"
        AutoScroll="true" Height="200" Width="400" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table>
                <tr>
                    <td style="padding-left: 10px">
                        <ext:TextField ID="txtRPName" runat="server" Width="200" LabelAlign="Top" FieldLabel="Resource Person"
                            LabelSeparator="" />
                    </td>
                </tr>
            </table>
            <table class="fieldTable">
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="Button2" Cls="btn btn-primary" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveRP_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <%-- <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                    </Click>
                                </Listeners>--%>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or
                            </div>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton3" Cls="btnFlatLeftGap"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{windowRP}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
    <ext:Window ID="windowTraining" runat="server" Title="Add/Edit Training" Icon="Application"
        ButtonAlign="Left" AutoScroll="true" Height="620" Width="900" BodyPadding="5"
        Hidden="true" Modal="false">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtTrainingName" runat="server" FieldLabel="Training name *" LabelAlign="Top"
                            Height="58" Width="350" LabelSeparator="" />
                        <asp:RequiredFieldValidator Display="None" ID="rfvTrainingName" runat="server" ValidationGroup="SaveUpdate"
                            ControlToValidate="txtTrainingName" ErrorMessage="Training Name is required." />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul class="itemList">
                            <li>
                                <ext:DateField Format="yyyy/MM/dd" LabelSeparator="" StyleSpec="200" FieldLabel="Starts on" runat="server"
                                    LabelAlign="Top" ID="txtStartDate" Width="110">
                                     <Plugins>
                                        <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                    <Listeners>
                                        <Change Handler="datechange();" />
                                    </Listeners>
                                </ext:DateField>
                                <asp:RequiredFieldValidator Display="None" ID="rfvStartDate" runat="server" ValidationGroup="SaveUpdate"
                                    ControlToValidate="txtStartDate" ErrorMessage="From date is required." />
                            </li>
                            <li>
                            <li>
                                <ext:DateField Format="yyyy/MM/dd" FieldLabel="Completes on" StyleSpec="margin-left:10px" LabelSeparator=""
                                    LabelAlign="Top" runat="server" ID="txtEndDate" Width="100">
                                     <Plugins>
                                        <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                    </Plugins>
                                    <Listeners>
                                        <Change Handler="datechange();" />
                                    </Listeners>
                                    <DirectEvents>
                                    </DirectEvents>
                                </ext:DateField>
                                <asp:RequiredFieldValidator Display="None" ID="rfvEndDate" runat="server" ValidationGroup="SaveUpdate"
                                    ControlToValidate="txtEndDate" ErrorMessage="End date is required." />
                            </li>
                            <%-- <li style="width: 20px; padding-left: 10px">
                                    <ext:DisplayField ID="dispInv3" runat="server" StyleSpec="margin-left:2px" />
                                    <ext:DisplayField ID="lblTotalDays" runat="server" />
                                </li>--%>
                            <%--<li style="width: 50px;">
                                    <ext:DisplayField ID="DisplayField1" runat="server" />
                                    <ext:DisplayField ID="dispDays" runat="server" Text="day" StyleSpec="margin-left:10px" />
                                </li>--%>
                            <li>
                                <ext:Button ID="Button1" runat="server" StyleSpec="margin-top:20px;margin-left:20px"
                                    Cls="btn btn-save" Height="30" Text="Load" Width="65">
                                    <DirectEvents>
                                        <Click OnEvent="btnFill_Click">
                                            <EventMask ShowMask="true" />
                                            <ExtraParams>
                                                <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridAttRequest}.getRowsValues({ selectedOnly: false }))"
                                                    Mode="Raw" />
                                            </ExtraParams>
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                            </li>
                            <%-- <li style="width: 100px;">
                                <ext:TimeField ID="tfStartTime" runat="server" MinTime="9:00" MaxTime="18:00" Increment="60"
                                    Width="90" SelectedTime="09:00" Format="hh:mm tt" FieldLabel="Time" LabelAlign="Top"
                                    LabelSeparator="">
                                    <Listeners>
                                        <Change Handler="CalculateEffectiveHour();" />
                                    </Listeners>
                                </ext:TimeField>
                            </li>--%>
                            <%--  <li style="width: 20px;">
                                <ext:DisplayField ID="dispInv4" runat="server" />
                                to </li>--%>
                            <%--  <li style="width: 95px;">
                                <ext:DisplayField ID="dispInv5" runat="server" />
                                <ext:TimeField ID="tfEndTime" runat="server" MinTime="9:00" MaxTime="18:00" Increment="60"
                                    Width="90" SelectedTime="09:00" Format="hh:mm tt" FieldLabel="" LabelAlign="Top">
                                    <Listeners>
                                        <Change Handler="CalculateEffectiveHour();" />
                                    </Listeners>
                                </ext:TimeField>
                            </li>--%>
                            <%-- <li style="width: 20px;">
                                <ext:DisplayField ID="dispInv6" runat="server" />
                                <ext:DisplayField ID="lblTotalHours" runat="server" />
                            </li>
                            <li>
                                <ext:DisplayField ID="DisplayField2" runat="server" />
                                <ext:DisplayField ID="DisplayField3" runat="server" Text="Hour" />
                            </li>--%>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul class="itemList">
                            <li>
                                <ext:ComboBox ID="cmbTrainingType" runat="server" Width="220" LabelWidth="110" LabelAlign="Top"
                                    FieldLabel="Type *" LabelSeparator="" QueryMode="Local" ValueField="TrainingTypeId"
                        DisplayField="TrainingTypeName" ForceSelection="true">
                                    <%--<Items>
                                        <ext:ListItem Text="Internal" Value="-1" />
                                        <ext:ListItem Text="External" Value="-2" />
                                    </Items>--%>
                                     <Store>
                                        <ext:Store ID="Store3" runat="server">
                                            <Model>
                                                <ext:Model ID="Model6" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="TrainingTypeId" Type="String" />
                                                        <ext:ModelField Name="TrainingTypeName" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="SaveUpdate" ControlToValidate="cmbTrainingType" ErrorMessage="Type is required." />
                            </li>
                            <li>
                                <ext:TextField ID="txtSeats" runat="server" Width="100" LabelAlign="Top" FieldLabel="Seats *"
                                    StyleSpec="margin-left:20px" LabelWidth="100" LabelSeparator="" MaskRe="[\-0123456789\,]" />
                                <asp:RequiredFieldValidator Display="None" ID="rfvSeats" runat="server" ValidationGroup="SaveUpdate"
                                    ControlToValidate="txtSeats" ErrorMessage="Seats is required." />
                            </li>
                            <li>
                                <%--<ext:ComboBox ID="cmbLevel" runat="server" ValueField="LevelId" DisplayField="Name"
                                    FieldLabel="Available to" LabelAlign="top" LabelSeparator="" ForceSelection="true" Width="208"
                                    QueryMode="Local">                     
                                      <Store>
                                        <ext:Store ID="Store3" runat="server">
                                            <Model>
                                                <ext:Model ID="Model3" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelId" Type="String" />
                                                        <ext:ModelField Name="Name" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                </ext:ComboBox>--%>
                                <ext:MultiCombo Width="208" LabelSeparator="" FieldLabel="Available to" LabelAlign="top"
                                    Hidden="true" ID="mcmbLevel" DisplayField="Name" ValueField="LevelId" runat="server">
                                    <Store>
                                        <ext:Store ID="Store2" runat="server">
                                            <Model>
                                                <ext:Model ID="Model4" IDProperty="LevelId" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="LevelId" Type="Int" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Listeners>
                                    </Listeners>
                                </ext:MultiCombo>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextField ID="txtVenue" runat="server" Width="350" LabelAlign="Top" FieldLabel="Training venue"
                            LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <ext:ComboBox ID="cmbCountry" runat="server" ForceSelection="true" Width="150" FieldLabel="Country" LabelAlign="Top"
                            LabelSeparator="" QueryMode="Local" DisplayField="CountryName" ValueField="CountryId">
                            <Store>
                                <ext:Store ID="store1" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="CountryId" Type="String" />
                                        <ext:ModelField Name="CountryName" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <%-- <ext:TextField ID="txtCountry" runat="server" FieldLabel="Country" LabelAlign="Top"
                            Width="150" LabelSeparator="" />--%>
                    </td>
                    <td>
                        <ext:TextField ID="txtCity" runat="server" FieldLabel="City" LabelAlign="Top" LabelSeparator=""
                            Width="190px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextField ID="txtHost" runat="server" FieldLabel="Training provider" LabelAlign="Top"
                            LabelSeparator="" Width="350" />
                    </td>
                </tr>
                 <tr>
                    <td colspan="2">
                        <ext:TextArea Height="55" ID="txtNotes" runat="server" FieldLabel="Notes" LabelAlign="Top"
                            LabelSeparator="" Width="350" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextField ID="txtResourcePerson" runat="server" FieldLabel="Resource Person"
                            Hidden="true" LabelWidth="101" LabelAlign="Top" LabelSeparator="" Width="540" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:DisplayField ID="lblRP" runat="server" Text="Resource Person : " Hidden="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:DisplayField ID="dispRP" runat="server" Text="" Hidden="true" />
                    </td>
                </tr>
            </table>
            <table class="fieldTable">
                <tr>
                    <td style="width: 250px;">
                        <ext:DisplayField ID="dispTrainingRequestCount" runat="server" Hidden="true" />
                    </td>
                    <td style="width: 250px;">
                        <ext:DisplayField ID="dispTrainingApprovedCount" runat="server" Hidden="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:Checkbox ID="cbChangeNotification" BoxLabel="Send Change Notification" BoxLabelAlign="After"
                            LabelWidth="150" Hidden="true" LabelSeparator="" runat="server" />
                    </td>
                    <td style="display:none">
                        <ext:Checkbox  ID="cbSendEmail" BoxLabel="Send Email" LabelWidth="80" LabelSeparator=""
                            BoxLabelAlign="After" Hidden="true" runat="server" />
                    </td>
                </tr>
            </table>
            <table class="tblDetails">
                <tr>
                    <td>
                        <ext:GridPanel StyleSpec="margin-top:5px;padding-left:10px; border-color: transparent;"
                            ID="gridAttRequest" MinHeight="150" runat="server" Cls="itemgrid" Width="840">
                            <Store>
                                <ext:Store ID="StoreTrainingLine" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LineID" />
                                                <ext:ModelField Name="DateEng" Type="Date" />
                                                <ext:ModelField Name="Day" Type="string" />
                                                <ext:ModelField Name="InTimeDT" Type="Date" />
                                                <ext:ModelField Name="OutTimeDT" Type="Date" />
                                                <ext:ModelField Name="Hours" />
                                                <ext:ModelField Name="IsTraining" />
                                                <ext:ModelField Name="ResourcePerson" Type="string" IsComplex="true" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                    <ext:DateColumn ID="colDate" runat="server" Align="Left" Text="Date" Width="80" MenuDisabled="true"
                                        Sortable="false" Format="yyyy/MM/dd" DataIndex="DateEng">
                                    </ext:DateColumn>
                                    <ext:Column ID="Column24" Sortable="false" MenuDisabled="true" runat="server" Text="Day"
                                        Width="90" Align="Left" DataIndex="Day">
                                    </ext:Column>
                                    <%--  <ext:CheckColumn ID="CheckColumn1" runat="server" Text="" DataIndex="IsTraining" Hidden="true"
                                        StopSelection="false" Editable="true" Width="50">
                                    </ext:CheckColumn>--%>
                                    <ext:Column ID="asdfas" runat="server" Text="" Sortable="false" MenuDisabled="true"
                                        Width="50" DataIndex="IsTraining" Border="false">
                                        <Editor>
                                            <ext:TextField ID="asd" runat="server">
                                            </ext:TextField>
                                        </Editor>
                                        <Renderer Fn="IsTrainingRender">
                                        </Renderer>
                                    </ext:Column>
                                    <ext:DateColumn ID="colInTime1" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="Start" Align="Left" Width="95" DataIndex="InTimeDT" Format="hh:mm tt">
                                        <Editor>
                                            <ext:TimeField ID="TimeField1" runat="server" SelectedTime="09:00" MinTime="00:00"
                                                MaxTime="23:59" Increment="1" Format="hh:mm tt">
                                            </ext:TimeField>
                                        </Editor>
                                        <%--<Renderer Fn="RenderColor"></Renderer>--%>
                                    </ext:DateColumn>
                                    <ext:DateColumn ID="colOutTime1" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="End" Align="Left" Width="95" DataIndex="OutTimeDT" Format="hh:mm tt">
                                        <Editor>
                                            <ext:TimeField ID="TimeField2" runat="server" SelectedTime="17:00" MinTime="00:00"
                                                MaxTime="23:59" Increment="1" Format="hh:mm tt">
                                            </ext:TimeField>
                                        </Editor>
                                    </ext:DateColumn>
                                    <ext:Column ID="colWorkHours" Sortable="false" MenuDisabled="true" runat="server"
                                        Text="Hours" Align="Left" Width="100" DataIndex="Hours">
                                        <Renderer Fn="CalculateWorkHours" />
                                    </ext:Column>
                                    <ext:TagColumn ID="TagColumn1" runat="server" Text="Resource Person(s)" DataIndex="ResourcePerson"
                                        Flex="1">
                                        <Editor>
                                            <ext:TagField ID="TagField1" QueryMode="Local" MinChars="2" StoreID="storeResoucrePerson"
                                                DisplayField="Name" ValueField="ResourcePersonId" runat="server" Width="550"
                                                ForceSelection="true"  LabelSeparator="" LabelAlign="Top">
                                                <ListConfig ID="ctlListwwwConfig">
                                                    <ItemTpl ID="ItemTpl1" runat="server">
                                                        <Html>
                                                            <tpl if="Name==''">
							                                    <div class="button"><a style='display:block' href="javascript:void(0)" class="button add" onclick="showAddNewRP();">Add New</a></div>
						                                    </tpl>
                                                            <tpl if="Name!=''">
                                                                {Name}
                                                            </tpl>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Listeners>
                                                    <%--#{TagField1}.removeTag(#{TagField1}.value);--%>
                                                    <BeforeQuery Handler="var q = queryEvent.query;if(q=='') return;
                                                queryEvent.query = new RegExp(q, 'ig');
                                                queryEvent.query.length = q.length;" />
                                                    <BeforeSelect Handler="return record.data.ResourcePersonId != 0;">
                                                    </BeforeSelect>
                                                    <Focus Handler="this.lastQuery = '';this.store.clearFilter();this.clearValue();" />
                                                    <Blur Handler="if(this.getStore().getById(this.getValue())==null) this.clearValue(); " />
                                                    <AfterRender Handler="this.getTrigger(0).show();this.store.clearFilter();" />
                                                    <Select Handler="if( this.getValue()=='') this.clearValue();" />
                                                </Listeners>
                                            </ext:TagField>
                                        </Editor>
                                        <Renderer Fn="renderResourcePersons">
                                        </Renderer>
                                    </ext:TagColumn>
                                    <ext:CommandColumn ID="CommandColumn4" runat="server" Width="60" Text="" Align="Center">
                                        <Commands>
                                            <ext:CommandSeparator />
                                            <ext:GridCommand Cls="CommandCls" ToolTip-Text="Apply same" Text="" CommandName="Clear"
                                                Icon="TextDirection" />
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="CommandHandlerClear(command,record);" />
                                        </Listeners>
                                    </ext:CommandColumn>
                                    <%--<ext:CommandColumn ID="CommandColumn5" runat="server" Sortable="false" MenuDisabled="true"
                                        Width="50" Align="Center">
                                        <Commands>
                                            <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                        </Commands>
                                        <Listeners>
                                            <Command Fn="RemoveItemLine">
                                            </Command>
                                        </Listeners>
                                    </ext:CommandColumn>--%>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:CellSelectionModel EnableKeyNav="true" runat="server" ID="CellSelectionModel_ItemGrid">
                                    <Listeners>
                                        <SelectionChange Fn="cellSelectionChanged" />
                                    </Listeners>
                                </ext:CellSelectionModel>
                            </SelectionModel>
                            <Plugins>
                                <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                                    <Listeners>
                                        <BeforeEdit Fn="beforeEdit">
                                        </BeforeEdit>
                                        <Edit Fn="afterEdit">
                                        </Edit>
                                    </Listeners>
                                </ext:CellEditing>
                            </Plugins>
                            <DockedItems>
                                <ext:FieldContainer ID="Container1" runat="server" Dock="Bottom" Layout="HBoxLayout"
                                    StyleSpec="margin-top:2px;">
                                    <Defaults>
                                        <ext:Parameter Name="height" Value="32" />
                                    </Defaults>
                                    <Items>
                                        <ext:DisplayField ID="lblDaysTotal" Cls="total-field" runat="server" Width="80" />
                                        <ext:DisplayField ID="DisplayField1" Text="" Cls="total-field" runat="server" Width="90" />
                                        <ext:DisplayField ID="DisplayField2" Text="" Cls="total-field" runat="server" Width="50" />
                                        <ext:DisplayField ID="DisplayField3" Text="" Cls="total-field" runat="server" Width="95" />
                                        <ext:DisplayField ID="DisplayField4" Text="" Cls="total-field" runat="server" Width="95" />
                                        <ext:DisplayField ID="lblTotalHours" Cls="total-field" runat="server" Width="100" />
                                        <ext:DisplayField ID="lblTotalRP" Cls="total-field" runat="server" Width="550" />
                                    </Items>
                                </ext:FieldContainer>
                            </DockedItems>
                            <View>
                                <ext:GridView ID="GridView2" runat="server" StripeRows="false">
                                    <GetRowClass Fn="getRowClass" />
                                </ext:GridView>
                            </View>
                        </ext:GridPanel>
                        <div style="border-bottom: 1px solid #cecece">
                        </div>
                        <%--  <ext:ToolTip ID="ToolTip1" runat="server" Target="={#{gridAttRequest}.getView().el}"
                            Delegate=".x-grid-cell" TrackMouse="true">
                            <Listeners>
                                <Show Handler="onShow(this, #{gridAttRequest});" />
                            </Listeners>
                        </ext:ToolTip>--%>
                    </td>
                </tr>
            </table>
            </div>
        </Content>
        <Buttons>
            <ext:Button runat="server" ID="btnSave1" Cls="btn btn-primary" Text="<i></i>Save">
                <DirectEvents>
                    <Click OnEvent="btnSave_Click">
                        <EventMask ShowMask="true" />
                        <ExtraParams>
                            <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridAttRequest}.getRowsValues({ selectedOnly: false }))"
                                Mode="Raw" />
                        </ExtraParams>
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btnFlatLeftGap"
                Text="<i></i>Cancel">
                <Listeners>
                    <Click Handler="#{windowTraining}.hide();">
                    </Click>
                </Listeners>
            </ext:LinkButton>
        </Buttons>
    </ext:Window>
    <ext:Window ID="WUnPublish" runat="server" Title="Unpublish Training" Icon="Application"
        Height="230" Width="300" BodyPadding="5" Hidden="true" Modal="false">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:DisplayField ID="dispUnpubilshTR" runat="server" Hidden="true" />
                    </td>
                    <td>
                        <ext:DisplayField ID="dispUnpublishTA" runat="server" Hidden="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:Checkbox ID="cbUnpublishSendNotific" BoxLabel="Send Change Notification" BoxLabelAlign="After"
                            LabelWidth="150" Hidden="true" LabelSeparator="" runat="server" />
                    </td>
                </tr>
                <tr style="display:none" >
                    <td colspan="2">
                        <ext:Checkbox ID="cbUnpublishSendEmail" BoxLabel="Send Email" LabelWidth="80" LabelSeparator=""
                            BoxLabelAlign="After" Hidden="true" runat="server" />
                    </td>
                </tr>
            </table>
            <table class="fieldTable">
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnUnpubl" Cls="btn btn-primary" Text="<i></i>Unpublish">
                                <DirectEvents>
                                    <Click OnEvent="btnUnpubl_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or
                            </div>
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                                Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{WUnPublish}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
    <script type="text/javascript">

        function reloadCalendar() {
            <%=CalendarPanel1.ClientID %>.getView().reloadStore();
        }
       
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
