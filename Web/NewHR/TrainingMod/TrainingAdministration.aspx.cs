﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils.Calendar;

namespace Web.NewHR.TrainingMod
{
    public partial class TrainingAdministration : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            ShowEvents();
            BindLastTenTrainingRequests();
            BindApprovedTrainingRequests();
            BindAllTrainingRequestHistory();
            BindHouseFullTrainingRequests();
            BindWaitingTrainingRequest();
            BindTrainingCombo();

            if (X.IsAjaxRequest)
            {
                Panel3.Render();
                Panel4.Render();
                Panel5.Render();
                Panel6.Render();
                Panel7.Render();                
            }

        }

        private void BindTrainingCombo()
        {
            List<Training> _list = TrainingManager.GetAllTrainings();
            cmbTraining.GetStore().DataSource = _list;
            cmbTraining.GetStore().DataBind();
        }

        private void ReloadSeatsGrids(int trainingId)
        {
            BindSeatsGrids(trainingId);
            BindTrainingReqSeatsAllocated(trainingId);
            BindTrainingRequestPending(trainingId);
            Panel1.Render();
            Panel2.Render();
        }

        private void ShowEvents()
        {
            List<Training> list = new List<Training>();
            int listCount = 0;
            int day = 0;
            int month = System.DateTime.Now.Month;
            int year = System.DateTime.Now.Year;
            int nextMonth = 0, nextYear = 0;

            dispThisMonthTitle.Html = "";
          //  dispThisMonthDetails.Html = "";
            dispNearFutureDetails.Html = "";
            dispNextMonthTitle.Html = "";
            dispNextMonthDetails.Html = "";
            dispTrainingReqPart.Html = "";

            string monthName = DateHelper.GetMonthName(month, true);

            //current month
            list = TrainingManager.GetPublishedTrainingByMonth(month, year).Take(5).ToList();
            listCount = list.Count;

            //dispThisMonthTitle.Html = monthName + " " + year.ToString() + "{Current Month}";
            dispThisMonthTitle.Html = "This Month - " + monthName + " " + year.ToString();

            //foreach (var item in list)
            //{
                //dispThisMonthDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "    " : "  (" + item.LeftSeats.ToString() + ")    ") + "<a class='clsCursorHand' onclick='return DetailsClick(" + item.TrainingID.ToString() + ");'>Details</a>" + "<br>";
           // }

            List<TrainingTitleBO> _listTitles = new List<TrainingTitleBO>();
            foreach (var item in list)
            {

                TrainingTitleBO _objTrainingTitleBO = new TrainingTitleBO();
                _objTrainingTitleBO.Name = item.Name;
                _objTrainingTitleBO.DateInfo = DateHelper.GetMonthShortName(item.FromDate.Value.Month, true) + " " + item.FromDate.Value.Day + " - " + DateHelper.GetMonthShortName(item.ToDate.Value.Month, true) + " " + item.ToDate.Value.Day + ", " + year.ToString() + " : " + item.Days + " days ";


               

                // + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")");
                _objTrainingTitleBO.Host = item.Host;

                string Seats = (item.AvailableSeats == 0 ? "" : item.AvailableSeats.ToString());
                string BookedSeats =  (item.BookedSeats == 0 ? "0" : item.BookedSeats.ToString());
                string Avilable = (item.LeftSeats == 0 ? "0" : item.LeftSeats.ToString());

                //_objTrainingTitleBO.BookingInfo = Seats + "; " + BookedSeats + "; " + Avilable + " <a class='clsCursorHand' onclick='return DetailsClick(" + item.TrainingID.ToString() + ");'>Details</a>";
                _objTrainingTitleBO.Seats = Seats;
                _objTrainingTitleBO.Avilable = Avilable;
                _objTrainingTitleBO.Booked = BookedSeats;
                _objTrainingTitleBO.ViewLink = " <a  class='clsCursorHand' onclick='return DetailsClick(" + item.TrainingID.ToString() + ");'> View</a>";

                

                
                //dispThisMonthDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")") + "<br>";
                _listTitles.Add(_objTrainingTitleBO);
            }
            rptTrainingList.DataSource = _listTitles;
            rptTrainingList.DataBind();


            if (list.Count == 0)
                dispThisMonthTitle.Hidden = true;
            

            list.Clear();

            list = TrainingManager.GetLatestTrainingList();
            listCount = list.Count;
            foreach (var item in list)
            {
                dispNearFutureDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "    " : "  (" + item.LeftSeats.ToString() + ")    ") + "<a class='clsCursorHand' onclick='return DetailsClick(" + item.TrainingID.ToString() + ");'>Details</a>" + "<br>";
            }
            if (list.Count == 0)
                dispNearFuture.Hidden = true;

            //next month
            if (month == 12)
            {
                nextMonth = 1;
                nextYear = year + 1;
            }
            else
            {
                nextMonth = month + 1;
                nextYear = year + 1;
            }

            monthName = DateHelper.GetMonthName(nextMonth, true);

            list.Clear();
            list = TrainingManager.GetPublishedTrainingByMonth(nextMonth, year);
            listCount = list.Count;

            //dispNextMonthTitle.Html = monthName + " " + year.ToString() + "{Next Month}";

            dispNextMonthTitle.Html = "Next Month - " + monthName + " " + year.ToString();

            foreach (var item in list)
            {
                dispNextMonthDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "    " : "  (" + item.LeftSeats.ToString() + ")    ") + "<a class='clsCursorHand' onclick='return DetailsClick(" + item.TrainingID.ToString() + ");'>Details</a>" + "<br>";
            }

            if (list.Count == 0)
                dispNextMonthTitle.Hidden = true;

            //Training Participation Requests
            List<TrainingRequest> listTR = TrainingManager.GetTrainingRequestListWithEmpName();
            foreach (var item in listTR)
            {
                dispTrainingReqPart.Html += item.EmployeeName + ", " + item.Name + ", " + item.FromDate.Day + "-" + item.ToDate.Day + ", " + item.Days + " days, " + item.TrainingType + "<br>";
            }
        }
        protected void lnkRemoveRow_Click(object sender, DirectEventArgs e)
        {
            int? EmployeeID = null;
            if (!string.IsNullOrEmpty(hdnSelectedEmployee.Text))
                EmployeeID = int.Parse(hdnSelectedEmployee.Text);

            string entryline = e.ExtraParams["gridItems"];
            List<TrainingInviteEmpBo> entryLineObj = JSON.Deserialize<List<TrainingInviteEmpBo>>(entryline);
            entryLineObj = entryLineObj.Where(x => x.EmployeeId != EmployeeID.Value).ToList();
            storeEmpAssign.DataSource = entryLineObj;
            storeEmpAssign.DataBind();
        }

        protected void cmbTraining_Change(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbTraining.SelectedItem.Value))
            {
                BindLastTenTrainingRequests();
            }
        }

        private void BindLastTenTrainingRequests()
        {
            List<TrainingRequest> _listResult = TrainingManager.GetLatestTrainingRequestsAllEmp();

            if (!string.IsNullOrEmpty(cmbTraining.SelectedItem.Value))
            _listResult = _listResult.Where(x => x.TrainingID == int.Parse(cmbTraining.SelectedItem.Value)).ToList();

            gridTrainingRequestPart.GetStore().DataSource = _listResult;
            gridTrainingRequestPart.GetStore().DataBind();
        }

        private void BindSeatsGrids(int trainingId)
        {
            gridTotalSeats.GetStore().DataSource = TrainingManager.GetTrainingForSeatsByTrainingId(trainingId);
            gridTotalSeats.GetStore().DataBind();

            gridSeatsBooked.GetStore().DataSource = TrainingManager.GetTrainingForSeatsByTrainingId(trainingId);
            gridSeatsBooked.GetStore().DataBind();

            gridAvailabelSeats.GetStore().DataSource = TrainingManager.GetTrainingForSeatsByTrainingId(trainingId);
            gridAvailabelSeats.GetStore().DataBind();
        }

        private void BindTrainingReqSeatsAllocated(int trainingId)
        {
            gridTRSeatsAllocated.GetStore().DataSource = TrainingManager.GetApprovedTRByTrainingId(trainingId);
            gridTRSeatsAllocated.GetStore().DataBind();
        }

        private void BindTrainingRequestPending(int trainingId)
        {
            gridTRPendingRequest.GetStore().DataSource = TrainingManager.GetPendingTRByTrainingId(trainingId);
            gridTRPendingRequest.GetStore().DataBind();
        }

        private void BindApprovedTrainingRequests()
        {
            gridTRApproved.GetStore().DataSource = TrainingManager.GetTrainingRequestsByStatus((int)TrainingRequestStatusEnum.Approved);
            gridTRApproved.GetStore().DataBind();
        }

        private void BindAllTrainingRequestHistory()
        {
            DateTime? dateFilter = null;
            if (!string.IsNullOrEmpty(txtDateFilter.Text.Trim()))
                dateFilter = DateTime.Parse(txtDateFilter.Text.Trim());

            gridTRHistory.GetStore().DataSource = TrainingManager.GetAllTrainingRequestHistory(dateFilter);
            gridTRHistory.GetStore().DataBind();
        }

        private void BindHouseFullTrainingRequests()
        {
            gridTRHouseFull.GetStore().DataSource = TrainingManager.GetTrainingRequestsByStatus((int)TrainingRequestStatusEnum.HouseFull);
            gridTRHouseFull.GetStore().DataBind();
        }

        private void BindWaitingTrainingRequest()
        {
            gridTRWaiting.GetStore().DataSource = TrainingManager.GetTrainingRequestsByStatus((int)TrainingRequestStatusEnum.WaitingList);
            gridTRWaiting.GetStore().DataBind();
        }

        private string GetAppropriateTime(TimeSpan ts)
        {
            if (ts.TotalHours < 12)
                return ts.TotalHours.ToString() + " AM";
            else if (ts.TotalHours == 12)
                return ts.TotalHours.ToString() + " PM";
            else
                return (ts.TotalHours - 12).ToString() + " PM";
        }


        private void BindTrainingLines(int trainingId,string RP, out int  TotalTrainingDays)
        {
            List<TrainingPlanningLine> _listTrainingList = TrainingManager.GetTrainingLines(trainingId);
            List<TrainingPlanningLine> _listTrainingLineForTotal = _listTrainingList.Where(x => x.IsTraining == true).ToList();
            
            foreach (TrainingPlanningLine item in _listTrainingList)
            {
                if (item.StartTime != null)
                {
                    DateTime inTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, item.StartTime.Value.Seconds);
                    //  item.InTimeString = inTimeDateTime.ToString();
                    item.InTimeDT = inTimeDateTime;
                }

                if (item.EndTime != null)
                {
                    DateTime outTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.EndTime.Value.Hours, item.EndTime.Value.Minutes, item.EndTime.Value.Seconds);
                    //  item.OutTimeString = outTimeDateTime.ToString();
                    item.OutTimeDT = outTimeDateTime;
                }

                //Display RP

                if (!string.IsNullOrEmpty(item.ResourcePerson))
                {
                    List<string> RPIds = item.ResourcePerson.Split(',').Distinct().ToList();
                    string ResourcePersons = "";
                    //getting Resoucre Persons
                    foreach (string Id in RPIds)
                    {
                        string Name = TrainingManager.GetResourePersons().ToList().SingleOrDefault(x => x.ResourcePersonId == int.Parse(Id)).Name;
                        if (!string.IsNullOrEmpty(ResourcePersons))
                            ResourcePersons = ResourcePersons + "," + Name;
                        else
                            ResourcePersons = Name;
                    }
                    item.ResourcePerson = ResourcePersons;
                }

              
               

                //-----------------
            }

            int TotalHours = 0, TotalMins = 0;
            foreach (TrainingPlanningLine element in _listTrainingLineForTotal)
            {
                if (element.Hours != null)
                {
                    string[] str = element.Hours.Split(':').ToArray();
                    TotalHours = TotalHours + int.Parse(str[0]);
                    TotalMins = TotalMins + int.Parse(str[1]);
                }
            }

            if (_listTrainingLineForTotal.Count > 0)

                lblDaysTotal.Text = _listTrainingLineForTotal.Count() + " Days";
            if (RP != null)
                lblTotalRP.Text = RP.Split(',').ToList().Count() + " resource persons";

            string strTime = "";
            if (TotalHours == 0 && TotalMins != 0)
                strTime = TotalMins + " Mins";

            if (TotalMins == 0 && TotalHours != 0)
                strTime = TotalHours + " Hrs";

            if (TotalHours != 0 && TotalMins != 0)
                strTime = TotalHours + " Hrs  " + TotalMins + " Mins";

            lblTotalHours.Text = strTime;
            TotalTrainingDays = _listTrainingLineForTotal.Count;
            StoreTrainingLines.DataSource = _listTrainingList;
            StoreTrainingLines.DataBind();
        }

        protected void btnTrainngDetails_Click(object sender, DirectEventArgs e)
        {
            int trainingId = int.Parse(hdnTrainingId.Text);

            Training obj = TrainingManager.GetTrainingById(trainingId);
            if (obj != null)
            {
                dispTrainingName.Html = obj.Name;

                //dispTrainingDetails.Html = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day.ToString() + "-" + obj.ToDate.Value.Day.ToString() + "," + obj.FromDate.Value.Year.ToString() + " " + obj.Days.ToString() + " days from " + obj.StartTime + " to " + obj.EndTime;

                int TotalDay = 0;
                BindTrainingLines(trainingId, obj.ResourcePerson,out TotalDay);

               
                dispTrainingDetails.Html = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day.ToString() + "-" + obj.ToDate.Value.Day.ToString() + "," + obj.FromDate.Value.Year.ToString() + ", " + TotalDay.ToString() + " days"; //from " + GetAppropriateTime(obj.StartTime.Value) + " to " + GetAppropriateTime(obj.EndTime.Value);
                dispVenue.Html = obj.Venue;
                dispNotes.Html = obj.Notes;
                dispHostedBy.Html = obj.Host;
                dispResourcePerson.Html = obj.ResourcePerson;

                BindSeatsGrids(trainingId);

                BindTrainingReqSeatsAllocated(trainingId);
                BindTrainingRequestPending(trainingId);
                
            }
            windowTPR.Center();
            windowTPR.Show();
        }

        protected void btnApprove_Click(object sender, DirectEventArgs e)
        {
            int trainingRequestId = int.Parse(hdnTrainingRequestId.Text);
            int trainingId = int.Parse(hdnTrainingId.Text);

           

            if (!TrainingManager.IsTrainingSeatRemaining(trainingId))
            {
                NewMessage.ShowWarningMessage("No remaining seats for the training, so cannot be approved.");
                return;
            }

            Status status = TrainingManager.ApproveTrainingRequest(trainingRequestId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Training Request approved successfully.");                
                Initialise();
                ReloadSeatsGrids(trainingId);
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnHouseFull_Click(object sender, DirectEventArgs e)
        {
            int trainingRequestId = int.Parse(hdnTrainingRequestId.Text);
            int trainingId = int.Parse(hdnTrainingId.Text);
            Status status = TrainingManager.HousefullTrainingRequest(trainingRequestId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Training Request house full saved successfully.");
                Initialise();
                ReloadSeatsGrids(trainingId);
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnWaiting_Click(object sender, DirectEventArgs e)
        {
            int trainingRequestId = int.Parse(hdnTrainingRequestId.Text);
            int trainingId = int.Parse(hdnTrainingId.Text);
            Status status = TrainingManager.WaitingTrainingRequest(trainingRequestId);
            
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Training Request waiting saved successfully.");
                Initialise();
                ReloadSeatsGrids(trainingId);
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnAllocateSeats_Click(object sender, DirectEventArgs e)
        {
            int trainingId = int.Parse(hdnTrainingId.Text);

            cmbEmployee.Text = "";
            txtASNotes.Text = "";

            Training obj = TrainingManager.GetTrainingById(trainingId);
            if (obj != null)
            {
                dispASTrainingName.Html = obj.Name;
                
                //dispASTrainingDetails.Html = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day.ToString() + "-" + obj.ToDate.Value.Day.ToString() + "," + obj.FromDate.Value.Year.ToString() + obj.Days.ToString() + " days from " + obj.StartTime + " to " + obj.EndTime;
               // dispASTrainingDetails.Html = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day.ToString() + "-" + obj.ToDate.Value.Day.ToString() + "," + obj.FromDate.Value.Year.ToString() + ", " + obj.Days.ToString() + " days" ; //"from " + GetAppropriateTime(obj.StartTime.Value) + " to " + GetAppropriateTime(obj.EndTime.Value);

                dispASHostedBy.Html = obj.Host;
               // dispASResourcePerson.Html = obj.ResourcePerson;
            }
            BindTrainingRequestsEmployees(trainingId);
            windowAllocateSeat.Show();
        }


        protected void BindTrainingRequestsEmployees(int TrainingID)
        {
            List<TrainingRequest> _list = TrainingManager.GetTrainingAssigedEmpByTrainingId(TrainingID);
            List<TrainingInviteEmpBo> objList = new List<TrainingInviteEmpBo>();

            foreach (var item in _list)
            {
                TrainingInviteEmpBo obj = new TrainingInviteEmpBo();
                obj.Name = EmployeeManager.GetEmployeeById(item.EmployeeId.Value).Name;
                obj.EmployeeId = item.EmployeeId.Value;

                GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(obj.EmployeeId);
                if (currentBranchDep != null)
                {
                    obj.Branch = currentBranchDep.Branch;
                    obj.Department = currentBranchDep.Department;
                }
                
                objList.Add(obj);
            }
            storeEmpAssign.DataSource = objList;
            storeEmpAssign.DataBind();
        }

        protected void btnAdddEmployee_Click(object sender, DirectEventArgs e)
        {
            // List<LeaveApprovalEmployeeBO> _listEmploye = new List<LeaveApprovalEmployeeBO>();
            string entryline = e.ExtraParams["gridItems"];
            List<TrainingInviteEmpBo> entryLineObj = new List<TrainingInviteEmpBo>();
            if (!string.IsNullOrEmpty(entryline))
                entryLineObj = JSON.Deserialize<List<TrainingInviteEmpBo>>(entryline);

            GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(int.Parse(cmbEmployee.SelectedItem.Value));
            string Branch = "", Department = "";
            if (currentBranchDep != null)
            {
                Branch = currentBranchDep.Branch;
                Department = currentBranchDep.Department;
            }

            if (!string.IsNullOrEmpty(cmbEmployee.SelectedItem.Value))
            {
                TrainingInviteEmpBo _objEEmployee = new TrainingInviteEmpBo();
                _objEEmployee.EmployeeId = int.Parse(cmbEmployee.SelectedItem.Value);
                _objEEmployee.Name = cmbEmployee.SelectedItem.Text;
                _objEEmployee.Department = Department;
                _objEEmployee.Branch = Branch;
                entryLineObj.Add(_objEEmployee);
            }


            if (entryLineObj.Any())
            {
                storeEmpAssign.DataSource = entryLineObj;
                storeEmpAssign.DataBind();
            }
            cmbEmployee.Clear();
            
            Ext.Net.FieldTrigger trigger1 = new FieldTrigger();
            trigger1.Icon = TriggerIcon.Clear;
            trigger1.HideTrigger = true;
            cmbEmployee.Triggers.Add(trigger1);
        }
        
        protected void btnAllocateSeatSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveAS");
            if (Page.IsValid)
            {

                int trainingId = int.Parse(hdnTrainingId.Text);
                
                //if (!TrainingManager.IsTrainingSeatRemaining(trainingId)) 
                //{
                //    NewMessage.ShowWarningMessage("No remaining seats for the training, so cannot be assign.");
                //    return;
                //}


                  string entryline = e.ExtraParams["gridItems"];
                  List<TrainingInviteEmpBo> entryLineObj = new List<TrainingInviteEmpBo>();

                  if (!string.IsNullOrEmpty(entryline))
                      entryLineObj = JSON.Deserialize<List<TrainingInviteEmpBo>>(entryline);

                if(!entryLineObj.Any())
                {
                    NewMessage.ShowNormalMessage("Select employee to allocate seat");
                    return;
                }

                int AvilableSeat = TrainingManager.GetTrainingById(trainingId).AvailableSeats.Value;
                if (entryLineObj.Count>AvilableSeat)
                {
                    NewMessage.ShowWarningMessage("Total seats for this training is : " + AvilableSeat.ToString());
                    return;
                }

                List<TrainingRequest> objList = new List<TrainingRequest>();
                foreach (TrainingInviteEmpBo _item in entryLineObj)
                {
                    TrainingRequest obj = new TrainingRequest();
                    obj.TrainingID = trainingId;
                    obj.EmployeeId = _item.EmployeeId;
                    obj.RequestedDate = System.DateTime.Now;
                    obj.IsAssign = true;
                    //if (!string.IsNullOrEmpty(txtASNotes.Text.Trim()))
                    //    obj.Notes = txtASNotes.Text.Trim();
                    obj.Status = (int)TrainingRequestStatusEnum.Approved;
                    obj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                    obj.ApprovedOn = System.DateTime.Now;
                    objList.Add(obj);
                }

                Status status = TrainingManager.AssignEmployeeTrainingRequest(objList, trainingId, cbSendEmail.Checked);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Training request assigned successfully.");

                    Initialise();
                    ReloadSeatsGrids(trainingId);
                    windowAllocateSeat.Close();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(txtDateFilter.Text.Trim()))
                return;

            BindAllTrainingRequestHistory();
        }

        protected void BindTrainingReqOnRead(object sender, StoreReadDataEventArgs e)
        {
            BindLastTenTrainingRequests();
        }

        protected void BindTRApprovedOnReadData(object sender, StoreReadDataEventArgs e)
        {
            BindApprovedTrainingRequests();
        }

        protected void BindTRHouseFulOnRead(object sender, StoreReadDataEventArgs e)
        {
            BindHouseFullTrainingRequests();
        }

        protected void BindTRWainingOnRead(object sender, StoreReadDataEventArgs e)
        {
            BindWaitingTrainingRequest();
        }

        protected void BindTRHistoryOnRead(object sender, StoreReadDataEventArgs e)
        {
            BindAllTrainingRequestHistory();
        }

        protected void btnInviteEmp_Click(object sender, DirectEventArgs e)
        {
            int trainingId = int.Parse(hdnTrainingId.Text);

            int sentCount = 0;

            Status status = TrainingManager.InviteEmployee(trainingId, ref sentCount);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage(sentCount.ToString() + " employee invited for training participation.");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }
    }
}