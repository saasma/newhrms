﻿<%@ Page Title="Training Attendance" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="TrainingEmpAttendance.aspx.cs" Inherits="Web.NewHR.TrainingMod.TrainingEmpAttendance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

      var CommandHandler = function(command, record){
            <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeId);
            <%= hdnLineID.ClientID %>.setValue(record.data.LineID);

                if(command=="Save")
                {
                   <%= btnSaveLine.ClientID %>.fireEvent('click');
                }
                else if(command == "Reset")
                {
                 <%= btnResetAttLine.ClientID %>.fireEvent('click');
                 
                }
           }

       function searchList() {
            <%=gridEmpAttendance.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    </script>
   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Training Employee Attendance
                </h4>
            </div>
        </div>
    </div>
    <ext:LinkButton runat="server" Hidden="true" ID="btnSaveLine">
        <DirectEvents>
            <Click OnEvent="btnSaveLine_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the record?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:LinkButton runat="server" Hidden="true" ID="btnResetAttLine">
        <DirectEvents>
            <Click OnEvent="btnResetAttLine_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to reset the record?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="contentpanel">
        <ext:Hidden ID="hdnTrainingId" runat="server" />
        <ext:Hidden ID="hdnEmployeeId" runat="server" />
        <ext:Hidden ID="hdnLineID" runat="server" />
        <ext:Hidden ID="hdnStartTime" runat="server" />
        <ext:Hidden ID="hdnEndTime" runat="server" />
        <ext:Hidden ID="hdnDate" runat="server" />
        <ext:Hidden ID="hdnTotalDays" runat="server" />
    
        <table class="fieldTable" style="margin-top:0px">
            <tr>
                <td style="padding-left:0px">
                    <ext:ComboBox ID="cmbTraining" runat="server" Width="250" FieldLabel="Training" LabelAlign="Top"
                        LabelSeparator="" QueryMode="Local" DisplayField="Name" ValueField="TrainingID">
                        <Store>
                            <ext:Store ID="store1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="TrainingID" />
                                    <ext:ModelField Name="Name" />
                                </Fields>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <Change OnEvent="cmbTraining_Change">
                            </Change>
                        </DirectEvents>
                    </ext:ComboBox>
                </td>
                <td style="width: 400px">
                    <ext:DateField ID="txtDate" runat="server" Width="120px" LabelAlign="Top" FieldLabel="Date"
                        LabelSeparator="">
                    </ext:DateField>
                </td>
                <td>
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model9" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox ID="cmbEmployee" FieldLabel="Employee" LabelWidth="150" LabelAlign="Top"
                        LabelSeparator="" runat="server" DisplayField="Name" ValueField="EmployeeId"
                        StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                        MinChars="1" TriggerAction="All" ForceSelection="true">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;margin-top:20px">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="padding-top: 22px">
                    <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="Load">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
            </tr>
            <%--  <tr>
                <td colspan="3">
                    <ext:DisplayField ID="dfTrainingName" FieldCls="clsBold" runat="server" Width="300" />
                </td>
            </tr>--%>
        </table>
     
        <ext:GridPanel ID="gridEmpAttendance" runat="server" MinHeight="0" Width="1070" StyleSpec="margin-top:10px">
            <Store>
                <ext:Store ID="storeEmpAttendance" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    PageSize="50" RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="model2" runat="server">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="String" />
                                <ext:ModelField Name="LineID" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="Branch" Type="String" />
                                <ext:ModelField Name="Department" Type="String" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="InTimeDT" Type="Date" />
                                <ext:ModelField Name="OutTimeDT" Type="Date" />
                                <ext:ModelField Name="DeviceIn" Type="Date" />
                                <ext:ModelField Name="DeviceOut" Type="Date" />
                                <ext:ModelField Name="AttMark" Type="String" />
                                <ext:ModelField Name="SaveStatus" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column16" Width="150" runat="server" Text="Name" DataIndex="Name"
                        MenuDisabled="true" Align="Left">
                    </ext:Column>
                    <ext:Column ID="DateColumn4" Width="60" runat="server" Text="EIN" DataIndex="EmployeeId"
                        Sortable="false" MenuDisabled="true">
                    </ext:Column>
                    <ext:Column ID="Column1" Width="100" runat="server" Text="Branch" DataIndex="Branch"
                        MenuDisabled="true" Align="Left">
                    </ext:Column>
                    <ext:Column ID="Column2" Width="100" runat="server" Text="Department" DataIndex="Department"
                        MenuDisabled="true" Align="Left">
                    </ext:Column>
                    <ext:DateColumn ID="DateColumn5" Width="95" runat="server" Text="Date" DataIndex="DateEng"
                        Format="yyyy-MM-dd" Sortable="false" MenuDisabled="true">
                    </ext:DateColumn>
                    <ext:DateColumn ID="colInTime1" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Start Time" Align="Right" Width="70" DataIndex="InTimeDT" Format="hh:mm tt">
                    </ext:DateColumn>
                    <ext:DateColumn ID="DateColumn1" Sortable="false" MenuDisabled="true" runat="server"
                        Text="End Time" Align="Right" Width="70" DataIndex="OutTimeDT" Format="hh:mm tt">
                    </ext:DateColumn>
                    <ext:DateColumn ID="DateColumn2" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Device In" Align="Right" Width="100" DataIndex="DeviceIn" Format="hh:mm tt">
                    </ext:DateColumn>
                    <ext:DateColumn ID="DateColumn3" Sortable="false" MenuDisabled="true" runat="server"
                        Text="Device Out" Align="Right" Width="100" DataIndex="DeviceOut" Format="hh:mm tt">
                    </ext:DateColumn>
                    <ext:Column ID="columnAttremarks" Width="70" runat="server" Text="Att Mark" DataIndex="AttMark"
                        Cls="clsAttBackColor" MenuDisabled="true" Align="Left">
                    </ext:Column>
                    <ext:Column ID="Column4" Width="60" runat="server" Text="Status" DataIndex="SaveStatus"
                        Cls="clsAttBackColor" MenuDisabled="true" Align="Left">
                    </ext:Column>
                    <ext:CommandColumn ID="CommandColumn3" runat="server" Width="120" MenuDisabled="true"
                        ToolTip="Save/Delete" Sortable="false">
                        <Commands>
                            <ext:SplitCommand Text="Save" CommandName="Save">
                                <Menu EnableScrolling="false">
                                    <Items>
                                        <ext:MenuCommand Text="Delete" CommandName="Reset" />
                                    </Items>
                                </Menu>
                            </ext:SplitCommand>
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command, record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="RowSelectionModel3" runat="server" Mode="Simple" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeEmpAttendance"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="1">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
            Height="30" ID="btnSave" Text="<i></i>Save Attendance">
            <DirectEvents>
                <Click OnEvent="btnSave_Click">
                    <EventMask ShowMask="true" />
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the Attendance?" />
                    <ExtraParams>
                        <ext:Parameter Name="gridItems" Value="Ext.encode(#{gridEmpAttendance}.getRowsValues({ selectedOnly: true }))"
                            Mode="Raw" />
                    </ExtraParams>
                </Click>
            </DirectEvents>
        </ext:Button>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
