﻿<%@ Page Title="Training Employee List" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="TrainingEmployee.aspx.cs" Inherits="Web.NewHR.TrainingMod.TrainingEmployee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">


<script type="text/javascript">
   
    var onGroupCommand = function (column, command, group) {
    
        var name = group.name;
        var spl = name.split(':');
        window.location = 'TrainingEmpAttendance.aspx?Id=' + spl[1].toString();
    };

   
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Training Employee List
                </h4>
            </div>
        </div>
    </div>

 <div class="contentpanel">

 <table>
    <tr>
        <td style="width:130px;">
            <ext:DateField ID="txtDateFrom" FieldLabel="From" LabelAlign="Top" LabelSeparator="" runat="server" Width="120" Format="MM-yyyy" ></ext:DateField>
        </td>
        <td style="width:140px;">
            <ext:DateField ID="txtDateTo" FieldLabel="To" LabelAlign="Top" LabelSeparator="" runat="server" Width="120" Format="MM-yyyy" ></ext:DateField>
        </td>
        <td style="padding-top:15px;">
            <ext:Button ID="btnLoad" runat="server" Cls="btn btn-primary" Text="Load">
                <DirectEvents>
                    <Click OnEvent="btnLoad_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </td>
    </tr>
 </table>
 <br />

    <ext:GridPanel ID="gridTrainingEmployee" runat="server" Cls="itemgrid" Width="700" Scroll="None">
                <Store>
                    <ext:Store ID="Store1" runat="server" GroupField="Name">
                        <Sorters>
                            <ext:DataSorter Property="SN" Direction="ASC" />
                        </Sorters>
                        <Model>
                            <ext:Model ID="Model2" runat="server" IDProperty="SN">
                                <Fields>
                                    <ext:ModelField Name="SN" />
                                    <ext:ModelField Name="TrainingId" Type="Int" />
                                    <ext:ModelField Name="EmployeeId" />
                                    <ext:ModelField Name="Name" />
                                    <ext:ModelField Name="EmployeeName" />
                                    <ext:ModelField Name="Branch" />
                                    <ext:ModelField Name="FromDate" Type="Date" />
                                    <ext:ModelField Name="ToDate" Type="Date" />
                                    <ext:ModelField Name="StartTime" />
                                    <ext:ModelField Name="EndTime" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                            <ext:Column ID="Column1" runat="server" Text="EIN" Width="50" DataIndex="EmployeeId" MenuDisabled="true" Sortable="false">
                        </ext:Column>
                        <ext:Column ID="colEmployeeName" runat="server" Text="Employee Name" Width="200" DataIndex="EmployeeName" MenuDisabled="true" Sortable="false">
                        </ext:Column>
                        <ext:Column ID="colBranch" runat="server" Text="Branch" Width="100" DataIndex="Branch" MenuDisabled="true" Sortable="false">
                        </ext:Column>

                       <ext:DateColumn ID="colFromDate" runat="server" Align="Right" Text="Training Start" Width="100"
                            MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="FromDate">
                        </ext:DateColumn>

                        <ext:DateColumn ID="colToDate" runat="server" Align="Right" Text="Training End" Width="100"
                            MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="ToDate">
                        </ext:DateColumn>

                        <ext:Column ID="colStartTime" runat="server" Text="Start Time" Width="100" DataIndex="StartTime" Align="Center" MenuDisabled="true" Sortable="false">
                        </ext:Column>
                        <ext:Column ID="colEndTime" runat="server" Text="End Time" Width="100" DataIndex="EndTime" Align="Center" MenuDisabled="true" Sortable="false">
                        </ext:Column>
                        
                         <ext:CommandColumn ID="CommandColumn1" runat="server" Hidden="true">
                        <GroupCommands>
                          <%--  <ext:GridCommand Icon="TableRow" CommandName="SelectGroup">
                                <ToolTip Title="Training Attendance" Text="Training Attendance" />
                            </ext:GridCommand>--%>
                            <ext:CommandFill />
                            <ext:GridCommand Text="Menu" StandOut="true">
                                <Menu>
                                    <Items>
                                        <ext:MenuCommand CommandName="ItemCommand" Text="Training Attendance" />
                                    </Items>
                                </Menu>
                               
                            </ext:GridCommand>
                           
                        </GroupCommands>
                        <Listeners>
                            <GroupCommand Fn="onGroupCommand" />
                        </Listeners>
                        
                    </ext:CommandColumn>
                        
                    </Columns>
                </ColumnModel>
                <Features>
                    <ext:GroupingSummary ID="Group1" runat="server" GroupHeaderTplString="{name}" HideGroupedHeader="true"
                        EnableGroupingMenu="false" />
                </Features>
                <%--<BottomBar>
                <ext:PagingToolbar ID="PagingToolbar2" runat="server" DisplayInfo="false">
                    <Items>
                      
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>--%>
              
            </ext:GridPanel>
            <br />
            <br />
 </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
