﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils.Calendar;
using Newtonsoft.Json.Linq;

namespace Web.NewHR.TrainingMod
{
    public partial class TrainingAttendance : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            int trainingId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
            {
                trainingId = int.Parse(Request.QueryString["Id"].ToString());
                hdnTrainingId.Text = trainingId.ToString();

                Training obj = TrainingManager.GetTrainingById(trainingId);
               

                if (obj != null)
                {
                    dfTrainingName.Text = obj.Name;
                    dfTrainingDetails.Text = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day + ", " + obj.FromDate.Value.Year + " - " + DateHelper.GetMonthName(obj.ToDate.Value.Month, true) + " " + obj.ToDate.Value.Day + ", " + obj.ToDate.Value.Year;//+ " " + AttendanceManager.CalculateManualVisibleTime(obj.StartTime.Value) + " to " + AttendanceManager.CalculateManualVisibleTime(obj.EndTime.Value);
                    chkCopyMode.Checked = true;

                    TimeSpan ts = obj.ToDate.Value - obj.FromDate.Value;
                   
                    List<TrainingPlanningLine> objTrainingPlanningLineCount = TrainingManager.GetTrainingLines(trainingId).Where(t=>t.IsTraining==true).ToList();
                    int totalDays = objTrainingPlanningLineCount.Count;//ts.Days;

                    hdnTotalDays.Text = (totalDays + 1).ToString();

                    List<GetHolidaysForAttendenceResult> holiday = new HolidayManager().GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, obj.FromDate.Value.Date, obj.ToDate.Value.Date,0).ToList();

                    string columnId = "Day_{0}";

                    for (int i = 0; i <= totalDays; i++)
                    {
                        string holidayAbbr = "";
                        GetHolidaysForAttendenceResult objHolidayResult = holiday.SingleOrDefault(x => x.DateEng.Value.Date == obj.FromDate.Value.Date.AddDays(i));
                        if (objHolidayResult != null && objHolidayResult.Abbreviation == "WH")
                            holidayAbbr = objHolidayResult.Abbreviation;
                        else
                            holidayAbbr = "";

                        string gridIndexId = string.Format(columnId,i);
                        ModelField field = new ModelField(gridIndexId, ModelFieldType.String);
                        field.UseNull = true;
                        
                        Model1.Fields.Add(gridIndexId, ModelFieldType.String);

                        DateTime dt = obj.FromDate.Value.AddDays(i);
                        string dayName = dt.Day + "-" + DateHelper.GetMonthShortName(dt.Month, true);
                        gridTrainingAttendance.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, dayName, holidayAbbr));
                    }
                   

                    List<TrainingRequest> listTR = TrainingManager.GetApprovedTRByTrainingId(trainingId);

                    object[] data = new object[listTR.Count * 2];
                    int row = 0;
                    int sn = 0;
                    

                    foreach (var item in listTR)
                    {
                        object[] rowData = new object[totalDays + 4];

                        rowData[0] = ++sn;
                        rowData[1] = item.EmployeeId.Value;
                        rowData[2] = item.Name;

                        int m = 3;
                        for (int k = 0; k <= totalDays; k++)
                        {                  
                            string holidayAbbr = "";
                            GetHolidaysForAttendenceResult objHolidayResult = holiday.SingleOrDefault(x => x.DateEng.Value.Date == obj.FromDate.Value.Date.AddDays(k));
                            if (objHolidayResult != null && objHolidayResult.Abbreviation == "WH")
                                holidayAbbr = objHolidayResult.Abbreviation;
                            else
                                holidayAbbr = "";


                            if (holidayAbbr == "")
                            {
                                AttendanceGetInAndOutTimeResult objAttendanceGetInAndOutTimeResult = new AttendanceGetInAndOutTimeResult();

                                objAttendanceGetInAndOutTimeResult = AttendanceManager.getAttendanceofGivenDate(item.EmployeeId.Value, obj.FromDate.Value.AddDays(k));

                                if (objAttendanceGetInAndOutTimeResult != null && objAttendanceGetInAndOutTimeResult.InDate != null)
                                {
                                    DateTime inTimeDateTime = new DateTime(objAttendanceGetInAndOutTimeResult.InDate.Value.Year, objAttendanceGetInAndOutTimeResult.InDate.Value.Month, objAttendanceGetInAndOutTimeResult.InDate.Value.Day, objAttendanceGetInAndOutTimeResult.InDate.Value.Hour, objAttendanceGetInAndOutTimeResult.InDate.Value.Minute, objAttendanceGetInAndOutTimeResult.InDate.Value.Second);
                                    rowData[m++] = inTimeDateTime.ToString();

                                }
                                else
                                {
                                    TrainingPlanningLine objTrainingPlanningLine = TrainingManager.GetTrainingLineByIdAndDate(trainingId, obj.FromDate.Value);
                                    DateTime inTimeDateTime = new DateTime(obj.FromDate.Value.Year, obj.FromDate.Value.Month, obj.FromDate.Value.Day, objTrainingPlanningLine.StartTime.Value.Hours, objTrainingPlanningLine.StartTime.Value.Minutes, objTrainingPlanningLine.StartTime.Value.Seconds);
                                    rowData[m++] = inTimeDateTime.ToString();
                                }
                            }
                            else
                                rowData[m++] = "WH";
                            
                        }

                        data[row++] = rowData;

                        object[] rowDataOut = new object[totalDays + 4];

                        rowDataOut[0] = ++sn;
                        rowDataOut[1] = item.EmployeeId.Value;
                        rowDataOut[2] = "";
                        int n = 3;

                        for (int k = 0; k <= totalDays; k++)
                        {

                            string holidayAbbr = "";
                            GetHolidaysForAttendenceResult objHolidayResult = holiday.SingleOrDefault(x => x.DateEng.Value.Date == obj.FromDate.Value.Date.AddDays(k));
                            if (objHolidayResult != null && objHolidayResult.Abbreviation == "WH")
                                holidayAbbr = objHolidayResult.Abbreviation;
                            else
                                holidayAbbr = "";

                            if (holidayAbbr == "")
                            {

                                AttendanceGetInAndOutTimeResult objAttendanceGetInAndOutTimeResult = new AttendanceGetInAndOutTimeResult();

                                objAttendanceGetInAndOutTimeResult = AttendanceManager.getAttendanceofGivenDate(item.EmployeeId.Value, obj.FromDate.Value.AddDays(k));
                                

                                if (objAttendanceGetInAndOutTimeResult != null && objAttendanceGetInAndOutTimeResult.OutDate != null)
                                {
                                    DateTime outTimeDateTime = new DateTime(objAttendanceGetInAndOutTimeResult.OutDate.Value.Year, objAttendanceGetInAndOutTimeResult.OutDate.Value.Month, objAttendanceGetInAndOutTimeResult.OutDate.Value.Day, objAttendanceGetInAndOutTimeResult.OutDate.Value.Hour, objAttendanceGetInAndOutTimeResult.OutDate.Value.Minute, objAttendanceGetInAndOutTimeResult.OutDate.Value.Second);
                                    rowDataOut[n++] = outTimeDateTime.ToString();
                                }
                                else
                                {
                                    TrainingPlanningLine objTrainingPlanningLine = TrainingManager.GetTrainingLineByIdAndDate(trainingId, obj.ToDate.Value);
                                    DateTime outTimeDateTime = new DateTime(obj.ToDate.Value.Year, obj.ToDate.Value.Month, obj.ToDate.Value.Day, objTrainingPlanningLine.EndTime.Value.Hours, objTrainingPlanningLine.EndTime.Value.Minutes, objTrainingPlanningLine.EndTime.Value.Seconds);
                                    rowDataOut[n++] = outTimeDateTime.ToString();
                                }
                            }
                            else
                                rowDataOut[n++] = "WH";

                        }

                        data[row++] = rowDataOut;
                        
                    }

                    gridTrainingAttendance.GetStore().DataSource = data;
                    gridTrainingAttendance.GetStore().DataBind();

                    gridTrainingAttendance.Width = ((totalDays + 1) * 100) + 200;

                }
            }
        }


        Column GetExtGridPanelColumn(string indexId, string headerName, string holidayAbbr)
        {
            Column column = new Column();
            column.DataIndex = indexId;
            column.DataIndex = indexId;
            column.Text = headerName;
            column.Align = Alignment.Right;
            column.ToolTip = headerName;
            column.MenuDisabled = true;
            column.Sortable = false;
            column.Width = new Unit(100);
            column.Align = Alignment.Center;

            TimeField tf = new TimeField();
            TimeSpan tsStart = TimeSpan.FromHours(8);
            TimeSpan tsEnd = TimeSpan.FromHours(18);
            tf.MinTime = tsStart;
            tf.MaxTime = tsEnd;
            tf.Increment = 1;

            if (holidayAbbr == "")
            {
                column.Renderer.Handler = "return Ext.util.Format.date(value, 'g:i a')";

                column.Editor.Add(tf);
            }

            return column;
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            string gridJSON = e.ExtraParams["gridItems"];

            int totalDays = int.Parse(hdnTotalDays.Text.Trim());
            int trainingId = int.Parse(hdnTrainingId.Text.Trim());

            Training objTraining = TrainingManager.GetTrainingById(trainingId);

            JArray jsonArray = Ext.Net.JSON.Deserialize(gridJSON) as JArray;

            if (jsonArray.Count == 0)
                return;

            List<TimeRequestLine> listTR = new List<TimeRequestLine>();
            List<TimeRequestLine> listResult = new List<TimeRequestLine>();


            for (int i = 0; i < jsonArray.Count; i++)
            {
                JToken row = jsonArray[i];

                if (i % 2 == 0)
                    listTR = new List<TimeRequestLine>();


                int employeeId = int.Parse(row["EmployeeId"].Value<string>());

               

                for (int j = 0; j < totalDays; j++)
                {
                    string key = string.Format("Day_{0}", j);

                    if (row[key] != null)
                    {

                        TimeRequestLine obj = new TimeRequestLine();

                        obj.EmpId = employeeId;
                        string inOutTime = row[key].Value<string>();


                        if (inOutTime != "WH")
                        {
                            string[] splInOutTime = inOutTime.Split(' ');

                            if (splInOutTime.ToList().Count > 3)
                            {
                                obj.InTime = TimeSpan.Parse(splInOutTime[4].ToString());
                            }
                            else
                            {
                                obj.InTime = TimeSpan.Parse(splInOutTime[1].ToString());
                                if (inOutTime.ToLower().Contains("pm"))
                                {
                                    TimeSpan addHour = TimeSpan.FromHours(12);
                                    obj.InTime = obj.InTime.Value.Add(addHour);
                                }
                            }

                            obj.DayName = "";

                        }
                        else
                        {
                            obj.EmpId = employeeId;
                            obj.DayName = "WH";
                        }

                        if (i % 2 != 0)
                        {

                            TimeRequestLine oldTr = listTR[j];
                            obj.OutTime = obj.InTime;
                            obj.InTime = oldTr.InTime;
                            obj.DateEng = objTraining.FromDate.Value.AddDays(j);
                            listTR.Remove(oldTr);
                            listTR.Insert(j, obj);

                        }
                        else
                        {
                            listTR.Add(obj);
                        }


                    }

                }


                if (i % 2 == 1)
                    listResult.AddRange(listTR);
            }


            Status status = TrainingManager.SaveTrainingAttendance(listResult);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Training Attendance saved successfully.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }








    }
}