﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Ext.Net;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using System.Data;

using DayPilot.Json;
using DayPilot.Utils;
using DayPilot.Web.Ui;
using DayPilot.Web.Ui.Data;
using DayPilot.Web.Ui.Enums;
using DayPilot.Web.Ui.Events;
using DayPilot.Web.Ui.Events.Bubble;
using DayPilot.Web.Ui.Events.Scheduler;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using Utils.Calendar;
using Bll;

namespace Web.NewHR.TrainingMod
{
    public partial class TrainingPlanning : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {          
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialize();


                if (Session["Red"] != null && Session["Red"].ToString() == "1")
                {
                    Session["Red"] = "0";
                    NewMessage.ShowNormalMessage(Session["Msg"].ToString());
                }
            }
        }      

        private void Initialize()
        {
            cmbTrainingType.Store[0].DataSource = CommonManager.GetTrainingTypeList();
            cmbTrainingType.Store[0].DataBind();

            ClearFields();
            BindBLevels();
            BindAllTrainingGrids();
            ShowEvents();
            BindComboResourcePerson();
            BindCalendar();
            BindCountry();
        }

        private void BindCountry()
        {
            List<CountryList> listCountries = new CommonManager().GetAllCountries().ToList();
            cmbCountry.GetStore().DataSource = listCountries;
            cmbCountry.GetStore().DataBind();
        }

        private void BindCalendar()
        {
            RegisterLegendColors();
            RegisterTrainingCalendarColorStyles();
        }

        private void BindAllTrainingGrids()
        {
            //BindCalendar();
            BindTrainings();
            BindTrainingEventHistory();
            BindTrainingUnpublished();

            if (X.IsAjaxRequest)
            {
                Panel1.Render();
                Panel2.Render();
                Panel3.Render();
            }
        }

        private void ClearFields()
        {
            mcmbLevel.Text = "";
            txtTrainingName.Text = "";
            txtStartDate.Text = "";            
            txtEndDate.Text = "";           
            cmbTrainingType.Text = "";
           // tfStartTime.Text = "9:00";
           // tfEndTime.Text = "9:00";
            txtSeats.Text = "";
            txtVenue.Text = "";
            txtHost.Text = "";
            txtNotes.Text = "";
            txtResourcePerson.Text = "";
           // lblTotalDays.Text = "";
           // lblTotalHours.Text = "";
            dispTrainingRequestCount.Text = "";
            dispTrainingApprovedCount.Text = "";
            dispTrainingRequestCount.Hide();
            dispTrainingApprovedCount.Hide();
            cbChangeNotification.Checked = false;
            cbChangeNotification.Hide();
            cbSendEmail.Checked = false;
            cbSendEmail.Hide();
            txtCity.Text = "";
            cmbCountry.ClearValue();
            dispRP.Text = "";

            //lblTotalHours.Text = "";
        }

        private void BindBLevels()
        {
            //cmbLevel.Store[0].DataSource = TrainingManager.GetBLevels();
            //cmbLevel.Store[0].DataBind();

            mcmbLevel.Store[0].DataSource = TrainingManager.GetBLevels();
            mcmbLevel.Store[0].DataBind();
        }

        private void BindComboResourcePerson()
        {

            List<ResourcePersongBO> _ListResourcePersongBO = TrainingManager.GetResourePersons();
            ResourcePersongBO emptyFirst = new ResourcePersongBO();
            emptyFirst.Name = "";
            emptyFirst.ResourcePersonId = 0;
            _ListResourcePersongBO.Insert(0, emptyFirst);

            //string RPs = "";
            //foreach(ResourcePersongBO _item in _ListResourcePersongBO)
            //{
            //    if (!string.IsNullOrEmpty(RPs))
            //        RPs = RPs + "," + "'" +  _item.ResourcePersonId + "'";
            //    else
            //        RPs = "'" + _item.ResourcePersonId.ToString() + "'";
            //}

            //hdnRPTags.Text = RPs;

            storeResoucrePerson.DataSource = _ListResourcePersongBO.OrderBy(t => t.Name);
            storeResoucrePerson.DataBind();
        }

       

        private void BindTrainings()
        {            
            gridTraining.GetStore().DataSource = TrainingManager.GetTrainingEvents();
            gridTraining.GetStore().DataBind();
        }

        [DirectMethod]
        public void ShowAddNewRP()
        {
            windowRP.Show();
        }

        protected void btnFill_Click(object sender, DirectEventArgs e)
        {
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();

            if (txtStartDate.SelectedDate != new DateTime())
                startDate = txtStartDate.SelectedDate;
            else
            {
                NewMessage.ShowWarningMessage("Start date is required.");
                return;
            }

            if (txtEndDate.SelectedDate != new DateTime())
                endDate = txtEndDate.SelectedDate;
            else
            {
                NewMessage.ShowWarningMessage("End date is required.");
                return;
            }

            //if (startDate > DateTime.Now.Date || endDate.Date > DateTime.Now.Date)
            //{
            //    NewMessage.ShowWarningMessage("You cannot request attendance in the future.");
            //    return;
            //}

            //if (AttendanceManager.CheckTimeRequestIsSaved(startDate, endDate, SessionManager.CurrentLoggedInEmployeeId))
            //{
            //    NewMessage.ShowWarningMessage("Time request is already saved for the period.");
            //    return;
            //}

            //string message = AttendanceManager.CheckEmpTimeAttRequestThreshold(startDate, endDate);
            //if (message != "")
            //{
            //    NewMessage.ShowWarningMessage(message);
            //    return;
            //}

            int? TrainingID = null;

            if (!string.IsNullOrEmpty(hdnTrainingId.Text))
                TrainingID = int.Parse(hdnTrainingId.Text);


            string gridItemsJSON = e.ExtraParams["gridItems"];
            List<TrainingPlanningLine> listGrid = JSON.Deserialize<List<TrainingPlanningLine>>(gridItemsJSON);

            List<TrainingPlanningLine> list = TrainingManager.GetTrainingLineForDateRange(startDate, endDate, TrainingID, listGrid);

          

            //foreach (var item in list)
            //{
            //    foreach (var itemGrid in listGrid)
            //    {
            //        if (itemGrid.DateEng.Value == item.DateEng.Value)
            //        {
            //            item.InTimeDT = itemGrid.InTimeDT;

            //            if (!string.IsNullOrEmpty(itemGrid.InNote))
            //                item.InNote = itemGrid.InNote;
            //            item.OutTimeDT = itemGrid.OutTimeDT;

            //            if (!string.IsNullOrEmpty(itemGrid.OutNote))
            //                item.OutNote = itemGrid.OutNote;
            //        }
            //    }
            //}

            gridAttRequest.GetStore().DataSource = list;
            gridAttRequest.GetStore().DataBind();

        }

        private void BindTrainingEventHistory()
        {
            DateTime? dateFilterFrom = null, dateFilterTo = null;
            if (!string.IsNullOrEmpty(txtDateFilterFrom.Text.Trim()))
                dateFilterFrom = DateTime.Parse(txtDateFilterFrom.Text.Trim());

            if (!string.IsNullOrEmpty(txtDateFilterTo.Text.Trim()))
                dateFilterTo = DateTime.Parse(txtDateFilterTo.Text.Trim());

            gridTrainingHistory.GetStore().DataSource = TrainingManager.GetTrainingEventsHistory(dateFilterFrom, dateFilterTo).ToList();
            gridTrainingHistory.GetStore().DataBind();
        }

        
        private void BindTrainingUnpublished()
        {
            gridTrainingUnpublished.GetStore().DataSource = TrainingManager.GetTrainingsUnpublished();
            gridTrainingUnpublished.GetStore().DataBind();
        }

        private void ShowEvents()
        {
            List<Training> list = new List<Training>();
            int listCount = 0;
            int day = 0;
            int month = System.DateTime.Now.Month;
            int year = System.DateTime.Now.Year;
            int lastMonth = 0, nextMonth = 0, lastYear = 0, nextYear = 0;

            dispThisMonthTitle.Html = "";
           // dispThisMonthDetails.Html = "";
           // dispLastMonthTitle.Html = "";
           // dispLastMonthDetails.Html = "";
          //  dispNextMonthTitle.Html = "";
           // dispNextMonthDetails.Html = "";

            string monthName = DateHelper.GetMonthName(month, true);

            //current month
            list = TrainingManager.GetTrainingsByMonth(month, year);
            listCount = list.Count;

            //dispThisMonthTitle.Html = monthName + " " + year.ToString() + "{Current Month}    " + listCount.ToString();
            dispThisMonthTitle.Html = "This Month - " + monthName + " " + year.ToString() + " (" + listCount.ToString() + ")";
            List<TrainingTitleBO> _listTitles = new List<TrainingTitleBO>();
            foreach (var item in list)
            {

                TrainingTitleBO _objTrainingTitleBO = new TrainingTitleBO();
                _objTrainingTitleBO.Name = item.Name;
                _objTrainingTitleBO.DateInfo = DateHelper.GetMonthShortName(item.FromDate.Value.Month, true) + " " + item.FromDate.Value.Day + " - " + DateHelper.GetMonthShortName(item.ToDate.Value.Month, true) + " " + item.ToDate.Value.Day  + ", " + year.ToString() + " : " + item.Days + " days ";

                // + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")");
                _objTrainingTitleBO.Host = item.Host;


                string Seats = (item.AvailableSeats == 0 ? "" : item.AvailableSeats.ToString());
                string BookedSeats = (item.BookedSeats == 0 ? "0" : item.BookedSeats.ToString());
                string Avilable = (item.LeftSeats == 0 ? "0" : item.LeftSeats.ToString());

                //_objTrainingTitleBO.BookingInfo = Seats + "; " + BookedSeats + "; " + Avilable + " <a class='clsCursorHand' onclick='return DetailsClick(" + item.TrainingID.ToString() + ");'>Details</a>";
                _objTrainingTitleBO.Seats = Seats;
                _objTrainingTitleBO.Avilable = Avilable;
                _objTrainingTitleBO.Booked = BookedSeats;
               // _objTrainingTitleBO.ViewLink = " <a  class='clsCursorHand' onclick='return DetailsClick(" + item.TrainingID.ToString() + ");'> View</a>";

                //dispThisMonthDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")") + "<br>";
                _listTitles.Add(_objTrainingTitleBO);
            }



            rptTrainingList.DataSource = _listTitles;
            rptTrainingList.DataBind();


            if (list.Count == 0)
                dispThisMonthTitle.Hidden = true;

            //last month
            if (month == 1)
            {
                lastMonth = 12;
                lastYear = year - 1;
            }
            else
            {
                lastMonth = month - 1;
                lastYear = year;
            }

            monthName = DateHelper.GetMonthName(lastMonth, true);

            list.Clear();
            list = TrainingManager.GetTrainingsByMonth(lastMonth, lastYear);
            listCount = list.Count;

            //dispLastMonthTitle.Html = monthName + " " + year.ToString() + "{Last Month}    " + listCount.ToString();
          //  dispLastMonthTitle.Html = "Previous Month - " + monthName + " " + year.ToString() + " (" + listCount.ToString() + ")";

            //foreach (var item in list)
            //{
            //    dispLastMonthDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")") + "<br>";
            //}

            List<TrainingTitleBO> _listTitlesLastMonth = new List<TrainingTitleBO>();
            foreach (var item in list)
            {

                TrainingTitleBO _objTrainingTitleBO = new TrainingTitleBO();
                _objTrainingTitleBO.Name = item.Name;
                _objTrainingTitleBO.DateInfo = DateHelper.GetMonthShortName(item.FromDate.Value.Month, true) + " " + item.FromDate.Value.Day + " - " + DateHelper.GetMonthShortName(item.ToDate.Value.Month, true) + " " + item.ToDate.Value.Day + ", " + year.ToString() + " : " + item.Days + " days ";

                // + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")");
                _objTrainingTitleBO.Host = item.Host;
                //dispThisMonthDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")") + "<br>";
                _listTitlesLastMonth.Add(_objTrainingTitleBO);
            }

           // RptLastMonthDetails.DataSource = _listTitlesLastMonth;
          //  RptLastMonthDetails.DataBind();


            if (list.Count == 0)
                //dispLastMonthTitle.Hidden = true;

            //next month
            if (month == 12)
            {
                nextMonth = 1;
                nextYear = year + 1;
            }
            else
            {
                nextMonth = month + 1;
                nextYear = year + 1;
            }

            monthName = DateHelper.GetMonthName(nextMonth, true);

            list.Clear();
            //list = TrainingManager.GetTrainingsByMonth(nextMonth, year);
            //listCount = list.Count;
            
            //dispNextMonthTitle.Html = monthName + " " + year.ToString() + "{Next Month}    " + listCount.ToString();
            //dispNextMonthTitle.Html = "Next Month - " + monthName + " " + year.ToString() + " (" + listCount.ToString() + ")";

            //foreach (var item in list)
            //{
            //    dispNextMonthDetails.Html += item.Name + ", " + item.FromDate.Value.Day + "-" + item.ToDate.Value.Day + ", " + item.Days + " days, " + ((TrainingTypeEnum)item.Type.Value).ToString() + (item.LeftSeats == 0 ? "" : "  (" + item.LeftSeats.ToString() + ")") + "<br>";
            //}
            //if (list.Count == 0)
            //    dispNextMonthTitle.Hidden = true;
        }
        
        protected void btnAddNew_Click(object sender, DirectEventArgs e)
        {
            hdnTrainingId.Text = "";
            //lblTotalHours.Text = "";
            ClearFields();
            windowTraining.Center();
            windowTraining.Show();
        }

        protected void btnSaveRP_Click(object sender, DirectEventArgs e)
        {
            TrainingResoucrePerson obj = new TrainingResoucrePerson();
            EmployeeManager.GetAllEmployees();
            obj.Name = txtRPName.Text;
            obj.RPID = TrainingManager.GetMaxResoucrePersonID() + 1;
            Status status = TrainingManager.SaveUpdateTrainingRP(obj);
            hdnAddedRP.Text = obj.RPID.ToString();
            if (status.IsSuccess == true)
            {
                NewMessage.ShowNormalPopup("Resource person added");
                windowRP.Hide();
                BindComboResourcePerson();

            }
        }
      
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveUpdate");
            if (Page.IsValid)
            {
                Training obj = new Training();

                bool sendNotification = false;
                bool sendEmail = false;

                if (!string.IsNullOrEmpty(hdnTrainingId.Text))
                {
                    obj.TrainingID = int.Parse(hdnTrainingId.Text);

                    if (cbChangeNotification.Checked)
                        sendNotification = true;
                    if (cbSendEmail.Checked)
                        sendEmail = true;
                }

                obj.Name = txtTrainingName.Text.Trim();
                obj.FromDate = DateTime.Parse(txtStartDate.Text.Trim());
                obj.ToDate = DateTime.Parse(txtEndDate.Text.Trim());
                obj.Days = double.Parse(hdnTotalDays.Text);

                if(obj.ToDate < obj.FromDate)
                {
                    NewMessage.ShowWarningMessage("Completes on date should be after Starts on date.");
                    return;
                }

                //if (!string.IsNullOrEmpty(tfStartTime.Text))             
                //    obj.StartTime = TimeSpan.Parse(tfStartTime.Text);

                //if (!string.IsNullOrEmpty(tfEndTime.Text))
                //    obj.EndTime = TimeSpan.Parse(tfEndTime.Text);

                if (!string.IsNullOrEmpty(hdnTotalHours.Text))
                    obj.Hours = double.Parse(hdnTotalHours.Text);


                if (!string.IsNullOrEmpty(cmbTrainingType.Text))
                    obj.Type = int.Parse(cmbTrainingType.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtSeats.Text.Trim()))
                    obj.AvailableSeats = int.Parse(txtSeats.Text.Trim());

                //if (!string.IsNullOrEmpty(cmbLevel.SelectedItem.Text))
                //    obj.LevelId = int.Parse(cmbLevel.SelectedItem.Value);

                string levelIds = "";
                foreach (Ext.Net.ListItem item in mcmbLevel.SelectedItems)
                {
                    levelIds += item.Value + ",";
                }

                if (levelIds != "")
                    obj.LevelIds = levelIds.Substring(0, levelIds.Length - 1);

                if (!string.IsNullOrEmpty(txtVenue.Text.Trim()))
                    obj.Venue = txtVenue.Text.Trim();

                if (!string.IsNullOrEmpty(txtHost.Text.Trim()))
                    obj.Host = txtHost.Text.Trim();

                obj.Notes = txtNotes.Text.Trim();

                if (!string.IsNullOrEmpty(txtResourcePerson.Text.Trim()))
                    obj.ResourcePerson = txtResourcePerson.Text.Trim();

                if (!string.IsNullOrEmpty(cmbCountry.SelectedItem.Value))
                    obj.Country = int.Parse(cmbCountry.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtCity.Text.Trim()))
                    obj.City = txtCity.Text.Trim();

                obj.Status = (int)TrainingStatusEnum.Saved;

                //Getting Grid Lines--

                string gridItemsJSON = e.ExtraParams["gridItems"];
                List<TrainingPlanningLineBO> listGrid = JSON.Deserialize<List<TrainingPlanningLineBO>>(gridItemsJSON);
                List<TrainingPlanningLine> ListTrainingPlanningLine = new List<TrainingPlanningLine>();

                List<string> RPIds = new List<string>();
                foreach (var item in listGrid)
                {
                    if (!string.IsNullOrEmpty(item.IsTraining) && (item.OutTimeDT == null || item.InTimeDT == null))
                    {
                        if (bool.Parse(item.IsTraining) == true)
                        {
                            NewMessage.ShowWarningMessage("Start time and end time is required");
                            return;
                        }
                    }

                    //TimeSpan? objStartTime = null;
                    //TimeSpan? objEndTime = null;

                    //if (DateTime.Parse(item.StartTime) != null && DateTime.Parse(item.StartTime) != new DateTime())
                    //{
                    //    DateTime StartDate = DateTime.Parse(item.StartTime);
                    //    objStartTime = new TimeSpan(StartDate.Hour, StartDate.Minute, 0);
                    //}

                    //if (DateTime.Parse(item.EndTime) != null && DateTime.Parse(item.EndTime) != new DateTime())
                    //{
                    //    DateTime EndDate = DateTime.Parse(item.EndTime);
                    //    objEndTime = new TimeSpan(EndDate.Hour, EndDate.Minute, 0);
                    //}

                    //if (!string.IsNullOrEmpty(item.IsTraining) && item.OutTimeDT != null && item.InTimeDT != null)
                    //{
                    //    if (bool.Parse(item.IsTraining) == true)
                    //    {
                            TrainingPlanningLine ObjTrainingPlanningLine = new TrainingPlanningLine();
                            if (item.InTimeDT != null && DateTime.Parse(item.InTimeDT) != new DateTime())
                            {
                                TimeSpan tsStartTime = new TimeSpan(DateTime.Parse(item.InTimeDT).Hour, DateTime.Parse(item.InTimeDT).Minute, 0);
                                ObjTrainingPlanningLine.StartTime = tsStartTime;
                            }

                            if (item.OutTimeDT != null && DateTime.Parse(item.OutTimeDT) != new DateTime())
                            {
                                TimeSpan tsEndTime = new TimeSpan(DateTime.Parse(item.OutTimeDT).Hour, DateTime.Parse(item.OutTimeDT).Minute, 0);
                                ObjTrainingPlanningLine.EndTime = tsEndTime;
                            }


                            ObjTrainingPlanningLine.LineID = Guid.NewGuid();
                            ObjTrainingPlanningLine.DateEng = item.DateEng;
                            ObjTrainingPlanningLine.Date = BLL.BaseBiz.GetAppropriateDate(item.DateEng.Value);
                            //ObjTrainingPlanningLine.StartTime = objStartTime;
                            //ObjTrainingPlanningLine.EndTime = objEndTime;
                            ObjTrainingPlanningLine.Day = item.Day;
                            if (!string.IsNullOrEmpty(item.IsTraining))
                            ObjTrainingPlanningLine.IsTraining = bool.Parse(item.IsTraining);
                            if (ObjTrainingPlanningLine.EndTime != null)
                            {
                                TimeSpan Hours = ObjTrainingPlanningLine.EndTime.Value - ObjTrainingPlanningLine.StartTime.Value;
                                ObjTrainingPlanningLine.Hours = Hours.ToString();
                            }

                            ObjTrainingPlanningLine.ResourcePerson = item.ResourcePerson;

                            if (!string.IsNullOrEmpty(item.ResourcePerson))
                            {
                                List<string> _list = item.ResourcePerson.Split(',').ToList();
                                RPIds.AddRange(_list);
                            }
                            ListTrainingPlanningLine.Add(ObjTrainingPlanningLine);
                    //    }
                    //}
                }

                RPIds = RPIds.Distinct().ToList();
                string ResourcePersons = "";
                //getting Resoucre Persons
                foreach (string Id in RPIds)
                {
                    string Name =TrainingManager.GetResourePersons().ToList().SingleOrDefault(x => x.ResourcePersonId == int.Parse(Id)).Name;
                    if (!string.IsNullOrEmpty(ResourcePersons))
                        ResourcePersons = ResourcePersons + ", " + Name;
                    else
                        ResourcePersons = Name;
                }
                obj.ResourcePerson = ResourcePersons;
                //-------------------------
                Status status = TrainingManager.SaveUpdateTraining(obj, ListTrainingPlanningLine, sendNotification);//, sendEmail);
                if (status.IsSuccess)
                {
                    //NewMessage.ShowNormalMessage("Training saved successfully.");
                    windowTraining.Close();
                    //BindAllTrainingGrids();

                    //ShowEvents();

                    //CalendarPanel1.Update();

                    Session["Msg"] = "Training saved successfully.";
                    Session["Red"] = "1";
                    Response.Redirect("~/newhr/TrainingMod/TrainingPlanning.aspx");

                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            int trainingId = int.Parse(hdnTrainingId.Text);
            Training obj = TrainingManager.GetTrainingById(trainingId);
           

            if (obj != null)
            {
                txtTrainingName.Text = obj.Name;
                txtStartDate.Text = obj.FromDate.ToString();
                txtEndDate.Text = obj.ToDate.ToString();
                //lblTotalDays.Text = obj.Days.ToString();

                //if(obj.StartTime != null)
                //    tfStartTime.Value = obj.StartTime;

                //if(obj.EndTime != null)
                //    tfEndTime.Value = obj.EndTime;

                //if(obj.Hours != null)
                //    lblTotalHours.Text = obj.Hours.ToString();

                if (obj.Type != null)
                    cmbTrainingType.SetValue(obj.Type.ToString());

                if (obj.AvailableSeats != null)
                    txtSeats.Text = obj.AvailableSeats.ToString();

                //if (obj.LevelId != null)
                //    cmbLevel.SetValue(obj.LevelId.ToString());
                //else
                //    cmbLevel.SelectedItem.Text = "";

                mcmbLevel.SelectedItems.Clear();
                if (obj.LevelIds != null)
                {
                    string[] spl = obj.LevelIds.Split(',');
                    foreach (var item in spl)
                    {
                        mcmbLevel.SelectedItems.Add(new Ext.Net.ListItem() { Value = item });
                    }
                    mcmbLevel.UpdateSelectedItems();
                }
                else
                    mcmbLevel.Text = "";               

                if (obj.Venue != null)
                    txtVenue.Text = obj.Venue;

                if (obj.Host != null)
                    txtHost.Text = obj.Host;

                if (obj.Notes != null)
                    txtNotes.Text = obj.Notes;

                if (obj.Country != null)
                    cmbCountry.SetValue(obj.Country.ToString());

                if (obj.City != null)
                    txtCity.Text = obj.City;

                if (obj.ResourcePerson != null)
                    txtResourcePerson.Text = obj.ResourcePerson;

                CalendarPanel1.SetStartDate(obj.FromDate.Value);

                TrainingRequests(trainingId);

                btnSave1.Text = "Update";
                //Bind Training Lines
                List<TrainingPlanningLine> _listTrainingList = TrainingManager.GetTrainingLines(trainingId);
                List<TrainingPlanningLine> _listTrainingLineForTotal = _listTrainingList.Where(x => x.IsTraining == true).ToList();

                foreach (TrainingPlanningLine item in _listTrainingList)
                {
                    if (item.StartTime != null)
                    {
                        DateTime inTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, item.StartTime.Value.Seconds);
                        //  item.InTimeString = inTimeDateTime.ToString();
                        item.InTimeDT = inTimeDateTime;
                    }
                    
                    if (item.EndTime != null)
                    {
                        DateTime outTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.EndTime.Value.Hours, item.EndTime.Value.Minutes, item.EndTime.Value.Seconds);
                        //  item.OutTimeString = outTimeDateTime.ToString();
                        item.OutTimeDT = outTimeDateTime;
                    }
                }

                if (!string.IsNullOrEmpty(obj.ResourcePerson))
                {
                    dispRP.Show();
                    lblRP.Show();
                    dispRP.Text = obj.ResourcePerson;
                }


                

                int TotalHours = 0, TotalMins = 0;
                foreach (TrainingPlanningLine element in _listTrainingLineForTotal)
                {
                    if (element.Hours != null)
                    {
                        string[] str = element.Hours.Split(':').ToArray();
                        TotalHours = TotalHours + int.Parse(str[0]);
                        TotalMins = TotalMins + int.Parse(str[1]);
                    }
                }

                //   DateTime StartDate = DateTime.Parse("2008-01-01" + element.StartTime.ToString());
                //   TimeSpan StartTime= new TimeSpan(StartDate.Hour, StartDate.Minute, 0);

                //   DateTime EndDate = DateTime.Parse("2008-01-01" + element.EndTime.ToString());
                //   TimeSpan EndTime = new TimeSpan(EndDate.Hour, EndDate.Minute, 0);

                //    element.StartTime = StartTime;
                //    element.EndTime = EndTime;
                    
                    
                //}


                if(_listTrainingLineForTotal.Count>0)

                lblDaysTotal.Text = _listTrainingLineForTotal.Count() + " Days";
                if(obj.ResourcePerson!=null)
                lblTotalRP.Text = obj.ResourcePerson.Split(',').ToList().Count() + " resource persons";

                string strTime = "";
                if(TotalHours==0 && TotalMins!=0)
                strTime = TotalMins +  " Mins";

                if(TotalMins==0 && TotalHours!=0)
                strTime = TotalHours + " Hrs";

                if (TotalHours != 0 && TotalMins != 0)
                strTime = TotalHours + " Hrs  " + TotalMins + " Mins";

                lblTotalHours.Text = strTime;

                gridAttRequest.GetStore().DataSource = _listTrainingList;
                gridAttRequest.GetStore().DataBind();
                windowTraining.Center();
                windowTraining.Show();
            }
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int trainingId = int.Parse(hdnTrainingId.Text);
            Status status = TrainingManager.DeleteTraining(trainingId);
            if (status.IsSuccess)
            {
                //NewMessage.ShowNormalMessage("Training deleted successfully.");
                //BindAllTrainingGrids();
                //CalendarPanel1.Update();
                //ShowEvents();

                Session["Msg"] = "Training deleted successfully.";
                Session["Red"] = "1";
                Response.Redirect("~/newhr/TrainingMod/TrainingPlanning.aspx");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnPublish_Click(object sender, DirectEventArgs e)
        {
            int trainingId = int.Parse(hdnTrainingId.Text);
            Status status = TrainingManager.PublishTraining(trainingId);
            if (status.IsSuccess)
            {
                //NewMessage.ShowNormalMessage("Training published successfully.");
                //CalendarPanel1.Update();
                //BindAllTrainingGrids();
                Session["Msg"] = "Training published successfully.";
                Session["Red"] = "1";
                Response.Redirect("~/newhr/TrainingMod/TrainingPlanning.aspx");
                
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnUnpublish_Click(object sender, DirectEventArgs e)
        {
            int trainingId = int.Parse(hdnTrainingId.Text);

            List<TrainingRequest> listApproved = TrainingManager.GetTrainingRequestByStatusAndTrainingId(trainingId, (int)TrainingRequestStatusEnum.Approved);
            List<TrainingRequest> listRequested = TrainingManager.GetTrainingRequestByStatusAndTrainingId(trainingId, (int)TrainingRequestStatusEnum.Pending);

            dispUnpubilshTR.Text = "";
            dispUnpublishTA.Text = "";
            cbUnpublishSendNotific.Checked = false;
            cbUnpublishSendEmail.Checked = false;

            dispUnpubilshTR.Hide();
            dispUnpublishTA.Hide();
            cbUnpublishSendNotific.Hide();
            cbUnpublishSendEmail.Hide();

            if (listApproved.Count > 0 || listRequested.Count > 0)
            {
                dispUnpubilshTR.Text = "Requested : " + (listRequested.Count + listApproved.Count);

                dispUnpublishTA.Text = "Approved : " + listApproved.Count;

                dispUnpubilshTR.Show();
                dispUnpublishTA.Show();
                cbUnpublishSendNotific.Show();
                cbUnpublishSendEmail.Show();
            }

            WUnPublish.Show();
           
        }

        [DirectMethod]
        public void CalculateEffeciveHours()
        {
            double startTime = 0, endTime = 0;

            //if (tfEndTime.SelectedTime.TotalHours == 9)
            //    return;

            //startTime = tfStartTime.SelectedTime.TotalHours;
            //endTime = tfEndTime.SelectedTime.TotalHours;

            if (startTime > endTime)
            {
                NewMessage.ShowWarningMessage("Start time cannot be greater than end time.");
                return;
            }


            //lblTotalHours.Text = (endTime - startTime).ToString();
            //hdnTotalHours.Text = lblTotalHours.Text;
        }

        protected void BindTrainingEventsStore(object s, StoreReadDataEventArgs e)
        {
            BindTrainings();
        }

        protected void BindTrainingEventsHistoryStore(object s, StoreReadDataEventArgs e)
        {
            BindTrainingEventHistory();
        }

        protected void BindTrainingUnpublishedStore(object s, StoreReadDataEventArgs e)
        {
            BindTrainingUnpublished();
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            //if (string.IsNullOrEmpty(txtDateFilterFrom.Text.Trim()))
            //    return;

            BindTrainingEventHistory();
        }

        void RegisterLegendColors()
        {
            StringBuilder str = new StringBuilder("\nlegends = new Array();");

            //add holiday legend color also
            str.AppendFormat("legends['{0}'] = '{1}';", ((int)TrainingStatusEnum.Publish).ToString(), "green");
            str.AppendFormat("legends['{0}'] = '{1}';", ((int)TrainingStatusEnum.Saved).ToString(), "black");
            str.AppendFormat("legends['{0}'] = '{1}';", ((int)TrainingStatusEnum.Unpublish).ToString(), "red");

            X.ResourceManager.RegisterBeforeClientInitScript(str.ToString());

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LegendColorReg123", str.ToString(), true);
        }


        void RegisterTrainingCalendarColorStyles()
        {
            StringBuilder str = new StringBuilder();

            var store = (Ext.Net.CalendarStore)X.GetCtl("GroupStore1");
            //List<Training> list = TrainingManager.GetAllTrainings();

            //foreach (Training obj in list)
            //{
            //    if(obj.Status.Value == (int)TrainingStatusEnum.Publish)
            //        str.Append(GetTrainingCalendarStyle(obj.Status.Value, "green"));
            //    else if(obj.Status.Value == (int)TrainingStatusEnum.Saved)
            //        str.Append(GetTrainingCalendarStyle(obj.Status.Value, "black"));
            //    else
            //        str.Append(GetTrainingCalendarStyle(obj.Status.Value, "red"));

            //    CalendarModel g = new CalendarModel();
            //    g.CalendarId = obj.TrainingID;
            //    g.Title = obj.Name;

            //    store.Calendars.Add(g);
            //}

            CalendarModel g1 = new CalendarModel();
            g1.CalendarId = 1;
            g1.Title = "";
            str.Append(GetTrainingCalendarStyle(1, "green"));
            store.Calendars.Add(g1);

            CalendarModel g2 = new CalendarModel();
            g2.CalendarId = 1;
            g2.Title = "";
            str.Append(GetTrainingCalendarStyle(2, "black"));
            store.Calendars.Add(g2);

            CalendarModel g3 = new CalendarModel();
            g3.CalendarId = 1;
            g3.Title = "";
            str.Append(GetTrainingCalendarStyle(3, "red"));
            store.Calendars.Add(g3);

            store.DataBind();

            X.ResourceManager.RegisterClientStyleBlock("CalendarStyle", str.ToString());
        }



        public string GetTrainingCalendarStyle(int id, string color)
        {
            //http://forums.ext.net/showthread.php?10566-CLOSED-CalendarPanel-GroupStore-and-CalendarId
            string style =
                @"
                    
            .ext-color-{0},
            .ext-ie .ext-color-{0}-ad,
            .ext-opera .ext-color-{0}-ad {{
                color: {1};
            }}
            .ext-cal-day-col .ext-color-{0},
            .ext-dd-drag-proxy .ext-color-{0},
            .ext-color-{0}-ad,
            .ext-color-{0}-ad .ext-cal-evm,
            .ext-color-{0} .ext-cal-picker-icon,
            .ext-color-{0}-x dl,
            .ext-color-{0}-x .ext-cal-evb {{
                background: {1};
            }}
            .ext-color-{0}-x .ext-cal-evb,
            .ext-color-{0}-x dl {{
                border-color: #7C3939;
            }}
                ";

            return string.Format(style, id, color);
        }

        private void TrainingRequests(int trainingId)
        {
            List<TrainingRequest> listApproved = TrainingManager.GetTrainingRequestByStatusAndTrainingId(trainingId, (int)TrainingRequestStatusEnum.Approved);
            List<TrainingRequest> listRequested = TrainingManager.GetTrainingRequestByStatusAndTrainingId(trainingId, (int)TrainingRequestStatusEnum.Pending);
            if (listApproved.Count > 0 || listRequested.Count > 0)
            {
                dispTrainingRequestCount.Text = "Requested : " + (listRequested.Count + listApproved.Count);

                dispTrainingApprovedCount.Text = "Approved : " + listApproved.Count;

                dispTrainingRequestCount.Show();
                dispTrainingApprovedCount.Show();
                cbSendEmail.Show();
                cbChangeNotification.Show();
            }
        }

        protected void btnUnpubl_Click(object sender, DirectEventArgs e)
        {
            int trainingId = int.Parse(hdnTrainingId.Text);

            bool sendNotification = false, sendEmail = false;

            if (cbUnpublishSendNotific.Checked)
                sendNotification = true;

            //if (cbUnpublishSendEmail.Checked)
            //    sendEmail = true;


            Status status = TrainingManager.UnpublishTraining(trainingId, sendNotification);
            if (status.IsSuccess)
            {
                //NewMessage.ShowNormalMessage("Training unpublished successfully.");
                WUnPublish.Close();
                //BindAllTrainingGrids();
                //CalendarPanel1.Update();
                Session["Msg"] = "Training unpublished successfully.";
                Session["Red"] = "1";
                Response.Redirect("~/newhr/TrainingMod/TrainingPlanning.aspx");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            DateTime? dateFilterFrom = null, dateFilterTo = null;
            if (!string.IsNullOrEmpty(txtDateFilterFrom.Text.Trim()))
                dateFilterFrom = DateTime.Parse(txtDateFilterFrom.Text.Trim());

            if (!string.IsNullOrEmpty(txtDateFilterTo.Text.Trim()))
                dateFilterTo = DateTime.Parse(txtDateFilterTo.Text.Trim());


            List<Training> list = TrainingManager.GetTrainingEventsHistory(dateFilterFrom, dateFilterTo).ToList();

            foreach (var item in list)
            {
                item.FromDateEnlgish = item.FromDate.Value.ToString("yyyy-MM-dd");
                item.ToDateEnglish = item.ToDate.Value.ToString("yyyy-MM-dd");
            }


            ExcelHelper.ExportToExcel("Training Event History List", list,
                new List<String>() { "TrainingID", "FromDate", "ToDate", "Days", "FromDateEng", "ToDateEng", "CreatedBy", "CreatedOn", "ModifiedBy", "ModifiedOn", "StartTime", "EndTime", "Hours", "Type", "Status", "LevelIds" }, // hidden
                new List<String>() { }, //total
                new Dictionary<string, string>() { { "Name", "Training Name" }, { "FromDateEnlgish", "From Date" }, { "ToDateEnglish", "To Date" }, { "TrainingType", "Type" }, { "ResourcePerson", "Resource Person" }, { "AvailableSeats", "Capacity" }, { "BookedSeats", "Enrollment" }, { "LeftSeats", "Left Seats" } },
                new List<string>() { },
                new Dictionary<string, string>() { },
                new List<string> { "Name", "FromDateEnlgish", "ToDateEnglish", "TrainingType", "Host", "ResourcePerson", "Venue", "AvailableSeats", "BookedSeats", "LeftSeats" });



        }
       
    }
}
