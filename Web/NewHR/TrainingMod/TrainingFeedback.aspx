﻿<%@ Page Title="Training Feedback" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="TrainingFeedback.aspx.cs" Inherits="Web.NewHR.TrainingMod.TrainingFeedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

      var CommandHandler = function(command, record){

            <%= hdnEmployeeId.ClientID %>.setValue(record.data.EmployeeID);
            <%= hdnTrainingId.ClientID %>.setValue(record.data.TrainingId);

             if(command=="feedback")
                {
                    <%= lnkFeedback.ClientID %>.fireEvent('click');
                }
           }


       function searchList() {
            <%=gridTrainingEmp.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    </script>
    <style type="text/css">
        .divTrainingInfo
        {
            margin-top: 12px;
            margin-left: 10px;
            background-color: #DDEBF7;
            padding: 10px;
            width: 1069px;
        }
        .clsTrainingName .x-form-display-field
        {
            color: #428BCA !important;
        }
        
        .clsspnOrganizedBy .x-form-display-field
        {
            font-style: italic !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Training Feedback
                </h4>
            </div>
        </div>
    </div>
    <ext:LinkButton ID="lnkFeedback" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="lnkFeedback_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="contentpanel">
        <ext:Hidden ID="hdnTrainingId" runat="server" />
        <ext:Hidden ID="hdnEmployeeId" runat="server" />
        <ext:Hidden ID="hdnLineID" runat="server" />
        <ext:Hidden ID="hdnStartTime" runat="server" />
        <ext:Hidden ID="hdnEndTime" runat="server" />
        <ext:Hidden ID="hdnDate" runat="server" />
        <ext:Hidden ID="hdnTotalDays" runat="server" />
        <table class="fieldTable" style="margin-top:0px">
            <tr>
                <td>
                    <ext:ComboBox ID="cmbTraining" ForceSelection="true" runat="server" Width="200" FieldLabel="Training" LabelAlign="Top" 
                        LabelSeparator="" QueryMode="Local" DisplayField="Name" ValueField="TrainingID">
                        <Store>
                            <ext:Store ID="store1" runat="server">
                                <Fields>
                                    <ext:ModelField Name="TrainingID" />
                                    <ext:ModelField Name="Name" />
                                </Fields>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                        <DirectEvents>
                            <Select OnEvent="cmbTraining_Change">
                                <EventMask ShowMask="true">
                                </EventMask>
                            </Select>
                        </DirectEvents>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model9" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox ID="cmbEmployee" FieldLabel="Employee" LabelWidth="150" LabelAlign="Top"
                        LabelSeparator="" runat="server" DisplayField="Name" ValueField="EmployeeId"
                        StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                        MinChars="1" TriggerAction="All" ForceSelection="true">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;margin-top:20px">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="padding-top: 22px">
                    <ext:Button runat="server" ID="btnLoad" Cls="btn btn-primary" Text="Load">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <div class="divTrainingInfo" id="divTrainingLabel" style="display:none">
            <table>
                <tr>
                    <td>
                        <ext:DisplayField ID="spnTrainingName" Cls="clsTrainingName" runat="server">
                        </ext:DisplayField>
                    </td>
                    <td>
                        <ext:DisplayField ID="spnOrganizedBy" Cls="clsspnOrganizedBy" runat="server">
                        </ext:DisplayField>
                    </td>
                    <td>
                        <ext:DisplayField ID="spnDays" runat="server">
                        </ext:DisplayField>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding-left: 10px; padding-top: 10px">
            <table>
                <tr>
                    <td>
                        <ext:DisplayField ID="lblParticipants" runat="server" Hidden="true" Text="No of participants &nbsp;">
                        </ext:DisplayField>
                    </td>
                    <td>
                        <ext:DisplayField ID="dispNoOfparticipants" runat="server">
                        </ext:DisplayField>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:DisplayField ID="DisplayField1" runat="server"  Text="Ratings">
                        </ext:DisplayField>
                    </td>
                </tr>
            </table>
        </div>
        <ext:GridPanel ID="gridTotalCount" Border="true" StripeRows="true" ForceFit="false" StyleSpec="margin-left:10px" Width="1070"
            AutoScroll="true"  runat="server" Cls="gridtbl">
            <Store>
                <ext:Store runat="server" ID="storeTotalCount">
                    <Reader>
                        <ext:ArrayReader>
                        </ext:ArrayReader>
                    </Reader>
                    <Model>
                        <ext:Model ID="ModelTotalCount" runat="server">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                             
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>

             <ColumnModel runat="server" ID="ColumnModelCount">
                <Columns>
                    <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" Text="EIN" Width="50" ColumnID="EmployeeId" Hidden="true"
                        runat="server" DataIndex="EmployeeId"></ext:Column>
                </Columns>
            </ColumnModel>

            <SelectionModel>
                <ext:CellSelectionModel ID="RowSelectionModel1" runat="server">
                </ext:CellSelectionModel>
            </SelectionModel>
           
        </ext:GridPanel>

        <ext:GridPanel ID="gridTrainingEmp" runat="server" MinHeight="0" Width="1070" StyleSpec="margin-top:10px;margin-left:10px">
            <Store>
                <ext:Store ID="storeTrainingEmp" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
                    PageSize="50" RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model2" runat="server">
                            <Fields>
                                <ext:ModelField Name="SN" />
                                <ext:ModelField Name="Name" />
                                <ext:ModelField Name="Branch" />
                                <ext:ModelField Name="Department" />
                                <ext:ModelField Name="Position" />
                                <ext:ModelField Name="Rating" />
                                <ext:ModelField Name="SLBefore" />
                                <ext:ModelField Name="SLAfter" />
                                <ext:ModelField Name="Comment" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:Column ID="Column1" runat="server" Text="SN" Width="40" DataIndex="SN" MenuDisabled="true"
                        Sortable="false">
                    </ext:Column>
                    <ext:Column ID="colEmployeeName" runat="server" Text="Name" Width="100" DataIndex="Name"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:Column ID="Column2" runat="server" Text="Branch" Width="100" DataIndex="Branch"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:Column ID="asdfasdf" Width="100" runat="server" Text="Department" DataIndex="Department"
                        Sortable="false" MenuDisabled="true">
                    </ext:Column>
                    <ext:Column ID="Column3" runat="server" Text="Position" Width="150" DataIndex="Position"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:Column ID="Column4" runat="server" Text="Rating" Width="100" DataIndex="Rating"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:Column ID="Column5" runat="server" Text="SL Before" Width="110" DataIndex="SLBefore"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:Column ID="Column6" runat="server" Text="SL After" Width="110" DataIndex="SLAfter"
                        MenuDisabled="true" Sortable="false">
                    </ext:Column>
                    <ext:Column ID="Column7" runat="server" Text="Comment" Width="110" DataIndex="Comment"
                        Flex="1" MenuDisabled="true" Sortable="false">
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" Mode="Single" />
            </SelectionModel>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeTrainingEmp"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="1">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
    </div>
    <ext:Window ID="windowFeedback" runat="server" Title="Training Feedback" Icon="None"
        Height="400" Width="400" BodyPadding="5" Hidden="true" Modal="false">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td colspan="2">
                        <ext:DisplayField ID="dispTrainingName" runat="server" Style="color: #47759E; font-size: 15px;
                            font-weight: bold;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span>My skill level(in a scale of 5)</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:NumberField ID="txtValueBeforeTraining" MinValue="0" MaxValue="5" runat="server"
                            LabelAlign="Top" LabelSeparator="" FieldLabel="Before the training">
                        </ext:NumberField>
                    </td>
                    <td>
                        <ext:NumberField ID="txtValueAfterTraining" MinValue="0" MaxValue="5" runat="server"
                            LabelAlign="Top" LabelSeparator="" FieldLabel="After the training">
                        </ext:NumberField>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:ComboBox ID="cmbRating" runat="server" Width="150" FieldLabel="My Rating" LabelAlign="Top"
                            LabelSeparator="" QueryMode="Local" DisplayField="RatingName" ValueField="RatingID">
                            <Store>
                                <ext:Store ID="store2" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="RatingID" />
                                        <ext:ModelField Name="RatingName" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                    <td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextArea ID="txtNotes" runat="server" Height="80" Width="314" FieldLabel="Additional remarks"
                            LabelAlign="Top" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="padding-right: 20px;">
                                    <ext:Button runat="server" Cls="btn btn-primary" StyleSpec="margin-top:20px;" ID="btnSave"
                                        Text="<i></i>Save">
                                        <DirectEvents>
                                            <Click OnEvent="btnSavefeedback_Click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click Handler="valGroup = 'SaveTrainingReq'; return CheckValidation();">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td style="padding-top: 20px;">
                                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton1" Cls="btnFlat"
                                        BaseCls="btnFlat" Text="<i></i>Cancel">
                                        <Listeners>
                                            <Click Handler="#{windowFeedback}.hide();">
                                            </Click>
                                        </Listeners>
                                    </ext:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
