﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils.Calendar;
using Newtonsoft.Json.Linq;

namespace Web.NewHR.TrainingMod
{
    public partial class TrainingFeedback : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            BindTrainingCombo();
            //bindTrainingCount(2);
            
           
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int TrainingID = -1;
            int EmpID = -1;
            

            if (!string.IsNullOrEmpty(cmbTraining.SelectedItem.Value))
                TrainingID = int.Parse(cmbTraining.SelectedItem.Value);

            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbEmployee.SelectedItem.Value))
                EmpID = int.Parse(cmbEmployee.SelectedItem.Value);

            //bind TrainingInfo----
            //---------------------
            List<GetTrainingFeedbackOfEmployeeResult> list = TrainingManager.GetTrainingFeedbackOfEmployee(e.Start / pageSize, pageSize, EmpID, TrainingID);

            List<TrainingFeedbackBo> _listTrainingFeedbackBo = new List<TrainingFeedbackBo>();

            foreach (GetTrainingFeedbackOfEmployeeResult _item in list)
            {
                TrainingFeedbackBo _obj = new TrainingFeedbackBo();
                _obj.Name = _item.EmployeeName;
                _obj.SN = _item.RowNumber.ToString();
                 int BranchId = BranchManager.GetEmployeeCurrentBranch(_item.EmployeeID, DateTime.Now.Date);
                _obj.Branch = BranchManager.GetBranchById(BranchId).Name;
                _obj.Department = TrainingManager.GetCurrentDepartmentOfEmployee(_item.EmployeeID);

                  int? desigId  =OvertimeManager.GetEmployeeCurrentDesignation(_item.EmployeeID, DateTime.Now.Date);

                  if (desigId != null)
                  {
                      EDesignation desig = CommonManager.GetDesigId(desigId.Value);
                      _obj.Position = desig.LevelAndDesignation;
                  }

                _obj.Rating = _item.RatingText;
                if (_item.ScoreBeforeTraining != null)
                _obj.SLBefore = _item.ScoreBeforeTraining.ToString();
                if (_item.ScoreAfterTraining != null)
                _obj.SLAfter = _item.ScoreAfterTraining.ToString();
                _obj.Comment = _item.Note;
                
                _listTrainingFeedbackBo.Add(_obj);

            }

            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;

            e.Total = totalRecords;

            storeTrainingEmp.DataSource = _listTrainingFeedbackBo;
            storeTrainingEmp.DataBind();

        }

        protected void cmbTraining_Change(object sender, DirectEventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbTraining.SelectedItem.Value))
            {
                int TrainingId = int.Parse(cmbTraining.SelectedItem.Value);
                BindTrainingInfo(TrainingId);
                bindTrainingCount(TrainingId);

                PagingToolbar1.DoRefresh();
            }
        }


        Column GetExtGridPanelColumn(string indexId, string headerName)
        {
            Column column = new Column();
            //column.ColumnID = indexId;
            column.DataIndex = indexId;
            column.Text = headerName;
            column.Align = Alignment.Center;
            column.ToolTip = headerName;
            column.MenuDisabled = true;
            column.Sortable = false;
            column.Width = new Unit(120);
            return column;
        }

        protected void bindTrainingCount(int TrainingID)
        {
            List<GetTrainingRatingCountResult> list = TrainingManager.GetTrainingRatingCount(TrainingID);
            if (list.Count == 0)
            {
                gridTotalCount.Hide();
                return;
            }
            gridTotalCount.Show();

            //ColumnModel columnModelProjects = gridProjects.ColumnModel;
            string columnId = "P_{0}";
            foreach (var item in list)
            {

                string gridIndexId = string.Format(columnId, item.RatingName);

                ModelField field = new ModelField(gridIndexId, ModelFieldType.Float);
                field.UseNull = true;
                //field. = true;
                ModelTotalCount.Fields.Add(gridIndexId, ModelFieldType.Float);
                // Add Checkbox
                gridTotalCount.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, item.RatingName));
            }


            //ModelProject.Fields.Add("Total", ModelFieldType.Float);
            //gridProjects.ColumnModel.Columns.Add(GetExtGridPanelColumn("Total", "Total"));


            // Generate data array & bind to Store
            // Add each  id as key
            Dictionary<string, double?> selection = new Dictionary<string, double?>();

            //foreach (var item in projectPays)
            //{
            //    selection.Add(item.EmployeeId + "_" + item.ProjectId, item.HoursOrDays);
            //}

            object[] data = new object[1];

            // Iterate for each Project & Skip first as it represents All Project
            object[] rowData = new object[list.Count + 1];
            rowData[0] = 0;


            for (int i = 0; i < list.Count; i++)
            {
                //first one for EIN, second for Name & last for Total




                rowData[i + 1] = list[i].RatingCount;
                //rowData[2] = list[1].RatingCount;

                // rowData[i] = list[i].RatingCount;


            }

            data[0] = rowData;
            //int gridwidth = gridProjects.ColumnModel.Columns.Count * 120;
            //gridProjects.Width = gridwidth;
           // gridTotalCount.Reconfigure();
            storeTotalCount.DataSource = data;
            //gridTotalCount.Reconfigure();
            gridTotalCount.Reconfigure(gridTotalCount.GetStore(), gridTotalCount.ColumnModel.Columns);
            //storeTotalCount.DataBind();

        }


        private object[] DataValue
        {
            get
            {
                return new object[]
            {
                new object[] { "1","1", "2"},
                
            };
            }
        }

        protected void lnkFeedback_Click(object sender, DirectEventArgs e)
        {
            ClearFeedbackWindow();
            windowFeedback.Show();
            BindcomboRatingText();
            GetFeedBackInfo();
            
        }

        protected void BindTrainingInfo(int TrainingId)
        {

            spnTrainingName.Text = "";
            spnDays.Text = "";
            spnOrganizedBy.Text = "";
            dispNoOfparticipants.Text = "";
            int traingingParticipant = TrainingManager.GetTrainingParticipantCount(TrainingId);
            if (traingingParticipant > 0)
            {
                dispNoOfparticipants.Text = traingingParticipant.ToString();
                lblParticipants.Show();
            }
            else
                lblParticipants.Hide();

            Training obj = TrainingManager.GetTrainingById(TrainingId);
            if (obj != null)
            {
               string Jsscript=  "document.getElementById('divTrainingLabel').style.display='block';";
               X.AddScript(Jsscript);
                int TotalDay = TrainingManager.GetTrainingLines(TrainingId).Where(x => x.IsTraining == true).Count();
                spnTrainingName.Text = obj.Name;
                spnDays.Text = " (" +  TotalDay.ToString() + " days starting from "  + obj.FromDate.Value.Day.ToString() + "-" + 
                    obj.ToDate.Value.Day.ToString() + " " + DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + ", " + 
                    obj.FromDate.Value.Year.ToString() + ")";
                string strHost = "";
                if (!string.IsNullOrEmpty(obj.Host))
                    strHost = "&nbsp;Organized by " + obj.Host;

                if (!string.IsNullOrEmpty(obj.City))
                    strHost = strHost + " in " + obj.City;

                if (obj.Country != null)
                {
                    string CountryName = CommonManager.GetCountryById(obj.Country.Value).CountryName;
                    strHost = strHost + ", " + CountryName + " ";
                }

                spnOrganizedBy.Text = strHost;//"Organized by " +  obj.Host + " " + obj.City + " ," + obj.Country;
            }

            
        }

        protected void ClearFeedbackWindow()
        {
            txtNotes.Clear();
            txtValueAfterTraining.Clear();
            txtValueBeforeTraining.Clear();
            dispTrainingName.Clear();
            cmbRating.ClearValue();
        }

        protected void GetFeedBackInfo()
        {
            int TrainingID = 0;
            int EmployeeID = 0;

            if (!string.IsNullOrEmpty(hdnTrainingId.Text))
            {
                TrainingID = int.Parse(hdnTrainingId.Text);

                Training obj = TrainingManager.GetTrainingById(TrainingID);
                if (obj != null)
                {
                    dispTrainingName.Html = obj.Name;
                }

                if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                    EmployeeID = int.Parse(hdnEmployeeId.Text);


                if (TrainingID == 0 || EmployeeID == 0)
                    return;

                TrainingRating dbTrainingRating = TrainingManager.GetTrainingRatingOfEmployeeByTraining(TrainingID, EmployeeID);
                if (dbTrainingRating != null)
                {
                    if (dbTrainingRating.ScoreBeforeTraining != null)
                        txtValueBeforeTraining.Text = dbTrainingRating.ScoreBeforeTraining.ToString();

                    if (dbTrainingRating.ScoreAfterTraining != null)
                        txtValueAfterTraining.Text = dbTrainingRating.ScoreAfterTraining.ToString();

                    if (dbTrainingRating.MyRatingRef_Id != null)
                        cmbRating.SetValue(dbTrainingRating.MyRatingRef_Id);

                    if (!string.IsNullOrEmpty(dbTrainingRating.Note))
                        txtNotes.Text = dbTrainingRating.Note;
                }

            }
        }


        protected void btnSavefeedback_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveTrainingReq");

            if (Page.IsValid)
            {
                TrainingRating obj = new TrainingRating();

                if (!string.IsNullOrEmpty(hdnTrainingId.Text))
                    obj.TrainingID = int.Parse(hdnTrainingId.Text);

                if (!string.IsNullOrEmpty(hdnEmployeeId.Text))
                    obj.EmployeeId = int.Parse(hdnEmployeeId.Text);

                if (!string.IsNullOrEmpty(cmbRating.SelectedItem.Value))
                    obj.MyRatingRef_Id = int.Parse(cmbRating.SelectedItem.Value);

                if (!string.IsNullOrEmpty(txtValueBeforeTraining.Text.Trim()))
                    obj.ScoreBeforeTraining = int.Parse(txtValueBeforeTraining.Text.Trim());

                if (!string.IsNullOrEmpty(txtValueAfterTraining.Text.Trim()))
                    obj.ScoreAfterTraining = int.Parse(txtValueAfterTraining.Text.Trim());

                if (!string.IsNullOrEmpty(txtNotes.Text.Trim()))
                    obj.Note = txtNotes.Text.Trim();

                Status status = TrainingManager.SaveUpdateTrainingFeedback(obj);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Information has been updated successfully.");
                    windowFeedback.Close();
                    gridTrainingEmp.GetStore().Reload();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }    
        
       
        private void BindTrainingCombo()
        {
            List<Training> _list = TrainingManager.GetAllTrainings();
            cmbTraining.GetStore().DataSource = _list;
            cmbTraining.GetStore().DataBind();
        }

        private void BindcomboRatingText()
        {
            List<TrainingRatingText> _list = TrainingManager.GetTrainingRatingText();
            cmbRating.GetStore().DataSource = _list;
            cmbRating.GetStore().DataBind();
        }

  



    }
}