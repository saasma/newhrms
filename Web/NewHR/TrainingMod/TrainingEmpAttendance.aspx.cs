﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils.Calendar;
using Newtonsoft.Json.Linq;

namespace Web.NewHR.TrainingMod
{
    public partial class TrainingEmpAttendance : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
           // int trainingId = 0;
            BindTrainingCombo();
            if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
            {
                cmbTraining.SelectedItem.Value = Request.QueryString["Id"];
                gridEmpAttendance.GetStore().Reload();
            }

            //trainingId = int.Parse(Request.QueryString["Id"].ToString());
            //hdnTrainingId.Text = trainingId.ToString();

            //Training obj = TrainingManager.GetTrainingById(trainingId);


            //if (obj != null)
            //{
            //   // dfTrainingName.Text = obj.Name;
            //   // dfTrainingDetails.Text = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day + ", " + obj.FromDate.Value.Year + " - " + DateHelper.GetMonthName(obj.ToDate.Value.Month, true) + " " + obj.ToDate.Value.Day + ", " + obj.ToDate.Value.Year;//+ " " + AttendanceManager.CalculateManualVisibleTime(obj.StartTime.Value) + " to " + AttendanceManager.CalculateManualVisibleTime(obj.EndTime.Value);
            // //   chkCopyMode.Checked = true;

            //    TimeSpan ts = obj.ToDate.Value - obj.FromDate.Value;

            //    List<TrainingPlanningLine> objTrainingPlanningLineCount = TrainingManager.GetTrainingLines(trainingId).Where(t=>t.IsTraining==true).ToList();
            //    int totalDays = objTrainingPlanningLineCount.Count;//ts.Days;

            //    hdnTotalDays.Text = (totalDays + 1).ToString();

            //    List<GetHolidaysForAttendenceResult> holiday = new HolidayManager().GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, obj.FromDate.Value.Date, obj.ToDate.Value.Date,0).ToList();

            //    string columnId = "Day_{0}";

            //    for (int i = 0; i <= totalDays; i++)
            //    {
            //        string holidayAbbr = "";
            //        GetHolidaysForAttendenceResult objHolidayResult = holiday.SingleOrDefault(x => x.DateEng.Value.Date == obj.FromDate.Value.Date.AddDays(i));
            //        if (objHolidayResult != null && objHolidayResult.Abbreviation == "WH")
            //            holidayAbbr = objHolidayResult.Abbreviation;
            //        else
            //            holidayAbbr = "";

            //        string gridIndexId = string.Format(columnId,i);
            //        ModelField field = new ModelField(gridIndexId, ModelFieldType.String);
            //        field.UseNull = true;

            //   //     Model1.Fields.Add(gridIndexId, ModelFieldType.String);

            //        DateTime dt = obj.FromDate.Value.AddDays(i);
            //        string dayName = dt.Day + "-" + DateHelper.GetMonthShortName(dt.Month, true);
            //     //   gridTrainingAttendance.ColumnModel.Columns.Add(GetExtGridPanelColumn(gridIndexId, dayName, holidayAbbr));
            //    }


            //    List<TrainingRequest> listTR = TrainingManager.GetApprovedTRByTrainingId(trainingId);

            //    object[] data = new object[listTR.Count * 2];
            //    int row = 0;
            //    int sn = 0;


            //    foreach (var item in listTR)
            //    {
            //        object[] rowData = new object[totalDays + 4];

            //        rowData[0] = ++sn;
            //        rowData[1] = item.EmployeeId.Value;
            //        rowData[2] = item.Name;

            //        int m = 3;
            //        for (int k = 0; k <= totalDays; k++)
            //        {                  
            //            string holidayAbbr = "";
            //            GetHolidaysForAttendenceResult objHolidayResult = holiday.SingleOrDefault(x => x.DateEng.Value.Date == obj.FromDate.Value.Date.AddDays(k));
            //            if (objHolidayResult != null && objHolidayResult.Abbreviation == "WH")
            //                holidayAbbr = objHolidayResult.Abbreviation;
            //            else
            //                holidayAbbr = "";


            //            if (holidayAbbr == "")
            //            {
            //                AttendanceGetInAndOutTimeResult objAttendanceGetInAndOutTimeResult = new AttendanceGetInAndOutTimeResult();

            //                objAttendanceGetInAndOutTimeResult = AttendanceManager.getAttendanceofGivenDate(item.EmployeeId.Value, obj.FromDate.Value.AddDays(k));

            //                if (objAttendanceGetInAndOutTimeResult != null && objAttendanceGetInAndOutTimeResult.InDate != null)
            //                {
            //                    DateTime inTimeDateTime = new DateTime(objAttendanceGetInAndOutTimeResult.InDate.Value.Year, objAttendanceGetInAndOutTimeResult.InDate.Value.Month, objAttendanceGetInAndOutTimeResult.InDate.Value.Day, objAttendanceGetInAndOutTimeResult.InDate.Value.Hour, objAttendanceGetInAndOutTimeResult.InDate.Value.Minute, objAttendanceGetInAndOutTimeResult.InDate.Value.Second);
            //                    rowData[m++] = inTimeDateTime.ToString();

            //                }
            //                else
            //                {
            //                    TrainingPlanningLine objTrainingPlanningLine = TrainingManager.GetTrainingLineByIdAndDate(trainingId, obj.FromDate.Value);
            //                    DateTime inTimeDateTime = new DateTime(obj.FromDate.Value.Year, obj.FromDate.Value.Month, obj.FromDate.Value.Day, objTrainingPlanningLine.StartTime.Value.Hours, objTrainingPlanningLine.StartTime.Value.Minutes, objTrainingPlanningLine.StartTime.Value.Seconds);
            //                    rowData[m++] = inTimeDateTime.ToString();
            //                }
            //            }
            //            else
            //                rowData[m++] = "WH";

            //        }

            //        data[row++] = rowData;

            //        object[] rowDataOut = new object[totalDays + 4];

            //        rowDataOut[0] = ++sn;
            //        rowDataOut[1] = item.EmployeeId.Value;
            //        rowDataOut[2] = "";
            //        int n = 3;

            //        for (int k = 0; k <= totalDays; k++)
            //        {

            //            string holidayAbbr = "";
            //            GetHolidaysForAttendenceResult objHolidayResult = holiday.SingleOrDefault(x => x.DateEng.Value.Date == obj.FromDate.Value.Date.AddDays(k));
            //            if (objHolidayResult != null && objHolidayResult.Abbreviation == "WH")
            //                holidayAbbr = objHolidayResult.Abbreviation;
            //            else
            //                holidayAbbr = "";

            //            if (holidayAbbr == "")
            //            {

            //                AttendanceGetInAndOutTimeResult objAttendanceGetInAndOutTimeResult = new AttendanceGetInAndOutTimeResult();

            //                objAttendanceGetInAndOutTimeResult = AttendanceManager.getAttendanceofGivenDate(item.EmployeeId.Value, obj.FromDate.Value.AddDays(k));

            //                if (objAttendanceGetInAndOutTimeResult != null && objAttendanceGetInAndOutTimeResult.OutDate != null)
            //                {
            //                    DateTime outTimeDateTime = new DateTime(objAttendanceGetInAndOutTimeResult.OutDate.Value.Year, objAttendanceGetInAndOutTimeResult.OutDate.Value.Month, objAttendanceGetInAndOutTimeResult.OutDate.Value.Day, objAttendanceGetInAndOutTimeResult.OutDate.Value.Hour, objAttendanceGetInAndOutTimeResult.OutDate.Value.Minute, objAttendanceGetInAndOutTimeResult.OutDate.Value.Second);
            //                    rowDataOut[n++] = outTimeDateTime.ToString();
            //                }
            //                else
            //                {
            //                    TrainingPlanningLine objTrainingPlanningLine = TrainingManager.GetTrainingLineByIdAndDate(trainingId, obj.ToDate.Value);
            //                    DateTime outTimeDateTime = new DateTime(obj.ToDate.Value.Year, obj.ToDate.Value.Month, obj.ToDate.Value.Day, objTrainingPlanningLine.EndTime.Value.Hours, objTrainingPlanningLine.EndTime.Value.Minutes, objTrainingPlanningLine.EndTime.Value.Seconds);
            //                    rowDataOut[n++] = outTimeDateTime.ToString();
            //                }
            //            }
            //            else
            //                rowDataOut[n++] = "WH";

            //        }

            //        data[row++] = rowDataOut;

            //    }

            //gridTrainingAttendance.GetStore().DataSource = data;
            //gridTrainingAttendance.GetStore().DataBind();

            //gridTrainingAttendance.Width = ((totalDays + 1) * 100) + 200;

        }

        protected void cmbTraining_Change(object sender, DirectEventArgs e)
        {
            int TrainingID = int.Parse(cmbTraining.SelectedItem.Value);
              Training obj = TrainingManager.GetTrainingById(TrainingID);
              if (obj != null)
              {
                  //dfTrainingName.Text = obj.Name;
                  // dfTrainingDetails.Text = DateHelper.GetMonthName(obj.FromDate.Value.Month, true) + " " + obj.FromDate.Value.Day + ", " + obj.FromDate.Value.Year + " - " + DateHelper.GetMonthName(obj.ToDate.Value.Month, true) + " " + obj.ToDate.Value.Day + ", " + obj.ToDate.Value.Year;//+ " " + AttendanceManager.CalculateManualVisibleTime(obj.StartTime.Value) + " to " + AttendanceManager.CalculateManualVisibleTime(obj.EndTime.Value);
                  //   chkCopyMode.Checked = true;
              }
        }
        private void BindTrainingCombo()
        {
            List<Training> _list = TrainingManager.GetAllTrainings();
            cmbTraining.GetStore().DataSource = _list;
            cmbTraining.GetStore().DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int TrainingID = -1;
            int EmpID = -1;
            DateTime? Date = null;

            if (txtDate.SelectedDate != new DateTime())
                Date = txtDate.SelectedDate;

            if (!string.IsNullOrEmpty(cmbTraining.SelectedItem.Value))
                TrainingID = int.Parse(cmbTraining.SelectedItem.Value);


            if (!string.IsNullOrEmpty(cmbEmployee.SelectedItem.Value))
                EmpID = int.Parse(cmbEmployee.SelectedItem.Value);

            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value); 
            List<GetTrainingEmpAttendnaceResult> list = TrainingManager.GetTrainingEmpAttendnace(e.Start / pageSize, pageSize,EmpID,TrainingID,Date);


            foreach (GetTrainingEmpAttendnaceResult item in list)
            {
                
                if (item.StartTime != null)
                {
                    DateTime inTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, item.StartTime.Value.Seconds);
                    //  item.InTimeString = inTimeDateTime.ToString();
                    item.InTimeDT = inTimeDateTime;
                    
                }

                if (item.EndTime != null)
                {
                    DateTime outTimeDateTime = new DateTime(item.DateEng.Value.Year, item.DateEng.Value.Month, item.DateEng.Value.Day, item.EndTime.Value.Hours, item.EndTime.Value.Minutes, item.EndTime.Value.Seconds);
                    //  item.OutTimeString = outTimeDateTime.ToString();
                    item.OutTimeDT = outTimeDateTime;
                }
                //getAttendnace---

                AttendanceGetInAndOutTimeResult objAttendanceGetInAndOutTimeResult = AttendanceManager.getAttendanceofGivenDate(item.EmployeeId, item.DateEng.Value);
                if (objAttendanceGetInAndOutTimeResult != null && objAttendanceGetInAndOutTimeResult.InDate != null)
                {
                    DateTime inTimeDateTime = new DateTime(objAttendanceGetInAndOutTimeResult.InDate.Value.Year, objAttendanceGetInAndOutTimeResult.InDate.Value.Month, objAttendanceGetInAndOutTimeResult.InDate.Value.Day, objAttendanceGetInAndOutTimeResult.InDate.Value.Hour, objAttendanceGetInAndOutTimeResult.InDate.Value.Minute, objAttendanceGetInAndOutTimeResult.InDate.Value.Second);
                    item.DeviceIn = inTimeDateTime;
                    if (!string.IsNullOrEmpty(objAttendanceGetInAndOutTimeResult.ModificationRemarks) && objAttendanceGetInAndOutTimeResult.ModificationRemarks == "TRA")
                    {
                        item.AttMark = "TRA";
                        item.SaveStatus = "Saved";
                    }
                    
                 
                    //  rowData[m++] = inTimeDateTime.ToString();

                }

                if (objAttendanceGetInAndOutTimeResult != null && objAttendanceGetInAndOutTimeResult.OutDate != null)
                {
                    DateTime OutTimeDateTime = new DateTime(objAttendanceGetInAndOutTimeResult.OutDate.Value.Year, objAttendanceGetInAndOutTimeResult.OutDate.Value.Month, objAttendanceGetInAndOutTimeResult.OutDate.Value.Day, objAttendanceGetInAndOutTimeResult.OutDate.Value.Hour, objAttendanceGetInAndOutTimeResult.OutDate.Value.Minute, objAttendanceGetInAndOutTimeResult.OutDate.Value.Second);
                    item.DeviceOut = OutTimeDateTime;

                    if (!string.IsNullOrEmpty(objAttendanceGetInAndOutTimeResult.ModificationRemarks) && objAttendanceGetInAndOutTimeResult.ModificationRemarks == "TRA")
                    {
                        item.AttMark = "TRA";
                        item.SaveStatus = "Saved";
                    }
                   
                    //  rowData[m++] = inTimeDateTime.ToString();
                }

                item.Department = TrainingManager.GetCurrentDepartmentOfEmployee(item.EmployeeId);
                //---------------
            }


            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;

            e.Total = totalRecords;

            storeEmpAttendance.DataSource = list;
            storeEmpAttendance.DataBind();

        }


        protected void btnResetAttLine_Click(object sender, DirectEventArgs e)
        {
            DateTime Date;
            if (!string.IsNullOrEmpty(hdnEmployeeId.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(hdnLineID.Text.Trim()))
                {
                    TrainingPlanningLine _DbTrainingPlanningLine = TrainingManager.GetTrainingLineByLineId(Guid.Parse(hdnLineID.Text.Trim()));
                    Date = _DbTrainingPlanningLine.DateEng.Value;
                    int EmployeeId = int.Parse(hdnEmployeeId.Text.Trim());
                    AttendanceMapping map = AttendanceManager.GetAttendanceMappingOfEmployee(EmployeeId);
                    //validation
                    AttendanceGetInAndOutTimeResult objAttendanceGetInAndOutTimeResult = AttendanceManager.getAttendanceofGivenDate(EmployeeId, Date);

                    if (objAttendanceGetInAndOutTimeResult == null || objAttendanceGetInAndOutTimeResult.InDate==null)
                    {
                        NewMessage.ShowNormalMessage("No Attendance set for this date.");
                        return;
                    }

                    Status mystatus = TrainingManager.DeleteTrainingAttendanceByDate(Date, map.DeviceId.ToString());
                    if (mystatus.IsSuccess)
                    {
                        NewMessage.ShowNormalMessage("Attendance reset successfully");
                        storeEmpAttendance.Reload();
                    }
                }
            }
        }

        protected void btnSaveLine_Click(object sender, DirectEventArgs e)
        {
            AttendanceCheckInCheckOut obj = new AttendanceCheckInCheckOut();
            DateTime? InTime = null, OutTime = null, Date = null;
            List<AttendanceCheckInCheckOut> chkInOutList = new List<AttendanceCheckInCheckOut>();
            if (!string.IsNullOrEmpty(hdnEmployeeId.Text.Trim()))
            {
                int EmpId = int.Parse(hdnEmployeeId.Text.Trim());

                if (!string.IsNullOrEmpty(hdnLineID.Text.Trim()))
                {
                    TrainingPlanningLine _DbTrainingPlanningLine = TrainingManager.GetTrainingLineByLineId(Guid.Parse(hdnLineID.Text.Trim()));
                    Date = _DbTrainingPlanningLine.DateEng;
                    AttendanceGetInAndOutTimeResult objAttendanceGetInAndOutTimeResult = AttendanceManager.getAttendanceofGivenDate(EmpId,Date.Value);

                    if (objAttendanceGetInAndOutTimeResult != null && objAttendanceGetInAndOutTimeResult.InDate != null)
                    {
                        NewMessage.ShowNormalMessage("Attendance already set for this date.");
                        return;
                    }

                    if (_DbTrainingPlanningLine.StartTime != null)
                    {
                        DateTime inTimeDateTime = new DateTime(_DbTrainingPlanningLine.DateEng.Value.Year, _DbTrainingPlanningLine.DateEng.Value.Month, _DbTrainingPlanningLine.DateEng.Value.Day, _DbTrainingPlanningLine.StartTime.Value.Hours, _DbTrainingPlanningLine.StartTime.Value.Minutes, _DbTrainingPlanningLine.StartTime.Value.Seconds);
                        InTime = inTimeDateTime;

                    }

                    if (_DbTrainingPlanningLine.EndTime != null)
                    {
                        DateTime outTimeDateTime = new DateTime(_DbTrainingPlanningLine.DateEng.Value.Year, _DbTrainingPlanningLine.DateEng.Value.Month, _DbTrainingPlanningLine.DateEng.Value.Day, _DbTrainingPlanningLine.EndTime.Value.Hours, _DbTrainingPlanningLine.EndTime.Value.Minutes, _DbTrainingPlanningLine.EndTime.Value.Seconds);
                        OutTime = outTimeDateTime;
                    }

                   
                }

                //if (!string.IsNullOrEmpty(hdnStartTime.Text.Trim()))
                //    InTime = Convert.ToDateTime(hdnStartTime.Text.Trim());


                //if (!string.IsNullOrEmpty(hdnEndTime.Text.Trim()))
                //    OutTime = Convert.ToDateTime(hdnEndTime.Text.Trim(), null);

                //if (!string.IsNullOrEmpty(hdnDate.Text.Trim()))
                //    Date = DateTime.Parse(hdnDate.Text.Trim());

                AttendanceMapping map = AttendanceManager.GetAttendanceMappingOfEmployee(int.Parse(hdnEmployeeId.Text.Trim()));

                if (map.DeviceId == null)
                {
                    NewMessage.ShowNormalMessage("Device id is not mapped for this employee.Please map employee with device id before proceed.");
                    return;
                }

                AttendanceCheckInCheckOut chkIn = new AttendanceCheckInCheckOut();
                chkIn.ChkInOutID = Guid.NewGuid();
                chkIn.DeviceID = map.DeviceId;
                chkIn.AuthenticationType = (int)TimeAttendanceAuthenticationType.Training;
                chkIn.InOutMode = (int)ChkINOUTEnum.ChkIn;
                chkIn.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                chkIn.ModifiedOn = DateTime.Now;
                TimeSpan ManualVisibleTime = new TimeSpan(0, InTime.Value.Hour, InTime.Value.Minute, InTime.Value.Second, InTime.Value.Millisecond);
                chkIn.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(ManualVisibleTime);
                chkIn.DateTime = new DateTime(Date.Value.Date.Year, Date.Value.Date.Month, Date.Value.Date.Day, InTime.Value.Hour, InTime.Value.Minute, InTime.Value.Second);
                chkIn.Date = chkIn.DateTime.Date;
                chkIn.ModificationRemarks = "TRA";
                chkInOutList.Add(chkIn);

                AttendanceCheckInCheckOut chkOut = new AttendanceCheckInCheckOut();
                chkOut.ChkInOutID = Guid.NewGuid();
                chkOut.DeviceID = map.DeviceId;
                chkOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.Training;
                chkOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
                chkOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                chkOut.ModifiedOn = DateTime.Now;
                TimeSpan ManualVisibleTime1 = new TimeSpan(0, OutTime.Value.Hour, OutTime.Value.Minute, OutTime.Value.Second, OutTime.Value.Millisecond);
                chkOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(ManualVisibleTime1);
                chkOut.DateTime = new DateTime(Date.Value.Date.Year, Date.Value.Date.Month, Date.Value.Date.Day, OutTime.Value.Hour, OutTime.Value.Minute, OutTime.Value.Second);
                chkOut.Date = chkIn.DateTime.Date;
                chkOut.ModificationRemarks = "TRA";
                chkInOutList.Add(chkOut);
                Status myStatus = TrainingManager.SaveTrainingAttendanceByLine(chkInOutList);
                if (myStatus.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Attendance Saved Successfully");
                    storeEmpAttendance.Reload();
                }
            }
        }

        Column GetExtGridPanelColumn(string indexId, string headerName, string holidayAbbr)
        {
            Column column = new Column();
            column.DataIndex = indexId;
            column.DataIndex = indexId;
            column.Text = headerName;
            column.Align = Alignment.Right;
            column.ToolTip = headerName;
            column.MenuDisabled = true;
            column.Sortable = false;
            column.Width = new Unit(100);
            column.Align = Alignment.Center;

            TimeField tf = new TimeField();
            TimeSpan tsStart = TimeSpan.FromHours(8);
            TimeSpan tsEnd = TimeSpan.FromHours(18);
            tf.MinTime = tsStart;
            tf.MaxTime = tsEnd;
            tf.Increment = 1;

            if (holidayAbbr == "")
            {
                column.Renderer.Handler = "return Ext.util.Format.date(value, 'g:i a')";

                column.Editor.Add(tf);
            }

            return column;
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            string gridJSON = e.ExtraParams["gridItems"];
            List<GetTrainingEmpAttendnaceResult> list = JSON.Deserialize<List<GetTrainingEmpAttendnaceResult>>(gridJSON);
            List<AttendanceCheckInCheckOut> chkInOutList = new List<AttendanceCheckInCheckOut>();
            foreach (GetTrainingEmpAttendnaceResult _item in list)
            {
                AttendanceMapping map = AttendanceManager.GetAttendanceMappingOfEmployee(_item.EmployeeId);

                if (map.DeviceId == null)
                {
                    NewMessage.ShowNormalMessage("Device id is not mapped for  employee :" + _item.Name + " .Please map employee with device id before proceed.");
                    return;
                }
                
                if(_item.DeviceIn!=null || _item.DeviceOut!=null)
                    continue;
                AttendanceCheckInCheckOut chkIn = new AttendanceCheckInCheckOut();
                chkIn.ChkInOutID = Guid.NewGuid();
                chkIn.DeviceID = map.DeviceId;
                chkIn.AuthenticationType = (int)TimeAttendanceAuthenticationType.Training;
                chkIn.InOutMode = (int)ChkINOUTEnum.ChkIn;
                chkIn.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                chkIn.ModifiedOn = DateTime.Now;
                TimeSpan ManualVisibleTime = new TimeSpan(0, _item.InTimeDT.Value.Hour, _item.InTimeDT.Value.Minute, _item.InTimeDT.Value.Second, _item.InTimeDT.Value.Millisecond);
                chkIn.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(ManualVisibleTime);
                chkIn.DateTime = new DateTime(_item.DateEng.Value.Date.Year, _item.DateEng.Value.Date.Month, _item.DateEng.Value.Date.Day, _item.InTimeDT.Value.Hour, _item.InTimeDT.Value.Minute, _item.InTimeDT.Value.Second);
                chkIn.Date = chkIn.DateTime.Date;
                chkIn.ModificationRemarks = "TRA";
                chkInOutList.Add(chkIn);

                AttendanceCheckInCheckOut chkOut = new AttendanceCheckInCheckOut();
                chkOut.ChkInOutID = Guid.NewGuid();
                chkOut.DeviceID = map.DeviceId;
                chkOut.AuthenticationType = (int)TimeAttendanceAuthenticationType.Training;
                chkOut.InOutMode = (int)ChkINOUTEnum.ChkIn;
                chkOut.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                chkOut.ModifiedOn = DateTime.Now;
                TimeSpan ManualVisibleTime1 = new TimeSpan(0, _item.OutTimeDT.Value.Hour, _item.OutTimeDT.Value.Minute, _item.OutTimeDT.Value.Second, _item.OutTimeDT.Value.Millisecond);
                chkOut.ManualVisibleTime = AttendanceManager.CalculateManualVisibleTime(ManualVisibleTime1);
                chkOut.DateTime = new DateTime(_item.DateEng.Value.Date.Year, _item.DateEng.Value.Date.Month, _item.DateEng.Value.Date.Day, _item.OutTimeDT.Value.Hour, _item.OutTimeDT.Value.Minute, _item.OutTimeDT.Value.Second);
                chkOut.Date = chkIn.DateTime.Date;
                chkOut.ModificationRemarks = "TRA";
                chkInOutList.Add(chkOut);
               
            }

            if (chkInOutList.Any())
            {
                Status myStatus = TrainingManager.SaveTrainingAttendanceByLine(chkInOutList);
                if (myStatus.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Attendance Saved Successfully");
                    storeEmpAttendance.Reload();
                }
            }
        }

    

        protected void btnSave_Click1(object sender, DirectEventArgs e)
        {
            string gridJSON = e.ExtraParams["gridItems"];

            int totalDays = int.Parse(hdnTotalDays.Text.Trim());
            int trainingId = int.Parse(hdnTrainingId.Text.Trim());

            Training objTraining = TrainingManager.GetTrainingById(trainingId);

            JArray jsonArray = Ext.Net.JSON.Deserialize(gridJSON) as JArray;

            if (jsonArray.Count == 0)
                return;

            List<TimeRequestLine> listTR = new List<TimeRequestLine>();
            List<TimeRequestLine> listResult = new List<TimeRequestLine>();


            for (int i = 0; i < jsonArray.Count; i++)
            {
                JToken row = jsonArray[i];

                if (i % 2 == 0)
                    listTR = new List<TimeRequestLine>();


                int employeeId = int.Parse(row["EmployeeId"].Value<string>());

               

                for (int j = 0; j < totalDays; j++)
                {
                    string key = string.Format("Day_{0}", j);

                    if (row[key] != null)
                    {

                        TimeRequestLine obj = new TimeRequestLine();

                        obj.EmpId = employeeId;
                        string inOutTime = row[key].Value<string>();


                        if (inOutTime != "WH")
                        {
                            string[] splInOutTime = inOutTime.Split(' ');

                            if (splInOutTime.ToList().Count > 3)
                            {
                                obj.InTime = TimeSpan.Parse(splInOutTime[4].ToString());
                            }
                            else
                            {
                                obj.InTime = TimeSpan.Parse(splInOutTime[1].ToString());
                                if (inOutTime.ToLower().Contains("pm"))
                                {
                                    TimeSpan addHour = TimeSpan.FromHours(12);
                                    obj.InTime = obj.InTime.Value.Add(addHour);
                                }
                            }

                            obj.DayName = "";

                        }
                        else
                        {
                            obj.EmpId = employeeId;
                            obj.DayName = "WH";
                        }

                        if (i % 2 != 0)
                        {

                            TimeRequestLine oldTr = listTR[j];
                            obj.OutTime = obj.InTime;
                            obj.InTime = oldTr.InTime;
                            obj.DateEng = objTraining.FromDate.Value.AddDays(j);
                            listTR.Remove(oldTr);
                            listTR.Insert(j, obj);

                        }
                        else
                        {
                            listTR.Add(obj);
                        }


                    }

                }


                if (i % 2 == 1)
                    listResult.AddRange(listTR);
            }


            Status status = TrainingManager.SaveTrainingAttendance(listResult);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Training Attendance saved successfully.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

        }








    }
}