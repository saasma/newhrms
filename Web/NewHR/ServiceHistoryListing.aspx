﻿<%@ Page Title="Service History Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ServiceHistoryListing.aspx.cs" Inherits="Web.Appraisal.ServiceHistoryListing" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

      

             
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        #content
        {
            margin: inherit;
        }
    </style>
    <script type="text/javascript">


        //        var Hidden_PageSize = null;
        //        var Hidden_EmployeeId = null;
        //        var PagingToolbar_AdjustmentList = null;
        //        var Hidden_CurrentPage = null;



         var prepareToolbar = function (grid, toolbar, rowIndex, record) {

//            if(record.data.Status =='1' || record.data.Status =='2' || record.data.Status =='3')
//            {
//            }
//            else{
//                toolbar.hide();
//            }

        
        };

         var CommandHandler1 = function(command, record){
          }
        //to track if dash board mode or customer details listing


        Ext.onReady(function () {



        });

        function renderView(e1,e2,record)
        {
            return '<a href="EmployeeAppraisalHRPage.aspx?efid='+ record.data.AppraisalEmployeeFormID + '&fid='+ record.data.AppraisalFormRef_ID +'">View</a>';
        }


        function searchList() {

            <%=hiddenFrom.ClientID %>.setValue( <%=calFromDate.ClientID %>.getValue() );
            <%=hiddenTo.ClientID %>.setValue( <%=calToDate.ClientID %>.getValue() );

            <%=PagingToolbar1.ClientID %>.doRefresh();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Service History
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="hiddenValueRow" />
        <ext:Hidden ID="Hidden_EmployeeId" Text="-1" runat="server">
        </ext:Hidden>
        <ext:Hidden ID="Hidden_CurrentPage" runat="server" Text="1">
        </ext:Hidden>
        <ext:Hidden ID="hiddenFrom" runat="server" Text="">
        </ext:Hidden>
        <ext:Hidden ID="hiddenTo" runat="server" Text="">
        </ext:Hidden>
        <ext:Hidden ID="Hidden_searchText" runat="server">
        </ext:Hidden>
        <ext:Hidden ID="Hidden_PageSize" runat="server" Text="20">
        </ext:Hidden>
        <div class="separator bottom">
        </div>
        <ext:Store ID="StorePaging" runat="server">
            <Model>
                <ext:Model ID="StorePagingModel" runat="server" IDProperty="Value">
                    <Fields>
                        <ext:ModelField Name="Text" Type="Int">
                        </ext:ModelField>
                        <ext:ModelField Name="Value" Type="Int">
                        </ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <div>
                <table class="fieldTable firsttdskip">
                    <tr>
                        <td>
                        <ext:ComboBox LabelAlign="Top" ID="cmbEventFilter" runat="server" ValueField="EventID" DisplayField="Name"
                                        Width="150"  LabelSeparator="" FieldLabel="Event Type" ForceSelection="true"
                                        QueryMode="Local">
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model7" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="EventID" Type="String" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                        </Listeners>
                                    </ext:ComboBox>
                        </td>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70"
                                LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <pr:CalendarExtControl Width="180px" FieldLabel="From Date" ID="calFromDate" runat="server"
                                LabelAlign="Top" LabelSeparator="" />
                        </td>
                        <td>
                            <pr:CalendarExtControl Width="180px" FieldLabel="To Date" ID="calToDate" runat="server"
                                LabelAlign="Top" LabelSeparator="" />
                        </td>
                        <td style="padding-top: 20px">
                            <ext:Button runat="server" Width="100" ID="btnLoad" Cls="btn btn-primary" Text="<i></i>Load"
                                runat="server">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
                <table style="margin-left: 0px;">
                    <%--<tr>
                    <td>
                        <div class="buttonBlock">
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAddLevel" Cls="btnFlat"
                                BaseCls="btnFlat" Text="<i></i>Add New Question" >
                                <DirectEvents>
                                    <Click OnEvent="btnAddLevel_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:LinkButton>
                        </div>
                    </td>
                   

                </tr>--%>
                    <tr>
                        <td colspan="4">
                            <div style="width: 100%;">
                                <ext:GridPanel StyleSpec="margin-top:15px;" Width="1200" ID="GridLevels" runat="server"
                                    Cls="itemgrid">
                                    <Store>
                                        <ext:Store ID="Store3" OnReadData="Store_ReadDataDynamic" runat="server" AutoLoad="true"
                                            PageSize="100">
                                            <Proxy>
                                                <ext:PageProxy>
                                                </ext:PageProxy>
                                            </Proxy>
                                            <AutoLoadParams>
                                                <ext:Parameter Name="start" Value="0" Mode="Raw" />
                                            </AutoLoadParams>
                                            <Parameters>
                                                <ext:StoreParameter Name="EmployeeId" Value="#{cmbSearch}.getValue()" Mode="Raw"
                                                    ApplyMode="Always" />
                                            </Parameters>
                                            <Model>
                                                <ext:Model ID="Model4" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="EIN" Type="String" />
                                                        <ext:ModelField Name="Name" Type="String" />
                                                        <ext:ModelField Name="EVENT" Type="String" />
                                                        <ext:ModelField Name="FromDate" Type="String" />
                                                        <ext:ModelField Name="FromDateEng" Type="Date" />
                                                        <ext:ModelField Name="Branch" Type="String" />
                                                        <ext:ModelField Name="Department" Type="String" />
                                                        <ext:ModelField Name="STATUS" Type="String" />
                                                        <ext:ModelField Name="Level" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                                                Width="60" Align="Right" DataIndex="EIN" />
                                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                                Width="180" Align="Left" DataIndex="Name" />
                                            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Event"
                                                Width="110" Align="Left" DataIndex="EVENT" />
                                            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Nepali Date"
                                                Width="110" Align="Left" DataIndex="FromDate" />
                                            <ext:DateColumn ID="colRolloutDate" runat="server" Align="Left" Text="English Date"
                                                MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="FromDateEng"
                                                Resizable="false" Draggable="false" Width="103">
                                            </ext:DateColumn>
                                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                                Width="200" Align="Left" DataIndex="Branch" />
                                            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                                                Width="200" Align="Left" DataIndex="Department" />
                                            <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                                Width="100" Align="Left" DataIndex="STATUS">
                                                <%-- <Renderer Handler='return value + "%"' />--%>
                                            </ext:Column>
                                            <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Position"
                                                Width="200" Align="Left" DataIndex="Level">
                                                <%--  <Renderer Handler='return value + "%"' />--%>
                                            </ext:Column>
                                        </Columns>
                                    </ColumnModel>
                                    <View>
                                        <ext:GridView EnableTextSelection="true" />
                                    </View>
                                    <SelectionModel>
                                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                    </SelectionModel>
                                    <BottomBar>
                                        <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="100" DisplayInfo="true"
                                            DisplayMsg="Displaying employees {0} - {1} of {2}" EmptyMsg="No Records to display">
                                        </ext:PagingToolbar>
                                    </BottomBar>
                                </ext:GridPanel>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
        <br />
    </div>
</asp:Content>
