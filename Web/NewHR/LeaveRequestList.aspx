﻿<%@ Page Title="Leave Requests" EnableEventValidation="false" ValidateRequest="false"
    Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true"
    CodeBehind="LeaveRequestList.aspx.cs" Inherits="Web.NewHR.LeaveRequestList" %>

<%@ Register Src="~/Employee/UserControls/AssignLeave.ascx" TagName="AssingLeave"
    TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        
        .RequestRow td
        {
            background-color: #FFF3C3 !important;
        }
        .RecommendedRow td
        {
            background-color: #B5D2FF !important;
        }
        .ApprovedRow td
        {
            background-color: #C6E0B4 !important;
        }
        .innerLR
        {
            padding-top: -20px !important;
        }
        .x-grid-cell-inner
        {
            padding: 5px 5px 5px 5px;
        }
        .x-column-header-inner
        {
            padding: 7px 5px 7px 5px;
        }
    </style>
    <script type="text/javascript">


        function searchList() {

           <%=hdnFromDate.ClientID %>.setValue(<%=txtFromDate.ClientID %>.rawValue);
            <%=hdnToDate.ClientID %>.setValue(<%=txtToDate.ClientID %>.rawValue);
            
           <%=PagingToolbar2.ClientID %>.doRefresh();
        }

        Ext.onReady(
            function(){
                //searchList();
            }
        );

          var getRowClass = function (record) {
            if (record.data.Status == 'Request' || record.data.Status == 'Request - Cancellation') {
                return "RequestRow";
            }
            else if (record.data.Status == 'Approved' || record.data.Status == 'Approved - Cancellation') {
                return "ApprovedRow";
            }
              else if (record.data.Status == 'Recommended') {
                return "RecommendedRow";
            }
            else{
                return "new-row";
            }
        };

        function setSorting()
        {
            var sort = <%=storeRequest.ClientID %>;
            if( sort.sorters.items.length > 0)
            {
                var value = sort.sorters.items[0].property + " " + sort.sorters.items[0].direction;
                if( value != "")
                    <%=Hidden_Sort.ClientID %>.setValue(value);

                    
            }
        }
            

        function MoreHandler(leaveRequestId) {
            

            <%=HiddenLeaveRequestId.ClientID %>.setValue(leaveRequestId);

            //call to clear time as during postback time value giving error
            clearTimeFields();
            //Ext.net.DirectMethods.ClickMore(leaveRequestId);

            <%=btn.ClientID %>.fireEvent('click');
        }

        function reloadGridsAfterApproval()
        {
            searchList();
        }
        function reloadGrids ()
        {
            searchList();
        }

        function fromDateChange()
        {
            var fromDate = <%=txtFromDate.ClientID %>.getRawValue();
            <%= hdnFromDate.ClientID %>.setValue(fromDate);
        }

        function toDateChange()
        {
            var toDate = <%= txtToDate.ClientID %>.getRawValue();
            <%= hdnToDate.ClientID %>.setValue(toDate);
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Leave Requests
                </h4>
            </div>
        </div>
    </div>
    <div class="separator bottom">
        <ext:Hidden ID="hdnFromDate" runat="server" />
        <ext:Hidden ID="hdnToDate" runat="server" />
        <ext:Hidden ID="Hidden_Sort" runat="server" />
        <ext:Hidden ID="HiddenLeaveRequestId" runat="server" Text="" />
        <ext:Button Hidden="true" runat="server" ID="btn" Cls="btn btn-primary">
            <DirectEvents>
                <Click OnEvent="btn_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <ext:Label ID="lblMsg" runat="server" />
            <div class="alert alert-info" style="margin-top: -15px;">
                <table class="fieldTable firsttdskip" style="margin-top: 00px !important;" runat="server"
                    id="tbl">
                    <tr>
                        <td>
                            <%--<ext:Checkbox runat="server" Checked="true" FieldLabel="Leave Date" LabelAlign="Top"
                                ID="chkUseModifiedDate" />--%>
                            <ext:ComboBox ID="cmbUseModifiedDate" runat="server" Width="120" LabelAlign="Top"
                                FieldLabel="Date Filter" QueryMode="Local" ForceSelection="true">
                                <Items>
                                    <ext:ListItem Text="Leave Date" Value="true" />
                                    <ext:ListItem Text="Request Date" Value="false" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Text="Leave Date" Value="true" />
                                </SelectedItems>
                            </ext:ComboBox>
                        </td>
                        <td>
                        </td>
                        <td>
                            <ext:DateField runat="server" Width="100" ID="txtFromDate" LabelAlign="Top" FieldLabel="From">
                                <Listeners>
                                    <Change Fn="fromDateChange" />
                                </Listeners>
                                <Plugins>
                                    <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                        </td>
                        <td>
                            <ext:DateField runat="server" Width="100" ID="txtToDate" LabelAlign="Top" FieldLabel="To">
                                <Listeners>
                                    <Change Fn="toDateChange" />
                                </Listeners>
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                        </td>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                          <ExtraParams>
                                            <ext:Parameter Name="RetiredAlso" Value="true" />
                                        </ExtraParams>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70" LabelAlign="Top"
                                runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                TypeAhead="false" Width="150" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox ID="cmbStatus" FieldLabel="Status" Width="100" LabelAlign="Top" runat="server">
                                <SelectedItems>
                                    <ext:ListItem Index="0" />
                                </SelectedItems>
                                <Items>
                                    <ext:ListItem Text="All" Value="0" />
                                    <ext:ListItem Text="Request" Value="1" />
                                    <ext:ListItem Text="Approved" Value="2" />
                                    <ext:ListItem Text="Denied" Value="3" />
                                    <ext:ListItem Text="ReDraft" Value="4" />
                                    <ext:ListItem Text="Cancelled" Value="5" />
                                    <ext:ListItem Text="Recommended" Value="6" />
                                </Items>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:MultiCombo FieldLabel="Branch" SelectionMode="All" ID="cmbBranch" Width="150px"
                                runat="server" ValueField="BranchId" DisplayField="Name" LabelAlign="Top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" IDProperty="BranchId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="BranchId" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:MultiCombo>
                        </td>
                        <td>
                            <ext:ComboBox FieldLabel="Leave Type" ID="cmbLeaveType" Width="120px" runat="server"
                                ValueField="LeaveTypeId" DisplayField="Title" LabelAlign="Top" LabelSeparator=""
                                ForceSelection="true" QueryMode="Local">
                                <Store>
                                    <ext:Store ID="Store2" runat="server">
                                        <Model>
                                            <ext:Model ID="Model3" IDProperty="LeaveTypeId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LeaveTypeId" />
                                                    <ext:ModelField Name="Title" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="padding-top: 20px">
                            <ext:Button runat="server" ID="btnLoad" Cls="btn btn-default" Text="<i></i>Load">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td style="padding-top: 20px">
                            <ext:Button ID="Button1" runat="server" Cls="btn btn-default" Text="Create Request">
                                <Menu>
                                    <ext:Menu runat="server">
                                        <Items>
                                            <ext:Button ID="btnCreateLeaveRequest" runat="server" Text="Create Request">
                                                <DirectEvents>
                                                    <Click OnEvent="btnCreateLeaveRequest_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                                <Listeners>
                                                    <Click Handler="clearTimeFields();" />
                                                </Listeners>
                                            </ext:Button>
                                            <ext:Button ID="btnAssign" runat="server" Text="Assign Leave">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAssign_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                                <Listeners>
                                                    <Click Handler="clearTimeFields();" />
                                                </Listeners>
                                            </ext:Button>
                                        </Items>
                                    </ext:Menu>
                                </Menu>
                            </ext:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <ext:GridPanel Layout="FitLayout" StyleSpec="margin-top:15px;" AutoScroll="true"
                Border="true" ID="gridRequest" runat="server" Cls="itemgrid">
                <Store>
                    <ext:Store runat="server" RemoteSort="true" PageSize="50" AutoLoad="false" ID="storeRequest">
                        <Listeners>
                            <BeforeLoad Fn="setSorting" />
                        </Listeners>
                        <Proxy>
                            <ext:AjaxProxy Url="~/LeaveRequestService.asmx/GetLeaveRequestListForHR" Json="true">
                                <ActionMethods Read="POST" />
                                <Reader>
                                    <ext:JsonReader Root="d.Data" TotalProperty="d.TotalRecords" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model runat="server" IDProperty="LeaveRequestId">
                                <Fields>
                                    <ext:ModelField Name="CreatedDate" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="Leave" Type="String" />
                                    <ext:ModelField Name="Title" Type="String" />
                                    <ext:ModelField Name="DaysOrHours" Type="String" />
                                    <ext:ModelField Name="FromDate" Type="String" />
                                    <ext:ModelField Name="ToDate" Type="String" />
                                    <ext:ModelField Name="Status" Type="String" />
                                    <ext:ModelField Name="Reason" Type="String" />
                                    <ext:ModelField Name="RecommendedByName" Type="string" />
                                    <ext:ModelField Name="ApprovedByName" Type="string" />
                                    <ext:ModelField Name="EmployeeId" Type="string" />
                                    <ext:ModelField Name="CompensatorIsAddType" Type="string" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <AutoLoadParams>
                            <ext:Parameter Name="start" Value="={0}" />
                            <ext:Parameter Name="limit" Value="={50}" />
                        </AutoLoadParams>
                        <Parameters>
                            <ext:StoreParameter Name="startDate" Value="#{txtFromDate}.getValue()" Mode="Raw" />
                            <ext:StoreParameter Name="endDate" Value="#{txtToDate}.getValue()" Mode="Raw" />
                            <ext:StoreParameter Name="Status" Value="#{cmbStatus}.getValue()" Mode="Raw" />
                            <ext:StoreParameter Name="Branch" Value="#{cmbBranch}.getSelectedValues().toString()"
                                Mode="Raw" />
                            <ext:StoreParameter Name="Employee" Value="#{cmbSearch}.getValue()" Mode="Raw" />
                            <ext:StoreParameter Name="leaveTypeId" Value="#{cmbLeaveType}.getValue()" Mode="Raw" />
                            <ext:StoreParameter Name="useModifiedDate" Value="#{cmbUseModifiedDate}.getValue()"
                                Mode="Raw" />
                            <ext:StoreParameter Name="sort" Value="#{Hidden_Sort}.getValue()" Mode="Raw" />
                        </Parameters>
                    </ext:Store>
                </Store>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                        <GetRowClass Fn="getRowClass" />
                    </ext:GridView>
                </View>
                <Listeners>
                    <SortChange Fn="setSorting" />
                </Listeners>
                <ColumnModel runat="server" ID="columnmodle1">
                    <Columns>
                        <ext:Column ID="DateColumn2" Locked="true"  Sortable="true" Width="100" runat="server" Text="Created Date"
                            DataIndex="CreatedDate">
                        </ext:Column>
                        <ext:Column ID="Column2" Locked="true"  Sortable="true" Width="50" runat="server" Text="EIN" DataIndex="EmployeeId">
                        </ext:Column>
                        <ext:Column Sortable="true" Locked="true"  Align="Left"  Header="Name" Width="130"
                            DataIndex="Name" />
                        <ext:Column Sortable="true" Locked="true"   Header="Branch" Width="100" DataIndex="Branch" />
                        <ext:Column Sortable="true"  Header="Department" Width="90" DataIndex="Department" />
                        <ext:Column Sortable="true"  Header="Leave" Width="110" DataIndex="Leave" />
                        <ext:Column Sortable="true"  ID="ColumnDaysOrHours" Text="Days"
                            Width="60" DataIndex="DaysOrHours" />
                        <ext:Column ID="DateColumn1" Width="85" runat="server" Text="From" DataIndex="FromDate">
                        </ext:Column>
                        <ext:Column ID="DateColumn3" Width="85" runat="server" Text="To" DataIndex="ToDate">
                        </ext:Column>
                        <ext:Column ID="Column1" Width="80" runat="server" Text="Reason" DataIndex="Reason">
                        </ext:Column>
                        <ext:Column Sortable="true"  Header="Status" Width="80" DataIndex="Status" />
                        <ext:Column Sortable="true"  Align="Left" Header="Recommended by"
                            Width="85" DataIndex="RecommendedByName" />
                        <ext:Column Sortable="true"  Header="Approved by" Width="120"
                            DataIndex="ApprovedByName" />
                        <ext:Column Hidden="true" Sortable="true"  Header="Compensatory Add Type" Width="110"
                            DataIndex="CompensatorIsAddType" />
                        <ext:CommandColumn Width="60" ButtonAlign="Center" Hidden="false">
                            <Commands>
                                <ext:GridCommand Icon="ArrowRight" CommandName="More" Text="more  " />
                            </Commands>
                            <Listeners>
                                <Command Handler="MoreHandler(record.data.LeaveRequestId)" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar2" runat="server" PageSize="50" DisplayInfo="true"
                        DisplayMsg="Displaying Leaves {0} - {1} of {2}" EmptyMsg="No record to display">
                    </ext:PagingToolbar>
                </BottomBar>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Width="30" />
                </SelectionModel>
                <%--<loadmask showmask="true" />--%>
            </ext:GridPanel>
            <uc5:AssingLeave IsHRView="true" Id="AssignLeave" runat="server" />
            <br />
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Cls="btn btn-primary" Text="<i></i>Export">
                </ext:Button>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
