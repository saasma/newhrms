﻿<%@ Page Title="Employee Retirement Details" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmpRetiringDetailsList.aspx.cs" Inherits="Web.NewHR.EmpRetiringDetailsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

 <script type="text/javascript">

    function searchList() {
             <%=gridRetDetails.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

</script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">


<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Possible Retirement Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="Name" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr> 
                                <td>
                                    <ext:ComboBox ID="cmbBranch" Width="180" runat="server" ValueField="BranchId" DisplayField="BranchName"
                                            FieldLabel="Branch" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                            QueryMode="Local">
                                            <Store>
                                                <ext:Store ID="Store5" runat="server">
                                                    <Model>
                                                        <ext:Model ID="Model2" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="BranchId" />
                                                                <ext:ModelField Name="BranchName" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                            </Triggers>
                                            <Listeners>
                                                <Select Handler="this.getTrigger(0).show();" />
                                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { 
                                                           this.clearValue(); 
                                                           this.getTrigger(0).hide();
                                                       }" />
                                            </Listeners>
                                        </ext:ComboBox>
                                </td>           
                                <td style='padding-left: 15px;'>
                                    <ext:ComboBox ID="cmbDepartment" Width="180" runat="server" ValueField="DepartmentId"
                                        DisplayField="Name" FieldLabel="Department" LabelAlign="top" LabelSeparator=""
                                        ForceSelection="true" QueryMode="Local">
                                        <Store>
                                            <ext:Store ID="Store6" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model6" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>                  
                                <td style='padding-left: 15px;'>
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="200" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridRetDetails" runat="server"
                Cls="itemgrid" OnReadData="Store_ReadData"  Scroll="Horizontal">
                <Store>
                    <ext:Store ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="Int" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="RetirementBy" Type="String" />
                                    <ext:ModelField Name="RetirementDate" Type="String" />
                                    <ext:ModelField Name="JoinDate" Type="String" />
                                    <ext:ModelField Name="StatusID" Type="String" />
                                    <ext:ModelField Name="StatusName" Type="String" />
                                    <ext:ModelField Name="Level" Type="String" />
                                    <ext:ModelField Name="DOB" Type="String" />
                                    <ext:ModelField Name="RetirementByAge" Type="String" />
                                    <ext:ModelField Name="RetirementByService" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column  ID="colEIN" Locked="true"  runat="server" Text="EIN" DataIndex="EmployeeId"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="60"  />
                        <ext:Column ID="colName"  Locked="true" runat="server" Text="Name" DataIndex="Name" MenuDisabled="true"
                            Sortable="false" Align="Left" Width="200"   />
                        <ext:Column ID="colBranch" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="true"
                            Sortable="false" Align="Left" Width="150" />
                        <ext:Column Locked="true"  ID="colDepartment" runat="server" Text="Department" DataIndex="Department" MenuDisabled="true"
                            Sortable="false" Align="Left" Width="150" />
                        <ext:Column ID="Column1" runat="server" Text="Level" DataIndex="Level"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="120" />
                        <ext:Column ID="colStatusName" runat="server" Text="Status" DataIndex="StatusName"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="120" />

                        <ext:Column ID="Column2" runat="server" Text="DOB" DataIndex="DOB"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                        <ext:Column ID="colJoinDate" runat="server" Text="Start Date/Join Date" DataIndex="JoinDate"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                        
                        <ext:Column ID="Column3" runat="server" Text="Retirement By Age" DataIndex="RetirementByAge"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                        <ext:Column ID="Column4" runat="server" Text="Retirement By Service" DataIndex="RetirementByService"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="100" />

                        <ext:Column ID="colRetirementBy" runat="server" Text="Retirement By" DataIndex="RetirementBy"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="120" />
                        <ext:Column ID="colRetirementDate" runat="server" Text="Retirement Date" DataIndex="RetirementDate"
                            MenuDisabled="true" Sortable="false" Align="Left" Width="120" />
                    </Columns>
                </ColumnModel>
                <%-- <Plugins>
                    <ext:FilterHeader ID="FilterHeader1" runat="server" OnCreateFilterableField="OnCreateFilterableField" />
                </Plugins>--%>
                <BottomBar>
                    <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeEmpList"
                        DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchList()" />
                                    <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="20" Text="20" />
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
