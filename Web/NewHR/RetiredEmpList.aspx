﻿<%@ Page Title="Retired Employee List" EnableViewState="false" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="RetiredEmpList.aspx.cs" Inherits="Web.NewHR.RetiredEmpList" %>

<%@ Register Src="~/newhr/UserControls/EmployeeContractTabStrip.ascx" TagName="EmployeeContractTabStripCtl"
    TagPrefix="ucEmployeeContractTabStrip" %>
<%@ Register Src="~/NewHR/UserControls/ActingListCtl.ascx" TagName="FamilyCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>

    <script type="text/javascript">
    
        var CommandHandler = function(command, record){
            <%= hdnFilterValue.ClientID %>.setValue(record.data.EmployeeId);
            
            
       
                <%= btnReHirePopup.ClientID %>.fireEvent('click');
            

        }

        function searchListEL() {

             <%=gridEmployee.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    </script>
    <style type="text/css">
  .x-grid-row, .x-grid-data-row
  {
      height:25px;
  }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
  
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Retired Employees
                </h4>
            </div>
        </div>
    </div>
    <ext:Button ID="btnReHirePopup" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnReHirePopup_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Window ID="windowPopup" runat="server" Title="Re-Hire Employee" Icon="Application"
        Width="600" Height="500" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:TextField ID="txtEIN" Disabled="true" LabelSeparator="" MinValue="0" runat="server"
                            FieldLabel="EIN" LabelAlign="Top">
                        </ext:TextField>
                    </td>
                    <td>
                        <ext:TextField ID="txtNewINo" LabelSeparator="" MinValue="0" runat="server" FieldLabel="I No"
                            LabelAlign="Top">
                        </ext:TextField>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ext:TextField ID="txtName" Disabled="true" LabelSeparator="" MinValue="0" runat="server"
                            FieldLabel="Name" LabelAlign="Top">
                        </ext:TextField>
                    </td>
                    <td>
                </tr>
                <tr>
                    <td>
                        <ext:TextField ID="txtAppointmentNo" LabelSeparator="" MinValue="0" runat="server"
                            FieldLabel="Appointment Letter No" LabelAlign="Top">
                        </ext:TextField>
                    </td>
                    <td>
                        <pr:CalendarExtControl Width="150px" FieldLabel="Re Hire Date *" ID="calStart" runat="server"
                            LabelAlign="Top" LabelSeparator="" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Re Hire Date is required."
                            ControlToValidate="calStart" ValidationGroup="SaveUpdate11" Display="None" />
                    </td>
                    <td>
                        <pr:CalendarExtControl Width="150px" FieldLabel="Contract End Date" ID="calEnd" runat="server"
                            LabelAlign="Top" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbStatus" Width="150px" runat="server" ValueField="Key" DisplayField="Value"
                            FieldLabel="Job Status *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store8" runat="server">
                                    <Model>
                                        <ext:Model ID="Model8" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Key" Type="String"  />
                                                <ext:ModelField Name="Value"  Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <%--  <DirectEvents>
                                <Select OnEvent="cmbStatus_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>--%>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator19" runat="server"
                            ValidationGroup="SaveUpdate11" ControlToValidate="cmbStatus" ErrorMessage="Please select a Job Status." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbBranch" Width="180px" runat="server" ValueField="BranchId" DisplayField="Name"
                            FieldLabel="Branch *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <DirectEvents>
                                <Select OnEvent="cmbBranch_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                            <Store>
                                <ext:Store ID="Store5" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="BranchId" Type="String"  />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator15" runat="server"
                            ValidationGroup="InsertUpdate" ControlToValidate="cmbBranch" ErrorMessage="Please select a Branch for the employee." />
                    </td>
                    <td style='width: 180px'>
                        <ext:ComboBox ID="cmbDepartment" Width="180px" runat="server" ValueField="DepartmentId"
                            DisplayField="Name" FieldLabel="Department *" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store6" runat="server">
                                    <Model>
                                        <ext:Model ID="Model6" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId"  Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator16" runat="server"
                            ValidationGroup="InsertUpdate" ControlToValidate="cmbDepartment" ErrorMessage="Please select a Department for the employee." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbLevel" Width="180px" runat="server" ValueField="LevelId" DisplayField="GroupLevel"
                            FieldLabel="Level *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="LevelId" Type="String" />
                                                <ext:ModelField Name="GroupLevel" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="cmbLevel_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="valLevel" runat="server" ValidationGroup="InsertUpdate"
                            ControlToValidate="cmbLevel" ErrorMessage="Please select employee Level." />
                    </td>
                    <td runat="server" id="tdStep">
                                            <ext:ComboBox ID="cmbStep" Width="180px" runat="server" DisplayField="Text" ValueField="Value"
                                                FieldLabel="Grade *" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store4" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model10" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="Text" />
                                                                    <ext:ModelField Name="Value" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                           
                                        </td>
                    <td>
                        <ext:ComboBox ID="cmbPosition" Width="180px" runat="server" ValueField="DesignationId"
                            DisplayField="Name" FieldLabel="Designation *" LabelAlign="top" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store41" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DesignationId"  Type="String" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator13" runat="server"
                            ValidationGroup="InsertUpdate" ControlToValidate="cmbPosition" ErrorMessage="Please select employee Designation." />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv">
                            <ext:Button runat="server" ID="btnLevelSaveUpdate" Cls="btn btn-primary" Text="<i></i>ReHire"
                                runat="server">
                                <DirectEvents>
                                    <Click OnEvent="btnLevelSaveUpdate_Click">
                                        <Confirmation ConfirmRequest="true" Message="Are you sure, you want to re-hire the employee?" />
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdate11'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton1" Text="<i></i>Cancel"
                                runat="server">
                                <Listeners>
                                    <Click Handler="#{windowPopup}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
    <ext:Hidden ID="hiddenFrom" runat="server" Text="">
    </ext:Hidden>
    <ext:Hidden ID="hiddenTo" runat="server" Text="">
    </ext:Hidden>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <ext:Hidden ID="hdnFilterValue" runat="server">
        </ext:Hidden>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <table class="fieldTable firsttdskip" runat="server" id="tbl">
                <tr>
                    <td>
                        <pr:CalendarExtControl LabelAlign="Top" FieldLabel="From" Width="150px" ID="txtFromDate"
                            runat="server" LabelSeparator="">
                        </pr:CalendarExtControl>
                    </td>
                    <td>
                        <pr:CalendarExtControl LabelAlign="Top" FieldLabel="To" Width="150px" ID="txtToDate"
                            runat="server" LabelSeparator="" />
                    </td>
                    <td>
                        <ext:ComboBox FieldLabel="Retirement Type" ID="cmbTypes" Width="180px" runat="server"
                            ValueField="EventID" DisplayField="Name" ForceSelection="true" LabelAlign="Top"
                            LabelSeparator="" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" IDProperty="EventID" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="EventID" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                             <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td>
                        <ext:Store runat="server" ID="storeEmpSearch" AutoLoad="false">
                            <Proxy>
                                <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                    <ActionMethods Read="GET" />
                                    <ExtraParams>
                                        <ext:StoreParameter Name="RetiredAlso" Value="true" />
                                    </ExtraParams>
                                    <Reader>
                                        <ext:JsonReader Root="plants" TotalProperty="total" />
                                    </Reader>
                                </ext:AjaxProxy>
                            </Proxy>
                            <Model>
                                <ext:Model ID="Model9" IDProperty="Name" Name="ItemLineModel" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" Type="String" />
                                        <ext:ModelField Name="EmployeeId" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                        <ext:ComboBox LabelSeparator="" ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70"
                            LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeEmpSearch"
                            TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                            TriggerAction="All" ForceSelection="true">
                            <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                <ItemTpl ID="ItemTpl1" runat="server">
                                    <Html>
                                        <tpl>
                                                <div class="search-item">
                                                                <span>{Name}</span>  
                                                 </div>
					                    </tpl>
                                    </Html>
                                </ItemTpl>
                            </ListConfig>
                            <Triggers>
                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                            </Triggers>
                            <Listeners>
                                <Select Handler="this.getTrigger(0).show();" />
                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                <TriggerClick Handler="if (index == 0) { 
                                               this.clearValue(); 
                                               this.getTrigger(0).hide();
                                           }" />
                            </Listeners>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-top: 20px">
                        <div>
                            <ext:Button Cls="btn btn-primary" runat="server" ID="btnLoad" Text="<i></i>Load">
                                <%--<DirectEvents>
                                    <Click OnEvent="btnLastMonth_Change">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>--%>
                                <Listeners>
                                    <Click Fn="searchListEL" />
                                </Listeners>
                            </ext:Button>
                        </div>
                    </td>
                    <td>
                        <ext:Button Cls="btn btn-primary" runat="server" AutoPostBack="true" OnClick="btnExport_Click"
                            ID="btnExport1" Text="<i></i>Export" MarginSpec="15 0 0 0">
                        </ext:Button>
                    </td>
                </tr>
            </table>
            <div>
            </div>
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="right">
            </div>
            <div style="clear: both">
            </div>
            <ext:GridPanel StyleSpec="margin-top:15px;" Border="true" ID="gridEmployee" runat="server"
                OnReadData="Store_ReadData" OverflowY="Hidden" Scroll="Horizontal" Cls="itemgrid">
                <Store>
                    <ext:Store ID="Store3" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model4" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="EmployeeId" Type="string" />
                                    <ext:ModelField Name="AccountNo" Type="string" />
                                    <ext:ModelField Name="Name" Type="string" />
                                    <ext:ModelField Name="Level" Type="string" />
                                    <ext:ModelField Name="Branch" Type="string" />
                                    <ext:ModelField Name="Department" Type="string" />
                                    <ext:ModelField Name="JoinDateEng" Type="string" />
                                    <ext:ModelField Name="JoinDesignation" Type="String" />
                                    <ext:ModelField Name="ConfirmationDateEng" Type="String" />
                                    <ext:ModelField Name="CurrentDesignation" Type="String" />
                                    <ext:ModelField Name="RetirementDateEng" Type="String" />
                                    <ext:ModelField Name="RetirementDate" Type="string" />
                                    <ext:ModelField Name="ServicePeriod" Type="string" />
                                    <ext:ModelField Name="BasicSalary" Type="Float" />
                                    <ext:ModelField Name="Allowance" Type="Float" />
                                    <ext:ModelField Name="IncentiveAllowance" Type="Float" />
                                    <ext:ModelField Name="StopPayDate" Type="String" />
                                    <ext:ModelField Name="RetirementType" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <View>
                    <ext:GridView runat="server" EnableTextSelection="true" />
                </View>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                            Align="Left" Width="50" DataIndex="EmployeeId" Lockable="true" Locked="true"
                            Wrap="true" />
                        <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Account No"
                            Align="Left" Width="100" DataIndex="AccountNo" Lockable="true" Locked="true"
                            Wrap="true" />
                        <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                            Wrap="true" Align="Left" Width="200" DataIndex="Name" Lockable="true" Locked="true" />
                        <ext:Column ID="Column6" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                            Wrap="true" Text="Level" Align="Left" DataIndex="Level">
                        </ext:Column>
                        <ext:Column ID="ColumnBranch" Width="140" Sortable="false" MenuDisabled="true" runat="server"
                            Wrap="false" Text="Branch" Align="Left" DataIndex="Branch">
                        </ext:Column>
                        <ext:Column ID="ColumnDepartment" Width="140" Sortable="false" MenuDisabled="true"
                            Wrap="false" runat="server" Text="Department" Align="Left" DataIndex="Department">
                        </ext:Column>
                        <ext:DateColumn ID="columnJoinDate1" Width="120" Sortable="false" MenuDisabled="true" Format="yyyy-MMM-dd"
                            Wrap="true" runat="server" Text="Joining Date" Align="Left" DataIndex="JoinDateEng">
                        </ext:DateColumn>
                        <ext:Column ID="Column2" Width="140" Sortable="false" MenuDisabled="true" runat="server"
                            Wrap="true" Text="Joining Designation" Align="Left" DataIndex="JoinDesignation">
                        </ext:Column>
                        <ext:DateColumn ID="DateColumn_Confirmation" Width="120" Sortable="false" MenuDisabled="true" Format="yyyy-MMM-dd"
                            Wrap="true" runat="server" Text="Confirmation Date" Align="Left" DataIndex="ConfirmationDateEng">
                        </ext:DateColumn>
                        <ext:Column ID="Column3" Width="150" Sortable="false" MenuDisabled="true" runat="server"
                            Wrap="true" Text="Current Designation" Align="Left" DataIndex="CurrentDesignation">
                        </ext:Column>
                        <ext:DateColumn ID="DateColumn2" Width="140" Sortable="false" MenuDisabled="true" Format="yyyy-MMM-dd"
                            Wrap="true" runat="server" Text="Retirement Date(A.D)" Align="Left" DataIndex="RetirementDateEng">
                        </ext:DateColumn>
                        <ext:Column ID="Column4" Width="140" Sortable="false" MenuDisabled="true" runat="server"
                            Wrap="true" Text="Retirement Date(B.S)" Align="Left" DataIndex="RetirementDate">
                        </ext:Column>
                        <ext:Column ID="Column7" Width="200" Sortable="false" MenuDisabled="true" runat="server"
                            Wrap="true" Text="Service Period" Align="Right" DataIndex="ServicePeriod">
                        </ext:Column>
                        <ext:NumberColumn ID="colBasicSalary" Width="120" Sortable="false" MenuDisabled="true"
                            runat="server" Text="Basic Salary" Wrap="true" Align="Right" DataIndex="BasicSalary" />
                        <ext:NumberColumn ID="colAllowance" Width="120" Sortable="false" MenuDisabled="true"
                            runat="server" Text="Allowance" Wrap="true" Align="Right" DataIndex="Allowance" />
                        <ext:NumberColumn ID="colInctAllowance" Width="120" Sortable="false" MenuDisabled="true"
                            runat="server" Text="Inct. Allowance" Wrap="true" Align="Right" DataIndex="IncentiveAllowance" />
                        <ext:DateColumn ID="DateColumn3" Width="140" Sortable="false" MenuDisabled="true" Format="yyyy-MMM-dd"
                            Wrap="true" runat="server" Text="Stop Pay Date" Align="Left" DataIndex="StopPayDate">
                        </ext:DateColumn>
                        <ext:Column ID="Column1" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                            Wrap="true" Text="Retirement Type" Align="Left" DataIndex="RetirementType">
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Width="80" Text="" Align="Center"
                            MenuDisabled="true" Sortable="false">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand CtCls="editGridButton" Text="Re Hire"  ToolTip-Text="Re Hire" CommandName="ReHire" />
                            </Commands>
                            <Listeners>
                                <Command Handler="CommandHandler(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
                </SelectionModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                        <Items>
                            <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                            <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                            <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                                ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                                <Listeners>
                                    <Select Handler="searchListEL()" />
                                    <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                                </Listeners>
                                <Items>
                                    <ext:ListItem Value="50" Text="50" />
                                    <ext:ListItem Value="100" Text="100" />
                                    <ext:ListItem Value="200" Text="200" />
                                </Items>
                                <SelectedItems>
                                    <ext:ListItem Index="0">
                                    </ext:ListItem>
                                </SelectedItems>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
            <div style="clear: both">
            </div>
        </div>
    </div>
    <br />
</asp:Content>
