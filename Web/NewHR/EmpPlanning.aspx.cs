﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Base;
using BLL;

namespace Web.NewHR
{
    public partial class EmpPlanning : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            BindBranchCombo();
            BindDepartmentCombo();
            BindLevelGroupCombo();

            LoadEmployeeHierarchy();
        }

        private void ClearValues()
        {
            cmbGroup.ClearValue();
            cmbLevel.ClearValue();
            cmbDesignation.ClearValue();
            cmbBranchAdd.ClearValue();
            cmbDepartmentAdd.ClearValue();
        }

        private void BindBranchCombo()
        {
            List<Branch> listBranch = ListManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            cmbBranchAdd.Store[0].DataSource = listBranch;
            cmbBranchAdd.Store[0].DataBind();

            Branch obj = new Branch() { BranchId = -1, Name = "All" };
            listBranch.Insert(0, obj);
            cmbBranch.Store[0].DataSource = listBranch;
            cmbBranch.Store[0].DataBind();
        }

        private void BindDepartmentCombo()
        {
            List<Department> list = new DepartmentManager().GetDepartmentsByCompany(SessionManager.CurrentCompanyId);

            cmbDepartmentAdd.Store[0].DataSource = list;
            cmbDepartmentAdd.Store[0].DataBind();

            Department obj = new Department() { DepartmentId = -1, Name = "All" };
            list.Insert(0, obj);
            cmbDepartment.Store[0].DataSource = list;
            cmbDepartment.Store[0].DataBind();
        }

        private void BindLevelGroupCombo()
        {
            cmbGroup.Store[0].DataSource = NewPayrollManager.GetLevelGroup();
            cmbGroup.Store[0].DataBind();
        }               

        private NodeCollection LoadEmployeeHierarchy()
        {

            Ext.Net.NodeCollection mainTreeNode = treeEmployeeHierarchy.Root;

            Ext.Net.Node rootTreeNode = new Ext.Net.Node();

            rootTreeNode.Text = "Root";
            rootTreeNode.Expanded = true;
            rootTreeNode.Leaf = false;
            rootTreeNode.NodeID = Guid.Empty.ToString();

            int branchId = -1, departmentId = -1;

            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Text))
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Text))
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            List<GetEmployeePlanResult> list = NewHRManager.GetEmployeePlanList(branchId, departmentId, cmbSortBy.Text.Trim());


            if (list.Count > 0)
            {
                List<BLevelGroup> listBLevelGroup = NewPayrollManager.GetLevelGroup();

                //List<int> groupIdList = (from groupLevel in list
                //                         select groupLevel.LevelGroupId.Value).Distinct().ToList();


                foreach (var itemGroup in listBLevelGroup)
                {
                    Node groupNode = new Node();
                    groupNode.NodeID = "G" + itemGroup.LevelGroupId.ToString();
                    groupNode.Text = itemGroup.Name;
                    groupNode.Expanded = false;


                    List<GetEmployeePlanResult> listEmployeeCountInGroup = list.Where(x => x.LevelGroupId == itemGroup.LevelGroupId).ToList();
                    groupNode.CustomAttributes.Add(new ConfigItem { Name = "ExistingEmployees", Value = listEmployeeCountInGroup.Count.ToString() });
                    
                    groupNode.CustomAttributes.Add(new ConfigItem { Name = "IsPage", Value = "No1" });

                    groupNode.CustomAttributes.Add(new ConfigItem { Name = "CssClass", Value = "BlueCls" });

                    //List<int> listBLevelId = (from blevel in list
                    //                          where blevel.LevelGroupId == itemGroup.LevelGroupId
                    //                          select blevel.LevelId.Value).Distinct().ToList();

                   
                    List<BLevel> listBLevel = NewPayrollManager.GetParentLevels(itemGroup.LevelGroupId);


                    int plannedEmployeesForGroup = 0;

                    foreach (var itemBLevel in listBLevel)
                    {
                        Node blevelNode = new Node();
                        blevelNode.NodeID = "BL" + itemBLevel.LevelId.ToString();
                        blevelNode.Text = itemBLevel.Name;
                        blevelNode.Expanded = false;
                        blevelNode.Leaf = false;

                        List<GetEmployeePlanResult> listEmployeeCountInBlevel = list.Where(x => x.LevelGroupId == itemGroup.LevelGroupId && x.LevelId == itemBLevel.LevelId).ToList();
                        blevelNode.CustomAttributes.Add(new ConfigItem { Name = "ExistingEmployees", Value = listEmployeeCountInBlevel.Count.ToString() });
                        
                        blevelNode.CustomAttributes.Add(new ConfigItem { Name = "IsPage", Value = "No2" });

                        blevelNode.CustomAttributes.Add(new ConfigItem { Name = "CssClass", Value = "LightBlue" });

                        //List<int> listEDesignationId = (from edesignation in list
                        //                                where edesignation.LevelGroupId == itemGroup.LevelGroupId && edesignation.LevelId == itemBLevel.LevelId
                        //                                select edesignation.DesignationId.Value).Distinct().ToList();

                        // NewPayrollManager.GetDesignationByLevel(int.Parse(cmbLevel.SelectedItem.Value))
                        List<EDesignation> listEDesignation = NewPayrollManager.GetDesignationByLevel(itemBLevel.LevelId);

                        int plannedEmployeesForLevel = 0;
                        int availableVacanciesForLevel = 0;

                        foreach (var itemEDesignation in listEDesignation)
                        {
                            List<GetEmployeePlanResult> listEmployeeHierarchy = (from employeelist in list
                                                                                 where employeelist.LevelGroupId == itemGroup.LevelGroupId && employeelist.LevelId == itemBLevel.LevelId && employeelist.DesignationId == itemEDesignation.DesignationId
                                                                                 select employeelist).ToList();
                            //For branch && department grouping
                            if (listEmployeeHierarchy.Count > 0)
                            {
                                List<int> branchIdList = (from empBranch in listEmployeeHierarchy
                                                          where empBranch.LevelGroupId == itemGroup.LevelGroupId && empBranch.LevelId == itemBLevel.LevelId && empBranch.DesignationId == itemEDesignation.DesignationId
                                                          select empBranch.BranchId.Value).Distinct().ToList();

                                foreach (var branchItem in branchIdList)
                                {
                                    List<int> departmentIdList = (from empDepartment in listEmployeeHierarchy
                                                                  where empDepartment.LevelGroupId == itemGroup.LevelGroupId && empDepartment.LevelId == itemBLevel.LevelId && empDepartment.DesignationId == itemEDesignation.DesignationId
                                                                  && empDepartment.BranchId == branchItem
                                                                  select empDepartment.DepartmentId.Value).Distinct().ToList();

                                    foreach (var departmentItem in departmentIdList)
                                    {
                                        List<GetEmployeePlanResult> listEmployeeInDepartment = listEmployeeHierarchy.Where(x => x.BranchId == branchItem && x.DepartmentId == departmentItem).ToList();

                                        Node eDesignationNode = new Node();
                                        eDesignationNode.NodeID = "depNode" + itemEDesignation.DesignationId.ToString() + branchItem.ToString() + departmentItem.ToString(); //"ED" + itemEDesignation.DesignationId.ToString();
                                        eDesignationNode.Text = itemEDesignation.Name;
                                        eDesignationNode.Expanded = false;
                                        eDesignationNode.Leaf = false;


                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "LevelGroupId", Value = itemGroup.LevelGroupId.ToString() });
                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "LevelId", Value = itemBLevel.LevelId.ToString() });
                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "DesignationId", Value = itemEDesignation.DesignationId.ToString() });

                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "BranchId", Value = branchItem.ToString() });
                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "Branch", Value = listEmployeeInDepartment[0].Branch });
                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "DepartmentId", Value = departmentItem.ToString() });
                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "Department", Value = listEmployeeInDepartment[0].Department });

                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "ExistingEmployees", Value = listEmployeeInDepartment.Count.ToString() });


                                        int plannedEmployees = NewHRManager.GetNoOfPlannedEmployeesForDesignationInBrDept(itemGroup.LevelGroupId, itemBLevel.LevelId, itemEDesignation.DesignationId, branchItem, departmentItem);
                                        int availabelVacancies = (plannedEmployees - listEmployeeInDepartment.Count);

                                        plannedEmployeesForLevel = plannedEmployeesForLevel + plannedEmployees;

                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "PlannedEmployees", Value = plannedEmployees.ToString() });

                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "AvailableVacancies", Value = availabelVacancies.ToString() });

                                        eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "IsPage", Value = "Yes" });

                                        eDesignationNode.Leaf = true;

                                        blevelNode.Children.Add(eDesignationNode);

                                    }
                                }

                            }
                            else
                            {
                                Node eDesignationNode = new Node();
                                eDesignationNode.NodeID = "depNode" + itemEDesignation.DesignationId.ToString();
                                eDesignationNode.Text = itemEDesignation.Name;
                                eDesignationNode.Expanded = false;
                                eDesignationNode.Leaf = true;

                                int plannedEmployeesForDesignation = 0;// NewHRManager.GetPlannedEmployeesNoForDesignationByIds(itemGroup.LevelGroupId, itemBLevel.LevelId, itemEDesignation.DesignationId);

                                eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "ExistingEmployees", Value = "0" });
                                eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "PlannedEmployees", Value = plannedEmployeesForDesignation.ToString() });
                                eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "AvailableVacancies", Value = plannedEmployeesForDesignation.ToString() });

                                blevelNode.Children.Add(eDesignationNode);
                            }

                            List<EmployeePlanning> newEmployeePlanningList = NewHRManager.GetEmployeePlanningListByIds(itemGroup.LevelGroupId, itemBLevel.LevelId, itemEDesignation.DesignationId);

                            int branchIdNew = 0, departmentIdNew = 0;

                            if (!string.IsNullOrEmpty(cmbBranch.Text))
                                branchIdNew = int.Parse(cmbBranch.SelectedItem.Value);

                            if(branchIdNew != 0 && branchIdNew != -1)
                                newEmployeePlanningList = newEmployeePlanningList.Where(x => x.BranchId == branchIdNew).ToList();



                            if (!string.IsNullOrEmpty(cmbDepartment.Text))
                                departmentIdNew = int.Parse(cmbDepartment.SelectedItem.Value);

                            if(departmentIdNew != 0 && departmentIdNew != -1)
                                newEmployeePlanningList = newEmployeePlanningList.Where(x => x.DepartmentId == departmentIdNew).ToList();

                            if (cmbSortBy.Text.Trim() == "Branch")
                                newEmployeePlanningList = newEmployeePlanningList.OrderBy(x => x.br).ToList();
                            else
                                newEmployeePlanningList = newEmployeePlanningList.OrderBy(x => x.dep).ToList();


                            foreach (var item in newEmployeePlanningList)
                            {
                                if (!listEmployeeHierarchy.Any(x => x.LevelGroupId == item.LevelGroupId.Value && x.LevelId == item.LevelId.Value && x.DesignationId == item.DesignationId.Value && x.BranchId == item.BranchId.Value && x.DepartmentId == item.DepartmentId.Value))
                                {
                                    Node eDesignationNode = new Node();
                                    eDesignationNode.NodeID = "depNode" + itemEDesignation.DesignationId.ToString() + item.BranchId.Value.ToString() + item.DepartmentId.Value.ToString(); //"ED" + itemEDesignation.DesignationId.ToString();
                                    eDesignationNode.Text = itemEDesignation.Name;
                                    eDesignationNode.Expanded = false;
                                    eDesignationNode.Leaf = false;


                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "LevelGroupId", Value = item.LevelGroupId.Value.ToString() });
                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "LevelId", Value = item.LevelId.Value.ToString() });
                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "DesignationId", Value = item.DesignationId.Value.ToString() });

                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "BranchId", Value = item.BranchId.Value.ToString() });
                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "Branch", Value = BranchManager.GetBranchById(item.BranchId.Value).Name });
                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "DepartmentId", Value = item.DepartmentId.Value.ToString() });
                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "Department", Value = DepartmentManager.GetDepartmentById(item.DepartmentId.Value).Name });

                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "ExistingEmployees", Value = "0" });


                                    int plannedEmployees = NewHRManager.GetNoOfPlannedEmployeesForDesignationInBrDept(item.LevelGroupId.Value, item.LevelId.Value, item.DesignationId.Value, item.BranchId.Value, item.DepartmentId.Value);
                                    int availabelVacancies = plannedEmployees;

                                    plannedEmployeesForLevel = plannedEmployeesForLevel + plannedEmployees;

                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "PlannedEmployees", Value = plannedEmployees.ToString() });

                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "AvailableVacancies", Value = availabelVacancies.ToString() });

                                    eDesignationNode.CustomAttributes.Add(new ConfigItem { Name = "IsPage", Value = "Yes" });

                                    eDesignationNode.Leaf = true;

                                    blevelNode.Children.Add(eDesignationNode);
                                }
                            }
                        }

                        blevelNode.CustomAttributes.Add(new ConfigItem { Name = "PlannedEmployees", Value = plannedEmployeesForLevel.ToString() });
                                                
                        availableVacanciesForLevel = plannedEmployeesForLevel - listEmployeeCountInBlevel.Count;
                        plannedEmployeesForGroup = plannedEmployeesForGroup + plannedEmployeesForLevel;

                        blevelNode.CustomAttributes.Add(new ConfigItem { Name = "AvailableVacancies", Value = availableVacanciesForLevel.ToString() });


                        groupNode.Children.Add(blevelNode);
                    }

                    groupNode.CustomAttributes.Add(new ConfigItem { Name = "PlannedEmployees", Value = plannedEmployeesForGroup.ToString() });



                    groupNode.CustomAttributes.Add(new ConfigItem { Name = "AvailableVacancies", Value = (plannedEmployeesForGroup - listEmployeeCountInGroup.Count).ToString() });

                    rootTreeNode.Children.Add(groupNode);
                }
                mainTreeNode.Add(rootTreeNode);


            }
            return mainTreeNode;
        }

        protected void cmbBranch_Change(object sender, DirectEventArgs e)
        {
            LoadEmployeeHierarchy();
            treeEmployeeHierarchy.Render();
            treeEmployeeHierarchy.AddCls("treepanelCls");

        }

        protected void cmbDepartment_Change(object sender, DirectEventArgs e)
        {
            LoadEmployeeHierarchy();
            treeEmployeeHierarchy.Render();
            treeEmployeeHierarchy.AddCls("treepanelCls");
        }

        protected void cmbSortBy_Change(object sender, DirectEventArgs e)
        {
            LoadEmployeeHierarchy();
            treeEmployeeHierarchy.Render();
            treeEmployeeHierarchy.AddCls("treepanelCls");
        }

        protected void SubmitNodes(object sender, SubmitEventArgs e)
        {
            var rootNode = e.RootNode;

            List<EmployeePlanning> list = new List<EmployeePlanning>();

            foreach (var groupNode in rootNode.Children)
            {
                foreach (var levelNode in groupNode.Children)
                {
                    foreach (var designationNode in levelNode.Children)
                    {
                        if (!string.IsNullOrEmpty(designationNode.Attributes["LevelGroupId"].ToString()) && !string.IsNullOrEmpty(designationNode.Attributes["LevelId"].ToString())
                            && !string.IsNullOrEmpty(designationNode.Attributes["DesignationId"].ToString()) && !string.IsNullOrEmpty(designationNode.Attributes["BranchId"].ToString()))
                        {
                            EmployeePlanning obj = new EmployeePlanning()
                            {
                                LevelGroupId = int.Parse(designationNode.Attributes["LevelGroupId"].ToString()),
                                LevelId = int.Parse(designationNode.Attributes["LevelId"].ToString()),
                                DesignationId = int.Parse(designationNode.Attributes["DesignationId"].ToString()),
                                BranchId = int.Parse(designationNode.Attributes["BranchId"].ToString()),
                                DepartmentId = int.Parse(designationNode.Attributes["DepartmentId"].ToString()),
                                PlannedEmployees = int.Parse(designationNode.Attributes["PlannedEmployees"].ToString())
                            };
                            list.Add(obj);
                        }

                    }
                }
            }

            if (list.Count > 0)
            {
                Status status = NewHRManager.SaveUpdateEmployeePlan(list);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Employee planning saved succesfully.");
                    LoadEmployeeHierarchy();
                    treeEmployeeHierarchy.Render();
                    treeEmployeeHierarchy.AddCls("treepanelCls");
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
            }

            treeEmployeeHierarchy.ExpandAll();
        }

        protected void cmbGroupName_Change(object sender, DirectEventArgs e)
        {
            ClearValues();
            cmbLevel.Text = "";
            cmbDesignation.Text = "";

            cmbLevel.Store[0].DataSource = NewPayrollManager.GetParentLevels(int.Parse(cmbGroup.SelectedItem.Value));
            cmbLevel.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = new List<EDesignation>();
            cmbDesignation.Store[0].DataBind();
        }

        protected void cmbLevel_Change(object sender, DirectEventArgs e)
        {
            ClearValues();
            cmbDesignation.Text = "";
            cmbDesignation.Store[0].DataSource = NewPayrollManager.GetDesignationByLevel(int.Parse(cmbLevel.SelectedItem.Value));
            cmbDesignation.Store[0].DataBind();
        }

        private void ClearEmployeesCount()
        {
            txtExistingEmployeeNo.Text = "";
            txtPlannedEmployeeNo.Text = "";
            txtShortExcessNo.Text = "";
        }

        protected void cmbDepartmentAdd_Change(object sender, DirectEventArgs e)
        {
            ClearEmployeesCount();
            EmployeePlanning obj = NewHRManager.GetEmployeePlanningByIds(int.Parse(cmbGroup.SelectedItem.Value), int.Parse(cmbLevel.SelectedItem.Value), int.Parse(cmbDesignation.SelectedItem.Value), int.Parse(cmbBranchAdd.SelectedItem.Value), int.Parse(cmbDepartmentAdd.SelectedItem.Value));
            if (obj != null)
            {
                int existingEmployees = 0, plannedEmployees = 0, shortExcess = 0;
                existingEmployees = NewHRManager.GetEmployeePlanList(int.Parse(cmbBranchAdd.SelectedItem.Value), int.Parse(cmbDepartmentAdd.SelectedItem.Value), "Branch").Where(x => x.LevelGroupId == int.Parse(cmbGroup.SelectedItem.Value) && x.LevelId == int.Parse(cmbLevel.SelectedItem.Value) && x.DesignationId == int.Parse(cmbDesignation.SelectedItem.Value)).ToList().Count;

                if (obj.PlannedEmployees != null)
                    plannedEmployees = obj.PlannedEmployees.Value;

                shortExcess = plannedEmployees - existingEmployees;

                txtExistingEmployeeNo.Text = existingEmployees.ToString();
                txtPlannedEmployeeNo.Text = plannedEmployees.ToString();
                txtShortExcessNo.Text = Math.Abs(shortExcess).ToString();


                if (shortExcess > 0)
                    txtShortExcessNo.FieldStyle = "background-color:green;";
                else if (shortExcess < 0)
                    txtShortExcessNo.FieldStyle = "background-color:red;";
                else
                    txtShortExcessNo.FieldStyle = "background-color:white;";
            }
            else
                txtShortExcessNo.FieldStyle = "background-color:white;";
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            ClearValues();
            WEmployeePlan.Show();
        }

        protected void btnSaveEmpPlan_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveEmployeePlan");
            if (Page.IsValid)
            {
                List<EmployeePlanning> list = new List<EmployeePlanning>();
                EmployeePlanning obj = new EmployeePlanning()
                {
                    LevelGroupId = int.Parse(cmbGroup.SelectedItem.Value),
                    LevelId = int.Parse(cmbLevel.SelectedItem.Value),
                    DesignationId = int.Parse(cmbDesignation.SelectedItem.Value),
                    BranchId = int.Parse(cmbBranchAdd.SelectedItem.Value),
                    DepartmentId = int.Parse(cmbDepartmentAdd.SelectedItem.Value),
                    PlannedEmployees = int.Parse(txtPlannedEmployeeNo.Text.Trim())
                };

                list.Add(obj);

                Status status = NewHRManager.SaveUpdateEmployeePlan(list);
                if (status.IsSuccess)
                {
                    WEmployeePlan.Close();
                    NewMessage.ShowNormalMessage("Employee planning saved succesfully.");
                    LoadEmployeeHierarchy();
                    treeEmployeeHierarchy.Render();
                    treeEmployeeHierarchy.AddCls("treepanelCls");
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);

                treeEmployeeHierarchy.ExpandAll();
            }
        }
    }
}