﻿<%@ Page Title="Deputation List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="DeputationSettings.aspx.cs" Inherits="Web.NewHR.UserControls.DeputationSettings" %>

<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmploymentHistoryCtl.ascx" TagName="EmploymentHistoryCtl"
    TagPrefix="ucEmploymentHistoryCtl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Deputation Settings
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden runat="server" ID="hdnID">
        </ext:Hidden>
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <div style="width:1030px">
                <!-- panel-heading -->
                <div class="panel-body">
                    <ext:GridPanel ID="Grid" runat="server" Cls="itemgrid">
                        <Store>
                            <ext:Store ID="StoreHealth" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="DeputationId">
                                        <Fields>
                                            <ext:ModelField Name="DeputationId" Type="string" />
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="Description" Type="string" />
                                            <ext:ModelField Name="AttendanceInText" Type="string" />
                                            <ext:ModelField Name="DailyAllowanceText" Type="string" />
                                            <ext:ModelField Name="ApplicableSalaryAllowanceText" Type="string" />
                                            <ext:ModelField Name="TravellingAllowanceText" Type="string" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="Name" DataIndex="Name" Width="150" />
                                <ext:Column ID="Column2" runat="server" Text="Description" DataIndex="Description"
                                    Width="150">
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" Text="Attendance" DataIndex="AttendanceInText"
                                    Width="150">
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Text="DA" DataIndex="DailyAllowanceText"
                                    Width="150">
                                </ext:Column>
                                <ext:Column ID="Column5" runat="server" Text="Salary" DataIndex="ApplicableSalaryAllowanceText"
                                    Width="150">
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="TA" DataIndex="TravellingAllowanceText"
                                    Width="150">
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="45">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Edit" Text="<i class='fa fa-pencil'></i>"
                                            CommandName="Edit" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridHealth_Command">
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.DeputationId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:CommandColumn ID="CommandColumn2" runat="server" Width="45">
                                    <Commands>
                                        <ext:CommandSeparator />
                                        <ext:GridCommand Cls="editGridButton" ToolTip-Text="Delete" Text="<i class='fa fa-trash-o'></i>"
                                            CommandName="Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="GridHealth_Command">
                                            <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Deputation?" />
                                            <EventMask ShowMask="true">
                                            </EventMask>
                                            <ExtraParams>
                                                <ext:Parameter Name="ID" Value="record.data.DeputationId" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <div class="buttonBlock">
                        <ext:Button runat="server" ID="LinkButton1" Cls="btn btn-primary btn-sect" Text="<i></i>Add Deputation Type">
                            <DirectEvents>
                                <Click OnEvent="btnAddNewLine_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ext:Window ID="AEWindow" runat="server" Title="Add/Edit Deputation" Icon="Application"
        Height="500" Width="500" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
                        <ext:TextField Width="180px" ID="txtName" runat="server" FieldLabel="Name *" LabelAlign="top"
                            LabelSeparator="" />
                        <asp:RequiredFieldValidator Display="None" ID="valtxtHealthStatus" runat="server"
                            ValidationGroup="SaveUpdate" ControlToValidate="txtName" ErrorMessage="Name is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:TextArea Height="70px" Width="350px" ID="txtDescription" runat="server" FieldLabel="Description"
                            LabelAlign="top" LabelSeparator="" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>
                            Settings</h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbAttendance" Width="350" LabelWidth="180" runat="server" ValueField="Value"
                            DisplayField="Text" FieldLabel="Attendance in" LabelAlign="Right" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store1" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" />
                                                <ext:ModelField Name="Text" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="valcmbContition" runat="server" ValidationGroup="SaveUpdate"
                            ControlToValidate="cmbAttendance" ErrorMessage="Attendance in is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbTA" Width="350" LabelWidth="180" runat="server" ValueField="Value"
                            DisplayField="Text" FieldLabel="Travelling Allowance" LabelAlign="Right" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model3" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" />
                                                <ext:ModelField Name="Text" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                            ValidationGroup="SaveUpdate" ControlToValidate="cmbTA" ErrorMessage="Travelling Allowance is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbDA" Width="350" LabelWidth="180" runat="server" ValueField="Value"
                            DisplayField="Text" FieldLabel="Daily Allowance" LabelAlign="Right" LabelSeparator=""
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store3" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" />
                                                <ext:ModelField Name="Text" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                            ValidationGroup="SaveUpdate" ControlToValidate="cmbTA" ErrorMessage="Daily Allowance is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbSalary" Width="350" LabelWidth="180" runat="server" ValueField="Value"
                            DisplayField="Text" FieldLabel="Applicable Salary/Allowance" LabelAlign="Right"
                            LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store4" runat="server">
                                    <Model>
                                        <ext:Model ID="Model5" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="Value" />
                                                <ext:ModelField Name="Text" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server"
                            ValidationGroup="SaveUpdate" ControlToValidate="cmbSalary" ErrorMessage="Applicable Salary/Allowance is required." />
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" colspan="2">
                        <div class="popupButtonDiv" style="padding-top: 15px">
                            <ext:Button runat="server" ID="btnSave" Cls="btn btn-primary btn-sect btn-sm" Text="<i></i>Save">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <Listeners>
                                    <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                            <div class="btnFlatOr">
                                or</div>
                            <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton2" Text="<i></i>Cancel">
                                <Listeners>
                                    <Click Handler="#{AEWindow}.hide();">
                                    </Click>
                                </Listeners>
                            </ext:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </Content>
    </ext:Window>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
