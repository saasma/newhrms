﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Leave.aspx.cs" Inherits="Web.NewHR.Leave" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Leave TreePanel</title>
 <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <script type="text/javascript">

        var targetTeamId = "";
        var sourceIDList = "";

        var dropHandler = function (e1, e2, e3, e4, e5) {
            <%=hdnTeam.ClientID %>.setValue(targetTeamId);
            <%=hdnList.ClientID %>.setValue(sourceIDList);
            <%=btnUpdate.ClientID %>.fireEvent('click');
        }
        var nodeDragOver = function (targetNode, position, dragData) {

            // target node should be 2 (Team)
            if (targetNode.data.depth != 2) {
                return false;
            }

            targetTeamId = targetNode.data.id;

            sourceIDList = "";

            for (i = 0; i < dragData.records.length; i++) {
                var record = dragData.records[i];
                // if dragged Depth is not 3(Employee) then cancel the drag & drop
                if (record.data.depth != 3)
                    return false;

                if (sourceIDList == "")
                    sourceIDList = record.data.id;
                else
                    sourceIDList += "," + record.data.id;

            }

            return true;
        };
    </script>
</head>
<body>
    <ext:ResourceManager ScriptMode="Debug" runat="server" />
    <ext:Hidden ID="hdnTeam" Text="" runat="server" />
    <ext:Hidden ID="hdnList" Text="" runat="server" />
    <ext:Button ID="btnUpdate" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnUpdate_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <h3>
        Manage Teams</h3>
    <table>
        <tr>
            <td valign="top">
                <ext:TreePanel MultiSelect="true" Icon="ApplicationGet" ID="treePanelEmpStruct" RootVisible="false"
                    MinHeight="800" Width="400" AutoScroll="true" Animate="true" Mode="Local" runat="server"
                    Collapsible="false" UseArrows="true" Title="Employee Structure">
                    <Fields>
                        <ext:ModelField Name="projectName" />
                    </Fields>
                    <ColumnModel>
                        <Columns>
                            <ext:TreeColumn ID="TreeColumn1" runat="server" Text="Task" DataIndex="text" Flex="1">
                            </ext:TreeColumn>
                            <ext:Column ID="Column1" runat="server" Text="Associated Team" Flex="1" DataIndex="projectName">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <%-- <Listeners>
                        <beforenodedrop fn="beforenodedrop" />
                    </Listeners>--%>
                    <Listeners>
                    </Listeners>
                    <View>
                        <ext:TreeView ID="TreeView1" runat="server">
                            <Plugins>
                                <ext:TreeViewDragDrop ID="TreeViewDragDrop1" EnableDrag="true" EnableDrop="false"
                                    runat="server" AppendOnly="true" />
                            </Plugins>
                        </ext:TreeView>
                    </View>
                </ext:TreePanel>
            </td>
            <td style="width: 50px">
            </td>
            <td valign="top">
                <ext:TreePanel ID="treePanelLeaveTeams" RootVisible="false" MinHeight="800" Width="400"
                    AutoScroll="true" Animate="true" Mode="Local" runat="server" Collapsible="false"
                    UseArrows="true" Title="Leave Teams">
                    <%--    <Listeners>
                        <befored fn="beforenodedrop" />
                    </Listeners>--%>
                    <View>
                        <ext:TreeView ID="treeViewLeaveTeams" runat="server">
                            <Listeners>
                                <Drop Fn="dropHandler" />
                            </Listeners>
                            <Plugins>
                                <ext:TreeViewDragDrop Icon="Folder" EnableDrag="false" EnableDrop="true" Expanded="true"
                                    ID="leaveTeamsDragDrop" runat="server" ContainerScroll="true">
                                </ext:TreeViewDragDrop>
                            </Plugins>
                        </ext:TreeView>
                    </View>
                    <Listeners>
                        <NodeDragOver Fn="nodeDragOver" />
                    </Listeners>
                </ext:TreePanel>
            </td>
        </tr>
    </table>
</body>
</html>
