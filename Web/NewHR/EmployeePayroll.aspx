﻿<%@ Page Title="Employee Payroll Information" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeePayroll.aspx.cs" Inherits="Web.NewHR.EmployeePayroll" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function changePasswordCall(userName) {
            var ret = changePassword("isPopup=true&u=" + userName);
            if (typeof (ret) != 'undefined') {
            }
        }
        function refreshWindow() {
            window.location.reload();
        }
        /*Incomes */
        function popupIncomeCall() {
            if (typeof (EmployeeId) != 'undefined') {
                var ret = popupIncome("EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'ReloadIncome') {
                        refreshIncomeList(0, EmployeeId);
                    }
                }
            }
            return false;
        }


        function popupUpdateIncomeCall(empIncomeId) {
            var ret = popupUpdateIncome('Id=' + empIncomeId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    refreshIncomeList(0, EmployeeId);
                }
            }
            return false;
        }

        function parentIncomeCallbackFunction(text, closingWindow, EmployeeId) {
            closingWindow.close();
            refreshIncomeList(0, EmployeeId);
        }

        //call to delete income
        function deleteIncomeCall(empIncomeId, employeeId,anchor) {
            
            var text = anchor.innerHTML.toString().toLowerCase();
            var msg = "";
            if(text=="un-apply")
            {
                msg  = "Do you want to remove the income for the employee?";
                isChangeAndEdit=true;
            }
            else if(text=="restore matrix")
            {
                msg  = "Are you sure, you want to restore source from Matrix for the employee?";
                isChangeAndEdit=false;
            }
            //            else if(text=="set source amount")
            //            {
            //                msg  = "Are you sure,you want to set the source from amount for the employee?";
            //            }    

            if (confirm(msg)) {
                showLoading();
                incomeDiv = document.getElementById('<%=incomeList.ClientID %>');
                 Web.PayrollService.DeleteIncome(empIncomeId, employeeId,text,refreshIncomeListCallback);
             }
             return false;
         }

         var incomeDiv = null;
         function refreshIncomeList(empIncomeId, EmployeeId) {
             if (typeof (EmployeeId) != 'undefined') {
                 showLoading();
                 incomeDiv = document.getElementById('<%=incomeList.ClientID %>');
                Web.PayrollService.GetIncomeList(empIncomeId, parseInt(EmployeeId), refreshIncomeListCallback);
            }
        }

        function refreshIncomeListCallback(result) {
            if (result) {
                incomeDiv.innerHTML
                = result;
            }
            hideLoading();
        }

        /*Deduction*/

        function popupDeductionCall() {

            if (typeof (EmployeeId) != 'undefined') {
                var ret = popupDeduction("EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'ReloadDeduction') {
                        refreshDeductionList(0, EmployeeId);
                    }
                }
            }
            return false;
        }

        function reloadDeduction(closingWindow, EmployeeId) {
            closingWindow.close();
            refreshDeductionList(0, EmployeeId);
        }

        function popupUpdateDeductionCall(deductionId) {

            var ret = popupUpdateDeduction('Id=' + deductionId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshDeductionList(0, EmployeeId);
                }
            }
            return false;
        }
        //call to delete income
        function deleteDeductionCall(deductionId, employeeId) {
            if (confirm("Do you want to delete the deduction for the employee?")) {
                refreshDeductionList(deductionId, employeeId);
            }
            return false;
        }
        var deductionDiv = null;
        function refreshDeductionList(deductionId, EmployeeId) {
            if (typeof (EmployeeId) != 'undefined') {
                showLoading();
                deductionDiv = document.getElementById('<%= deductionList  %>' + EmployeeId);
                Web.PayrollService.GetDeductionList(deductionId, parseInt(EmployeeId), refreshDeductionListCallback);
            }
        }

        function refreshDeductionListCallback(result) {
            if (result) {
                deductionDiv.innerHTML
                = result;
            }
            hideLoading();
        }


        var isChangeAndEdit = false;
        function applyIncome(incomeId,anchor)
        {   
            var text = anchor.innerHTML.toString().toLowerCase();
            if(text=="change")
                isChangeAndEdit=true;
            else
                isChangeAndEdit=false;
            if(confirm("Are you sure, you want to "+text+" the income to the employee?"))
            {
                Web.PayrollService.AddIncomeToEmployee
                (incomeId, parseInt(EmployeeId), applyIncomeCallback);
         
            }
        }

        function applyIncomeCallback(employeeIncomeId)
        {
            if(isChangeAndEdit)
            {
                popupUpdateIncomeCall(employeeIncomeId);
            }
            refreshIncomeList(0, EmployeeId);
        }

        function popupIncomeCall() {
            if (typeof (EmployeeId) != 'undefined') {
                var ret = popupIncome("EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'ReloadIncome') {
                        refreshIncomeList(0, EmployeeId);
                    }
                }
            }
            return false;
        }


        function popupUpdateIncomeCall(empIncomeId) {
            var ret = popupUpdateIncome('Id=' + empIncomeId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'ReloadIncome') {
                    refreshIncomeList(0, EmployeeId);
                }
            }
            return false;
        }

        function parentIncomeCallbackFunction(text, closingWindow) {

            closingWindow.close();
            refreshIncomeList(0, EmployeeId);
        }

        /*Deduction section*/
        function popupDeductionCall() {

            if (typeof (EmployeeId) != 'undefined') {
                var ret = popupDeduction("EId=" + parseInt(EmployeeId));
                if (typeof (ret) != 'undefined') {
                    if (ret == 'ReloadDeduction') {
                        refreshDeductionList(0);
                    }
                }
            }
            return false;
        }

        function reloadDeduction(closingWindow) {
            closingWindow.close();
            refreshDeductionList(0);
        }

        function popupUpdateDeductionCall(deductionId) {

            var ret = popupUpdateDeduction('Id=' + deductionId);
            if (typeof (ret) != 'undefined') {
                if (ret == 'Reload') {
                    refreshDeductionList(0);
                }
            }
            return false;
        }
        //call to delete income
        function deleteDeductionCall(deductionId) {
            if (confirm("Do you want to delete the deduction for the employee?")) {
                refreshDeductionList(deductionId);
            }
            return false;
        }

        function refreshDeductionList(deductionId) {
            if (typeof (EmployeeId) != 'undefined') {
                showLoading();
                Web.PayrollService.GetDeductionList(deductionId, parseInt(EmployeeId), refreshDeductionListCallback);
            }
        }

        function refreshDeductionListCallback(result) {
            if (result) {
                document.getElementById('<%=deductionList.ClientID %>').innerHTML
            = result;
        }
        hideLoading();
    }
    function citChange(isAmount) {
        var txtCITRateAdditionalAmount = <%=txtCITRateAdditionalAmount.ClientID %>;
            var txtCITAmount = <%=txtCITAmount.ClientID %>;
            var txtCITRate = <%=txtCITRate.ClientID %>;
            if (isAmount) {
                txtCITRate.disable();
                txtCITAmount.enable();
                txtCITRateAdditionalAmount.disable();
            }
            else{
                txtCITRate.enable();
                txtCITRateAdditionalAmount.enable();
                txtCITAmount.disable();
            }
        }
        var ddlNoCITFromMonth = '<%= ddlNoCITFromMonth.ClientID %>';
        var ddlNoCITFromYear = '<%= ddlNoCITFromYear.ClientID %>';
        var ddlNoCITToMonth = '<%=ddlNoCITToMonth.ClientID %>';
        var ddlNoCITToYear = '<%= ddlNoCITToYear.ClientID %>';
        function validateNoCITDates(source, args) {
            args.IsValid = false;
           
            if (document.getElementById('<%= chkCITNoContribution.ClientID %>').checked) {
               
                var noCITFrom = $('#' + ddlNoCITFromYear).val() + "/" + $('#' + ddlNoCITFromMonth).val() + "/" + "1";
                var noCITTo = $('#' + ddlNoCITToYear).val()+ "/" + $('#' + ddlNoCITToMonth).val() + "/1" ;
               

                if (isDateSame(noCITFrom,noCITTo)) {

                }

                else if (isSecondCalendarCtlDateGreater(noCITFrom, noCITTo) == false) {
                    alert('<%= Resources.Messages.EmployeeNoCITToDateGreaterEqualThanToDate  %>');
                    return;
                }
        }
        args.IsValid = true;
    }

    function ValidateTextBoxBank(source, args) {
        var value = <%= cmbPaymentMode.ClientID %>.getValue();

            if(value==null)
            {
                args.IsValid = false;
                return;
            }

            args.IsValid = true;
            if (value == "Bank Deposit" || value == "Cheque") {
                var str = new String(args.Value);
                if (str.trim() == "")
                    args.IsValid = false;
            }
        }

        function ValidateTextBox1(source, args) {
            var value = <%= cmbPaymentMode.ClientID %>.getValue();

            if(value==null)
            {
                args.IsValid = false;
                return;
            }

            args.IsValid = true;

            if (value == "Bank Deposit" || value == "Cheque") {
                var str = new String(args.Value);
                if (str.trim() == "")
                    args.IsValid = false;
            }
        }

        function onChangeForPayMode(value) {
            var txtBankACNo = <%= txtBankNo.ClientID %>;
            var txtBankName = <%= cmbBankName.ClientID %>;

            if (value == "Bank Deposit" || value == "Cheque") {
                txtBankACNo.enable();
                txtBankName.enable();
            }
            else {
                txtBankACNo.disable();
                txtBankName.disable();
            }
        }
    </script>
    <style type="text/css">
        .disabledIncomeDeduction td
        {
            background-color: #ddd!important;
        }
        
        .unappliedIncomeDeduction td
        {
            color: lightgray;
        }
        
        .widget .widget-body
        {
            background: white;
        }
        
        .table .selected td
        {
            background-color: lightgray !important;
        }
        
        .nogap
        {
            margin-top: -10px;
            margin-bottom: 3px;
        }
        .nogapbottom
        {
            margin-top: 0px;
            margin-bottom: 2px;
        }
        .userDetailsClass
        {
            background-color: #D8E7F3;
            height: 50px;
            text-align: center;
            padding-top: 10px;
            margin-bottom: 20px;
        }
        .panel-heading .accordion-toggle:after
        {
            /* symbol for "opening" panels */
            font-family: 'Glyphicons Halflings'; /* essential for enabling glyphicon */
            content: "\e114"; /* adjust as needed, taken from bootstrap.css */
            float: right; /* adjust as needed */
            color: grey; /* adjust as needed */
        }
        .panel-heading .accordion-toggle.collapsed:after
        {
            /* symbol for "collapsed" panels */
            content: "\e080"; /* adjust as needed, taken from bootstrap.css */
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body" style="margin-left: 148px;">
                <h4>
                    Payroll Information Setting
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <%--<uc2:EmployeeWizard Id="EmployeeWizard1" runat="server" />--%>
            <ext:Label ID="lblMsg" runat="server" />
            <table>
                <tr>
                    <td valign="top">
                        <div style="float: left; margin-right: 30px;">
                            <ucEW:EmpDetailsWizard Id="EmployeeWizard1" runat="server" />
                        </div>
                    </td>
                    <td valign="top">
                        <div class="userDetailsClass">
                            <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                        </div>
                        <p>
                            Click to expand Tax Information</p>
                        <div class="panel panel-danger" style="border: 0px;">
                            <div class="panel-heading">
                                <div class="panel-btns" style="display: block!important;">
                                    <a href="#" class="panel-minimize tooltips maximize" data-toggle="tooltip" title=""
                                        data-original-title="Minimize Panel"><i class="fa fa-plus"></i></a>
                                </div>
                                <!-- panel-btns -->
                                <h3 class="panel-title" style='cursor: pointer' onclick='document.getElementById("bodyDetails").style.display="block";'>
                                    Employee Tax Information</h3>
                            </div>
                            <div class="panel-body" id="bodyDetails" style="display: none; overflow: hidden;
                                padding: 0px;">
                                <div class="alert alert-warning nogapbottom">
                                    These Tax related settings have been collected from various Employee Information
                                    pages. Check the source if you want to change them.
                                </div>
                                <table class="table table-bordered table-condensed table-striped table-primary">
                                    <thead>
                                        <tr>
                                            <th style="width: 150px">
                                                Criteria
                                            </th>
                                            <th style="width: 150px">
                                                Values
                                            </th>
                                            <th>
                                                Source
                                            </th>
                                            <th>
                                                Effect on Tax Calculation
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td runat="server" id="gender">
                                                {0}
                                            </td>
                                            <td>
                                                <ext:Label ID="lblGender" runat="server" />
                                            </td>
                                            <td>
                                                Personal Details
                                            </td>
                                            <td>
                                                <ext:Label ID="lblGenderTaxEffect" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Martial Status
                                            </td>
                                            <td>
                                                <ext:Label ID="lblMaritalStatus" runat="server" />
                                            </td>
                                            <td>
                                                Personal Details
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tax Status
                                            </td>
                                            <td style="padding-left: 0px">
                                                <ext:Checkbox ID="chkTaxStatus" Disabled="true" BoxLabel="Couple" BoxLabelAlign="Before"
                                                    runat="server" />
                                                <%--  <ext:Label ID="lblTaxStatus" runat="server" />--%>
                                            </td>
                                            <td>
                                                <ext:LinkButton runat="server" ID="btnTaxStatusEdit" Text="Edit">
                                                    <Listeners>
                                                        <Click Handler="#{chkTaxStatus}.enable();" />
                                                    </Listeners>
                                                </ext:LinkButton>
                                            </td>
                                            <td>
                                                Only those Females who have chosen Single Tax Status will get Tax Rebate
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Income from Other Source
                                            </td>
                                            <td style="padding-left: 0px">
                                                <ext:Checkbox ID="chkIsIncomeFromOtherSource" Disabled="true" BoxLabel="" BoxLabelAlign="Before"
                                                    runat="server" />
                                                <%--  <ext:Label ID="lblTaxStatus" runat="server" />--%>
                                            </td>
                                            <td>
                                                <ext:LinkButton runat="server" ID="LinkButton4" Text="Edit">
                                                    <Listeners>
                                                        <Click Handler="#{chkIsIncomeFromOtherSource}.enable();" />
                                                    </Listeners>
                                                </ext:LinkButton>
                                            </td>
                                            <td>
                                                If the employee has income from other sources, the employee is not required to pay
                                                SST and the employee has to file a separate return. And if the employee is female,
                                                she will not get 10% tax rebate.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" ID="lblHandicapped1" />
                                            </td>
                                            <td>
                                                <ext:Label ID="lblHandicapped" runat="server" />
                                            </td>
                                            <td>
                                                Health Information
                                            </td>
                                            <td>
                                                50% additional amount is allowed on Exemption limit for
                                                <asp:Label runat="server" ID="lblHandicapped2" />
                                                employees
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Remote Area
                                            </td>
                                            <td>
                                                <ext:Label ID="lblRemoateArea" Text="N/A" runat="server" />
                                            </td>
                                            <td>
                                                <ext:Label ID="lblBranch" Text="Branch Location" runat="server" />
                                            </td>
                                            <td>
                                                Tax rebate will be given for defined remote areas if the employee works in Remote
                                                Branch
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Local Employee
                                            </td>
                                            <td>
                                                <ext:Checkbox ID="chkLocalEmployee" Disabled="true" BoxLabelAlign="Before" runat="server" />
                                                <%-- <ext:Label ID="lblLocalEmployee" Text="No" runat="server" />--%>
                                            </td>
                                            <td>
                                                <ext:LinkButton runat="server" ID="btnEditLocalEmployee" Text="Edit">
                                                    <Listeners>
                                                        <Click Handler="#{chkLocalEmployee}.enable();" />
                                                    </Listeners>
                                                </ext:LinkButton>
                                            </td>
                                            <td>
                                                For local employees in the remote area, there is no tax rebate
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Non Resident
                                            </td>
                                            <td>
                                                <ext:Checkbox ID="chkNonResident" Disabled="true" BoxLabelAlign="Before" runat="server" />
                                                <%--<ext:Label ID="lblNonResident" Text="No" runat="server" />--%>
                                            </td>
                                            <td>
                                                <ext:LinkButton runat="server" ID="btnNonResident" Text="Edit">
                                                    <Listeners>
                                                        <Click Handler="#{chkNonResident}.enable();" />
                                                    </Listeners>
                                                </ext:LinkButton>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                No Tax
                                            </td>
                                            <td>
                                                <ext:Checkbox ID="chkNoTax" Disabled="true" BoxLabelAlign="After" runat="server" />
                                                <%--<ext:Label ID="lblNoTax" Text="No" runat="server" />--%>
                                            </td>
                                            <td>
                                                <ext:LinkButton runat="server" ID="btnEditNoTax" Text="Edit">
                                                    <Listeners>
                                                        <Click Handler="#{chkNoTax}.enable();" />
                                                    </Listeners>
                                                </ext:LinkButton>
                                            </td>
                                            <td>
                                                If checked, tax will not be deducted. Do not change this without your auditor's
                                                advice.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                No Provident Fund
                                            </td>
                                            <td>
                                                <ext:Checkbox ID="chkNoPF" Disabled="true" BoxLabelAlign="After" runat="server" />
                                                <%--<ext:Label ID="lblNoTax" Text="No" runat="server" />--%>
                                            </td>
                                            <td>
                                                <ext:LinkButton runat="server" ID="LinkButton3" Text="Edit">
                                                    <Listeners>
                                                        <Click Handler="#{chkNoPF}.enable();" />
                                                    </Listeners>
                                                </ext:LinkButton>
                                            </td>
                                            <td>
                                                If checked, the employee will not be eligible for PF even if otherwise the employee
                                                is eligible for Provident Fund.
                                            </td>
                                        </tr>
                                        <tr runat="server" id="rowDoNotCalculateSalary">
                                            <td>
                                                Do Not Calculate Salary
                                            </td>
                                            <td>
                                                <ext:Checkbox ID="chkExcludeInSalary" Disabled="true" BoxLabelAlign="After" runat="server" />
                                                <%--<ext:Label ID="lblNoTax" Text="No" runat="server" />--%>
                                            </td>
                                            <td>
                                                <ext:LinkButton runat="server" ID="LinkButton5" Text="Edit">
                                                    <Listeners>
                                                        <Click Handler="#{chkExcludeInSalary}.enable();" />
                                                    </Listeners>
                                                </ext:LinkButton>
                                            </td>
                                            <td>
                                                If checked, employee salary will not be calculated. To calculate salary this option
                                                should be unchecked and valid Salary Calculate date need to place from Personal
                                                Details
                                            </td>
                                        </tr>
                                        <%--  <tr>
                                            <td>
                                               Do Not Calculate Gratuity
                                            </td>
                                            <td>
                                                <ext:Checkbox ID="chkExcludeGratuity" Disabled="true" BoxLabelAlign="After" runat="server" />
                                              
                                            </td>
                                            <td>
                                                <ext:LinkButton runat="server" ID="LinkButton6" Text="Edit">
                                                    <Listeners>
                                                        <Click Handler="#{chkExcludeGratuity}.enable();" />
                                                    </Listeners>
                                                </ext:LinkButton>
                                            </td>
                                            <td>
                                                If checked, employee gratuity will not be calculated. 
                                            </td>
                                        </tr>--%>
                                    </tbody>
                                </table>
                                <table class="table table-bordered table-condensed table-striped table-primary" style="margin-top: 10px;
                                    border-top: 1px solid #dddddd;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 150px">
                                                Retirement fund
                                            </td>
                                            <td style="width: 150px">
                                                <ext:Label ID="lblRetirementFund" Text="No" runat="server" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                CIT Contribution
                                            </td>
                                            <td>
                                                <ext:Label ID="lblCITContribution" Text="No" runat="server" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Insurance
                                            </td>
                                            <td>
                                                <ext:Label ID="lblInsurance" Text="No" runat="server" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Medical Tax Credit
                                            </td>
                                            <td>
                                                <ext:Label ID="lblMedicalTaxCredit" Text="No" runat="server" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- panel-body -->
                        </div>
                        <%-- <div class="panel-group" id="accordion2">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2">
                                           
                                                Employee Tax Information
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                            </div>
                                </div>
                            </div>
                            <!-- panel -->
                        </div>--%>
                        <div class="separator bottom">
                        </div>
                        <h4 class="heading">
                            Incomes</h4>
                        <div class="alert alert-warning nogap">
                            Incomes shown in gray are currently not received by the employee.
                        </div>
                        <div id='incomeList' runat="server">
                        </div>
                        <div style="margin-top: 20px; margin-bottom: 20px;">
                            <ext:Button runat="server" ID="btnSaveUpdate" Cls="btn btn-success" Text="<i></i>Add Income">
                                <Listeners>
                                    <Click Handler="return popupIncomeCall();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </div>
                        <h4 class="heading">
                            Deductions</h4>
                        <div id='deductionList' runat="server" style="width: 845px">
                        </div>
                        <div style="margin-top: 20px; margin-bottom: 20px;">
                            <ext:Button runat="server" Cls="btn btn-success" ID="LinkButton1" Text="<i></i>Add Deduction">
                                <Listeners>
                                    <Click Handler="return popupDeductionCall();">
                                    </Click>
                                </Listeners>
                            </ext:Button>
                        </div>
                        <div style="width: 845px; border: 1px solid #428BCA; margin-bottom: 15px;">
                            <div>
                                <!-- panel-btns -->
                                <h4 class="panel-title sectionHeading" style="margin-left: 0px;">
                                    <i></i>Salary Payment
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body">
                                <table class="fieldTable firsttdskip">
                                    <tr>
                                        <td>
                                            <ext:ComboBox ID="cmbPaymentMode" Width="180px" runat="server" FieldLabel="Payment Mode"
                                                LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                                <Items>
                                                    <ext:ListItem Text="Cash" Value="Cash" />
                                                    <ext:ListItem Text="Bank Deposit" Value="Bank Deposit" />
                                                </Items>
                                                <Listeners>
                                                    <Select Handler="onChangeForPayMode(this.getValue())" />
                                                </Listeners>
                                            </ext:ComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select payment mode."
                                                Display="None" ControlToValidate="cmbPaymentMode" ValidationGroup="SaveUpdate"
                                                runat="server">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:ComboBox Disabled="true" ID="cmbBankName" Width="180px" runat="server" ValueField="BankID"
                                                DisplayField="Name" FieldLabel="Bank Name" LabelAlign="top" LabelSeparator=""
                                                ForceSelection="true" QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store2" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="BankID" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="cmbBankName_Change">
                                                        <EventMask ShowMask="true" />
                                                    </Select>
                                                </DirectEvents>
                                            </ext:ComboBox>
                                            <asp:CustomValidator Display="None" ValidateEmptyText="true" ClientValidationFunction="ValidateTextBoxBank"
                                                ControlToValidate="cmbBankName" ValidationGroup="SaveUpdate" ID="custValidateBankName3"
                                                runat="server" ErrorMessage="Bank Name is required."></asp:CustomValidator>
                                        </td>
                                        <td>
                                            <ext:TextField Disabled="true" ID="txtBankNo" LabelSeparator="" runat="server" FieldLabel="Salary/Bank Account Number"
                                                LabelAlign="Top" Width="180px" />
                                            <asp:CustomValidator Display="None" ValidateEmptyText="true" ClientValidationFunction="ValidateTextBox1"
                                                ControlToValidate="txtBankNo" ValidationGroup="SaveUpdate" ID="valCustomRFAC3"
                                                runat="server" ErrorMessage="Bank Account No is required."></asp:CustomValidator>
                                        </td>
                                        <td style="display: none">
                                            <ext:TextField ID="txtLoanAccount" LabelSeparator="" runat="server" FieldLabel="Loan Account Number"
                                                LabelAlign="Top" Width="180px" />
                                        </td>
                                        <td style="display: none">
                                            <ext:TextField ID="txtAdvanceAccount" LabelSeparator="" runat="server" FieldLabel="Advance Account Number"
                                                LabelAlign="Top" Width="180px" />
                                        </td>
                                        <td>
                                            <ext:ComboBox ID="cmbBankBranch" Width="180px" runat="server" ValueField="BankBranchID"
                                                DisplayField="Name" FieldLabel="Bank Branch Name" LabelAlign="top" LabelSeparator=""
                                                ForceSelection="true" QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store3" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model3" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="BankBranchID" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <Triggers>
                                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                                </Triggers>
                                                <Listeners>
                                                    <Select Handler="this.getTrigger(0).show();" />
                                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                                </Listeners>
                                            </ext:ComboBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="width: 845px; border: 1px solid #428BCA; margin-bottom: 15px;">
                            <div>
                                <!-- panel-btns -->
                                <h4 class="panel-title sectionHeading" style="margin-left: 0px;">
                                    <i></i>Permanent Account Number
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body">
                                <table class="fieldTable firsttdskip">
                                    <tr>
                                        <%--<td>
                                            <ext:ComboBox ID="cmbEmployeePAN" Width="180px" runat="server" ValueField="Key" DisplayField="Value"
                                                FieldLabel="Employee PAN" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store1" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model1" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="Key" />
                                                                    <ext:ModelField Name="Value" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </td>--%>
                                        <td>
                                            <ext:TextField ID="txtPANNo" runat="server" FieldLabel="PAN No" LabelAlign="Top"
                                                Width="180px" />
                                            <asp:RegularExpressionValidator ID="valRegExPanNo2" runat="server" ControlToValidate="txtPANNo"
                                                Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ErrorMessage="Invalid Pan No, must be 9 digit value." ValidationGroup="SaveUpdate"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="width: 845px; border: 1px solid #428BCA; margin-bottom: 15px;">
                            <div>
                                <!-- panel-btns -->
                                <h4 class="panel-title sectionHeading" style="margin-left: 0px;">
                                    <i></i>Provident Fund
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body">
                                <table class="fieldTable firsttdskip">
                                    <tr runat="server" id="rowPFList">
                                        <td>
                                            <ext:ComboBox ID="cmbPFList" Width="180px" runat="server" ValueField="Key" DisplayField="Value"
                                                FieldLabel="Fund Type" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store4" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model4" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="Key" />
                                                                    <ext:ModelField Name="Value" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <SelectedItems>
                                                    <ext:ListItem Index="0" />
                                                </SelectedItems>
                                            </ext:ComboBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtPFNumber" runat="server" FieldLabel="PF Number" LabelSeparator=""
                                                LabelAlign="Top" Width="180px" />
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtCIDNumber" runat="server" FieldLabel="CID / Other PF No" LabelSeparator=""
                                                LabelAlign="Top" Width="180px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtPFNominee" runat="server" FieldLabel="Nominee" LabelSeparator=""
                                                LabelAlign="Top" Width="180px" />
                                        </td>
                                        <td>
                                            <ext:ComboBox ID="cmbPFRelation" Width="180px" runat="server" ValueField="Key" DisplayField="Value"
                                                FieldLabel="Relation" LabelAlign="top" LabelSeparator="" ForceSelection="true"
                                                QueryMode="Local">
                                                <Store>
                                                    <ext:Store ID="Store8" runat="server">
                                                        <Model>
                                                            <ext:Model ID="Model8" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="Key" />
                                                                    <ext:ModelField Name="Value" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td colspan="2">
                                        <ext:TextField ID="txtAdditionalPFDeduction" LabelSeparator="" FieldLabel="Additional PF Deduction" runat="server" LabelAlign="Top" Width="180px" />
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtAdditionalPFDeduction"
                                            Display="None" Operator="DataTypeCheck" Type="Currency" ErrorMessage="Invalid Additional PF deduction amount."
                                            ValidationGroup="SaveUpdate"></asp:CompareValidator></td>
                                    </tr>--%>
                                    <%--<tr>
                            <td>
                                <ext:ComboBox ID="cmbNOPF" Width="180px" runat="server" FieldLabel="No PF" LabelAlign="top"
                                    LabelSeparator="" ForceSelection="true" QueryMode="Local">
                                    <Items>
                                        <ext:ListItem Text="" Value="false" />
                                        <ext:ListItem Text="Not eligible for PF" Value="true" />
                                    </Items>
                                    <Listeners>
                                        <Select Handler="onChangeForPayMode(this.getValue())" />
                                    </Listeners>
                                </ext:ComboBox>
                            </td>
                        </tr>--%>
                                </table>
                            </div>
                        </div>
                        <div style="width: 845px; border: 1px solid #428BCA; margin-bottom: 15px;">
                            <div>
                                <!-- panel-btns -->
                                <h4 class="panel-title sectionHeading" style="margin-left: 0px;">
                                    <i></i>Citizen Investment Trust
                                </h4>
                            </div>
                            <!-- panel-heading -->
                            <div class="panel-body">
                                <table class="fieldTable firsttdskip">
                                    <tr>
                                        <td>
                                            <ext:TextField ID="txtCITNumber" runat="server" FieldLabel="CIT Number" LabelSeparator=""
                                                LabelAlign="Top" Width="180px" />
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtCITCode" runat="server" FieldLabel="CIT Code" LabelSeparator=""
                                                LabelAlign="Top" Width="180px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <ext:Checkbox ID="chkCITTable" NoteAlign="Top" Note="Set each month CIT Amount from Payroll -> Employee CIT"
                                                Disabled="true" runat="server" BoxLabel="Monthly Table" BoxLabelAlign="After">
                                            </ext:Checkbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:Radio ID="rdbAmount" Checked="true" Name="CIT" runat="server" BoxLabel="Monthly amount"
                                                BoxLabelAlign="After">
                                                <Listeners>
                                                    <Change Handler="citChange(#{rdbAmount}.getValue());" />
                                                </Listeners>
                                            </ext:Radio>
                                        </td>
                                        <td>
                                            <ext:TextField ID="txtCITAmount" runat="server" LabelAlign="Top" Width="180px" />
                                            <asp:CompareValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCITAmount"
                                                Display="None" Operator="DataTypeCheck" Type="Currency" ErrorMessage="Invalid CIT monthly amount."
                                                ValidationGroup="SaveUpdate"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ext:Radio Name="CIT" ID="rdbRate" runat="server" BoxLabel="Monthly rate" BoxLabelAlign="After">
                                                <Listeners>
                                                    <Change Handler="citChange(#{rdbAmount}.getValue());" />
                                                </Listeners>
                                            </ext:Radio>
                                        </td>
                                        <td>
                                            <ext:TextField Disabled="true" ID="txtCITRate" runat="server" LabelAlign="Top" Width="180px" />
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtCITRate"
                                                Display="None" Operator="DataTypeCheck" Type="Double" ErrorMessage="Invalid CIT rate."
                                                ValidationGroup="SaveUpdate"></asp:CompareValidator>
                                        </td>
                                        <td>
                                            % of
                                            <ext:Label ID="lblIncomes" runat="server" Text="Basic" />
                                        </td>
                                        <td>
                                            Additional CIT Amount to be added in monthly rate value
                                        </td>
                                        <td>
                                            <ext:TextField Disabled="true" ID="txtCITRateAdditionalAmount" runat="server" LabelAlign="Top"
                                                Width="180px" />
                                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtCITRateAdditionalAmount"
                                                Display="None" Operator="DataTypeCheck" Type="Currency" ErrorMessage="Invali amount."
                                                ValidationGroup="SaveUpdate"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 10px; padding-bottom: 5px;">
                                            <asp:CheckBox ID="chkCITNoContribution" runat="server" Text="No CIT Deduction" />
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="0" cellspacing="0" width="350px">
                                    <tr>
                                        <td class="lf" colspan="2">
                                            <div id="divNoCIT" runat="server">
                                                <table cellpadding="0" cellspacing="0" width="350px">
                                                    <tr>
                                                        <td width="150px" style="padding-bottom: 5px;">
                                                            <My:Label ID="lblFrom" runat="server" Text="From" />
                                                        </td>
                                                        <td style="padding-left: 45px">
                                                            <asp:DropDownList Enabled="false" ID="ddlNoCITFromMonth" DataTextField="Value" DataValueField="Key"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                            <asp:DropDownList Enabled="false" ID="ddlNoCITFromYear" DataTextField="Value" DataValueField="Key"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="150px">
                                                            <My:Label ID="Label28" runat="server" Text="To" />
                                                        </td>
                                                        <td style="padding-left: 45px">
                                                            <asp:DropDownList Enabled="false" ID="ddlNoCITToMonth" DataTextField="Value" DataValueField="Key"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                            <asp:DropDownList Enabled="false" ID="ddlNoCITToYear" DataTextField="Value" DataValueField="Key"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtCITNoContribution" runat="server" Style="display: none" />
                                                            <asp:CustomValidator ValidateEmptyText="true" ControlToValidate="txtCITNoContribution"
                                                                ID="CustomValidator2" runat="server" ValidationGroup="SaveUpdate" Display="None"
                                                                ErrorMessage="" ClientValidationFunction="validateNoCITDates" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="margin-top: 20px;">
                        </div>
                        <ext:Button runat="server" Width="120px" Height="30px" ID="LinkButton2" Cls="btn btn-primary btn-sm btn-sect"
                            Text="<i></i>Update">
                            <DirectEvents>
                                <Click OnEvent="btnSaveUpdate_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
</asp:Content>
