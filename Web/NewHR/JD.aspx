﻿<%@ Page Title="Job Description" EnableViewState="false" EnableEventValidation="false"
    Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true"
    ValidateRequest="false" CodeBehind="JD.aspx.cs" Inherits="Web.NewHR.JD" %>

    <%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .items
        {
            padding-left: 20px;
        }
        .items span
        {
            
            display: block;
            font-family: 'Open Sans' ,sans-serif;
            padding-bottom: 2px;
        }
        .items img
        {
            float:left;
            width:18px;height:18px;
            margin-right: 10px;
        }
        
        .fieldTable span
        {
            width: 400px;
        }
        
        /*ext grid to nomal grid*/


        .buttonBlock
        {
            display: none;
        }
        #content
        {
            background: white;
        }

        }
        .accordion-edit:hover, .accordion-edit
        {
            font-weight: normal;
        }
        a:hover
        {
            
            font-weight: normal;
        }
        .accordion {
margin-bottom: 5px;

}

    .x-tab-bar-default{background-color:Transparent!important;}

    .editImgPosition
    {
        float:right;
    }
    
      
    .x-btn-default-toolbar-small-over,.x-btn-default-toolbar-small{background-image:none!important;
                                      background-color:transparent!important;border:none;}
 .userDetailsClass
        {
            background-color: #D8E7F3;
            height: 50px;
            text-align: center;
            padding-top: 10px;
            margin-bottom: 10px;
        }
   </style>
    <script type="text/javascript">

        function expandAllCollapseAllHandler(btn) {
            if (btn.getText() == "EXPAND") {
                btn.setText("COLLAPSE");

                var list = Ext.query(".accordion-toggle");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];
                    item.className = "accordion-toggle collapsed";
                }


                list = Ext.query(".accordion-body");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];

                    item.className = "accordion-body in collapse";
                    item.style.height = "";
                }
            }
            else {
                btn.setText("EXPAND");


                var list = Ext.query(".accordion-toggle");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];
                    item.className = "accordion-toggle";
                }

                list = Ext.query(".accordion-body");
                for (i = 0; i < list.length; i++) {
                    var item = list[i];


                    item.className = "accordion-body collapse";
                    item.style.height = "";
                }
            }
        }

        var CommandHandler = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.EffectiveDateEng);
                if(command=="Edit")
                {
                    <%= btnEditJd.ClientID %>.fireEvent('click');
                }
                else
                {
//                    <%= btnDeleteJd.ClientID %>.fireEvent('click');
                }
             }

        function SetEditorValue()
        {
          <%=hdnEditorDescription.ClientID%>.setValue(CKEDITOR.instances.ctl00_ContentPlaceHolder_Main_txtEditorDescription.getData());
        }

       
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden runat="server" ID="hiddenValue" />
    <ext:Hidden runat="server" ID="hdnEditorDescription" />
    <ext:Hidden runat="server" ID="hiddenLevelId" />
    <ext:Hidden runat="server" ID="hiddenPositionId" />
    <ext:Hidden runat="server" ID="hiddenFunctionalTitleId" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEditJd">
        <DirectEvents>
            <Click OnEvent="btnEditJd_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDeleteJd" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDeleteJd_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Hidden ID="Hidden_CustomerID" runat="server">
    </ext:Hidden>
    <div class="contentpanel">
        <div class="innerLR">
            <div class="separator bottom">
            </div>
            <table>
                <tr>
                    <td valign="top">
                        <div style="float: left; margin-right: 10px;">
                            <ucEW:EmpDetailsWizard ID="EmployeeWizard1" runat="server" />
                        </div>
                    </td>
                    <td valign="top" style="padding-left: 10px;">
                        <ext:Label ID="lblMsg" runat="server" />
                        <div style="margin-bottom: 10px">
                            <table>
                                <tr>
                                    <td valign="top" style="width: 3px">
                                        <asp:Image ID="image" CssClass="left" runat="server" ImageUrl="~/images/sample.jpg"
                                            Width="150px" Height="150px" />
                                    </td>
                                    <td valign="top">
                                        <div class="left items">
                                            <h3 runat="server" class="heading" style="color: #214B71; margin: 0; margin-top: 2px;
                                                margin-bottom: 3px;" id="title">
                                            </h3>
                                            <span runat="server" style="font-size: 15px; font-weight: bold" id="positionDesignation">
                                            </span>
                                            <img runat="server" id="groupLevelImg" src="../Styles/images/emp_position.png" style="margin-top: 8px;" />
                                            <span runat="server" style="padding-top: 10px;" id="groupLevel"></span>
                                            <img src="../Styles/images/emp_br_dept.png" />
                                            <span runat="server" id="branch"></span>
                                            <img runat="server" id="imgMobile" src="../Styles/images/emp__phone.png" />
                                            <span runat="server" id="contactMobile"></span>
                                            <img src="../Styles/images/telephone.png" />
                                            <span runat="server" id="contactPhone"></span>
                                            <img src="../Styles/images/emp_email.png" />
                                            <span runat="server" id="email"></span>
                                            <img src="../Styles/images/emp_work.png" />
                                            <span runat="server" id="workingFor" style="width: 500px"></span><span runat="server"
                                                id="spanRetiredDate" style="width: 500px; color: Red"></span>
                                        </div>
                                    </td>
                                    <td style="width: 10px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" colspan="2">
                                        <ext:TabPanel ID="Panel2" runat="server" MinHeight="150" AutoHeight="true" StyleSpec="padding-top:10px"
                                            AutoWidth="true" DefaultBorder="false">
                                            <Items>
                                                <ext:Panel Title="Current Job Description" runat="server">
                                                    <Content>
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <ext:GridPanel ID="gridJobDesc" runat="server" Cls="itemgrid" MinHeight="57" Width="570">
                                                                    <Store>
                                                                        <ext:Store ID="Store3" runat="server">
                                                                            <Model>
                                                                                <ext:Model ID="Model4" runat="server">
                                                                                    <Fields>
                                                                                        <ext:ModelField Name="EffectiveDate" />
                                                                                        <ext:ModelField Name="EffectiveDateEng" />
                                                                                        <ext:ModelField Name="Level" Type="string" />
                                                                                        <ext:ModelField Name="Position" Type="string" />
                                                                                        <ext:ModelField Name="FunctionalTitle" Type="string" />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                    <ColumnModel>
                                                                        <Columns>
                                                                            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Effective Date"
                                                                                Align="Left" Width="150" DataIndex="EffectiveDate" />
                                                                            <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Level"
                                                                                Align="Left" Width="100" DataIndex="Level" />
                                                                            <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Position"
                                                                                Align="Center" DataIndex="Position">
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Functional Title"
                                                                                Align="Center" DataIndex="FunctionalTitle">
                                                                            </ext:Column>
                                                                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="120" Text="Job Description">
                                                                                <Commands>
                                                                                    <ext:GridCommand Icon="None">
                                                                                        <Menu EnableScrolling="false">
                                                                                            <Items>
                                                                                                <ext:MenuCommand Text="Edit" CommandName="Edit" />
                                                                                                <ext:MenuCommand CommandName="Archive" Text="Arctive">
                                                                                                </ext:MenuCommand>
                                                                                            </Items>
                                                                                        </Menu>
                                                                                        <ToolTip Text="Menu" />
                                                                                    </ext:GridCommand>
                                                                                </Commands>
                                                                                <Listeners>
                                                                                    <Command Handler="CommandHandler(command,record);" />
                                                                                </Listeners>
                                                                            </ext:CommandColumn>
                                                                        </Columns>
                                                                    </ColumnModel>
                                                                    <SelectionModel>
                                                                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                                                                    </SelectionModel>
                                                                </ext:GridPanel>
                                                            </div>
                                                        </div>
                                                    </Content>
                                                </ext:Panel>
                                                <ext:Panel ID="Panel1" Title="Job Description History" runat="server">
                                                    <Content>
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <ext:GridPanel ID="gridJobDescHistory" runat="server" Width="570" MinHeight="57">
                                                                    <Store>
                                                                        <ext:Store ID="Store1" runat="server">
                                                                            <Model>
                                                                                <ext:Model ID="Model2" runat="server">
                                                                                    <Fields>
                                                                                        <ext:ModelField Name="EffectiveDate" />
                                                                                        <ext:ModelField Name="EffectiveDateEng" />
                                                                                        <ext:ModelField Name="Level" Type="string" />
                                                                                        <ext:ModelField Name="Position" Type="string" />
                                                                                        <ext:ModelField Name="FunctionalTitle" Type="string" />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                    <ColumnModel>
                                                                        <Columns>
                                                                            <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Effective Date"
                                                                                Align="Left" Width="150" DataIndex="EffectiveDate" />
                                                                            <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Level"
                                                                                Align="Left" Width="100" DataIndex="Level" />
                                                                            <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Position"
                                                                                Align="Center" DataIndex="Position">
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Functional Title"
                                                                                Align="Center" DataIndex="FunctionalTitle">
                                                                            </ext:Column>
                                                                            <ext:CommandColumn ID="CommandColumn2" runat="server" Width="120" Text="Job Description">
                                                                                <Commands>
                                                                                    <ext:GridCommand Icon="None">
                                                                                        <Menu EnableScrolling="false">
                                                                                            <Items>
                                                                                                <ext:MenuCommand Text="Edit" CommandName="Edit" />
                                                                                                <ext:MenuCommand CommandName="Archive" Text="Arctive">
                                                                                                </ext:MenuCommand>
                                                                                            </Items>
                                                                                        </Menu>
                                                                                        <ToolTip Text="Menu" />
                                                                                    </ext:GridCommand>
                                                                                </Commands>
                                                                                <Listeners>
                                                                                    <Command Handler="CommandHandler(command,record);" />
                                                                                </Listeners>
                                                                            </ext:CommandColumn>
                                                                        </Columns>
                                                                    </ColumnModel>
                                                                    <SelectionModel>
                                                                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                                                                    </SelectionModel>
                                                                </ext:GridPanel>
                                                            </div>
                                                        </div>
                                                    </Content>
                                                </ext:Panel>
                                            </Items>
                                        </ext:TabPanel>
                                    </td>
                                    <td style="width: 10px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <ext:Button runat="server" ID="btasdnAdd" Cls="btn" Text="New Job Description">
                                            <DirectEvents>
                                                <Click OnEvent="btnAddJd_Click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </td>
                                    <td style="width: 10px; text-align: right;">
                                        <ext:Button runat="server" ID="Button2" Cls="btn" Text="Save Description">
                                            <DirectEvents>
                                                <Click OnEvent="btnSaveEditor_Click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                            <Listeners>
                                                <Click Handler="SetEditorValue();">
                                                </Click>
                                            </Listeners>
                                        </ext:Button>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" valign="top" style="margin-top: 7px">
                                        <br />
                                        <CKEditor:CKEditorControl ExtraPlugins="autogrow" ID="txtEditorDescription" Width="845px"
                                            Height="200px" runat="server" BasePath="~/ckeditor/">
                                        </CKEditor:CKEditorControl>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <ext:Store ID="storeLevel" runat="server">
        <Model>
            <ext:Model ID="modelLevel" runat="server">
                <Fields>
                    <ext:ModelField Name="LevelId" Type="Int" />
                    <ext:ModelField Name="Name" Type="String" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Window ID="WindowLevel" runat="server" Title="New Job Description" Icon="Application"
        ButtonAlign="Left" Width="550" Height="300" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table class="fieldTable">
                <tr>
                    <td>
         <%--               Level:
                        <ext:Label ID="lblLevel" runat="server" Width="100">
                        </ext:Label>
                        Position :
                        <ext:Label ID="lblDesignation" runat="server" Width="100">
                        </ext:Label>
                        Functional Title :
                        <ext:Label ID="lblFunctionalTitle" runat="server" Width="100">
                        </ext:Label>--%>
                          <div class="userDetailsClass">
                        <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                    </div>
                    </td>
                </tr>
            </table>
            <table class="fieldTable">
                <tr>
                    <td style="width: 35px">
                        <pr:CalendarExtControl Width="180px" FieldLabel="Effective Date*" ID="calEffectiveDate"
                            runat="server" LabelAlign="Top" LabelSeparator="">
                        </pr:CalendarExtControl>
                        <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator5" runat="server"
                            ValidationGroup="SaveUpdateJD" ControlToValidate="calEffectiveDate" ErrorMessage="Effetive Date is required." />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ext:ComboBox ID="cmbTemplate" Width="180px" runat="server" ValueField="TemplateId"
                            LabelSeparator="" LabelAlign="Top" DisplayField="Name" FieldLabel="Select template to start with"
                            ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store41" runat="server">
                                    <Model>
                                        <ext:Model ID="Model1" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="TemplateId" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </td>
                </tr>
            </table>
        </Content>
        <Buttons>
            <ext:Button runat="server" Cls="btn btn-primary" ID="btnLevelSaveUpdate" Text="<i></i>Save">
                <DirectEvents>
                    <Click OnEvent="btnSaveJD_Click">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
                <Listeners>
                    <Click Handler="valGroup = 'SaveUpdateJD'; if(CheckValidation()) return this.disable(); else return false;">
                    </Click>
                </Listeners>
            </ext:Button>
            <ext:LinkButton runat="server" Cls="btnFlatLeftGap" ID="LinkButton4" Text="<i></i>Cancel"
                runat="server">
                <Listeners>
                    <Click Handler="#{WindowLevel}.hide();">
                    </Click>
                </Listeners>
            </ext:LinkButton>
        </Buttons>
    </ext:Window>
</asp:Content>
