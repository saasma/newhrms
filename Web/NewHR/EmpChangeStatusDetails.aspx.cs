﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class EmpChangeStatusDetails : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachPopUpCode(Page, "promote", "../CP/ChangeEmployeeStatus.aspx", 480, 400);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "importPopup", "../ExcelWindow/EmpChangeStatusImportExcel.aspx", 450, 500);
        }

        private void Initialise()
        {
            cmbSubDepartment.Store[0].DataSource = new DepartmentManager().GetSubDepartmentsByCompany(SessionManager.CurrentCompanyId);
            cmbSubDepartment.Store[0].DataBind();

            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments();
            cmbDepartment.Store[0].DataBind();

            storeLevel.DataSource = ActivityManager.GetLevels();
            storeLevel.DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.Name).ToList();
            cmbDesignation.Store[0].DataBind();

            List<KeyValue> list = new JobStatus().GetMembers();
            list.RemoveAt(0);
            cmbJobStatus.Store[0].DataSource = list;
            cmbJobStatus.Store[0].DataBind();

            if (CommonManager.IsUnitEnabled)
            {
                cmbUnit.Show();
                cmbUnit.Store[0].DataSource = CommonManager.GetAllUnitList();
                cmbUnit.Store[0].DataBind();
            }
       

            if (SessionManager.IsReadOnlyUser)
            {
                CommandColumnChange.Hide();
            }
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int employeeId = -1, pageSize = 50;
            if (cmbEmployee.SelectedItem != null && cmbEmployee.SelectedItem.Value != null)
                employeeId = int.Parse(cmbEmployee.SelectedItem.Value);

            int branchId = -1, departmentId = -1, designationId = -1, levelId = -1, subDepartmentId = -1, unitId = -1;
            string statusList = "";

            if (cmbPageSize.SelectedItem != null && cmbPageSize.SelectedItem.Value != null)
                pageSize = int.Parse(cmbPageSize.SelectedItem.Value);

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);

            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                departmentId = int.Parse(cmbDepartment.SelectedItem.Value);

            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);

            foreach (Ext.Net.ListItem item in cmbJobStatus.SelectedItems)
                statusList += item.Value.ToString() + ",";

            if (statusList != "")
                statusList = statusList.TrimEnd(',');

            if (cmbSubDepartment.SelectedItem != null && cmbSubDepartment.SelectedItem.Value != null)
                subDepartmentId = int.Parse(cmbSubDepartment.SelectedItem.Value);

            if (cmbUnit.SelectedItem != null && cmbUnit.SelectedItem.Value != null)
                unitId = int.Parse(cmbUnit.SelectedItem.Value);

            int totalRows = 0;

            List<GetEmployeeChangeStatusResult> list = NewHRManager.GetEmployeeChangeStatusList(employeeId, branchId, departmentId, designationId, levelId,
                statusList, subDepartmentId, unitId, e.Start / pageSize, pageSize);

            storeChangeStatus.DataSource = list;
            storeChangeStatus.DataBind();

            if (list.Count > 0)
                totalRows = list[0].TotalRows.Value;

            e.Total = totalRows;          

        }

    }

    public class EmpStatusCls
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string FromStatus { get; set; }
        public string From_FromDate { get; set; }
        public string From_ToDate { get; set; }
        public string From_Note { get; set; }
        public string ToStatus { get; set; }
        public string To_FromDate { get; set; }
        public string  To_ToDate{ get; set; }
        public string To_Note { get; set; }
    }
}