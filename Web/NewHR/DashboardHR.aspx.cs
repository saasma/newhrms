﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
//using System.Web.UI.DataVisualization.Charting;
using BLL.Base;
using System.Globalization;
using Utils.Calendar;
using BLL.BO;
using Ext.Net;

namespace Web.CP
{
    public partial class DashboardHR : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }           
        }

        CommonManager commonMgr = new CommonManager();
        //private double totalSalarySummary = 0;


        protected void Page_PreRender(object sender, EventArgs e)
        {

            if (IsAccessible("newhr/PromotionListReport.aspx"))
                levelGradePromotionHistory.Visible = CommonManager.CompanySetting.HasLevelGradeSalary;


            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
            {
                EmployeeAwardList.Visible = false;
            }


            linkEmpSupervisor.Visible =
                CommonManager.CompanySetting.WhichCompany == WhichCompany.Care ||
                CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI;

            leaveFareMenu.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.Prabhu ||
                                    CommonManager.CompanySetting.WhichCompany == WhichCompany.Janata ||
                                    CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil ||
                                    CommonManager.CompanySetting.WhichCompany == WhichCompany.MBL;

            if (CommonManager.IsServiceHistoryEnabled == false)
                linkServiceHistory.Visible = false;

            OvertimeRounding otsetting = OvertimeManager.getTopRounding();
            if (otsetting != null)
            {
                if (otsetting.OTAllowanceRequestType != null)
                {

                    if (otsetting.OTAllowanceRequestType == (int)OvertimeAllowanceType.NIBLLikeAutoGeneration)
                    {
                        linkOTUnitReport.Visible = false;
                        linkOTRequests.Visible = false;
                        linkOTCalculationReport.Visible = false;
                        linkAllowanceRequests.Visible = false;
                    }
                    if (otsetting.OTAllowanceRequestType == (int)OvertimeAllowanceType.RequestType)
                    {
                        liGenerateOvertime.Visible = false;
                    }
                }
            }


            // hide TA request for civill and mbl
            ulTravelRequest.Visible = !(CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil
                || CommonManager.CompanySetting.WhichCompany == WhichCompany.MBL);

            // show training for civil only for now
            // show for all for demo
            tdTraining.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil ||
                     CommonManager.CompanySetting.WhichCompany == WhichCompany.OtherTesting ||
                      CommonManager.CompanySetting.WhichCompany == WhichCompany.Default;

            ulDailyAcitivities.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.Rigo;

            linkTADAList.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome;
            menuHolidayPay.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.ArghakhachiCement;
            //OOvertimeSetting otsetting = OvertimeManager.geto
            // Hide Travel request/TADA module
            //if (CommonManager.CompanySetting.WhichCompany != WhichCompany.Sangrila &&
            //    CommonManager.CompanySetting.WhichCompany != WhichCompany.Prabhu &&
            //    CommonManager.CompanySetting.WhichCompany != WhichCompany.DishHome)
            //    HideTravelRequestModule();

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
                this.DataBind();
                ShowHideGradeTransfer();
                LoadHRCounts();
                LoadProfileAndPhotoChangesCount();
                CheckPwdChangedBeforePwdChangingDays();
                ShowPasswordChangedMessage();

                LoadHPLDutySchedule();
            }
            
            
        }

        void SetRoles()
        {

            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();
            if (period != null)
            {
                currentPeriod.InnerHtml = period.Name.Replace("/", " ");
                if (this.IsEnglish == false)
                {
                    currentPeriod.InnerHtml += " (" +
                        period.StartDateEng.Value.ToString("dd MMM yyyy") + " - " +
                         period.EndDateEng.Value.ToString("dd MMM yyyy") + ")";
                }
            }

          
        }

        public bool HideOtherBlockAsDueToLargeEmployee()
        {
            return CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL;
        }

        void Initialise()
        {
            SetRoles();


        

            //if (CommonManager.CompanySetting.WhichCompany != WhichCompany.RBB)
            //{
                //rbbRegularTask.Visible = false;
                //anchorDeputationList.Visible = false;
            if (CommonManager.CompanySetting.HasLevelGradeSalary == false)
            {
                levelSalaryChange.Style["display"] = "none";
                levelPromotionList.Style["display"] = "none";
                //acting.Style["display"] = "none";
            }
            //}
            //else
            //    otherRegularTasks.Visible = false;


            //if (SessionManager.IsCustomRole)
            //    employeeListSearch.Visible = false;

            // First parepare this month & next month dates
            CustomDate today = CustomDate.GetTodayDate(IsEnglish);
            int currentYear = today.Year;
            DateTime FromThisDate,FromNextDate;
            DateTime ToThisDate,ToNextDate;
            DateTime ToNextNextDate;
            CustomDate firstDateOfThisMonth = new CustomDate(1, today.Month, currentYear, IsEnglish);
            CustomDate lastDateOfThisMonth = firstDateOfThisMonth.GetLastDateOfThisMonth();

            FromThisDate = DateManager.GetStartDate(firstDateOfThisMonth.EnglishDate);
            ToThisDate = DateManager.GetEndDate(lastDateOfThisMonth.EnglishDate);

            CustomDate firstDateOfNextMonth = lastDateOfThisMonth.IncrementByOneDay();
            CustomDate lastDateOfNextMonth = firstDateOfNextMonth.GetLastDateOfThisMonth();

            FromNextDate = DateManager.GetStartDate(firstDateOfNextMonth.EnglishDate);
            ToNextDate = DateManager.GetEndDate(lastDateOfNextMonth.EnglishDate);

            CustomDate firstDateOfNextNextMonth = lastDateOfNextMonth.IncrementByOneDay();
            CustomDate lastDateOfNextNextMonth = firstDateOfNextNextMonth.GetLastDateOfThisMonth();

            ToNextNextDate = lastDateOfNextNextMonth.EnglishDate;

            if (HideOtherBlockAsDueToLargeEmployee())
            {
                divRetiring.Visible = false;
                divNewJoin.Visible = false;
                divOvertime.Visible = false;
                divEveningCounter.Visible = false;
                divBirthday.Visible = false;
            }

            else
            {
                // Retriring
                //rptRetiringThisMonth.DataSource = DashboardManager.GetEmployeeRetiring(FromThisDate, ToThisDate);
                //rptRetiringThisMonth.DataBind();
                countRetiringThisMonth.InnerHtml = DashboardManager.GetEmployeeRetiring(FromThisDate, ToThisDate).Count.ToString();
                if (countRetiringThisMonth.InnerHtml == "0")
                    countRetiringThisMonth.Visible = false;

                //rptRetiringNextMonth.DataSource = DashboardManager.GetEmployeeRetiring(FromNextDate, ToNextDate);
                //rptRetiringNextMonth.DataBind();
                countRetiringNextMonth.InnerHtml = DashboardManager.GetEmployeeRetiring(FromNextDate, ToNextDate).Count.ToString();
                if (countRetiringNextMonth.InnerHtml == "0")
                    countRetiringNextMonth.Visible = false;

                // New Joines in this month
                //rptNewJoines.DataSource = DashboardManager.GetEmployeeJoiningThisMonth();
                //rptNewJoines.DataBind();
                countNewJoineesThisMonth.InnerHtml = DashboardManager.GetEmployeeJoiningThisMonth().Count.ToString();
                if (countNewJoineesThisMonth.InnerHtml == "0")
                    countNewJoineesThisMonth.Visible = false;


                // // 6 Month completion list
                // count6MonthThisMonth.InnerHtml = DashboardManager.GetPeriodCompletionList(6, FromThisDate, ToThisDate).Count.ToString();
                // if (count6MonthThisMonth.InnerHtml == "0")
                //     count6MonthThisMonth.Visible = false;

                //count6MonthNextMonth.InnerHtml = DashboardManager.GetPeriodCompletionList(6, FromNextDate, ToNextDate).Count.ToString();
                // if (count6MonthNextMonth.InnerHtml == "0")
                //     count6MonthNextMonth.Visible = false;
                // // 1 year completion list
                // count1YearThisMonth.InnerHtml = DashboardManager.GetPeriodCompletionList(12, FromThisDate, ToThisDate).Count.ToString();
                // if (count1YearThisMonth.InnerHtml == "0")
                //     count1YearThisMonth.Visible = false;
                //count1YearNextMonth.InnerHtml = DashboardManager.GetPeriodCompletionList(12, FromNextDate, ToNextDate).Count.ToString();
                // if (count1YearNextMonth.InnerHtml == "0")
                //     count1YearNextMonth.Visible = false;

                LoadOvertime();
                LoadEveningCounter();

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI || CommonManager.CompanySetting.WhichCompany==WhichCompany.Citizen)
                    BirthDayReminder();
                else
                    divBirthday.Visible = false;



                List<GetContractExpiringEmpListResult> list1 = null, list2 = null, list3 = null;
                // Contract Expiring
                DashboardManager.GetContractExpiringListForDashboard(FromThisDate, ToThisDate
                    , ToNextDate, ToNextNextDate, ref list1, ref list2, ref list3);

                // This Month
                if (list1.Count <= 0)
                    contractCurrentMonth.Visible = false;
                else
                {
                    contractCurrentMonth.InnerText += " " + DateHelper.GetMonthName(firstDateOfThisMonth.Month, IsEnglish);
                    if (IsEnglish == false)
                    {
                        contractCurrentMonth.InnerText += " (" + DateHelper.GetEngDateRangeForNepaliDate(today) + ")";
                    }
                    contractThisMonthCount.InnerHtml = list1.Count.ToString();
                    rptContractExpiringThisMonth.DataSource = list1;
                    rptContractExpiringThisMonth.DataBind();
                }

                // Next Month
                countContractExpireNextMonth.InnerHtml = list2.Count.ToString();
                if (list2.Count <= 0)
                    countContractExpireNextMonth.Visible = false;
                else
                {
                    contractNextMonth.InnerText += " " + DateHelper.GetMonthName(firstDateOfNextMonth.Month, IsEnglish);
                    if (IsEnglish == false)
                    {
                        contractNextMonth.InnerText += " (" + DateHelper.GetEngDateRangeForNepaliDate(firstDateOfNextMonth) + ")";
                    }
                    rptContractExpiringNextMonth.DataSource = list2;
                    rptContractExpiringNextMonth.DataBind();
                }


                // Next Next Month
                countContractExpireNextNextMonth.InnerHtml = list3.Count.ToString();
                if (list3.Count <= 0)
                {
                    countContractExpireNextNextMonth.Visible = false;
                    contractNextNextMonth.Visible = false;
                }
                else
                {
                    contractNextNextMonth.InnerText += " " + DateHelper.GetMonthName(firstDateOfNextNextMonth.Month, IsEnglish);
                    if (IsEnglish == false)
                    {
                        contractNextNextMonth.InnerText += " (" + DateHelper.GetEngDateRangeForNepaliDate(firstDateOfNextNextMonth) + ")";
                    }
                    rptContractExpiringNextNextMonth.DataSource = list3;
                    rptContractExpiringNextNextMonth.DataBind();
                }

            }
        }


        void SetContracExpiring()
        {

        }

        public void HideTravelRequestModule()
        {
            ulTravelRequest.Visible = false;
            anchorTravelAllowance.Visible = false;
            anchorTravelLocation.Visible = false;
        }

        protected void BirthDayReminder()
        {

            DateTime Today = BLL.BaseBiz.GetCurrentDateAndTime();
            int ThisMonth = Today.Month;
            int NextMonth = Today.AddMonths(1).Month;
            int NextNextMonth = Today.AddMonths(2).Month;

            List<BirthDayBo> _listThisMonth = DashboardManager.GetUpcommingBirthDay(ThisMonth,false);
            // This Month
            if (_listThisMonth.Count <= 0)
            {
                spnBrithDayThisMonthName.Visible = false;
                spnBrithDayThisMonthCount.Visible = false;
            }
            else
            {
                spnBrithDayThisMonthName.InnerText += " " + DateHelper.GetMonthName(ThisMonth, true);
                if (IsEnglish == false)
                {
                    //spnBrithDayThisMonthName.InnerText += " (" + DateHelper.GetEngDateRangeForNepaliDate(today) + ")";
                }
                spnBrithDayThisMonthCount.InnerHtml = _listThisMonth.Count.ToString();
                RptBirthDayThisMonth.DataSource = _listThisMonth;
                RptBirthDayThisMonth.DataBind();
            }

            List<BirthDayBo> _listNextMonth = DashboardManager.GetUpcommingBirthDay(NextMonth,false);
            // Next Month
            if (_listNextMonth.Count <= 0)
            {
                spnBrithDayNextMonthName.Visible = false;
                spnBrithDayNextMonthCount.Visible = false;
            }
            else
            {

                spnBrithDayNextMonthName.InnerText += " " + DateHelper.GetMonthName(NextMonth, true);
                if (IsEnglish == false)
                {
                   // spnBrithDayNextMonthName.InnerText += " (" + DateHelper.GetEngDateRangeForNepaliDate(firstDateOfNextNextMonth) + ")";
                }
                spnBrithDayNextMonthCount.InnerHtml = _listNextMonth.Count.ToString();
                RptBrithDayNextMonth.DataSource = _listNextMonth;
                RptBrithDayNextMonth.DataBind();
            }

            // Next Next Month
            List<BirthDayBo> _listNextNextMonth = DashboardManager.GetUpcommingBirthDay(NextNextMonth,false);

            if (_listNextNextMonth.Count <= 0)
            {
                spnBrithDayNextNextMonthName.Visible = false;
                spnBrithDayNextNextMonthCount.Visible = false;
            }
            else
            {
                spnBrithDayNextNextMonthName.InnerText += " " + DateHelper.GetMonthName(NextNextMonth, true);
                if (IsEnglish == false)
                {
                   // spnBrithDayNextNextMonthName.InnerText += " (" + DateHelper.GetEngDateRangeForNepaliDate(firstDateOfNextMonth) + ")";
                }
                spnBrithDayNextNextMonthCount.InnerHtml = _listNextNextMonth.Count.ToString();
                RptBirthDayNextNetMonth.DataSource = _listNextNextMonth;
                RptBirthDayNextNetMonth.DataBind();
            }


        }
        public void LoadOvertime()
        {
            int? pendingForRecommend = 0, recommendForApproval = 0,approvedForForward = 0;

            OvertimeManager.SetOvertimeCount(ref pendingForRecommend, ref recommendForApproval, ref approvedForForward);

            if (pendingForRecommend > 0 || recommendForApproval > 0 || approvedForForward > 0)
            {
                divOvertime.Visible = true;


                spanOTPending.InnerHtml = pendingForRecommend.ToString();
                spanOTRecommended.InnerHtml = recommendForApproval.ToString();
                spanOTApproved.InnerHtml = approvedForForward.ToString();
            }


            OvertimeManager.SetTADACount(ref pendingForRecommend, ref recommendForApproval, ref approvedForForward);

            if (pendingForRecommend > 0 || recommendForApproval > 0 || approvedForForward > 0)
            {
                divTADA.Visible = true;


                spanTADAPending.InnerHtml = pendingForRecommend.ToString();
                spanTADARecommended.InnerHtml = recommendForApproval.ToString();
                spanTADAApproved.InnerHtml = approvedForForward.ToString();
            }

            int approvedForAdvance = 0, empSettled = 0;
            // Travel requests
            OvertimeManager.SetTravelRequestsCount(ref approvedForAdvance, ref empSettled);

            if (approvedForAdvance > 0 || empSettled > 0)
            {
                divTravelRequests.Visible = true;


                spanTRApproved.InnerHtml = approvedForAdvance.ToString();
                spanTREmpSettled.InnerHtml = empSettled.ToString();
            }

        }

        public void LoadEveningCounter()
        {
            int? pendingForRecommend = 0, recommendForApproval = 0, approvedForForward = 0;

            OvertimeManager.SetEveningCounterCount(ref pendingForRecommend, ref recommendForApproval, ref approvedForForward);

            if (pendingForRecommend > 0 || recommendForApproval > 0 || approvedForForward > 0)
            {
                divEveningCounter.Visible = true;


                spanEveningPending.InnerHtml = pendingForRecommend.ToString();
                spanEveningRecommended.InnerHtml = recommendForApproval.ToString();
                spanEveningApproved.InnerHtml = approvedForForward.ToString();
            }


         
        }

        private bool moduleAccessible = false;
        public new bool IsAccessible(string url)
        {
            if (moduleAccessible)
                return true;

            if (SessionManager.User.URole.URoleModules.Any(x => x.ModuleRef_ID == (int)ModuleEnum.HumanResource))
              
            {
                moduleAccessible = true;
                return true;
            }

            return UserManager.IsPageAccessible(url);

        }

        private void ShowHideGradeTransfer()
        {
            if (CommonManager.Setting != null)
            {
                if (CommonManager.Setting.ShowGradeStep != null && CommonManager.Setting.ShowGradeStep == true)
                    liNonLevelGrade.Visible = true;
                else
                    liNonLevelGrade.Visible = false;
            }
        }

        private void LoadHRCounts()
        {
            int? br = 0, dep = 0, loc = 0, totalEmp = 0, maleEmp = 0, femEmp = 0, newJoin = 0, retired = 0;
            NewHRManager.GetHRInfoCounts(ref br, ref dep, ref loc, ref totalEmp, ref maleEmp, ref femEmp, ref newJoin, ref retired);

            spanBranches.InnerHtml = br.ToString();
            spanDepartments.InnerHtml = dep.ToString();
            spanLocations.InnerHtml = loc.ToString();
            spanTotalEmployees.InnerHtml = totalEmp.ToString();
            spanMaleEmployees.InnerHtml = maleEmp.ToString();
            spanFemaleEmployees.InnerHtml = femEmp.ToString();
            spanNewJoin.InnerHtml = newJoin.ToString();
            spanRetired.InnerHtml = retired.ToString();
            spTotalEmployees.InnerHtml = totalEmp.ToString();

            spanBr.InnerHtml = br.ToString();
            spanDept.InnerHtml = dep.ToString();
            spanEmp.InnerHtml = totalEmp.ToString();
            spanEMale.InnerHtml = maleEmp.ToString();
            spanEFemale.InnerHtml = femEmp.ToString();
            spanENewJoin.InnerHtml = newJoin.ToString();
            spanERetire.InnerHtml = retired.ToString();
        }

        private void CheckPwdChangedBeforePwdChangingDays()
        {
            int days = 0;
            if (!IsPwdChangedBeforePwdChangingDays(SessionManager.UserName, ref days))
            {
                Setting objSetting = UserManager.GetSettingForPasswordChangeRule();
                if (objSetting.PasswordChangeType == (int)PasswordChangeTypeEnum.RecommendPwdChange)
                {
                    WPasswordNotification.Show();
                    return;
                }
                   
                //string res = "Password is not changed from the last " + days.ToString() + " days!";
                //ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('" + res + "');", true);
            }
        }

        private void LoadProfileAndPhotoChangesCount()
        {
            List<SelectAll_EmployeeProfileChangeResult> listProfileChanges = NewHRManager.GetAllEmpProfileChangeList();
            spanProfileChanges.InnerHtml = listProfileChanges.Count.ToString();

            List<HHumanResource> listPhotoChanges = NewHRManager.GetHHumanResourceListForPhotoApproval();
            spanPhotoChanges.InnerHtml = listPhotoChanges.Count.ToString();
        }

        private void ShowPasswordChangedMessage()
        {
            if (Session["PwdChanged"] != null && Session["PwdChanged"].ToString() == "1")
            {
                Session["PwdChanged"] = "0";
                NewMessage.ShowNormalPopup("Your Password has been changed.");
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "javascript:ShowMsg();", true);
            }
        }

        private void LoadHPLDutySchedule()
        {
            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL)
            {
                divHPLSchedule.Visible = true;

                ulDutySchedule.Visible = true;

                int columnWidth = 55;

                Model SiteModel = (this.gridSchedule.Store[0].Model[0] as Model);
                string columnId = "Item_{0}";

                int weekNumber = AttendanceManager.GetWeekNumber(DateTime.Today);

                List<HPLDutyPeriod> listPeriods = listPeriods = NewHRManager.GetHPLDutyPeriodsForDashboard();

                int listcount = listPeriods.Count;

                int k = 0;

                foreach (var item in listPeriods)
                {
                    string gridIndexIdMonth = string.Format(columnId, ++k);

                    ModelField field = new ModelField(gridIndexIdMonth, ModelFieldType.String);
                    field.UseNull = true;
                    SiteModel.Fields.Add(field);

                    Column column = new Column();
                    column.Text = item.MonthName;
                    column.Sortable = false;
                    column.Draggable = false;
                    column.MenuDisabled = true;

                    Column column1 = new Column();
                    column1.Text = item.PeriodName;
                    column1.Sortable = false;
                    column1.Draggable = false;
                    column1.MenuDisabled = true;

                    column.Columns.Add(column1);

                    Column dataColumn = new Column { DataIndex = gridIndexIdMonth, Align = Alignment.Center, Text = "WK " + item.WeekNo.ToString().PadLeft(2, '0'), Width = columnWidth, MenuDisabled = true, Sortable = false, Draggable = false };
                    dataColumn.Renderer.Fn = "myRenderer1";
                    column1.Columns.Add(dataColumn);

                    gridSchedule.ColumnModel.Columns.Add(column);

                }

                List<int> employeeIds = NewHRManager.GetScheduleEmployeesByYearForDashboard(DateTime.Today.Year);

                object[] data = new object[employeeIds.Count];

                int sn = 0, l = 2;
                int row = 0;
                foreach (int employeeId in employeeIds)
                {
                    sn++;
                    object[] rowData = new object[listcount + 3];
                    rowData[0] = sn.ToString();
                    rowData[1] = employeeId.ToString();
                    rowData[2] = EmployeeManager.GetEmployeeById(employeeId).Name;
                    l = 2;
                    foreach (var item in listPeriods)
                    {
                        HPLDutySchedule obj = NewHRManager.GetEmployeeDutyScheduleForPeriod(employeeId, item.PeriodId);
                        if (obj != null)
                        {
                            if (obj.Value != null && obj.Value.Value)
                                rowData[++l] = "On";
                            else
                                rowData[++l] = "Off";
                        }
                        else
                            rowData[++l] = null;
                    }

                    //Add a Row
                    data[row++] = rowData;

                }

                gridSchedule.Store[0].DataSource = data;
                gridSchedule.Store[0].DataBind();
            }
            else
                gridSchedule.Visible = false;

        }
        



    }
}
