﻿<%@ Page Title="Employee Reward" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeGradeReward.aspx.cs" Inherits="Web.NewHR.EmployeeGradeReward" %>

<%@ Register Src="~/newhr/UserControls/GradeRewardCtl.ascx" TagName="GradeRewardCtl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <uc2:GradeRewardCtl runat="server" id="ctl" IsEmployee="true" />
</asp:Content>
