﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;

namespace Web.NewHR
{
    public partial class PayGroupManager : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.AdministratorTools;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            LoadGroups();
        }
        private void LoadGroups()
        {
            List<PayGroup> list = PeriodicPayManager.GetPayGroup();

            foreach (var itme in list)
            {
                PIncome income = PayManager.GetIncome(itme.IncomeId.Value);
                if (income != null)
                    itme.Income = income.Title;
            }
            GridLevels.Store[0].DataSource = list;
            GridLevels.Store[0].DataBind();

            cmbUnitIncomes.Store[0].DataSource = PayManager.GetUnitRateIncomes();
            cmbUnitIncomes.Store[0].DataBind();
        }

        public void ClearGroupFields()
        {
            hiddenValue.Text = string.Empty;
            txtLevelName.Text = string.Empty;
            txtDesc.Text = "";
            txtTDSRate.Text = "";
            cmbUnitIncomes.ClearValue();
        }

        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            ClearGroupFields();
            WindowLevel.Show();
        }


        protected void btnDeleteLevel_Click(object sender, DirectEventArgs e)
        {
            var levelId = int.Parse(hiddenValue.Text.Trim());
            var status = PeriodicPayManager.DeleteGroup(levelId);
            if (status.IsSuccess)
            {
                LoadGroups();
                NewMessage.ShowNormalMessage("Group deleted.");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }


        protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        {
            var levelId = int.Parse(hiddenValue.Text.Trim());
            var entity = PeriodicPayManager.GetPayGroup(levelId);
            WindowLevel.Show();

            txtLevelName.Text = entity.Name;
            txtDesc.Text = entity.Description;
            txtTDSRate.Text = entity.TDSRate.ToString();
            cmbUnitIncomes.SetValue(entity.IncomeId.ToString());
        }

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("InsertUpdateLevel");
            if (Page.IsValid)
            {
                var entity = new PayGroup();
                var isInsert = true;
                if (!string.IsNullOrEmpty(hiddenValue.Text))
                {
                    isInsert = false;
                    entity.PayGroupID = int.Parse(hiddenValue.Text.Trim());
                }
                entity.Name = txtLevelName.Text.Trim();
                entity.Description = txtDesc.Text.Trim();

                double tdsRate = 0;

                double.TryParse(txtTDSRate.Text.Trim(), out tdsRate);

                entity.TDSRate = tdsRate;
                entity.IncomeId = int.Parse(cmbUnitIncomes.SelectedItem.Value);



                var status = PeriodicPayManager.InsertUpdateGroup(entity, isInsert);

                if (status.IsSuccess)
                {
                    WindowLevel.Hide();
                    LoadGroups();
                    NewMessage.ShowNormalMessage("Group saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }
    }
}
