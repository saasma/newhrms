﻿<%@ Page Title="Health" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="Health.aspx.cs" Inherits="Web.NewHR.Health" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/HealthCtl.ascx" TagName="HealthCtl" TagPrefix="ucHealthCtl" %>
<%@ Register Src="~/NewHR/UserControls/EmpDetailsLeftMenu.ascx" TagName="EmpDetailsWizard"
    TagPrefix="ucEW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
     .userDetailsClass
        {
            background-color:#D8E7F3; 
            height:50px; text-align:center; 
            padding-top:10px; 
            margin-bottom:10px;
            margin-left:20px;
        }
</style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

     <div class="pageheader">
        <div class="media">
            <div class="media-body" style="margin-left:150px;">
                <h4>
                    Health
                </h4>
            </div>
        </div>
    </div>

    <div class="contentpanel">
        <div class="innerLR">
            <div class="separator bottom">
            </div>
        
            <%--<uc2:EmployeeWizard Id="EmployeeWizard1" runat="server" />--%>
            <table>
                <tr>
                    <td valign="top">
                        <div style="float: left; margin-right: 10px;">
                            <ucEW:EmpDetailsWizard Id="EmployeeWizard1" runat="server" />
                        </div>
                    </td>
                    <td valign="top" style="width:100%;">
                        
                        <div class="userDetailsClass">
                                <uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />
                              </div>                        

                        <div>
                            
                            <div class="panel-body" style="margin-top:-25px;">
                                <ucHealthCtl:HealthCtl Id="ucHealthCtl1" runat="server" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
