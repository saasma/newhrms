﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;

namespace Web.NewHR
{
    public partial class EmployeeListing : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPagePermission();

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                Initialise();

                ProcessSectionLevelPermission();
            }
        }

        public void Initialise()
        {

            // Disable all emp export/import once salary is saved
            if (BLL.BaseBiz.PayrollDataContext.CCalculations.Any(
                x => x.IsFinalSaved == true))
            {
                btnExportPopup.Visible = false;
            }

            GridLevels.ColumnModel.Columns[6].Text = CommonManager.GetLevelName;

            cmbRet.Select(0);

           List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
           cmbBranch.Store[0].DataSource = branchList;
           cmbBranch.Store[0].DataBind();

           cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
           cmbDepartment.Store[0].DataBind();

           cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.Name).ToList();
           cmbDesignation.Store[0].DataBind();

           this.storeLevel.DataSource = ActivityManager.GetLevels();
           this.storeLevel.DataBind();

            JobStatus statues = new JobStatus();
            //storeEmpStatus.DataSource = statues.GetMembersForHirarchyView();

            storeEmployeeList.DataSource = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name).ToList();
            storeEmployeeList.DataBind();

            cmbBranch.SelectedItem.Index = 0;
            cmbDepartment.SelectedItem.Index = 0;
            cmbDesignation.SelectedItem.Index = 0;
           // cmbEmpStatus.SelectedItem.Index = 0;
            cmbLevel.SelectedItem.Index = 0;

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "employeePopup", "../ExcelWindow/EmployeeExcel.aspx", 450, 500);
            JavascriptHelper.AttachNonDialogPopUpCode(Page, "otherPopup", "../ExcelWindow/OtherEmployeeImport.aspx", 450, 500);
        }

        /// <summary>
        /// Set for Custom Role if is in Readonly Mode or Not
        /// </summary>
        public void SetPagePermission()
        {
            if (SessionManager.IsCustomRole && SessionManager.CustomRole.IsReadOnly != null && SessionManager.CustomRole.IsReadOnly.Value)
            {
                //btnAddNew.Visible = false;
                btnExportPopup.Visible = false;
                btnExportImportAccounts.Visible = false;
            }
        }

        public void ProcessSectionLevelPermission()
        {
            if (SessionManager.IsCustomRole)
            {
                UUser user = UserManager.GetUserByUserName(Page.User.Identity.Name);
                int currentPageId = UserManager.GetPageIdByPageUrl(CurrentPageUrl);

                btnExportPopup.Visible = false;

                List<UPermissionSection> sections = UserManager.GetAccessibleSectionsForPage(user.RoleId.Value, currentPageId);

                foreach (UPermissionSection section in sections)
                {
                    if (section.PageSectionId == 2)
                    {
                        btnExportPopup.Visible = true;
                    }
                }

            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;

            string deparmentList = null;
            if (SessionManager.IsCustomRole)
                deparmentList = SessionManager.CustomRoleDeparmentIDList;
                
            bool? showRetiredOnly = null;
            if (cmbRet.SelectedItem.Text != "All")
                showRetiredOnly = bool.Parse(cmbRet.SelectedItem.Value);

            EmployeeManager empMgr = new EmployeeManager();
            List<EEmployee> list = empMgr.GetAllEmployees(1, 9999, ref totalRecords, "",
                SessionManager.CurrentCompanyId, cmbEmployee.SelectedItem.Text == null ? "" : cmbEmployee.SelectedItem.Text, 
                txtINO.Text.Trim(), showRetiredOnly,
                 int.Parse(cmbBranch.SelectedItem.Value), int.Parse(cmbDepartment.SelectedItem.Value), 
                 int.Parse(cmbDesignation.SelectedItem.Value), int.Parse(cmbLevel.SelectedItem.Value), deparmentList
                 ,"",-1,-1,-1,"",-1);

            ExcelGenerator.WriteExcel(ExcelGenerator.GetExcelForEmployeeList(list));

        }


        private void LoadLevels()
        {
            EmployeeManager empMgr = new EmployeeManager();
            int totalRecords = 0;

            string customRoleDeparmentList = null;
            if (SessionManager.IsCustomRole)
                customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;

            


            //List<EEmployee> firstBindList = new List<EEmployee>();
            //firstBindList = empMgr.GetAllEmployees(pagintCtl.CurrentPage, int.Parse(pagintCtl.DDLRecords.SelectedValue), ref totalRecords,
            //     ("EmployeeId" + " " + System.Web.UI.WebControls.SortDirection.Ascending).ToLower(), SessionManager.CurrentCompanyId, filterCtl.EmployeeNameValue.Trim(),
            //     filterCtl.EmployeeIDCardNoValue.Trim(), chkIsRetiredOrResignedAlso.Checked,
            //    filterCtl.BranchId, filterCtl.DepartmentId, filterCtl.DesignationId, filterCtl.LevelId, customRoleDeparmentList,
            //    filterCtl.StatusID, filterCtl.StatusName.ToString().ToLower().Contains("only"));


            PagingToolbar1.DoRefresh();
        }
       
        

        public void ClearLevelFields()
        {
            fup.Disabled = false;
            hiddenValue.Text = "";
            txtDesc.Text = "";
        }

        protected void ddlRecords_SelectedIndexChanged()
        {
            LoadLevels();
        }
      
        
        protected void btnAddLevel_Click(object sender, DirectEventArgs e)
        {
            ClearLevelFields();
            WindowLevel.Show();
        }


        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int empId = int.Parse(hiddenValue.Text.Trim());
            EmployeeManager empMgr = new EmployeeManager();

            if (empMgr.Delete(empId))
            {
                X.Js.AddScript("searchList();");
                NewMessage.ShowNormalMessage(Resources.Messages.EmployeeDeletedMsg);

            }
            else
            {
                NewMessage.ShowWarningMessage("Employee could not be deleted.");

            }


        }

        

        //protected void btnEditLevel_Click(object sender, DirectEventArgs e)
        //{
        //    //int levelId = int.Parse(hiddenValue.Text.Trim());
        //    //HDocument entity = NewPayrollManager.GetDocumentById(levelId);
        //    //WindowLevel.Show();

        //    ////cmbGroup.SetValue(entity.LevelGroupId.ToString());

        //    //txtDesc.Text = entity.Description;
        //    //fup.Disabled = true;

        //    EmpImage.ImageUrl = Hidden_SelectedEmpPic.Text;

        //}

        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {
            if (fup.HasFile)
            {
                string ext = Path.GetExtension(fup.FileName);
                string guidFileName = Guid.NewGuid().ToString() + ext;
                string saveLocation = Path.Combine(Server.MapPath(Config.UploadLocation), guidFileName);


                UploadType type = (UploadType)this.CustomId;
                string code = "closeWindow(\"{0}\");";
                HRManager mgr = new HRManager();
                
                    try
                    {
                        guidFileName = guidFileName.Replace(Path.GetExtension(guidFileName), ".ashx");
                        saveLocation = saveLocation.Replace(Path.GetExtension(saveLocation), ".ashx");

                        EncryptorDecryptor enc = new EncryptorDecryptor();
                        enc.Encrypt(fup.FileContent, saveLocation);

                        HDocument doc = new HDocument();
                        doc.EmployeeId = int.Parse(Request.QueryString["id"]);
                        doc.Name = Path.GetFileName(fup.FileName);
                        if (!string.IsNullOrEmpty(hiddenValue.Text))
                        {
                            doc.DocumentId = int.Parse(hiddenValue.Text);
                        }
                        if (!string.IsNullOrEmpty(txtDesc.Text.Trim()))
                        {
                            doc.Description = txtDesc.Text.Trim();
                        }
                        else
                            doc.Description = Path.GetFileNameWithoutExtension(fup.FileName);

                        doc.ContentType = fup.PostedFile.ContentType;
                        doc.Url = guidFileName;
                        doc.Size = GetSize(fup.PostedFile.ContentLength);

                        mgr.SaveDocument(doc);


                        code = string.Format(code, mgr.GetHTMLDocuments(doc.EmployeeId.Value));
                        JavascriptHelper.DisplayClientMsg("Document added.", Page, code);
                    }
                    catch (Exception exp1)
                    {
                        JavascriptHelper.DisplayClientMsg(exp1.Message, Page);
                    }
                
                LoadLevels();
            }
        }
        

        public static bool IsImage(string pExtension)
        {
            if ((pExtension.ToLower() == ".jpg" | pExtension.ToLower() == ".gif" | pExtension.ToLower() == ".bmp" | pExtension.ToLower() == ".jpeg" | pExtension.ToLower() == ".png"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GetSize(int byteValue)
        {
            if (byteValue < 1024)
                return "1 kb";

            double kb = ConvertBytesIntoKB(byteValue);
            if (kb > 1024)
            {
                return ConvertKBIntoMB(kb) + " mb";
            }
            else
            {
                return kb + " kb";
            }
        }

        public static int ConvertBytesIntoKB(int byteValue)
        {
            double mb = (byteValue / 1024);
            if (mb.ToString("#") == "")
                return 0;
            return Convert.ToInt32(mb.ToString("#"));
        }
        public static string ConvertKBIntoMB(object value)
        {
            long kb = Convert.ToInt64(value);
            double mb = (kb / 1024.0);
            return mb.ToString("#.#");
        }

        protected void chkIsRetiredOrResignedAlso_CheckedChanged(object sender, EventArgs e)
        {
            LoadLevels();
        }
        protected void btnNext_Click()
        {
           
            LoadLevels();
        }

        protected void ChangePageNumber()
        {
            LoadLevels();
        }

        protected void btnPrevious_Click()
        {

            LoadLevels();
        }
        public void ReBindEmployees()
        {

            LoadLevels();
        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int start = 0;
            int pagesize = 50;
            int BranchID = -1;
            int DepartmentID = -1;
            int LevelID = -1;
            int DesignationID = -1;
            int StatusID = -1;
            int CurrentPage = 1;

            bool? ShowRetired = null;
            string EmpID = "";
            string INO = "";
            string StatusOnly = "";

            int? total = 0;
            bool statusOnly = false;
            string customRoleDeparmentList = null;

            if (cmbBranch.SelectedItem.Value != null && cmbBranch.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbBranch.SelectedItem.Value.Trim()))
                BranchID = Convert.ToInt32(cmbBranch.SelectedItem.Value);

            if (cmbDepartment.SelectedItem.Value != null && cmbDepartment.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value.Trim()))
                DepartmentID = Convert.ToInt32(cmbDepartment.SelectedItem.Value);


            if (cmbLevel.SelectedItem.Value != null && cmbLevel.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbLevel.SelectedItem.Value.Trim()))
                LevelID = Convert.ToInt32(cmbLevel.SelectedItem.Value);

            if (cmbDesignation.SelectedItem.Value != null && cmbDesignation.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbDesignation.SelectedItem.Value.Trim()))
                DesignationID = Convert.ToInt32(cmbDesignation.SelectedItem.Value);

            //if (cmbEmpStatus.SelectedItem.Value != null && cmbEmpStatus.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbEmpStatus.SelectedItem.Value.Trim()))
            //{
            //    StatusID = Convert.ToInt32(cmbEmpStatus.SelectedItem.Value);
            //    statusOnly = cmbEmpStatus.SelectedItem.Text.ToLower().Contains("only");
            //}

            if (!string.IsNullOrEmpty(txtINO.Text))
                INO = Convert.ToString(txtINO.Text);

            if (!string.IsNullOrEmpty(cmbEmployee.SelectedItem.Text))
                EmpID = Convert.ToString(cmbEmployee.SelectedItem.Text);

            if (cmbRet.SelectedItem.Value != null && cmbRet.SelectedItem.Index != -1 && !string.IsNullOrEmpty(cmbRet.SelectedItem.Value.Trim()))
            {
                if (cmbRet.SelectedItem.Value.ToLower().Trim() == "true" || cmbRet.SelectedItem.Value.ToLower().Trim() == "false")
                {
                    ShowRetired = Convert.ToBoolean(cmbRet.SelectedItem.Value);
                }
            }

            
            if (SessionManager.IsCustomRole)
                customRoleDeparmentList = SessionManager.CustomRoleDeparmentIDList;

            

            
            


            EmployeeManager employeeManager = new EmployeeManager();
            
            List<GetAllEmployeesNewResult> resultSet = employeeManager.GetAllEmployeesNew(e.Start, int.Parse(cmbPageSize.SelectedItem.Value),
                 ref total, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), SessionManager.CurrentCompanyId,
                   EmpID.Trim(), INO.Trim(), ShowRetired, BranchID, DepartmentID, DesignationID, LevelID, customRoleDeparmentList, StatusID, statusOnly);


            e.Total = total.Value;

            if (resultSet.Any())
                storeEmpList.DataSource = resultSet;
            storeEmpList.DataBind();
        }

    }
}


