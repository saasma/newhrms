﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using Web.Master;
using BLL.Base;
using DAL;
using Bll;

namespace Web.NewHR
{
    public partial class LeaveRequestList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {


            HR master = this.Master as HR;
            if (master != null)
                master.DisableViewState = true;

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                txtFromDate.SelectedDate = DateTime.Now;
                txtToDate.SelectedDate = DateTime.Now;

                ColumnDaysOrHours.Text = LeaveAttendanceManager.DaysOrHourTitle;

                X.AddScript("searchList();");

                List<LLeaveType> list = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId)
                    .Where(x => x.IsChildLeave == null || x.IsChildLeave == false).ToList();
                list.Add(new LLeaveType { LeaveTypeId = 0, Title = "Unpaid Leave" });
                cmbLeaveType.Store[0].DataSource = list;
                cmbLeaveType.Store[0].DataBind();

                cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                cmbBranch.Store[0].DataBind();

                if (!string.IsNullOrEmpty(Request.QueryString["status"]))
                {
                    ExtControlHelper.ComboBoxSetSelected(Request.QueryString["status"].ToString(), cmbStatus);
                    txtFromDate.Text = "";
                    txtToDate.Text="";
                }
            }
        }

        public void btn_Click(object sender, DirectEventArgs e)
        {
            ClickMore(HiddenLeaveRequestId.Text.Trim());
        }

        [DirectMethod]
        public void ClickMore(string LeaveRequestId)
        {
            int leaveId = 0;

            if (int.TryParse(LeaveRequestId, out leaveId) == false)
                return;

            if (leaveId == 0)
                return;

            DAL.LeaveRequest leave = LeaveAttendanceManager.GetLeaveRequestById(leaveId);
            if (leave != null)
            {
                //if (LeaveRequestManager.CanCurrentEmployeeChangeLeaveRequestStatus(SessionManager.CurrentLoggedInEmployeeId, leave) == false)
                //{
                //    X.MessageBox.Show(
                //   new MessageBoxConfig
                //   {
                //       Message = "Not enough permission to view the leave details.",
                //       Buttons = MessageBox.Button.OK,
                //       Title = "Warning",
                //       Icon = MessageBox.Icon.WARNING,
                //       MinWidth = 300
                //   });
                //    return;
                //}
            }

            //X.ResourceManager.RegisterClientScriptBlock("skdjfls", "if(typeof(DisableEnableControls)!='undefined') DisableEnableControls();");
            //Dictionary<string,string> row = new Dictionary<string,string>();
            //row.Add("LeaveRequestId", LeaveRequestId);
            //row.Add("LeaveTypeId", LeaveTypeId);
            //row.Add("EmployeeId", EmployeeId);
            //row.Add("EmployeeName", EmployeeId);
            //row.Add("FromDateEng", FromDate);
            //row.Add("ToDateEng", ToDateEng);
            //row.Add("DaysOrHours", DaysOrHours);
            //row.Add("ApplyTo", ApplyTo);
            //row.Add("CC1Name", CC1Name);
            //row.Add("CC2Name", CC2Name);
            //row.Add("Reason", Reason);
            //row.Add("IsHour", IsHour);
            //row.Add("Title", Title);


            this.AssignLeave.ClearField();
            if (this.AssignLeave.LoadSelectedData(leaveId) == false)
            {
                X.MessageBox.Show(
                 new MessageBoxConfig
                 {
                     Message = "Leave is disabled for this employee,can not be changed.",
                     Buttons = MessageBox.Button.OK,
                     Title = "Warning",
                     Icon = MessageBox.Icon.WARNING,
                     MinWidth = 300
                 });

                Ext.Net.Mask m = new Mask();
                m.Hide();
                return;
            }


            this.AssignLeave.AssignLeaveWindow.Center();
            this.AssignLeave.AssignLeaveWindow.Show();
            //  this.AssignLeave.LoadData(false);


        }

        [DirectMethod]
        public static string GetDaysForLeave(string fromdate, string todate, int leaveTypeId1, int? empId)
        {
            float daysCount = 0;

            if (empId == null || empId == -1)
                empId = 0;

            if (string.IsNullOrEmpty(fromdate))
                return "";

            DateTime from = Convert.ToDateTime(fromdate);
            DateTime? to = null;

            if (!string.IsNullOrEmpty(todate))
                to = Convert.ToDateTime(todate);
            else
                to = from;

            if (CommonManager.CalculationConstant.CompanyHasHourlyLeave != null && CommonManager.CalculationConstant.CompanyHasHourlyLeave.Value)
                return (to.Value - from).Days.ToString();

            List<GetHolidaysForAttendenceResult> holiday = new HolidayManager()
                .GetMonthsHolidaysForAttendence(0, 0, SessionManager.CurrentCompanyId, 0, from, to,empId).ToList();


            LeaveAttendanceManager.GetLeaveDetail(false, false, from, to.Value, leaveTypeId1
                , null, null, holiday, 0, false, empId.Value, ref daysCount);

            return daysCount.ToString();
        }

        protected void btnCreateLeaveRequest_Click(object sender, DirectEventArgs e)
        {
            //this.AssignLeave.CurrentDate = hdFromDateEng.Text;
            this.AssignLeave.ClearField();
            this.AssignLeave.AssignLeaveWindow.Center();
            this.AssignLeave.AssignLeaveWindow.Show();
            // this.AssignLeave.LoadData(true);
            //this.AssignLeave.Initialize();



            this.AssignLeave.ShowForHRCreateRequest();


        }

        protected void btnAssign_Click(object sender, DirectEventArgs e)
        {
            //this.AssignLeave.CurrentDate = hdFromDateEng.Text;
            this.AssignLeave.ClearField();
            this.AssignLeave.AssignLeaveWindow.Center();
            this.AssignLeave.AssignLeaveWindow.Show();
            // this.AssignLeave.LoadData(true);
            //this.AssignLeave.Initialize();



            this.AssignLeave.ShowForAssignLeave();


        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            DateTime? startDate = null, endDate = null;
            bool useModifiedDate = false;
            int status = 0, start = 0, limit = 99999;
            string branch = "", employee = "0";
            int? leaveTypeId = null;

            if (cmbLeaveType.SelectedItem != null && cmbLeaveType.SelectedItem.Value != null)
                leaveTypeId = int.Parse(cmbLeaveType.SelectedItem.Value);

            if (!string.IsNullOrEmpty(hdnFromDate.Text.Trim()))
                startDate = DateTime.Parse(hdnFromDate.Text.Trim());
            //else
            //    startDate = DateTime.Parse(txtFromDate.Text.Trim());

            if (!string.IsNullOrEmpty(hdnToDate.Text.Trim()))
                endDate = DateTime.Parse(hdnToDate.Text.Trim());
            //else
            //    endDate = DateTime.Parse(txtToDate.Text.Trim());

            status = int.Parse(cmbStatus.SelectedItem.Value);

            if (!string.IsNullOrEmpty(cmbBranch.SelectedItem.Text))
                branch = "";

            foreach (Ext.Net.ListItem item in cmbBranch.SelectedItems)
            {
                branch += item.Value + ",";
            }

            branch = branch.Substring(0, branch.Length - 1);


            if (!string.IsNullOrEmpty(cmbSearch.SelectedItem.Text))
                employee = cmbSearch.SelectedItem.Value;

            //useModifiedDate = !chkUseModifiedDate.Checked;


            if (cmbUseModifiedDate.SelectedItem.Value == "true")
                useModifiedDate = false;
            else
                useModifiedDate = true;


            List<DAL.GetLeaveRequestListForHRResult> list = LeaveAttendanceManager.GetLeaveRequestListForHR
                (startDate, endDate, status, branch, employee, start, limit, useModifiedDate,leaveTypeId,Hidden_Sort.Text);

            foreach (GetLeaveRequestListForHRResult item in list)
            {
                item.CreatedDate = item.CreatedOn == null ? "" : item.CreatedOn.Value.ToString("dd-MMM-yyyy h:m tt");
                item.FromDate = item.FromDateEng.Value.ToString("yyyy-MM-dd");
                item.ToDate = item.ToDateEng.Value.ToString("yyyy-MM-dd");
            }

            
            ExcelHelper.ExportToExcel("Leave Request List", list,
                new List<String>() { "LeaveRequestId", "CreatedOn", "TotalRows", "RowNumber", "FromDateEng", "ToDateEng" },
                new List<String>() { },
                new Dictionary<string, string>() { { "CreatedDate", "Created Date" }, { "DaysOrHours", LeaveAttendanceManager.DaysOrHourTitle}, { "FromDate", "From" }, { "ToDate", "To" }, { "RecommendedByName", "Recommended By" }, { "ApprovedByName", "Approved By" } },
                new List<string>() {},new List<string>{},new List<string>{"FromDate","ToDate"},
                new Dictionary<string, string>() { },
                new List<string> { "CreatedDate", "Name", "Branch", "Department", "Leave", "DaysOrHours", "FromDate", "ToDate", "Status", "RecommendedByName", "ApprovedByName" });

        }

    }
}