﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Calendar;
using Utils.Helper;

namespace Web.NewHR
{
    public partial class PromotionListGeneral : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "taxImportPopup", "../ExcelWindow/PromotionListGeneralExcel.aspx", 450, 500);

        }

        [DirectMethod]
        public string GetNewGrade(int levelId, int gradeStep,int newLevelId)
        {
            return NewPayrollManager.GetNewGrade(levelId, gradeStep, newLevelId, GetCurrentDateForSalary(), LevelGradeChangeType.Promotion).ToString();            
        }

        public void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int detailId = int.Parse(hdn.Text.Trim());
            Status status = SalaryManager.DeleteSavedPromotion(detailId);
            if (status.IsSuccess)
            {
                LoadLastOne(false);
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        public void Initialise()
        {
            StoreLevel.DataSource = NewPayrollManager.GetAllParentLevels();
            StoreLevel.DataBind();

            StorePostDesignation.DataSource = NewPayrollManager.GetAllPositionOrDesignationsList();
            StorePostDesignation.DataBind();

            StoreAllDesignation.DataSource = NewPayrollManager.GetAllPositionOrDesignationsList();
            StoreAllDesignation.DataBind();

            LoadLastOne(false);
        }

        private void LoadLastOne(bool lastApprovedAlso)
        {
            LevelGradeChange promotion = SalaryManager.GetUnapprovedPromotion(lastApprovedAlso);
            if (promotion != null)
            {
                txtName.Text = promotion.Name;
                calFromDate.Text = promotion.FromDate;

                List<PromotionBO> list = new List<PromotionBO>();
                List<LevelGradeChangeDetail> details = promotion.LevelGradeChangeDetails.OrderBy(x => x.SequenceNo).ToList();
                EmployeeManager empMgr = new EmployeeManager();
                foreach (LevelGradeChangeDetail item in details)
                {
                    PromotionBO emp = new PromotionBO();
                    emp.DetailId = item.DetailId;
                    emp.EmployeeId = item.EmployeeId.Value;
                    emp.EmployeeName = empMgr.GetById(item.EmployeeId.Value).Name;
                    emp.PrevLevelId = item.PrevLevelId.ToString();
                    emp.PrevDesignationId = item.PrevDesignationId.ToString();
                    emp.PrevGrade = item.PrevStepGrade.ToString();
                    emp.FromDate = item.FromDate;

                    emp.DesignationId = item.DesignationId.ToString();
                    emp.LevelId = item.LevelId.ToString();
                    emp.StepGrade = item.StepGrade.ToString();

                    if (item.Status == (int)LevelGradeChangeStatusType.Saved)
                        emp.StatusText = "Saved";
                    else
                        emp.StatusText = "Approved";

                    list.Add(emp);
                }

                gridPromotion.Store[0].DataSource = list;
                gridPromotion.Store[0].DataBind();
            }
            else
            {
                gridPromotion.Store[0].DataSource = new List<PromotionBO>();
                gridPromotion.Store[0].DataBind();
            }
        }

        public void lnkBtnApprove_Click(object sender, DirectEventArgs e)
        {
            string gridJSON = e.ExtraParams["List"];
            List<PromotionBO> items = JSON.Deserialize<List<PromotionBO>>(gridJSON);
            LevelGradeChange change = new LevelGradeChange();
            change.Type = (int)LevelGradeChangeType.Promotion;

            LevelGradeChangeStatusType status = LevelGradeChangeStatusType.Approved;
            //if (sender == lnkBtnReject)
            //    status = LevelGradeChangeStatusType.Reject;

            foreach (PromotionBO item in items)
            {
                if (string.IsNullOrEmpty(item.FromDate))
                {
                    SetWarning(lblMsg, string.Format("From date is required for the employee {0}.", item.EmployeeName));
                    return;
                }
                if (string.IsNullOrEmpty(item.LevelId))
                {
                    SetWarning(lblMsg, string.Format("New Grade is required for the employee {0}.", item.EmployeeName));
                    return;
                }
                if (string.IsNullOrEmpty(item.StepGrade))
                {
                    SetWarning(lblMsg, string.Format("New Grade is required for the employee {0}.", item.EmployeeName));
                    return;
                }
                if (string.IsNullOrEmpty(item.DesignationId))
                {
                    SetWarning(lblMsg, string.Format("New Post is required for the employee {0}.", item.EmployeeName));
                    return;
                }
                LevelGradeChangeDetail emp = new LevelGradeChangeDetail();
                emp.EmployeeId = item.EmployeeId;
                emp.PrevDesignationId = int.Parse(item.PrevDesignationId);
                emp.FromDate = item.FromDate;
                emp.FromDateEng = GetEngDate(item.FromDate);

                emp.PrevLevelId = int.Parse(item.PrevLevelId);
                emp.PrevDesignationId = int.Parse(item.PrevDesignationId);
                emp.PrevStepGrade = double.Parse(item.PrevGrade);

                emp.DesignationId = int.Parse(item.DesignationId);
                emp.LevelId = int.Parse(item.LevelId);

                bool allowDecimalInGrade = NewPayrollManager.AllowDecimalInGrade();

                if (!allowDecimalInGrade && item.StepGrade.Contains("."))
                {
                    SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                    return;
                }

                if (allowDecimalInGrade && item.StepGrade.Contains("."))
                {
                    string[] splitString = item.StepGrade.Split('.');

                    int numberValue = int.Parse(splitString[0]);
                    int decimalValue = int.Parse(splitString[1]);

                    if (numberValue == 0)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                        return;
                    }

                    if (decimalValue != 0 && decimalValue != 5)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                        return;
                    }
                }

                emp.StepGrade = double.Parse(item.StepGrade);

                BLevel oldLevel = NewPayrollManager.GetLevelById(emp.PrevLevelId.Value);
                BLevel newLevel = NewPayrollManager.GetLevelById(emp.LevelId.Value);

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.RBB)
                {
                    if (newLevel.LevelId == oldLevel.LevelId)
                    {
                        SetWarning(lblMsg, string.Format("Same level promotion can not be done for the employee {0}.", item.EmployeeName));
                        return;
                    }
                    if (oldLevel.Order <= newLevel.Order)
                    {
                        SetWarning(lblMsg, string.Format("Promotion can not be done in lower level for the employee {0}.", item.EmployeeName));
                        return;
                    }
                }
                BLevelRate rate = NewPayrollManager.GetLevelRate(emp.LevelId.Value, GetCurrentDateForSalary());

                if (allowDecimalInGrade)
                {
                    double maxStepGrade = double.Parse(rate.NoOfStepsGrade == null ? "0" : rate.NoOfStepsGrade.Value.ToString()) + 0.5;

                    if (emp.StepGrade > maxStepGrade)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}, max grade value is {2}.", item.EmployeeName, emp.StepGrade, maxStepGrade));
                        return;
                    }
                }
                else
                {
                    if (rate != null && emp.StepGrade > rate.NoOfStepsGrade)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}, max grade value is {2}.", item.EmployeeName, emp.StepGrade, rate.NoOfStepsGrade));
                        return;
                    }
                }

                emp.Type = (int)LevelGradeChangeType.Promotion;
                emp.Status = (int)status;

               

                change.LevelGradeChangeDetails.Add(emp);
            }

           
            Status respStatus = SalaryManager.ApprovePromotionSalaryChangeStatus(change,status);

            if (respStatus.IsSuccess)
            {
                if (status==LevelGradeChangeStatusType.Approved)
                    SetMessage(lblMsg, string.Format("Approved for {0} employees.", respStatus.Count));
                else
                {
                    SetMessage(lblMsg, string.Format("Rejected for {0} employees.", respStatus.Count));
                }
                LoadLastOne(false);
            }
            else
            {
                SetWarning(lblMsg, respStatus.ErrorMessage);
            }
        }
        public void btnSaveUpdate_Save(object sender, DirectEventArgs e)
        {
            string gridJSON = e.ExtraParams["List"];
            List<PromotionBO> items = JSON.Deserialize<List<PromotionBO>>(gridJSON);
            List<PromotionBO> itemsDistinct = new List<PromotionBO>();
            LevelGradeChange change = new LevelGradeChange();
            change.Name = txtName.Text.Trim();
            change.FromDate = calFromDate.Text.Trim();
            if (!string.IsNullOrEmpty(calFromDate.Text.Trim()))
                change.FromDateEng = GetEngDate(change.FromDate);
            change.Type = (int)LevelGradeChangeType.Promotion;

           

            int seq = 0;
            foreach (PromotionBO item in items)
            {

                if (itemsDistinct.Any(x => x.EmployeeId == item.EmployeeId))
                {
                    SetWarning(lblMsg, string.Format("Multiple Items for same Employee ID {0}.", item.EmployeeId));
                    return;
                }
                else
                {
                    itemsDistinct.Add(item);
                }

                if(string.IsNullOrEmpty(item.FromDate))
                {
                    SetWarning(lblMsg, string.Format("From date is required for the employee {0}.", item.EmployeeName));
                    return;
                }
                if (string.IsNullOrEmpty(item.LevelId))
                {
                    SetWarning(lblMsg, string.Format("New Level is required for the employee {0}.", item.EmployeeName));
                    return;
                }
                if (string.IsNullOrEmpty(item.StepGrade))
                {
                    SetWarning(lblMsg, string.Format("New Grade is required for the employee {0}.", item.EmployeeName));
                    return;
                }
                if (string.IsNullOrEmpty(item.DesignationId))
                {
                    SetWarning(lblMsg, string.Format("New Post is required for the employee {0}.", item.EmployeeName));
                    return;
                }
                LevelGradeChangeDetail emp = new LevelGradeChangeDetail();
                emp.EmployeeId = item.EmployeeId;
                emp.PrevDesignationId = int.Parse(item.PrevDesignationId);
                emp.FromDate = item.FromDate;
                emp.FromDateEng = GetEngDate(item.FromDate);
                emp.PrevLevelId = int.Parse(item.PrevLevelId);
                emp.PrevDesignationId = int.Parse(item.PrevDesignationId);
                emp.PrevStepGrade = double.Parse(item.PrevGrade);

                emp.DesignationId = int.Parse(item.DesignationId);
                emp.LevelId = int.Parse(item.LevelId);

                bool allowDecimalInGrade = NewPayrollManager.AllowDecimalInGrade();

                if (!allowDecimalInGrade && item.StepGrade.Contains("."))
                {
                    SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                    return;
                }

                if (allowDecimalInGrade && item.StepGrade.Contains("."))
                {
                    string[] splitString = item.StepGrade.Split('.');

                    int numberValue = int.Parse(splitString[0]);
                    int decimalValue = int.Parse(splitString[1]);

                    if (numberValue == 0)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                        return;
                    }

                    if (decimalValue != 0 && decimalValue != 5)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}.", item.EmployeeName, emp.StepGrade));
                        return;
                    }
                }

                emp.StepGrade = double.Parse(item.StepGrade);


                BLevel oldLevel = NewPayrollManager.GetLevelById(emp.PrevLevelId.Value);
                BLevel newLevel = NewPayrollManager.GetLevelById(emp.LevelId.Value);

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.RBB)
                {
                    if (newLevel.LevelId == oldLevel.LevelId)
                    {
                        SetWarning(lblMsg, string.Format("Same level promotion can not be done for the employee {0}.", item.EmployeeName));
                        return;
                    }
                    if (oldLevel.Order <= newLevel.Order)
                    {
                        SetWarning(lblMsg, string.Format("Promotion can not be done in lower level for the employee {0}.", item.EmployeeName));
                        return;
                    }
                }

                EDesignation desig = new CommonManager().GetDesignationById(emp.DesignationId.Value);
                if (desig.LevelId != emp.LevelId)
                {
                    SetWarning(lblMsg, string.Format("New level and new Post does not match for the employee {0}.", item.EmployeeName));
                    return;
                }

                BLevelRate rate = NewPayrollManager.GetLevelRate(emp.LevelId.Value, GetCurrentDateForSalary());

                if (allowDecimalInGrade)
                {
                    double maxStepGrade = double.Parse(rate.NoOfStepsGrade == null ? "0" : rate.NoOfStepsGrade.Value.ToString()) + 0.5;

                    if (emp.StepGrade > maxStepGrade)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}, max grade value is {2}.", item.EmployeeName, emp.StepGrade, maxStepGrade));
                        return;
                    }
                }
                else
                {
                    if (rate != null &&  emp.StepGrade > rate.NoOfStepsGrade)
                    {
                        SetWarning(lblMsg, string.Format("New Grade {1} not valid for the employee {0}, max grade value is {2}.", item.EmployeeName, emp.StepGrade, rate.NoOfStepsGrade));
                        return;
                    }
                }

                emp.Type = (int)LevelGradeChangeType.Promotion;
                emp.Status = (int)LevelGradeChangeStatusType.Saved;

                emp.SequenceNo = (seq++);

                change.LevelGradeChangeDetails.Add(emp);
            }

            if (change.LevelGradeChangeDetails.Count <= 0)
            {
                SetWarning(lblMsg, string.Format("Employee(s) must be selected for Promotion."));
                return;
            }

            bool isInsert = true;
            Status status = SalaryManager.InsertUpdatePromotion(change,ref isInsert);

            if (status.IsSuccess)
            {
                btnApprove.Enable();

                if (isInsert)
                    SetMessage(lblMsg, string.Format("Promotion saved for {0} employees.", status.Count));
                else
                {
                    SetMessage(lblMsg, string.Format("Promotion changed for {0} employees.", status.Count));
                }
                LoadLastOne(false);
            }
            else
            {
                SetWarning(lblMsg, status.ErrorMessage);
            }
        }


        public void cmbSearch_Select(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(calFromDate.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("From date is required.");
                return;
            }
            


            string gridJSON = e.ExtraParams["List"];
            List<PromotionBO> items = JSON.Deserialize<List<PromotionBO>>(gridJSON);

            string employeeId = cmbSearch.SelectedItem.Value;
            if (items.Any(x => x.EmployeeId.ToString() == employeeId.ToString().ToLower().Trim()) == false)
            {
                if (EmployeeManager.HasLevelGrade(int.Parse(employeeId)))
                {
                    EEmployee emp = EmployeeManager.GetEmployeeById(int.Parse(employeeId));
                    BLevel currentLevel = NewHRManager.GetEmployeeCurrentLevel(emp.EmployeeId);
                    double? currentGrade = NewHRManager.GetEmployeeCurrentGradeStep(emp.EmployeeId);
                    EDesignation designation = NewHRManager.GetEmployeeCurrentPostDesitionPosition(emp.EmployeeId);

                    // if current level is null then the Employee could be moving from Contract to Permanent case,
                    // so for that case pick Old Level from Designation case
                    if (currentLevel == null)
                    {
                        items.Add(new PromotionBO
                        {

                            EmployeeId = emp.EmployeeId,
                            EmployeeName = emp.Name,
                            Branch = new BranchManager().GetById(emp.BranchId.Value).Name,
                            Department = new DepartmentManager().GetById(emp.DepartmentId.Value).Name,
                            PrevLevelId = designation.LevelId.ToString(),
                            PrevGrade = "0",
                            PrevDesignationId = designation.DesignationId.ToString(),
                            FromDate = calFromDate.Text.Trim(),

                            StatusText = ""
                        });
                    }
                    else if (currentLevel != null && currentGrade != null)
                    {
                        items.Add(new PromotionBO
                        {
                            
                            EmployeeId = emp.EmployeeId,
                            EmployeeName = emp.Name,
                            Branch = new BranchManager().GetById(emp.BranchId.Value).Name,
                            Department = new DepartmentManager().GetById(emp.DepartmentId.Value).Name,
                            PrevLevelId = currentLevel.LevelId.ToString(),
                            PrevGrade = currentGrade.Value.ToString(),
                            PrevDesignationId = designation.DesignationId.ToString(),
                            FromDate = calFromDate.Text.Trim(),

                            StatusText = ""
                        });
                    }
                    else
                    {
                        NewMessage.ShowWarningMessage("Employee has no level or grade assigned.");
                        return;
                    }
                }
                else
                {
                    NewMessage.ShowWarningMessage("Level/Grade assigned employee can only be promoted from this page.");
                }

            }

            gridPromotion.Store[0].DataSource = items.OrderBy(x => x.EmployeeName).ToList();
            gridPromotion.Store[0].DataBind();

            cmbSearch.ClearValue();
        }



        public void btnLoadQuestion_Click(object sender, DirectEventArgs e)
        {
            LoadLevels();
        }

        public void LoadLevels()
        {
            var list = (List<LevelGradeChangeDetail>)Session["PromotionList"];
            List<LevelGradeChangeDetail> levelList = new List<LevelGradeChangeDetail>();
            if (list != null)
                levelList = list;


            List<EmployeeLevelGradeBO> currentLeveList = NewPayrollManager.GetEmployeeCurrentLevelAndGradeForExportPromotion();

            List<PromotionBO> boList = new List<PromotionBO>();
            EmployeeManager empMgr = new EmployeeManager();
            foreach (LevelGradeChangeDetail item in levelList)
            {
                PromotionBO emp = new PromotionBO();
                emp.DetailId = item.DetailId;
                emp.EmployeeId = item.EmployeeId.Value;
                EEmployee eemp = empMgr.GetById(item.EmployeeId.Value);
                emp.EmployeeName = eemp.Name;

                EmployeeLevelGradeBO currentLevel = currentLeveList.FirstOrDefault(x => x.EIN == item.EmployeeId);
                if (currentLevel != null)
                    emp.PrevLevelId = currentLevel.LevelId.ToString();

                if (currentLevel != null)
                    emp.PrevDesignationId = currentLevel.DesignationId.ToString();

                if (currentLevel != null)
                    emp.PrevGrade = currentLevel.Grade.ToString();

                emp.FromDate = item.FromDate;

                emp.DesignationId = item.DesignationId.ToString();
                emp.LevelId = item.LevelId.ToString();
                emp.StepGrade = item.StepGrade.ToString();


                emp.StatusText = "";

                boList.Add(emp);
            }

            gridPromotion.Store[0].Add(boList);
            //gridPromotion.Store[0].Add(boList);

            //X.Msg.Alert(boList.Count().ToString(), boList.Count().ToString()).Show();

            

        }

    }
}