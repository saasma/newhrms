﻿<%@ Page Title="Grade Fitting List" Language="C#" MasterPageFile="~/Master/HR.Master"
    AutoEventWireup="true" CodeBehind="GradeFittingList.aspx.cs" Inherits="Web.NewHR.GradeFittingList" %>

<%@ Register Src="~/newhr/UserControls/EmployeeWizard.ascx" TagName="EmployeeWizard"
    TagPrefix="uc2" %>
<%@ Register Src="~/NewHR/UserControls/ActingListCtl.ascx" TagName="FamilyCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var levelRenderer = function (value) {
            if (value == "")
                return "";

            var level = <%=StoreLevel.ClientID %>.getById(value);
            if (Ext.isEmpty(level))
                return "";
            return level.data.GroupLevel;
        }
        var designationRenderer = function (value) {
            if (value == "" || value == null)
                return "";

            var level = <%=StorePostDesignation.ClientID %>.getById(value);
            if (Ext.isEmpty(level))
                return "";
            return level.data.Name;
        }
        var record = null;
        var afterEdit = function (editor, e) {
            if (e.field == "FromDate") 
            {
                if(e.record.data.FromDate != "" && isValidDate(e.record.data.FromDate) == false)
                {
                    e.record.data.FromDate = "";
                }
            }
            if (e.field == "LevelId") 
            {

                Ext.getBody().mask("Loading...");
                Ext.net.DirectMethods.GetNewGrade(
                    e.record.data.PrevLevelId,e.record.data.PrevGrade,e.record.data.LevelId,
                {
                            success: function (result) {  Ext.getBody().unmask();promotionCallback(result);  },
                            failure: function (result) {   Ext.getBody().unmask();promotionCallback(""); }
                }
                );


                record = e.record;
            }
            if(e.record.data.StepGrade=="")
                e.record.data.StepGrade="0";
            e.record.commit();
        }

        var promotionCallback = function(result)
        {
            record.data.StepGrade=result;
            record.commit();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="separator bottom">
    </div>
    <ext:Store ID="StoreLevel" runat="server">
        <Model>
            <ext:Model ID="Model1" runat="server" IDProperty="LevelId">
                <Fields>
                    <ext:ModelField Name="LevelId" Type="String" />
                    <ext:ModelField Name="GroupLevel" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <ext:Store ID="StorePostDesignation" runat="server">
        <Model>
            <ext:Model ID="Model3" runat="server" IDProperty="DesignationId">
                <Fields>
                    <ext:ModelField Name="DesignationId" Type="String" />
                    <ext:ModelField Name="Name" />
                </Fields>
            </ext:Model>
        </Model>
    </ext:Store>
    <div class="innerLR">
        <h4 class="heading">
            Annual Grade List</h4>
        <ext:Label ID="lblMsg" runat="server" />
        <div class="widget">
            <div class="widget-head">
            </div>
            <div class="widget-body">
                <table class="fieldTable firsttdskip">
                    <tr>
                        <td>
                            <pr:CalendarExtControl Width="180px" FieldLabel="From Date" ID="calFromDate" runat="server"
                                LabelAlign="Top" LabelSeparator="" />
                            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="calFromDate" ErrorMessage="Date is required." />
                        </td>
                        <td>
                            <ext:TextField ID="txtName" runat="server" Width="180px" LabelAlign="Top" FieldLabel="Name">
                            </ext:TextField>
                              <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server"
                                ValidationGroup="SaveUpdate" ControlToValidate="txtName" ErrorMessage="Name is required." />
                        </td>
                        <td style="padding-top: 15px;">
                            <ext:LinkButton runat="server" StyleSpec="padding:0px;margin-top:20px;" ID="btnGenerate"
                                Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Generate" runat="server">
                                <DirectEvents>
                                    <Click OnEvent="btnGenerate_Click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                                <%--<Listeners>
                                    <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                    </Click>
                                </Listeners>--%>
                            </ext:LinkButton>
                        </td>
                    </tr>
                </table>
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridGradeFitting" runat="server"
                    Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store3" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="EmployeeId">
                                    <Fields>
                                        <ext:ModelField Name="EmployeeId" Type="string" />
                                        <ext:ModelField Name="EmployeeName" Type="string" />
                                        <%-- <ext:ModelField Name="Branch" Type="string" />--%>
                                        <ext:ModelField Name="PrevLevelId" Type="string" />
                                        <ext:ModelField Name="PrevGrade" Type="string" />
                                        <ext:ModelField Name="PrevDesignationId" Type="string" />
                                        <ext:ModelField Name="StepGrade" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <Plugins>
                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                            <Listeners>
                                <%-- <BeforeEdit Fn="beforeEdit" />--%>
                                <Edit Fn="afterEdit" />
                            </Listeners>
                        </ext:CellEditing>
                    </Plugins>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                                Align="Left" Width="60" DataIndex="EmployeeId" />
                            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                                Align="Left" Width="180" DataIndex="EmployeeName" />
                            <ext:Column ID="Column6" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Post" Align="Left" DataIndex="PrevDesignationId">
                                <Renderer Fn="designationRenderer" />
                            </ext:Column>
                            <ext:Column ID="Column2" Width="140" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Level" Align="Left" DataIndex="PrevLevelId">
                                <Renderer Fn="levelRenderer" />
                            </ext:Column>
                            <ext:Column ID="Column3" Width="90" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Grade" Align="Left" DataIndex="PrevGrade">
                            </ext:Column>
                            <ext:Column ID="Column4" Width="90" Sortable="false" MenuDisabled="true" runat="server"
                                Text="New Grade" Align="Left" DataIndex="StepGrade">
                                <Editor>
                                    <ext:NumberField runat="server" MinValue="0" />
                                </Editor>
                            </ext:Column>
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Width="75" Text="Actions" Align="Center">
                                <Commands>
                                    <%--<ext:CommandSeparator />
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />--%>
                                    <%--<ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" CommandName="Delete" />--%>
                                </Commands>
                                <%--<Listeners>
                                    <Command Handler="CommandHandler(command,record);" />
                                </Listeners>--%>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div class="buttonBlock">
                    <div class="left">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;margin-top:20px;" ID="btnSaveUpdate"
                            Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Save" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnSaveUpdate_Save">
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save the grade fitting?" />
                                    <ExtraParams>
                                        <ext:Parameter Name="List" Value="Ext.encode(#{gridGradeFitting}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                    <div class="right">
                        <ext:LinkButton ID="btnApprove" Cls="btnFlatGreen" BaseCls="btnFlatGreen" runat="server"
                            StyleSpec="padding:0px;margin-top:20px;" Text="Approve">
                            <Listeners>
                                <Click Handler="valGroup = 'saveupdate'; return CheckValidation();" />
                            </Listeners>
                            <DirectEvents>
                                <Click OnEvent="lnkBtnApprove_Click" Timeout="492000">
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to approve the Annual Grade Fitting?" />
                                    <ExtraParams>
                                        <ext:Parameter Name="List" Value="Ext.encode(#{gridGradeFitting}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </div>
                </div>
                <div style="clear: both">
                </div>
            </div>
        </div>
    </div>
    <br />
</asp:Content>
