﻿<%@ Page Title="Family Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmpFamilyList.aspx.cs" Inherits="Web.NewHR.EmpFamilyList" %>

<%@ Register Src="../Controls/MultiCheckCombo.ascx" TagName="MultiCheckCombo" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
    
        function searchList() {
            <%=gridFamilyMemb.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

        function refreshWindow()
        {
            searchList();
        }


    </script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>Family Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Store ID="storeBranch" runat="server">
            <Model>
                <ext:Model ID="Model1" runat="server" IDProperty="BranchId">
                    <Fields>
                        <ext:ModelField Name="BranchId" Type="String" />
                        <ext:ModelField Name="Name" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store ID="storeLevel" runat="server">
            <Model>
                <ext:Model ID="Model2" runat="server" IDProperty="LevelId">
                    <Fields>
                        <ext:ModelField Name="LevelId" Type="String" />
                        <ext:ModelField Name="Name" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <table border="0">
            <tr>
                <td style="width: 160px;">Branch</td>
                <td style="width: 160px;">Level</td>
                <td style=""></td>
                <td style="width: 160px;">Employee</td>
                <td style="width: 160px;">Relation</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="width: 160px;">
                    <ext:ComboBox ID="cmbBranch" runat="server" ValueField="BranchId" DisplayField="Name" EmptyText="Branch Filter"
                        FieldLabel="" Width="150" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local" StoreID="storeBranch">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 160px;">
                    <ext:ComboBox ID="cmbLevel" runat="server" ValueField="LevelId" DisplayField="Name" EmptyText="Level Filter"
                        FieldLabel="" Width="150" LabelSeparator="" ForceSelection="true"
                        QueryMode="Local" StoreID="storeLevel">
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td>
                    <ext:ComboBox ID="cmbRelation" Hidden="true" runat="server" ValueField="ID" DisplayField="Name" EmptyText="Relation Filter"
                        LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="Store1" runat="server">
                                <Model>
                                    <ext:Model ID="Model5" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="ID" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="width: 210px;">
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model3" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" DisplayField="Name"
                        ValueField="EmployeeId" EmptyText="Search Employee"
                        StoreID="storeSearch" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="padding-right: 5px;">
                    <uc3:MultiCheckCombo ID="multiRelation" runat="server" Width="200" />
                </td>
                <td>
                    <ext:Button ID="btnLoad" runat="server" Text="Load">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnExport" runat="server" Text="Export" AutoPostBack="true" OnClick="btnExport_Click" MarginSpec="0 0 0 10">
                    </ext:Button>
                </td>
                <td>
                    <ext:Button ID="btnImportExcel" runat="server" OnClientClick="EmpFamilyImport();return false;" Text="<i></i>Import from Excel" MarginSpec="0 0 0 10">
                    </ext:Button>
                </td>

            </tr>
        </table>
        <br />

        <ext:GridPanel StyleSpec="margin-top:10px;" ID="gridFamilyMemb" runat="server"
            Cls="itemgrid" OnReadData="Store_ReadData" Scroll="Horizontal">
            <Store>
                <ext:Store ID="storeFamilyMem" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="FamilyId">
                            <Fields>
                                <ext:ModelField Name="FamilyId" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="String" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="MemberName" Type="String" />
                                <ext:ModelField Name="Relation" Type="String" />
                                <ext:ModelField Name="DateOfBirth" Type="String" />
                                <ext:ModelField Name="IsDependent" Type="String" />
                                <ext:ModelField Name="Occupation" Type="String" />
                                <ext:ModelField Name="AgeOnSpecifiedSPDate" Type="String" />
                                <ext:ModelField Name="ContactNumber" Type="String" />
                                <ext:ModelField Name="IdCardNo" Type="String" />
                                <ext:ModelField Name="Gender" Type="String" />
                                <ext:ModelField Name="BloodGroup" Type="String" />
                                <ext:ModelField Name="Nationality" Type="String" />
                                <ext:ModelField Name="DocumentId" Type="String" />
                                <ext:ModelField Name="DocumentIdType" Type="String" />
                                <ext:ModelField Name="DocumentIssueDate" Type="String" />
                                <ext:ModelField Name="DocumentIssuePlace" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel ID="ColumnModel1" runat="server">
                <Columns>
                    <ext:Column ID="colEmplooyeeName" runat="server" Text="Name" DataIndex="Name"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="150" Locked="true" />
                    <ext:Column ID="colEmployeeId" runat="server" Text="EIN" DataIndex="EmployeeId" MenuDisabled="true"
                        Sortable="false" Align="Center" Width="60" Locked="true" />
                    <ext:Column ID="colEmpINO" runat="server" Text="INO" DataIndex="IdCardNo" MenuDisabled="true"
                        Sortable="false" Align="Center" Width="80" Locked="true" />
                    <ext:Column ID="colLevel" runat="server" Text="Member Name" DataIndex="MemberName" MenuDisabled="true"
                        Sortable="false" Align="Left" Width="150" />
                    <ext:Column ID="colFaculty" runat="server" Text="Relation" DataIndex="Relation" MenuDisabled="true"
                        Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colCourse" runat="server" Text="Date Of Birth" DataIndex="DateOfBirth" MenuDisabled="true"
                        Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colUniversity" runat="server" Text="Dependent" DataIndex="IsDependent"
                        MenuDisabled="true" Sortable="false" Align="Center" Width="80" />
                    <ext:Column ID="colInstitution" runat="server" Text="Occupation" DataIndex="Occupation"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="100" />
                    <ext:Column ID="colCountry" runat="server" Text="Age on specified Date" DataIndex="AgeOnSpecifiedSPDate"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="180" />
                    <ext:Column ID="colPercentage" runat="server" Text="Contact Number" DataIndex="ContactNumber"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="120" />
                    <ext:Column ID="colGender" runat="server" Text="Gender" DataIndex="Gender"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="60" />
                    <ext:Column ID="colBloodGroup" runat="server" Text="Blood Group" DataIndex="BloodGroup"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="80" />
                    <ext:Column ID="colNationality" runat="server" Text="Nationality" DataIndex="Nationality"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="80" />
                    <ext:Column ID="colDocument" runat="server" Text="Document ID" DataIndex="DocumentId"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="150" />
                    <ext:Column ID="colDocumentType" runat="server" Text="Document Type" DataIndex="DocumentIdType"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="150" />
                    <ext:Column ID="colDocumentIssueDate" runat="server" Text="Issue Date" DataIndex="DocumentIssueDate"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="100" />

                    <ext:Column ID="colDocumentIssuePlace" runat="server" Text="Issue Place" DataIndex="DocumentIssuePlace"
                        MenuDisabled="true" Sortable="false" Align="Left" Width="100" />




                </Columns>
            </ColumnModel>
            <Plugins>
                <ext:FilterHeader ID="FilterHeader2" runat="server" Remote="true" OnCreateFilterableField="OnCreateFilterableField" />
            </Plugins>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storeFamilyMem"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                                <%--#{cmbPageSize}.pageSize = this.getValue();#{PagingToolbar1}.moveFirst();--%>
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="25" Text="25" />
                                <ext:ListItem Value="100" Text="100" />
                                <ext:ListItem Value="500" Text="500" />
                                <ext:ListItem Value="100000" Text="All" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <br />
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
