﻿using BLL;
using BLL.Manager;
using Ext.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.NewHR
{
    public partial class ResignationRequestList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                tabChange.Text = 0.ToString();
            }
        }
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            gridReqList.Store[0].DataSource = CommonManager.ResignationReqListByStatus(Convert.ToInt32(tabChange.Text));
            gridReqList.Store[0].DataBind();
        }
        
        protected void btnStartExit_Click(object sender, DirectEventArgs e)
        {
            List<int> exitClearanceIds = new List<int>();
            if (FinanceClearance.Value.Equals(true))
            {
                exitClearanceIds.Add((int)ExitClearanceForms.FinanceClearanceForm);
            }
            if (AssetCustodian.Value.Equals(true))
            {
                exitClearanceIds.Add((int)ExitClearanceForms.AssetCustodianForm);
            }
            if (LibraryClearance.Value.Equals(true))
            {
                exitClearanceIds.Add((int)ExitClearanceForms.LibraryClearanceForm);
            }
            bool status = CommonManager.UpdateResignationReqStatus(hiddenValue.Text, "ExitProcess", exitClearanceIds);
            StartExitProcess.Close();
            gridReqList.Store[0].Reload();
        }

        protected void btnReject_Click(object sender, DirectEventArgs e)
        {
            bool status = CommonManager.UpdateResignationReqStatus(hiddenValue.Text, "Rejected", null);
            Reject.Close();
            gridReqList.Store[0].Reload();
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {
            var req = CommonManager.GetResignationReqById(hiddenValue.Text);
            ExpectedDate.Text = req.ExpectedDate.ToString();
            Reason.Text = req.Reason;
        }

        protected void btnTab_Click(object sender, DirectEventArgs e)
        {
            gridReqList.Store[0].DataSource = CommonManager.ResignationReqListByStatus(Convert.ToInt32(tabChange.Text));
            gridReqList.Store[0].DataBind();
            gridReqList.Store[0].Reload();            
        }
    }
}