﻿<%@ Page Title="Employee Activity Summary" Language="C#" MasterPageFile="~/Master/HROld.Master" AutoEventWireup="true" CodeBehind="HREmpActivitySummary.aspx.cs" Inherits="Web.NewHR.HREmpActivitySummary" %>


<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grayBackground .x-grid-cell
        {
            background-color: lightgray;
        }
        
        
        .x-column-header-inner
        {
            background-color: #428BCA;
            color: White;
        }
        
        .cellBorderCls .x-grid-cell
        {
            border: solid 1px #D9EDF7;
        }
    </style>
    <script type="text/javascript">

        function linkClick(EmployeeId){
            <%= hdnEmployeeId.ClientID %>.setValue(EmployeeId);
             <%= btnShowDetails.ClientID %>.fireEvent('click');
        }
        
        function linkRenderer(value, meta, record){          
            var employeeId = record.data.EmployeeId;              
            return String.format("<a class='company-link' href='#' onclick='linkClick(\"{0}\");'>{1}</a>", employeeId, value);
        }

        function StatusRenderer(e1,e2,record)
        {
            var status =  record.data.Status;
            if(status.toLowerCase() == 'yes' )
            {
                return '<img src="../images/yes.png" style="height:20px; width:20px;" />';
            }
            else if(status.toLowerCase() == 'no' )
            {
                return '<img src="../images/no.png" ="height:20px; width:20px;"/>';
            }
            else 
                return status;
            //return '<a href="EmployeeAppraisalHRPage.aspx?efid='+ record.data.AppraisalEmployeeFormID + '&fid='+ record.data.AppraisalFormRef_ID +'">View</a>';
        }
  
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <ext:ResourceManager ID="ResourceManager1" DisableViewState="false"  runat="server" />

    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Button ID="btnShowDetails" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnShowDetails_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Activity Summary
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div>Attendance Register need to save for the report</div>
        <table>
            
            <tr>
                <td style="width: 200px;">
                    
                    <ext:ComboBox ID="cmbPayrollPeriod" QueryMode="Local" runat="server" EmptyText="Month Filter"
                        ForceSelection="true" DisplayField="Name" ValueField="PayrollPeriodId"
                        Width="180" LabelSeparator="">
                        <Store>
                            <ext:Store ID="store14" runat="server">
                                <Model>
                                    <ext:Model ID="Model15" IDProperty="PayrollPeriodId" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="PayrollPeriodId" Type="String" />
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                            <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                    this.clearValue(); 
                                    this.getTrigger(0).hide();
                                }" />
                        </Listeners>
                    </ext:ComboBox>
                    <asp:RequiredFieldValidator Display="None" ID="rfvMonth" runat="server" ValidationGroup="LoadGrid"
                        ControlToValidate="cmbPayrollPeriod" ErrorMessage="Month is required." />
                </td>
                <td style="width:200px;"> 
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>

                    <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                        TypeAhead="false" Width="180" PageSize="9999" HideBaseTrigger="true" MinChars="1" EmptyText="Employee Filter"
                        TriggerAction="All" ForceSelection="true">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                        <div class="search-item">
                                                        <span>{Name}</span>  
                                            </div>
					            </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                        this.clearValue(); 
                                        this.getTrigger(0).hide();
                                    }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                
                <td style="padding-bottom: 5px; width: 100px;">
                    <ext:Button ID="btnLoad" runat="server" Text="Show" Cls="btn btn-primary">
                        <DirectEvents>
                            <Click OnEvent="btnLoad_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'LoadGrid'; if(CheckValidation()) return this.disable(); else return false;">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
                <td>
                    <ext:LinkButton ID="lnkExcelPrint" Icon="PageExcel" runat="server" Text="Export" MarginSpec="0 0 0 485"
                        OnClick="btnExcelPrint_Click" Hidden="false" AutoPostBack="true">
                    </ext:LinkButton>
                </td>
            </tr>
        </table>
        <br />
        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridEmpActivitySummary" runat="server"
            Cls="itemgrid" Scroll="Both" Width="1050" >
            <Store>
                <ext:Store ID="Store1" runat="server">
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="EmployeeId">
                            <Fields>
                                <ext:ModelField Name="EmployeeId" Type="Int" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="MonthDays" Type="Int" />
                                <ext:ModelField Name="WeeklyHoliday" Type="Int" />
                                <ext:ModelField Name="PublicHoliday" Type="Int" />
                                <ext:ModelField Name="LeaveDays" Type="Int" />
                                <ext:ModelField Name="NetWorkDays" Type="Int" />
                                <ext:ModelField Name="ActivitySubmitted" Type="Int" />
                                <ext:ModelField Name="ActivityNotSubmitted" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column3" Sortable="true" MenuDisabled="true" runat="server" Header="Name"
                        Align="Left" Width="180" DataIndex="Name">
                        <%--<Renderer Fn="myRenderer1" />--%>
                    </ext:Column>
                    <ext:Column ID="Column2" Sortable="true" MenuDisabled="true" runat="server" Header="EIN"
                        Align="Center" Width="60" DataIndex="EmployeeId">
                    </ext:Column>

                    <ext:Column ID="Column4" Sortable="true" MenuDisabled="true" runat="server" Header="Month days"
                        Align="Center" Width="110" DataIndex="MonthDays">
                    </ext:Column>
                    <ext:Column ID="Column5" Sortable="true" MenuDisabled="true" runat="server" Header="Weekly holidays"
                        Align="Center" Width="110" DataIndex="WeeklyHoliday">
                    </ext:Column>
                    <ext:Column ID="Column6" Sortable="true" MenuDisabled="true" runat="server" Header="Public holidays"
                        Align="Center" Width="110" DataIndex="PublicHoliday">
                    </ext:Column>
                    <ext:Column ID="Column7" Sortable="true" MenuDisabled="true" runat="server" Header="Leave days"
                        Align="Center" Width="110" DataIndex="LeaveDays">
                    </ext:Column>
                    <ext:Column ID="Column8" Sortable="true" MenuDisabled="true" runat="server" Header="Net work days"
                        Align="Center" Width="110" DataIndex="NetWorkDays">
                    </ext:Column>
                    <ext:Column ID="Column9" Sortable="true" MenuDisabled="true" runat="server" Header="Activity submitted"
                        Align="Center" Width="120" DataIndex="ActivitySubmitted">
                        <Renderer Fn="linkRenderer" />
                    </ext:Column>
                    <ext:Column ID="Column10" Sortable="true" MenuDisabled="true" runat="server" Header="Activity not submitted"
                        Align="Center" Width="140" DataIndex="ActivityNotSubmitted">
                        <Renderer Fn="linkRenderer" />
                    </ext:Column>

                    
                    
                </Columns>
            </ColumnModel>
            <%--<View>
                <ext:GridView ID="GridPanel1" runat="server">
                    <GetRowClass Fn="applyRowBackground" />
                </ext:GridView>
            </View>
            --%>
        </ext:GridPanel>
      
        <ext:Window ID="wActivityDetails" Width="700" Height="550" Title="Activity Status" OverflowY="Scroll"
        Closable="true" Resizable="false" Hidden="true" runat="server" AutoScroll="true">
            <Content>
                <div class="windowContentWrapper">
                    <table style="margin-left: 20px; margin-top:20px;">
                        <tr>
                            <td colspan="2">On activity submitted and not submitted</td>
                        </tr>
                        <tr>
                            <td><ext:Label ID="lblName" runat="server" MarginSpec="20 0 0 0" StyleSpec="font-weight:bold;" /></td>
                            <td>
                                <ext:LinkButton ID="btnDetailsExport" Icon="PageExcel" runat="server" Text="Export" MarginSpec="20 0 0 120"
                                    OnClick="btnDetailsExport_Click" Hidden="false" AutoPostBack="true">
                                </ext:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ext:GridPanel ID="gridActivityDetails" runat="server"
                                    Cls="itemgrid" Scroll="Both" Width="500" MarginSpec="20 0 0 0" >
                                    <Store>
                                        <ext:Store ID="Store2" runat="server">
                                            <Model>
                                                <ext:Model ID="Model1" runat="server" IDProperty="Day">
                                                    <Fields>
                                                        <ext:ModelField Name="Day" Type="Int" />
                                                        <ext:ModelField Name="DateEng" Type="Date" />
                                                        <ext:ModelField Name="Status" Type="String" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <ColumnModel>
                                        <Columns>
                                            <ext:Column ID="Column1" Sortable="true" MenuDisabled="true" runat="server" Header="Day"
                                                Align="Center" Width="150" DataIndex="Day">
                                            </ext:Column>
                                            <ext:DateColumn ID="Column12" Format="yyyy-MMM-dd" Sortable="true" MenuDisabled="true" runat="server" Header="Date"
                                                Align="Center" Width="150" DataIndex="DateEng">
                                            </ext:DateColumn>
                                            <ext:Column ID="Column11" Sortable="true" MenuDisabled="true" runat="server" Header="Status"
                                                Align="Center" Width="150" DataIndex="Status">
                                                <Renderer Fn="StatusRenderer" />
                                            </ext:Column>
                    
                                        </Columns>
                                    </ColumnModel>
                                </ext:GridPanel>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                </div>
            </Content>
        </ext:Window>


    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>