﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL.Manager;
using BLL;
using DAL;
using Ext.Net;
using Utils.Web;
using Utils.Helper;
using System.Text;
using Newtonsoft.Json.Linq;
using BLL.BO;

namespace Web.NewHR
{
    public partial class EMILoanImport : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
                Initialise();

            JavascriptHelper.AttachNonDialogPopUpCode(Page, "shiftPopup", "../ExcelWindow/EMILoanImportExcel.aspx", 450, 500);


            // programatic client-side calls to __doPostBack() can bypass this
            Page.ClientScript.RegisterOnSubmitStatement(GetType(), "ServerForm",
                                                    "if (this.submitted) return false; this.submitted = true; return true;");
        }

        private void Initialise()
        {
            BindComboLoan();
            BindList();
        }

        private void BindComboLoan()
        {
            cmbLoan.Store[0].DataSource = LoanManager.GetEMILoanList();
            cmbLoan.Store[0].DataBind();
        }

        private void BindList()
        {
            if (cmbLoan.SelectedItem == null || string.IsNullOrEmpty(cmbLoan.SelectedItem.Value))
                return;

            gridLoanRepayment.GetStore().DataSource = LoanManager.GetSILoanImportList(int.Parse(cmbLoan.SelectedItem.Value));
            gridLoanRepayment.GetStore().DataBind();
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            BindList();
        }
    }
}