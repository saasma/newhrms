﻿<%@ Page Title="Employee Current Pay Salary" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeeCurrentPaySalary.aspx.cs" Inherits="Web.CP.EmployeeCurrentPaySalary" %>

<%@ MasterType VirtualPath="~/Master/NewDetails.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

 
   
    function SelEmpSearch()
    {
        <%=hdnEmpSearch.ClientID %>.setValue(<%=cmbEmpSearch.ClientID %>.getValue());
    }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <ext:Hidden ID="hdnEmployeeId" runat="server" />
    <ext:Hidden ID="hdnBranchDepartmentId" runat="server" />
    <ext:Hidden ID="hdnMsg" runat="server" />
    <ext:Hidden ID="hdnEmpSearch" runat="server" />
    <ext:Hidden ID="hdnSortBy" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee and Current Pay
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="margin-top: 0px !important; padding-top: 0px !important;">
        <div class="innerLR">
            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                <Proxy>
                    <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                        <ActionMethods Read="GET" />
                        <Reader>
                            <ext:JsonReader Root="plants" TotalProperty="total" />
                        </Reader>
                    </ext:AjaxProxy>
                </Proxy>
                <Model>
                    <ext:Model ID="Model5" IDProperty="EmployeeId" Name="ItemLineModel" runat="server">
                        <Fields>
                            <ext:ModelField Name="Name" Type="String" />
                            <ext:ModelField Name="EmployeeId" Type="String" />
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
            <div class="alert alert-info" style="margin-top: 10px; padding-top: 0px!important;">
                <ext:Container ID="listingContainer" runat="server">
                    <Content>
                        <table>
                            <tr>
                                <td>
                                    <ext:ComboBox Width="140" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                        LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                                        runat="server" FieldLabel="Branch">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store5" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="BranchId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store6" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DepartmentId" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                                        Width="140" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                                        LabelSeparator="" LabelAlign="Top">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="storeLevel" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelLevel" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="LevelId" Type="Int" />
                                                            <ext:ModelField Name="Name" Type="String" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                        Width="140" MarginSpec="0 5 0 5" runat="server" ValueField="DesignationId" DisplayField="LevelAndDesignation"
                                        LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                        <Items>
                                            <ext:ListItem Index="0" Value="-1" Text="All">
                                            </ext:ListItem>
                                        </Items>
                                        <Store>
                                            <ext:Store ID="Store7" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model9" IDProperty="DesignationId" runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="DesignationId" />
                                                            <ext:ModelField Name="LevelAndDesignation" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </td>
                                <td style="width: 310px;">
                                    <ext:ComboBox LabelSeparator="" ID="cmbEmpSearch" FieldLabel="Search Employee" EmptyText="Employee Name"
                                        LabelWidth="80" LabelAlign="Top" runat="server" DisplayField="Name" ValueField="EmployeeId"
                                        StoreID="storeSearch" TypeAhead="false" Width="250" PageSize="9999" HideBaseTrigger="true"
                                        MinChars="1" TriggerAction="All" ForceSelection="false">
                                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                <Html>
                                                    <tpl>
                                                        <div class="search-item">
                                                                        <span>{Name}</span>  
                                                         </div>
					                            </tpl>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Triggers>
                                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                        </Triggers>
                                        <Listeners>
                                            <Select Handler="this.getTrigger(0).show(); SelEmpSearch();" />
                                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                            <TriggerClick Handler="if (index == 0) { 
                                                       this.clearValue(); 
                                                       this.getTrigger(0).hide();
                                                   }" />
                                        </Listeners>
                                    </ext:ComboBox>
                                </td>
                                <td>
                                    <ext:Button ID="btnLoad" runat="server" Text="Load" Width="100" Cls="btn btn-default"
                                        MarginSpec="25 10 10 10">
                                        <Listeners>
                                            <Click Fn="searchList">
                                            </Click>
                                        </Listeners>
                                    </ext:Button>
                                </td>
                                <td style='padding-top:14px;'>
                                    <asp:LinkButton ID="LinkButton1" class="btn btn-success btn-sm btn-sect" runat="server"
                        Text="Select Incomes" Style="height: 30px; width: 150px; margin-bottom: 0px;
                        margin-left: 0px;" OnClientClick="insertUpdate();return false;" />
                                </td>
                            </tr>
                        </table>
                    </Content>
                </ext:Container>
            </div>
            <ext:KeyMap ID="KeyMap1" runat="server" Target="={#{listingContainer}.getBody()}">
                <Binding>
                    <ext:KeyBinding Handler="#{btnLoad}.fireEvent('click');">
                        <Keys>
                            <ext:Key Code="ENTER" />
                        </Keys>
                    </ext:KeyBinding>
                </Binding>
            </ext:KeyMap>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridBranchTransfer" runat="server"
                 OnReadData="Store_ReadData"  AutoScroll="true">
                <Store>
                    <ext:Store PageSize="25" ID="storeEmpList" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="EmployeeId">
                                <Fields>
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="IDCardNo" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="Position" Type="String" />
                                    <ext:ModelField Name="Designation" Type="String" />
                                    <ext:ModelField Name="AccountNo" Type="String" />
                                    <ext:ModelField Name="Income1" Type="String" />
                                    <ext:ModelField Name="Income2" Type="String" />
                                    <ext:ModelField Name="Income3" Type="String" />
                                    <ext:ModelField Name="Income4" Type="String" />
                                    <ext:ModelField Name="Income5" Type="String" />
                                    <ext:ModelField Name="Income6" Type="String" />
                                    <ext:ModelField Name="Income7" Type="String" />
                                    <ext:ModelField Name="Income8" Type="String" />
                                    <ext:ModelField Name="Income9" Type="String" />
                                    <ext:ModelField Name="Income10" Type="String" />
                                    <ext:ModelField Name="Total" Type="String" />
                                    <ext:ModelField Name="JoinDate" Type="Date" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="EmployeeId" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colFromDate" runat="server" Text="SN" Locked="true"  DataIndex="RowNumber" MenuDisabled="false"
                            Sortable="false" Align="Right" Width="50" />
                        <ext:Column ID="Column1" runat="server" Text="EIN" Locked="true"  DataIndex="EmployeeId" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="50" />
                        <ext:Column ID="Column2" runat="server" Text="I No" Locked="true"  DataIndex="IDCardNo" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="50" />
                        <ext:Column ID="Column3" runat="server" Text="Name" Locked="true" DataIndex="Name" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="150" />
                        <ext:Column ID="Column4" runat="server" Text="Branch" DataIndex="Branch" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column5" runat="server" Text="Department" DataIndex="Department"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column6" runat="server" Text="Position" DataIndex="Position" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="Column7" runat="server" Text="Designation" DataIndex="Designation"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:DateColumn ID="colFromDateEng" runat="server" Text="Join Date" DataIndex="JoinDate"
                            Format="yyyy-MMM-dd" MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                        <ext:Column ID="Column8" runat="server" Text="Account No" DataIndex="AccountNo" MenuDisabled="false"
                            Sortable="true" Align="Left" Width="120" />
                        <ext:Column ID="income1" runat="server" Text="" DataIndex="Income1" MenuDisabled="false"
                            Sortable="true" Align="Center" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="income2" runat="server" Text="" DataIndex="Income2" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="income3" runat="server" Text="" DataIndex="Income3" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="income4" runat="server" Text="" DataIndex="Income4" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="income5" runat="server" Text="" DataIndex="Income5" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Income6" runat="server" Text="" DataIndex="Income6" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Income7" runat="server" Text="" DataIndex="Income7" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Income8" runat="server" Text="" DataIndex="Income8" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Income9" runat="server" Text="" DataIndex="Income9" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Income10" runat="server" Text="" DataIndex="Income10" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                        <ext:Column ID="Column13" runat="server" Text="Total" DataIndex="Total" MenuDisabled="false"
                            Sortable="true" Align="Right" Width="100">
                            <Renderer Fn="getFormattedAmount" />
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
        <div class="alert alert-info" style="margin-top: 15px; height: 50px;">
            <div style="float: right;">
                <ext:Button runat="server" AutoPostBack="true" OnClick="btnExport_Click" ID="btnExport"
                    Text="<i></i>Export To Excel">
                </ext:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
