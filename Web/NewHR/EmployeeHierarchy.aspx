﻿<%@ Page Title="Employee Hierarchy" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmployeeHierarchy.aspx.cs" Inherits="Web.NewHR.EmployeeHierarchy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">



<style type="text/css">

   .treepanelCls
   {
       margin-left:80px;
       margin-top:50px;
   }
   .groupCls
   {
       background-color:Blue;
   }
   
    .BlueCls .x-grid-cell
   {
       background-color:#157FCC !important;
       color:White;
   }
   
    .MediumBlueCls .x-grid-cell
   {
       background-color:#47AAE3 !important;
       color:White;
   }
   .LightBlue .x-grid-cell
   {
       background-color:#DFEAF2 !important;
       color:black !important;
   }
</style>

<script type="text/javascript">


    var applyRowBackground = function (record, rowIndex, rowParams, store) {
        return record.data.CssClass;
    };

</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">


<div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Hierarchy
                </h4>
            </div>
        </div>
    </div>

    <div class="contentpanel">
        
            <div class="separator bottom">
            </div>

            <table>
            <tr>
                <td>
                   
                </td>
                <td style="width:200px;">
                    <ext:ComboBox ID="cmbBranch" runat="server" ValueField="BranchId" DisplayField="Name" FieldLabel="Branch Filter" Width="180"
                        LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                        <Store>
                            <ext:Store ID="storeBranch" runat="server">
                                <Model>
                                    <ext:Model ID="Model8" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="BranchId" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <DirectEvents>
                            <Change OnEvent="cmbBranch_Change">
                                <EventMask ShowMask="true" />
                            </Change>
                        </DirectEvents>
                                      
                    </ext:ComboBox>
                </td>

                <td style="width:200px;">                  

                    <ext:ComboBox ID="cmbDepartment" runat="server" ValueField="DepartmentId" DisplayField="Name" FieldLabel="Department Filter" Width="180" 
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store7" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="DepartmentId" Type="String" />
                                                <ext:ModelField Name="Name" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Change OnEvent="cmbDepartment_Change">
                                    <EventMask ShowMask="true" />
                                </Change>     
                            </DirectEvents>  
                        </ext:ComboBox>

                </td>

                <td>
                    <ext:ComboBox ID="cmbSortBy" runat="server" FieldLabel="Sort By" Width="180"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="Branch" Value="Branch" />
                                <ext:ListItem Text="Department" Value="Department" />
                            </Items>  
                            <SelectedItems>
                                <ext:ListItem Text="Branch" Value="Branch" />
                            </SelectedItems> 
                             <DirectEvents>
                                <Change OnEvent="cmbSortBy_Change">
                                    <EventMask ShowMask="true" />
                                </Change>     
                            </DirectEvents>  
                        </ext:ComboBox>
                </td>
                
            </tr>
         
            
        </table>

       <br />

       

            <ext:TreePanel MultiSelect="true" Icon="None" ID="treeEmployeeHierarchy" RootVisible="false" Layout="AbsoluteLayout" Width="1010"
                    AutoScroll="true" Animate="true" Mode="Local" runat="server" Collapsible="false"
                    UseArrows="true"  Title="Employee Hierarchy">
                    <Fields>
                        <ext:ModelField Name="Name" />
                        <ext:ModelField Name="Branch" />
                        <ext:ModelField Name="Department" />
                        <ext:ModelField Name="JoinDate" Type="String" />
                        <ext:ModelField Name="RetirementEligibleDate" Type="String" />
                        <ext:ModelField Name="CssClass" Type="String" />
                    </Fields>
                    <ColumnModel>
                        <Columns>
                            <ext:TreeColumn ID="TreeColumn1" Width="300" runat="server" Text="" DataIndex="text" MenuDisabled="true" Sortable="false"
                                Flex="1">
                            </ext:TreeColumn>
                            
                            <ext:Column ID="colBranch" Width="200" runat="server" Flex="1" MenuDisabled="true" Sortable="false" Text="Branch"
                                DataIndex="Branch" />
                            <ext:Column ID="colDepartment" Width="200" runat="server" Flex="1" MenuDisabled="true" Sortable="false" Text="Department"
                                DataIndex="Department" />
                          
                            <ext:DateColumn ID="colJoinDate1" runat="server" Align="Left" Text="Join Date"
                                MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="JoinDate"
                                Resizable="false" Draggable="false" Width="150">
                            </ext:DateColumn>

                            <ext:DateColumn ID="colRetirementEligibleDate1" runat="server" Align="Left" Text="Retirement Eligible Date"
                                MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="RetirementEligibleDate"
                                Resizable="false" Draggable="false" Width="160">
                            </ext:DateColumn>
                            
                        </Columns>
                    </ColumnModel>
                    <%-- <Listeners>
                        <beforenodedrop fn="beforenodedrop" />
                    </Listeners>--%>
                    <Listeners>
                    </Listeners>
                    <View>
                        <ext:TreeView ID="TreeView1" runat="server">
                            <%--<Plugins>
                                <ext:TreeViewDragDrop ID="TreeViewDragDrop1" EnableDrag="true" EnableDrop="false"
                                    runat="server" AppendOnly="true" />
                            </Plugins>--%>
                            <GetRowClass Fn="applyRowBackground" />
                        </ext:TreeView>
                    </View>
                </ext:TreePanel>
    
         
       
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
