﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using BLL.Manager;
using BLL;
using Web.Master;
using BLL.Base;
using DAL;
using Bll;

namespace Web.NewHR
{
    public partial class GenerateAttendanceReportData : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.LeaveAndAttendance;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {


            HR master = this.Master as HR;
            if (master != null)
                master.DisableViewState = true;

            if (!IsPostBack && !X.IsAjaxRequest)
            {
                DispDate();
            }
            
        }

        public void DispDate()
        {
            List<AttendanceEmployeeDate> dates = AttendanceManager.GetAttendanceProcessedLastDate();
            grid.Store[0].DataSource = dates;
            grid.Store[0].DataBind();
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            int empId = -1;
            DateTime start, end;

            if (String.IsNullOrEmpty(txtFromDate.Text))
            {
                NewMessage.ShowWarningMessage("From date is required.");
                return;
            }

            if (String.IsNullOrEmpty(txtToDate.Text))
            {
                NewMessage.ShowWarningMessage("To date is required.");
                return;
            }

            start = txtFromDate.SelectedDate.Date;
            end = txtToDate.SelectedDate.Date;


            if (start > end)
            {
                NewMessage.ShowWarningMessage("From date can not be greater than To date.");
                return;
            }


            if(cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
            {
                empId = int.Parse(cmbSearch.SelectedItem.Value);
            }

            AttendanceManager.GenerateAttendanceEmployeeTable(empId, start, end, false);

            NewMessage.ShowNormalMessage("Attendance report regenerated for the given parameters.");

            DispDate();
            return;
        }

    }
}