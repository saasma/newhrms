﻿<%@ Page Title="Activity Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="NewActivityReport.aspx.cs" Inherits="Web.NewHR.NewActivityReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">
     /* A header Checkbox of CheckboxSelectionModel deals with the current page only.
     This override demonstrates how to take into account all the pages.
     It works with local paging only. It is not going to work with remote paging.
     */
     Ext.selection.CheckboxModel.override({
         selectAll: function (suppressEvent) {
             var me = this,
                    selections = me.store.getAllRange(), // instead of the getRange call
                    i = 0,
                    len = selections.length,
                    start = me.getSelection().length;

             me.suspendChanges();

             for (; i < len; i++) {
                 me.doSelect(selections[i], true, suppressEvent);
             }

             me.resumeChanges();
             if (!suppressEvent) {
                 me.maybeFireSelectionChange(me.getSelection().length !== start);
             }
         },

         deselectAll: Ext.Function.createSequence(Ext.selection.CheckboxModel.prototype.deselectAll, function () {
             this.view.panel.getSelectionMemory().clearMemory();
         }),

         updateHeaderState: function () {
             var me = this,
                    store = me.store,
                    storeCount = store.getTotalCount(),
                    views = me.views,
                    hdSelectStatus = false,
                    selectedCount = 0,
                    selected, len, i;

             if (!store.buffered && storeCount > 0) {
                 selected = me.view.panel.getSelectionMemory().selectedIds;
                 hdSelectStatus = true;
                 for (s in selected) {
                     ++selectedCount;
                 }

                 hdSelectStatus = storeCount === selectedCount;
             }

             if (views && views.length) {
                 me.toggleUiHeader(hdSelectStatus);
             }
         }
     });

     Ext.grid.plugin.SelectionMemory.override({
         memoryRestoreState: function (records) {
             if (this.store !== null && !this.store.buffered && !this.grid.view.bufferedRenderer) {
                 var i = 0,
                        ind,
                        sel = [],
                        len,
                        all = true,
                        cm = this.headerCt;

                 if (!records) {
                     records = this.store.getAllRange(); // instead of getRange
                 }

                 if (!Ext.isArray(records)) {
                     records = [records];
                 }

                 if (this.selModel.isLocked()) {
                     this.wasLocked = true;
                     this.selModel.setLocked(false);
                 }

                 if (this.selModel instanceof Ext.selection.RowModel) {
                     for (ind = 0, len = records.length; ind < len; ind++) {
                         var rec = records[ind],
                                id = rec.getId();

                         if ((id || id === 0) && !Ext.isEmpty(this.selectedIds[id])) {
                             sel.push(rec);
                         } else {
                             all = false;
                         }

                         ++i;
                     }

                     if (sel.length > 0) {
                         this.surpressDeselection = true;
                         this.selModel.select(sel, false, !this.grid.selectionMemoryEvents);
                         this.surpressDeselection = false;
                     }
                 } else {
                     for (ind = 0, len = records.length; ind < len; ind++) {
                         var rec = records[ind],
                                id = rec.getId();

                         if ((id || id === 0) && !Ext.isEmpty(this.selectedIds[id])) {
                             if (this.selectedIds[id].dataIndex) {
                                 var colIndex = cm.getHeaderIndex(cm.down('gridcolumn[dataIndex=' + this.selectedIds[id].dataIndex + ']'))
                                 this.selModel.setCurrentPosition({
                                     row: i,
                                     column: colIndex
                                 });
                             }
                             return false;
                         }

                         ++i;
                     }
                 }

                 if (this.selModel instanceof Ext.selection.CheckboxModel) {
                     if (all) {
                         this.selModel.toggleUiHeader(true);
                     } else {
                         this.selModel.toggleUiHeader(false);
                     }
                 }

                 if (this.wasLocked) {
                     this.selModel.setLocked(true);
                 }
             }
         }
     });


     var prepareActivity = function (grid, toolbar, rowIndex, record) {
         var markButton = toolbar.items.get(0);
         if (record.data.Status == 3) {
             markButton.setVisible(false);
            
         }
     }

     var prepareDownloadActivity = function (grid, toolbar, rowIndex, record) {
         var downloadBtn = toolbar.items.get(1);
         if (record.data.ContainsFile == 0) {
             downloadBtn.setVisible(false);
         }
     }

     var CommandDownload = function(command, record){
            <%= hdnActivityId.ClientID %>.setValue(record.data.ActivityId);
            <%= hdnAcitivityType.ClientID %>.setValue(record.data.ActivityType);
                
            <%= btnDownload.ClientID %>.fireEvent('click');
           }


    var CommandHandler = function(command, record){
            <%= hdnActivityId.ClientID %>.setValue(record.data.ActivityId);
            <%= hdnAcitivityType.ClientID %>.setValue(record.data.ActivityType);
                if(command=="MarkRead")
                {
                    <%= btnMarkRead.ClientID %>.fireEvent('click');
                }
           }


    function ActivityTypeSelect()
    {
        <%= btnClientNameSelect.ClientID %>.fireEvent('click');
              
    }

    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden runat="server" ID="hdnActivityId" />
<ext:Hidden runat="server" ID="hdnAcitivityType" />


<ext:LinkButton ID="btnDownload" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnDownload_Click">
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:LinkButton ID="btnMarkRead" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnMarkRead_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>

<ext:LinkButton ID="btnClientNameSelect" runat="server" Hidden="true">
    <DirectEvents>
        <Click OnEvent="btnClientNameSelect_Click">
            <EventMask ShowMask="true" />
        </Click>
    </DirectEvents>
</ext:LinkButton>


    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Activity Report
                </h4>
            </div>
        </div>
    </div>

    
       <div class="contentpanel">

        <table>
            <tr>
                <td>
                    <pr:CalendarExtControl Width="180" FieldLabel="Start Date" ID="txtStartDate" runat="server"
                        LabelAlign="Top" LabelSeparator="">                     
                    </pr:CalendarExtControl>
                </td>
                <td style="width:200px;">
                    <pr:CalendarExtControl Width="180" FieldLabel="End Date" ID="txtEndDate" runat="server"
                        LabelAlign="Top" LabelSeparator="">                     
                    </pr:CalendarExtControl>
                </td>

                <td>
                    <ext:ComboBox ID="cmbActivityType" runat="server" FieldLabel="Activity Type" Width="180"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="All" Value="0" />
                                <ext:ListItem Text="Client Visit" Value="1" />
                                <ext:ListItem Text="Software Testing" Value="2" />
                                <ext:ListItem Text="Documentation" Value="3" />
                                <ext:ListItem Text="Remote Support" Value="4" />
                                <ext:ListItem Text="Other" Value="5" />
                            </Items>   
                            <Listeners>
                                <Select Fn="ActivityTypeSelect" />
                            </Listeners>
                        </ext:ComboBox>
                </td>
                <td style="width:200px;">

                    <ext:ComboBox ID="cmbClient" runat="server" ValueField="ClientId" DisplayField="ClientName" FieldLabel="Select Client" Width="180" Disabled="true"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store8" runat="server">
                                    <Model>
                                        <ext:Model ID="Model8" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="ClientId" Type="String" />
                                                <ext:ModelField Name="ClientName" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>


                    <ext:ComboBox ID="cmbSoftware" runat="server" ValueField="SoftwareId" DisplayField="SoftwareName" FieldLabel="Select Software" Width="180" Disabled="true" Hidden="true"
                            LabelAlign="top" LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store7" runat="server">
                                    <Model>
                                        <ext:Model ID="Model7" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="SoftwareId" Type="String" />
                                                <ext:ModelField Name="SoftwareName" Type="String" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>

                </td>

               <td style="width:220px;">
                    <ext:TextField ID="txtEmployeeName" LabelSeparator="" runat="server" FieldLabel="Employee Name"
                        Width="200" LabelAlign="Top">
                    </ext:TextField>
                </td>
                <td style="padding-top:14px;">
                    <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Load" Width="120">
                        <DirectEvents>
                            <Click OnEvent="btnLoad_Click">                               
                                <EventMask ShowMask="true" />                              
                            </Click>
                        </DirectEvents>
                    </ext:Button>                       
                </td>
            </tr>
            <tr>
                <td style="padding-top:10px;">
                    <ext:Button ID="btnMarkAllAsRead" runat="server" Cls="btn btn-save" Text="Mark all as Read" Width="130">
                        <DirectEvents>
                            <Click OnEvent="btnMarkAllAsRead_Click">                               
                                <EventMask ShowMask="true" />
                                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to mark the activities as read?" />
                                    <ExtraParams>
                                        <ext:Parameter Name="gridItemsCV" Value="Ext.encode(#{gridClientVisit}.getRowsValues({selectedOnly:true}))"
                                            Mode="Raw" />
                                        <ext:Parameter Name="gridItemsST" Value="Ext.encode(#{gridSoftwareTesting}.getRowsValues({selectedOnly:true}))"
                                            Mode="Raw" />
                                        <ext:Parameter Name="gridItemsDoc" Value="Ext.encode(#{gridDocumentation}.getRowsValues({selectedOnly:true}))"
                                            Mode="Raw" />
                                        <ext:Parameter Name="gridItemsRS" Value="Ext.encode(#{gridRemoteSupport}.getRowsValues({selectedOnly:true}))"
                                            Mode="Raw" />
                                        <ext:Parameter Name="gridItemsOther" Value="Ext.encode(#{gridOther}.getRowsValues({selectedOnly:true}))"
                                            Mode="Raw" />
                                    </ExtraParams>                             
                            </Click>
                        </DirectEvents>
                    </ext:Button>      
                </td>
                
            </tr>
            
        </table>


        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridClientVisit" runat="server" Cls="itemgrid" Scroll="None" Title="Client Visit" >
            <Store>
                <ext:Store ID="Store1" runat="server">
                    <Model>
                        <ext:Model ID="Model4" runat="server" IDProperty="ActivityId">
                            <Fields>
                                <ext:ModelField Name="ActivityId" Type="String" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="ClientName" Type="string" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="DurationTime" Type="string" />
                                <ext:ModelField Name="Representative" Type="string" />
                                <ext:ModelField Name="Issue" Type="string" />
                                <ext:ModelField Name="Result" Type="string" />
                                <ext:ModelField Name="NextPlannedVisitEng" Type="Date" />
                                <ext:ModelField Name="Status" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column3" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                        Align="Left" Width="150" DataIndex="EmployeeName" />
                    <ext:Column ID="colClientName" Sortable="false" MenuDisabled="true" runat="server" Text="Client Name"
                        Align="Left" Width="150" DataIndex="ClientName" />
                    <ext:DateColumn ID="colDate" runat="server" Align="Right" Text="Date" Width="90"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="colStart" Sortable="false" MenuDisabled="true" runat="server" Text="Start" Width="75"
                        Align="Right" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="colEndTime" Sortable="false" MenuDisabled="true" runat="server" Text="End" Width="75"
                        Align="Right" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="Column10" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Center" Width="70" DataIndex="DurationTime">
                    </ext:Column>
                    <ext:Column ID="Column7" Sortable="false" MenuDisabled="true" runat="server" Text="Representative"
                        Align="Left" Width="150" DataIndex="Representative">
                    </ext:Column>
                     <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Issue"
                        Align="Left" Width="150" DataIndex="Issue">
                    </ext:Column>
                    <ext:Column ID="Column2" Sortable="false" MenuDisabled="true" runat="server" Text="Result"
                        Align="Left" Width="135" DataIndex="Result">
                    </ext:Column>
                    <ext:DateColumn ID="DateColumn1" runat="server" Align="Right" Text="Next Pl. Visit" Width="90"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="NextPlannedVisitEng">
                    </ext:DateColumn>
                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="90" Text="Actions" Align="Center">
                        <Commands>
                            <ext:GridCommand Text="Mark as Read" CommandName="MarkRead" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi" />
            </SelectionModel>
        </ext:GridPanel>

        <br />

        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridSoftwareTesting" runat="server" Cls="itemgrid" Scroll="None" Title="Software Testing" >
            <Store>
                <ext:Store ID="Store2" runat="server">
                    <Model>
                        <ext:Model ID="Model1" runat="server" IDProperty="ActivityId">
                            <Fields>
                                <ext:ModelField Name="ActivityId" Type="String" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="SoftwareName" Type="string" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="DurationTime" Type="string" />
                                <ext:ModelField Name="Description" Type="string" />
                                <ext:ModelField Name="Result" Type="string" />
                                <ext:ModelField Name="Issue" Type="string" />                                
                                <ext:ModelField Name="ContainsFile" Type="Int" />
                                <ext:ModelField Name="Status" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column4" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                        Align="Left" Width="150" DataIndex="EmployeeName" />
                    <ext:Column ID="Column5" Sortable="false" MenuDisabled="true" runat="server" Text="Software Name"
                        Align="Left" Width="150" DataIndex="SoftwareName" />
                    <ext:DateColumn ID="DateColumn2" runat="server" Align="Right" Text="Date" Width="90"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column6" Sortable="false" MenuDisabled="true" runat="server" Text="Start" Width="75"
                        Align="Right" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="End" Width="75"
                        Align="Right" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Center" Width="70" DataIndex="DurationTime">
                    </ext:Column>
                    <ext:Column ID="Column11" Sortable="false" MenuDisabled="true" runat="server" Text="Module Tested"
                        Align="Left" Width="150" DataIndex="Description">
                    </ext:Column>
                     <ext:Column ID="Column12" Sortable="false" MenuDisabled="true" runat="server" Text="Result"
                        Align="Left" Width="150" DataIndex="Result">
                    </ext:Column>
                    <ext:Column ID="Column13" Sortable="false" MenuDisabled="true" runat="server" Text="Issue"
                        Align="Left" Width="135" DataIndex="Issue">
                    </ext:Column>
                    <ext:CommandColumn ID="CCSTDownload" runat="server" Width="40" Sortable="false"
                            MenuDisabled="true" Align="Center">
                            <Commands>
                                <ext:CommandSeparator />
                                <ext:GridCommand Icon="PageWhitePut" CommandName="Download" ToolTip-Text="Download attachment" />
                            </Commands>
                            <PrepareToolbar Fn="prepareDownloadActivity" />
                            <Listeners>
                                <Command Handler="CommandDownload(command,record);" />
                            </Listeners>
                        </ext:CommandColumn>
                    <ext:CommandColumn ID="CommandColumn2" runat="server" Width="90" Text="Actions" Align="Center">
                        <Commands>
                            <ext:GridCommand Text="Mark as Read" CommandName="MarkRead" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel2" runat="server" Mode="Multi" />
            </SelectionModel>
        </ext:GridPanel>

        <br />

         <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridDocumentation" runat="server" Cls="itemgrid" Scroll="None" Title="Documentation" >
            <Store>
                <ext:Store ID="Store3" runat="server">
                    <Model>
                        <ext:Model ID="Model2" runat="server" IDProperty="ActivityId">
                            <Fields>
                                <ext:ModelField Name="ActivityId" Type="String" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="SoftwareName" Type="string" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="DurationTime" Type="string" />
                                <ext:ModelField Name="Issue" Type="string" />     
                                <ext:ModelField Name="Description" Type="string" />
                                <ext:ModelField Name="Status" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column14" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                        Align="Left" Width="150" DataIndex="EmployeeName" />
                    <ext:Column ID="Column15" Sortable="false" MenuDisabled="true" runat="server" Text="Softwares Name"
                        Align="Left" Width="150" DataIndex="SoftwareName" />
                    <ext:DateColumn ID="DateColumn3" runat="server" Align="Right" Text="Date" Width="90"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column16" Sortable="false" MenuDisabled="true" runat="server" Text="Start" Width="75"
                        Align="Right" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="Column17" Sortable="false" MenuDisabled="true" runat="server" Text="End" Width="75"
                        Align="Right" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="Column18" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Center" Width="70" DataIndex="DurationTime">
                    </ext:Column>
                    <ext:Column ID="Column19" Sortable="false" MenuDisabled="true" runat="server" Text="Module Documented"
                        Align="Left" Width="150" DataIndex="Issue">
                    </ext:Column>
                     <ext:Column ID="Column20" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                        Align="Left" Width="150" DataIndex="Description">
                    </ext:Column>                   
                    <ext:CommandColumn ID="CommandColumn4" runat="server" Width="90" Text="Actions" Align="Center">
                        <Commands>
                            <ext:GridCommand Text="Mark as Read" CommandName="MarkRead" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel3" runat="server" Mode="Multi" />
            </SelectionModel>
        </ext:GridPanel>

         <br />

        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridRemoteSupport" runat="server" Cls="itemgrid" Scroll="None" Title="Remote Support" >
            <Store>
                <ext:Store ID="Store4" runat="server">
                    <Model>
                        <ext:Model ID="Model3" runat="server" IDProperty="ActivityId">
                            <Fields>
                                <ext:ModelField Name="ActivityId" Type="String" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="ClientName" Type="string" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="DurationTime" Type="string" />
                                <ext:ModelField Name="Description" Type="string" />
                                <ext:ModelField Name="Issue" Type="string" />     
                                <ext:ModelField Name="Result" Type="string" />   
                                <ext:ModelField Name="NextPlannedVisitEng" Type="Date" />
                                <ext:ModelField Name="Status" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column21" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                        Align="Left" Width="150" DataIndex="EmployeeName" />
                    <ext:Column ID="Column22" Sortable="false" MenuDisabled="true" runat="server" Text="Client Name"
                        Align="Left" Width="150" DataIndex="ClientName" />
                    <ext:DateColumn ID="DateColumn4" runat="server" Align="Right" Text="Date" Width="90"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column23" Sortable="false" MenuDisabled="true" runat="server" Text="Start" Width="75"
                        Align="Right" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="Column24" Sortable="false" MenuDisabled="true" runat="server" Text="End" Width="75"
                        Align="Right" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="Column25" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Center" Width="70" DataIndex="DurationTime">
                    </ext:Column>
                    <ext:Column ID="Column26" Sortable="false" MenuDisabled="true" runat="server" Text="Support Mode"
                        Align="Left" Width="150" DataIndex="Description">
                    </ext:Column>
                     <ext:Column ID="Column27" Sortable="false" MenuDisabled="true" runat="server" Text="Issue"
                        Align="Left" Width="150" DataIndex="Issue">
                    </ext:Column>
                    <ext:Column ID="Column28" Sortable="false" MenuDisabled="true" runat="server" Text="Result"
                        Align="Left" Width="135" DataIndex="Result">
                    </ext:Column>
                    <ext:DateColumn ID="DateColumn5" runat="server" Align="Right" Text="Next Pl. Visit" Width="90"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="NextPlannedVisitEng">
                    </ext:DateColumn>
                    <ext:CommandColumn ID="CommandColumn5" runat="server" Width="90" Text="Actions" Align="Center">
                        <Commands>
                            <ext:GridCommand Text="Mark as Read" CommandName="MarkRead" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel4" runat="server" Mode="Multi" />
            </SelectionModel>
        </ext:GridPanel>

        <br />

        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridOther" runat="server" Cls="itemgrid" Scroll="None" Title="Other" >
            <Store>
                <ext:Store ID="Store5" runat="server">
                    <Model>
                        <ext:Model ID="Model5" runat="server" IDProperty="ActivityId">
                            <Fields>
                                <ext:ModelField Name="ActivityId" Type="String" />
                                <ext:ModelField Name="ActivityType" Type="String" />
                                <ext:ModelField Name="EmployeeName" Type="String" />
                                <ext:ModelField Name="Name" Type="string" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="StartTime" Type="string" />
                                <ext:ModelField Name="EndTime" Type="string" />
                                <ext:ModelField Name="DurationTime" Type="string" />
                                <ext:ModelField Name="Description" Type="string" /> 
                                <ext:ModelField Name="Status" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>
                    <ext:Column ID="Column29" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                        Align="Left" Width="150" DataIndex="EmployeeName" />
                    <ext:Column ID="Column30" Sortable="false" MenuDisabled="true" runat="server" Text="Work Type"
                        Align="Left" Width="150" DataIndex="Name" />
                    <ext:DateColumn ID="DateColumn6" runat="server" Align="Right" Text="Date" Width="90"
                        MenuDisabled="true" Sortable="false" Format="yyyy-MM-dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="Column31" Sortable="false" MenuDisabled="true" runat="server" Text="Start" Width="75"
                        Align="Right" DataIndex="StartTime">
                    </ext:Column>
                    <ext:Column ID="Column32" Sortable="false" MenuDisabled="true" runat="server" Text="End" Width="75"
                        Align="Right" DataIndex="EndTime">
                    </ext:Column>
                    <ext:Column ID="Column33" Sortable="false" MenuDisabled="true" runat="server" Text="Duration"
                        Align="Center" Width="70" DataIndex="DurationTime">
                    </ext:Column>
                     <ext:Column ID="Column35" Sortable="false" MenuDisabled="true" runat="server" Text="Description"
                        Align="Left" Width="150" DataIndex="Description">
                    </ext:Column>                   
                    <ext:CommandColumn ID="CommandColumn3" runat="server" Width="90" Text="Actions" Align="Center">
                        <Commands>
                            <ext:GridCommand Text="Mark as Read" CommandName="MarkRead" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                        <PrepareToolbar Fn="prepareActivity" />
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel5" runat="server" Mode="Multi" />
            </SelectionModel>
        </ext:GridPanel>

         <br />

         <ext:Button runat="server" Cls="btn btn-primary btn-sect" Width="150" StyleSpec="margin-top:10px"
                Height="30" ID="btnSaveAndSend" Text="<i></i>Save And Send">
            <DirectEvents>
                <Click OnEvent="btnSaveAndSend_Click">
                    <EventMask ShowMask="true" />
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to save and send the activities?" />
                    <ExtraParams>
                        <ext:Parameter Name="gridItemsCV" Value="Ext.encode(#{gridClientVisit}.getRowsValues({selectedOnly:true}))"
                            Mode="Raw" />
                        <ext:Parameter Name="gridItemsST" Value="Ext.encode(#{gridSoftwareTesting}.getRowsValues({selectedOnly:true}))"
                            Mode="Raw" />
                        <ext:Parameter Name="gridItemsDoc" Value="Ext.encode(#{gridDocumentation}.getRowsValues({selectedOnly:true}))"
                            Mode="Raw" />
                        <ext:Parameter Name="gridItemsRS" Value="Ext.encode(#{gridRemoteSupport}.getRowsValues({selectedOnly:true}))"
                            Mode="Raw" />
                        <ext:Parameter Name="gridItemsOther" Value="Ext.encode(#{gridOther}.getRowsValues({selectedOnly:true}))"
                            Mode="Raw" />
                    </ExtraParams>
                </Click>
            </DirectEvents>
        </ext:Button>
       
       <br />

</div>


</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
