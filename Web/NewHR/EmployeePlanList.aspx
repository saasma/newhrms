﻿<%@ Page Title="Employee Plan Report" Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true" CodeBehind="EmployeePlanList.aspx.cs" Inherits="Web.NewHR.EmployeePlanList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

 <script type="text/javascript">

    
      var CommandHandler = function(command, record){
            <%= hdnPlanId.ClientID %>.setValue(record.data.PlanId);
                if(command=="View")
                {
                    <%= btnView.ClientID %>.fireEvent('click');
                }
           };

        function searchList() {
            <%=gridPlanList.ClientID %>.getStore().pageSize = <%=cmbPageSize.ClientID %>.getValue();
            <%=PagingToolbar1.ClientID %>.doRefresh();
        }

    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>

    <ext:Hidden runat="server" ID="hdnPlanId" />
    <ext:LinkButton ID="btnView" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnView_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Employee Plan Report
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <table>
            <tr>
                <td style="width: 140px;">
                    <ext:DateField ID="txtStartDate" runat="server" Width="120px" LabelAlign="Top" FieldLabel="From Date"
                        LabelSeparator="">
                        <Plugins>
                            <ext:GenericPlugin ID="HighlightDates1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td style="width: 140px;">
                    <ext:DateField ID="txtEndDate" runat="server" Width="120px" LabelAlign="Top" FieldLabel="To Date"
                        LabelSeparator="">
                        <Plugins>
                            <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                        </Plugins>
                    </ext:DateField>
                </td>
                <td style="width: 220px;">
                    <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                        <Proxy>
                            <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                                <ActionMethods Read="GET" />
                                <Reader>
                                    <ext:JsonReader Root="plants" TotalProperty="total" />
                                </Reader>
                            </ext:AjaxProxy>
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model3" IDProperty="Name" Name="ItemLineModel" runat="server">
                                <Fields>
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    <ext:ComboBox LabelSeparator="" ID="cmbSearch" runat="server" DisplayField="Name"
                        FieldLabel="Employee" LabelAlign="Top" ValueField="EmployeeId" EmptyText="Search Employee"
                        StoreID="storeSearch" TypeAhead="false" Width="200" HideBaseTrigger="true" MinChars="1"
                        TriggerAction="All" ForceSelection="false">
                        <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                            <ItemTpl ID="ItemTpl1" runat="server">
                                <Html>
                                    <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                </Html>
                            </ItemTpl>
                        </ListConfig>
                        <Triggers>
                            <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                        </Triggers>
                        <Listeners>
                            <Select Handler="this.getTrigger(0).show();" />
                            <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                            <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                        </Listeners>
                    </ext:ComboBox>
                </td>
                <td style="padding-top: 14px;">
                    <ext:Button ID="btnLoad" runat="server" Cls="btn btn-save" Text="Load" Width="120">
                        <Listeners>
                            <Click Fn="searchList">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </td>
            </tr>
        </table>
        <br />
        <ext:TabPanel ID="TabPanel1" runat="server" Width="1050">
            <Items>
                <ext:Panel ID="pnlToday" runat="server" Title="Today">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlYesterday" runat="server" Title="Yesterday">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlThisWeek" runat="server" Title="This Week">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlLastWeek" runat="server" Title="Last Week">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlThisMonth" runat="server" Title="This Month">
                    <Content>
                    </Content>
                </ext:Panel>
                <ext:Panel ID="pnlAll" runat="server" Title="All">
                    <Content>
                    </Content>
                </ext:Panel>
            </Items>
            <Listeners>
                <TabChange Fn="searchList" />
            </Listeners>
        </ext:TabPanel>
        <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridPlanList" runat="server" Cls="itemgrid"
            Scroll="None" Width="1050">
            <Store>
                <ext:Store ID="storePlan" runat="server" AutoLoad="true" OnReadData="Store_ReadData"
                    RemotePaging="true" RemoteSort="true">
                    <Proxy>
                        <ext:PageProxy />
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model6" runat="server" IDProperty="PlanId">
                            <Fields>
                                <ext:ModelField Name="PlanId" Type="Int" />
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="DateEng" Type="Date" />
                                <ext:ModelField Name="DetailCount" Type="Int" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>
            <ColumnModel>
                <Columns>                    
                   <ext:DateColumn ID="colDateEng" runat="server" Align="Left" Text="Date" Width="150"
                        MenuDisabled="true" Sortable="false" Format="yyyy/MM/dd" DataIndex="DateEng">
                    </ext:DateColumn>
                    <ext:Column ID="colName" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                        Width="300" Align="Left" DataIndex="Name">
                    </ext:Column>
                    <ext:Column ID="colCount" Sortable="false" MenuDisabled="true" runat="server" Text="No of Plans"
                        Width="500" Align="Center" DataIndex="DetailCount">
                    </ext:Column>
                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="100" Text="" Align="Center">
                        <Commands>
                            <ext:GridCommand Text="View" CommandName="View" />
                        </Commands>
                        <Listeners>
                            <Command Handler="CommandHandler(command,record);" />
                        </Listeners>
                    </ext:CommandColumn>
                </Columns>
            </ColumnModel>
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
            </SelectionModel>
            <Plugins>
                <ext:RowExpander ID="RowExpander1" runat="server">
                    <Loader ID="Loader1" runat="server" DirectMethod="#{DirectMethods}.GetGrid" Mode="Component">
                        <LoadMask ShowMask="true" />
                        <Params>
                            <ext:Parameter Name="id" Value="this.record.getId()" Mode="Raw" />
                        </Params>
                    </Loader>
                </ext:RowExpander>
            </Plugins>
            <BottomBar>
                <ext:PagingToolbar Height="40" ID="PagingToolbar1" runat="server" StoreID="storePlan"
                    DisplayInfo="true">
                    <Items>
                        <ext:Label ID="lblPageSize" runat="server" Text="Page size:" />
                        <ext:ToolbarSpacer ID="ToolbarSpacer_listItems" runat="server" Width="10" />
                        <ext:ComboBox ID="cmbPageSize" runat="server" Width="80" SelectOnFocus="true" Selectable="true"
                            ValueField="Value" DisplayField="Text" ForceSelection="true" AllowBlank="false">
                            <Listeners>
                                <Select Handler="searchList()" />
                            </Listeners>
                            <Items>
                                <ext:ListItem Value="20" Text="20" />
                                <ext:ListItem Value="50" Text="50" />
                                <ext:ListItem Value="100" Text="100" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0">
                                </ext:ListItem>
                            </SelectedItems>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
        </ext:GridPanel>
        <br />
    </div>
    
     <ext:Window ID="wPlan" runat="server" Title="Employee Plan" Icon="Application" Resizable="false" OverflowY="Scroll"
        Width="980" Height="320" BodyPadding="5" Hidden="true" Modal="true">
        <Content>
            <table style="margin-left: 20px; margin-top: 20px;">
                <tr>
                    <td style="width: 700px;">
                        <ext:DateField ID="txtDate" runat="server" Width="120" LabelAlign="Top" FieldLabel=""
                            LabelSeparator="">
                            <Plugins>
                                <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                            </Plugins>
                        </ext:DateField>
                    </td>
                    <td>
                        <ext:Image ID="img" runat="server" Width="30" Height="30" Cls="img-circle" />
                    </td>
                    <td>
                        <ext:DisplayField ID="dfName" runat="server" />
                    </td>
                </tr>
            </table>
            <ext:GridPanel StyleSpec="margin-top:15px; margin-left:20px;" ID="gridPlan" runat="server"
                Cls="itemgrid" Scroll="Vertical" Width="900">
                <Store>
                    <ext:Store ID="store1" runat="server">
                        <Model>
                            <ext:Model ID="modelPlan" Name="SettingModel" runat="server" IDProperty="SN">
                                <Fields>
                                    <ext:ModelField Name="SN" Type="Int" />
                                    <ext:ModelField Name="Description" Type="String" />
                                    <ext:ModelField Name="TotalTime" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="colSN" Sortable="false" MenuDisabled="true" runat="server" Text="SN"
                            Align="Center" Width="50" DataIndex="SN">
                        </ext:Column>
                        <ext:Column ID="colDescription" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Description" Align="Left" Width="650" DataIndex="Description" Wrap="true">
                        </ext:Column>
                        <ext:Column ID="colTime" Sortable="false" MenuDisabled="true" runat="server"
                            Text="Time" Align="Left" Width="200" DataIndex="TotalTime">
                        </ext:Column>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                </SelectionModel>
                <Plugins>
                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1">
                    </ext:CellEditing>
                </Plugins>
            </ext:GridPanel>
        </Content>
    </ext:Window>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
