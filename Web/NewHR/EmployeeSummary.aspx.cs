﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using Utils;
using System.IO;
using Web.Helper;
using Utils.Calendar;

namespace Web.NewHR
{
    public partial class EmployeeSummary : BasePage
    {
        int employeeId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            employeeId = int.Parse(Request.QueryString["ID"]);

            if (!X.IsAjaxRequest && !IsPostBack)
            {
                //btnEdit.Listeners.Click.Handler += employeeId;
                gender1.InnerHtml = "Gender";//string.Format(gender.InnerHtml, CommonManager.GetGenderName);
                LoadEditData();

            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            bool isEng = false;
            if (UserManager.IsEnglishDatePreferredInNepaliDateSys())
                isEng = true;

            UserControl ctrl = Master.ContentHeaderCtrl();
            ResourceManager rm = ctrl.FindControl("ResourceManager1") as ResourceManager;

            rm.RegisterClientInitScript("Date11111", string.Format("isEnglish = {0};", isEng.ToString().ToLower()));

            rm.RegisterClientInitScript("Date22222", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(isEng).ToString()));
        }

        protected void btnEditDetails_Click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(Request.QueryString["ID"]))
                return;
            string EmployeeId = Request.QueryString["ID"].ToString();
            Response.Redirect("~/newhr/PersonalDetail.aspx?ID=" + employeeId);
        }


        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveBrTran");
            if (Page.IsValid)
            {
                EmployeeServiceHistory obj = new EmployeeServiceHistory();

                if (!string.IsNullOrEmpty(txtLetterNumber.Text.Trim()))
                    obj.LetterNo = txtLetterNumber.Text.Trim();


                if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
                {
                    obj.LetterDateEng = BLL.BaseBiz.GetEngDate(calLetterDate.Text.Trim(), true);
                    obj.LetterDate = BLL.BaseBiz.GetAppropriateDate(obj.LetterDateEng.Value);


                    obj.DateEng = BLL.BaseBiz.GetEngDate(calFromDate.Text.Trim(), true);
                    obj.Date = BLL.BaseBiz.GetAppropriateDate(obj.DateEng);

                    if (!string.IsNullOrEmpty(calToDate.Text.Trim()))
                    {
                        obj.ToDateEng = BLL.BaseBiz.GetEngDate(calToDate.Text.Trim(), true);
                        obj.ToDate = BLL.BaseBiz.GetAppropriateDate(obj.ToDateEng.Value);
                    }
                }
                else
                {
                    obj.Date = calFromDate.Text.Trim();
                    obj.DateEng = GetEngDate(obj.Date);

                    obj.ToDate = calToDate.Text.Trim();
                    if (!string.IsNullOrEmpty(obj.ToDate))
                        obj.ToDateEng = GetEngDate(obj.ToDate);

                    obj.LetterDate = calLetterDate.Text.Trim();
                    obj.LetterDateEng = GetEngDate(obj.LetterDate);


                }


                obj.BranchId = int.Parse(cmbBranch.SelectedItem.Value);
                obj.DepartmentId = int.Parse(cmbDepartment.SelectedItem.Value);
                obj.LevelId = int.Parse(cmbLevel.SelectedItem.Value);
                obj.DesignationId = int.Parse(cmbDesignation.SelectedItem.Value);
                obj.StatusId = int.Parse(cmbStatus.SelectedItem.Value);

                //if(!string.IsNullOrEmpty(txtSpecialResponsibility.Text.Trim()))
                //    obj.SpecialResponsibility = txtSpecialResponsibility.Text.Trim();



                obj.EventID = short.Parse(cmbEventType.SelectedItem.Value);


                if (!string.IsNullOrEmpty(txtNote.Text.Trim()))
                    obj.Notes = txtNote.Text.Trim();

                if (!string.IsNullOrEmpty(cmbSubDepartment.Text))
                    obj.SubDepartmentId = int.Parse(cmbSubDepartment.SelectedItem.Value);

                obj.EmployeeId = employeeId;

                if (!string.IsNullOrEmpty(hdnBranchDepartmentId.Text))
                {
                    obj.EventSourceID = int.Parse(hdnBranchDepartmentId.Text);
                }


                // If the it is last branch transfer then only allow to update
                //if (CommonManager.IsLastBranchTransfer(obj.BranchDepartmentId, obj.EmployeeId.Value) == false)
                //{
                //    NewMessage.ShowWarningMessage("Only latest branch transfer can be edited.");
                //    return;
                //}

                Status status =  CommonManager.SaveUpdateServiceHistory(obj);

                if (status.IsSuccess)
                {
                    
                    UpdateBranchDepPos(EmployeeManager.GetEmployeeById(employeeId));
                    eventAddEditWindow.Close();
                }
                else
                    NewMessage.ShowWarningMessage(status.ErrorMessage);

            }

            BindEventList();
        }
        #region "Service event add/edit"

        protected void cmbLevel_Select(object sender, DirectEventArgs e)
        {
            cmbDesignation.GetStore().DataSource = NewHRManager.GetDesignationsByLevel(
                int.Parse(cmbLevel.SelectedItem.Value)).ToList();
            cmbDesignation.GetStore().DataBind();
        }

        private void Clear()
        {
            txtLetterNumber.Text = "";
            calLetterDate.Text = "";
            cmbBranch.Clear();
            cmbDepartment.Clear();
            cmbSubDepartment.Clear();
            //txtSpecialResponsibility.Text = "";
            //calDepartureDate.Text = "";
            calFromDate.Text = "";
            txtNote.Text = "";

        }
        private void LoadDepartment(int branchId)
        {
            cmbDepartment.GetStore().DataSource = DepartmentManager.GetAllDepartmentsByBranch(branchId);
            cmbDepartment.GetStore().DataBind();
        }

        protected void Branch_Select(object sender, DirectEventArgs e)
        {
            cmbDepartment.ClearValue();
            LoadDepartment(int.Parse(cmbBranch.SelectedItem.Value));
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int eventSourceID = int.Parse(hdnBranchDepartmentId.Text);
            EmployeeServiceHistory history = CommonManager.GetServiceHistory(eventSourceID);
            int employeeId = history.EmployeeId;
            //EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);

            

            if (history.EventID != null)
                cmbEventType.SetValue(history.EventID.ToString());
            else
                cmbEventType.ClearValue();

            cmbBranch.SetValue(history.BranchId.Value.ToString());
            LoadDepartment(history.BranchId.Value);
            cmbDepartment.SetValue(history.DepartmentId.Value.ToString());
            txtLetterNumber.Text = history.LetterNo;
            //txtSpecialResponsibility.Text = history.SpecialResponsibility;                   

            cmbStatus.SetValue(history.StatusId.ToString());

            int levelId = CommonManager.GetLevelIDForDesignation(history.DesignationId.Value);
            cmbLevel.SetValue(levelId.ToString());

            cmbDesignation.GetStore().DataSource = NewHRManager.GetDesignationsByLevel(levelId).ToList();
            cmbDesignation.GetStore().DataBind();

            cmbDesignation.SetValue(history.DesignationId.ToString());


            if (IsEnglish == false && SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred)
            {

                if (history.LetterDateEng != null)
                    calLetterDate.Text = history.LetterDateEng.Value.ToString("yyyy/MM/dd");

                if (history.DateEng != null)
                    calFromDate.Text = history.DateEng.ToString("yyyy/MM/dd");

                //if(history.DepartureDateEng != null)
                //    calDepartureDate.Text = history.DepartureDateEng.Value.ToString("yyyy/MM/dd");

            }
            else
            {
                calLetterDate.Text = history.LetterDate;
                calFromDate.Text = history.Date;
                //calDepartureDate.Text = history.DepartureDate;
            }

            txtNote.Text = history.Notes;

            if (history.SubDepartmentId != null)
                cmbSubDepartment.SetValue(history.SubDepartmentId.Value.ToString());

            btnSave.Text = Resources.Messages.Update;
            eventAddEditWindow.Show();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int branchDepartmentId = int.Parse(hdnBranchDepartmentId.Text);

            Status status = CommonManager.DeleteServiceHistory(branchDepartmentId);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Event deleted.");
                //X.Js.Call("searchList");
                BindEventList();
                UpdateBranchDepPos(EmployeeManager.GetEmployeeById(employeeId));
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }
        protected void btnNewEvent_Click(object sender, DirectEventArgs e)
        {
            Clear();

            hdnBranchDepartmentId.Text = "";

            cmbEventType.ClearValue();
            calFromDate.Text = "";
            txtLetterNumber.Text = "";
            calLetterDate.Text = "";
            cmbBranch.ClearValue();
            cmbDepartment.ClearValue();
            cmbLevel.ClearValue();
            cmbDesignation.ClearValue();
            cmbStatus.ClearValue();
            txtNote.Text = "";


            GetEmployeeCurrentBranchDepartmentResult branchDep =
               BLL.BaseBiz.PayrollDataContext.GetEmployeeCurrentBranchDepartment(DateTime.Now.Date, employeeId)
               .FirstOrDefault();

            int branchId = branchDep.BranchId.Value;
            int departmentId = branchDep.DepartmentId.Value;
            int statusId = BLL.BaseBiz.PayrollDataContext.GetEmployeeStatusForDate(DateTime.Now.Date, employeeId).Value;
            int designationId = BLL.BaseBiz.PayrollDataContext.GetEmployeeCurrentPositionForDate(DateTime.Now.Date, employeeId).Value;
            int levelId = CommonManager.GetLevelIDForDesignation(designationId);


            {
                cmbBranch.SetValue(branchId.ToString());
                LoadDepartment(branchId);
                cmbDepartment.SetValue(departmentId.ToString());

                cmbStatus.SetValue(statusId.ToString());

                cmbLevel.SetValue(levelId.ToString());

                cmbDesignation.GetStore().DataSource = NewHRManager.GetDesignationsByLevel(levelId).ToList();
                cmbDesignation.GetStore().DataBind();

                cmbDesignation.SetValue(designationId.ToString());
            }

            eventAddEditWindow.Show();
        }
        private void LoadEventPoupUpData()
        {

            List<HR_Service_Event> eventList = CommonManager.GetServieEventTypesExcludingRetirementType();
            if (eventList.Count > 0)
            {
                cmbEventType.Store[0].DataSource = eventList;
                cmbEventType.Store[0].DataBind();
            }
            else
                reqdEventType.Enabled = false;

            cmbEventType.Store[0].DataSource = eventList;
            cmbEventType.Store[0].DataBind();

            cmbBranch.GetStore().DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.GetStore().DataBind();

            cmbSubDepartment.GetStore().DataSource = ListManager.GetSubDepartments();
            cmbSubDepartment.GetStore().DataBind();

            cmbLevel.Store[0].DataSource = NewPayrollManager.GetAllParentLevelList();
            cmbLevel.Store[0].DataBind();

            JobStatus status = new JobStatus();
            List<KeyValue> list = status.GetMembers();
            list.RemoveAt(0);
            cmbStatus.Store[0].DataSource = list;
            cmbStatus.Store[0].DataBind();

        }

        #endregion

        private void LoadEditData()
        {
            LoadEventPoupUpData();

            foreach (Ext.Net.HyperLink item in btnEditDetails.Menu[0].Items)
            {
                item.NavigateUrl += employeeId;
            }

            linkRewardAddition.Visible = CommonManager.CompanySetting.HasLevelGradeSalary;
           

            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            if (emp != null)
            {

                LoadBasicInfo(emp);
                LoadPersonalInfo(emp);
                LoadContactInfo(emp);
                LoadHireDetails(emp);
              


                if (emp.IsRetired != null && emp.IsRetired.Value)
                {
                    if (emp.EHumanResources[0].DateOfRetirementEng != null && emp.EHumanResources[0].DateOfRetirementEng < DateTime.Now)
                    {
                        spanRetiredDate.InnerHtml = "Retired : " + emp.EHumanResources[0].DateOfRetirement + " (" + emp.EHumanResources[0].DateOfRetirementEng.Value.ToLongDateString() + ")";
                        //spanRetiredDate.InnerHtml = "Already retired on :<br> " +  emp.EHumanResources[0].DateOfRetirementEng.Value.ToLongDateString();
                    }
                }
                if (emp.IsResigned != null && emp.IsResigned.Value)
                {
                    if (emp.EHumanResources[0].DateOfResignationEng != null && emp.EHumanResources[0].DateOfResignationEng < DateTime.Now)
                    {
                        spanRetiredDate.InnerHtml = "Retired : " + emp.EHumanResources[0].DateOfResignation + " (" + emp.EHumanResources[0].DateOfResignationEng.Value.ToLongDateString() + ")";
                    }
                }


               


                    BindEventList();
                
                //else
                //{
                //    // load service history
                //    gridServiceHistoryOld.DataSource = EmployeeManager.GetServiceHistoryList(employeeId, null, null, 0, 1000,null);
                //    gridServiceHistoryOld.DataBind();
                //}
            }
        }

        private void BindEventList()
        {
            int totalRecords = 0;
            List<GetServiceHistoryNewResult> list = BranchManager.GetNewServiceHistory
           (0, 9999, ref totalRecords, "", employeeId, null, null, null);
            // load service history


            gridEventList.Store[0].DataSource = list;
            gridEventList.Store[0].DataBind();
        }

        private void LoadHireDetails(EEmployee emp)
        {

            //empPostionAndGrade.HRef += emp.EmployeeId;
            // Group/Level
            int? levelId = null;
            double grade = 0;
            NewHRManager.SetEmployeeJoiningLevelGrade(ref levelId, ref grade, emp.EmployeeId);
            if (levelId != null)
            {
                BLevel levelEntity = NewPayrollManager.GetLevelById(levelId.Value);
                level.InnerHtml = levelEntity.BLevelGroup.Name + " - " + levelEntity.Name;
                gradeDisplay.InnerHtml = grade.ToString();
            }
            else
            {
                trLevel.Visible = false;
                trGrade.Visible = false;
               // divLevelGradeHistory.Visible = false;
            }
            

            if (emp.EHumanResources.Count > 0)
            {
                if (emp.EHumanResources[0].HireMethod != null)
                    hireMethod.InnerHtml = EmployeeManager.GetHireMethod(emp.EHumanResources[0].HireMethod.Value).HireMethodName;



            }

            ServicePeroid first = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            if (first != null)
            {
                hireDate.InnerHtml = first.FromDate;
                if (IsEnglish == false)
                    hireDate.InnerHtml += " (" + NewHelper.FormatDate(first.FromDateEng) + " AD)";
                spnJobStatus.InnerHtml = first.CurrentStatusText;
            }

            if (emp.EHumanResources[0].AppointmentLetterDateEng != null)
            {
                spanAppointmentDate.InnerHtml = emp.EHumanResources[0].AppointmentLetterDate;
                if (IsEnglish == false)
                    spanAppointmentDate.InnerHtml += " (" + NewHelper.FormatDate(emp.EHumanResources[0].AppointmentLetterDateEng.Value) + " AD)";
            }
            

            ECurrentStatus empLastStatus = new EmployeeManager().GetCurrentLastStatus(emp.EmployeeId);
            if (empLastStatus != null)
            {
                //currentJobStatus.InnerHtml = empLastStatus.CurrentStatusText;
                spnCurrentStatus.InnerHtml = empLastStatus.CurrentStatusText;

                if (!string.IsNullOrEmpty(empLastStatus.ToDate))
                    contractEndDate.InnerHtml = empLastStatus.ToDate;

            }

            //binding starting branch,department,level,designation

        }

        public void UpdateBranchDepPos(EEmployee emp)
        {

            X.Js.AddScript(
                string.Format("document.getElementById('ctl00_ContentPlaceHolder_Main_positionDesignation').innerHTML='{0}';",
                emp.EDesignation.Name));

            string BranchDepartment = "";
            GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);

            if (!string.IsNullOrEmpty(currentBranchDep.Department))
            {
                BranchDepartment = currentBranchDep.Department;
            }

            if (!string.IsNullOrEmpty(currentBranchDep.Branch))
            {
                if (!string.IsNullOrEmpty(BranchDepartment))
                {
                    BranchDepartment += ",<br>" + currentBranchDep.Branch;
                }
                else
                    BranchDepartment += ",<br>" + currentBranchDep.Branch;

            }

            X.Js.AddScript(
               string.Format("document.getElementById('ctl00_ContentPlaceHolder_Main_SpnPositionDesignationDepartment').innerHTML='{0}';",
               BranchDepartment));
        }

        private void LoadContactInfo(EEmployee emp)
        {
            //editContact.HRef += emp.EmployeeId;
            //editAddress.HRef += emp.EmployeeId;

            
            if (emp.EAddresses.Count > 0)
            {
                // Address
                EAddress entity = emp.EAddresses[0];

                string present = "", permanent = "";
                WebHelper.GetAddressHTMLForEmployeeProfile(entity, ref present, ref permanent);
                //addressTemporary.InnerHtml = present;
                if (present != "")
                    spnAddress.InnerHtml = present;
                else
                    spnAddress.InnerHtml = permanent;
                if (string.IsNullOrEmpty(present) && string.IsNullOrEmpty(permanent))
                    spnAddress.Visible = false;


                positionDesignation.InnerHtml = " " + emp.EDesignation.Name;

                string BranchDepartment = "";
                GetEmployeeCurrentBranchDepartmentResult currentBranchDep = EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);

                if (!string.IsNullOrEmpty(currentBranchDep.Department))
                {
                    BranchDepartment = currentBranchDep.Department;
                }

                if (!string.IsNullOrEmpty(currentBranchDep.Branch))
                {
                    if (!string.IsNullOrEmpty(BranchDepartment))
                    {
                        BranchDepartment += ",<br>" + currentBranchDep.Branch;
                    }
                    else
                        BranchDepartment += ",<br>" + currentBranchDep.Branch;

                }

                SpnPositionDesignationDepartment.InnerHtml = BranchDepartment;

                spnPermanentAddress.InnerHtml = permanent;

                //if(permanent=="" && present=="")
                //spnLabelAddress.InnerText = "";
                // Contact
                //extNo.InnerHtml = entity.Extension;
                //noPersonalNo.InnerHtml = entity.PersonalPhone;
                //noPersonalMobile.InnerHtml = entity.PersonalMobile;
                //contactEmergency.InnerHtml = entity.EmergencyName + (!string.IsNullOrEmpty(entity.EmergencyRelation) ? ", " + entity.EmergencyRelation : "");
                //contactEmergencyNo.InnerHtml = entity.EmergencyPhone + (!string.IsNullOrEmpty(entity.EmergencyMobile) ? " / " + entity.EmergencyMobile : "");
            }
        }
        private void LoadPersonalInfo(EEmployee emp)
        {
            ein.InnerHtml = emp.EmployeeId.ToString();
            gender.InnerHtml = new Gender().GetText(emp.Gender.Value);
            dob.InnerHtml = emp.DateOfBirth;

            if (emp.DateOfBirthEng != null)
                dob.InnerHtml += "<span style='padding-left:15px'></span> " + NewHelper.GetElapsedTime(emp.DateOfBirthEng.Value, DateTime.Now);

            maritalStatus.InnerHtml = emp.MaritalStatus;
            fatherName.InnerHtml = emp.FatherName;
            spnGrandFatherName.InnerHtml = CommonManager.GetFamilyMemberNameByRelation(emp.EmployeeId, "grandfather");
            SpnSpouseName.InnerHtml = CommonManager.GetFamilyMemberNameByRelation(emp.EmployeeId, "spouse");

           // editPersonalInfo.HRef += emp.EmployeeId;
           

            
        }



        private void LoadBasicInfo(EEmployee emp)
        {

            title.InnerHtml = emp.Title + " " + emp.Name;//+ " - " + emp.EmployeeId;
            if (!string.IsNullOrEmpty(emp.EHumanResources[0].IdCardNo))
            {
               // title.InnerHtml += " (I No : " + emp.EHumanResources[0].IdCardNo + ")";
                spnIdcardNumber.InnerText = " (I No : " + emp.EHumanResources[0].IdCardNo + ")";
                if (!string.IsNullOrEmpty(emp.EHumanResources[0].CardSerialNo))
                    spnCardSeriallNumber.InnerText = emp.EHumanResources[0].CardSerialNo;

            }


            if (!string.IsNullOrEmpty(emp.Hobby))
                spnHobby.InnerText = emp.Hobby;

            if (!string.IsNullOrEmpty(emp.Birthmark))
                spnBrithMark.InnerText = emp.Birthmark;

            if (emp.MarriageAniversaryEng!=null)
            {
                SpnMarriageAnniversary.InnerHtml = DateHelper.GetMonthName(emp.MarriageAniversaryEng.Value.Month, true) + ", " + emp.MarriageAniversaryEng.Value.Day;
                //SpnMarriageAnniversary.InnerHtml += "<span style='padding-left:20px'></span> in " + NewHelper.calculateAge(
                //    emp.MarriageAniversaryEng.Value, DateTime.Now);
            }
            if (!string.IsNullOrEmpty(emp.MotherName))
                spnMotherName.InnerText = emp.MotherName;

            if (!string.IsNullOrEmpty(emp.GrandFatherName))
                spnGrandFatherName.InnerText = emp.GrandFatherName;

            if (emp.Children!=null)
                spnNoOfChildren.InnerText = emp.Children.ToString();

            spanRefernce.InnerText = emp.EHumanResources[0].HireReference;
            
            List<HCitizenship> _list  = NewHRManager.GetCitizenshipByEmployeeID(emp.EmployeeId);
            if (_list.Any())
            {
                spnCitizenshipNo.InnerText = _list[0].CitizenshipNo;
                spnNationality.InnerHtml = _list[0].Nationality;
            }
            
            //if (emp.HCitizenships.Count > 0)
            //{
            //    if (emp.HCitizenships[0].CitizenshipNo != null)
            //        spnCitizenshipNo.InnerText = emp.HCitizenships[0].CitizenshipNo;
            //}

            if (emp.HPassports.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HPassports[0].PassportNo))
                    spnPasswordNo.InnerText = emp.HPassports[0].PassportNo;
            }


            List<HDrivingLicence> _HDrivingLicences = NewHRManager.GetDrivingLicenceByEmployeeID(emp.EmployeeId);
            if (_HDrivingLicences.Any())
            {
                spnDrivingLicenseNo.InnerText = _HDrivingLicences[0].DrivingLicenceNo;
                spnLicenseType.InnerText = _HDrivingLicences[0].LiscenceTypeName;
            }



            // PF start date
            Company company = SessionManager.CurrentCompany;
            ECurrentStatus pfStatusDate = EmployeeManager.GetStatusStart(emp.EmployeeId, company.PFRFFunds[0].EffectiveFrom);
            if (pfStatusDate != null)
            {
                spnPFStartDate.InnerHtml = pfStatusDate.FromDate;
                if (IsEnglish == false)
                    spnPFStartDate.InnerHtml += " (" + pfStatusDate.FromDateEng.ToString("yyyy/MM/dd") + " AD)";
            }

            //if (emp.HDrivingLicences.Count > 0)
            //{
            //    if (!string.IsNullOrEmpty(emp.HDrivingLicences[0].DrivingLicenceNo))
            //        spnDrivingLicenseNo.InnerText = emp.HDrivingLicences[0].DrivingLicenceNo;


            //    if (!string.IsNullOrEmpty(emp.HDrivingLicences[0].LiscenceTypeName))
            //        spnLicenseType.InnerText = emp.HDrivingLicences[0].LiscenceTypeName;
            //}

            if (!string.IsNullOrEmpty(emp.EFundDetails[0].PFRFNo))
                spnPFNo.InnerText = emp.EFundDetails[0].PFRFNo;

            if (!string.IsNullOrEmpty(emp.EFundDetails[0].CITNo))
                spnCITNo.InnerHtml = emp.EFundDetails[0].CITNo;

            if (!string.IsNullOrEmpty(emp.EFundDetails[0].PANNo))
                SpnPanNo.InnerHtml = emp.EFundDetails[0].PANNo;

            if (!string.IsNullOrEmpty(emp.PPays[0].BankName))
                spnPaymentBank.InnerHtml = emp.PPays[0].BankName;

            if (!string.IsNullOrEmpty(emp.PPays[0].BankACNo))
                spnAccountNumber.InnerHtml = emp.PPays[0].BankACNo;

            
            //if (emp.HCitizenships.Count > 0)
            //{
            //    if (!string.IsNullOrEmpty(emp.HCitizenships[0].Nationality))
            //        spnNationality.InnerHtml = emp.HCitizenships[0].Nationality;

            //    if (!string.IsNullOrEmpty(emp.HCitizenships[0].Nationality))
            //        spnNationality.InnerHtml = emp.HCitizenships[0].Nationality;
            //}

            if (emp.HHumanResources.Count > 0)
            {
                if (emp.HHumanResources[0].CasteId != null)
                {
                    HCaste _HCaste = CommonManager.GetCastesById(emp.HHumanResources[0].CasteId.Value);
                    if (_HCaste != null)
                        spnEthinicity.InnerHtml = _HCaste.CasteName;
                }

                if (!string.IsNullOrEmpty(emp.HHumanResources[0].Religion))
                    spnReligion.InnerHtml = emp.HHumanResources[0].Religion;
            }
            
           // Designation/Position
            
 
            // Group/Level
            if (emp.PEmployeeIncomes.Count > 0 && emp.PEmployeeIncomes[0].LevelId != null)

            {
                BLevel level = NewPayrollManager.GetLevelById(emp.PEmployeeIncomes[0].LevelId.Value);
                BLevel actingLevel = NewPayrollManager.GetActingLevelInTodayDate(emp.EmployeeId);

                if (level != null)
                    groupLevel.InnerHtml = level.BLevelGroup.Name + " (Level - " + level.Name +
                        (actingLevel == null ? "" : ", Acting Level - " + actingLevel.Name)
                        + ")";

               
            }
            else
                groupLevelImg.Visible = false;

            string StartingLevelOfEmployee = CommonManager.GetStartingLevelOfEmployee(emp.EmployeeId);
            if (!string.IsNullOrEmpty(StartingLevelOfEmployee))
                spnStartingLevel.InnerHtml = StartingLevelOfEmployee;
            else
            {
                if (emp.EDesignation.LevelId != null)
                {
                    BLevel _BLevel = NewPayrollManager.GetLevelById(emp.EDesignation.LevelId.Value);
                    if (_BLevel != null)
                    {
                        spnStartingLevel.InnerHtml = _BLevel.Name;
                    }

                }
            }

            BLevel _BLevel1 = NewPayrollManager.GetLevelById(emp.EDesignation.LevelId.Value);
            if (_BLevel1 != null)
            {
                spnCurrentLevel.InnerHtml = _BLevel1.Name;
            }


            DateTime? lastPromotionDate = EmployeeManager.GetLastPromotionDate(emp.EmployeeId);
            if (lastPromotionDate != null)
            {
                spnLastPromotion.InnerHtml = BLL.BaseBiz.GetAppropriateDate(lastPromotionDate.Value);
                if(IsEnglish==false)
                    spnLastPromotion.InnerHtml += " (" + NewHelper.FormatDate(lastPromotionDate.Value) + " AD)";
            }


            string StartingDesignationOfEmployee = CommonManager.GetStartingDesignationOfEmployee(emp.EmployeeId);
           
            if (!string.IsNullOrEmpty(StartingDesignationOfEmployee))
            {
                spnStartingPosition.InnerHtml = StartingDesignationOfEmployee;
                spnCurrentPosition.InnerHtml = emp.EDesignation.Name;               
            }
            else
            {

                if (emp.EDesignation != null)
                {
                    spnStartingPosition.InnerHtml = emp.EDesignation.Name;
                    spnCurrentPosition.InnerHtml = emp.EDesignation.Name;
                  
                }
            }

            // Branch

            spnStartingBarnch.InnerHtml = CommonManager.GetStartingBranchOfEmployee(emp.EmployeeId);
            spnStartingDepartment.InnerHtml = CommonManager.GetStartingDepartmentOfEmployee(emp.EmployeeId);

            GetEmployeeCurrentBranchDepartmentResult currentBranchDep = 
                EmployeeManager.GetEmployeeCurrentBranchDepartment(emp.EmployeeId);
            if (currentBranchDep != null)
            {
                branch.InnerHtml = currentBranchDep.Department + ", " + currentBranchDep.Branch;
                spnCurrentBranch.InnerHtml = currentBranchDep.Branch;
                spnCurrentDepartment.InnerHtml = currentBranchDep.Department;
            }
            
             

            // Contact
            if (emp.EAddresses.Count > 0)
            {

                EAddress entity = emp.EAddresses[0];

                //if (!string.IsNullOrEmpty(entity.CIPhoneNo) || !string.IsNullOrEmpty(entity.CIMobileNo) || !string.IsNullOrEmpty(entity.Extension))
                //    contactMobile.InnerHtml += string.Format("{0} / {1} ({2})", entity.CIPhoneNo, entity.CIMobileNo, entity.Extension);


                // Mobile
                //if (EmployeeManager.IsBranchOrDepartmentOrRegionalHead(emp.EmployeeId))
                //{
                    if (!string.IsNullOrEmpty(entity.CIMobileNo))
                    {
                        contactMobile.InnerHtml = entity.CIMobileNo + " (O)";

                        if (!string.IsNullOrEmpty(entity.PersonalMobile))
                        {
                            contactMobile.InnerHtml += " " + entity.PersonalMobile + " (P)";
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(entity.PersonalMobile))
                        {
                            contactMobile.InnerHtml += entity.PersonalMobile + " (P)";
                        }
                    }
                    contactMobile.InnerHtml += "&nbsp;";
                //}
                //else
                //{
                //    contactMobile.Visible = false;
                //    imgMobile.Visible = false;
                //}

                // Phone
                if (!string.IsNullOrEmpty(entity.CIPhoneNo))
                    contactPhone.InnerHtml = entity.CIPhoneNo;
                if( !string.IsNullOrEmpty(entity.Extension))
                    contactPhone.InnerHtml += " - " + entity.Extension;
                if (contactPhone.InnerHtml != "")
                    contactPhone.InnerHtml += " (O)";

                if (!string.IsNullOrEmpty(entity.PersonalPhone))
                    contactPhone.InnerHtml += " " + entity.PersonalPhone + " (R)";

                contactPhone.InnerHtml += "&nbsp;";
            

                email.InnerHtml += string.Format("<a href='mailto:{0}'>{0}</a> ", entity.CIEmail);
            }

            // Working for since
            int years, months, days, hours;
            int minutes, seconds, milliseconds;
            ServicePeroid firstStatus = EmployeeManager.GetServicePeroidDate(emp.EmployeeId);
            if (firstStatus != null)
            {
                NewHelper.GetElapsedTime(firstStatus.FromDateEng, firstStatus.ToDateEng.Value, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                string text = "";
                if (years != 0)
                    text = " " + years + " years";
                if(months != 0)
                    text += " " + months + " months";
                if (days != 0)
                    text += " " + days + " days";

                workingFor.InnerHtml += text;

                workingFor.InnerHtml += "<br>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Since " +
                    (IsEnglish ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + "<br>" + " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + firstStatus.FromDateEng.ToShortDateString() + ")");
                
            }

            // Photo
            bool hasPhoto = false;
            if (emp.HHumanResources.Count > 0)
            {
                if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                {
                    image.Src =Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    if (File.Exists(Server.MapPath(image.Src)))
                        hasPhoto = true;

                    //image.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                    //if (File.Exists(Server.MapPath(image.ImageUrl)))
                    //    hasPhoto = true;
                }
            }
            if (!hasPhoto)
            {
               //image.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(0, emp));
                image.Src = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(emp.EmployeeId));
            }
        }

        private void HideButtonsAndColumns()
        {
           
        }



       

    }
}