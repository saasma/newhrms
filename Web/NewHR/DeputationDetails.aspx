﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Master/HROld.Master"
    AutoEventWireup="true" Title="Deputation Details" EnableEventValidation="false"
    CodeBehind="DeputationDetails.aspx.cs" Inherits="Web.NewHR.DeputationDetails" %>

<%@ Register Src="~/Controls/ContentHeader.ascx" TagName="ContentHeader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .GridRowHover
        {
            cursor: pointer;
        }
        .empName
        {
            font-size: 20px;
            color: #439EC6;
            font-weight: bold;
        }
        .titleDesign
        {
            font-size: 15px;
            color: #000000;
            text-decoration: underline;
            padding-top: 10px;
        }
        
        .items span
        {
            display: block;
            padding-top: 3px;
            font-weight: bold;
        }
        .selected td
        {
            background-color: #E0EDF9 !important;
        }
        form strong
        {
            display: block !important;
            padding-bottom: 3px;
        }
        .fieldTable > tbody > tr > td
        {
            padding-left: 15px !important;
            padding-top: 15px;
        }
    </style>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        function popupUpdateLeaveCall(LeaveId) {
            var ret = popup('Id=' + LeaveId + '&type=Deputation');


            return false;
        }
    </script>
    <link href="../css/core.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <uc1:ContentHeader Id="ContentHeader1" runat="server" />
    <asp:HiddenField ID="hdn" runat="server" />
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Deputation Details
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <div class="attribute" style="padding:10px">
            <div class="left">
                <strong>Branch Filter</strong>
                <asp:DropDownList Width="200px" ID="ddlFilterByBranch" runat="server" DataValueField="BranchId"
                    DataTextField="Name" AppendDataBoundItems="true" OnSelectedIndexChanged="LoadEmployees"
                    AutoPostBack="true">
                    <asp:ListItem Value="-1">--Select Branch--</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="left" style="padding-left: 10px;">
                <strong>Employee</strong>
                <asp:DropDownList Width="250px" ID="ddlEmployeeList" runat="server" DataValueField="EmployeeId"
                    DataTextField="IDAndName" AutoPostBack="true" OnSelectedIndexChanged="LoadEmployeeDetails">
                    <asp:ListItem Value="-1">--Select Employee--</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ValidationGroup="AEEmployee" ID="valEmployeeReq" ControlToValidate="ddlEmployeeList"
                    InitialValue="-1" Display="None" runat="server" ErrorMessage="Please select an Employee."></asp:RequiredFieldValidator>
            </div>
        </div>
        <div style="clear: both" />
        <uc2:MsgCtl ID="divMsgCtl" Width="900px" EnableViewState="false" Hide="true" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" Width="900px" EnableViewState="false" Hide="true"
            runat="server" />
        <div class="widget" style="margin-top: 15px">
            <div class="widget-body">
                <table style='clear: both; padding: 10px; margin-top: 10px;'>
                    <tr>
                        <td valign="top" style="padding-left: 10px;">
                            <asp:Image ID="image" runat="server" ImageUrl="~/images/sample.jpg" Width="150px"
                                Height="150px" />
                        </td>
                        <td valign="top" style="padding-left: 50px" class="items">
                            <asp:Label ID="lblName" CssClass="empName" runat="server" />
                            <asp:Label ID="lblWorkingIn" runat="server" Text="Working In" class="titleDesign" />
                            <asp:Label ID="lblBranch" runat="server" />
                            <asp:Label ID="lblDepartment" runat="server" />
                            <asp:Label ID="lblSince" runat="server" />
                            <asp:Label ID="lblTime" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" style="padding-left: 10px" class="items">
                            <asp:Label ID="Label1" runat="server" Text="Deputation Details" class="titleDesign" />
                            <table class="fieldTable firsttdskip" style="margin-left: -10px">
                                <tr>
                                    <td>
                                        <strong>Branch *</strong>
                                        <asp:DropDownList Width="200px" ID="ddlTransferToBranch" runat="server" DataValueField="BranchId"
                                            DataTextField="Name" OnSelectedIndexChanged="LoadDepartments" AppendDataBoundItems="true"
                                            AutoPostBack="true">
                                            <asp:ListItem Value="-1">--Select Branch--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTransferToBranch"
                                            Display="None" ErrorMessage="Branch is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <strong>Department *</strong>
                                        <asp:DropDownList Width="200px" ID="ddlTransferToDepartment" runat="server" DataValueField="DepartmentId"
                                            DataTextField="Name" OnSelectedIndexChanged="LoadSubDepartments" AutoPostBack="true">
                                            <asp:ListItem Value="-1">--Select Department--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlTransferToDepartment"
                                            Display="None" ErrorMessage="Department is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr runat="server" id="rowDeputationType">
                                    <td>
                                        <strong>Kaaj Type *</strong>
                                        <asp:DropDownList Width="200px" OnSelectedIndexChanged="ddlDeputationList_IndexChanged"
                                            ID="ddlDeputationList" runat="server" DataValueField="DeputationId" DataTextField="Name"
                                            AppendDataBoundItems="true" AutoPostBack="true">
                                            <asp:ListItem Value="-1">--Select Deputation--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlDeputationList"
                                            Display="None" ErrorMessage="Deputation is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                        <strong runat="server" id="desc"></strong>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>Deputation Details</strong>
                                        <asp:TextBox ID="txtDescription" TextMode="MultiLine" runat="server" Width="420"
                                            Height="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Letter Number</strong>
                                        <asp:TextBox ID="txtLetterNumber" Width="200px" runat="server" />
                                    </td>
                                    <td>
                                        <strong style="padding-bottom: 0px; margin-top: -3px;">Letter Date</strong>
                                        <pr:CalendarExtControl Width="200px" ID="calLetterDate" runat="server" LabelSeparator="" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="calLetterDate"
                                            Display="None" ErrorMessage="Letter date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong style="padding-bottom: 0px; margin-top: -3px;">Deputation Date</strong>
                                        <pr:CalendarExtControl Width="200px" ID="calDeputationDate" runat="server" LabelSeparator="" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="calDeputationDate"
                                            Display="None" ErrorMessage="Deputation date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <strong>Commute Days</strong>
                                        <asp:TextBox ID="txtCommuteDays" Width="200px" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtCommuteDays"
                                            Display="None" ErrorMessage="Commute Days is required." ValidationGroup="AEEmployee">
                                        </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="RequiredFieldValidator13" runat="server" Operator="GreaterThanEqual"
                                            ValueToCompare="0" Type="Double" ControlToValidate="txtCommuteDays" Display="None"
                                            ErrorMessage="Invalid Commute Days." ValidationGroup="AEEmployee"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>From Date *</strong>
                                        <pr:CalendarExtControl Width="200px" ID="calFrom" runat="server" LabelSeparator="" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="calFrom"
                                            Display="None" ErrorMessage="From date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <strong>To Date</strong>
                                        <%-- <My:Calendar Id="calFromDate" runat="server" />--%>
                                        <pr:CalendarExtControl Width="200px" ID="calTo" runat="server" LabelSeparator="" />
                                        <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="calTo"
                                            Display="None" ErrorMessage="To date is required." ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>--%>
                                    </td>
                                </tr>
                                <tr runat="server" id="rowDeputation1">
                                    <td>
                                        <strong>Attendance</strong>
                                        <asp:DropDownList Width="200px" ID="ddlAttendance" runat="server" DataValueField="Value"
                                            DataTextField="Text" AppendDataBoundItems="true">
                                            <asp:ListItem Value="-1">--Select Attendance--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlAttendance"
                                            Display="None" ErrorMessage="Attendance is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr runat="server" id="rowDeputation2">
                                    <td>
                                        <strong>Travelling Allowance</strong>
                                        <asp:DropDownList Width="200px" ID="ddlTA" runat="server" DataValueField="Value"
                                            DataTextField="Text" AppendDataBoundItems="true">
                                            <asp:ListItem Value="-1">--Select TA--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlTA"
                                            Display="None" ErrorMessage="TA is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <strong>Amount</strong>
                                        <asp:TextBox ID="txtTAAmount" Width="200px" runat="server" />
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" Operator="GreaterThanEqual"
                                            ValueToCompare="1" Type="Double" ControlToValidate="txtCommuteDays" Display="None"
                                            ErrorMessage="Invalid TA amount." ValidationGroup="AEEmployee"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr runat="server" id="rowDeputation3">
                                    <td>
                                        <strong>Daily Allowance</strong>
                                        <asp:DropDownList Width="200px" ID="ddlDA" runat="server" DataValueField="Value"
                                            DataTextField="Text" AppendDataBoundItems="true">
                                            <asp:ListItem Value="-1">--Select DA--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ddlDA"
                                            Display="None" ErrorMessage="DA is required." InitialValue="-1" ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <strong>Amount</strong>
                                        <asp:TextBox ID="txtDAAmount" Width="200px" runat="server" />
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" Operator="GreaterThanEqual"
                                            ValueToCompare="1" Type="Double" ControlToValidate="txtDAAmount" Display="None"
                                            ErrorMessage="Invalid DA amount." ValidationGroup="AEEmployee"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr runat="server" id="rowDeputation4">
                                    <td>
                                        <strong>Applicable Salary / Allowance</strong>
                                        <asp:DropDownList Width="200px" ID="ddlSalary" runat="server" DataValueField="Value"
                                            DataTextField="Text" AppendDataBoundItems="true">
                                            <asp:ListItem Value="-1">--Select Salary--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlSalary"
                                            Display="None" ErrorMessage="Applicable Salary is required." InitialValue="-1"
                                            ValidationGroup="AEEmployee"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="buttonsDiv" style="text-align: left; clear: both" runat="server" id="buttonsBar">
                    &nbsp; &nbsp;
                    <asp:Button ID="btnDelete" CssClass="cancel" CausesValidation="false" runat="server"
                        Text="Delete" OnClick="btnDelete_Click" Style='display: none' />
                    <asp:Button ID="btnSave" CssClass="save" runat="server" Text="Save" OnClientClick="valGroup='AEEmployee';return CheckValidation()"
                        OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" CssClass="cancel" CausesValidation="false" runat="server"
                        Text="Cancel" OnClick="btnCancel_Click" />
                    <asp:LinkButton runat="server" Text="Review Team" ID="btnReview" Visible="false">
                         <%--<a href="javascript:void(0)" onclick='<%# "popupUpdateLeaveCall(" +  Eval("DeputationEmployeeId") + ");return false;" %>'
                                style="width: 80px; display: block;" id="ImageButton12" />Review Team</a>--%>
                    </asp:LinkButton>
                </div>
                <div style="clear: both" />
            </div>
        </div>
    </div>
</asp:Content>
