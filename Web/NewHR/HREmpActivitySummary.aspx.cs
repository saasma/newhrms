﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using System.IO;
using System.Text;
using Utils.Calendar;
using Bll;
namespace Web.NewHR
{
    public partial class HREmpActivitySummary : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            List<PayrollPeriod> payrollPeriods = BLL.BaseBiz.PayrollDataContext
                .PayrollPeriods.OrderByDescending(x => x.PayrollPeriodId).ToList();

            cmbPayrollPeriod.Store[0].DataSource = payrollPeriods;
            cmbPayrollPeriod.Store[0].DataBind();
        }

        protected void btnLoad_Click(object sender, DirectEventArgs e)
        {
            if (cmbPayrollPeriod.SelectedItem == null || cmbPayrollPeriod.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Month Filter is required.");
                cmbPayrollPeriod.Focus();
                return;
            }

            int payrollPeriodId = int.Parse(cmbPayrollPeriod.SelectedItem.Value);
            int employeeId = -1;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbSearch.SelectedItem.Value);

            gridEmpActivitySummary.Store[0].DataSource = NewHRManager.GetNewActivitySummaryReport(payrollPeriodId, employeeId);
            gridEmpActivitySummary.Store[0].DataBind();          
        }


        protected void btnExcelPrint_Click(object sender, EventArgs e)
        {
            if (cmbPayrollPeriod.SelectedItem == null || cmbPayrollPeriod.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Month Filter is required.");
                cmbPayrollPeriod.Focus();
                return;
            }

            int payrollPeriodId = int.Parse(cmbPayrollPeriod.SelectedItem.Value);
            int employeeId = -1;

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbSearch.SelectedItem.Value);

            List<GetNewActivitySummaryReportResult> list = NewHRManager.GetNewActivitySummaryReport(payrollPeriodId, employeeId);


            ExcelHelper.ExportToExcel("EmloyeeActivitySummary", list,
                new List<String>() { },
              new List<String>() { },
                     new Dictionary<string, string>() { { "EmployeeId", "EIN" }, { "MonthDays", "Month days" }, { "WeeklyHoliday", "Weekly holidays" }
                         , { "PublicHoliday", "Public holiday" }, { "LeaveDays", "Leave days" }, { "NetWorkDays", "Net work days" }, { "ActivitySubmitted", "Activity submitted" }
                         , { "ActivityNotSubmitted", "Activity not submitted" }},
              new List<string>() { }
              , new Dictionary<string, string>() { },
              new List<string> { "Name", "EmployeeId", "MonthDays", "WeeklyHoliday", "PublicHoliday", "LeaveDays", "NetWorkDays", "ActivitySubmitted", "ActivityNotSubmitted" }
             );
        }

        protected void btnShowDetails_Click(object sender, DirectEventArgs e)
        {
            int employeeId = int.Parse(hdnEmployeeId.Text);
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            lblName.Text = string.Format("{0} (EIN - {1})", emp.Name, employeeId.ToString());
            int payrollPeriodId = int.Parse(cmbPayrollPeriod.SelectedItem.Value);

            gridActivityDetails.Store[0].DataSource = NewHRManager.GetNewActivityDetailsReport(payrollPeriodId, employeeId);
            gridActivityDetails.Store[0].DataBind();

            wActivityDetails.Center();
            wActivityDetails.Show();
        }

        protected void btnDetailsExport_Click(object sender, EventArgs e)
        {
            int employeeId = int.Parse(hdnEmployeeId.Text);
            EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
            
            int payrollPeriodId = int.Parse(cmbPayrollPeriod.SelectedItem.Value);

            List<GetNewActivityDetailsReportResult> list = NewHRManager.GetNewActivityDetailsReport(payrollPeriodId, employeeId);


            ExcelHelper.ExportToExcel(string.Format("Activity Details - {0}(EIN - {1})", emp.Name, employeeId.ToString()), list,
                new List<String>() { "DateEng" },
              new List<String>() { },
                     new Dictionary<string, string>() { },
              new List<string>() { }
              , new Dictionary<string, string>() { { "Activity Details", string.Format("{0} (EIN - {1})", emp.Name, employeeId.ToString()) } },
              new List<string> { "Day", "Status" }
             );
        }
        


    }
}