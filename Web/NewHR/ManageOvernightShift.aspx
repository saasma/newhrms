﻿<%@ Page Title="Manage Overnight Shift" EnableEventValidation="false" ValidateRequest="false"
    Language="C#" MasterPageFile="~/Master/NewDetails.Master" AutoEventWireup="true"
    CodeBehind="ManageOvernightShift.aspx.cs" Inherits="Web.NewHR.ManageOvernightShift" %>

<%@ Register Src="~/Employee/UserControls/AssignLeave.ascx" TagName="AssingLeave"
    TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="<%= ResolveUrl("~/Employee/override.js?v=") + Web.Helper.WebHelper.Version %>"></script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
        
        
        .overnight, .overnight a, .overnight td
        {
            color: #469146;
        }
        
        .RequestRow td
        {
            background-color: #FFF3C3 !important;
        }
        .RecommendedRow td
        {
            background-color: #B5D2FF !important;
        }
        .ApprovedRow td
        {
            background-color: #C6E0B4 !important;
        }
        .innerLR
        {
            padding-top: -20px !important;
        }
        
        .fieldTable > tbody > tr > td
        {
            padding-left: 2px !important;
        }
    </style>
    <script type="text/javascript">

    function searchList() {
              <%=PagingToolbar1.ClientID %>.doRefresh();
        }

     var overnightRender = function(value)
     {
        if(value==true)
            return 'Yes';
        return '';
     }

     
     var getRowClass = function (record) {
    
        var IsNightShift = record.data.IsNightShift;
        

        if(IsNightShift==true)
         {
            return "overnight";
         }
        
         else //if(dayValue=="Working Day")
         {
            return "";
         }
         //else 

      };


     var CommandHandler = function(column, command, record, recordIndex, cellIndex)
     {
        //if(command=='Mark')
        {
             <%= hdnEIN.ClientID %>.setValue(record.data.EmployeeId); 

            var dateStr = 
                record.data.Date.getFullYear() + '/' + (record.data.Date.getMonth() + 1) + '/' + record.data.Date.getDate();

             <%= hdnDate.ClientID %>.setValue(dateStr); 

             if(command=='Mark')
                <%=btnMark.ClientID %>.fireEvent('click');
             else if(command=='UnMark')
                <%=btnUnMark.ClientID %>.fireEvent('click');
            else if(command=='Details')
                <%=btnDetails.ClientID %>.fireEvent('click');
        }
     }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Manage Overnight Time
                </h4>
            </div>
        </div>
    </div>
    <div class="separator bottom">
        <ext:Hidden ID="hdnDate" runat="server" />
        <ext:Hidden ID="hdnEIN" runat="server" />
        <ext:LinkButton ID="btnMark" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnMark_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton ID="btnDetails" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDetails_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:LinkButton ID="btnUnMark" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnUnMark_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the Overnight marking?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
    </div>
    <div class="contentpanel">
        <div class="innerLR">
            <ext:Label ID="lblMsg" runat="server" />
            <div class="alert alert-info" style="margin-top: -15px;">
                <table class="fieldTable firsttdskip" style="margin-top: 00px !important;" runat="server"
                    id="tbl">
                    <tr>
                        <td>
                            <ext:DateField runat="server" Width="120" ID="txtFromDate" LabelAlign="Top" FieldLabel="From">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin1" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                        </td>
                        <td>
                            <ext:DateField runat="server" Width="120" ID="txtToDate" LabelAlign="Top" FieldLabel="To">
                                <Plugins>
                                    <ext:GenericPlugin ID="GenericPlugin2" runat="server" InstanceName="Ext.ux.DatePicker.HighlightWithNepDates" />
                                </Plugins>
                            </ext:DateField>
                        </td>
                        <td>
                            <ext:ComboBox Width="120" MatchFieldWidth="false" MarginSpec="0 5 0 5" LabelSeparator=""
                                LabelAlign="Top" QueryMode="Local" ID="cmbBranch" DisplayField="Name" ValueField="BranchId"
                                runat="server" FieldLabel="Branch">
                                <Items>
                                    <ext:ListItem Index="0" Value="-1" Text="All">
                                    </ext:ListItem>
                                </Items>
                                <Store>
                                    <ext:Store ID="Store5" runat="server">
                                        <Model>
                                            <ext:Model ID="Model7" IDProperty="BranchId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="BranchId" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox FieldLabel="Department" MatchFieldWidth="false" ID="cmbDepartment"
                                Width="120" MarginSpec="0 5 0 5" runat="server" ValueField="DepartmentId" DisplayField="Name"
                                LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                <Items>
                                    <ext:ListItem Index="0" Value="-1" Text="All">
                                    </ext:ListItem>
                                </Items>
                                <Store>
                                    <ext:Store ID="Store6" runat="server">
                                        <Model>
                                            <ext:Model ID="Model8" IDProperty="DepartmentId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="DepartmentId" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox ID="cmbLevel" MatchFieldWidth="false" runat="server" FieldLabel="Level/Position"
                                Width="120" MarginSpec="0 5 0 5" DisplayField="Name" ValueField="LevelId" QueryMode="Local"
                                LabelSeparator="" LabelAlign="Top">
                                <Items>
                                    <ext:ListItem Index="0" Value="-1" Text="All">
                                    </ext:ListItem>
                                </Items>
                                <Store>
                                    <ext:Store ID="storeLevel" runat="server">
                                        <Model>
                                            <ext:Model ID="modelLevel" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="LevelId" Type="Int" />
                                                    <ext:ModelField Name="Name" Type="String" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:ComboBox MatchFieldWidth="false" FieldLabel="Designation" ID="cmbDesignation"
                                Width="120" MarginSpec="0 5 0 5" runat="server" ValueField="DesignationId" DisplayField="LevelAndDesignation"
                                LabelAlign="Top" LabelSeparator="" QueryMode="Local">
                                <Items>
                                    <ext:ListItem Index="0" Value="-1" Text="All">
                                    </ext:ListItem>
                                </Items>
                                <Store>
                                    <ext:Store ID="Store7" runat="server">
                                        <Model>
                                            <ext:Model ID="Model9" IDProperty="DesignationId" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="DesignationId" />
                                                    <ext:ModelField Name="LevelAndDesignation" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </td>
                        <td>
                            <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                                <Proxy>
                                    <ext:AjaxProxy Url="../Handler/EmpSearch.ashx">
                                        <ActionMethods Read="GET" />
                                        <Reader>
                                            <ext:JsonReader Root="plants" TotalProperty="total" />
                                        </Reader>
                                    </ext:AjaxProxy>
                                </Proxy>
                                <Model>
                                    <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="EmployeeId" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                            <ext:ComboBox ID="cmbSearch" FieldLabel="Search Employee" LabelWidth="70" LabelAlign="Top"
                                runat="server" DisplayField="Name" ValueField="EmployeeId" StoreID="storeSearch"
                                TypeAhead="false" Width="150" PageSize="9999" HideBaseTrigger="true" MinChars="1"
                                TriggerAction="All" ForceSelection="true">
                                <ListConfig LoadingText="Searching..." MinWidth="200" StyleSpec="border-top:1px solid #98c0f4;">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <tpl>
                                            <div class="search-item">
                                                            <span>{Name}</span>  
                                             </div>
					                </tpl>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                </Listeners>
                            </ext:ComboBox>
                        </td>
                        <td style="padding-top: 20px; margin-left: 10px;">
                            <ext:Button ID="btnGenerate" runat="server" Cls="btn btn-default" Text="Load">
                                <Listeners>
                                    <Click Handler="searchList();" />
                                </Listeners>
                            </ext:Button>
                        </td>
                        <td style="padding-top: 20px; padding-left: 10px;">
                        </td>
                    </tr>
                </table>
            </div>
            <ext:GridPanel StyleSpec="margin-top:15px;" ID="grid" runat="server" Cls="itemgrid1"
                Width="1180" Height="600" AutoScroll="true">
                <Store>
                    <ext:Store PageSize="100" ID="storeEmpList" runat="server" AutoLoad="false" OnReadData="Store_ReadData"
                        RemotePaging="true" RemoteSort="true">
                        <Proxy>
                            <ext:PageProxy />
                        </Proxy>
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="ChkInOutID">
                                <Fields>
                                    <ext:ModelField Name="RowNumber" Type="String" />
                                    <ext:ModelField Name="EmployeeId" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Branch" Type="String" />
                                    <ext:ModelField Name="Department" Type="String" />
                                    <ext:ModelField Name="Date" Type="Date" />
                                    <ext:ModelField Name="IsNightShift" Type="Boolean" />
                                    <ext:ModelField Name="Position" Type="String" />
                                    <ext:ModelField Name="Designation" Type="String" />
                                    <ext:ModelField Name="WorkHourText" Type="String" />
                                    <ext:ModelField Name="InTimeText" Type="String" />
                                    <ext:ModelField Name="OutTimeText" Type="String" />
                                    <ext:ModelField Name="EmployeeComment" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                        <Sorters>
                            <ext:DataSorter Property="Name" Direction="ASC" />
                        </Sorters>
                    </ext:Store>
                </Store>
                <%--    <SelectionModel>
                    <ext:CheckboxSelectionModel runat="server" Mode="Multi" />
                </SelectionModel>--%>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column Locked="true" ID="colFromDate" runat="server" Text="SN" DataIndex="RowNumber"
                            MenuDisabled="false" Sortable="false" Align="Right" Width="30" />
                        <ext:Column Locked="true" ID="Column1" runat="server" Text="EIN" DataIndex="EmployeeId"
                            MenuDisabled="false" Sortable="true" Align="Right" Width="40" />
                        <ext:Column Locked="true" ID="Column3" runat="server" Text="Name" DataIndex="Name"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
                        <ext:Column Locked="false" ID="Column4" runat="server" Text="Branch" DataIndex="Branch"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                        <%--<ext:Column Locked="false" ID="Column5" runat="server" Text="Department" DataIndex="Department"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />--%>
                        <ext:Column Locked="false" ID="Column8" runat="server" Text="Position" DataIndex="Position"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                        <ext:Column Locked="false" ID="Column9" runat="server" Text="Designation" DataIndex="Designation"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="120" />
                        <ext:DateColumn Format="dd-MMM-yyyy" ID="Column2" runat="server" Text="Date" DataIndex="Date"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="100" />
                        <ext:Column ID="Column6" runat="server" Text="In Time" DataIndex="InTimeText" MenuDisabled="false"
                            Sortable="false" Align="Right" Width="120" />
                        <ext:Column ID="Column10" runat="server" Text="Out Time" DataIndex="OutTimeText"
                            MenuDisabled="false" Sortable="false" Align="Right" Width="120" />
                        <ext:Column ID="Column11" runat="server" Text="Worked hr" DataIndex="WorkHourText"
                            MenuDisabled="false" Sortable="false" Align="Right" Width="70" />
                        <ext:Column ID="Column7" runat="server" Text="Overnight" DataIndex="IsNightShift"
                            MenuDisabled="false" Sortable="true" Align="Center" Width="70">
                            <Renderer Fn="overnightRender" />
                        </ext:Column>
                        <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                            Width="180" Align="Center">
                            <Commands>
                                <ext:GridCommand Text="Mark" CommandName="Mark" />
                            </Commands>
                            <Commands>
                                <ext:GridCommand Text="Un-Mark" CommandName="UnMark" />
                            </Commands>
                            <Commands>
                                <ext:GridCommand Text="Details" CommandName="Details" />
                            </Commands>
                            <Listeners>
                                <Command Fn="CommandHandler">
                                </Command>
                            </Listeners>
                        </ext:CommandColumn>
                    </Columns>
                </ColumnModel>
                <View>
                    <ext:GridView ID="GridView1" runat="server">
                        <Listeners>
                        </Listeners>
                        <GetRowClass Fn="getRowClass" />
                    </ext:GridView>
                </View>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" StoreID="storeEmpList" DisplayInfo="true">
                    </ext:PagingToolbar>
                </BottomBar>
            </ext:GridPanel>
        </div>
    </div>
    <ext:Window runat="server" Hidden="true" Title="Marking List" ID="windowMark" Modal="false"
        Width="350" Height="300">
        <Content>
            <table class="fieldTable" style="margin-left: 10px;">
                <tr>
                    <td>
                        <ext:DisplayField LabelSeparator="" LabelAlign="Top" runat="server" ID="dispEmployee"
                            Width="300">
                        </ext:DisplayField>
                    </td>
                    <%--  <td>
                        <ext:DisplayField LabelSeparator="" FieldLabel="Date" LabelAlign="Top" runat="server"
                            ID="dispDate" Width="200">
                        </ext:DisplayField>
                    </td>--%>
                    <td>
                    </td>
                </tr>
            </table>
            <ext:GridPanel StyleSpec="margin-top:15px;margin-left:10px;" ID="gridMarkingList"
                runat="server" Cls="itemgrid1" Width="200" Height="150" AutoScroll="true">
                <Store>
                    <ext:Store ID="store1" runat="server" AutoLoad="true">
                        <Model>
                            <ext:Model ID="Model3" runat="server" IDProperty="ChkInOutID">
                                <Fields>
                                    <ext:ModelField Name="Time" Type="String" />
                                    <ext:ModelField Name="ChkInOutID" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <SelectionModel>
                    <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi" />
                </SelectionModel>
                <ColumnModel ID="ColumnModel2" runat="server">
                    <Columns>
                        <ext:Column Locked="true" ID="Column14" runat="server" Text="Time" DataIndex="Time"
                            MenuDisabled="false" Sortable="true" Align="Left" Width="150" />
                    </Columns>
                </ColumnModel>
            </ext:GridPanel>
            <ext:Button StyleSpec="margin-top:10px;margin-left:10px;" ID="btnSaveNightShift"
                runat="server" Cls="btn btn-primary" Text="Set Overnight">
                <DirectEvents>
                    <Click OnEvent="btnSaveNightShift_Save">
                        <EventMask ShowMask="true" />
                        <Confirmation ConfirmRequest="true" Message="Confirm make the selected time as overnight shift or prev day punch out?" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Content>
    </ext:Window>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
