﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using System.Data;
using System.Data.OleDb;
using System.IO;
using BLL.BO;

namespace Web.CP
{
    public partial class DefinedSalaryExcel : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                {
                    return false;
                }
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public int incomeID = 0;
        public bool isDeduction = false;
        public List<int> incomeIDs = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                bodyBlock.InnerHtml = "Income not selected";   
                return;
            }
            incomeID = int.Parse(Request.QueryString["ID"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdf", "var hasImport = " + (HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            var template = ("~/App_Data/ExcelTemplate/DefinedSalaryImport.xlsx");

            var dateId = NewPayrollManager.GetLatestDateForIncome(incomeID);
            var income = new PayManager().GetIncomeById(incomeID);
            var xlist = NewPayrollManager.GetDefinedList(incomeID, true);
            var ylist = NewPayrollManager.GetDefinedList(incomeID, false);
            var values = NewPayrollManager.GetDefinedAmounts(incomeID, dateId);

            if (income.XType != null && income.XType != 0 && (income.YType == null || income.YType == 0))
            {
                ExcelGenerator.WriteDefinedXSingleColumnIncomeForExportExcel(template, xlist, values, ((IncomeDefinedTypeEnum)income.XType).ToString());
            }
            else
            {
                ExcelGenerator.WriteDefinedIncomeForExportExcel(template, xlist, ylist, values);
            }
        }


        private List<int> GetEmployeeIdList()
        {
            return new List<int>();
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            var template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                var datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


                var xTypeItem = 0;
                float? value;

                var importedCount = 0;


                try
                {
                    var projectPays = new List<PIncomeDefinedAmount>();
                    var xlist = NewPayrollManager.GetDefinedList(incomeID, true);
                    var ylist = NewPayrollManager.GetDefinedList(incomeID, false);
                    var income = new PayManager().GetIncomeById(incomeID);
                    var name = string.Empty;
                    var rowNumber = 1;

                    if (income.XType != null && income.XType != 0 && (income.YType == null || income.YType == 0))
                    {
                        var columnName = ((IncomeDefinedTypeEnum)income.XType).ToString();

                        foreach (DataRow row in datatable.Rows)
                        {
                            name = row[columnName].ToString();

                            foreach (TextValue item in xlist)
                            {
                                if (item.Text == name)
                                {
                                    xTypeItem = int.Parse(item.Value);
                                    break;
                                }
                            }

                            rowNumber += 1;
                            importedCount += 1;



                            value = ExcelGenerator.GetValueFromCell("Amount", row, xTypeItem);
                            if (value == null)
                            {
                                value = (float)0;
                            }
                            if (value != null)
                            {
                                var projectPay = new PIncomeDefinedAmount();
                                projectPay.IncomeId = incomeID;
                                projectPay.XType = xTypeItem;
                                projectPay.YType = 0;
                                projectPay.Amount = (decimal)value;
                                projectPays.Add(projectPay);
                            }
                        }
                    }
                    else
                    {
                        foreach (DataRow row in datatable.Rows)
                        {
                            name = row["Name"].ToString();

                            foreach (TextValue item in ylist)
                            {
                                if (item.Text == name)
                                {
                                    xTypeItem = int.Parse(item.Value);
                                    break;
                                }
                            }

                            rowNumber += 1;
                            importedCount += 1;

                            foreach (var xItem in xlist)
                            {
                                value = ExcelGenerator.GetValueFromCell(xItem.Text, row, xTypeItem);
                                if (value == null)
                                {
                                    value = (float)0;
                                }
                                if (value != null)
                                {
                                    var projectPay = new PIncomeDefinedAmount();
                                    projectPay.IncomeId = incomeID;
                                    projectPay.XType = int.Parse(xItem.Value);
                                    projectPay.YType = xTypeItem;
                                    projectPay.Amount = (decimal)value;
                                    projectPays.Add(projectPay);
                                }
                            }
                        }
                    }
                    var status = NewPayrollManager.SaveUpdateDefinedIncomeAmount(projectPays, incomeID);

                    if (status.IsSuccess == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }


                    divMsgCtl.InnerHtml = "Import successfull";
                    divMsgCtl.Hide = false;

                    HasImport = true;
                }
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                divWarningMsg.InnerHtml = "Excel version should be 2007(.xlsx) or higher.";
                divWarningMsg.Hide = false;
            }
        }
    }
}

