﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;

namespace Web.CP
{
    public partial class EmployeeHomeSalary : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
               Initialize();

                              

            }
        }

        private void Initialize()
        {
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataSource = branchList;
            cmbBranch.Store[0].DataBind();

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            cmbDepartment.Store[0].DataBind();

            cmbDesignation.Store[0].DataSource = new CommonManager().GetAllDesignations().OrderBy(x => x.LevelAndDesignation).ToList();
            cmbDesignation.Store[0].DataBind();

            this.storeLevel.DataSource = ActivityManager.GetLevels().OrderBy(x => x.Name);
            this.storeLevel.DataBind();


            int? incomeID1 = null;
            int? incomeID2 = null;
            int? incomeID3 = null;
            int? incomeID4 = null;
            int? incomeID5 = null;
            int? incomeID6 = null;
            EmployeeManager.SetIncomeListForEmployeeHomeSalaryReport(ref incomeID1, ref incomeID2, ref incomeID3, ref incomeID4, ref incomeID5, ref incomeID6);
            PayManager mgr = new PayManager();

            PIncome income = null;
            if (incomeID1 != null)
            {                
                income = mgr.GetIncomeById(incomeID1.Value);
                ColumnAIncome1.Text = income.Title;
                ColumnIncome1.Text = income.Title;
            }

            if (incomeID2 != null)
            {
                income = mgr.GetIncomeById(incomeID2.Value);
                ColumnAIncome2.Text = income.Title;
                ColumnIncome2.Text = income.Title;
            }
            else
            {
                ColumnAIncome2.Hide();
                ColumnIncome2.Hide();
            }

            if (incomeID3 != null)
            {
                income = mgr.GetIncomeById(incomeID3.Value);
                ColumnAIncome3.Text = income.Title;
                ColumnIncome3.Text = income.Title;
            }
            else
            {
                ColumnAIncome3.Hide();
                ColumnIncome3.Hide();
            }

            if (incomeID4 != null)
            {
                income = mgr.GetIncomeById(incomeID4.Value);
                ColumnAIncome4.Text = income.Title;
                ColumnIncome4.Text = income.Title;
            }
            else
            {
                ColumnAIncome4.Hide();
                ColumnIncome4.Hide();
            }


            if (incomeID5 != null)
            {
                income = mgr.GetIncomeById(incomeID5.Value);
                ColumnAIncome5.Text = income.Title;
                ColumnIncome5.Text = income.Title;
            }
            else
            {
                ColumnAIncome5.Hide();
                ColumnIncome5.Hide();
            }


            if (incomeID6 != null)
            {
                income = mgr.GetIncomeById(incomeID6.Value);
                ColumnAIncome6.Text = income.Title;
                ColumnIncome6.Text = income.Title;
            }
            else
            {
                ColumnAIncome6.Hide();
                ColumnIncome6.Hide();
            }
        }

        private void Clear()
        {
            
        }

       
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;
           
            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            List<Report_GetHomeSalaryResult> list = EmployeeManager.GetHomeSalaryReport
                (branchId, depId, levelId, designationId, employeeId, e.Page-1, e.Limit, (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower(), ref totalRecords);

            e.Total = totalRecords;

            if (list.Any())
                storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

     
        public void btnExport_Click(object sender, EventArgs e)
        {
            int totalRecords = 0;
            int branchId = -1, depId = -1, levelId = -1, designationId = -1;

            if (cmbBranch.SelectedItem != null && cmbBranch.SelectedItem.Value != null)
                branchId = int.Parse(cmbBranch.SelectedItem.Value);
            if (cmbDepartment.SelectedItem != null && cmbDepartment.SelectedItem.Value != null)
                depId = int.Parse(cmbDepartment.SelectedItem.Value);
            if (cmbLevel.SelectedItem != null && cmbLevel.SelectedItem.Value != null)
                levelId = int.Parse(cmbLevel.SelectedItem.Value);
            if (cmbDesignation.SelectedItem != null && cmbDesignation.SelectedItem.Value != null)
                designationId = int.Parse(cmbDesignation.SelectedItem.Value);

            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;


            List<Report_GetHomeSalaryResult> list = EmployeeManager.GetHomeSalaryReport
                (branchId, depId, levelId, designationId, employeeId, 0, 9999999, ("").ToLower(), ref totalRecords);


            int? incomeID1 = null;
            int? incomeID2 = null;
            int? incomeID3 = null;
            int? incomeID4 = null;
            int? incomeID5 = null;
            int? incomeID6 = null;
            EmployeeManager.SetIncomeListForEmployeeHomeSalaryReport(ref incomeID1, ref incomeID2, ref incomeID3, ref incomeID4, ref incomeID5,ref incomeID6);
            List<string> hiddenList = new List<string>{"IncomeList","TaxList","ID","TotalRows","ACIT","CIT"};
            hiddenList.Add("TotalRows");
            if (incomeID2 == null)
            {
                hiddenList.Add("Income2");
                hiddenList.Add("AIncome2");
            }
            if (incomeID3 == null)
            {
                hiddenList.Add("Income3");
                hiddenList.Add("AIncome3");
            }
            if (incomeID4 == null)
            {
                hiddenList.Add("Income4");
                hiddenList.Add("AIncome4");
            }
            if (incomeID5 == null)
            {
                hiddenList.Add("Income5");
                hiddenList.Add("AIncome5");
            }
            if (incomeID6 == null)
            {
                hiddenList.Add("Income6");
                hiddenList.Add("AIncome6");
            }
            Dictionary<string, string> renameList = new Dictionary<string, string>();
         
            renameList.Add("RowNumber", "SN");
            renameList.Add("EmployeeId", "EIN");
            renameList.Add("IDCardNo", "I No");
            renameList.Add("APFIncome", "PF Income");
            renameList.Add("AGrossSalary", "GrossSalary");
        renameList.Add("APFDeduction", "PF Deduction");
            renameList.Add("ACIT", "CIT");
        renameList.Add("ATax","Tax");
        renameList.Add("AnnualNetSalary","Annual Net Salary");
        renameList.Add("PFIncome", "PF Income");



            PayManager mgr = new PayManager();
            PIncome income = null;
            if (incomeID1 != null)
            {
                income = mgr.GetIncomeById(incomeID1.Value);
                renameList.Add("Income1", income.Title);
                renameList.Add("AIncome1", income.Title);
            }

            if (incomeID2 != null)
            {
                income = mgr.GetIncomeById(incomeID2.Value);
                renameList.Add("Income2", income.Title);
                renameList.Add("AIncome2", income.Title);
            }

            if (incomeID3 != null)
            {
                income = mgr.GetIncomeById(incomeID3.Value);
                renameList.Add("Income3", income.Title);
                renameList.Add("AIncome3", income.Title);
            }

            if (incomeID4 != null)
            {
                income = mgr.GetIncomeById(incomeID4.Value);
                renameList.Add("Income4", income.Title);
                renameList.Add("AIncome4", income.Title);
            }

            if (incomeID5 != null)
            {
                income = mgr.GetIncomeById(incomeID5.Value);
                renameList.Add("Income5", income.Title);
                renameList.Add("AIncome5", income.Title);
            }

            if (incomeID6 != null)
            {
                income = mgr.GetIncomeById(incomeID6.Value);
                renameList.Add("Income6", income.Title);
                renameList.Add("AIncome6", income.Title);
            }

            

            Bll.ExcelHelper.ExportToExcel("Employee Home Salary List", list,
                hiddenList,
            new List<String>() {  },
            renameList,
            new List<string>() {      
            "AIncome1","AIncome2","AIncome3","AIncome4","AIncome5","AIncome6","APFIncome","AGrossSalary","APFDeduction","ACIT"
                ,"ATax" ,"AnnualNetSalary","Income1","Income2","Income3","Income4","Income5","Income6","PFIncome" ,"GrossSalary","PFDeduction","CIT","Tax","NetSalary"
            }
            , new Dictionary<string, string>() {
            }
            , new List<string> { "RowNumber", "EmployeeId", "IDCardNo", "NAME", "Branch", "Department", "Position","Designation","AccountNo",
                "Income1","Income2","Income3","Income4","Income5","Income6","PFIncome" ,"GrossSalary","PFDeduction","CIT","Tax","NetSalary", 
                "AIncome1","AIncome2","AIncome3","AIncome4","AIncome5","AIncome6","APFIncome","AGrossSalary","APFDeduction","ACIT","ATax" ,"AnnualNetSalary"
                  });


        }


    }
}