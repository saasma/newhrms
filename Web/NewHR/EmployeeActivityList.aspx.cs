﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL.Entity;
using BLL;
using System.IO;
using Utils;
using System.Text;
using BLL.BO;

namespace Web.NewHR
{
    public partial class EmployeeActivityList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                BindCombos();

                if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Sana)
                {
                    lblActivityClients.Hide();
                    tdClients.InnerText = "";
                    ColClients.Hide();
                    gridActivity.Width = 960;
                    TabPanel1.Width = 960;
                }

            }
        }

        private void BindCombos()
        {
            storeClient.DataSource = ListManager.GetAllClients();
            storeClient.DataBind();

            storeType.DataSource = ListManager.GetActivityTypes();
            storeType.DataBind();
        }

        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            int totalRecords = 0;
            int clientId = -1;
            int activityTypeId = -1;
            DateTime? startDate = null, endDate = null;

            if (cmbClientFilter.SelectedItem != null && cmbClientFilter.SelectedItem.Value != null)
                clientId = int.Parse(cmbClientFilter.SelectedItem.Value);

            if (cmbTypeFilter.SelectedItem != null && cmbTypeFilter.SelectedItem.Value != null)
                activityTypeId = int.Parse(cmbTypeFilter.SelectedItem.Value);

            int type = 0, employeeId = -1;
            bool all = false;

            type = TabPanel1.ActiveTabIndex + 1;

            if (TabPanel1.ActiveTabIndex == 5)
            {
                type = -1;
                all = true;

                if (txtStartDate.SelectedDate != new DateTime())
                {
                    startDate = txtStartDate.SelectedDate;

                    if (txtEndDate.SelectedDate == new DateTime())
                    {
                        NewMessage.ShowWarningMessage("To Date is required for date filter.");
                        txtEndDate.Focus();
                        return;
                    }
                }

                if (txtEndDate.SelectedDate != new DateTime())
                {
                    endDate = txtEndDate.SelectedDate;

                    if (txtStartDate.SelectedDate == new DateTime())
                    {
                        NewMessage.ShowWarningMessage("From Date is required for date filter.");
                        txtStartDate.Focus();
                        return;
                    }
                }
            }

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbSearch.SelectedItem.Value);

            int pageSize = int.Parse(cmbPageSize.SelectedItem.Value); ;
            List<GetActivityListForAdminResult> list = NewHRManager.GetActivityForAdmin(e.Start / pageSize, pageSize, employeeId, clientId, activityTypeId, type, startDate, endDate, all, -1);

            if (list.Count > 0)
                totalRecords = list[0].TotalRows.Value;

            e.Total = totalRecords;

            storeActivity.DataSource = list;
            storeActivity.DataBind();

        }

        protected void btnMarkRead_Click(object sender, DirectEventArgs e)
        {
            int detailId = int.Parse(hdnActivityId.Text);

            Status status = NewHRManager.MarkNewActivityAsApproved(detailId);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Activity marked as read successfully.");
                X.Js.Call("searchList");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }

        protected void btnMarkAllAsRead_Click(object sender, DirectEventArgs e)
        {
            List<GetActivityListForAdminResult> listResult = new List<GetActivityListForAdminResult>();

            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetActivityListForAdminResult> list = JSON.Deserialize<List<GetActivityListForAdminResult>>(gridItemsJson);
            listResult.AddRange(list);

            if (listResult.Count == 0)
            {
                NewMessage.ShowWarningMessage("Select the activities to approve.");
                return;
            }

            Status status = NewHRManager.SaveNewActivitiesListAsApproved(listResult);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();

                NewMessage.ShowNormalMessage("Activities Approved saved successfully.");
                X.Js.Call("searchList");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }

        protected void btnSaveAndSend_Click(object sender, DirectEventArgs e)
        {
            List<GetActivityListForAdminResult> listResult = new List<GetActivityListForAdminResult>();

            string gridItemsJson = e.ExtraParams["gridItems"];
            List<GetActivityListForAdminResult> list = JSON.Deserialize<List<GetActivityListForAdminResult>>(gridItemsJson);
            listResult.AddRange(list);

            if (listResult.Count == 0)
            {
                NewMessage.ShowWarningMessage("Select the activities to Save and Send.");
                return;
            }

            Status status = NewHRManager.SaveAndSendNewActivitiesList(listResult);
            if (status.IsSuccess)
            {
                CheckboxSelectionModel1.ClearSelection();

                NewMessage.ShowNormalMessage("Activities Save and Send saved successfully.");

                X.Js.Call("searchList");
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);
        }

        private void Clear()
        {
            lblActivityDate.Text = "";
        }

        protected void btnView_Click(object sender, DirectEventArgs e)
        {
            int detailId = int.Parse(hdnActivityId.Text);
            NewActivityDetail obj = NewHRManager.GetNewActivityDetailById(detailId);
            txtComment.Text = "";

            if (obj != null)
            {
                lblActivityDate.Text = string.Format("{0}, {1}", obj.DateEng.Value.DayOfWeek, obj.DateEng.Value.ToString("MMMMM dd"));

                if(obj.CreatedOn != null)
                    lblActivityDateDetls.Html = "<span style='color:#356FA2;'>" + "Submitted &nbsp;&nbsp; " + "</span>" + obj.CreatedOn.Value.ToString("MMMMM dd, yyyy hh:mm:ss tt");

                int employeeId = obj.EmployeeId.Value;
                EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
                bool hasPhoto = false;
                if (emp.HHumanResources.Count > 0)
                {
                    if (!string.IsNullOrEmpty(emp.HHumanResources[0].UrlPhotoThumbnail))
                    {
                        img.ImageUrl = Page.ResolveUrl(Config.UploadLocation + "/" + emp.HHumanResources[0].UrlPhotoThumbnail);
                        if (File.Exists(Server.MapPath(img.ImageUrl)))
                            hasPhoto = true;
                    }
                }
                if (!hasPhoto)
                {
                    img.ImageUrl = Page.ResolveUrl(Web.Helper.WebHelper.GetEmployeeNoImage(employeeId));
                }

                lblEmpName.Text = emp.Name;
                lblDesignation.Text = emp.EDesignation.Name;

                DateTime date = obj.DateEng.Value;
                //int i = 0;
                //foreach (var item in obj.NewActivityDetails)
                //{
                //    i++;
                //    item.SN = i;
                //    item.StartTimeDate = new DateTime(date.Year, date.Month, date.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, 0);

                //    if (item.EndTime != null)
                //    {
                //        TimeSpan tsDiff = item.EndTime.Value - item.StartTime.Value;
                //        item.DurationTime = tsDiff.Hours + ":" + tsDiff.Minutes.ToString().PadLeft(2, '0');
                //    }
                //}

                //gridActivityAdd.Store[0].DataSource = obj.NewActivityDetails;
                //gridActivityAdd.Store[0].DataBind();

                lblActivityType.Text = hdnActivityType.Text;

                lblActivityClients.Text = hdnClients.Text;
                lblActivityDuration.Text = hdnActivityDuration.Text;
                txtActivityDetails.Text = obj.Description;

                portalPlannedActivities.Hide();

                EmployeePlan dbEmpPlan = NewHRManager.GetEmployeePlanByDate(obj.DateEng.Value, obj.EmployeeId.Value);

                if (dbEmpPlan != null)
                {
                    if (obj != null)
                    {
                        gridPlan.Store[0].DataSource = dbEmpPlan.EmployeePlanDetails;
                        gridPlan.Store[0].DataBind();

                        Portlet1.Collapse();
                        portalPlannedActivities.Show();
                    }
                }

                LoadActivityCommentDetails(detailId);

                BindNoOfActivityButtonList(obj.EmployeeId.Value, obj.DateEng.Value);


                WActivity.Center();
                WActivity.Show();
            }
        }

        //[DirectMethod]
        //public static string GetGrid(Dictionary<string, string> parameters)
        //{
        //    int activityId = int.Parse(parameters["id"]);

        //    NewActivity obj = NewHRManager.GetNewActivityById(activityId);
        //    if (obj != null)
        //    {
        //        List<object> data = new List<object>();

        //        int i = 0;
        //        DateTime date = obj.DateEng.Value;

        //        foreach (var item in obj.NewActivityDetails)
        //        {
        //            i++;
        //            item.SN = i;
        //            item.StartTimeDate = new DateTime(date.Year, date.Month, date.Day, item.StartTime.Value.Hours, item.StartTime.Value.Minutes, 0);

        //            if (item.EndTime != null)
        //            {
        //                TimeSpan tsDiff = item.EndTime.Value - item.StartTime.Value;
        //                item.DurationTime = tsDiff.Hours + ":" + tsDiff.Minutes.ToString().PadLeft(2, '0');
        //            }

        //            item.ClientName = ListManager.GetClientById(item.ClientSoftwareId.Value).ClientName;
        //            item.ActivityName = ListManager.GetActivityTypeById(item.ActivityType.Value).Name;

        //            data.Add(new
        //            {
        //                SN = i,
        //                Client = item.ClientName,
        //                ActType = item.ActivityName,
        //                StartTimeDate = item.StartTimeDate.ToString("HH:mm tt"),
        //                DurationTime = item.DurationTime,
        //                Representative = item.Representative,
        //                Description = item.Description
        //            });
        //        }

        //        int count = obj.NewActivityDetails.Count();
        //        int height = 42 + 32 * count;

        //        GridPanel grid = new GridPanel
        //        {
        //            Height = height,
        //            EnableColumnHide = false,
        //            Store = 
        //        { 
        //            new Store 
        //            { 
        //                Model = {
        //                    new Model {
        //                        IDProperty = "SN",
        //                        Fields = 
        //                        {
        //                            new ModelField("SN"),
        //                            new ModelField("Client"),
        //                            new ModelField("ActType"),
        //                            new ModelField("StartTimeDate"),
        //                            new ModelField("DurationTime"),
        //                            new ModelField("Representative"),
        //                            new ModelField("Description")
        //                        }
        //                    }
        //                },
        //                DataSource = data
        //            }
        //        },
        //            ColumnModel =
        //            {
        //                Columns = 
        //            { 
        //                new Column { Text = "SN", DataIndex = "SN", Width=10, Sortable = false, MenuDisabled = true, Hidden=true },
        //                new Column { Text = "Client", DataIndex = "Client", Width=150, Sortable = false, MenuDisabled = true },
        //                new Column { Text = "Type", DataIndex = "ActType", Width=150, Sortable = false, MenuDisabled = true },
        //                new Column { Text = "Start Time", DataIndex = "StartTimeDate", Width=90, Sortable = false, MenuDisabled = true, Align = Alignment.Center },
        //                new Column { Text = "Duration(h:m)", DataIndex = "DurationTime", Width=90, Sortable = false, MenuDisabled = true, Align = Alignment.Center },
        //                new Column { Text = "Person", DataIndex = "Representative", Width=180, Sortable = false, MenuDisabled = true },
        //                new Column { Text = "Description", DataIndex = "Description", Width=430, Sortable = false, MenuDisabled = true, Wrap = true  }
        //            }
        //            }
        //        };

        //        return ComponentLoader.ToConfig(grid);
        //    }
        //    else
        //        return "";
        //}

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("SaveComment");
            if (Page.IsValid)
            {
                
                int activityId = int.Parse(hdnActivityId.Text);
                ActivityComment obj = new ActivityComment()
                {
                    ActivityId = activityId,
                    Comment = txtComment.Text.Trim(),
                    CreatedBy = SessionManager.CurrentLoggedInUserID,
                    CreatedOn = DateTime.Now
                };

                Status status = NewHRManager.SaveActivityComment(obj);
                if (status.IsSuccess)
                {
                    WActivity.Close();
                    NewMessage.ShowNormalMessage("Activity comment saved successfully.");
                    X.Js.Call("searchList");
                }
                else
                    NewMessage.ShowWarningMessage("Error in activity comment saving.");
            }
        }

        protected void btnSaveAndSendAct_Click(object sender, DirectEventArgs e)
        {
            int activityId = int.Parse(hdnActivityId.Text);
            ActivityComment obj = new ActivityComment();
            obj.ActivityId = activityId;

            if(!string.IsNullOrEmpty(txtComment.Text.Trim()))
            {               
                obj.Comment = txtComment.Text.Trim();
                obj.CreatedBy = SessionManager.CurrentLoggedInUserID;
                obj.CreatedOn = DateTime.Now;
            }

            if (obj.Comment.Length > 2000)
            {
                NewMessage.ShowWarningMessage("Maxmium comment lenght is 2000 characters.");
                txtComment.Focus();
                return;
            }

            Status status = NewHRManager.SaveActivityComment(obj);
            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Activity saved and sent saved successfully.");

                txtComment.Clear();
                LoadActivityCommentDetails(activityId);

                X.Js.Call("searchList");
            }
            else
                NewMessage.ShowWarningMessage("Error in activity saving and sending.");
        }

        private void BindNoOfActivityButtonList(int employeeId, DateTime activityDate)
        {
            List<GetActivityListForAdminResult> list = NewHRManager.GetActivityForAdmin(0, 1000, employeeId, -1, -1, -1, activityDate.Date, activityDate.Date, true, -1);

            int i = 1;
            if (list.Count > 1)
            {
                foreach (var item in list)
                {
                    item.SN = i;

                    if (int.Parse(hdnActivityId.Text) == item.DetailId)
                        item.ClsName = "ActivitySelectedCls";
                    else
                        item.ClsName = "ActivityCls";

                    i++;
                }

                storeActivityDV.DataSource = list;
                storeActivityDV.DataBind();
                dvActivity.Show();
            }
            else
                dvActivity.Hide();
        }


        private void LoadActivityCommentDetails(int activityId)
        {
            dfComments.Html = string.Empty;
            List<ActivityComment> listActivityComments = NewHRManager.GetHRActivityComments(activityId);

            if (listActivityComments.Count > 0)
            {
                List<EmpActivityCommentCls> listComments = new List<EmpActivityCommentCls>();


                foreach (var item in listActivityComments)
                {
                    Guid commentedByUserID = item.CreatedBy.Value;
                    string commentBy = "";

                    UUser objUUser = UserManager.GetUserByUserID(commentedByUserID);
                    if (objUUser.UserMappedEmployeeId != null)
                    {
                        EEmployee createdUserEmp = EmployeeManager.GetEmployeeById(objUUser.UserMappedEmployeeId.Value);
                        commentBy = createdUserEmp.Name;
                    }
                    else
                        commentBy = (string.IsNullOrEmpty(objUUser.NameIfNotEmployee) ? objUUser.UserName : objUUser.NameIfNotEmployee);

                    commentBy = commentBy.Split(' ')[0].ToString();


                    EmpActivityCommentCls objEmpActivityCommentCls = new EmpActivityCommentCls()
                    {
                        CommentBy = commentBy,
                        CommentedOn = item.CreatedOn.Value,
                        Comment = item.Comment
                    };

                    listComments.Add(objEmpActivityCommentCls);
                }


                List<ActivityEmpComment> listEmpActivityComments = NewHRManager.GetEmpActivityCommentsByActivityId(activityId);

                foreach (var item in listEmpActivityComments)
                {
                    string commentBy = "";

                    EEmployee emp = EmployeeManager.GetEmployeeById(item.EmployeeId.Value);
                    commentBy = emp.Name.Split(' ')[0].ToString();

                    EmpActivityCommentCls objEmpActivityCommentCls = new EmpActivityCommentCls()
                    {
                        CommentBy = commentBy,
                        CommentedOn = item.CreatedOn.Value,
                        Comment = item.Comment
                    };

                    listComments.Add(objEmpActivityCommentCls);
                }

                listComments = listComments.OrderByDescending(x => x.CommentedOn).ToList();

                foreach (var item in listComments)
                {
                    dfComments.Html += "<span style='color:#356FA2;'>" + item.CommentBy + "</span>" + "<span style='font-style: italic; margin-left:8px;'>" + item.CommentedOn.ToString("dd MMMMM hh:mm") + "</span>" + "<br/>" + "<span style='margin-top:5px !important;'>" + item.Comment + "</span>" + "<br/><br/>";
                }

            }
            else
            {
                dfComments.Clear();

            }
        }
    }
}