﻿<%@ Page Title="6 Month Complete List" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="EmployeePeriodSixMonthComplete.aspx.cs" Inherits="Web.NewHR.EmployeePeriodSixMonthComplete" %>

<%@ Register Src="~/newhr/UserControls/EmployeeContractTabStrip.ascx" TagName="EmployeeContractTabStripCtl"
    TagPrefix="ucEmployeeContractTabStrip" %>
<%@ Register Src="~/NewHR/UserControls/ActingListCtl.ascx" TagName="FamilyCtl" TagPrefix="uc1" %>
<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <script>
    function focusEvent(e1, tab) {

        if (tab.title.toString().toLowerCase() == "this month") {
            
            <%= btnThisMonth.ClientID %>.fireEvent('click');
        }
           else if (tab.title.toString().toLowerCase() == "last month") {
            
            <%= btnLastMonth.ClientID %>.fireEvent('click');
        }
           else if (tab.title.toString().toLowerCase() == "next month") {
            
            <%= btnNextMonth.ClientID %>.fireEvent('click');
        }
         

        
    };
    </script>
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <ext:Hidden ID="hdnFilterValue" runat="server">
    </ext:Hidden>
    <div class="separator bottom">
    </div>
    <ext:Button ID="btnThisMonth" runat="server" Text="This Month" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnThisMonth_Change">
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnNextMonth" runat="server" Text="This Year" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnNextMonth_Change">
            </Click>
        </DirectEvents>
    </ext:Button>
    <ext:Button ID="btnLastMonth" runat="server" Text="Last Month" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnLastMonth_Change">
            </Click>
        </DirectEvents>
    </ext:Button>
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    6 Month Complete
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel" style="padding-top: 0px">
        <div class="widget">
            <ucEmployeeContractTabStrip:EmployeeContractTabStripCtl Id="EmployeeContractTabStripCtl1"
                runat="server" />
            <div class="widget-body">
                <div>
                    <ext:TabPanel ID="TabPanel_Main" ActiveIndex="0" Border="false" Unstyled="true" runat="server"
                        Plain="true" OverflowY="Auto">
                        <Items>
                            <ext:Panel ID="Panel_ThisMonth" runat="server" AutoHeight="true" Title="This Month"
                                Header="False" Border="false" OverflowY="Auto">
                                <Content>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_NextMonth" AutoHeight="true" runat="server" Title="Next Month"
                                Header="False" Border="false">
                                <Content>
                                </Content>
                            </ext:Panel>
                            <ext:Panel ID="Panel_LastMonth" AutoHeight="true" runat="server" Title="Last Month"
                                Header="False" Border="false">
                                <Content>
                                </Content>
                            </ext:Panel>
                        </Items>
                        <Listeners>
                            <TabChange Fn="focusEvent">
                            </TabChange>
                        </Listeners>
                    </ext:TabPanel>
                </div>
                <ext:Store runat="server" ID="storeSearch" AutoLoad="false">
                    <Proxy>
                        <ext:AjaxProxy Url="../Handler/EmpSearchID.ashx">
                            <ActionMethods Read="GET" />
                            <Reader>
                                <ext:JsonReader Root="plants" TotalProperty="total" />
                            </Reader>
                        </ext:AjaxProxy>
                    </Proxy>
                    <Model>
                        <ext:Model ID="Model2" IDProperty="Name" Name="ItemLineModel" runat="server">
                            <Fields>
                                <ext:ModelField Name="Name" Type="String" />
                                <ext:ModelField Name="EmployeeId" Type="String" />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
                <div class="right">
                    <asp:LinkButton ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                        CssClass=" excel marginRight" Style="float: left;" />
                </div>
                <div style="clear: both">
                </div>
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridEmployee" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="Store3" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="EmployeeId">
                                    <Fields>
                                        <ext:ModelField Name="EmployeeId" Type="string" />
                                         <ext:ModelField Name="IdCardNo" Type="String" />
                                        <ext:ModelField Name="Name" Type="string" />
                                        <ext:ModelField Name="Branch" Type="string" />
                                        <ext:ModelField Name="Department" Type="string" />
                                        <ext:ModelField Name="Level" Type="string" />
                                        <ext:ModelField Name="Designation" Type="string" />
                                        <ext:ModelField Name="JoinDateEng" Type="string" />
                                        <ext:ModelField Name="JoinDateNepali" Type="string" />
                                        <ext:ModelField Name="ServicePeriod" Type="string" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="Column9" Sortable="false" MenuDisabled="true" runat="server" Text="EIN"
                                Align="Left" Width="60" DataIndex="EmployeeId" />
                              <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="I No"
                            Align="Left" Width="60" DataIndex="IdCardNo" />
                            <ext:Column ID="Column8" Sortable="false" MenuDisabled="true" runat="server" Text="Employee"
                                Align="Left" Width="180" DataIndex="Name" />
                            <ext:Column ID="Column6" Width="100" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Designation" Align="Left" DataIndex="Designation">
                            </ext:Column>
                            <ext:Column ID="Column2" Width="140" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Level" Align="Left" DataIndex="Level">
                            </ext:Column>
                            <ext:Column ID="ColumnBranch" Width="90" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Branch" Align="Left" DataIndex="Branch">
                            </ext:Column>
                            <ext:Column ID="ColumnDepartment" Width="90" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Depratment" Align="Left" DataIndex="Department">
                            </ext:Column>
                            <ext:DateColumn ID="columnJoinDate1" Width="120" Sortable="false" MenuDisabled="true"
                                runat="server" Text="Join Date (A.D)" Align="Left" DataIndex="JoinDateEng">
                            </ext:DateColumn>
                            <ext:DateColumn ID="ColumnsBS" Width="120" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Join Date (B.S)" Align="Left" DataIndex="JoinDateNepali">
                            </ext:DateColumn>
                            <ext:Column ID="columns" Width="210" Sortable="false" MenuDisabled="true" runat="server"
                                Text="Service Period" Align="Left" DataIndex="ServicePeriod">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2111" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
                <div style="clear: both">
                </div>
            </div>
        </div>
    </div>
    <br />
</asp:Content>
