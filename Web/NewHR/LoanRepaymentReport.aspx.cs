﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using Utils.Helper;
using System.IO;
using Utils;
using Utils.Security;
using Utils.Calendar;
using Web.Helper;

namespace Web.CP
{
    public partial class LoanRepaymentReport : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.HumanResource;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
               Initialize();

                              

            }
        }

        private void Initialize()
        {
            PayrollPeriod period = CommonManager.GetLastPayrollPeriod();

            //calDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            cmbYear.Store[0].DataSource = CommonManager.GetYearList();
            cmbYear.Store[0].DataBind();

            ExtControlHelper.ComboBoxSetSelected(period.Year.ToString(), cmbYear);
            ExtControlHelper.ComboBoxSetSelected(period.Month.ToString(), cmbMonth);

            cmbMonth.Store[0].DataSource = DateManager.GetCurrentMonthList();
            cmbMonth.Store[0].DataBind();

            cmbLoanDeduction.Store[0].DataSource = LoanManager.GetRepaymentLoanList();
            cmbLoanDeduction.Store[0].DataBind();

        }

        private void Clear()
        {
            
        }

       
        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {

            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);

            int type = 2;

            if (cmbType.SelectedItem != null && cmbType.SelectedItem.Value != null)
                type = int.Parse(cmbType.SelectedItem.Value);

            int deductionId = -1;

            if (cmbLoanDeduction.SelectedItem != null && cmbLoanDeduction.SelectedItem.Value != null)
                deductionId = int.Parse(cmbLoanDeduction.SelectedItem.Value);

           if (period == null)
           {
               NewMessage.ShowWarningMessage("Period does not exists for this date.");
           }

            int totalRecords = 0;
            

            int employeeId = -1;
           
            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

            hdnSortBy.Text = (e.Sort[0].Property + " " + e.Sort[0].Direction).ToLower();

            List<Report_GetLoanRepaymentResult> list = EmployeeManager.GetLoanRepaymentReport
                (employeeId,e.Page-1,e.Limit,"",ref totalRecords,period.PayrollPeriodId,deductionId,type);


            e.Total = totalRecords;

            if (list.Any())
                storeEmpList.DataSource = list;
            storeEmpList.DataBind();

        }

     
        public void btnExport_Click(object sender, EventArgs e)
        {
            int year = int.Parse(cmbYear.SelectedItem.Value);
            int month = int.Parse(cmbMonth.SelectedItem.Value);

            PayrollPeriod period = CommonManager.GetPayrollPeriod(month, year);

            int type = 2;

            if (cmbType.SelectedItem != null && cmbType.SelectedItem.Value != null)
                type = int.Parse(cmbType.SelectedItem.Value);

            int deductionId = -1;

            if (cmbLoanDeduction.SelectedItem != null && cmbLoanDeduction.SelectedItem.Value != null)
                deductionId = int.Parse(cmbLoanDeduction.SelectedItem.Value);

            if (period == null)
            {
                NewMessage.ShowWarningMessage("Period does not exists for this date.");
            }

            int totalRecords = 0;


            int employeeId = -1;

            int.TryParse(cmbEmpSearch.SelectedItem.Value, out employeeId);

            if (employeeId == 0)
                employeeId = -1;

           

            List<Report_GetLoanRepaymentResult> list = EmployeeManager.GetLoanRepaymentReport
                (employeeId, 0, 9999, "", ref totalRecords, period.PayrollPeriodId, deductionId, type);



            Bll.ExcelHelper.ExportToExcel("Loan Repayment Report", list,
                new List<string> { "TotalRows", "TakenOn", "TakenOnEng", "EndDate", "EndDateEng", "DeductionId","Amount" },
            new List<String>() { },
            new Dictionary<string, string>() { { "TakenOnText", "Taken On" }, { "TakenOnEngText", "Taken On Eng" }, { "NoOfInstallment", "No Of Installment" }
                ,{"RemainingInstallment","Rem Installment"},{"EndDateText","End Date"},{"EndDateEngText","End Date Eng"}},
            new List<string>() { "PPMT", "PrincipleAmount" }
            , new List<string> { }
            , new List<string> { }
            , new Dictionary<string, string>() { { "Loan Repayment Report ", period.Name } }
            , new List<string> { "SN", "EmployeeId", "AccountNo", "Name", "Deduction", "TakenOnText", "TakenOnEngText","PrincipleAmount",
                    "NoOfInstallment","RemainingInstallment","PPMT","EndDateText","EndDateEngText"});


        }


    }
}