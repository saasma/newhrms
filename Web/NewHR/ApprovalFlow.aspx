﻿<%@ Page Title="Approval Flow" Language="C#" MasterPageFile="~/Master/NewDetails.Master"
    AutoEventWireup="true" CodeBehind="ApprovalFlow.aspx.cs" Inherits="Web.CP.ApprovalFlow" %>

<%--<%@ Register Src="~/NewHR/UserControls/EmployeeDetailsCtl.ascx" TagName="EmployeeDetailsCtl"
    TagPrefix="uc1" %>--%>
<%--#TODO: Group the Branc-Department-Department Head with Branch Column--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .hideLeftBlockCssInPage
        {
            margin: 0px !important;
            padding-left: 20px !important;
        }
        #menu
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
            

            var TARenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeTravelOrder.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.StepName;
                };

            var AuthorityRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeAuthorityType.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.AuthorityTypeName;
                };

                var additionalRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeAdditionalStep.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Text;
                };

                var RecommendRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                   if(value==true)
                    return "Yes";
                  
                   return "";
                };

                var EmpRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                    var r = <%= storeEmployee.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.NameEIN;
                };

                var DesignationRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
                    
                    var r = <%= storeDesignation.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.Name;
                };

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.BranchID);
            <%= hiddenValueDept.ClientID %>.setValue(record.data.DepartmentID);
                if(command=="Edit")
                {
                    <%= btnEditLevel.ClientID %>.fireEvent('click');
                }
             }
             


            var totalCost = function (records) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('Quantity') * record.get('Rate');
            }

            return total;
        };
        
        var addNewRow = function (grid) {
            var newRow = new SettingModel();

            newRow.data.StepID = "";

            var rowIndex = grid.getStore().data.items.length;

            //gridProjectSplitter.stopEditing();
            grid.getStore().insert(rowIndex, newRow);
        }

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };


        var beforeEdit = function (e1, e, e2, e3) {


//            if ((e.field == "Person1ID" || e.field == "Person2ID") && e.record.data.AuthorityType != "6") 
//            {
//                    e.cancel = true;
//                    return;
//            }
               
            
        }


    </script>
    <style>
        table#albums
        {
            border-collapse: separate;
            border-spacing: 0 10px;
        }
        
        table#albums1
        {
            border-collapse: separate;
            border-spacing: 0 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    <div class="pageheader">
        <div class="media">
            <div class="media-body">
                <h4>
                    Approval Flow
                </h4>
            </div>
        </div>
    </div>
    <div class="contentpanel">
        <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:Hidden runat="server" ID="hiddenValueDept">
        </ext:Hidden>
        <ext:Store runat="server" ID="storeTravelOrder">
            <Model>
                <ext:Model ID="Model2" IDProperty="StepID" runat="server">
                    <Fields>
                        <ext:ModelField Name="StepName" />
                        <ext:ModelField Name="StepID" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store runat="server" ID="storeAuthorityType">
            <Model>
                <ext:Model ID="Model3" IDProperty="AuthorityType" runat="server">
                    <Fields>
                        <ext:ModelField Name="AuthorityTypeName" />
                        <ext:ModelField Name="AuthorityType" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store runat="server" ID="storeAdditionalStep">
            <Model>
                <ext:Model ID="Model6" IDProperty="Value" runat="server">
                    <Fields>
                        <ext:ModelField Name="Value" />
                        <ext:ModelField Name="Text" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store runat="server" ID="storeEmployee">
            <Model>
                <ext:Model ID="Model4" IDProperty="EmployeeId" runat="server">
                    <Fields>
                        <ext:ModelField Name="NameEIN" />
                        <ext:ModelField Name="EmployeeId" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store runat="server" ID="storeDesignation">
            <Model>
                <ext:Model ID="Model5" IDProperty="DesignationId" runat="server">
                    <Fields>
                        <ext:ModelField Name="Name" />
                        <ext:ModelField Name="DesignationId" Type="String" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Button Cls="btn btn-primary" runat="server" Hidden="true" ID="btnEditLevel">
            <DirectEvents>
                <Click OnEvent="btnEditLevel_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>
        <div class="separator bottom">
        </div>
        <div class="innerLR">
            <%--<uc1:EmployeeDetailsCtl Id="EmployeeDetailsCtl1" runat="server" />--%>
            <div class="widget">
                <div>
                    <div class="alert alert-info">
                        <ext:ComboBox ID="cmbFlowType" runat="server" FieldLabel="Approval Process" LabelAlign="Top"
                            Width="200" QueryMode="Local" DisplayField="Name" ValueField="Value" ForceSelection="true"
                            LabelSeparator="">
                            <Store>
                                <ext:Store ID="storeFlowType" runat="server">
                                    <Fields>
                                        <ext:ModelField Name="Name" />
                                        <ext:ModelField Name="Value" Type="String" />
                                    </Fields>
                                </ext:Store>
                            </Store>
                            <DirectEvents>
                                <Select OnEvent="FlowType_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </div>
                    <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator4" runat="server"
                        ValidationGroup="SaveUpdate" ControlToValidate="cmbFlowType" ErrorMessage="Flow type is required." />
                    <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridAllowances" runat="server" Height="300"
                        Cls="itemgrid">
                        <Store>
                            <ext:Store ID="storeAllowances" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" Name="SettingModel" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="StepID" Type="String" />
                                            <ext:ModelField Name="StepName" Type="String" />
                                            <ext:ModelField Name="AuthorityType" Type="String" />
                                            <ext:ModelField Name="Person1ID" Type="String" />
                                            <ext:ModelField Name="Person2ID" Type="String" />
                                            <ext:ModelField Name="SequenceNo" Type="String" />
                                            <ext:ModelField Name="DesignationID" Type="String" />
                                            <ext:ModelField Name="AuthorityTypeDisplayName" Type="String" />
                                            <ext:ModelField Name="ShowAdditionalStep" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                            </ext:CellEditing>
                        </Plugins>
                        <Listeners>
                            <BeforeEdit Fn="beforeEdit" />
                        </Listeners>
                        <ColumnModel>
                            <Columns>
                                <ext:RowNumbererColumn ID="RowNumbererColumn1" DataIndex="SequenceNo" runat="server"
                                    Width="35" />
                                <ext:Column ID="colTravelOrder" runat="server" Text="Step" Sortable="false" MenuDisabled="true"
                                    Width="70" DataIndex="StepID" Border="false">
                                    <Renderer Fn="TARenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="StepName" QueryMode="Local" ForceSelection="true" ValueField="StepID"
                                            StoreID="storeTravelOrder" ID="cmbTravelOrder" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column5" runat="server" Text="Status Name" Sortable="false" MenuDisabled="true"
                                    Width="120" DataIndex="StepName" Border="false">
                                    <Editor>
                                        <ext:TextField runat="server" />
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column1" runat="server" Text="Authority" Sortable="false" MenuDisabled="true"
                                    Width="150" DataIndex="AuthorityType" Border="false">
                                    <Renderer Fn="AuthorityRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="AuthorityTypeName" QueryMode="Local" ForceSelection="true"
                                            ValueField="AuthorityType" StoreID="storeAuthorityType" ID="ComboBox1" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="Authority Display Name" Sortable="false"
                                    MenuDisabled="true" Width="150" DataIndex="AuthorityTypeDisplayName" Border="false">
                                    <Editor>
                                        <ext:TextField ID="TextField1" runat="server" />
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="ColumnDesignation" runat="server" Text="Designation" Sortable="false"
                                    MenuDisabled="true" Width="180" DataIndex="DesignationID" Border="false">
                                    <Renderer Fn="DesignationRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="Name" QueryMode="Local" ValueField="DesignationId" StoreID="storeDesignation"
                                            ID="ComboBox4" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="ColumnEmp1" runat="server" Text="Employee 1" Sortable="false" MenuDisabled="true"
                                    Width="200" DataIndex="Person1ID" Border="false">
                                    <Renderer Fn="EmpRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="NameEIN" ForceSelection="true" QueryMode="Local" ValueField="EmployeeId"
                                            StoreID="storeEmployee" ID="ComboBox2" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="ColumnEmp2" runat="server" Text="Employee 2" Sortable="false" MenuDisabled="true"
                                    Width="200" DataIndex="Person2ID" Border="false">
                                    <Renderer Fn="EmpRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="NameEIN" ForceSelection="true" QueryMode="Local" ValueField="EmployeeId"
                                            StoreID="storeEmployee" ID="ComboBox3" runat="server">
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column7" runat="server" Text="Additional Step" Sortable="false" MenuDisabled="true"
                                    Width="200" DataIndex="ShowAdditionalStep" Border="false">
                                    <Renderer Fn="additionalRenderer" />
                                    <Editor>
                                        <ext:ComboBox DisplayField="Text" QueryMode="Local" ForceSelection="true" ValueField="Value"
                                            StoreID="storeAdditionalStep" ID="ComboBox5" runat="server">
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                            </Triggers>
                                            <Listeners>
                                                <Select Handler="this.getTrigger(0).show();" />
                                                <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                                <TriggerClick Handler="if (index == 0) { 
                                           this.clearValue(); 
                                           this.getTrigger(0).hide();
                                       }" />
                                            </Listeners>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                    Width="30" Align="Center">
                                    <Commands>
                                        <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                    </Commands>
                                    <Listeners>
                                        <Command Fn="RemoveItemLine">
                                        </Command>
                                    </Listeners>
                                </ext:CommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <ext:Button Cls="btn btn-primary" ID="btnAddNewLineToMain" runat="server" Icon="Add"
                        Height="26" Text="Add New Row">
                        <Listeners>
                            <Click Handler="addNewRow(#{gridAllowances});" />
                        </Listeners>
                    </ext:Button>
                    <div style="margin-top: 25px; width: 200px;">
                        <ext:Button Cls="btn btn-primary" ID="Button1" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                            <DirectEvents>
                                <Click OnEvent="ButtonNext_Click">
                                    <EventMask ShowMask="true" />
                                    <ExtraParams>
                                        <ext:Parameter Name="GridValues" Value="Ext.encode(#{gridAllowances}.getRowsValues({selectedOnly : false}))"
                                            Mode="Raw" />
                                    </ExtraParams>
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
