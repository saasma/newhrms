﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using BLL.BO;
using Utils.Calendar;
using Web.Helper;
using Bll;

namespace Web.NewHR
{
    public partial class RetiredEmpList : BasePage
    {
        public override MenuTypeEnum MenuType
        {
            get
            {
                return MenuTypeEnum.Retirement;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();

            }
        }



        protected void btnLevelSaveUpdate_Click(object sender, DirectEventArgs e)
        {

            if (string.IsNullOrEmpty(calStart.Text.Trim()))
            {
                NewMessage.ShowWarningMessage("Re Hire date is required.");
                return;
            }


            if (CommonManager.CompanySetting.HasLevelGradeSalary)
            {
                if (cmbStep.SelectedItem == null || cmbStep.SelectedItem.Value == null)
                {
                    NewMessage.ShowWarningMessage("Grade is required.");
                    return;
                }
            }

                if (cmbStatus.SelectedItem == null || cmbStatus.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Status is required.");
                return;
            }
            if (cmbBranch.SelectedItem == null || cmbBranch.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Branch is required.");
                return;
            }
            if (cmbDepartment.SelectedItem == null || cmbDepartment.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Department is required.");
                return;
            }
            if (cmbLevel.SelectedItem == null || cmbLevel.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Level is required.");
                return;
            }
            if (cmbPosition.SelectedItem == null || cmbPosition.SelectedItem.Value == null)
            {
                NewMessage.ShowWarningMessage("Position/Designation is required.");
                return;
            }

            int gradeStep = 0;
            if (cmbStep.SelectedItem != null && cmbStep.SelectedItem.Value != null)
                gradeStep = int.Parse(cmbStep.SelectedItem.Value);

            Status status = EmployeeManager.ReHireEmployee(int.Parse(hdnFilterValue.Text.Trim()),
                txtNewINo.Text.Trim(), int.Parse(cmbStatus.SelectedItem.Value),
                calStart.Text.Trim(), calEnd.Text.Trim(), int.Parse(cmbBranch.SelectedItem.Value),
                int.Parse(cmbDepartment.SelectedItem.Value), txtAppointmentNo.Text.Trim(),
                int.Parse(cmbLevel.SelectedItem.Value), int.Parse(cmbPosition.SelectedItem.Value), gradeStep);

            if (status.IsSuccess)
            {
                NewMessage.ShowNormalMessage("Employee has been re-hired");
                X.Js.Call("searchListEL");
                windowPopup.Close();
            }
            else
                NewMessage.ShowWarningMessage(status.ErrorMessage);

        }

        protected void cmbLevel_Change(object sender, DirectEventArgs e)
        {
            bool allowDecimalInGrade = NewPayrollManager.AllowDecimalInGrade();

            List<TextValue> list = new List<TextValue>();
            BLevelRate stepGrades = NewPayrollManager.GetLevelRate(int.Parse(cmbLevel.SelectedItem.Value),DateTime.Now.Date);
            if (stepGrades != null)
            {
                for (int i = 0; i <= stepGrades.NoOfStepsGrade; i++)
                {
                    list.Add(new TextValue { Text = i.ToString(), Value = i.ToString() });

                    if (allowDecimalInGrade && i != 0)
                        list.Add(new TextValue { Text = (i + 0.5).ToString(), Value = (i + 0.5).ToString() });
                }

            }

            cmbStep.Store[0].DataSource = list;
            cmbStep.Store[0].DataBind();


            cmbPosition.Store[0].DataSource = NewPayrollManager.GetDesignationByLevel(int.Parse(cmbLevel.SelectedItem.Value));
            cmbPosition.Store[0].DataBind();


        }

        protected void LoadLevelDepedent(int levelId)
        {
            bool allowDecimalInGrade = NewPayrollManager.AllowDecimalInGrade();

            List<TextValue> list = new List<TextValue>();
            BLevelRate stepGrades = NewPayrollManager.GetLevelRate(levelId, DateTime.Now.Date);
            if (stepGrades != null)
            {
                for (int i = 0; i <= stepGrades.NoOfStepsGrade; i++)
                {
                    list.Add(new TextValue { Text = i.ToString(), Value = i.ToString() });

                    if (allowDecimalInGrade && i != 0)
                        list.Add(new TextValue { Text = (i + 0.5).ToString(), Value = (i + 0.5).ToString() });
                }

            }

            cmbStep.Store[0].DataSource = list;
            cmbStep.Store[0].DataBind();


            cmbPosition.Store[0].DataSource = NewPayrollManager.GetDesignationByLevel(levelId);
            cmbPosition.Store[0].DataBind();


        }

        protected void btnReHirePopup_Click(object sender, DirectEventArgs e)
        {
            int empId = int.Parse(hdnFilterValue.Text.Trim());

            EEmployee emp = EmployeeManager.GetEmployeeById(empId);

            if ((emp.IsRetired != null && emp.IsRetired.Value) ||
                (emp.IsResigned != null && emp.IsResigned.Value))
            {
            }
            else
            {
                NewMessage.ShowWarningMessage("Employee not retired for rehire.");
                return;
            }

            txtEIN.Text = emp.EmployeeId.ToString();
            txtName.Text = emp.Name;




            cmbBranch.SetValue(emp.BranchId.ToString());

            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartmentsByBranch(emp.BranchId.Value);
            cmbDepartment.Store[0].DataBind();

            cmbDepartment.SetValue(emp.DepartmentId.ToString());

            txtNewINo.Text = "";
            calStart.Text = "";
            calEnd.Text = "";

            cmbLevel.ClearValue();
            cmbPosition.ClearValue();

            int? statusId= EmployeeManager.GetEmployeeCurrentStatus(emp.EmployeeId);
            if (statusId != null)
                cmbStatus.SetValue(statusId.ToString());

            PIncome inc = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
            PEmployeeIncome basicIncome = PayManager.GetEmployeeIncomeById(
                emp.EmployeeId, inc.IncomeId);
            if (basicIncome != null && basicIncome.LevelId != null)
            {
                cmbLevel.SetValue(basicIncome.LevelId.ToString());

                LoadLevelDepedent(basicIncome.LevelId.Value);

                if (basicIncome.Step != null)
                    cmbStep.SetValue(basicIncome.Step.ToString());


                EDesignation desg = CommonManager.GetDesigId(emp.DesignationId.Value);
                if (desg.LevelId == basicIncome.LevelId)
                    cmbPosition.SetValue(emp.DesignationId.ToString());

            }
            else
            {
                EDesignation desg = CommonManager.GetDesigId(emp.DesignationId.Value);

                cmbLevel.SetValue(desg.LevelId.ToString());

                LoadLevelDepedent(desg.LevelId.Value);


                cmbPosition.SetValue(emp.DesignationId.ToString());
            }

            windowPopup.Show();
            windowPopup.Center();
        }

        public void Initialise()
        {

            List<KeyValue> list = new JobStatus().GetMembers();
            list.RemoveAt(0);
            cmbStatus.Store[0].DataSource = list;
            cmbStatus.Store[0].DataBind();

            cmbBranch.Store[0].DataSource = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            cmbBranch.Store[0].DataBind();

            cmbLevel.Store[0].DataSource = NewPayrollManager.GetAllParentLevels().OrderBy(x => x.GroupLevel)
                .ThenBy(x => x.Order);
            cmbLevel.Store[0].DataBind();

            if (CommonManager.CompanySetting.HasLevelGradeSalary)
                cmbStep.Show();
            else
                cmbStep.Hide();
            //txtFromDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            //txtToDate.Text = txtFromDate.Text;

            cmbTypes.Store[0].DataSource = CommonManager.GetServieEventTypes();
            cmbTypes.Store[0].DataBind();

            DateColumn_Confirmation.Visible = CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL;
        }

        public void btnLastMonth_Change(object sender, DirectEventArgs e)
        {
            DateTime FromDate = GetEngDate(txtFromDate.Text);
            DateTime ToDate = GetEngDate(txtToDate.Text);

            hiddenFrom.Text = txtFromDate.Text;
            hiddenTo.Text = txtToDate.Text;

            int? retType = null;

            if (cmbTypes.SelectedItem != null && cmbTypes.SelectedItem.Value != null)
                retType = int.Parse(cmbTypes.SelectedItem.Value);

            List<EmployeeServiceBo> expiringThisMonth = DashboardManager.GetEmployeeRetiringList(FromDate, ToDate, retType);

            gridEmployee.GetStore().DataSource = expiringThisMonth;
            gridEmployee.GetStore().DataBind();

        }

        public void btnExport_Click(object sender, EventArgs e)
        {
            DateTime? fromDate = null, toDate = null;
            int? retirementType = null, employeeId = null;

            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()))
                fromDate = GetEngDate(txtFromDate.Text);

            if (!string.IsNullOrEmpty(txtToDate.Text.Trim()))
                toDate = GetEngDate(txtToDate.Text);

            if (cmbTypes.SelectedItem != null && cmbTypes.SelectedItem.Value != null)
                retirementType = int.Parse(cmbTypes.SelectedItem.Value);

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbSearch.SelectedItem.Value);

            int totalRecords = 0;

            List<GetRetiredEmployeeListResult> _ListResult = DashboardManager.GetRetiredEmployeeList(fromDate, toDate, retirementType, employeeId, 0, 999999, out totalRecords);


            if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
            {
                ExcelHelper.ExportToExcel("Employee Retired List", _ListResult,
                new List<String>() { "TotalRows", "RowNumber", "JoinDateEng", "ConfirmationDateEng", "RetirementDateEng", "StopPayDate" },
                new List<String>() { },
                new Dictionary<string, string>() { { "EmployeeId", "EIN" }, { "JoinDateText", "Join Date" }, { "JoinDesignation", "Joining Designation" }, { "ConfirmationDateText", "Confirmation Date" },
            { "CurrentDesignation", "Current Designation" }, { "RetirementDateText", "Ret Date(A.D)" } , { "RetirementDate", "Ret Date(B.S)" }, { "ServicePeriod", "Service Period" },
            { "BasicSalary", "Basic Salary" },  { "IncentiveAllowance", "Inct. Allowance" },  { "StopPayDateText", "Stop Pay Date" },  { "RetirementType", "Retirement Type" }},
                new List<string>() { "BasicSalary", "Allowance", "IncentiveAllowance" },
                new List<string>() {  },
                new List<string>() { },
                new Dictionary<string, string>() { {"Employee Retired List"," from " + txtFromDate.Text  + " to " + txtToDate.Text } }
                , new List<string> { "EmployeeId", "AccountNo", "Name", "Level", "Branch", "Department", "JoinDateText", "JoinDesignation", "ConfirmationDateText" ,
             "CurrentDesignation",  "RetirementDateText",  "RetirementDate",  "ServicePeriod",  "BasicSalary",  "Allowance", "IncentiveAllowance", "StopPayDateText", "RetirementType" });
            }
            else
            {
                ExcelHelper.ExportToExcel("Employee Retired List", _ListResult,
                new List<String>() { "BasicSalary", "Allowance", "IncentiveAllowance", "TotalRows", "RowNumber", "JoinDateEng", "ConfirmationDateEng", "RetirementDateEng", "StopPayDate" },
                new List<String>() { },
                new Dictionary<string, string>() { { "EmployeeId", "EIN" }, { "JoinDateText", "Join Date" }, { "JoinDesignation", "Joining Designation" }, { "ConfirmationDateText", "Confirmation Date" },
            { "CurrentDesignation", "Current Designation" }, { "RetirementDateText", "Ret Date(A.D)" } , { "RetirementDate", "Ret Date(B.S)" }, { "ServicePeriod", "Service Period" },
            { "BasicSalary", "Basic Salary" },  { "IncentiveAllowance", "Inct. Allowance" },  { "StopPayDateText", "Stop Pay Date" },  { "RetirementType", "Retirement Type" }},
                new List<string>() { "BasicSalary", "Allowance", "IncentiveAllowance" },
                new List<string>() {  },
                new List<string>() { },
                 new Dictionary<string, string>() { { "Employee Retired List", " from " + txtFromDate.Text + " to " + txtToDate.Text } }
                , new List<string> { "EmployeeId", "AccountNo", "Name", "Level", "Branch", "Department", "JoinDateText", "JoinDesignation", "ConfirmationDateText" ,
             "CurrentDesignation",  "RetirementDateText",  "RetirementDate",  "ServicePeriod", "StopPayDateText", "RetirementType" });
            }
        }


        protected void cmbBranch_Change(object sender, DirectEventArgs e)
        {
            cmbDepartment.Store[0].DataSource = DepartmentManager.GetAllDepartmentsByBranch(int.Parse(cmbBranch.SelectedItem.Value));
            cmbDepartment.Store[0].DataBind();

        }


        protected void Store_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DateTime? fromDate = null, toDate = null;
            int? retirementType = null, employeeId = null;

            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()))
                fromDate = GetEngDate(txtFromDate.Text);

            if (!string.IsNullOrEmpty(txtToDate.Text.Trim()))
                toDate = GetEngDate(txtToDate.Text);

            if (cmbTypes.SelectedItem != null && cmbTypes.SelectedItem.Value != null)
                retirementType = int.Parse(cmbTypes.SelectedItem.Value);

            if (cmbSearch.SelectedItem != null && cmbSearch.SelectedItem.Value != null)
                employeeId = int.Parse(cmbSearch.SelectedItem.Value);

            int totalRecords = 0;

            gridEmployee.Store[0].DataSource = DashboardManager.GetRetiredEmployeeList(fromDate, toDate, retirementType, employeeId, e.Start, int.Parse(cmbPageSize.SelectedItem.Value), out totalRecords);
            gridEmployee.Store[0].DataBind();

            if (CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
            {
                colBasicSalary.Hide();
                colAllowance.Hide();
                colInctAllowance.Hide();
            }

            e.Total = totalRecords;


        }

    }
}