﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;

namespace Web.CP
{
    public partial class EmployeeAttendanceTimeExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                cal.SelectTodayDate();
                List<DAL.AttendanceInOutTime> list = AttendanceManager.GetEmployeeTimeForExport();
                if (list.Count > 0)
                {
                    AttendanceInOutTime time = list.FirstOrDefault(x => x.DateEng != null);
                    if (time != null)
                    {
                        DateTime date = time.DateEng.Value;
                        CustomDate cdate = new CustomDate(date.Day, date.Month, date.Year, true);
                        cal.SetSelectedDate(cdate.ToString(), true);

                    }
                }
            }
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/EmployeeAttendanceTime.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            List<DAL.AttendanceInOutTime> list = AttendanceManager.GetEmployeeTimeForExport();

            ExcelGenerator.ExportEmployeeTime(template, list);

        }
            

        

        

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<AttendanceInOutTime> list = new List<AttendanceInOutTime>();

                int employeeId;
                string deviceId;
                object value;

                DateTime time = DateTime.Now;

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        //int.TryParse(row["EIN"].ToString(), out employeeId);

                        AttendanceInOutTime timeEntity = new AttendanceInOutTime();

                        //if (!employeeIDList.Contains(employeeId))
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                        //    divWarningMsg.Hide = false;
                        //    return;

                        //    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}

                        importedCount += 1;

                        //timeEntity.EmployeeId = employeeId;

                        //if (employeeId == 0)
                        //    timeEntity.EmployeeId = null;

                        value = row["In Time"];
                        DateTime.TryParse(value.ToString(), out time);
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid In Time for employee id : " + employeeId + " of row " + rowNumber;
                        //    divWarningMsg.Hide = false;
                        //}
                        timeEntity.OfficeInTime = new TimeSpan(time.Hour, time.Minute, time.Second);

                        value = row["Out Time"];
                        DateTime.TryParse(value.ToString(), out time);
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid Out Time for employee id : " + employeeId + " of row " + rowNumber;
                        //    divWarningMsg.Hide = false;
                        //}
                        timeEntity.OfficeOutTime = new TimeSpan(time.Hour, time.Minute, time.Second);

                        timeEntity.Threshold = 0;

                        value = row["Break In"];
                        DateTime.TryParse(value.ToString(), out time);
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid Out Time for employee id : " + employeeId + " of row " + rowNumber;
                        //    divWarningMsg.Hide = false;
                        //}
                        timeEntity.OfficeFirstHalfInTime = new TimeSpan(time.Hour, time.Minute, time.Second);

                        value = row["Break Out"];
                        DateTime.TryParse(value.ToString(), out time);
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid Out Time for employee id : " + employeeId + " of row " + rowNumber;
                        //    divWarningMsg.Hide = false;
                        //}
                        timeEntity.OfficeSecondHalfOutTime = new TimeSpan(time.Hour, time.Minute, time.Second);


                        value = row["Half Holiday Out Time"];
                        if (value != null && !string.IsNullOrEmpty(value.ToString()))
                        {
                            DateTime.TryParse(value.ToString(), out time);
                                 timeEntity.HalfDayHolidayOutTime = new TimeSpan(time.Hour, time.Minute, time.Second);
                        }


                        value = row["HalfDay Leave In Out Time"];
                        if (value != null && !string.IsNullOrEmpty(value.ToString()))
                        {
                            DateTime.TryParse(value.ToString(), out time);
                                 timeEntity.HalfDayHolidayInTime = new TimeSpan(time.Hour, time.Minute, time.Second);
                        }

                        value = row["Public Holiday In Time"];
                        if (value != null && !string.IsNullOrEmpty(value.ToString()))
                        {
                            DateTime.TryParse(value.ToString(), out time);
                                 timeEntity.PublicHolidayInTime = new TimeSpan(time.Hour, time.Minute, time.Second);
                        }

                        value = row["Public Holiday Out Time"];
                        if (value != null && !string.IsNullOrEmpty(value.ToString()))
                        {
                            DateTime.TryParse(value.ToString(), out time);
                            timeEntity.PublicHolidayOutTime = new TimeSpan(time.Hour, time.Minute, time.Second);
                        }

                        value = row["HalfDay Leave In Out Time"];
                        if (value != null && !string.IsNullOrEmpty(value.ToString()))
                        {
                            DateTime.TryParse(value.ToString(), out time);
                                 timeEntity.HalfDayHolidayInTime = new TimeSpan(time.Hour, time.Minute, time.Second);
                        }


                        value = row["Shift"];
                        if (value != null && !string.IsNullOrEmpty(value.ToString()))
                        {
                            // DateTime.TryParse(value.ToString(), out time);
                            int ShiftID = AttendanceManager.getShiftIDByItsName(value.ToString().Trim());
                            if (ShiftID == 0)
                            {
                                divWarningMsg.InnerHtml = "Invalid shift name " + value.ToString() + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            timeEntity.ShiftID = ShiftID;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Shift name is required, create default shift if there is no shift also.";
                            divWarningMsg.Hide = false;
                            return;
                        }


                        list.Add(timeEntity);

                    }
                    
                    AttendanceManager.SaveEmployeeTime(list,cal.SelectedDate.EnglishDate);

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

