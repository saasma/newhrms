﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class NewDesignationImportExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       
        

        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/NewDesignationImport.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            //List<DAL.GetForDeviceMappingResult> list = TaxManager.GetForDeviceMapping("", 0, 999999, ref total);

            List<DAL.BLevel> list1 = new List<BLevel>();
           
            list1 = NewPayrollManager.GetAllParentLevelList();

            ExcelGenerator.ExportNewDesignationImportList(template, list1);
        }

   



        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

          

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<EDesignation> dbDesignationList = NewHRManager.GetDesignations();
                List<EDesignation> designationList = new List<EDesignation>();

                List<DAL.BLevel> levelList = new List<BLevel>();
                levelList = NewPayrollManager.GetAllParentLevelList();
                foreach (BLevel item in levelList)
                    item.GroupLevel = item.GroupName + " - " + item.Name;

                int levelId, order;
                string designationName, code, levelName;

                int importedCount = 0;
                int rowNumber = 0;

                try
                {
                   


                    foreach (DataRow row in datatable.Rows)
                    {
                        
                        {
                            EDesignation newDesignation = new EDesignation();



                            rowNumber += 1;
                            //int.TryParse(row["EIN"].ToString(), out employeeId);



                            importedCount += 1;


                            levelName = row["Level"].ToString();

                            if (string.IsNullOrEmpty(levelName))
                            {
                                divWarningMsg.InnerHtml = "Level name is required for the row " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            BLevel level = levelList.FirstOrDefault(x => x.GroupLevel.ToLower().Equals(levelName.Trim().ToLower()));
                            if (level == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid level name for the row " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            newDesignation.LevelId = level.LevelId;

                            designationName = row["Designation Name"].ToString();
                            if (string.IsNullOrEmpty(designationName))
                            {
                                divWarningMsg.InnerHtml = "Designation name is required for the row " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            newDesignation.Name = designationName.Trim();

                            EDesignation deDesignation = dbDesignationList.FirstOrDefault(x => x.Name.ToLower().Equals(newDesignation.Name.ToLower())
                                && x.LevelId == newDesignation.LevelId);
                            if (deDesignation != null)
                            {
                                divWarningMsg.InnerHtml = "Designation name " + newDesignation.Name + " already exists for the level " + levelName +  "  for the row " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            newDesignation.Code = row["Code"].ToString();

                            
                            int intOrder = 0;
                            string orderValue =  row["Order"].ToString();
                            if (int.TryParse(orderValue, out intOrder))
                                newDesignation.Order = intOrder;

                            newDesignation.CompanyId = SessionManager.CurrentCompanyId;
                            newDesignation.CategoryId = ((int)(DesignationCategory.Administration));
                            newDesignation.GroupId = ((int)(DesignationGroup.Administration));

                            

                            designationList.Add(newDesignation);
                        }



                    }

                    


                    //bool isInsert = true;

                    Status status = NewHRManager.InsertImportedDesignation(designationList);

                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = "Designation imported.";
                        divMsgCtl.Hide = false;
                    }
                    else
                    {
                        divMsgCtl.InnerHtml = status.ErrorMessage;
                        divMsgCtl.Hide = false;
                    }



                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

