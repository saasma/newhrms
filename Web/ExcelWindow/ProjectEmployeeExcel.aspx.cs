﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;

namespace Web.CP
{
    public partial class ProjectEmployeeExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        DateTime start;
        DateTime end;

        protected void Page_Load(object sender, EventArgs e)
        {


            start = DateTime.Parse(Request.QueryString["start"]);
            end = DateTime.Parse(Request.QueryString["end"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/ProjectEmployeeImport.xlsx");        
                 
            int total=0;

            string[] GetProjectList = ProjectManager.GetAllProjectList()
                .Where(x=>x.EndDateEng >= start).OrderBy(x=>x.Name).Select(x => x.Name).ToArray();


            //List<IIndividualInsurance> list = InsuranceManager.GetCurrentYearInsuranceList(yearId,0, int.MaxValue, ref total, "", 0);

            List<GetProjectAssociationResult> ProjectAssociationList = ProjectManager.GetProjectAssociation(0, 999999,"EmployeeName ASC",null, null, start, end).ToList();


            ExcelGenerator.WriteProjectEmpExcel(template, GetProjectList, ProjectAssociationList);
        }




       

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            int total = 0;
            

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

               
                int employeeId;

                string value;
                List<Project> projectList = ProjectManager.GetAllProjectList();
                List<GetProjectAssociationResult> list = new List<GetProjectAssociationResult>();
                int importedCount = 0;

                List<int> einList = BLL.BaseBiz.PayrollDataContext.EEmployees.Select(x => x.EmployeeId).ToList();

                try
                {
                    int rowNumber = 1;
                    int insuranceId ;
                    foreach (DataRow row in datatable.Rows)
                    {
                        GetProjectAssociationResult item = new GetProjectAssociationResult();
                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.EmployeeName = row["Employee"].ToString();
                        item.EID = employeeId;

                        if (einList.Contains(item.EID) == false)
                        {
                            divWarningMsg.InnerHtml = "EIN " + item.EID + " in row number " + rowNumber + " does not exists.";
                            divWarningMsg.Hide = false;
                            return;
                        }


                        if (!string.IsNullOrEmpty(row["ID"].ToString()))
                        {
                            int.TryParse(row["ID"].ToString(), out insuranceId);
                            //if (int.TryParse(row["Insurance ID"].ToString(), out insuranceId) == false)
                            //{
                            //    divWarningMsg.InnerHtml = "Invalid insurance id in row number " + rowNumber + ".";
                            //    divWarningMsg.Hide = false;
                            //    return;
                            //}
                            item.ID = insuranceId;
                        }



                        value = row["Project Name"].ToString().Trim().ToLower();
                        if (!string.IsNullOrEmpty(value))
                        {
                            Project pro = projectList.FirstOrDefault(x => x.Name.ToLower().Trim().Equals(value.ToString().Trim()));
                            if (pro != null)
                                item.ProjectId = pro.ProjectId;
                            else
                            {
                                divWarningMsg.InnerHtml = "Invalid Project name for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Project name is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.ProjectName = row["Project Name"].ToString();
                        item.ContractID = row["Contract ID"].ToString();


                        value = row["Activity ID"].ToString().Trim().ToLower();
                        if (!string.IsNullOrEmpty(value))
                        {
                            decimal decValue = 0;
                            if (decimal.TryParse(value.ToString(), out decValue))
                            {
                                string msg = "";
                                if (NewTimeSheetManager.IsValidActivityIDForCare((long)decValue, ref msg, true) == false)
                                {
                                    //NewMessage.ShowWarningMessage(msg);

                                    divWarningMsg.InnerHtml = msg +  " in row number " + rowNumber + ".";
                                    divWarningMsg.Hide = false;
                                    return;
                                }
                                //if (decValue > 500 && decValue < 999999999999999)
                                //{
                                //    divWarningMsg.InnerHtml = " Activity should be 1-500 or 999999999999999 in row number " + rowNumber + ".";
                                //    divWarningMsg.Hide = false;
                                //    return;
                                //}

                                //if (decValue < 1 || decValue > 999999999999999)
                                //{
                                //    divWarningMsg.InnerHtml = " Activity should be 1-500 or 999999999999999 in row number " + rowNumber + ".";
                                //    divWarningMsg.Hide = false;
                                //    return;
                                //}
                                item.Activity = (double)decValue;
                            }
                        }

                        value = row["Project Start Date"].ToString().Trim();
                        DateTime date = DateTime.Now;
                        if (DateTime.TryParse(value, out date))
                            item.StartDate = date;
                        else
                        {
                            divWarningMsg.InnerHtml = "Invalid start date for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (!string.IsNullOrEmpty(row["Project End Date"].ToString()))
                        {
                            value = row["Project End Date"].ToString().Trim();
                            if (DateTime.TryParse(value, out date))
                                item.EndDate = date;
                            else
                            {
                                divWarningMsg.InnerHtml = "Invalid end date for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }

                       




                        list.Add(item);

                        importedCount += 1;

                        

                      

                    }



                    int imported = 0;
                    //string msg;
                    Status status = ProjectManager.SaveUpdateProjectAssocation(list);

                    //if (!string.IsNullOrEmpty(msg))
                    //{
                    //    divWarningMsg.InnerHtml = msg;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                    }

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

