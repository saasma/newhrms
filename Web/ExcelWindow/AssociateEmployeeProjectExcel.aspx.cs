﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class AssociateEmployeeProjectExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        int departmentId = -1;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["DepartmentId"] != null)
                departmentId = int.Parse(Request.QueryString["DepartmentId"]);
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmployeeLeaveProject.xlsx");        
                 
            int total=0;

            string[] GetProjectList =
                LeaveRequestManager.GetProjectList("", 1, 99999, ref total).OrderBy(x=>x.Name)
                .Select(a => a.Name).ToArray();

            List<GetLeaveProjectEmployeesResult> list =
               LeaveRequestManager.GetEmployeeProjectList(
              "", 1, 9999, ref total, departmentId,-1,-1);



            ExcelGenerator.WriteEmployeeLeaveProjectExcel(template,GetProjectList, list);
        }




       

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            int total = 0;
            List<GetLeaveProjectsResult> projectList =
                LeaveRequestManager.GetProjectList("", 1, 99999, ref total).ToList();



            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

               
                int employeeId;

                string projectName;

                List<GetLeaveProjectEmployeesResult> list = new List<GetLeaveProjectEmployeesResult>();
                int importedCount = 0;
                

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        projectName = row["Project"].ToString().Trim().ToLower();

                        if (string.IsNullOrEmpty(projectName))
                        {
                            divWarningMsg.InnerHtml = string.Format("Project must be set for the employee ID {0}.",
                                  employeeId);
                            divWarningMsg.Hide = false;
                            return;
                        }

                        GetLeaveProjectEmployeesResult item = new GetLeaveProjectEmployeesResult();
                        item.EmployeeId = employeeId;

                        GetLeaveProjectsResult result = projectList.FirstOrDefault(x=>x.Name.Trim().ToLower()==projectName);

                        if( result == null)
                        {
                            divWarningMsg.InnerHtml = string.Format("Project named \"{0}\" does not exists associated with the employee having ID {1}.",
                                  projectName,employeeId );
                                divWarningMsg.Hide = false;
                            return;
                        }


                        item.LeaveProjectId = result.LeaveProjectId;


                        list.Add(item);

                        importedCount += 1;

                        

                      

                    }



                    int imported = 0;
                    string msg;
                    LeaveRequestManager.SaveUpdateEmployeeProject(list);

                    //if (!string.IsNullOrEmpty(msg))
                    //{
                    //    divWarningMsg.InnerHtml = msg;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

