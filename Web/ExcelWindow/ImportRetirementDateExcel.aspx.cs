﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;

namespace Web.CP
{
    public partial class ImportRetirementDateExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

       
        protected void Page_Load(object sender, EventArgs e)
        {
          


            
           
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/RetirementDate.xlsx");

            
            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteExportExcel(template);
        }


    


        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

            
                int employeeId;
                //string value;
                //float? Days_At_Palati, Days_At_Kirne, On_Call, Shift_Afternoon, Shift_Night, Split_Shift_Morning, Split_Shift_Evening, OT_150, OT_200, OT_300, Public_Holiday,
                //    Other_Leave, Snacks_Allowance, Special_Compensation_Allowance, Meal_Allowance, TADA, Vehicle_Allowance;
               

                //int importedCount = 0;
                //int employeeCount = 0;

                try
                {
                    List<EmployeeRet> list = new List<EmployeeRet>();
                  

                    string name = "";
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                            
                        {
                            continue;
                        }


                        EmployeeRet emp = new EmployeeRet();

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }


                        emp.EIN = employeeId;
                        if (row["Retirement Date Eng"] == DBNull.Value || row["Retirement Date Eng"].ToString() == "")
                        {
                            divWarningMsg.InnerHtml = "Invalid retirement date for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                        }

                        string date = row["Retirement Date Eng"].ToString();

                        DateTime engDate = DateTime.Now;
                        if (DateTime.TryParse(date, out engDate) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid retirement date for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        emp.RetirementDate = engDate;

                        list.Add(emp);

                    }

                    int count = 0;
                    string pastRetirementList = "";
                    Status status = PayManager.ImportRetirementDate(list, ref count, ref pastRetirementList);

                    if (status.IsSuccess == false)
                    {

                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    divMsgCtl.InnerHtml = string.Format(@"Imported for {0} employees.", count, pastRetirementList);
                    divMsgCtl.Hide = false;

                    divWarningMsg.InnerHtml = string.Format(@"Past retirement already set for these ein {1}, so no change
                        has been made.", count, pastRetirementList);
                    divWarningMsg.Hide = false;


                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
            else
            {
                divWarningMsg.InnerHtml = "Excel version should be 2007(.xlsx) or higher.";
                divWarningMsg.Hide = false;
            }
        }
        
     
       
    }

   
}

