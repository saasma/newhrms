﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Ext.Net;

namespace Web.CP
{
    public partial class MailMergeImport : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/MailMergeImportTemplate.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            List<DAL.GetForTaxHistoryResult> list = TaxManager.GetForTaxHistory("", 0, 999999, ref total);

            List<MailMergeTemplateHeader> headerList = new List<MailMergeTemplateHeader>();
            headerList = MailMergeManager.GetAllHeaderList(null);

            List<MailMergeExcelTemp> iList = new List<MailMergeExcelTemp>();
            iList = MailMergeManager.getMailMergeExcelTempRows();
            iList.Insert(0,new MailMergeExcelTemp { EmployeeID = "EID", EmployeeName = "Name",
                                         Column1 = headerList[0].HeaderText,
                                         Column2 = headerList[1].HeaderText,
                                         Column3 = headerList[2].HeaderText,
                                         Column4 = headerList[3].HeaderText,
                                         Column5 = headerList[4].HeaderText,
                                         Column6 = headerList[5].HeaderText,
                                         Column7 = headerList[6].HeaderText,
                                         Column8 = headerList[7].HeaderText,
                                         Column9 = headerList[8].HeaderText,
                                         Column10 = headerList[9].HeaderText

            });


            ExcelGenerator.WriteMailMergeExportExcel(template, iList);
        }



       
        

        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            List<DAL.GetForTaxHistoryResult> list = TaxManager.GetForTaxHistory("", 0, 999999, ref total);
            List<int> ids = new List<int>();

            foreach (GetForTaxHistoryResult item in list)
            {
                ids.Add(item.EmployeeId);
            }
            return ids;
        }

        #region
        /*
        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xlsx");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xlsx"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Import$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");

                int employeeId;
                decimal? grossAmount,pf,cit,insurance,sst,tds,remoteArea;
               
                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);
                        
                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                            divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        grossAmount = ExcelGenerator.GetAmountValueFromCell("Opening Gross Amount", row, employeeId);
                        //Days_At_Kirne = ExcelGenerator.GetValueFromCell("Days_At_Kirne", row, employeeId);

                        pf = ExcelGenerator.GetAmountValueFromCell("Opening PF", row, employeeId);

                        cit = ExcelGenerator.GetAmountValueFromCell("Opening CIT", row, employeeId);
                        insurance = ExcelGenerator.GetAmountValueFromCell("Opening Insurance", row, employeeId);
                        sst = ExcelGenerator.GetAmountValueFromCell("Opening SST", row, employeeId);
                        tds = ExcelGenerator.GetAmountValueFromCell("Opening Tax Paid", row, employeeId);

                        remoteArea = ExcelGenerator.GetAmountValueFromCell("Remote Area Opening", row, employeeId);

                        // if Remote area opening not enabled then don't allow to import
                        if (CommonManager.CompanySetting.RemoteAreaUsingProportionateDays == false
                            && remoteArea != null && remoteArea != 0)
                        {
                            divWarningMsg.InnerHtml = "Remote area opening not enabled so value can not be set for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        xml.Append(
                            string.Format("<row EmployeeId=\"{0}\" Amount=\"{1}\" PF=\"{2}\" CIT=\"{3}\"  Insurance=\"{4}\"  TaxPaid=\"{5}\" OpeningSST=\"{6}\"  OpeningRemoteArea=\"{7}\" /> ",
                                 employeeId, grossAmount, pf, cit, insurance, tds, sst,remoteArea)
                            );

                    
                      

                    }



                    xml.Append("</root>");

                    TaxManager.SaveForTax(xml.ToString());

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        */
        #endregion




        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile)// && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);



                try
                {
                    DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Import$]");

                    if (datatable == null)
                    {
                        divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                        divWarningMsg.Hide = false;
                        File.Delete(template);
                        return;
                    }

                    List<MailMergeExcelTemp> mmRows = new List<MailMergeExcelTemp>();
                    MailMergeExcelTemp mmRow;
                    foreach (DataRow row in datatable.Rows)
                    {
                        mmRow = new MailMergeExcelTemp();
                        mmRow.ID = Guid.NewGuid();
                        mmRow.EmployeeID = row[0].ToString();
                        mmRow.EmployeeName = row[1].ToString();
                        mmRow.Column1 = row[2].ToString();
                        mmRow.Column2 = row[3].ToString();
                        mmRow.Column3 = row[4].ToString();
                        mmRow.Column4 = row[5].ToString();
                        mmRow.Column5 = row[6].ToString();
                        mmRow.Column6 = row[7].ToString();
                        mmRow.Column7 = row[8].ToString();
                        mmRow.Column8 = row[9].ToString();
                        mmRow.Column9 = row[10].ToString();
                        mmRow.Column10 = row[11].ToString();

                        mmRows.Add(mmRow);
                    }


                    Status status = MailMergeManager.InsertExcelRowsTemp(mmRows);


                    divMsgCtl.InnerHtml = string.Format(mmRows.Count + " employee inserted.");
                    divMsgCtl.Hide = false;
                }

                catch (Exception exp)
                {

                    //divMsgCtl.Text = exp.ToString();
                }
                

            }


        }

       
    }

   
}

