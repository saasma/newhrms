﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;

namespace Web.CP
{
    public partial class GradeRewardExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/GradeReward.xlsx");        
                 
            //int total=0;
            //// Retrieve data from SQL Server table.
            //List<GetUserListResult> list = UserManager.GetUserList("", "", true, 0, 2000, ref total, true)
            //    .OrderBy(x => x.EmployeeId).ToList();

            ExcelGenerator.WriteExportExcel(template);
        }




        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            // Retrieve data from SQL Server table.
            List<int> ids = EmployeeManager.GetAllEmployeeID();
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

               
                int employeeId;
                //decimal? grossAmount,pf,cit,insurance,sst,tds;

              
                List<int> employeeIDList = GetEmployeeIdList();
                List<GradeRewardEmployee> list = new List<GradeRewardEmployee>();
                int importedCount = 0;

                string date, note;
                decimal? amount;
                decimal? points, rate;


                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                            //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            //divWarningMsg.Hide = false;
                            //return;
                        }

                        if (DBNull.Value.Equals(row["Date"]) || row["Date"] == "")
                        {
                            divWarningMsg.InnerHtml = "Date is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }


                        CustomDate dateValue = null;
                        //if error in date conversion
                        try
                        {
                            dateValue = CustomDate.GetCustomDateFromString(row["Date"].ToString(), IsEnglish);

                            
                        }
                        catch
                        {

                            divWarningMsg.InnerHtml = "Date is invalid for the employee having id : " + employeeId + " ,should be in the format dd/mm/yyy.";
                            divWarningMsg.Hide = false;
                        }

                        amount = ExcelGenerator.GetAmountValueFromCell("Amount", row, employeeId);
                        if (amount == null)
                        {
                            divWarningMsg.InnerHtml = "Amount is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        points = ExcelGenerator.GetAmountValueFromCell("Points", row, employeeId);
                        if (points == null || points == 0)
                        {
                            divWarningMsg.InnerHtml = "Points is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        rate = ExcelGenerator.GetAmountValueFromCell("Rate", row, employeeId);
                        if (rate == null || rate == 0)
                        {
                            divWarningMsg.InnerHtml = "Rate is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        GradeRewardEmployee item = new GradeRewardEmployee();
                        item.EmployeeId = employeeId;
                        item.RewardDate = row["Date"].ToString().Trim();
                        item.RewardDateEng = GetEngDate(item.RewardDate);
                        item.RewardPoints = (double)points;
                        item.RewardRate = rate;
                        item.Amount = item.RewardRate * (decimal)item.RewardPoints;
                        BLevel level = NewHRManager.GetEmployeeLastestLevel(item.EmployeeId);
                        if(level==null)
                        {
                             divWarningMsg.InnerHtml = "Reward can be given to level assigned employee only : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.LevelId = level.LevelId;
                        item.Notes = row["Note"].ToString().Trim();
                        item.IsActive = true;

                        

                        list.Add(item);

                       

                      

                    }



                    int imported = 0;
                    string msg;
                    msg = NewPayrollManager.InsertUpdateGradeRewardDetail(list, ref imported);

                    if (!string.IsNullOrEmpty(msg))
                    {
                        divWarningMsg.InnerHtml = msg;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, imported);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

