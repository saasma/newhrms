﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class EPreviousExpImport : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            // other user can also import so not needed for these of minor types
            //if (SessionManager.User.URole.RoleId != (int)Role.Administrator)
            //{
            //    Response.Write("Not enough permisson.");
            //    Response.End();
            //    return;
            //}

        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmpExperienceImport.xlsx");

            string[] experienceCategories = ListManager.GetExperienceCategory().Select(x => x.Name).ToArray();

            string path = HttpContext.Current.Server.MapPath(template);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();
                    ExcelGenerator _objExcelGenerator = new ExcelGenerator();
                    ExcelGenerator.WriteIntoSheet(conn, "ExperienceCategoryList", experienceCategories);
                }
            }

            catch (Exception ex)
            {
                divWarningMsg.InnerHtml = ex.ToString();
                divWarningMsg.Hide = false;
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(template));

        }

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID(); 
        }
        
        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<ExperienceCategory> listExpCategories = ListManager.GetExperienceCategory();

                int employeeId;
                
                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                List<HPreviousEmployment> list = new List<HPreviousEmployment>();

                string experienceCategory = "";

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);
                        
                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        HPreviousEmployment obj = new HPreviousEmployment();
                        obj.EmployeeId = employeeId;

                        experienceCategory = row["Experience Category"].ToString().Trim();
                        if (!string.IsNullOrEmpty(experienceCategory))
                        {
                            ExperienceCategory objExperienceCategory = listExpCategories.SingleOrDefault(x => x.Name.ToLower() == experienceCategory.ToLower());
                            if (objExperienceCategory == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid Experience Category for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            obj.CategoryID = objExperienceCategory.CategoryID;
                            obj.Category = objExperienceCategory.Name;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Experience Category is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (!string.IsNullOrEmpty(row["Organization"].ToString().Trim()))
                            obj.Organization = row["Organization"].ToString().Trim();
                        else
                        {
                            divWarningMsg.InnerHtml = "Organization is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (!string.IsNullOrEmpty(row["Place"].ToString().Trim()))
                            obj.Place = row["Place"].ToString().Trim();
                        else
                        {
                            divWarningMsg.InnerHtml = "Place is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (!string.IsNullOrEmpty(row["Job Responsibility"].ToString().Trim()))
                            obj.JobResponsibility = row["Job Responsibility"].ToString().Trim();
                        else
                        {
                            divWarningMsg.InnerHtml = "Job Responsibility is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (!string.IsNullOrEmpty(row["Position"].ToString().Trim()))
                            obj.Position = row["Position"].ToString().Trim();
                        else
                        {
                            divWarningMsg.InnerHtml = "Position is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (!string.IsNullOrEmpty(row["Reason for leaving"].ToString().Trim()))
                            obj.ReasonForLeaving = row["Reason for leaving"].ToString().Trim();
                        else
                        {
                            divWarningMsg.InnerHtml = "Reason for leaving is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }                        

                        if (!string.IsNullOrEmpty(row["Notes"].ToString().Trim()))
                            obj.Notes = row["Notes"].ToString().Trim();
                        
                        obj.Status = (int)HRStatusEnum.Approved;
                        obj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        obj.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();

                        list.Add(obj);
                    }

                    importedCount = list.Select(x => x.EmployeeId).Distinct().Count();

                    Status status = NewHRManager.ImportEmpPrevWorkExperience(list);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = string.Format("Previous work exp records imported for {0} employees successfully.", importedCount);
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

