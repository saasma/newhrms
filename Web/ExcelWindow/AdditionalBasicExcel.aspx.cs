﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;

namespace Web.CP
{
    public partial class AdditionalBasicExcel : BasePage
    {
        int incomeId = -1;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["ID"] != null)
                this.incomeId = int.Parse(Request.QueryString["ID"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/AdditionBasic.xlsx");

            List<GetAdditionaBasicListResult> list =
                BLL.BaseBiz.PayrollDataContext.GetAdditionaBasicList(-1, 0, 99999, -1, -1, incomeId, false).ToList();

            //int total=0;
            //// Retrieve data from SQL Server table.
            //List<GetUserListResult> list = UserManager.GetUserList("", "", true, 0, 2000, ref total, true)
            //    .OrderBy(x => x.EmployeeId).ToList();

            ExcelGenerator.ExportAdditoinIncome(template,list);
        }




        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            // Retrieve data from SQL Server table.
            List<int> ids = EmployeeManager.GetAllEmployeeID();
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

               
                int employeeId;
                //decimal? grossAmount,pf,cit,insurance,sst,tds;

              
                List<int> employeeIDList = GetEmployeeIdList();
                List<GradeAdditionlBasic> list = new List<GradeAdditionlBasic>();
                int importedCount = 0;

                string date, note;
                decimal? amount;
                decimal? points, rate;

                string Income;


                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                            //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            //divWarningMsg.Hide = false;
                            //return;
                        }

                        

                        amount = ExcelGenerator.GetAmountValueFromCell("Amount", row, employeeId);
                        if (amount == null)
                        {
                            divWarningMsg.InnerHtml = "Amount is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        Income = row["Income"].ToString();
                        PIncome dbIncome = new PayManager().GetIncomeByName(Income);
                        if (dbIncome == null)
                        {
                            divWarningMsg.InnerHtml = "Valid Income is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        GradeAdditionlBasic item = new GradeAdditionlBasic();
                        item.EmployeeId = employeeId;
                        item.IncomeId = dbIncome.IncomeId;
                        item.Amount = amount;
                        BLevel level = NewHRManager.GetEmployeeLastestLevel(item.EmployeeId);
                        if(level==null)
                        {
                             divWarningMsg.InnerHtml = "Additional basic can be given to level assigned employee only : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.LevelId = level.LevelId;
                        item.Notes = row["Note"].ToString().Trim();
                        

                        

                        list.Add(item);

                       

                      

                    }



                    int imported = 0;
                    string msg;
                    msg = NewPayrollManager.InsertUpdateAdditionBasicDetail(list, ref imported);

                    if (!string.IsNullOrEmpty(msg))
                    {
                        divWarningMsg.InnerHtml = msg;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, imported);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

