﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class LeaveSettingsExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        int departmentId = -1;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["DepartmentId"] != null)
                departmentId = int.Parse(Request.QueryString["DepartmentId"]);
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/LeaveSetting.xlsx");        
                 
            int total=0;
            List<GetEmployeesByBranchResult> empList = new EmployeeManager().GetEmployeesByBranch(-1, -1);
            string[] employeeList = empList.Select(x => x.Name).ToArray();

            List<GetLeaveApprovalSettingListResult> list = LeaveRequestManager.GetLeaveApprovalList
              (-1, 0, int.MaxValue, -1, -1).OrderBy(x => x.Branch).ThenBy(x => x.Project).ToList();


            ExcelGenerator.WriteEmployeeLeaveRequestExcel(template, employeeList, list, empList);
        }




       

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            int total = 0;

            List<GetEmployeesByBranchResult> empList = new EmployeeManager().GetEmployeesByBranch(-1, -1);



            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

               
                int employeeId;

                string projectName, r1, r2, a1, a2;
                GetEmployeesByBranchResult emp = null;

                List<GetLeaveApprovalSettingListResult> list = new List<GetLeaveApprovalSettingListResult>();
                int importedCount = 0;
                int totalProjects=0;
                List<GetLeaveProjectsResult> projectlist = LeaveRequestManager.GetProjectList("", 1, 9999999, ref totalProjects);

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        
                        projectName = row["Team"].ToString().Trim().ToLower();
                        r1 = row["Recommend 1"].ToString().Trim().ToLower();
                        r2 = row["Recommend 2"].ToString().Trim().ToLower();
                        a1 = row["Approval 1"].ToString().Trim().ToLower();
                        a2 = row["Approval 2"].ToString().Trim().ToLower();

                        GetLeaveProjectsResult project = projectlist.Where(x => x.Name.ToLower().Trim() == projectName).FirstOrDefault();


                        if (project== null)
                        {
                            divWarningMsg.InnerHtml = string.Format("Team named \"{0}\" does not exists for the row {1}.",
                                 projectName, rowNumber);
                            divWarningMsg.Hide = false;
                            return;
                        }

                        GetLeaveApprovalSettingListResult item = new GetLeaveApprovalSettingListResult();
                        item.LeaveProjectId = project.LeaveProjectId;


                        emp = empList.Where(x => x.Name.ToLower().Trim() == a1).FirstOrDefault();
                        if (emp == null)
                        {
                            divWarningMsg.InnerHtml = string.Format("Approval 1 is required for the row {0}.",
                                 rowNumber);
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.ApplyTo = emp.EmployeeId.ToString();


                        emp = empList.Where(x => x.Name.ToLower().Trim() == a2).FirstOrDefault();
                        if (emp != null)
                        {
                            item.CC1 = emp.EmployeeId.ToString();
                        }


                        emp = empList.Where(x => x.Name.ToLower().Trim() == r1).FirstOrDefault();
                        if (emp != null)
                        {
                            item.CC4 = emp.EmployeeId.ToString();
                        }

                        emp = empList.Where(x => x.Name.ToLower().Trim() == r2).FirstOrDefault();
                        if (emp != null)
                        {
                            item.CC5 = emp.EmployeeId.ToString();
                        }


                        list.Add(item);
                        importedCount += 1;
                    }



                    int imported = 0;
                    string msg;
                    LeaveRequestManager.SaveUpdateLeaveRequestSettings(list);

                    //if (!string.IsNullOrEmpty(msg))
                    //{
                    //    divWarningMsg.InnerHtml = msg;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

