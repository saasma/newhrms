﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.ExcelWindow
{
    public partial class AdjustArrearIncrementExcel : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["IncomeId"]))
                    hdnIncomeId.Value = Request.QueryString["IncomeId"];

                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                    hdnType.Value = Request.QueryString["Type"];
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/AdjustArrearIncrement.xlsx");

            PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            int payrollPeriodId = validPayrollPeriod == null ? 0 : validPayrollPeriod.PayrollPeriodId;

            List<AdjustArrearIncrement> list = PayManager.GetAdjustArrearIncrementList(int.Parse(hdnIncomeId.Value), int.Parse(hdnType.Value), payrollPeriodId);

            bool hideColumn = false;

            if (int.Parse(hdnType.Value) < 0)
                hideColumn = true;


            PIncome income = new PayManager().GetIncomeById(int.Parse(hdnIncomeId.Value));

            if (income.IsPFCalculated == false)
            {
                hideColumn = false;
            }

            if (income.Calculation == IncomeCalculation.PERCENT_OF_INCOME)
            {
                template = ("~/App_Data/ExcelTemplate/AdjustArrearIncrementPercentType.xlsx");
                ExcelGenerator.ExportAdjustArrearIncrementPercentType(template, list, hideColumn);
                return;
            }


            ExcelGenerator.ExportAdjustArrearIncrement(template, list, hideColumn);
        }
        
        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            int payrollPeriodId = validPayrollPeriod == null ? 0 : validPayrollPeriod.PayrollPeriodId;

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<AdjustArrearIncrement> list = new List<AdjustArrearIncrement>();

                int employeeId; 
                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;

                        AdjustArrearIncrement obj = new AdjustArrearIncrement();

                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        obj.EmployeeId = employeeId;

                        importedCount += 1;

                        if (row["Total Increase"] != DBNull.Value && row["Total Increase"].ToString() != "")
                        {
                            obj.TotalIncrease = ExcelGenerator.GetAmountValueFromCell("Total Increase", row, employeeId).Value;                           
                        }

                        if (row["Total PF"] != DBNull.Value && row["Total PF"].ToString() != "")
                        {
                            obj.TotalPF = ExcelGenerator.GetAmountValueFromCell("Total PF", row, employeeId).Value;
                        }

                        list.Add(obj);

                    }

                    var count = 0;
                    Status status = PayManager.UpdateRetrospectIncrement(list, payrollPeriodId, int.Parse(hdnIncomeId.Value), int.Parse(hdnType.Value), ref count);
                    if (status.IsSuccess == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }


                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                                                            count);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;
                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}