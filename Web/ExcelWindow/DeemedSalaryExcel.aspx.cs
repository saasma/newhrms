﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace Web.CP
{
    public partial class DeemedSalaryExcel : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                {
                    return false;
                }
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public int IncomeId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            IncomeId = int.Parse(Request.QueryString["IncomeID"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            var income = new PayManager().GetIncomeById(IncomeId);

            if (income != null)
            {
                var template = string.Empty;
                if (income.DeemedIsAmount.Value)
                {
                    template = ("~/App_Data/ExcelTemplate/DeemedAmount.xlsx");
                }
                else
                {
                    template = ("~/App_Data/ExcelTemplate/DeemedPercent.xlsx");
                }
                var list = PayManager.GetPEmployeeIncomes(IncomeId);


                ExcelGenerator.ExportDeemedIncome(template, list, income);
            }
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            var template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");
            var income = new PayManager().GetIncomeById(IncomeId);

            if (income == null)
            {
                return;
            }

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                var datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                int employeeId;
                //float? value;

                var importedCount = 0;
                //var totalMismatchEmployeeCount = 0;


                try
                {
                    var list = new List<PEmployeeIncome>();

                    var name = string.Empty;
                    var rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false
                             && ( row["Employee"].Equals(DBNull.Value) || string.IsNullOrEmpty(row["Employee"].ToString()))
                        )
                        {
                            continue;
                        }

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        name = row["Employee"].ToString();

                        var empIncome = new PEmployeeIncome();
                        empIncome.EmployeeId = employeeId;
                        empIncome.Name = name;

                        if (income.DeemedIsAmount != null && income.DeemedIsAmount.Value)
                        {
                            empIncome.MarketValue = ExcelGenerator.GetAmountValueFromCell("Amount", row, employeeId);
                            empIncome.Amount = 0;

                            if (empIncome.MarketValue == null)
                                empIncome.MarketValue = 0;

                            empIncome.MarketValue = decimal.Parse(empIncome.MarketValue.Value.ToString("n2"));
                        }
                        else
                        {
                            var rate = ExcelGenerator.GetAmountValueFromCell("Rate %", row, employeeId);

                            if (rate == null)
                            {
                                empIncome.RatePercent = 0;
                            }
                            else
                            {
                                empIncome.RatePercent = (double)rate.Value;
                            }
                        }

                        importedCount += 1;

                        list.Add(empIncome);
                    }

                    var count = 0;
                    var status = PayManager.SaveDeemedIncome(list,  ref count, income);

                    if (status.IsSuccessType == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }


                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                                                            count);
                    divMsgCtl.Hide = false;


                    this.HasImport = true;
                }
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}

