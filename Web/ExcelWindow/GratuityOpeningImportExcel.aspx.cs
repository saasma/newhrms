﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class GratuityOpeningImportExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/GratuityOpeningWorkDay.xlsx");        
                 
            // Retrieve data from SQL Server table.
            //List<DAL.GetForTaxHistoryResult> list = TaxManager.GetForTaxHistory("", 0, 999999, ref total);

            int totalRecords = 0;
            List<DAL.GetForWorkdaysResult> list
                = EmployeeManager.GetWorkdays("", 0, 9999, ref totalRecords);

            List<TextValue> accountList = EmployeeManager.GetAccountNoForAllEmp();

            ExcelGenerator.ExportGratuityWorkdayOpening(template, list, accountList);
        }


        

      

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = "Default sheet name 'Sheet1' can not be changed.";
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<GetForWorkdaysResult> list = new List<GetForWorkdaysResult>();

                int employeeId;
                //decimal? grossAmount, pf, cit, insurance, sst, tds, remoteArea;

                float? workday = 0;
                int? upl = 0;
                decimal? adjustment = 0;

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        // don't validate, while placing input should check for it
                        //if (!employeeIDList.Contains(employeeId))
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                        //    divWarningMsg.Hide = false;
                        //    return;

                        //    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}

                        importedCount += 1;

                        GetForWorkdaysResult item = new GetForWorkdaysResult();
                        item.EmployeeId = employeeId;

                        workday = ExcelGenerator.GetValueFromCell("Workdays Adjustment", row, employeeId);
                        //Days_At_Kirne = ExcelGenerator.GetValueFromCell("Days_At_Kirne", row, employeeId);

                        upl = ExcelGenerator.GetIntValueFromCell("UPL for Gratuity", row, employeeId);

                        adjustment = ExcelGenerator.GetAmountValueFromCell("Gratuity Amount Adjustment", row, employeeId);


                        item.WorkdayAdjustment = workday == null ? 0 : workday.Value;
                        item.UnpaidLeavesForGratuity = upl == null ? 0 : upl.Value;
                        item.GratuityAmountAdjustment = adjustment == null ? 0 : adjustment.Value;

                        list.Add(item);
                    }




                    EmployeeManager.UpdateWorkday(list);

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                divWarningMsg.InnerHtml = "xlsx file is required.";
                divWarningMsg.Hide = false;
            }
        }
        
     
       
    }

   
}

