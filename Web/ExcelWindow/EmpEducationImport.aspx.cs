﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class EmpEducationImport : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            //if (SessionManager.User.URole.RoleId != (int)Role.Administrator)
            //{
            //    Response.Write("Not enough permisson.");
            //    Response.End();
            //    return;
            //}

        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmpEducationImport.xlsx");        
                 
            string[] listEducationLevel = CommonManager.GetEducationLevelList().Select(x => x.Name).ToArray();
            string[] listEducationFaculty = CommonManager.GetEducationFacultyList().Select(x => x.Name).ToArray();
            string[] listDivision = ListManager.GetAllDivisions().Select(x => x.Division).ToArray();
            string[] listCountries = new CommonManager().GetAllCountries().Select(x => x.CountryName).ToArray();

            string path = HttpContext.Current.Server.MapPath(template);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();
                    ExcelGenerator _objExcelGenerator = new ExcelGenerator();
                    ExcelGenerator.WriteIntoSheet(conn, "LevelSheet", listEducationLevel);
                    ExcelGenerator.WriteIntoSheet(conn, "FacultySheet", listEducationFaculty);
                    ExcelGenerator.WriteIntoSheet(conn, "CountrySheet", listCountries);
                    ExcelGenerator.WriteIntoSheet(conn, "DivisionSheet", listDivision);
                }
            }

            catch (Exception ex)
            {
                divWarningMsg.InnerHtml = ex.ToString();
                divWarningMsg.Hide = false;
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(template));

        }

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID(); 
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<FixedValueEducationLevel> listEducationLevel = CommonManager.GetEducationLevelList().ToList();
                List<FixedValueEducationFaculty> listEducationFaculty = CommonManager.GetEducationFacultyList().ToList();
                List<FixedValueDivision> listDivision = ListManager.GetAllDivisions().ToList();
                List<CountryList> listCountries = new CommonManager().GetAllCountries().ToList();

                int employeeId;
                
                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                List<HEducation> list = new List<HEducation>();

                string level, faculty, country = "", division = "";

                //if (BLL.BaseBiz.PayrollDataContext.HEducations.Any(x => x.EmployeeId == 3))
                //{
                //    divWarningMsg.InnerHtml = "Data already imported.";
                //    divWarningMsg.Hide = false;
                //    return;
                //}

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);
                        
                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        HEducation obj = new HEducation();
                        obj.EmployeeId = employeeId;

                        if (!string.IsNullOrEmpty(row["Course Name"].ToString().Trim()))
                            obj.Course = row["Course Name"].ToString().Trim();

                        level = row["Level"].ToString().Trim();
                        if (!string.IsNullOrEmpty(level))
                        {                            
                            FixedValueEducationLevel objEduLevel = listEducationLevel.SingleOrDefault(x => x.Name.ToLower() == level.ToLower());
                            if (objEduLevel == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid level for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            obj.LevelTypeID = objEduLevel.ID;
                            obj.LevelName = objEduLevel.Name;
                        }
                        
                        faculty = row["Faculty"].ToString().Trim();
                        if (!string.IsNullOrEmpty(faculty))
                        {                            
                            FixedValueEducationFaculty objFaculty = listEducationFaculty.SingleOrDefault(x => x.Name.ToLower() == faculty.ToLower());
                            if (objFaculty == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid faculty for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            obj.FacultyID = objFaculty.ID;
                            obj.FacultyName = objFaculty.Name;
                        }

                        if(!string.IsNullOrEmpty(row["Institute"].ToString().Trim()))
                            obj.College = row["Institute"].ToString().Trim();

                        if(!string.IsNullOrEmpty(row["University"].ToString().Trim()))
                            obj.University = row["University"].ToString().Trim();

                        country = row["Country"].ToString().Trim();
                        if (!string.IsNullOrEmpty(country))
                        {                          
                            CountryList objCountry = listCountries.SingleOrDefault(x => x.CountryName.ToLower() == country.ToLower());
                            if (objCountry == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid country for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            obj.Country = objCountry.CountryName;
                        }
                        
                        if(!string.IsNullOrEmpty(row["Percentage/Grade"].ToString().Trim()))
                            obj.Percentage = row["Percentage/Grade"].ToString().Trim();

                        division = row["Division"].ToString().Trim();
                        if (!string.IsNullOrEmpty(division))
                        {
                            FixedValueDivision objDivision = listDivision.SingleOrDefault(x => x.Division.ToLower() == division.ToLower());
                            if (objDivision == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid division for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            obj.DivisionID = objDivision.DivisionId;
                            obj.DivisionName = objDivision.Division;
                        }

                        if (!string.IsNullOrEmpty(row["Passed Year"].ToString().Trim()))
                        {

                            int val = 0;

                            if (int.TryParse(row["Passed Year"].ToString().Trim(), out val) == false)
                            {
                                divWarningMsg.InnerHtml = "Invalid passed year for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            obj.PassedYear = int.Parse(row["Passed Year"].ToString().Trim());
                        }

                        if (!string.IsNullOrEmpty(row["Major Subjects"].ToString().Trim()))
                            obj.MajorSubjects = row["Major Subjects"].ToString().Trim();
                        
                        obj.Status = (int)HRStatusEnum.Approved;
                        obj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        obj.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();

                        list.Add(obj);
                    }

                    importedCount = list.Select(x => x.EmployeeId).Distinct().Count();

                    Status status = NewHRManager.ImportEmployeeEducation(list);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

