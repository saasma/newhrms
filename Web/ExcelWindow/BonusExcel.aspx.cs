﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class BonusExcel : BasePage
    {
        int bonusId;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            bonusId = int.Parse(Request.QueryString["bonusId"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/BonusImport.xlsx");        
                 
           // Retrieve data from SQL Server table.
           // List<DAL.GetForTaxHistoryResult> list = TaxManager.GetForTaxHistory("", 0, 999999, ref total);

            List<BonusEmployeeBO> list = CommonManager.GetBonusEmployee(bonusId);

            ExcelGenerator.ExportBonus(template, list);

        }


        

        

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            bool status = CommonManager.IsBonusEditable(bonusId);

            if (status==false)
            {
                divWarningMsg.InnerHtml = "Bonus already calculated, can not be changed.";
                divWarningMsg.Hide = false;
                return;
            }


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


                List<BonusEmployeeBO> list = new List<BonusEmployeeBO>();
                int employeeId;
              
                string empList = "";
                int importedCount = 0;



                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;

                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        BonusEmployeeBO item = new BonusEmployeeBO();
                        list.Add(item);
                        item.EmployeeId = employeeId;



                        item.UnpaidLeave = ExcelGenerator.GetValueFromCell("Unpaid Leave", row, employeeId);
                        item.OtherUneligibleDays = ExcelGenerator.GetValueFromCell("Other Deduct Days", row, employeeId);
                        item.OtherUneligibleDaysReason = row["Other Deduct Days Reason"].ToString();

                        item.AnnualSalary = ExcelGenerator.GetAmountValueFromCell("Annual Salary", row, employeeId);
                        //Days_At_Kirne = ExcelGenerator.GetValueFromCell("Days_At_Kirne", row, employeeId);

                     
                       
                        importedCount += 1;

                    }

                    CommonManager.ImportBonus(list,bonusId);

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

