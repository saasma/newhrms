﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class MassIncrementSalary : BasePage
    {
        //int payrollPerodId = 0;
        ////int typeId=0;
        ////int sourceValueId=0;
        //KeyValue keyValue = null;

        int incomeId = -1;
        int status = -1;
        int designationId = -1;
        string date = "";
        int groupId = -1;
        int levelId = -1;
        int eventId = -1;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["incomeId"]))
                incomeId = int.Parse(Request.QueryString["incomeId"]);

            if (!string.IsNullOrEmpty(Request.QueryString["status"]))
                status = int.Parse(Request.QueryString["status"]);

            if (!string.IsNullOrEmpty(Request.QueryString["designationId"]))
                designationId = int.Parse(Request.QueryString["designationId"]);

            if (!string.IsNullOrEmpty(Request.QueryString["levelId"]))
                levelId = int.Parse(Request.QueryString["levelId"]);


            if (!string.IsNullOrEmpty(Request.QueryString["groupId"]))
                groupId = int.Parse(Request.QueryString["groupId"]);


            if (!string.IsNullOrEmpty(Request.QueryString["eventId"]))
                eventId = int.Parse(Request.QueryString["eventId"]);

            if (!string.IsNullOrEmpty(Request.QueryString["date"]))
                date = (Request.QueryString["date"]);

            if (!IsPostBack && !string.IsNullOrEmpty(date))
            {
                title.InnerHtml += (date + "(" + GetEngDate(date).ToString("yyyy-MMM-dd")+ ") of "  );
                string eventName = CommonManager.GetServieName(eventId);
                title.InnerHtml += eventName + " event type";
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            int? _tempCount = 0;
            PayrollPeriod validPayrollPeriod = CommonManager.GetValidLastPayrollPeriod();
            int payrollPeriodId = validPayrollPeriod == null ? 0 : validPayrollPeriod.PayrollPeriodId;
            decimal filterAmount = 0;
            

            List<GetEmployeeListForMassIncrementResult> list = BLL.Manager.PayManager.GetEmployeeListForMassIncrementResult(incomeId,
                status, -1, designationId,
                SessionManager.CurrentCompanyId, payrollPeriodId, 1,
               99999, ref _tempCount, ""
                , -1, -1, filterAmount, -1,"",groupId,levelId,-1);

            string template = ("~/App_Data/ExcelTemplate/SalaryIncrementImport.xlsx");



            int totalRecords = 0;

            string incomeTitle = "";
            PIncome inc = new PayManager().GetIncomeById(incomeId);
            {
                ExcelGenerator.ExportSalaryIncrement(template, list, inc==null? "" : inc.Title);
            }

        }


        

       

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            if (string.IsNullOrEmpty(date) || eventId == -1)
            {
                divWarningMsg.InnerHtml = "Date and event selection is required.";
                divWarningMsg.Hide = false;
                return;
            }

            PayrollPeriod payrollPeriod = CommonManager.GetLastPayrollPeriod();

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<GetEmployeeListForMassIncrementResult> list = new List<GetEmployeeListForMassIncrementResult>();

                int employeeId; string dateEngValue = "", dateNepValue = "";
                DateTime engDate = DateTime.MinValue;
                string nepDate = null;
                decimal? existingAmount, newAmount, increasedBy;
               
              

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        //int.TryParse(row["EIN"].ToString(), out employeeId);
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        //headerName = row["Income/Deduction Head"].ToString();

                        //if( headerName.ToLower().Trim() != keyValue.Key.ToLower().Trim())
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid Income/Deduction Head.";
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}

                        existingAmount = ExcelGenerator.GetAmountValueFromCell("Existing Income", row, employeeId);
                        newAmount = ExcelGenerator.GetAmountValueFromCell("New Income", row, employeeId);
                        increasedBy = ExcelGenerator.GetAmountValueFromCell("Increased By", row, employeeId);
                        

                        GetEmployeeListForMassIncrementResult item = new GetEmployeeListForMassIncrementResult();
                        item.EmployeeId = employeeId;
                        item.ExistingIncome = existingAmount == null ? double.Parse("0") : Convert.ToDouble(existingAmount);
                        item.IncreasedBy = increasedBy == null ? 0 : increasedBy.Value;


                        if (CalculationManager.IsCalculationSavedForEmployee(payrollPeriod.PayrollPeriodId, employeeId))
                        {
                            divWarningMsg.InnerHtml = string.Format("Salary is already saved for the employee \"{0}\" for current period.", EmployeeManager.GetEmployeeById(employeeId).Name);
                            divWarningMsg.Hide = false;

                            return;
                        }

                        if ((newAmount == null || newAmount == 0) && increasedBy ==0)
                        {
                            item.IncrementIncome = 0;
                        }
                        else
                            item.IncrementIncome = Convert.ToDouble(newAmount) - item.ExistingIncome;

                        item.NewIncome = newAmount == null ? 0 : newAmount.Value;
                        

                        item.EventID = eventId;

                        // if has eng date
                        dateEngValue = row["Eng Date"].ToString().Trim();
                        dateNepValue = row["Effective Date"].ToString().Trim();

                        if (!string.IsNullOrEmpty(dateEngValue) && DateTime.TryParse(dateEngValue, out engDate))
                        {
                            item.EffectiveDateEng = engDate;
                            item.EffectiveDate = BLL.BaseBiz.GetAppropriateDate(item.EffectiveDateEng.Value);
                        }
                        else if (!string.IsNullOrEmpty(dateNepValue))
                        {
                            try
                            {
                                item.EffectiveDate = dateNepValue;
                                item.EffectiveDateEng = GetEngDate(dateNepValue);
                            }
                            catch
                            {
                                divWarningMsg.InnerHtml = "Invalid Effective date format in row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }
                        else
                        {
                            item.EffectiveDate = date;
                            item.EffectiveDateEng = GetEngDate(date);
                        }

                        list.Add(item);
                    }




                    if (importedCount > 0)
                    {

                        string msg = PayManager.ImportMassIncrement(list, incomeId);

                        if (msg.ToLower().Contains("Import successfull".ToLower()))
                        {
                            divMsgCtl.InnerHtml = msg;
                            divMsgCtl.Hide = false;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = msg;
                            divWarningMsg.Hide = false;
                        }
                        this.HasImport = true;
                    }
                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

