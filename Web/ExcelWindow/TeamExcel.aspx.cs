﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class TeamExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               this.CustomId = UrlHelper.GetIdFromQueryString("positionId");                
            }


        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/TeamImport.xlsx");


            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteTeamExcel(template);
        }


        

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID();
        }

        private int GetStepIdByName(string name, List<Step> steps)
        {
            foreach (Step step in steps)
                if (step.Name.Equals(name))
                    return step.StepId;
            return -1;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


               

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }



                int importedCount = 0;

                List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
                List<int> empIDList = GetEmployeeIdList();
                List<LeaveProject> leaveProjects = LeaveAttendanceManager.GetAllLeaveProject();
                int empManagerId = 0;

                int rowIndex = 2;
                try
                {
                    List<LeaveProject> list = new List<LeaveProject>();

                    string branch;
                    string teamName;
                    string managerEmpId;

                    foreach (DataRow row in datatable.Rows)
                    {
                        branch = row["Branch"].ToString();
                        teamName = row["Team Name"].ToString();
                        managerEmpId = row["Manager EmployeeId"].ToString();

                        branch = branch == null ? "" : branch.Trim();
                        Branch branchEntity = branchList.FirstOrDefault(x => x.Name.ToLower() == branch.ToLower());

                        if (branchEntity == null)
                        {
                            divWarningMsg.InnerHtml = "Invalid branch for the row " + rowIndex;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (string.IsNullOrEmpty(teamName))
                        {
                            divWarningMsg.InnerHtml = "Team name is required for the row " + rowIndex;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (leaveProjects.Any(x => x.Name.ToLower() == teamName.ToLower()))
                        {
                            divWarningMsg.InnerHtml = "Team name \"" + teamName + "\" already exists for the row " + rowIndex;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (list.Any(x => x.Name.ToLower() == teamName.ToLower()))
                        {
                            divWarningMsg.InnerHtml = "Team name \"" + teamName + "\" repeated for the row " + rowIndex;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (string.IsNullOrEmpty(managerEmpId))
                        {
                            divWarningMsg.InnerHtml = "Invalid Manager Employee Id for the row " + rowIndex;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (int.TryParse(managerEmpId, out empManagerId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Manager Employee Id for the row " + rowIndex;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (empIDList.Contains(empManagerId) == false)
                        {
                            divWarningMsg.InnerHtml = "Manager Employee Id does not exists for the row " + rowIndex;
                            divWarningMsg.Hide = false;
                            return;
                        }




                        LeaveProject pro = new LeaveProject();
                        pro.BranchId = branchEntity.BranchId;
                        pro.Name = teamName;
                        pro.ProjectManagerEmployeeId = empManagerId;
                        pro.DepartmentId = -1;

                        list.Add(pro);
                        // add to total mis match employee count for showing message
                        //if (totalHoursOrDays != totalHoursOrDaysInAMonth)
                        //    totalMismatchEmployeeCount += 1;

                        rowIndex += 1;

                    }


                    LeaveAttendanceManager.SaveLeaveProject(list);


                    divMsgCtl.InnerHtml = "Import successfull.";
                    divMsgCtl.Hide = false;
                    //if (totalMismatchEmployeeCount == 0)
                    //{
                    //    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                    //                                        importedCount);
                    //    divMsgCtl.Hide = false;
                    //}
                    //else
                    {
                        //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                        //                                        importedCount)
                        //                          + string.Format(" For {0} employees, total hours {1} doesn't match, please check in the details view.",totalMismatchEmployeeCount,totalHoursOrDaysInAMonth);
                        //divWarningMsg.Hide = false; 
                    }

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

