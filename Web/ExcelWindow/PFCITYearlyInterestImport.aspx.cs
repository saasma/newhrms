﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class PFCITYearlyInterestImport : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int FinancialYearID = int.Parse(Request.QueryString["id"]);

            if( new CommonManager().GetAllFinancialDates().Any(x=>x.FinancialDateId == FinancialYearID))
            {
                FinancialDate date = new CommonManager().GetAllFinancialDates().FirstOrDefault(x => x.FinancialDateId == FinancialYearID);
                date.SetName(IsEnglish);
                
                txtSelectedYear.Text = "The Financial Year selected: "+ date.Name;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {


            string template = ("~/App_Data/ExcelTemplate/PFCITYearlyInterestImport.xlsx");

            int FinancialYearID = int.Parse(Request.QueryString["id"]);
                 
            int total=0;
            // Retrieve data from SQL Server table.
            List<DAL.GetInterestImportResult> list = TaxManager.GetForPFYearlyInterest("", 0, 99999, ref total, FinancialYearID);

            ExcelGenerator.ExportPFCITYearLyInterst(template, list);
        }


        

        private List<int> GetEmployeeIdList()
        {
            int FinancialYearID = int.Parse(Request.QueryString["id"]);
            int total = 0;
            List<DAL.GetInterestImportResult> list = TaxManager.GetForPFYearlyInterest("", 0, 999, ref total, FinancialYearID);
            List<int> ids = new List<int>();
            foreach (var item in list)
            {
                ids.Add(item.EmployeeId);
            }
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {

            //JavascriptHelper.DisplayClientMsg("The PF/CIT Interests that has been already save will be updated.", Page);

            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xlsx");

            List<PFCITYearlyInterest> insertInstance = new List<PFCITYearlyInterest>();
            PFCITYearlyInterest thisInstance = new PFCITYearlyInterest();

            int FinancialYearID = int.Parse(Request.QueryString["id"]);


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xlsx"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


                int employeeId;
                decimal?  PFInterest, CITInterest;

                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row[0].ToString(), out employeeId);
                        
                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                            //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            //divWarningMsg.Hide = false;
                            //return;
                        }

                        importedCount += 1;


                        PFInterest = ExcelGenerator.GetAmountValueFromCell("PFInterest", row, employeeId);
                        CITInterest = ExcelGenerator.GetAmountValueFromCell("CITInterest", row, employeeId);

                        thisInstance = new PFCITYearlyInterest();
                        if(employeeId!=null)
                        {
                            thisInstance.EmployeeId = employeeId;
                            thisInstance.FinancialDateId = FinancialYearID;
                            thisInstance.PFInterest = PFInterest == null ? 0 : PFInterest.Value;
                            thisInstance.CITInterest = CITInterest == null ? 0 : CITInterest.Value;
                        }

                        if (thisInstance != null && employeeId != null & employeeId != 0)
                        {
                            insertInstance.Add(thisInstance);
                        }
                        
                        /*
                        xml.Append(
                            string.Format("<row EmployeeId=\"{0}\" Amount=\"{1}\" PF=\"{2}\" CIT=\"{3}\"  Insurance=\"{4}\"  TaxPaid=\"{5}\" OpeningSST=\"{6}\"  OpeningRemoteArea=\"{7}\" /> ",
                                 employeeId, grossAmount, pf, cit, insurance, tds, sst,remoteArea)
                            );

                    */
                      

                    }



                    //xml.Append("</root>");

                    //TaxManager.SaveForTax(xml.ToString());

                    Status status = TaxManager.ImportPFCITYearlyInterest(insertInstance);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = string.Format("PF/CIT Opening Imported", importedCount);
                        divMsgCtl.Hide = false;
                    }

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
        
     
       
    }

   
}

