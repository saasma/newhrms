﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class EmpMaritalTaxStatusImportExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

          
        }

       
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmpMaritalTaxStatus.xlsx");

            List<GetEmployeeMaritalAndTaxStatusListResult> list = NewHRManager.GetEmployeeMaritalAndTaxStatusList("", -1, -1, -1, -1,
                "", -1, -1, "", null);

            ExcelGenerator.ExportEmpMaritalTaxStatusDetails(template, list);
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xlsx");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xlsx"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<GetEmployeeMaritalAndTaxStatusListResult> list = new List<GetEmployeeMaritalAndTaxStatusListResult>();


                int employeeId;             

                int importedCount = 0;
                int totalRows = 0;
                object value;
              
                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        GetEmployeeMaritalAndTaxStatusListResult item = new GetEmployeeMaritalAndTaxStatusListResult();

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        item.EmployeeId = employeeId;

                        value = row["Marital Status"]; 
                        if (value == DBNull.Value || string.IsNullOrEmpty(value.ToString().Trim()))
                        {
                            divWarningMsg.InnerHtml = "Marital stauts is required for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        item.MaritalStatus = value.ToString();


                        value = row["Has Couple Tax Status"];
                        if (value != DBNull.Value && !string.IsNullOrEmpty(value.ToString().Trim()))
                        {
                            if (value.ToString().ToLower() == "yes")
                            {
                                if (item.MaritalStatus != MaritalStatus.MARRIED)
                                {
                                    divWarningMsg.InnerHtml = string.Format("{0} status cannot have couple tax status for employee id : {1} of row {2}", item.MaritalStatus, employeeId, rowNumber);
                                    divWarningMsg.Hide = false;
                                    return;
                                }

                                item.HasCoupleTaxStatus = true;

                            }
                            else
                                item.HasCoupleTaxStatus = false;
                        }


                        list.Add(item);

                    }



                    Status status = NewHRManager.UpdateEmployeeMaritalAndTaxStatus(list);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = "Employee marital and tax status updated successfully.";
                        divMsgCtl.Hide = false;

                        this.HasImport = true;  
                    }
                    else
                    {

                        divWarningMsg.InnerHtml = "Error while updating employee marital and tax status.";
                        divWarningMsg.Hide = false;
                    }
                    

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
       
     
       
    }

   
}

