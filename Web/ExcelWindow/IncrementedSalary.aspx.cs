﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class IncrementedSalary : BasePage
    {
        int payrollPerodId = 0;
        int typeId=0;
        int sourceValueId=0;
        KeyValue keyValue = null;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

           // string template = ("~/App_Data/ExcelTemplate/IncomeDeductionAdjustment.xlsx");

           
         
           // int totalRecords = 0;

           // List<GetIncomeDeductionAdjustmentEmployeesResult> list = PayManager.GetIncomeDeductionAdjustment(
           // payrollPerodId, -1, -1,
           //  -1, sourceValueId, typeId,
           // 0, 99999, ref totalRecords);

          

           //if (keyValue != null)
           //{
           //    ExcelGenerator.ExportAdjustment(template, list,keyValue.Key);
           //}
        }


        

       

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

          
            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<GetEmployeeListForMassIncrementResult> list = new List<GetEmployeeListForMassIncrementResult>();

                int employeeId; string headerName = "", note = "";
                decimal? existingAmount,newAmount;
               
              

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        //int.TryParse(row["EIN"].ToString(), out employeeId);
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                       
                        importedCount += 1;

                        //headerName = row["Income/Deduction Head"].ToString();

                        //if( headerName.ToLower().Trim() != keyValue.Key.ToLower().Trim())
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid Income/Deduction Head.";
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}

                        existingAmount = ExcelGenerator.GetAmountValueFromCell("Existing Income", row, employeeId);
                        newAmount = ExcelGenerator.GetAmountValueFromCell("New Income", row, employeeId);


                        
                            GetEmployeeListForMassIncrementResult item = new GetEmployeeListForMassIncrementResult();
                            item.EmployeeId = employeeId;
                        item.ExistingIncome =  existingAmount == null ? double.Parse("0") : Convert.ToDouble(existingAmount);

                        if (newAmount == null || newAmount == 0)
                        {
                            item.IncrementIncome = 0;
                        }
                        else
                            item.IncrementIncome = Convert.ToDouble( newAmount) - item.ExistingIncome;



                        list.Add(item);
                    }




                    if (importedCount > 0)
                    {

                        Session["IncrementList"] = list;

                        divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

