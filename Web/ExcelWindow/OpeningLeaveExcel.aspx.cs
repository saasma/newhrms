﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;
using Utils;


namespace Web.CP
{
    public partial class OpeningLeaveExcel : BasePage
    {
       
        CommonManager mgr = new CommonManager();

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/OpeningLeave.xlsx");


            string statusFilterList = Request.QueryString["status"];
            statusFilterList = Server.UrlDecode(statusFilterList);
            JobStatus statues = new JobStatus();
            List<KeyValue> statusList = statues.GetMembers();
            string[] statusTexts = statusFilterList.Split(new char[] { ',' });
            string statusIDTexts = "";
            foreach (string item in statusTexts)
            {
                string text = item.ToString().ToLower().Trim();
                KeyValue status = statusList.FirstOrDefault(x => x.Value.ToString().ToLower().Trim() == text);
                if (status != null)
                {
                    if (statusIDTexts == "")
                        statusIDTexts = status.Key;
                    else
                        statusIDTexts += "," + status.Key;
                }
            }


            
            string[] leaveList = LeaveAttendanceManager.GetLeaveListForImport(SessionManager.CurrentCompanyId).Select(a => a.Title).ToArray();
            List<LLeaveType> leaves = LeaveAttendanceManager.GetLeaveListForImport(SessionManager.CurrentCompanyId).ToList();

            int firstPayrollPeriodId = CommonManager.GetFirstPayrollPeriod(SessionManager.CurrentCompanyId).PayrollPeriodId;
            List<EEmployee> employeeList = new List<EEmployee>();

            if (string.IsNullOrEmpty(statusIDTexts))
            {
                employeeList = LeaveAttendanceManager.GetEmployeeListInAttendance(firstPayrollPeriodId);

                if (employeeList.Count > 0 &&
                    BLL.BaseBiz.PayrollDataContext.PayrollPeriods.Count() >= 2)
                {
                }
                else//if (employeeList.Count <= 0)
                {
                    employeeList = EmployeeManager.GetAllEmployees();
                }
            }
            else
            {
                // get list with status filter
                int totalRecords = 0;

                DAL.Entity.LeaveOpeningBalEntityCol list =
                    new LeaveAttendanceManager().GetLeaveOpeningBalance(SessionManager.CurrentCompanyId,
                        -1, "", 0, 9999, ref totalRecords, statusIDTexts);

                List<int> ein = new List<int>();
                foreach (var item in list)
                {

                    //if (ein.Contains(item.EmployeeId) == false)
                    {
                        ein.Add(item.EmployeeId);
                        //employeeList.Add(new EEmployee { EmployeeId = item.EmployeeId, Name = item.Name });
                    }
                }

                employeeList = LeaveAttendanceManager.GetSelectedEmployeeList(ein);

            }

            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteOpeningLeaveExportExcel(employeeList,template, leaveList, leaves);

        }

        public EEmployee SetImportError(string msg, EEmployee emp)
        {
            emp.IsValidImport = false;
            emp.ImportErrorMsg = msg + " for the employee \"" + (emp.Name == null ? "" : emp.Name) + "\".";
            return emp;
        }


       

        public EEmployee GetImportedEmployee(List<LLeaveType> leaveList, DataRow row, DataTable table)
        {
            // If all Columns Empty then return null
           //if( row[ExcelGenerator.employeesStaticColumns[0]] == DBNull.Value &&
              
           //    row[ExcelGenerator.employeesStaticColumns[1]] == DBNull.Value)
           //    return null;


            EEmployee emp = new EEmployee();
         
            emp.IsValidImport = true;

            object value; decimal amount;

            // EIN Column
            value = row["EIN"];
            if (value != null && value.ToString() != "")
            {
                int ein;
                if (int.TryParse(value.ToString(), out ein))
                    emp.EmployeeId = ein;
                else
                    return SetImportError("Invalid EIN", emp);
            }

          
            if (!ProcessLeave(emp, leaveList, row))
                return emp;

            return emp;
        }

       

        public bool ProcessLeave(EEmployee emp, List<LLeaveType> leaveList, DataRow row)
        {
            object value; decimal amount;
            //Deduction Columns
            foreach (LLeaveType leave in leaveList)
            {
                if (row.Table.Columns.Contains(leave.Title) == false)
                    continue;

                value = row[leave.Title];

                if (value == null || value.ToString().Trim() == "")
                {
                    continue; //for other skip the Income 
                }

                bool canHaveCurrentYearBalance = leave.FreqOfAccrual == LeaveAccrue.YEARLY;

                LEmployeeLeave empLeave = new LEmployeeLeave();

                if (value.ToString().Contains("_"))
                {
                    string[] values = value.ToString().Split(new char[]{'_'});

                    if (!decimal.TryParse(values[0], out amount)
                        || !decimal.TryParse(values[1], out amount))
                    {
                        SetImportError(string.Format("Invalid value for {0} leave", leave.Title), emp);
                        return false;
                    }

                    empLeave.BeginningBalance = double.Parse(values[0]);
                    empLeave.CurrentYearBalance = double.Parse(values[1]);
                    if (values.Length >= 3)
                        empLeave.OpeningTaken = double.Parse(values[2]);
                }
                else
                {
                    if (!decimal.TryParse(value.ToString().Trim(), out amount))
                    {
                        SetImportError(string.Format("Invalid value for {0} leave", leave.Title), emp);
                        return false;
                    }

                    empLeave.BeginningBalance = (double)amount;
                }

                




               
                empLeave.EditSequence = 1;
                empLeave.LeaveTypeId = leave.LeaveTypeId;
            
                empLeave.Accured = 0;
                empLeave.IsOpeningBalanceSaved = true;
                empLeave.IsActive = true;



                emp.LEmployeeLeaves.Add(empLeave);
            }

            return true;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListForImport(SessionManager.CurrentCompanyId);


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                try
                {

                    DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Employee_Info$]");

                    if (datatable == null)
                    {
                        File.Delete(template);
                        return;
                    }

                    List<EEmployee> employeeList = new List<EEmployee>();


                    foreach (DataRow row in datatable.Rows)
                    {

                        EEmployee emp = GetImportedEmployee(leaveList, row, datatable);

                        if (emp == null)
                            continue;

                        if (emp.IsValidImport)
                        {
                            emp.CompanyId = SessionManager.CurrentCompanyId;
                                                    
                          
                        
                            employeeList.Add(emp);

                        }
                        if (emp.IsValidImport == false)
                        {
                            divWarningMsg.InnerHtml = emp.ImportErrorMsg;
                            divWarningMsg.Hide = false;
                            return;
                        }

                    }

                    int count = 0;
                    if (EmployeeManager.SaveImportedOpeningLeave(employeeList,ref count))
                    {
                        divMsgCtl.InnerHtml = string.Format("{0} employees imported successfully.", count);
                        divMsgCtl.Hide = false;
                    }

                }




                   //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                         exp.ColumnName, "") + " : " + exp.ToString();
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError) + " : " + exp.ToString();
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError) + " : " + exp.ToString();
                    divWarningMsg.Hide = false;
                }
                //catch (CompulsoryImportFieldException exp1)
                //{
                //    divMsgCtl.Text = exp1.Message;
                //}

                finally
                {
                    File.Delete(template);
                }

                this.HasImport = true;
            }


        }

    }
}

   


