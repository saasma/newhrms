﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using System.Data;
using System.Data.OleDb;
using System.IO;
using Utils.Calendar;

namespace Web.ExcelWindow
{
    public partial class SILoanImportExcel : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                {
                    return false;
                }
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public int DeductionId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            DeductionId = int.Parse(Request.QueryString["DeductionId"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            var pDeduction = LoanManager.GetPDeductionById(DeductionId);

            if (pDeduction != null)
            {
                var template = string.Empty;
                template = ("~/App_Data/ExcelTemplate/SILoanImport.xlsx");
                var list = LoanManager.GetPEmployeeDeductions(DeductionId);

                ExcelGenerator.ExportSILoanImport(template, list, pDeduction);
            }
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            var template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");
            var pDeduction = LoanManager.GetPDeductionById(DeductionId);

            if (pDeduction == null)
            {
                return;
            }

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                var datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                int employeeId;

                var importedCount = 0;

                try
                {
                    var list = new List<PEmployeeDeduction>();

                    var name = string.Empty;
                    var rowNumber = 1;


                    foreach (DataRow row in datatable.Rows)
                    {
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false
                             && (row["Employee Name"].Equals(DBNull.Value) || string.IsNullOrEmpty(row["Employee Name"].ToString()))
                        )
                        {
                            continue;
                        }

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        name = row["Employee Name"].ToString();

                        var empDeduction = new PEmployeeDeduction();
                        empDeduction.EmployeeId = employeeId;
                        empDeduction.EmployeeName = name;


                        double loanAmount = 0;
                        double interestRate =0;


                        if (row["Loan Amount"] != DBNull.Value && row["Loan Amount"].ToString() != "")
                        {
                            empDeduction.AdvanceLoanAmount = ExcelGenerator.GetAmountValueFromCell("Loan Amount", row, employeeId);
                            empDeduction.AmountWithInitialInterest = empDeduction.AdvanceLoanAmount;
                            loanAmount = double.Parse(row["Loan Amount"].ToString());
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Loan Amount is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        

                        CustomDate customStartDate = null;
                        CustomDate customEndDate = null;

                        DateTime date = new DateTime();

                        if (row["Loan Taken On Eng"] != DBNull.Value && row["Loan Taken On Eng"].ToString() != "")
                        {
                            try
                            {
                                date = Convert.ToDateTime(row["Loan Taken On Eng"].ToString());
                                customStartDate = CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(date), IsEnglish);

                                customStartDate = CustomDate.ConvertEngToNep(customStartDate);

                                empDeduction.TakenOn = new CustomDate(1, customStartDate.Month, customStartDate.Year, IsEnglish).ToString();
                                empDeduction.TakenOnEng = GetEngDate(empDeduction.TakenOn);
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Loan Taken On Eng is invalid for the employee having id : " + employeeId;
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Loan Taken On Eng is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                        }


                        if (row["Interest Rate"] != DBNull.Value && row["Interest Rate"].ToString() != "")
                        {
                            try
                            {
                                empDeduction.InterestRate = double.Parse(row["Interest Rate"].ToString());
                                interestRate = double.Parse(row["Interest Rate"].ToString());
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Interest Rate is invalid for the employee having id : " + employeeId;
                                divWarningMsg.Hide = false;
                            }
                            
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Interest Rate is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Market Rate"] != DBNull.Value && row["Market Rate"].ToString() != "")
                        {
                            try
                            {
                                empDeduction.MarketRate = double.Parse(row["Market Rate"].ToString());
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Market Rate is invalid for the employee having id : " + employeeId;
                                divWarningMsg.Hide = false;
                            }

                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Market Rate is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["No of Installments"] != DBNull.Value && row["No of Installments"].ToString() != "" && row["No of Installments"].ToString() != "0")
                        {
                            int totalInstallment = int.Parse(row["No of Installments"].ToString());
                            if (totalInstallment < 0)
                            {
                                divWarningMsg.InnerHtml = "Invalid No of Installments for the employee having id : " + employeeId;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            empDeduction.ToBePaidOver = totalInstallment;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "No of Installments is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }


                        if (row["Deduction Start Date Eng"] != DBNull.Value && row["Deduction Start Date Eng"].ToString() != "")
                        {
                            try
                            {
                                date = Convert.ToDateTime(row["Deduction Start Date Eng"].ToString());
                                customEndDate = CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(date), IsEnglish);

                                customEndDate = CustomDate.ConvertEngToNep(customEndDate);

                                empDeduction.StartingFrom = new CustomDate(1, customEndDate.Month, customEndDate.Year, IsEnglish).ToString();
                                empDeduction.StartingFromEng = GetEngDate(empDeduction.StartingFrom);
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Deduction Start Date Eng is invalid for the employee having id : " + employeeId;
                                divWarningMsg.Hide = false;
                                return;
                            }                           
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Deduction Start Date Eng is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                        }

                        

                     
                        int lastPaymentMonth = (int)((empDeduction.ToBePaidOver.Value - 1) * 1);

                        DateTime lastPaymentDate = new DateTime(customEndDate.Year, customEndDate.Month, 1).AddMonths(lastPaymentMonth);

                        CustomDate lastPayment = new CustomDate(1, lastPaymentDate.Month, lastPaymentDate.Year, IsEnglish);
                        empDeduction.LastPayment = lastPayment.ToString();//"1/" + lastPaymentDate.Month + "/" + lastPaymentDate.Year;
                        empDeduction.LastPaymentEng = GetEngDate(empDeduction.LastPayment);


                        double Days_In_A_Year;
                        Days_In_A_Year = 360;

                        int totayDaysInStartingMonth = 0;
                        int daysDiffBetTakenOnStartingFrom;// = (startingDateEng - takenOnDateEng).Days;
                        double interest;

                        totayDaysInStartingMonth =
                        DateHelper.GetTotalDaysInTheMonth(empDeduction.StartingFromEng.Value.Year, empDeduction.StartingFromEng.Value.Month, true);

                        if (empDeduction.TakenOnEng > empDeduction.StartingFromEng)
                        {
                            daysDiffBetTakenOnStartingFrom = totayDaysInStartingMonth - (empDeduction.TakenOnEng.Value - empDeduction.StartingFromEng.Value).Days;
                            interest = loanAmount * (interestRate / 100.0) * daysDiffBetTakenOnStartingFrom / Days_In_A_Year;
                        }
                        else
                        {
                            if (CommonManager.CalculationConstant.EMILengthOfPeriod == 0)
                            {
                                divWarningMsg.InnerHtml = "EMI Length Of Period is 0.";
                                divWarningMsg.Hide = false;
                                return;
                            }          
                            

                            interest = loanAmount * (interestRate / 100.0 / CommonManager.CalculationConstant.EMILengthOfPeriod );
                        }

                        empDeduction.InterestAmount = (decimal)interest;
                        empDeduction.InstallmentAmount = empDeduction.AdvanceLoanAmount.Value / empDeduction.ToBePaidOver.Value;
                        empDeduction.Balance = Math.Round((decimal)(loanAmount + interest), 2);
                        empDeduction.NoOfInstallments = empDeduction.ToBePaidOver;

                        importedCount += 1;

                        list.Add(empDeduction);
                    }

                    var count = 0;
                    var status = LoanManager.SaveSILoanImport(list, ref count, pDeduction);

                    if (status.IsSuccessType == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }


                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                                                            count);
                    divMsgCtl.Hide = false;


                    this.HasImport = true;
                }
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}