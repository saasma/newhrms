﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using System.Data;
using System.Data.OleDb;
using System.IO;
using Utils.Calendar;

namespace Web.ExcelWindow
{
    public partial class LoanRepaymentExcel : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                {
                    return false;
                }
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public int DeductionId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            DeductionId = int.Parse(Request.QueryString["DeductionId"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            var pDeduction = LoanManager.GetPDeductionById(DeductionId);

            if (pDeduction != null)
            {
                var template = string.Empty;
                template = ("~/App_Data/ExcelTemplate/LoanRepayment.xlsx");
                var list = LoanManager.GetPEmployeeDeductions(DeductionId);

                ExcelGenerator.ExportLoanRepayment(template, list, pDeduction);
            }
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            var template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");
            var pDeduction = LoanManager.GetPDeductionById(DeductionId);

            if (pDeduction == null)
            {
                return;
            }

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                var datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                int employeeId;
                //float? value;

                var importedCount = 0;
                //var totalMismatchEmployeeCount = 0;

                try
                {
                    var list = new List<PEmployeeDeduction>();

                    var name = string.Empty;
                    var rowNumber = 1;
                    
                    
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false
                             && (row["Employee Name"].Equals(DBNull.Value) || string.IsNullOrEmpty(row["Employee Name"].ToString()))
                        )
                        {
                            continue;
                        }

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        name = row["Employee Name"].ToString();

                        // check employee repeat
                        if (list.Any(x => x.EmployeeId == employeeId))
                        {
                            divWarningMsg.InnerHtml = "EIN " + employeeId + " is being repeated in the import.";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        var empDeduction = new PEmployeeDeduction();
                        empDeduction.EmployeeId = employeeId;
                        empDeduction.EmployeeName = name;

                        if (row["Total Installment"] != DBNull.Value && row["Total Installment"].ToString() != "")
                        {
                            int totalInstallment  = 0;
                            if (int.TryParse(row["Total Installment"].ToString(), out totalInstallment))
                            {
                                //int totalInstallment = int.Parse(row["Total Installment"].ToString());
                                if (totalInstallment < 0)
                                {
                                    divWarningMsg.InnerHtml = "Invalid Total Installment for the employee having id : " + employeeId;
                                    divWarningMsg.Hide = false;
                                    return;
                                }
                            }
                            else
                            {
                                divWarningMsg.InnerHtml = "Invalid Total Installment " + row["Total Installment"].ToString() + " for the employee having EIN : " + employeeId;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            empDeduction.TotalInstallment = totalInstallment;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Total Installment is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Monthly Repayment"] != DBNull.Value && row["Monthly Repayment"].ToString() != "")
                        {
                            empDeduction.Amount = ExcelGenerator.GetAmountValueFromCell("Monthly Repayment", row, employeeId);
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Monthly Repayment is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Installment No"] != DBNull.Value && row["Installment No"].ToString() != "")
                        {
                            int installementNo = int.Parse(row["Installment No"].ToString());
                            if (installementNo < 0)
                            {
                                divWarningMsg.InnerHtml = "Invalid Installment No for the employee having id : " + employeeId;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            empDeduction.InstallmentNo = installementNo;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Installment No is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        CustomDate customStartDate = null;
                        CustomDate customEndDate = null;

                        DateTime startDate = new DateTime();
                        DateTime endDate = new DateTime();
                        DateTime date = new DateTime();
                        int noOfDeductions = 0;

                        if (row["Start Date Eng"] != DBNull.Value && row["Start Date Eng"].ToString() != "")
                        {
                            try
                            {
                                date = Convert.ToDateTime(row["Start Date Eng"].ToString());
                                customStartDate = CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(date), IsEnglish);

                                customStartDate = CustomDate.ConvertEngToNep(customStartDate);

                                empDeduction.StartingFrom = new CustomDate(1, customStartDate.Month, customStartDate.Year, IsEnglish).ToString();
                                empDeduction.StartingFromEng = GetEngDate(empDeduction.StartingFrom);
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Start date is invalid for the employee having id : " + employeeId;
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Start date is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["End Date Eng"] != DBNull.Value && row["End Date Eng"].ToString() != "")
                        {
                            try
                            {
                                date = Convert.ToDateTime(row["End Date Eng"].ToString());
                                customEndDate = CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(date), IsEnglish);

                                //customEndDate = CustomDate.GetCustomDateFromString(row["End Date Eng"].ToString().Replace('-', '/'), IsEnglish);

                                empDeduction.LastPayment = new CustomDate(
                                    DateHelper.GetTotalDaysInTheMonth(customEndDate.Year, customEndDate.Month, IsEnglish), customEndDate.Month, customEndDate.Year, IsEnglish).ToString();
                               
                                empDeduction.LastPaymentEng = GetEngDate(empDeduction.LastPayment);

                                DateTime date1 = empDeduction.StartingFromEng.Value;
                                DateTime date2 = empDeduction.LastPaymentEng.Value;

                                empDeduction.NoOfInstallments =
                                     (((date2.Year - date1.Year) * 12) + date2.Month - date1.Month);
                               
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "End date is invalid for the employee having id : " + employeeId;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            if (row["No of Deductions"] != DBNull.Value && row["No of Deductions"].ToString() != "")
                            {
                                try
                                {
                                    noOfDeductions = int.Parse(row["No of Deductions"].ToString());
                                    empDeduction.NoOfInstallments = noOfDeductions;


                                    DateTime startingDate = new DateTime(customStartDate.Year, customStartDate.Month, 1);
                                    int lastPaymentMonth = (int)((noOfDeductions - 1) * 1);
                                    DateTime lastPaymentDate = startingDate.AddMonths(lastPaymentMonth);

                                    CustomDate endingDate = new CustomDate(
                                         DateHelper.GetTotalDaysInTheMonth(lastPaymentDate.Year, lastPaymentDate.Month, IsEnglish)
                                         , lastPaymentDate.Month, lastPaymentDate.Year, IsEnglish);


                                    empDeduction.LastPaymentEng = endingDate.EnglishDate;
                                    empDeduction.LastPayment = endingDate.ToString();

                                }
                                catch (Exception ex)
                                {
                                    divWarningMsg.InnerHtml = "No of Deductions is invalid for the employee having id : " + employeeId;
                                    divWarningMsg.Hide = false;
                                    return;
                                }
                            }
                            else
                            {
                                divWarningMsg.InnerHtml = "End Date or No of Deductions is required for the employee having id : " + employeeId;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }

                        string takenOn = row["Taken On Eng"].ToString();
                        if (!string.IsNullOrEmpty(takenOn))
                        {
                            DateTime takendate;
                            if (DateTime.TryParse(takenOn, out takendate))
                            {
                                empDeduction.TakenOnEng = takendate;
                                empDeduction.TakenOn = BLL.BaseBiz.GetAppropriateDate(empDeduction.TakenOnEng.Value);
                            }
                        }

                        if (empDeduction.LastPaymentEng < empDeduction.StartingFromEng)
                        {
                            divWarningMsg.InnerHtml = "Start Date cannot be greater than End Date for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Loan Account"] != DBNull.Value && row["Loan Account"].ToString() != "")
                        {
                            empDeduction.LoanAccount = row["Loan Account"].ToString();
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Loan Account is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Advance Loan Amount"] != DBNull.Value && row["Advance Loan Amount"].ToString() != "")
                            empDeduction.AdvanceLoanAmount = ExcelGenerator.GetAmountValueFromCell("Advance Loan Amount", row, employeeId);
                        else
                            empDeduction.AdvanceLoanAmount = null;


                        if (list.Any(x => x.EmployeeId == empDeduction.EmployeeId))
                        {
                            divWarningMsg.InnerHtml = "EIN " + employeeId + " has been repeated multiple times in the excel";
                            divWarningMsg.Hide = false;
                            return;
                        }


                        importedCount += 1;


                        list.Add(empDeduction);
                    }

                    var count = 0;
                    var status = LoanManager.SaveLoanEmployeeDeduction(list, ref count, pDeduction);

                    if (status.IsSuccessType == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }


                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                                                            count);
                    divMsgCtl.Hide = false;


                    this.HasImport = true;
                }
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}