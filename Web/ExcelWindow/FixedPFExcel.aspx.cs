﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class FixedPFExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/FixedPF.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            List<DAL.GetFixedPFDeductionEmpListResult> list =
                TaxManager.GetFixedPFList
                ("", 0, 99999, ref total);

            ExcelGenerator.ExportFixedPFDeduction(template, list);
        }


        

        

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");
                StringBuilder strMonthsAmounts = new StringBuilder("<root>");

                int employeeId;

                bool isFixed = false;
                decimal amount = 0;
                string amountValue = "";
                string type = "";
                int importedCount = 0;

                int totalRows = 0;
                //List<DAL.GetEmployeeListForProvisionalCITResult> list = TaxManager.GetListForPrivisionalCIT("", 0, 999999, ref totalRows);

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        GetFixedPFDeductionEmpListResult item = new GetFixedPFDeductionEmpListResult();

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        type = row["PF Deduction Type"].ToString().ToLower();

                        if (type.Equals("Default PF".ToLower()))
                            isFixed = false;
                        else if (type.Equals("Fixed Amount".ToLower()))
                            isFixed = true;
                        else
                        {
                            divWarningMsg.InnerHtml = "Invalid value for \"PF Deduction Type\" for the employee " + row["Employee"].ToString()
                                                        + " in row number " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                      

                        item.EmployeeId = employeeId;

                        amountValue = row["PF Deduction"].ToString();
                        if (decimal.TryParse(amountValue.ToString().Trim(), out amount) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid  amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        

                        importedCount += 1;

                        //string strExclude = "";

                        //if (excludeValue != null)
                        //    strExclude = excludeValue.ToString().ToLower();


                        xml.Append(
                            string.Format("<row EmployeeId=\"{0}\" IsPFDeductionFixed=\"{1}\" FixedPFDeductionAmount=\"{2}\" /> ",
                                 employeeId, isFixed.ToString(), amount)
                            );

                    }



                    xml.Append("</root>");

                    TaxManager.SaveFixedPFDeduction(xml.ToString());

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

