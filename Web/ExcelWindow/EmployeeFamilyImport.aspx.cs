﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class EmployeeFamilyImport : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            //if (SessionManager.User.URole.RoleId != (int)Role.Administrator)
            //{
            //    Response.Write("Not enough permisson.");
            //    Response.End();
            //    return;
            //}

        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmpFamilyImport.xlsx");

            string[] listFamilyRelations = CommonManager.GetRelationList().Select(x => x.Name).ToArray();
            string[] occupations = ListManager.GetAllOccuations().Select(x => x.Occupation).ToArray();
            string[] BloodGroup = CommonManager.GetAllBloodGroup().Select(x => x.BloodGroupName).ToArray();
            string[] NationalityList = CommonManager.GetCitizenshipNationaliyList().Select(x => x.Nationality).ToArray();
            String[] DocumentTypeList = CommonManager.GetDocumentDocumentTypeList().Select(x => x.DocumentTypeName).ToArray();

            string path = HttpContext.Current.Server.MapPath(template);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    conn.Open();
                    ExcelGenerator _objExcelGenerator = new ExcelGenerator();
                    ExcelGenerator.WriteIntoSheet(conn, "RelationList", listFamilyRelations);
                    ExcelGenerator.WriteIntoSheet(conn, "OccupationList", occupations);
                    ExcelGenerator.WriteIntoSheet(conn, "BloodGroupList", BloodGroup);
                    ExcelGenerator.WriteIntoSheet(conn, "NationalityList", NationalityList);
                    ExcelGenerator.WriteIntoSheet(conn, "DocumentTypeList", DocumentTypeList);
                }
            }

            catch (Exception ex)
            {
                divWarningMsg.InnerHtml = ex.ToString();
                divWarningMsg.Hide = false;
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(template));

        }

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID();
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                int employeeId;

                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                List<HFamily> list = new List<HFamily>();

                string relationName = "", relativeName = "", documentType = "", nationality = "", bloodGroup = "";

                List<FixedValueFamilyRelation> listFamilyRelations = CommonManager.GetRelationList();
                List<HBloodGroup> listBloodGroup = CommonManager.GetAllBloodGroup();
                List<CitizenNationality> listNationality = CommonManager.GetCitizenshipNationaliyList();
                List<DocumentDocumentType> listDocumentType = CommonManager.GetDocumentDocumentTypeList();

                //if (BLL.BaseBiz.PayrollDataContext.HFamilies.Any(x => x.EmployeeId == 3))
                //{
                //    divWarningMsg.InnerHtml = "Data already imported.";
                //    divWarningMsg.Hide = false;
                //    return;
                //}

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        HFamily obj = new HFamily();
                        obj.EmployeeId = employeeId;

                        relationName = row["Relation"].ToString().Trim();
                        if (!string.IsNullOrEmpty(relationName))
                        {
                            FixedValueFamilyRelation objFamilyRelation = listFamilyRelations.SingleOrDefault(x => x.Name.ToLower() == relationName.ToLower());
                            if (objFamilyRelation == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid relation for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            obj.Relation = objFamilyRelation.Name;
                            obj.RelationID = objFamilyRelation.ID;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Relation is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        relativeName = row["Name"].ToString().Trim();
                        if (!string.IsNullOrEmpty(relativeName))
                            obj.Name = relativeName;
                        else
                        {
                            divWarningMsg.InnerHtml = "Name is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Date of Birth Eng"] != DBNull.Value && row["Date of Birth Eng"].ToString().Trim() != "")
                        {
                            

                            try
                            {
                                obj.DateOfBirthEng = Convert.ToDateTime(row["Date of Birth Eng"].ToString().Trim());

                                obj.DateOfBirth = BLL.BaseBiz.GetAppropriateDate(obj.DateOfBirthEng.Value);
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Invalid Date Of Birth in row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else if (row["Date Of Birth"] != DBNull.Value && row["Date Of Birth"].ToString().Trim() != "")
                        {
                            obj.DateOfBirth = row["Date Of Birth"].ToString().Trim();

                            try
                            {
                                obj.DateOfBirthEng = GetEngDate(obj.DateOfBirth);
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Invalid Date Of Birth in row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }

                        if (row["Dependent"] != DBNull.Value && row["Dependent"].ToString() != "")
                        {
                            if (row["Dependent"].ToString().ToLower() == "yes")
                                obj.HasDependent = true;
                            else
                                obj.HasDependent = false;
                        }

                        if (row["Occupation"] != DBNull.Value && row["Occupation"].ToString() != "")
                        {
                            HrFamilyOccupation objFamilyOcc = ListManager.GetHrFamilyOccupationByName(row["Occupation"].ToString().Trim());
                            if (objFamilyOcc != null)
                            {
                                obj.Occupation = objFamilyOcc.Occupation;
                                obj.OccupationId = objFamilyOcc.OccupationId;
                            }
                            else
                            {
                                divWarningMsg.InnerHtml = "Invalid Occupation in row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }

                        if (row["Contact Number"] != DBNull.Value && row["Contact Number"].ToString() != "")
                            obj.ContactNumber = row["Contact Number"].ToString();

                        //added by Namaraj on 2017/03/22
                        if (row["Document ID"] != DBNull.Value && row["Document ID"].ToString() != "")
                            obj.DocumentId = row["Document ID"].ToString();

                        if (row["Document Issue Place"] != DBNull.Value && row["Document Issue Place"].ToString() != "")
                            obj.DocumentIssuePlace = row["Document Issue Place"].ToString();

                        if (row["Document Issue Date"] != DBNull.Value && row["Document Issue Date"].ToString() != "")
                        {
                            obj.DocumentIssueDate = row["Document Issue Date"].ToString();

                            try
                            {
                                obj.DocumentIssueDateEng = GetEngDate(obj.DocumentIssueDate);
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Invalid document issue date in row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }

                        documentType = row["Document Type"].ToString().Trim();
                        if (!string.IsNullOrEmpty(documentType))
                        {
                            DocumentDocumentType objDocType = listDocumentType.SingleOrDefault(x => x.DocumentTypeName.ToLower() == documentType.ToLower());
                            if (objDocType == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid document type for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            obj.DocumentIdType = objDocType.DocumentTypeId;
                        }

                        nationality = row["Nationality"].ToString().Trim();
                        if (!string.IsNullOrEmpty(nationality))
                        {
                            CitizenNationality objnationality = listNationality.SingleOrDefault(x => x.Nationality.ToLower() == nationality.ToLower());
                            if (objnationality == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid nationality for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            obj.Nationality = objnationality.Nationality;
                        }


                        bloodGroup = row["Blood Group"].ToString().Trim();
                        if (!string.IsNullOrEmpty(bloodGroup))
                        {
                            HBloodGroup objBloodGroup = listBloodGroup.SingleOrDefault(x => x.BloodGroupName.ToLower() == bloodGroup.ToLower());
                            if (objBloodGroup == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid blood group for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            obj.BloodGroup = objBloodGroup.Id;
                        }

                        if (row["Gender"] != null)
                            obj.Gender = row["Gender"].ToString().ToLower() == "male" ? 1 : row["Gender"].ToString().ToLower() == "female" ? 0 : 2;

                        //if (row["Date Of Birth"] != DBNull.Value && row["Date Of Birth"].ToString() != "")
                        if(obj.DateOfBirthEng != null)
                        {
                            int years, months, days, hours, minutes, seconds, milliseconds;
                            NewHelper.GetElapsedTime(obj.DateOfBirthEng.Value,
                                BLL.BaseBiz.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                            obj.AgeOnSpecifiedSPDate= string.Format(" {0} years {1} months {2} days", years, months, days);
                        }

                        obj.Status = (int)HRStatusEnum.Approved;
                        obj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        obj.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();

                        list.Add(obj);

                    }

                    importedCount = list.Select(x => x.EmployeeId).Distinct().Count();

                    Status status = NewHRManager.ImportEmpFamilyMembers(list);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }



    }


}

