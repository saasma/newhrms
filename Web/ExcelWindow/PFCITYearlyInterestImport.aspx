<%@ Page Title="Import PF/CIT Yearly Interest" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true"
    CodeBehind="PFCITYearlyInterestImport.aspx.cs" Inherits="Web.CP.PFCITYearlyInterestImport" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        var skipLoadingCheck = true;
        function closePopup() {

            
            if(hasImport)
                window.opener.refreshWindow(); ;
        }

        window.onunload = closePopup;   
        
    </script>
   
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h3>Import PF/CIT Yearly Interest</h3>
    </div>
    <div class=" marginal" style='margin-top:0px'>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
      
        <div>
          <h2 class="templateHeader"> Step 1. Download our template file </h2>
          
            Start by downloading PF/CIT Yearly Interest excel template file. This file has the correct column headings
            that needs to import your allowance. 
            
            <div>
            <asp:LinkButton ID="btnExport"  CssClass=" excel marginRight" Style="float: left;margin-top:5px;margin-bottom:5px" runat="server" Text="Download Temp." OnClick="btnExport_Click" />
            </div>
            
         <h2 class="templateHeader">    Step 2. Copy your PF/CIT Yearly Interest details into the template</h2>
         
            Using Excel or another spreadsheet editor, fill
            the template with your allowance data. Make sure the allowance data matches
            the column headings provided in the template. 
            
           <p class='templateWarning'> <em> IMPORTANT: </em>Do not change the column
            headings in the template file. These need to be unchanged for the import to work
            in the next step. 
            </p>
            
          <h2 class="templateHeader">   Step 3. Import the updated template file </h2> Choose a file to import
            & press "Import" button. The file you import must be an excel file.            
            
        </div>

        <div style="margin-top:25px; color:Red;">
            <asp:Label runat="server" ID="txtSelectedYear"></asp:Label>
        </div>

        <div style='margin-top:10px;margin-bottom:10px;'>
        <asp:FileUpload ID="fupUpload" runat="server" />
        </div>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ControlToValidate="fupUpload"
            ValidationGroup="upload" ErrorMessage="File is required." runat="server" />
        <asp:Button ID="btnImport" runat="server" ValidationGroup="upload" Text="Import"
            CssClass="save" OnClick="btnImport_Click" OnClientClick="if(confirm('The PF/CIT Interests that has been already save will be updated.')==false) return false;"/>
        <asp:ValidationSummary ID="ValidationSummary1" ShowSummary="false" ValidationGroup="upload" ShowMessageBox="true"
            runat="server" />
    </div>
</asp:Content>
