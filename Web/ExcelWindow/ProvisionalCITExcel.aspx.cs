﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class ProvisionalCITExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/ProvisionalCIT.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            List<DAL.GetEmployeeListForProvisionalCITResult> list = TaxManager.GetListForPrivisionalCIT("", 0, 999999, ref total);

            ExcelGenerator.ExportProvisionalCIT(template, list);
        }


        

        //private List<int> GetEmployeeIdList()
        //{
        //    int total = 0;
        //    List<DAL.GetEmployeeListForProvisionalCITResult> list = TaxManager.GetListForPrivisionalCIT("", 0, 999999, ref total);
        //    List<int> ids = new List<int>();
        //    foreach (GetEmployeeListForProvisionalCITResult item in list)
        //    {
        //        ids.Add(item.EmployeeId);
        //    }
        //    return ids;
        //}

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");
                StringBuilder strMonthsAmounts = new StringBuilder("<root>");

                int employeeId;
                string citType = "";
                decimal privisionalAmount, regularCITAmountRate, currentMonthAddition, remainingProvision, externalCIT,rate;
                object value;
                object excludeValue;
                decimal oneTimeAdjustment = 0;

                int isAutoProvision = 0;

                //List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;

                int totalRows = 0;
                List<DAL.GetEmployeeListForProvisionalCITResult> list = TaxManager.GetListForPrivisionalCIT("", 0, 999999, ref totalRows);

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        GetEmployeeListForProvisionalCITResult item = new GetEmployeeListForProvisionalCITResult();

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);


                        //if (!list.Any(x => x.EmployeeId == employeeId))
                        //{
                        //    continue;
                        //}

                        item.EmployeeId = employeeId;

                        GetEmployeeListForProvisionalCITResult emp = list.FirstOrDefault(x => x.EmployeeId == employeeId);

                        if(emp == null)
                        {
                            divWarningMsg.InnerHtml = "EIN : " + employeeId + " of row " + rowNumber + " does not exists in the system.";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        citType = emp.RegularCITType;
                        //if (citType.ToLower() == "optimum")
                        //    continue;

                        item.RegularCITType = row["CIT Type"].ToString().Trim();
                        if (citType.ToLower() != "optimum" && item.RegularCITType.ToLower() == "optimum")
                        {
                            divWarningMsg.InnerHtml = "Optimum CIT can not be set for the employee " + row["Employee"].ToString()
                                + " from the import, please use Optimum CIT page " + " of row " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        isAutoProvision = 0;
                        string autoPrivision = row["Auto Provision"].ToString();
                        if (!string.IsNullOrEmpty(autoPrivision) && autoPrivision.ToLower().Trim() == "yes")
                        {
                            isAutoProvision = 1;
                        }

                        //value = row["Provisional CIT Amount"];
                        //privisionalAmount = 0;
                        //if (decimal.TryParse(value.ToString().Trim(), out privisionalAmount) == false)
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid Provisional amount for employee id : " + employeeId + " of row " + rowNumber;
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}

                        value = row["Fixed CIT Amount"];
                        regularCITAmountRate = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out regularCITAmountRate) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Regular CIT amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.RegularCITAmountRate = (double)regularCITAmountRate;

                        value = row["One Time Adjustment"];
                        oneTimeAdjustment = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out oneTimeAdjustment) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid One Time Adjustment amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.CurrentMonthAdditionalCIT = (decimal)oneTimeAdjustment;

                        value = row["Rate"];
                        rate = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out rate) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Regular CIT rate for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.CITRate = (double)rate;
                        
                        value = row["Sharwan"];
                        currentMonthAddition = 0;
                        if (value!=DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Sharwan amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month4 = currentMonthAddition;

                        value = row["Bhadra"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Bhadra amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month5 = currentMonthAddition;

                        value = row["Ashwin"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Ashwin amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month6 = currentMonthAddition;

                        value = row["Kartik"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Kartik amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month7 = currentMonthAddition;

                        value = row["Mangsir"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Mangsir amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month8 = currentMonthAddition;

                        value = row["Poush"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Poush amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month9 = currentMonthAddition;

                        value = row["Magh"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Magh amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month10 = currentMonthAddition;

                        value = row["Phalgun"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Phalgun amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month11 = currentMonthAddition;

                        value = row["Chaitra"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Chaitra amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month12 = currentMonthAddition;

                        value = row["Baisakh"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Baisakh amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month1 = currentMonthAddition;

                        value = row["Jestha"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Jestha amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month2 = currentMonthAddition;

                        value = row["Ashadh"];
                        currentMonthAddition = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out currentMonthAddition) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Ashadh amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Month3 = currentMonthAddition;
                        //value = row["Remaining Provision"];
                      
                        //if (decimal.TryParse(value.ToString().Trim(), out remainingProvision) == false)
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid remaining provision amount for employee id : " + employeeId + " of row " + rowNumber;
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}

                        value = row["External CIT"];
                        externalCIT = 0;
                        if (value != DBNull.Value && decimal.TryParse(value.ToString().Trim(), out externalCIT) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid External CIT amount for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.ExternalSettlementCIT = externalCIT;

                        importedCount += 1;

                        //string strExclude = "";

                        //if (excludeValue != null)
                        //    strExclude = excludeValue.ToString().ToLower();

                        xml.Append(
                            string.Format("<row EmployeeId=\"{0}\" ProvisionAmount=\"{1}\" RegularAmount=\"{2}\" Rate=\"{3}\" CurrentMonthAddition=\"{4}\" RemainingAmount=\"{5}\" AmountType=\"{6}\"  ExternalCIT=\"{7}\" IsAutoProvision=\"{8}\"  CurrentMonthAdditionalCIT=\"{9}\"  /> ",
                            employeeId, 0, regularCITAmountRate, rate, 0, 0, item.RegularCITType, externalCIT,isAutoProvision,oneTimeAdjustment)
                        );


                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 4, item.Month4, 4));
                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 5, item.Month5, 5));
                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 6, item.Month6, 6));
                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 7, item.Month7, 7));

                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 8, item.Month8, 8));
                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 9, item.Month9, 9));
                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 10, item.Month10, 10));
                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 11, item.Month11, 11));


                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 12, item.Month12, 12));
                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 1, item.Month1, 13));
                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 2, item.Month2, 14));
                        strMonthsAmounts.Append(string.Format("<row EmployeeId=\"{0}\" Month=\"{1}\" Amount=\"{2}\" Order=\"{3}\" /> ", employeeId, 3, item.Month3, 15));


                    }



                    xml.Append("</root>");
                    strMonthsAmounts.Append("</root>");

                    TaxManager.SaveForPrivisionalCIT(xml.ToString(), strMonthsAmounts.ToString());

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

