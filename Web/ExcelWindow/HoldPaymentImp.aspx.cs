﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class HoldPaymentImp : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

          
        }

       
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/HoldPaymentExcel.xlsx");

            ExcelGenerator.WriteHoldPaymentExcel(template);

        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xlsx");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xlsx"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<int> holdPaymentEmployeeIds = new List<int>();

                int employeeId;             

                List<int> empIDList = EmployeeManager.GetAllEmployeeID();
              
                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {                       

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!empIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "EIN is not valid for row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        EEmployee emp = EmployeeManager.GetEmployeeById(employeeId);
                        if (emp.Name.ToLower() != row["Name"].ToString().Trim().ToLower())
                        {
                            divWarningMsg.InnerHtml = "EIN and name does not match for row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        holdPaymentEmployeeIds.Add(employeeId);                     


                    }



                    InsertUpdateStatus status = new HoldPaymentManager().SaveEmployeesHoldPayment(holdPaymentEmployeeIds);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = "Hold payment saved for the employees successfully.";
                        divMsgCtl.Hide = false;

                        this.HasImport = true;  
                    }
                    else
                    {

                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                    }
                    

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
       
     
       
    }

   
}

