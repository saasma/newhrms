﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class BranchTranferHistoryImport : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            //if (SessionManager.User.URole.RoleId != (int)Role.Administrator)
            //{
            //    Response.Write("Not enough permisson.");
            //    Response.End();
            //    return;
            //}

        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/BranchDepartmentTransferImport.xlsx");

            string[] listEventType = CommonManager.GetServieEventTypes().Select(x => x.Name).ToArray();
            string[] listBranch = ListManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).Select(x => x.Name).ToArray();
            string[] listDepartment = DepartmentManager.GetAllDepartments().Select(x => x.Name).ToArray();

            string path = HttpContext.Current.Server.MapPath(template);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();
                    ExcelGenerator _objExcelGenerator = new ExcelGenerator();
                    ExcelGenerator.WriteIntoSheet(conn, "EventTypeSheet", listEventType);
                    ExcelGenerator.WriteIntoSheet(conn, "BranchSheet", listBranch);
                    ExcelGenerator.WriteIntoSheet(conn, "DepartmentSheet", listDepartment);
                }
            }

            catch (Exception ex)
            {
                divWarningMsg.InnerHtml = ex.ToString();
                divWarningMsg.Hide = false;
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(template));

        }

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID();
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<Branch> listBranch = ListManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).ToList();
                List<Department> listDepartment = DepartmentManager.GetAllDepartments().ToList();
                List<HR_Service_Event> listEventType = CommonManager.GetServieEventTypes().ToList();

                int employeeId;
                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                List<BranchDepartmentHistory> list = new List<BranchDepartmentHistory>();
                string branch, dep, eventType = "";

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;


                        if (string.IsNullOrEmpty(row["EIN"].ToString()))
                        {
                            divWarningMsg.InnerHtml = "EIN can't be empty for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }


                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;
                        BranchDepartmentHistory obj = new BranchDepartmentHistory();

                        obj.EmployeeId = employeeId;


                        //branch = row["From Branch"].ToString().Trim();
                        //if (!string.IsNullOrEmpty(branch))
                        //{
                        //    Branch objBranch = listBranch.SingleOrDefault(x => x.Name.ToLower() == branch.ToLower());
                        //    if (objBranch == null)
                        //    {
                        //        divWarningMsg.InnerHtml = "Invalid transfer from branch for row number " + rowNumber + ".";
                        //        divWarningMsg.Hide = false;
                        //        return;
                        //    }
                        //    obj.FromBranchId = objBranch.BranchId;
                        //}

                        branch = row["To Branch"].ToString().Trim();
                        if (!string.IsNullOrEmpty(branch))
                        {
                            Branch objtoBranch = listBranch.SingleOrDefault(x => x.Name.ToLower() == branch.ToLower());
                            if (objtoBranch == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid transferred branch for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            obj.BranchId = objtoBranch.BranchId;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "To Branch is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }


                        //dep = row["From Department"].ToString().Trim();
                        //if (!string.IsNullOrEmpty(dep))
                        //{
                        //    Department objDepartment = listDepartment.SingleOrDefault(x => x.Name.ToLower() == dep.ToLower());
                        //    if (objDepartment == null)
                        //    {
                        //        divWarningMsg.InnerHtml = "Invalid transfer from department for row number " + rowNumber + ".";
                        //        divWarningMsg.Hide = false;
                        //        return;
                        //    }
                        //    obj.FromDepartmentId = objDepartment.DepartmentId;
                        //}

                        dep = row["To Department"].ToString().Trim();
                        if (!string.IsNullOrEmpty(dep))
                        {
                            Department objtoDepartment = listDepartment.SingleOrDefault(x => x.Name.ToLower() == dep.ToLower());
                            if (objtoDepartment == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid transferred department for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            obj.DepeartmentId = objtoDepartment.DepartmentId;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "To Department is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        eventType = row["Event Type"].ToString().Trim();
                        if (!string.IsNullOrEmpty(eventType))
                        {
                            HR_Service_Event objHR_Service_Event = listEventType.SingleOrDefault(x => x.Name.ToLower() == eventType.ToLower());
                            if (objHR_Service_Event == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid event type for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            obj.EventID = objHR_Service_Event.EventID;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Event Type can't be empty for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }


                       
                        if (!string.IsNullOrEmpty(row["Tranfer Date"].ToString())) // && string.IsNullOrEmpty(row["Tranfer Date Eng"].ToString()))
                        {
                            obj.FromDateEng = BLL.BaseBiz.GetEngDate(row["Tranfer Date"].ToString(), IsEnglish);
                            obj.FromDate = BLL.BaseBiz.GetAppropriateDate(obj.FromDateEng.Value);
                        }
                        else if (!string.IsNullOrEmpty(row["Tranfer Date Eng"].ToString()))
                        {
                            obj.FromDateEng = DateTime.Parse(row["Tranfer Date Eng"].ToString());
                            obj.FromDate = BLL.BaseBiz.GetAppropriateDate(DateTime.Parse(row["Tranfer Date Eng"].ToString()));
                        }


                        if (obj.FromDateEng == null || obj.FromDateEng.Value == DateTime.MinValue)
                        {
                            divWarningMsg.InnerHtml = "Invalid transfer date or transfer date not supplied for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        obj.LetterDate = obj.FromDate;
                        obj.LetterDateEng = obj.FromDateEng;


                        
                        if (!string.IsNullOrEmpty(row["Departure Date"].ToString()))// && string.IsNullOrEmpty(row["Departure Date Eng"].ToString()))
                        {
                            obj.DepartureDateEng = BLL.BaseBiz.GetEngDate(row["Departure Date"].ToString(), IsEnglish);
                            obj.DepartureDate = BLL.BaseBiz.GetAppropriateDate(obj.DepartureDateEng.Value);
                        }
                        else if (!string.IsNullOrEmpty(row["Departure Date Eng"].ToString()))
                        {
                            obj.DepartureDateEng = DateTime.Parse(row["Departure Date Eng"].ToString());
                            obj.DepartureDate = BLL.BaseBiz.GetAppropriateDate(DateTime.Parse(row["Departure Date Eng"].ToString()));
                        }


                        if (!string.IsNullOrEmpty(row["Letter No"].ToString()))
                            obj.LetterNumber = row["Letter No"].ToString();

                        if (!string.IsNullOrEmpty(row["Note"].ToString()))
                            obj.Note = row["Note"].ToString();

                        if (!string.IsNullOrEmpty(row["Special Responsibility"].ToString()))
                            obj.SpecialResponsibility = row["Special Responsibility"].ToString();

                        //bool IsDepartmentTrans = true;
                        //if (!string.IsNullOrEmpty(row["IsBranchTransfer"].ToString()))
                        //{
                        //    IsDepartmentTrans = row["IsBranchTransfer"].ToString().ToLower() == "no" ? true : false;
                        //}

                        obj.IsDepartmentTransfer = false;//IsDepartmentTrans;

                        if(obj.IsDepartmentTransfer.Value && obj.DepeartmentId==null)
                        {
                            divWarningMsg.InnerHtml = "Required department details for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if(!obj.IsDepartmentTransfer.Value && obj.BranchId==null)
                        {
                            divWarningMsg.InnerHtml = "Required branch details for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }


                        list.Add(obj);
                    }

                    importedCount = list.Select(x => x.EmployeeId).Distinct().Count();
                    Status status = NewHRManager.ImportBranchOrDepartmentTransfer(list);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml =string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }



    }


}

