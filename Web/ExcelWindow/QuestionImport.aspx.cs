﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;

namespace Web.CP
{
    public partial class QuestionImport : BasePage
    {
        public int CategoryID { get; set; }
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            CategoryID = int.Parse(Request.QueryString["id"]);

            int formid = AppraisalManager.GetFormIDByQuestionnaireCategoryID(CategoryID);
            if(formid != 0)
            {
                if (NewHRManager.IsAppraisalQuestionnariesUsedInEmployee(formid))
                {
                    divWarningMsg.InnerHtml = "This appraisal form already been submitted, can not change.";
                    divWarningMsg.Hide = false;

                    btnImport.Visible = false;
                    //SetWarning(lblMsg, "You can't add and delete Review as this data has been used in employee from.");
                }
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/AppraisalQuestion.xlsx");        
                 
            //int total=0;
            //// Retrieve data from SQL Server table.
            //List<GetUserListResult> list = UserManager.GetUserList("", "", true, 0, 2000, ref total, true)
            //    .OrderBy(x => x.EmployeeId).ToList();

            List<AppraisalFormQuestionnaire> list = AppraisalManager.GetCategoryQuestionList(CategoryID);
            ExcelGenerator.WriteQuestionExportExcel(template, list);
            
        }




        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            // Retrieve data from SQL Server table.
            List<int> ids = EmployeeManager.GetAllEmployeeID();
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

               
                int employeeId;
                //decimal? grossAmount,pf,cit,insurance,sst,tds;


                string question;
                List<string> list = new List<string>();
                int importedCount = 0;

                string date, note;
                decimal? amount;
                decimal? points, rate;


                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        

                        rowNumber += 1;



                        question = row["Question"].ToString();
                        if (row["Question"] == null || string.IsNullOrEmpty(question))
                        {
                            continue;
                        }







                        list.Add(question);

                       

                      

                    }



                    int imported = list.Count;
                    string msg;
                    AppraisalManager.UpdateImportedQuestion(CategoryID, list);



                    divMsgCtl.InnerHtml = imported + " questions imported.";
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

