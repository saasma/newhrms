﻿<%@ Page Title="Designation Import" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="DesignationImport.aspx.cs" Inherits="Web.ExcelWindow.DesignationImport" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        var skipLoadingCheck = true;
        function closePopup() {


            if (hasImport)
                window.opener.refreshWindow(); ;
        }


        window.onunload = closePopup;   
        
        
    </script>
   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h3>
          Designation History Import</h3>
    </div>
    <div class=" marginal" style='margin-top:0px'>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
      
        <div>
          <h2 class="templateHeader"> Step 1. Download our template file </h2>
          
            Start by downloading tax excel template file. This file has the correct column headings
            that needs to import your designation history. 
            
            <div>
            <asp:LinkButton ID="btnExport"  CssClass=" excel marginRight" Style="float: left;margin-top:5px;margin-bottom:5px" runat="server" Text="Download Temp." OnClick="btnExport_Click" />
            </div>
            
         <h2 class="templateHeader">    Step 2. Copy your designation history details into the template</h2>
         
            Using Excel or another spreadsheet editor, fill
            the template with your designation history data. Make sure the designation history data matches
            the column headings provided in the template. 
            
           <p class='templateWarning'> <em> IMPORTANT: </em>Date should be nepali in the format("yyyy/mm/dd")
            </p>
            
          <h2 class="templateHeader">   Step 3. Import the updated template file </h2> Choose a file to import
            & press "Import" button. The file you import must be an excel file.            
            
        </div>
        <br />
        <table>
            <tr>
                <td style="width:70px;">Event Type</td>
                <td><asp:DropDownList ID="ddlEventType" Width="130px" DataTextField="Name" DataValueField="EventID"
                    AppendDataBoundItems="true" runat="server">
                    <asp:ListItem Text="Select" Value="-1" Selected="True" />
                </asp:DropDownList></td>
            </tr>
        </table>
        
        <asp:RequiredFieldValidator ID="rfvEventType" Display="None" ControlToValidate="ddlEventType" InitialValue="-1"
            ValidationGroup="upload" ErrorMessage="Event type is required." runat="server" />
        <div style='margin-top:10px;margin-bottom:10px;'>
        <asp:FileUpload ID="fupUpload" runat="server" />
        </div>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ControlToValidate="fupUpload"
            ValidationGroup="upload" ErrorMessage="File is required." runat="server" />
        <asp:Button ID="btnImport" runat="server" ValidationGroup="upload" Text="Import"
            CssClass="save" OnClick="btnImport_Click" />
        <asp:ValidationSummary ID="ValidationSummary1" ShowSummary="false" ValidationGroup="upload" ShowMessageBox="true"
            runat="server" />
    </div>
</asp:Content>