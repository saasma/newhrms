﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.ExcelWindow
{
    public partial class AssignEmployeeShiftExcel : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/ShiftAssign.xlsx");

            ExcelGenerator.ExportEmployeeAssignedShift(template);

        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


             
            
            
          


               List<EWorkShiftHistory> list = new List<EWorkShiftHistory>();

                int employeeId;
                //object value;

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);
                        // EWorkShiftHistory workShift = new EWorkShiftHistory();
                        EWorkShiftHistory workShift = new EWorkShiftHistory();

                        if (employeeId == 0)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        workShift.EmployeeId = employeeId;
                        int ID = 0;
                        int.TryParse(row["ID"].ToString(), out ID);
                        workShift.WorkShiftHistoryId = ID;

                        if (string.IsNullOrEmpty(row["Shift"].ToString()))
                        {

                            divWarningMsg.InnerHtml = "Shift in required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                        {


                            EWorkShift shiftName = ShiftManager.GetShiftByName(row["Shift"].ToString().ToLower());
                            if (shiftName == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid Shift for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            else
                                workShift.WorkShiftId = shiftName.WorkShiftId;
                        }


                        if (string.IsNullOrEmpty(row["From"].ToString()))
                        {
                            divWarningMsg.InnerHtml = "From Date in required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                        {
                            if (!IsDateTime(row["From"].ToString()))
                            {
                                divWarningMsg.InnerHtml = "Invalid Date for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            else
                                workShift.FromDateEng = DateTime.Parse(row["From"].ToString());
                        }

                        AttendanceInOutTime time = ShiftManager.GetShiftTime(workShift.WorkShiftId.Value, workShift.FromDateEng.Value);
                        EWorkShift shift = ShiftManager.GetShiftById(workShift.WorkShiftId.Value);

                        if (time == null)
                        {
                            //NewMessage.ShowWarningMessage("Shift time not defined.");
                            //return;
                            divWarningMsg.InnerHtml = "Shift time not defined. " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }



                        workShift.FromDateEngWithShiftTime = new DateTime(
                            workShift.FromDateEng.Value.Year, workShift.FromDateEng.Value.Month, workShift.FromDateEng.Value.Day,
                            time.OfficeInTime.Value.Hours, time.OfficeInTime.Value.Minutes, 0);


                        //if (chkHasToDate.Checked == false)


                        if (!string.IsNullOrEmpty(row["To"].ToString()))
                        {
                            if (!IsDateTime(row["To"].ToString()))
                            {
                                divWarningMsg.InnerHtml = "Invalid To Date for row number. " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            workShift.ToDateEng = DateTime.Parse(row["To"].ToString());
                            workShift.FromDate = BLL.BaseBiz.GetAppropriateDate(workShift.FromDateEng.Value);
                            workShift.ToDate = BLL.BaseBiz.GetAppropriateDate(workShift.ToDateEng.Value);

                            int hour, min;

                            // for overnight set mid night as date could not touch other day
                            if (shift.Type == 2)
                            {
                                hour = 23;
                                min = 59;
                            }
                            else
                            {
                                hour = time.OfficeOutTime.Value.Hours;
                                min = time.OfficeOutTime.Value.Minutes;
                            }

                            workShift.ToDateEngWithShiftTime = new DateTime(
                                workShift.ToDateEng.Value.Year, workShift.ToDateEng.Value.Month, workShift.ToDateEng.Value.Day,
                                hour, min, 0);
                        }

                        importedCount += 1;
                        list.Add(workShift);
                    }

                    //save as in list
                    Status status = ShiftManager.InsertUpdateShiftImport(list);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = string.Format("shift assigned for {0} employees.", importedCount);
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                    else
                    {
                        divMsgCtl.InnerHtml = string.Format("fail for import data {0}.", importedCount);
                        divMsgCtl.Hide = false;
                        this.HasImport = false;
                    }

                }

                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
        public static bool IsDateTime(string txtDate)
        {
            DateTime tempDate;

            return DateTime.TryParse(txtDate, out tempDate) ? true : false;
        }
    }

}