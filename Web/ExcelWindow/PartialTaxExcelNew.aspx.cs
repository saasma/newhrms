﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;

namespace Web.CP
{
    public partial class PartialTaxExcelNew : BasePage
    {
        int payrollPerodId = 0;
        int addOnId = 0;
      //  KeyValue keyValue = null;
        int ein = -1;
        int unitId = -1;
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string pp = Request.QueryString["payrollPeriod"];
            if (pp.Contains(":"))
                payrollPerodId = int.Parse(pp.Substring(0, pp.IndexOf(":")));
            else

                payrollPerodId = int.Parse(pp);

            if (!string.IsNullOrEmpty(Request.QueryString["ein"]))
            {
                ein = int.Parse(Request.QueryString["ein"]);
            }

            if (!string.IsNullOrEmpty(Request.QueryString["unitid"]))
            {
                unitId = int.Parse(Request.QueryString["unitid"]);
            }

            addOnId = int.Parse(Request.QueryString["id"]);

            if (!IsPostBack)
                title.InnerHtml += " " + PayManager.GetAddOn(addOnId).Name;

            //List<KeyValue> incDedList = PayManager.GetEmployeesIncomeDeductionList(payrollPerodId);

            


            //foreach (KeyValue value in incDedList)
            //{
            //    if (value.Value == (sourceValueId + ":" + typeId))
            //    {
            //        keyValue = value;
            //        break;
            //    }
            //}
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/PartialTax.xlsx");

           
         
            int? totalRecords = 0;

            List<CalcGetHeaderListResult> headerList = CalculationManager.GetAddOnHeaderList(addOnId, true);

            List<CalcGetCalculationListResult> employeeList = CalculationManager.GetAddOnEmployeeList(SessionManager.CurrentCompanyId,
                payrollPerodId,addOnId,0,99999,ref totalRecords,ein,"",true,false,null,-1,-1, unitId);


            List<AddOnDetail> amountList = PartialAddOnManager.GetNewAddOnDetail(addOnId);



            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteNewAddOnIncomeForExportExcel(template, headerList, employeeList, amountList);

        }


        

        //private List<int> GetEmployeeIdList()
        //{
        //    int? totalRecords = 0;
        //    List<CalcGetCalculationListResult> employeeList = CalculationManager.GetAddOnEmployeeList(SessionManager.CurrentCompanyId,
        //        payrollPerodId, addOnId, 0, 99999, ref totalRecords, -1,"",true);

        //    List<int> ids = new List<int>();
        //    foreach (CalcGetCalculationListResult item in employeeList)
        //    {
        //        ids.Add(item.EmployeeId.Value);
        //    }
        //    return ids;
        //}

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");
            // do not validate as in ashadh we may need to give add on import after saving salary also
            //if (CalculationManager.IsCalculationSaved(payrollPerodId) != null)
            //{
            //    divWarningMsg.InnerHtml = "Salary already saved for this period.";
            //    divWarningMsg.Hide = false;
            //    return;
            //}

            //if (CommonManager.GetLastPayrollPeriod().PayrollPeriodId != payrollPerodId)
            //{
            //    divWarningMsg.InnerHtml = "Add on can be added for last payroll period only.";
            //    divWarningMsg.Hide = false;
            //    return;
            //}

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


                int employeeId;
                float? value;
                //float? Days_At_Palati, Days_At_Kirne, On_Call, Shift_Afternoon, Shift_Night, Split_Shift_Morning, Split_Shift_Evening, OT_150, OT_200, OT_300, Public_Holiday,
                //    Other_Leave, Snacks_Allowance, Special_Compensation_Allowance, Meal_Allowance, TADA, Vehicle_Allowance;

                //List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                int totalMismatchEmployeeCount = 0;


                try
                {
                    List<AddOnDetail> amountList = new List<AddOnDetail>();
                    List<CalcGetHeaderListResult> headerList = CalculationManager.GetAddOnHeaderList(addOnId, true);



                    string name = "";
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false
                             && (row["Employee"].Equals(DBNull.Value) || string.IsNullOrEmpty(row["Employee"].ToString()))
                            )
                        {
                            continue;
                        }

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        name = row["Employee"].ToString();



                        importedCount += 1;

                        foreach (var header in headerList)
                        {
                            value = ExcelGenerator.GetValueFromCell(header.HeaderName, row, employeeId);
                            if (value == null)
                                value = (float)0;
                            //if (value != null)
                            {

                                AddOnDetail projectPay = new AddOnDetail();
                                projectPay.EmployeeId = employeeId;
                                projectPay.AddOnHeader = new AddOnHeader();
                                projectPay.AddOnHeader.Type = header.Type.Value;
                                projectPay.AddOnHeader.SourceId = header.SourceId.Value;

                                projectPay.Amount = (decimal)value;



                                //if (value.Value < 0 && isDeduction == true)
                                //{
                                //    divWarningMsg.InnerHtml = string.Format("Employee {0} contains negative value, so please correct it & import the excel.", name);
                                //    divWarningMsg.Hide = false;
                                //    return;
                                //}

                                amountList.Add(projectPay);
                            }
                        }



                    }

                    int count = 0;
                    //ResponseStatus status = PayManager.SaveVariableIncome(amountList, isDeduction, ref count);


                    string message = "";
                    int result =  PayManager.ImportAddOnList(amountList,addOnId, payrollPerodId,headerList,ref message);

                    if (result == 0 && !string.IsNullOrEmpty(message))
                    {
                        divWarningMsg.InnerHtml = message;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    count = rowNumber;
                    //if (status.IsSuccessType == false)
                    //{

                    //    divWarningMsg.InnerHtml = status.ErrorMessage;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}


                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                                                            importedCount);
                    divMsgCtl.Hide = false;


                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
     
       
    }

   
}

