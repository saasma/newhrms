﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class TargetAchievementExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            targetId = int.Parse(Request.QueryString["target"]);
            empId = int.Parse(Request.QueryString["empid"]);
            year = int.Parse(Request.QueryString["year"]);
        }

        int targetId = -1;
        int empId = -1;
        int year = 0;

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/TargetAchievment.xlsx");        
                 
            List<TargetAchievmentBO> targetList = AppraisalManager.GetEmpTargetAchievments
             (0, 9999, empId, targetId, year).ToList();



            ExcelGenerator.ExportTargetAchievement(template, targetList);
        }


        

        //private List<int> GetEmployeeIdList()
        //{
        //    int total = 0;
        //    List<DAL.GetEmployeeListForProvisionalCITResult> list = TaxManager.GetListForPrivisionalCIT("", 0, 999999, ref total);
        //    List<int> ids = new List<int>();
        //    foreach (GetEmployeeListForProvisionalCITResult item in list)
        //    {
        //        ids.Add(item.EmployeeId);
        //    }
        //    return ids;
        //}

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");
                StringBuilder strMonthsAmounts = new StringBuilder("<root>");

                int employeeId;
                string citType = "";
              
                decimal oneTimeAdjustment = 0;

                int isAutoProvision = 0;
                string target = "";
                //List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                object value;
                double input=0;
                int totalRows = 0;
                List<TargetAchievmentBO> targetList = new List<TargetAchievmentBO>();
                List<AppraisalTarget> targets = AppraisalManager.GetAllTargets();
                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        TargetAchievmentBO item = new TargetAchievmentBO();

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);


                        //if (!list.Any(x => x.EmployeeId == employeeId))
                        //{
                        //    continue;
                        //}

                        item.EIN = employeeId;




                        target = row["Target"].ToString();
                        if (targets.Any(x => x.Name.ToLower() == target.ToLower().Trim()) == false)
                        {
                             divWarningMsg.InnerHtml = "Invalid Target for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.TargetID = targets.FirstOrDefault(x => x.Name.ToLower() == target.ToLower().Trim()).TargetID;


                        value = row["Baisakh Target"];
                        input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Baisakh target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target1 = input;

                        value = row["Baisakh Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Baisakh achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment1 = input;

                        value = row["Jestha Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Jestha target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target2 = input;

                        value = row["Jestha Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Jestha achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment2 = input;

                        value = row["Ashadh Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid ashadh target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target3 = input;

                        value = row["Ashadh Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid ashadh achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment3 = input;

                        value = row["Sharwan Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid sharwan target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target4 = input;

                        value = row["Sharwan Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid sharwan achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment4 = input;

                        value = row["Bhadra Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid bhadra target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target5 = input;

                        value = row["Bhadra Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid bhadra achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment5 = input;

                        value = row["Ashwin Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid ashwin target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target6 = input;

                        value = row["Ashwin Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid ashwin achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment6 = input;

                        value = row["Kartik Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid kartik target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target7 = input;

                        value = row["Kartik Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid kartik achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment7 = input;

                        value = row["Mangsir Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid mangsir target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target8 = input;

                        value = row["Mangsir Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid mangsir achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment8 = input;

                        value = row["Poush Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid poush target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target9 = input;

                        value = row["Poush Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid poush achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment9 = input;

                        value = row["Magh Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid magh target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target10 = input;

                        value = row["Magh Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid magh achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment10 = input;

                        value = row["Falgun Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid falgun target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target11 = input;

                        value = row["Falgun Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid falgun achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment11 = input;

                        value = row["Chaitra Target"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid chaitra target value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Target12 = input;

                        value = row["Chaitra Achievement"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid chaitra achievement value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Achievment12 = input;



                        importedCount += 1;

                        targetList.Add(item);

                    }





                    AppraisalManager.UpdateTargetAchievement(year, targetList);

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

