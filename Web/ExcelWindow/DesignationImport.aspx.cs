﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.ExcelWindow
{
    public partial class DesignationImport : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlEventType.DataSource = CommonManager.GetServieEventTypes();
                ddlEventType.DataBind();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/DesignationHistImp.xls");

            ExcelGenerator.ExportDesignationHist(template, "DesignationHistImp.xls");
        }




        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            // Retrieve data from SQL Server table.
            List<int> ids = EmployeeManager.GetAllEmployeeID();
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


                int employeeId;

                int eventType = -1;

                if (ddlEventType.SelectedValue != null && ddlEventType.SelectedValue != "-1")
                    eventType = int.Parse(ddlEventType.SelectedValue);

                List<int> employeeIDList = GetEmployeeIdList();
                int importedCount = 0;

                List<DesignationHistory> list = new List<DesignationHistory>();
                List<EDesignation> listDesignations = NewHRManager.GetDesignations();

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        DesignationHistory obj = new DesignationHistory();

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber ;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        obj.EmployeeId = employeeId;

                        if (row["Old Designation"] != DBNull.Value && row["Old Designation"].ToString() != "")
                        {
                            try
                            {
                                obj.FromDesignationId = listDesignations.FirstOrDefault(x => (x.LevelName.ToLower() == row["Old Designation"].ToString().ToLower())).DesignationId;
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Please select proper old designation for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Old designation is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["New Designation"] != DBNull.Value && row["New Designation"].ToString() != "")
                        {
                            try
                            {
                                obj.DesignationId = listDesignations.FirstOrDefault(x => (x.LevelName.ToLower() == row["New Designation"].ToString().ToLower())).DesignationId;
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Please select proper new designation for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "New designation is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Nepali Date"] != DBNull.Value && row["Nepali Date"].ToString() != "")
                        {
                            try
                            {
                                obj.FromDate = row["Nepali Date"].ToString();
                                obj.FromDateEng = GetEngDate(obj.FromDate);
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Proper nepali date is required for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {

                            string engDate = row["English Date"].ToString();
                            DateTime date;
                            if (DateTime.TryParse(engDate, out date) == false)
                            {
                                divWarningMsg.InnerHtml = "Nepali or English date is required for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            obj.FromDateEng = date;
                            obj.FromDate = BLL.BaseBiz.GetAppropriateDate(obj.FromDateEng.Value);
                            
                        }

                        obj.EventID = eventType;
                        obj.CreatedBy = SessionManager.CurrentLoggedInUserID;
                        obj.ModifiedBy = obj.CreatedBy;
                        obj.CreatedOn = DateTime.Now;
                        obj.ModifiedOn = obj.CreatedOn;
                        obj.IsFirst = false;

                        list.Add(obj);
                        importedCount++;
                    }

                    Status status = NewHRManager.SaveDesignationHistoryImport(list);

                    if (!status.IsSuccess)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}