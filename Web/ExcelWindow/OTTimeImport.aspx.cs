﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using BLL;
using System.Data;
using System.IO;
using BLL.Manager;
using DAL;
using BLL.Entity;
using System.Data.OleDb;
using BLL.BO;

namespace Web.ExcelWindow
{
    public partial class OTTimeImport : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/OTTimeImport.xlsx");

            ExcelGenerator.ExportOTTimeAttendance(template, "OTTimeImport" + ".xlsx");

        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


           
            List<TimeRequestLine> list = new List<TimeRequestLine>();

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;

                    EEmployee objEmp = new EEmployee();

                    foreach (DataRow row in datatable.Rows)
                    {
                        TimeRequestLine objTimeRequestLine = new TimeRequestLine(); 

                        rowNumber += 1;

                         if (row["EIN"] == DBNull.Value || row["EIN"].ToString() == "")
                         {
                             continue;
                         }

                        if (row["EIN"] != DBNull.Value && row["EIN"].ToString() != "")
                        {
                            try
                            {
                                objTimeRequestLine.EmpId = int.Parse(row["EIN"].ToString());
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Please select proper employee for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Employee is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        DateTime date = new DateTime();

                        if (row["Is Overnight"] != DBNull.Value && row["Is Overnight"].ToString() != "")
                        {

                            objTimeRequestLine.OvernightShift = row["Is Overnight"].ToString().Trim().ToLower() == "yes" ? true : false;
                           
                        }

                        if (row["Date"] != DBNull.Value && row["Date"].ToString() != "")
                        {
                            try
                            {
                                date = Convert.ToDateTime(row["Date"].ToString());
                                objTimeRequestLine.DateEng = date;
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Date is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Date is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["In Time"] != DBNull.Value && row["In Time"].ToString() != "")
                        {
                            try
                            {
                                DateTime dtStart = DateTime.Parse(row["In Time"].ToString());
                                int hoursStart = dtStart.Hour;
                                int minutesStart = dtStart.Minute;
                                objTimeRequestLine.InTime = new TimeSpan(hoursStart, minutesStart, 0);
                                objTimeRequestLine.InTimeDT = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "In Time is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "In Time required or invalid for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Out Time"] != DBNull.Value && row["Out Time"].ToString() != "")
                        {
                            try
                            {
                                DateTime dtEnd = DateTime.Parse(row["Out Time"].ToString());
                                int hoursStart = dtEnd.Hour;
                                int minutesStart = dtEnd.Minute;
                                objTimeRequestLine.OutTime = new TimeSpan(hoursStart, minutesStart, 0);
                                objTimeRequestLine.OutTimeDT = new DateTime(date.Year, date.Month, date.Day, hoursStart, minutesStart, 0);
                            }
                            catch
                            {

                                divWarningMsg.InnerHtml = "Out Time is invalid for the row : " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Out Time required or invalid for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (objTimeRequestLine.InTime == null || objTimeRequestLine.OutTime == null)
                        {
                            divWarningMsg.InnerHtml = "In Time and Out Time is required for the row : " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;

                        }

                        if (objTimeRequestLine.InTime.Value > objTimeRequestLine.OutTime.Value && 
                            (objTimeRequestLine.OvernightShift == false || objTimeRequestLine.OvernightShift == null))
                        {
                            divWarningMsg.InnerHtml = "In time is greater than out time for the row : " + rowNumber + " , if it is overnight shift then mark the column to import.";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["Remarks"] != DBNull.Value && row["Remarks"].ToString() != "")
                        {
                            objTimeRequestLine.InNote = row["Remarks"].ToString();
                        }
                                                
                        objTimeRequestLine.SN = importedCount;
                        list.Add(objTimeRequestLine);
                    }



                    if (list.Count > 0)
                    {

                        Status status = AttendanceManager.ImportOTTime(list);
                        if (status.IsSuccess)
                        {
                            divMsgCtl.InnerHtml = "OT Time attendance imported successfully.";
                            divMsgCtl.Hide = false;
                        }

                        else
                        {
                            divWarningMsg.InnerHtml = status.ErrorMessage;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = "Error while importing time attendance.";
                        divWarningMsg.Hide = false;
                    }


                }

                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}