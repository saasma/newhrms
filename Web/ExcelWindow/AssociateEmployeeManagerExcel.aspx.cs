﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class AssociateEmployeeManagerExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        int departmentId = -1;

        protected void Page_Load(object sender, EventArgs e)
        {

            
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmployeeReportTo.xlsx");        
                 
            int total=0;

            //string[] GetProjectList =
            //    LeaveRequestManager.GetProjectList("", 1, 99999, ref total).OrderBy(x=>x.Name)
            //    .Select(a => a.Name).ToArray();

            List<GetLeaveProjectEmployeesResult> list =
               LeaveRequestManager.GetEmployeeProjectList(
              "", 1, 9999, ref total, departmentId,-1,-1);


            List<EEmployee> empList = EmployeeManager.GetAllEmployeesForEmployeeImportExport().OrderBy(x => x.Name).ToList();
            foreach (var val1 in empList)
            {
                val1.FirstName = val1.EmployeeId + " - " + val1.Name;
                val1.LastName = "";
                if (val1.EHumanResources.FirstOrDefault() != null)
                {
                    if (val1.EHumanResources.FirstOrDefault().ReportToEmployeeId != null)
                    {
                        val1.LastName = val1.EHumanResources.FirstOrDefault().ReportToEmployeeId + " - "+ EmployeeManager.GetEmployeeById(val1.EHumanResources.FirstOrDefault().ReportToEmployeeId.Value).Name;
                    }
                }
            }

            string[] GetProjectList = empList.Select(x => x.FirstName).ToArray();



            ExcelGenerator.WriteEmployeeManagersExcel(template, GetProjectList, empList);
        }




       

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            int total = 0;
            List<GetLeaveProjectsResult> projectList =
                LeaveRequestManager.GetProjectList("", 1, 99999, ref total).ToList();



            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

               
                int employeeId;
                int managerID;
                string managerName;
                string projectName;

                List<GetLeaveProjectEmployeesResult> list = new List<GetLeaveProjectEmployeesResult>();

                List<EHumanResource> empList = new List<EHumanResource>();
                int importedCount = 0;
                EHumanResource emp = new EHumanResource();



                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {


                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        emp = new EHumanResource();
                        emp.EmployeeId = employeeId;

                        if (!string.IsNullOrEmpty(row["Manager"].ToString()))
                        {

                            managerName = row["Manager"].ToString().Trim().Split('-')[0];

                            if (int.TryParse(managerName, out managerID) == false)
                            {
                                divWarningMsg.InnerHtml = "Invalid Manager in row Manager Column " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            emp.ReportToEmployeeId = managerID;


                            EEmployee managerEmp = EmployeeManager.GetEmployeeById(managerID);

                            if (managerEmp == null)
                            {
                                divWarningMsg.InnerHtml = string.Format("The manager you have selected {0} does not exist", row["Manager"].ToString());
                                divWarningMsg.Hide = false;
                                return;
                            }




                            empList.Add(emp);

                            importedCount += 1;
                        }

                        

                      

                    }



                    int imported = 0;
                    string msg;
                    EmployeeManager.UpdateReportsInBulk(empList);

                    //if (!string.IsNullOrEmpty(msg))
                    //{
                    //    divWarningMsg.InnerHtml = msg;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    divMsgCtl.InnerHtml = string.Format("Employee Manager Imported", importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

