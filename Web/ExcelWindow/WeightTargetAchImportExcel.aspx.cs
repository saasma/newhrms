﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class WeightTargetAchImportExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            targetId = int.Parse(Request.QueryString["targetid"]);
            empId = int.Parse(Request.QueryString["empid"]);
            formid = int.Parse(Request.QueryString["formid"]);
        }

        int targetId = -1;
        int empId = -1;
        int formid = 0;

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/WeightTargetAchievements.xlsx");

            List<TargetOtherDetail> targetList = AppraisalManager.GetTargetOtherDetails(empId, formid, targetId);

            ExcelGenerator.ExportWeightTargetAchievement(template, targetList);
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xlsx");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xlsx"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<AppraisalEmployeeTargetOtherDetail> listAppraisalEmployeeTargetOtherDetail = new List<AppraisalEmployeeTargetOtherDetail>();


                int employeeId;             

                int importedCount = 0;
         
                double weight =0;
                int totalRows = 0;
                object value;
                double input = 0;
              
                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        AppraisalEmployeeTargetOtherDetail item = new AppraisalEmployeeTargetOtherDetail();

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        item.EmployeeId = employeeId;
                        item.FormRef_ID = formid;
                        item.TargetRef_ID = targetId;                       


                        value = row["Weight"]; input = 0;
                        if (value != DBNull.Value && double.TryParse(value.ToString().Trim(), out input) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid weight value for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Weight = input;

                        value = row["Target Assigned"];
                        if (value == DBNull.Value || value.ToString() == "")
                        {
                            divWarningMsg.InnerHtml = "Target Assigned is required for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.AssignedTargetTextValue = row["Target Assigned"].ToString();

                        value = row["Achievement"];
                        if (value == DBNull.Value || value.ToString() == "")
                        {
                            divWarningMsg.InnerHtml = "Achievement is required for employee id : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.AchievementTextValue = row["Achievement"].ToString();

                        importedCount += 1;

                        listAppraisalEmployeeTargetOtherDetail.Add(item);

                    }


                    //SaveUpdateWeightsTargetsAchievements
                    AppraisalManager.SaveUpdateWeightsTargetsAchievements(listAppraisalEmployeeTargetOtherDetail);

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

