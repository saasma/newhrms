﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using System.Data;
using System.Data.OleDb;
using System.IO;
using BLL.BO;

namespace Web.CP
{
    public partial class TravelAllowanceExcel : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                {
                    return false;
                }
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public int allowanceID = 0;
        public bool isDeduction = false;
        public List<int> incomeIDs = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                bodyBlock.InnerHtml = "Allowance not selected";   
                return;
            }
            allowanceID = int.Parse(Request.QueryString["ID"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdf", "var hasImport = " + (HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {
            var template = ("~/App_Data/ExcelTemplate/DefinedSalaryImport.xlsx");

            List<TAAllowanceRate> detailGridList = TravelAllowanceManager.getAllowanceDetailList(allowanceID);

            List<TAAllowanceLocation> locations = TravelAllowanceManager.GetLocationList();
            List<BLevel> levels = NewPayrollManager.GetAllParentLevelList();
          
            {
                ExcelGenerator.WriteTravelAllowanceForExportExcel(template, locations, levels, detailGridList,allowanceID);
            }
        }


        private List<int> GetEmployeeIdList()
        {
            return new List<int>();
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            var template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                var datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


                var xTypeItem = 0;
                

                var importedCount = 0;


                try
                {
                    var rateList = new List<TAAllowanceRate>();

                    List<TAAllowanceLocation> locations = TravelAllowanceManager.GetLocationList();
                    List<BLevel> levels = NewPayrollManager.GetAllParentLevelList();
          
                    //var income = new PayManager().GetIncomeById(allowanceID);
                    var name = string.Empty;
                    var rowNumber = 1;
                    int levelId = 0;
                    object value = "";
                    decimal rate = 0;

                    //if (income.XType != null && income.XType != 0 && (income.YType == null || income.YType == 0))
                    {
                        //var columnName = ((IncomeDefinedTypeEnum)income.XType).ToString();

                       
                        foreach (DataRow row in datatable.Rows)
                        {
                            name = row["Level"].ToString();

                            if (levels.Any(x => x.GroupLevel.ToLower() == name.ToLower().Trim()) == false)
                            {
                                divWarningMsg.InnerHtml = "Invalid level in the row " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            levelId = levels.FirstOrDefault(x => x.GroupLevel.ToLower() == name.ToLower().Trim()).LevelId;

                            foreach (TAAllowanceLocation item in locations)
                            {
                                value  = row[item.LocationName];

                                if (value == null)
                                    continue;

                                if (value.ToString() == "")
                                    continue;

                                if (decimal.TryParse(value.ToString(), out rate))
                                {
                                    rateList.Add(new TAAllowanceRate
                                    {
                                        AllowanceID = allowanceID,
                                        LevelID = levelId,
                                        LocationId = item.LocationId,
                                        Rate = rate
                                    });
                                }

                            }

                            rowNumber += 1;
                            importedCount += 1;



                        }
                    }
                    

                    var status = TravelAllowanceManager.InsertUpdateAllowanceDetail(rateList, allowanceID);

                    if (status.IsSuccess == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }


                    divMsgCtl.InnerHtml = "Import successfull";
                    divMsgCtl.Hide = false;

                    HasImport = true;
                }
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                divWarningMsg.InnerHtml = "Excel version should be 2007(.xlsx) or higher.";
                divWarningMsg.Hide = false;
            }
        }
    }
}

