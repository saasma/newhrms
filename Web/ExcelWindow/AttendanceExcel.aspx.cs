﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;
using Utils;
using BLL.BO;
using BLL.Entity;

namespace Web.CP
{
    public partial class AttendanceExcel : BasePage
    {
        int payrollPerodId = 0;
        CommonManager mgr = new CommonManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.payrollPerodId = UrlHelper.GetIdFromQueryString("payrollPeriod");

        }
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/AttendanceLeave.xlsx");


            //string[] leaveList = LeaveAttendanceManager.GetLeaveListForImport(SessionManager.CurrentCompanyId).Select(a => a.Title).ToArray();
            //List<LLeaveType> leaves = LeaveAttendanceManager.GetLeaveListForImport(SessionManager.CurrentCompanyId).ToList();

       

            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteExportExcel(template);

        }

        public AttendanceImportEmployeeLeave SetImportError(string msg, AttendanceImportEmployeeLeave emp)
        {
            emp.IsValidImport = false;
            emp.ImportErrorMsg = msg + " for the employee \"" + (emp.Name == null ? "" : emp.Name) + "\" ";
            emp.ImportErrorMsg += "taken on " + emp.TakenDate.ToShortDateString() + ".";
            return emp;
        }




        public AttendanceImportEmployeeLeave GetImportedEmployee(List<LLeaveType> leaveList, DataRow row, DataTable table,PayrollPeriod payroll)
        {
            // If all Columns Empty then return null
           //if( row[ExcelGenerator.employeesStaticColumns[0]] == DBNull.Value &&
              
           //    row[ExcelGenerator.employeesStaticColumns[1]] == DBNull.Value)
           //    return null;


            AttendanceImportEmployeeLeave emp = new AttendanceImportEmployeeLeave();
         
            emp.IsValidImport = true;

            object value;

            // EIN Column
            value = row["EIN"];
            if (value != null && value.ToString() != "")
            {
                int ein;
                if (int.TryParse(value.ToString(), out ein))
                    emp.EmployeeId = ein;
                else
                    return SetImportError("Invalid EIN", emp);
            }

            value = row["Employee Name"];
            if (value != null && value.ToString() != "")
                emp.Name = value.ToString();

            value = row["Leave Taken Date"];
            DateTime date;
            if (!DateTime.TryParse(value.ToString(), out date))
            {
                return SetImportError("Invalid Leave taken date", emp);
            }
            emp.TakenDate = date;

            //current month attendance only
            if (emp.TakenDate < payroll.StartDateEng || emp.TakenDate > payroll.EndDateEng)
                return null;

            value = row["Is Half Day"];
            if (value == null || value.ToString() == "")
                emp.IsHalfDay = false;
            else
            {
                bool isHalfDay = false;
                if (bool.TryParse(value.ToString(), out isHalfDay))
                {
                    emp.IsHalfDay = isHalfDay;
                }
            }

            value = row["Leave Taken"];
            if (value == null || value.ToString() == "")
            {
                emp.NoLeave = true;
                //return SetImportError("Leave taken blank ", emp);
            }
            else
            {

                string leaveName = value.ToString().ToLower();
                bool leaveExists = false;

                if (leaveName == LeaveAttendanceManager.Unpaid_Leave.ToLower())
                {
                    leaveExists = true;
                    emp.LeaveAbbr = "UPL";
                    if (emp.IsHalfDay)
                        emp.LeaveAbbr += "/2";
                    emp.IsUnpaidLeave = true;   
                }
                else
                {
                    foreach (LLeaveType leave in leaveList)
                    {
                        if (leaveName == leave.Title.ToLower())
                        {
                            emp.LeaveTypeId = leave.LeaveTypeId;
                            emp.LeaveAbbr = leave.Abbreviation;
                            if (emp.IsHalfDay)
                                emp.LeaveAbbr += "/2";
                            leaveExists = true;
                        }
                    }
                }
                if (leaveExists == false)
                {
                    return SetImportError(string.Format("Leave \"{0}\" doesn't exist assigned ",value), emp);
                }
            }
           


            return emp;
        }



        
        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            PayrollPeriod payroll = CommonManager.GetPayrollPeriod(this.payrollPerodId);

            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListForImport(SessionManager.CurrentCompanyId);


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                try
                {
                    DataTable datatable = new DataTable();
                    string excelConnection = string.Format(ExcelGenerator.ExcelConnectionString, template);
                    string sql = "select * from [Employee_Info$]";// where [Leave Taken Date] between ? And ?";
                     using (OleDbConnection conn = new OleDbConnection(excelConnection))
                     {
                         OleDbCommand cmd = new OleDbCommand(sql, conn);
                         cmd.Parameters.Add("StartDate", OleDbType.DBDate, 0);
                         cmd.Parameters.Add("EndDate", OleDbType.DBDate, 0);

                         cmd.Parameters["StartDate"].Value = payroll.StartDateEng;
                         cmd.Parameters["EndDate"].Value = payroll.EndDateEng;

                         OleDbDataAdapter da = new OleDbDataAdapter(sql, conn);
                         
                         // Fill the DataTable with data from the Excel spreadsheet.
                         da.Fill(datatable);
                     }


                    if (datatable == null)
                    {
                        File.Delete(template);
                        return;
                    }

                   // List<AttendanceImportEmployee> employeeList = new List<AttendanceImportEmployee>();
                    Dictionary<int, AttendanceImportEmployee> list = new Dictionary<int, AttendanceImportEmployee>();


                    foreach (DataRow row in datatable.Rows)
                    {

                        AttendanceImportEmployeeLeave emp = GetImportedEmployee(leaveList, row, datatable,payroll);

                        if (emp == null)
                            continue;

                        if (emp.IsValidImport)
                        {
                            if (list.ContainsKey(emp.EmployeeId))
                            {
                                list[emp.EmployeeId].leaves.Add(emp);
                            }
                            else
                            {
                                AttendanceImportEmployee empLeave = new AttendanceImportEmployee();
                                empLeave.EmployeeId = emp.EmployeeId;
                                empLeave.leaves.Add(emp);

                                list[empLeave.EmployeeId] = empLeave;
                            }

                        }
                        if (emp.IsValidImport == false)
                        {
                            divWarningMsg.InnerHtml = emp.ImportErrorMsg;
                            divWarningMsg.Hide = false;
                            return;
                        }

                    }

                    int count = 0;
                    string msg = "";
                    if (EmployeeManager.SaveImportedAttendance(list.Values.ToList(), payroll.PayrollPeriodId, ref msg, ref count,null))
                    {
                        divMsgCtl.InnerHtml = string.Format("{0} employees imported successfully.", count);
                        divMsgCtl.Hide = false;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = msg;
                        divWarningMsg.Hide = false;
                    }

                }




                   //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                         exp.ColumnName, "") + " : " + exp.ToString();
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError) + " : " + exp.ToString();
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError) + " : " + exp.ToString();
                    divWarningMsg.Hide = false;
                }
                //catch (CompulsoryImportFieldException exp1)
                //{
                //    divMsgCtl.Text = exp1.Message;
                //}

                finally
                {
                    File.Delete(template);
                }

                this.HasImport = true;
            }


        }

    }
}

   


