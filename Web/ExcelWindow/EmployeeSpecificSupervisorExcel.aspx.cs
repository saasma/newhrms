﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class EmployeeSpecificSupervisorExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                    hdnType.Text = Request.QueryString["Type"];
            }
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmployeeSupervisor.xlsx");

            int total = 0;
            int type = int.Parse(hdnType.Text);

            List<GetEmpSpecificLeaveDefListResult> list =
                LeaveRequestManager.GetEmployeeSupervisorList(0, 99999, -1, -1, -1, -1, -1, type, ref total).OrderBy(x => x.Name)
                .ToList();



            ExcelGenerator.WriteEmployeeSupervisorExcel(template, list);
        }




       

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            int total = 0;
            List<GetLeaveProjectsResult> projectList =
                LeaveRequestManager.GetProjectList("", 1, 99999, ref total).ToList();



            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

               
                int employeeId;
                string ein = "";
                int otherEmpId = 0;

                List<GetEmpSpecificLeaveDefListResult> list = new List<GetEmpSpecificLeaveDefListResult>();
                int importedCount = 0;
                

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {

                         GetEmpSpecificLeaveDefListResult item = new GetEmpSpecificLeaveDefListResult();

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.EmployeeId = employeeId;

                        otherEmpId = 0;
                        ein = row["Recommender1 EIN"].ToString();
                        if (!string.IsNullOrEmpty(ein) && int.TryParse(ein, out otherEmpId) == false)
                        {
                            divWarningMsg.InnerHtml = string.Format("Invalid employee ID {0}.",
                                  employeeId);
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                            item.R1EIN = otherEmpId;

                        otherEmpId = 0;
                        ein = row["Recommender2 EIN"].ToString();
                        if (!string.IsNullOrEmpty(ein) && int.TryParse(ein, out otherEmpId) == false)
                        {
                            divWarningMsg.InnerHtml = string.Format("Invalid employee ID {0}.",
                                  employeeId);
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                            item.R2EIN = otherEmpId;

                        otherEmpId = 0;
                        ein = row["Approval1 EIN"].ToString();
                        if (!string.IsNullOrEmpty(ein) && int.TryParse(ein, out otherEmpId) == false)
                        {
                            divWarningMsg.InnerHtml = string.Format("Invalid employee ID {0}.",
                                  employeeId);
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                            item.A1EIN = otherEmpId;

                        otherEmpId = 0;
                        ein = row["Approval2 EIN"].ToString();
                        if (!string.IsNullOrEmpty(ein) && int.TryParse(ein, out otherEmpId) == false)
                        {
                            divWarningMsg.InnerHtml = string.Format("Invalid employee ID {0}.",
                                  employeeId);
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                            item.A2EIN = otherEmpId;

                        if (item.R1EIN == 0)
                            item.R1EIN = null;
                        if (item.R2EIN == 0)
                            item.R2EIN = null;
                        if (item.A1EIN == 0)
                            item.A1EIN = null;
                        if (item.A2EIN == 0)
                            item.A2EIN = null;

                        item.Type = int.Parse(hdnType.Text);

                        list.Add(item);

                        importedCount += 1;

                        

                      

                    }



                    int imported = 0;
                    string msg;
                    LeaveRequestManager.SaveUpdateEmployeeSupervisor(list);

                    //if (!string.IsNullOrEmpty(msg))
                    //{
                    //    divWarningMsg.InnerHtml = msg;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

