﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;
using System.Data.Linq;

namespace Web.CP
{
    public partial class EmpAddressImportExcel : BasePage
    {
        public string EmpId;
        public string BranchId;
        public string LevelId;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["EmpID"]))
                EmpId = Request.QueryString["EmpID"];
            if (!string.IsNullOrEmpty(Request.QueryString["BranchID"]))
                BranchId = Request.QueryString["BranchID"];
            if (!string.IsNullOrEmpty(Request.QueryString["LevelID"]))
                LevelId = Request.QueryString["LevelID"];
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/EmpAddressImport.xlsx");

            int empID = -1, braID = -1, levlID = -1;
            if (!string.IsNullOrEmpty(EmpId))
            {
                empID = int.Parse(EmpId);
            }

            if (!string.IsNullOrEmpty(BranchId))
            {
                braID = int.Parse(BranchId);
            }

            if (!string.IsNullOrEmpty(LevelId))
            {
                levlID = int.Parse(LevelId);
            }

            List<GetEmployeeAddressResult> list = NewHRManager.GetEmployeeAddressResult(0, 9999999, empID, "", braID, levlID);
            List<CountryName> CountryList = CommonManager.GetCountryNames();
            List<DistrictList> DistrictList = CommonManager.GetDistrictNames();
            List<ZoneList> ZoneList = CommonManager.GetZoneList();
            ExcelGenerator.ExportEmployeeAddress(template, list, CountryList, ZoneList, DistrictList);
        }

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID();
        }
        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);
                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");
                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }
                int employeeId;
                List<int> employeeIDList = GetEmployeeIdList();
                List<int> addedEmployeeIDList = new List<int>();

                List<CountryList> listCountries = new CommonManager().GetAllCountries().ToList();
                List<DistrictList> DistrictList = CommonManager.GetDistrictNames();
                List<ZoneList> ZoneList = CommonManager.GetZoneList();
                string pcountry = "", pdistrict = "", pzone = "", tcountry = "", tdistrict = "", tzone = "";

                int importedCount = 0;
                try
                {
                    List<EAddress> list = new List<EAddress>();
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        EAddress _db = new EAddress();
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == true)
                        {
                            if (addedEmployeeIDList.Contains(employeeId))
                            {
                                divWarningMsg.InnerHtml = "Employee ID " + employeeId + " has been repeated in the excel.";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            else
                                addedEmployeeIDList.Add(employeeId);

                            rowNumber += 1;

                            _db.EmployeeId = employeeId;

                            #region parmanent

                            pcountry = row["Permanent Country"].ToString().Trim();
                            if (!string.IsNullOrEmpty(pcountry))
                            {
                                CountryList objCountry = listCountries.SingleOrDefault(x => x.CountryName.ToLower() == pcountry.ToLower());
                                if (objCountry == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid parmanent country for row number " + rowNumber + ".";
                                    divWarningMsg.Hide = false;
                                    return;
                                }

                                _db.PECountryId = objCountry.CountryId;
                            }

                            pzone = row["Permanent Zone"].ToString().Trim();
                            if (!string.IsNullOrEmpty(pzone))
                            {
                                ZoneList objzone = ZoneList.SingleOrDefault(x => x.Zone.ToLower() == pzone.ToLower());
                                if (objzone == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid permanent zone for row number " + rowNumber + ".";
                                    divWarningMsg.Hide = false;
                                    return;
                                }

                                _db.PEZoneId = objzone.ZoneId;
                            }

                            pdistrict = row["Permanent District"].ToString().Trim();
                            if (!string.IsNullOrEmpty(pdistrict))
                            {
                                DistrictList objDistrict = DistrictList.SingleOrDefault(x => x.District.ToLower() == pdistrict.ToLower());
                                if (objDistrict == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid permanent district for row number " + rowNumber + ".";
                                    divWarningMsg.Hide = false;
                                    return;
                                }

                                _db.PEDistrictId = objDistrict.DistrictId;
                            }

                            if (!string.IsNullOrEmpty(row["Permanent VDC/Municipality"].ToString().Trim()))
                            {
                                _db.PEVDCMuncipality = row["Permanent VDC/Municipality"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Permanent Street/Colony"].ToString().Trim()))
                            {
                                _db.PEStreet = row["Permanent Street/Colony"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Permanent Ward No"].ToString().Trim()))
                            {
                                _db.PEWardNo = row["Permanent Ward No"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Permanent House No"].ToString().Trim()))
                            {
                                _db.PEHouseNo = row["Permanent House No"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Permanent Locality"].ToString().Trim()))
                            {
                                _db.PELocality = row["Permanent Locality"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Permanent State"].ToString().Trim()))
                            {
                                _db.PEState = row["Permanent State"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Permanent Zip Code"].ToString().Trim()))
                            {
                                _db.PEZipCode = row["Permanent Zip Code"].ToString().Trim();
                            }

                            #endregion
                            #region temporary
                            tcountry = row["Temporary Country"].ToString().Trim();
                            if (!string.IsNullOrEmpty(tcountry))
                            {
                                CountryList objCountry = listCountries.SingleOrDefault(x => x.CountryName.ToLower() == tcountry.ToLower());
                                if (objCountry == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid temporary country for row number " + rowNumber + ".";
                                    divWarningMsg.Hide = false;
                                    return;
                                }

                                _db.PSCountryId = objCountry.CountryId;
                            }

                            tzone = row["Temporary Zone"].ToString().Trim();
                            if (!string.IsNullOrEmpty(tzone))
                            {
                                ZoneList objzone = ZoneList.SingleOrDefault(x => x.Zone.ToLower() == tzone.ToLower());
                                if (objzone == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid temporary zone for row number " + rowNumber + ".";
                                    divWarningMsg.Hide = false;
                                    return;
                                }

                                _db.PSZoneId = objzone.ZoneId;
                            }

                            tdistrict = row["Temporary District"].ToString().Trim();
                            if (!string.IsNullOrEmpty(tdistrict))
                            {
                                DistrictList objDistrict = DistrictList.SingleOrDefault(x => x.District.ToLower() == tdistrict.ToLower());
                                if (objDistrict == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid temporary district for row number " + rowNumber + ".";
                                    divWarningMsg.Hide = false;
                                    return;
                                }

                                _db.PSDistrictId = objDistrict.DistrictId;
                            }
                            if (!string.IsNullOrEmpty(row["Temporary VDC/Municipality"].ToString().Trim()))
                            {
                                _db.PSVDCMuncipality = row["Temporary VDC/Municipality"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Temporary Street/Colony"].ToString().Trim()))
                            {
                                _db.PSStreet = row["Temporary Street/Colony"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Temporary Ward No"].ToString().Trim()))
                            {
                                _db.PSWardNo = row["Temporary Ward No"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Temporary House No"].ToString().Trim()))
                            {
                                _db.PSHouseNo = row["Temporary House No"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Temporary Locality"].ToString().Trim()))
                            {
                                _db.PSLocality = row["Temporary Locality"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Temporary State"].ToString().Trim()))
                            {
                                _db.PSState = row["Temporary State"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Temporary Zip Code"].ToString().Trim()))
                            {
                                _db.PSZipCode = row["Temporary Zip Code"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Citizenship Issue Place"].ToString().Trim()))
                            {
                                _db.PSCitIssDis = row["Citizenship Issue Place"].ToString().Trim();
                            }

                            #endregion
                            #region official Contact
                            if (!string.IsNullOrEmpty(row["Official Email"].ToString().Trim()))
                            {
                                _db.CIEmail = row["Official Email"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Official Phone"].ToString().Trim()))
                            {
                                _db.CIPhoneNo = row["Official Phone"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Official Extension"].ToString().Trim()))
                            {
                                _db.Extension = row["Official Extension"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Official Mobile"].ToString().Trim()))
                            {
                                _db.CIMobileNo = row["Official Mobile"].ToString().Trim();
                            }
                            #endregion
                            #region personal Contact
                            if (!string.IsNullOrEmpty(row["Personal Email"].ToString().Trim()))
                            {
                                _db.PersonalEmail = row["Personal Email"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Personal Phone"].ToString().Trim()))
                            {
                                _db.PersonalPhone = row["Personal Phone"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Personal Mobile"].ToString().Trim()))
                            {
                                _db.PersonalMobile = row["Personal Mobile"].ToString().Trim();
                            }
                            #endregion
                            #region Emergency Contact
                            if (!string.IsNullOrEmpty(row["Emergency Contact Name"].ToString().Trim()))
                            {
                                _db.EmergencyName = row["Emergency Contact Name"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Emergency Relation"].ToString().Trim()))
                            {
                                _db.EmergencyRelation = row["Emergency Relation"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Emergency Phone"].ToString().Trim()))
                            {
                                _db.EmergencyPhone = row["Emergency Phone"].ToString().Trim();
                            }

                            if (!string.IsNullOrEmpty(row["Emergency Mobile"].ToString().Trim()))
                            {
                                _db.EmergencyMobile = row["Emergency Mobile"].ToString().Trim();
                            }
                            #endregion


                            importedCount += 1;

                            list.Add(_db);
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                    }
                    Status status = NewHRManager.SaveEmployeeAddressImport(list);
                    if (status.IsSuccess == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }
                    divMsgCtl.InnerHtml = string.Format("{0} Records has been imported", importedCount);
                    divMsgCtl.Hide = false;
                    this.HasImport = true;
                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value, exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                divWarningMsg.InnerHtml = "Excel version should be 2007(.xlsx) or higher.";
                divWarningMsg.Hide = false;
            }
        }
    }
}

