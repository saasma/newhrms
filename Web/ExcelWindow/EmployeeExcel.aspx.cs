﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;
using Utils;
using BLL.BO;

namespace Web.CP
{
    public partial class EmployeeExcel : BasePage
    {
        //private bool? _IsEnglishDate = null;
        public bool ImportIsEnglishDate
        {
            get
            {
               
                // for Nepali calendar is English option set in web.config then 
                if (this.IsEnglish == false && ddlDatesType.SelectedValue.ToString().ToLower().Equals("true")) ///Config.ImportDateIsEnglish != null && Config.ImportDateIsEnglish.Value)
                    return true;
               // Config.ImportDateIsEnglish
                return this.IsEnglish;

                //return this.IsEnglish;
                //if (_IsEnglishDate != null)
                //    return _IsEnglishDate.Value;

                //_IsEnglishDate = Config.ImportDateIsEnglish;

                //if (_IsEnglishDate == null)
                //    _IsEnglishDate = IsEnglish;

                //return _IsEnglishDate.Value;

            }
        }
        private string _DateFormat = ""; 
        public string ImportDateFormat
        {
            get
            {
                if (_DateFormat != "")
                    return _DateFormat;
                _DateFormat = Config.ImportDateFormat;

                return _DateFormat;
            }
        }

        CommonManager mgr = new CommonManager();
        List<HCaste> ethinicityList = new CommonManager().GetAllCastes().ToList();//.Select(x => x.CasteName).ToArray();
        List<ELocation> locationList = new CommonManager().GetAllLocations().ToList();//.Select(x => x.Name).ToArray();
        List<EGrade> gradeStepList = new CommonManager().GetAllGrades().ToList();//.Select(x => x.Name).ToArray();

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public void ProcessSectionLevelPermission()
        {
            if (SessionManager.IsCustomRole)
            {
                UUser user = UserManager.GetUserByUserName(Page.User.Identity.Name);
                int currentPageId = UserManager.GetPageIdByPageUrl("employee.aspx");

                bool permissionExists = false;
                List<UPermissionSection> sections = UserManager.GetAccessibleSectionsForPage(user.RoleId.Value, currentPageId);

                foreach (UPermissionSection section in sections)
                {
                    if (section.PageSectionId == 2)
                    {
                        permissionExists = true;
                    }
                }

                if (permissionExists == false)
                {
                    Response.Write("Not Enough Permission.");
                    Response.End();
                    return;
                }

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (SessionManager.User.URole.RoleId != (int)Role.Administrator)
            {
                Response.Write("Not enough permisson.");
                Response.End();
                return;
            }

            if(!IsPostBack)
            {
                divDates.Visible = IsEnglish == false;
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            ProcessSectionLevelPermission();
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }

        

        protected void btnExport_Click(object sender, EventArgs e)
        {

              PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

              if (lastPayroll == null)
              {
                  divWarningMsg.InnerHtml = "Payroll Period must be defined.";
                  divWarningMsg.Hide = false;
                  return;
              }

              bool isBlankEmpList = btnBlankExport == sender;
            bool isAllEmployees = btnAllEmp == sender;

            string template = ("~/App_Data/ExcelTemplate/Employee.xlsx");

            List<EDesignation> desitList = mgr.GetAllDesignations();
            string[] designationList = desitList.Select(a => a.BLevel.Name + " - " + a.Name).ToArray();
            string[] branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).Select(a => a.Name).ToArray();

            string[] departmentList = new DepartmentManager().GetDepartmentsByCompany(SessionManager.CurrentCompanyId).Select(a => a.DepartmentAndBrachName).ToArray();
            string[] subdepartmentList = new DepartmentManager().GetSubDepartmentsByCompany(SessionManager.CurrentCompanyId).Select(a => a.HierarchyName).ToArray();

            string[] shiftList = ShiftManager.GetAllShifts().Select(x => x.Name).ToArray();

            string[] bankList = PayManager.GetBankList().Select(x => x.Name).ToArray();

            string[] unitList = CommonManager.GetAllUnitList().Select(x => x.Name).ToArray();

            string[] costCodeList = CommonManager.GetAllCostCodes().Select(x => x.Name).ToArray();
            //List<HCaste> ethinicityList = new CommonManager().GetAllCastes().ToList();//.Select(x => x.CasteName).ToArray();
            //List<ELocation> locationList = new CommonManager().GetAllLocations().ToList();//.Select(x => x.Name).ToArray();
            //List<EGrade> gradeStepList = new CommonManager().GetAllGrades().ToList();//.Select(x => x.Name).ToArray();
            string[] levelList = NewPayrollManager.GetAllParentLevelList().Select(x => x.GroupName + " - " + x.Name).ToList().ToArray();
            string[] incomeList = PayManager.GetIncomeListByCompanyForImportEmployee(SessionManager.CurrentCompanyId).Select(a => a.Title).ToArray();
            List<PIncome> incomes = PayManager.GetIncomeListByCompanyForImportEmployee(SessionManager.CurrentCompanyId).ToList();
            string[] deductionList =
                PayManager.GetDeductionListByCompanyForEmployeeImport(SessionManager.CurrentCompanyId).Select(a => a.Title).ToArray();
            List<PDeduction> deductions = PayManager.GetDeductionListByCompanyForEmployeeImport(SessionManager.CurrentCompanyId).ToList();
            string[] leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .Where(x => x.IsParentGroupLeave == null || x.IsParentGroupLeave == false).Select(a => a.Title).ToArray();
            List<LLeaveType> leaves = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .Where(x=>x.IsParentGroupLeave == null || x.IsParentGroupLeave == false).ToList();

            List<KeyValue> statusList = new JobStatus().GetMembers();

            statusList.RemoveAt(0);

            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteEmployeeExportExcel(template,desitList, designationList, branchList, departmentList,subdepartmentList, incomeList,
                deductionList, leaveList,ImportIsEnglishDate,ImportDateFormat
                , incomes, deductions, leaves, statusList, levelList, shiftList, costCodeList,ethinicityList,locationList,gradeStepList,bankList,isBlankEmpList,unitList,isAllEmployees);

        }
       
        public EEmployee SetImportError(string msg, EEmployee emp,int row)
        {
            emp.IsValidImport = false;
            emp.ImportErrorMsg += "<br>" + msg + " for the employee \"" + (emp.Name == null ? "" : emp.Name) + "\" for the row " + (row+1) + ".";
            return emp;
        }

        public EEmployee SetImportErrorWithoutAppend(string msg, EEmployee emp, int row)
        {
            emp.IsValidImport = false;
            emp.ImportErrorMsg += "<br>" + msg + "\" for the row " + (row + 1) + ".";
            return emp;
        }

        public string GetAvailableStatuses(List<KeyValue> statusList)
        {
            string str = "";
            foreach (KeyValue val in statusList)
            {
                if (str == "")
                    str = val.Value;
                else
                    str += ", " + val.Value;
            }
            return str;
        }


        public EEmployee GetImportedEmployee(List<EEmployee> employeeList, List<EDesignation> designationList, List<Branch> branchList, List<Department> departmentList,List<SubDepartment> subdepartmentList,
             List<PIncome> incomeList, List<PDeduction> deductionList, List<LLeaveType> leaveList, DataRow row, DataTable table, List<KeyValue> statusNameList, List<Bank> bankList, int index, List<BLevel> levelList, PIncome basicIncome, List<EWorkShift> shiftList, List<ECostCode> costCodeList
            , List<UnitList> unitList)
        {
            // If all Columns Empty then return null
           if( row[ExcelGenerator.employeesStaticColumns[0]] == DBNull.Value &&
               row[ExcelGenerator.employeesStaticColumns[1]] == DBNull.Value &&
               row[ExcelGenerator.employeesStaticColumns[2]] == DBNull.Value)
               return null;



            EEmployee emp = new EEmployee();
            emp.EHumanResources.Add(new EHumanResource());
            emp.HHumanResources.Add(new HHumanResource());
            emp.EAddresses.Add(new EAddress());
            EFundDetail fundDetail = new EFundDetail();
            fundDetail.NoCITContribution = false;
            emp.EFundDetails.Add(fundDetail);
            emp.PPays.Add(new PPay());

            // to track if no EIN column employee exists then all columns should have values else only updatable column exists or column can be deleted
            bool isNewEmployee = false;

            emp.IsValidImport = true;

            object value; decimal amount;

            // EIN Column
            value = row["EIN"];
            if (value != null && value.ToString() != "")
            {
                int ein;
                if (int.TryParse(value.ToString(), out ein))
                {
                    if (ein == 0)
                    {
                        SetImportError("0 EIN cannot not be imported", emp, index);
                    }
                    else
                    {
                        isNewEmployee = false;
                        emp.EmployeeId = ein;
                    }
                }
                else
                    SetImportError("Invalid EIN", emp,index);
            }
            else
                isNewEmployee = true;

           

            //Title Column
            value = row[ExcelGenerator.employeesStaticColumns[0]];
            if (value == null || value.ToString().Trim() == "")
                SetImportError(string.Format("Title is required in row {0} ", (index + 1)), emp, index);
            KeyValue titleValue = new Title().GetMembers().SingleOrDefault(a => a.Value.ToLower() == value.ToString().ToLower().Trim());
            if (titleValue==null)
                SetImportError("Invalid title \"" + value + "\" for row " + (index + 1) + " ", emp, index);
			else
				emp.Title = titleValue.Value;

            //Employee Name Column
            value = row[ExcelGenerator.employeesStaticColumns[1]];
            if (value == null || value.ToString().Trim() == "")
                SetImportError("First name is required", emp, index);
            emp.FirstName = value.ToString().Trim();
            emp.MiddleName = row[ExcelGenerator.employeesStaticColumns[2]].ToString().Trim();
            value = row[ExcelGenerator.employeesStaticColumns[3]];
            if (value == null || value.ToString().Trim() == "")
                SetImportError("Last name is required", emp, index);
            emp.LastName = value.ToString().Trim();
            emp.Name = //emp.FirstName + " " + emp.MiddleName + " " + emp.LastName;
            EEmployee.GetCombinedName(emp.FirstName, emp.MiddleName, emp.LastName);
            //Employee Name Column
            value = row[ExcelGenerator.employeesStaticColumns[4]];
            emp.FatherName = value == null ? "" : value.ToString();

            // validate if EIN repeated in the excel
            if (isNewEmployee == false &&  employeeList
                .Any(x => x.EmployeeId == emp.EmployeeId))
            {
                SetImportError("EIN " + emp.EmployeeId + " has been repeated", emp, index);
            }

            //Gender
            value = row[ExcelGenerator.employeesStaticColumns[5]];
            if (value == null || value.ToString().Trim() == "")
                SetImportError("Sex/Gender is required", emp, index);
            if (new Gender().GetMembers().Any(a => a.Value == value.ToString().Trim()) == false)
                SetImportError("Invalid sex/gender", emp, index);
            if (!string.IsNullOrEmpty(value.ToString().Trim()))
                emp.Gender = new Gender().GetValue(value.ToString().Trim());


            //Martial Status
            value = row[ExcelGenerator.employeesStaticColumns[6]];
            if (value == null || value.ToString().Trim() == "")
                SetImportError("Marital Status is required", emp, index);
            if (new MaritalStatus().GetMembers().Any(a => a.Value == value.ToString().Trim()) == false)
                SetImportError("Invalid marital status", emp, index);
            emp.MaritalStatus = value.ToString().Trim();
            
            //Is Handicapped
            value = row[ExcelGenerator.employeesStaticColumns[7]];
            if (value != null && value.ToString().Trim().ToLower() == "yes")
                emp.IsHandicapped = true;
            else
                emp.IsHandicapped = false;

            //Couple Tax Status only if Marital status is Married
            if (emp.MaritalStatus == MaritalStatus.MARRIED)
            {
                value = row[ExcelGenerator.employeesStaticColumns[8]];
                if (value != null && value.ToString().Trim().ToLower() == "yes")
                    emp.HasCoupleTaxStatus = true;
                else
                    emp.HasCoupleTaxStatus = false;
            }

            //Dependent if Marital status is Window/Windower
            if (emp.MaritalStatus == MaritalStatus.WIDOW || emp.MaritalStatus == MaritalStatus.WIDOWER)
            {
                value = row[ExcelGenerator.employeesStaticColumns[9]];
                if (value != null && value.ToString().Trim().ToLower() == "yes")
                    emp.HasDependent = true;
                else
                    emp.HasDependent = false;
            }

            if (this.IsEnglish)
            {
                // If English DOB  Cell
                value = row[ExcelGenerator.employeesStaticColumns[11]];
                if (value != null && value.ToString() != "")
                {
                    try
                    {
                        emp.IsEnglishDOB = true;
                        emp.DateOfBirth = CustomDate.GetCustomDateFromString(value.ToString(), true).ToString();
                        emp.DateOfBirthEng = DateTime.Parse(emp.DateOfBirth);
                    }
                    catch (Exception e1)
                    {
                        SetImportError("Invalid english DOB, should be in the format(yyyy/mm/dd)", emp, index);
                    }
                }


                // Nep DOB Cell
                value = row[ExcelGenerator.employeesStaticColumns[10]];
                if (value != null && value.ToString() != "")
                {
                    // Trim time field if has
                    if (value.ToString().Contains(" "))
                        value = value.ToString().Remove(value.ToString().IndexOf(" "));
                    try
                    {
                        emp.IsEnglishDOB = true;
                        CustomDate nepDOB = CustomDate.GetCustomDateFromString(value.ToString(), false);
                        CustomDate engDOB = CustomDate.ConvertNepToEng(nepDOB);
                        emp.DateOfBirth = engDOB.ToString();
                        emp.DateOfBirthEng = GetEngDate(emp.DateOfBirth);
                    }
                    catch (Exception e1)
                    {
                        SetImportError("Invalid nepali DOB, should be in the format(yyyy/mm/dd)", emp, index);
                    }
                }

              

            }
            else
            {

                // If English DOB Cell
                value = row[ExcelGenerator.employeesStaticColumns[11]];
                if (value != null && value.ToString() != "")
                {
                    try
                    {
                        emp.IsEnglishDOB = false;
                        emp.DateOfBirthEng = DateTime.Parse(value.ToString());
                        CustomDate engDOB = new CustomDate(emp.DateOfBirthEng.Value.Day, emp.DateOfBirthEng.Value.Month,
                            emp.DateOfBirthEng.Value.Year, true);
                        emp.DateOfBirth = CustomDate.ConvertEngToNep(engDOB).ToString();
                        
                    }
                    catch (Exception e1)
                    {
                        SetImportError("Invalid english DOB, should be in the format(yyyy/mm/dd)", emp, index);
                    }
                }



                // Nep DOB Cell
                value = row[ExcelGenerator.employeesStaticColumns[10]];
                if (value != null && value.ToString() != "")
                {
                    // Trim time field if has
                    if (value.ToString().Contains(" "))
                        value = value.ToString().Remove(value.ToString().IndexOf(" "));
                    try
                    {
                        emp.IsEnglishDOB = false;
                        emp.DateOfBirth = CustomDate.GetCustomDateFromString(value.ToString(), false).ToString();
                        emp.DateOfBirthEng = GetEngDate(emp.DateOfBirth);
                    }
                    catch (Exception e1)
                    {
                        SetImportError("Invalid nepali DOB, should be in the format(yyyy/mm/dd)", emp, index);
                    }
                }


            }

            

            //Designation
            value = row[ExcelGenerator.employeesStaticColumns[12]];
            if (value == null || value.ToString().Trim() == "")
                SetImportError("Designation is required", emp, index);
            string designationText = value.ToString();
            EDesignation desig = designationList
                .Where(w =>     
                        // Level - Designation Name or Designation Name
                        (w.BLevel.Name + " - "+ w.Name).ToLower().Trim() == value.ToString().Trim().ToLower() ||
                        (w.BLevel.Name + "-" + w.Name).ToLower().Trim() == value.ToString().Trim().ToLower() ||
                        (w.Name).ToLower().Trim() == value.ToString().Trim().ToLower()
                        )
                .FirstOrDefault();
            if (desig == null)
                SetImportError("Invalid designation for row " + (index + 1) + " ", emp, index);
            else
                emp.DesignationId = desig.DesignationId;


            string branchName = "";
            //Branch
            value = row[ExcelGenerator.employeesStaticColumns[13]];
            if (value == null || value.ToString().Trim() == "")
                SetImportError("Branch is required", emp, index);
            Branch branch = branchList.Where(w => w.Name.ToString().ToLower()
                == value.ToString().Trim().ToLower()).Take(1).SingleOrDefault();
            if (branch == null)
                SetImportError("Invalid Branch", emp, index);
            else
            {
                branchName = value.ToString().Trim();
                emp.BranchId = branch.BranchId;
            }

            //Department
            value = row[ExcelGenerator.employeesStaticColumns[14]];
            if (value == null || value.ToString().Trim() == "")
                SetImportError("Department is required", emp, index);
            List<Department> departments = departmentList.Where(w => w.DepartmentAndBrachName == value.ToString().Trim()
                || w.Name.ToLower() == value.ToString().Trim()).ToList();// .Take(1).SingleOrDefault();
            Department department = null;
            // If Multiple Department with same name then find the correct one under the current Branch
            //if (departments.Count > 1)
            //{
            //    foreach (Department depEntity in departments)
            //    {
            //        if (depEntity.Branch.Name == branchName)
            //        {
            //            department = depEntity;
            //        }
            //    }
            //}
            if (department == null && departments.Count > 0)
                department = departments[0];

            if (department == null)
                SetImportError("Invalid Department", emp, index);

            if (emp.BranchId != null && department != null)
                if (DepartmentManager.IsDepartmentUnderThisBranch(emp.BranchId.Value, department.DepartmentId) == false)
                    SetImportError("Selected Department should come under the same Branch.", emp, index);

            //if( department.BranchId != emp.BranchId)
            //    return SetImportError("Selected Department should come under the same Branch", emp);
            if (department != null)
                emp.DepartmentId = department.DepartmentId;

            //Sub Department
            value = row[ExcelGenerator.employeesStaticColumns[15]];
            if (value != null && value.ToString().Trim() != "")
            {
                SubDepartment subdepartment = subdepartmentList.FirstOrDefault(w => w.HierarchyName.Trim().ToLower() == value.ToString().Trim().ToLower());// .Take(1).SingleOrDefault();


                if (subdepartment == null)
                    SetImportError("Invalid Sub Department", emp, index);
                else
                {
                    if (subdepartment.DepartmentId != emp.DepartmentId)
                        SetImportError("Selected Sub Department should come under the same Department", emp, index);
                    emp.SubDepartmentId = subdepartment.SubDepartmentId;
                }
            }

            //Status
            value = row[ExcelGenerator.employeesStaticColumns[16]];
            if (value == null || value.ToString().Trim() == "")
                SetImportError("Status is required", emp, index);

            try
            {
                string[] statusList = value.ToString().Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                DateTime? prevStatusDate = null;
                //iterate each status
                foreach (string status in statusList)
                {
                    string[] statusValue = status.ToString().Trim().Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                    //if status & date portion doesn't exists
                    if (statusValue.Length != 2 && statusValue.Length != 3)
                    {
                        SetImportError("Invalid status, value & date should be separated by \"_\"", emp,index);
                        continue;
                    }
                    //if Status doesn't exists
                    if (statusNameList.Any(a => a.Value == statusValue[0].Trim()) == false)
                    {
                        string availableStatuses = GetAvailableStatuses(statusNameList);

                        SetImportError(string.Format("Invalid status value \"{0}\", available statuses are \"{1}\",  ", statusValue[0],
                            availableStatuses), emp, index);
                        continue;
                    }


                    CustomDate date = null;
                    CustomDate toDate = null;
                    //if error in date conversion
                    //From Date
                    try
                    {
                        date = CustomDate.GetCustomDateFromString(statusValue[1], ImportIsEnglishDate,ImportDateFormat);
                        // If system and import date doesn't match
                        if (this.IsEnglish == false && ImportIsEnglishDate)
                        {
                            date = CustomDate.ConvertEngToNep(date);
                        }
                    }
                    catch
                    {
                        SetImportError( string.Format("Invalid status date '{0}', should be {1} in the format {2} ",date.ToString(),
                            (ImportIsEnglishDate ? "english" : "nepali" ),
                            ImportDateFormat), emp, index);
                        continue;
                    }
                    // TO Date
                    try
                    {
                        if (statusValue.Length == 3)
                        {
                            toDate = CustomDate.GetCustomDateFromString(statusValue[2], ImportIsEnglishDate, ImportDateFormat);
                            // If system and import date doesn't match
                            if (this.IsEnglish == false && ImportIsEnglishDate)
                            {
                                toDate = CustomDate.ConvertEngToNep(toDate);
                            }
                            //if (IsEnglish != ImportIsEnglishDate)
                            //{
                            //    if (IsEnglish)
                            //        date = CustomDate.ConvertNepToEng(date);
                            //    else
                            //        date = CustomDate.ConvertEngToNep(date);
                            //}
                        }
                    }
                    catch
                    {
                        SetImportError(string.Format("Invalid status date '{0}', should be {1} in the format {2} ", toDate.ToString(),
                            (ImportIsEnglishDate ? "english" : "nepali"),
                            ImportDateFormat), emp, index);
                        continue;
                    }

                    ECurrentStatus dbStatus = new ECurrentStatus();
                    KeyValue keyValue = statusNameList.SingleOrDefault(x => x.Value == statusValue[0].Trim());
                    //if( keyValue != null)
                        dbStatus.CurrentStatus = Convert.ToInt32(keyValue.Key);
                    //else
                    //    dbStatus.CurrentStatus = Convert.ToInt32((JobStatusEnum)Enum.Parse(typeof(JobStatusEnum), statusValue[0].Trim()));
                    
                    dbStatus.FromDate = date.ToString();
                    dbStatus.FromDateEng = GetEngDate(date.ToString());
                    if (toDate != null)
                    {
                        dbStatus.ToDate = toDate.ToString();
                        dbStatus.ToDateEng = GetEngDate(toDate.ToString());
                        dbStatus.DefineToDate = true;
                    }
                    //Validate status date as current should be greater then or equal previous status (equal is reqd for Asian)
                    if (prevStatusDate != null)
                    {
                        if (dbStatus.FromDateEng < prevStatusDate)
                        {
                            SetImportError("Status date order is not correct for " + date.ToString(), emp, index);
                            continue;
                        }
                    }

                    prevStatusDate = dbStatus.FromDateEng;

                    emp.ECurrentStatus.Add(dbStatus);
                }
            }
            catch (Exception exp)
            {
                SetImportError("Status processing error : " + exp.ToString(), emp, index);
            }

            // Contract No (Remaining)

            // Employee ID Card No
            value = row[ExcelGenerator.employeesStaticColumns[18]];
            emp.EHumanResources[0].IdCardNo = value == null ? "" : value.ToString().Trim();
            
            // Official Email, Phone and Ext
            value = row[ExcelGenerator.employeesStaticColumns[19]];
            emp.EAddresses[0].CIEmail = value == null ? "" : value.ToString().Trim();
            if (emp.EAddresses[0].CIEmail != "" && !System.Text.RegularExpressions.Regex.IsMatch(
                emp.EAddresses[0].CIEmail, Utils.Config.EmailAddressRegEx))
            {
                SetImportError("Invalid \"Official Email\"", emp, index);
            }
            
            value = row[ExcelGenerator.employeesStaticColumns[20]];
            emp.EAddresses[0].CIPhoneNo = value == null ? "" : value.ToString();

            value = row[ExcelGenerator.employeesStaticColumns[21]];
            emp.EAddresses[0].Extension = value == null ? "" : value.ToString();
            
            // 19. PF No
            value = row[ExcelGenerator.employeesStaticColumns[22]];
            emp.EFundDetails[0].PFRFNo = value == null ? "" : value.ToString();

            // 20. PAN No
            value = row[ExcelGenerator.employeesStaticColumns[23]];
            if (value != null && value.ToString() != "")
            {
                if (!System.Text.RegularExpressions.Regex.IsMatch(value.ToString(), Utils.Config.PanNoRegExp))
                {
                    SetImportError("PAN No must be 9 digit", emp, index);
                }
                emp.EFundDetails[0].EmployeePan = PAN.HASPAN;
                emp.EFundDetails[0].PANNo = value.ToString();
            }
            else
            {
                emp.EFundDetails[0].EmployeePan = "";
                emp.EFundDetails[0].PANNo = "";
            }
           
            // 21. CIT No
            value = row[ExcelGenerator.employeesStaticColumns[24]];
            emp.EFundDetails[0].CITNo = value == null ? "" : value.ToString();

            // 22. CIT Code
            value = row[ExcelGenerator.employeesStaticColumns[25]];
            emp.EFundDetails[0].CITCode = value == null ? "" : value.ToString();

            // 23. CIT Effective From Date
            emp.EFundDetails[0].HasCIT = true;
            //value = row[ExcelGenerator.employeesStaticColumns[24]];           
            //if (value != null && value.ToString() != "")
            //{
            //    string citEffectiveFromDate = value.ToString();
            //    try
            //    {

            //        CustomDate citDate = null;
            //        if( citEffectiveFromDate.IndexOf("/") != citEffectiveFromDate.LastIndexOf("/"))
            //            citDate = CustomDate.GetCustomDateFromString(citEffectiveFromDate, IsEnglish);
            //        else
            //            citDate =  CustomDate.GetCustomDateFromString("1/" + citEffectiveFromDate, IsEnglish);

            //        emp.EFundDetails[0].CITEffectiveFromMonth = citDate.Month;
            //        emp.EFundDetails[0].CITEffectiveFromYear = citDate.Year;
            //        emp.EFundDetails[0].HasCIT = true;

            //    }
            //    catch
            //    {
            //        return SetImportError(string.Format("Invalid CIT Effective From date '{0}', should be {1} in the format {2} ", citEffectiveFromDate.ToString(),
            //            (IsEnglish ? "english" : "nepali"),
            //            "dd/mm/yyy or mm/yyyy"), emp);
            //    }
            //}
            // 24. CIT Amount/Rate
            value = row[ExcelGenerator.employeesStaticColumns[27]];
            if (value != null && value.ToString().Trim() != "")
            {
                bool isRate = false;
                string amountValue = value.ToString();
                if (amountValue.Contains("%"))
                {
                    isRate = true;
                    amountValue = amountValue.Replace("%", "");
                }
                if (decimal.TryParse(amountValue, out amount))
                {
                    if (isRate)
                    {
                        emp.EFundDetails[0].CITIsRate = true;
                        emp.EFundDetails[0].CITRate = (double)amount;
                    }
                    else
                    {
                        emp.EFundDetails[0].CITIsRate = false;
                        emp.EFundDetails[0].CITAmount = amount;
                    }

                }
                else
                {
                    if(isRate)
                        SetImportError("Invalid CIT Rate", emp, index);
                    else
                        SetImportError("Invalid CIT Amount", emp, index);
                }
            }

            // 25-27. Payment Mode        
            value = row[ExcelGenerator.employeesStaticColumns[28]];
            if (value != null && value.ToString().Trim() != "")
            {
                if (new PaymentMode().GetMembers().Any(a => a.Value == value.ToString().Trim()) == false)
                    SetImportError("Invalid Payment Mode", emp, index);
                emp.PPays[0].PaymentMode = value.ToString();

                value = row[ExcelGenerator.employeesStaticColumns[29]];
                emp.PPays[0].BankName = value == null ? "" : value.ToString().Trim();

                value = row[ExcelGenerator.employeesStaticColumns[30]];
                emp.PPays[0].BankACNo = value == null ? "" : value.ToString().Trim();
                emp.PPays[0].IsCurrencyNotNepali = false;
                if (emp.PPays[0].PaymentMode == PaymentMode.BANK_DEPOSIT)
                {
                    Bank selectedBank = bankList.Where(x => x.Name.ToLower().Trim() == emp.PPays[0].BankName.ToLower().Trim()).FirstOrDefault();
                    if (selectedBank == null)
                    {
                        SetImportError("Selected Bank Name does not exists in the system ", emp, index);
                    }
                    else
                        emp.PPays[0].BankID = selectedBank.BankID;
                }
            }


            // Insurance Prevmium
             value = row[ExcelGenerator.employeesStaticColumns[31]];
             decimal premiumAmount = 0;
             if (value != null && value.ToString().Trim() != "")
             {
                 if (decimal.TryParse(value.ToString().Trim(), out premiumAmount))
                     premiumAmount = decimal.Parse(value.ToString().Trim());
                 else
                 {
                     SetImportError("Invalid insurance premium amount ", emp, index);
                 }
             }

             IIndividualInsurance inc = InsuranceManager.GetDefaultInsuranceEntityForImport(emp.EmployeeId, premiumAmount);
             emp.IIndividualInsurances.Add(inc);
                        

            // Income Columns
            if (!ProcessIncome(emp, incomeList, row,index))
                return emp;

            // Level/Grade
            if (emp.ECurrentStatus.Count > 0)
            {
                if (CommonManager.CompanySetting.HasLevelGradeSalary && EmployeeManager.HasLevelGradeForStatus(emp.ECurrentStatus.OrderByDescending(x => x.FromDateEng).First().CurrentStatus, emp.SkipLevelGradeMatrix))
                {
                    value = row[ExcelGenerator.employeesStaticColumns[32]];
                    if (value == null || string.IsNullOrEmpty(value.ToString()))
                    {
                        SetImportError("Level is required", emp, index);
                    }
                    BLevel level = GetLevelId(levelList, value.ToString());
                    if (level == null)
                        SetImportError("Invalid level", emp, index);

                    value = row[ExcelGenerator.employeesStaticColumns[33]];
                    if (value == null || string.IsNullOrEmpty(value.ToString()))
                    {
                        SetImportError("Grade is required", emp, index);
                    }
                    double grade = 0;
                    if (!double.TryParse(value.ToString(), out grade))
                    {
                        SetImportError("Invalid Grade", emp, index);
                    }
                    //if (levelList.Any(a => a.BLevelGroup.Name.ToLower() + " - " + a.Name.ToLower() == value.ToString().Trim()) == false)
                    //    return SetImportError("Invalid level", emp);

                    PEmployeeIncome empBasicIncome = emp.PEmployeeIncomes.FirstOrDefault(x => x.IncomeId == basicIncome.IncomeId);
                    if (basicIncome != null && level != null)
                    {
                        empBasicIncome.LevelId = level.LevelId;
                        empBasicIncome.Step = grade;


                        // if valid level, then check earlier designation field so that the DesignationId exists in this level or not,
                        // if exists then replace in DesignationId field as other designation id could be set due to same name

                        EDesignation levelDesig = designationList
                           .Where(w =>
                               // Level - Designation Name or Designation Name
                                   (
                                       (w.BLevel.Name + " - " + w.Name).ToLower().Trim() == designationText.ToString().Trim().ToLower() ||
                                       (w.BLevel.Name + "-" + w.Name).ToLower().Trim() == designationText.ToString().Trim().ToLower() ||
                                       (w.Name).ToLower().Trim() == designationText.ToString().Trim().ToLower()
                                    ) && w.LevelId == level.LevelId
                                   )
                           .FirstOrDefault();

                        if (levelDesig == null)
                            SetImportError("Invalid designation, should exists under the selected level for row " + (index + 1) + " ", emp, index);
                        else
                            emp.DesignationId = levelDesig.DesignationId;




                    }
                }
            }

            // Shift
            value = row[ExcelGenerator.employeesStaticColumns[34]];
            if (value != null && value.ToString().Trim() != "")
            {
                EWorkShift shift = shiftList.FirstOrDefault(x => x.Name.ToLower() == value.ToString().ToLower().Trim());
                if (shift == null)
                {
                    SetImportError("Invalid shift", emp, index);
                }
                else
                {
                    EWorkShiftHistory empShift = new EWorkShiftHistory();
                    empShift.WorkShiftId = shift.WorkShiftId;
                    empShift.FromDate = emp.ECurrentStatus[0].FromDate;
                    empShift.FromDateEng = emp.ECurrentStatus[0].FromDateEng;
                    emp.EWorkShiftHistories.Add(empShift);
                }
            }
            else
            {
                // do not force for shift
                //if (BLL.BaseBiz.PayrollDataContext.EWorkShifts.Any())
                //{
                //    SetImportError("Shift is required", emp, index);
                //}
            }

            // Cost Code
            value = row[ExcelGenerator.employeesStaticColumns[35]];
            if (value != null && value.ToString().Trim() != "")
            {
                ECostCode shift = costCodeList.FirstOrDefault(x => x.Name.ToLower() == value.ToString().ToLower().Trim());
                if (shift == null)
                {
                    SetImportError("Invalid Cost Code", emp, index);
                }
                else
                    emp.CostCodeId = shift.CostCodeId;
            }

            // Ethnicity
            value = row["Ethnicity"];
            if (value != null && value.ToString().Trim() != "")
            {
                HCaste caste = ethinicityList.FirstOrDefault(x => x.CasteName.ToLower() == value.ToString().ToLower().Trim());
                if (caste == null)
                {
                    SetImportError("Invalid Ethnicity", emp, index);
                }
                else
                    emp.HHumanResources[0].CasteId = caste.CasteId;
            }

            // Location
            value = row["Location"];
            if (value != null && value.ToString().Trim() != "")
            {
                ELocation location = locationList.FirstOrDefault(x => x.Name.ToLower() == value.ToString().ToLower().Trim());
                if (location == null)
                {
                    SetImportError("Invalid location", emp, index);
                }
                else
                    emp.EHumanResources[0].LocationId = location.LocationId;
            }

            // GradeStep
            value = row["GradeStep"];
            if (value != null && value.ToString().Trim() != "")
            {
                EGrade grade = gradeStepList.FirstOrDefault(x => x.Name.ToLower() == value.ToString().ToLower().Trim());
                if (grade == null)
                {
                    SetImportError("Invalid Grade/Step", emp, index);
                }
                else
                    emp.GradeId = grade.GradeId;
            }

            // Unit
            value = row["Unit"];
            if (unitList.Any())
            {
                if (value != null && value.ToString().Trim() != "")
                {
                    UnitList unit = unitList.FirstOrDefault(x => x.Name.ToLower() == value.ToString().ToLower().Trim());
                    if (unit == null)
                    {
                        SetImportError("Invalid unit", emp, index);
                    }
                    else
                        emp.UnitId = unit.UnitId;
                }
                else
                    SetImportError("Unit blank", emp, index);
            }
            if (!ProcessDeduction(emp, deductionList, row, index))
                return emp;

            if (!ProcessLeave(emp, leaveList, row,index))
                return emp;

            return emp;
        }
        public BLevel GetLevelId(List<BLevel> levelList, string value)
        {
            foreach (BLevel level in levelList)
            {
                if (
                    (level.GroupName.ToLower() + " - " + level.Name.ToLower()).Equals(value.ToLower().Trim())
                    || (level.GroupName.ToLower() + "-" + level.Name.ToLower()).Equals(value.ToLower().Trim())
                    || level.Name.ToLower().Equals(value.ToLower().Trim())
                    
                    )
                {
                    return level;
                }
            }
            return null;
        }
        public bool ProcessIncome(EEmployee emp, List<PIncome> incomeList, DataRow row,int index)
        {
            // sort Basic to be first
            incomeList = incomeList.OrderByDescending(x => x.IsBasicIncome.ToString())
                .ToList();

            object value; decimal amount;
            foreach (PIncome income in incomeList)
            {
                value = row[income.Title + " Income"];

                if (value == null || value.ToString().Trim() == "")
                {
                    if (income.IsBasicIncome)
                    {
                        SetImportError(income.Title + " Income is required", emp, index);
                        return false;   
                    }
                    else
                        continue; //for other skip the Income 
                }

                if (!decimal.TryParse(value.ToString().Trim(), out amount))
                {
                    switch (income.Calculation)
                    {
                        case IncomeCalculation.VARIABLE_AMOUNT:
                        case IncomeCalculation.FIXED_AMOUNT:
                            SetImportError(string.Format("Invalid amount for {0} income", income.Title), emp, index);
                            return false;
                        case IncomeCalculation.PERCENT_OF_INCOME:
                        case IncomeCalculation.Unit_Rate:
                            SetImportError(string.Format("Invalid rate for {0} income", income.Title), emp, index);
                            return false;
                        case IncomeCalculation.FESTIVAL_ALLOWANCE:

                            if (income.IsVariableAmount.HasValue && income.IsVariableAmount.Value)
                            {
                                SetImportError(string.Format("Invalid amount for {0} income", income.Title), emp, index);
                                return false;
                            }
                            else
                            {
                                SetImportError(string.Format("Invalid rate for {0} income", income.Title), emp, index);
                                return false;
                            }
                    }
                }

                PEmployeeIncome empIncome = new PEmployeeIncome();
                empIncome.EditSequence = 1;
                empIncome.IsForeignAllowance = false;
                empIncome.IsEnabled = true;
                empIncome.IncomeId = income.IncomeId;
                empIncome.IsValid = true;
                switch (income.Calculation)
                {
                    case IncomeCalculation.VARIABLE_AMOUNT:
                    case IncomeCalculation.FIXED_AMOUNT:
                        empIncome.Amount = amount;
                        break;
                    case IncomeCalculation.PERCENT_OF_INCOME:
                    case IncomeCalculation.Unit_Rate:
                        empIncome.RatePercent = (double)amount;
                        break;
                    case IncomeCalculation.FESTIVAL_ALLOWANCE:

                        if (income.IsVariableAmount.HasValue && income.IsVariableAmount.Value)
                            empIncome.Amount = amount;
                        else
                            empIncome.RatePercent = (double)amount;
                        break;
                }
                emp.PEmployeeIncomes.Add(empIncome);
            }
            return true;
        }

        public bool ProcessDeduction(EEmployee emp,List<PDeduction> deductionList,DataRow row,int index)
        {
            object value; decimal amount;
            //Deduction Columns
            foreach (PDeduction ded in deductionList)
            {
                value = row[ded.Title + " Deduction"];

                if (value == null || value.ToString().Trim() == "")
                {
                    continue; //for other skip the Income 
                }

                if (!decimal.TryParse(value.ToString().Trim(), out amount))
                {
                    switch (ded.Calculation)
                    {
                        case DeductionCalculation.VARIABLE:
                        case DeductionCalculation.FIXED:
                            SetImportError(string.Format("Invalid amount for {0} deduction", ded.Title), emp, index);
                            return false;
                        case DeductionCalculation.PERCENT:
                            SetImportError(string.Format("Invalid rate for {0} deduction", ded.Title), emp, index);
                            return false;

                    }
                }

                PEmployeeDeduction empDeduction = new PEmployeeDeduction();
                empDeduction.EditSequence = 1;
                empDeduction.DeductionId = ded.DeductionId;
                empDeduction.IsValid = true;
                switch (ded.Calculation)
                {
                    case IncomeCalculation.VARIABLE_AMOUNT:
                    case IncomeCalculation.FIXED_AMOUNT:
                        empDeduction.Amount = amount;
                        break;
                    case IncomeCalculation.PERCENT_OF_INCOME:
                        empDeduction.Rate = (double)amount;
                        break;

                }
                emp.PEmployeeDeductions.Add(empDeduction);
            }

            return true;
        }



        public bool ProcessLeave(EEmployee emp, List<LLeaveType> leaveList, DataRow row, int index)
        {
            object value; decimal amount;
            //Deduction Columns
            foreach (LLeaveType leave in leaveList)
            {
                value = row[leave.Title + " Leave"];

                if (value == null || value.ToString().Trim() == "")
                {
                    continue; //for other skip the Income 
                }

                bool canHaveCurrentYearBalance = leave.FreqOfAccrual == LeaveAccrue.YEARLY;

                LEmployeeLeave empLeave = new LEmployeeLeave();

                if (value.ToString().Contains("_"))
                {
                    string[] values = value.ToString().Split(new char[]{'_'});

                    if (!decimal.TryParse(values[0], out amount)
                        || !decimal.TryParse(values[1], out amount))
                    {
                        SetImportError(string.Format("Invalid value for {0} leave", leave.Title), emp,index);
                        return false;
                    }

                    empLeave.BeginningBalance = double.Parse(values[0]);
                    empLeave.CurrentYearBalance = double.Parse(values[1]);
                    if (values.Length >= 3)
                        empLeave.OpeningTaken = double.Parse(values[2]);
                }
                else
                {
                    if (!decimal.TryParse(value.ToString().Trim(), out amount))
                    {
                        SetImportError(string.Format("Invalid value for {0} leave", leave.Title), emp,index);
                        return false;
                    }

                    empLeave.BeginningBalance = (double)amount;
                }

                




               
                empLeave.EditSequence = 1;
                empLeave.LeaveTypeId = leave.LeaveTypeId;
            
                empLeave.Accured = 0;
                empLeave.IsOpeningBalanceSaved = true;
                empLeave.IsActive = true;



                emp.LEmployeeLeaves.Add(empLeave);
            }

            return true;
        }

        public string ValidatePFCITPANBankNo(List<EEmployee> list, EEmployee newEmp)
        {
            string pfNo = newEmp.EFundDetails[0].PFRFNo == null ? "" : newEmp.EFundDetails[0].PFRFNo.Trim().ToLower();
            string citNo = newEmp.EFundDetails[0].CITNo == null ? "" : newEmp.EFundDetails[0].CITNo.Trim().ToLower();
            string panNo = newEmp.EFundDetails[0].PANNo == null ? "" : newEmp.EFundDetails[0].PANNo.Trim().ToLower();
            string bankNo = newEmp.PPays[0].BankACNo == null ? "" : newEmp.PPays[0].BankACNo.Trim().ToLower();
            

            if (!string.IsNullOrEmpty(pfNo))
            {
                foreach (EEmployee emp in list)
                {
                    EFundDetail otherEmp = emp.EFundDetails.Where(x => x.PFRFNo.Trim().ToLower() == pfNo)
                        .FirstOrDefault();
                    if (otherEmp != null)
                    {
                        return
                            "PF No \"" + newEmp.EFundDetails[0].PFRFNo + "\" already exists between employees " + otherEmp.EEmployee.Name + " and " + newEmp.Name + ".";
                    }
                }
            }

            if (!string.IsNullOrEmpty(citNo))
            {
                foreach (EEmployee emp in list)
                {
                    EFundDetail otherEmp = emp.EFundDetails.Where(x => x.CITNo.Trim().ToLower() == citNo)
                        .FirstOrDefault();
                    if (otherEmp != null)
                    {
                        return
                            "CIT No \"" + newEmp.EFundDetails[0].CITNo + "\" already exists between employees " + otherEmp.EEmployee.Name + " and " + newEmp.Name + ".";
                    }
                }
            }

            if (!string.IsNullOrEmpty(panNo))
            {
                foreach (EEmployee emp in list)
                {
                    EFundDetail otherEmp = emp.EFundDetails.Where(x => x.PANNo.Trim().ToLower() == panNo)
                        .FirstOrDefault();
                    if (otherEmp != null)
                    {
                        return
                            "PAN No \"" + newEmp.EFundDetails[0].PANNo + "\" already exists between employees " + otherEmp.EEmployee.Name + " and " + newEmp.Name + ".";
                    }
                }
            }

            if (!string.IsNullOrEmpty(bankNo))
            {
                foreach (EEmployee emp in list)
                {
                    PPay otherEmp = emp.PPays.Where(x => x.BankACNo != null && x.BankACNo.Trim().ToLower() == bankNo)
                        .FirstOrDefault();
                    if (otherEmp != null)
                    {
                        return
                            "Bank AC Number \"" + newEmp.PPays[0].BankACNo + "\" already exists between employees " + otherEmp.EEmployee.Name + " and " + newEmp.Name + ".";
                    }
                }
            }
            return "";

        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            List<EDesignation> designationList = mgr.GetAllDesignations();
            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            List<Department> departmentList = new DepartmentManager().GetDepartmentsByCompany(SessionManager.CurrentCompanyId);
            List<SubDepartment> subdepartmentList = new DepartmentManager().GetSubDepartmentsByCompany(SessionManager.CurrentCompanyId);
            List<PIncome> incomeList = PayManager.GetIncomeListByCompanyForImportEmployee(SessionManager.CurrentCompanyId);
            List<PDeduction> deductionList = PayManager.GetDeductionListByCompanyForEmployeeImport(SessionManager.CurrentCompanyId);
            List<LLeaveType> leaveList = LeaveAttendanceManager.GetLeaveListByCompany(SessionManager.CurrentCompanyId)
                .Where(x => x.IsParentGroupLeave == null || x.IsParentGroupLeave == false).ToList();
            List<Bank> bankList = PayManager.GetBankList();
            List<KeyValue> statusList = new JobStatus().GetMembers();
            List<BLevel> levelList = NewPayrollManager.GetAllParentLevelList().ToList();
            PIncome basicIncome = new PayManager().GetBasicIncome(SessionManager.CurrentCompanyId);
            List<EWorkShift> shifts = ShiftManager.GetAllShifts();
            List<ECostCode> costCodeList = CommonManager.GetAllCostCodes();
            List<UnitList> unitList = CommonManager.GetAllUnitList();

            divWarningMsg.InnerHtml = "";

            if (fupUpload.HasFile)
            {
                fupUpload.SaveAs(template);


                try
                {

                    DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Employee_Info$]");

                    if (datatable == null)
                    {
                        divWarningMsg.InnerHtml = "Sheet name has been changed, should be \"Employee_Info\".";
                        divWarningMsg.Hide = false;

                        File.Delete(template);
                        return;
                    }

                    List<EEmployee> employeeList = new List<EEmployee>();
                    bool anyError = false;
                    int index = 0;
                    foreach (DataRow row in datatable.Rows)
                    {

                        EEmployee emp = GetImportedEmployee(employeeList,designationList, branchList, departmentList, subdepartmentList, incomeList,
                            deductionList, leaveList, row, datatable, statusList, bankList, ++index, levelList, basicIncome, shifts, costCodeList,unitList);

                        if (emp == null)
                            continue;

                        string pfCITPANNo = ValidatePFCITPANBankNo(employeeList, emp);
                        if (!string.IsNullOrEmpty(pfCITPANNo))
                        {
                            SetImportErrorWithoutAppend(pfCITPANNo, emp, index);
                        }

                        if (emp.IsValidImport)
                        {
                            emp.CompanyId = SessionManager.CurrentCompanyId;
                            emp.EditSequence = 1;
                            emp.IsLocalEmployee = false;
                            emp.IsNonResident = false;
                            emp.IsRetired = false;
                            emp.IsResigned = false;
                            emp.Created = BLL.BaseBiz.GetCurrentDateAndTime();
                            emp.Modified = BLL.BaseBiz.GetCurrentDateAndTime();

                                                        
                         
                          
                            //set first status date                            
                            emp.EHumanResources[0].SalaryCalculateFrom = emp.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDate;
                            emp.EHumanResources[0].SalaryCalculateFromEng = emp.ECurrentStatus.OrderBy(x => x.FromDateEng).First().FromDateEng;                            


                        
                            employeeList.Add(emp);

                        }
                        if (emp.IsValidImport == false)
                        {
                            anyError = true;
                            divWarningMsg.InnerHtml += "<br/>" + emp.ImportErrorMsg;
                            divWarningMsg.Hide = false;                           
                        }

                    }

                    if (anyError == false)
                    {
                        Status status = EmployeeManager.SaveImportedEmployee(employeeList);
                        if (status.IsSuccess)
                        {
                            divMsgCtl.InnerHtml = string.Format("{0} employees imported successfully.", employeeList.Count);
                            divMsgCtl.Hide = false;
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = status.ErrorMessage;
                            divWarningMsg.Hide = false;
                        }
                    }

                }




                   //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                         exp.ColumnName, "") + " : " + exp.ToString();
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError) + " : " + exp.ToString();
                    divWarningMsg.Hide = false;
                }
                //catch (ArgumentException exp)
                //{
                //    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError) + " : " + exp.ToString();
                //    divWarningMsg.Hide = false;
                //}
                //catch (CompulsoryImportFieldException exp1)
                //{
                //    divMsgCtl.Text = exp1.Message;
                //}

                finally
                {
                    File.Delete(template);
                }

                this.HasImport = true;
            }


        }

    }
}

   


