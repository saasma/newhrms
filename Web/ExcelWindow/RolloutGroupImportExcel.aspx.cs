﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;
using System.Data.Linq;

namespace Web.CP
{
    public partial class RolloutGroupImportExcel : BasePage
    {
        public string GroupId;
        public string EmpId;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["EmpID"]))
                EmpId = Request.QueryString["EmpID"];
            if (!string.IsNullOrEmpty(Request.QueryString["GroupId"]))
                GroupId = Request.QueryString["GroupId"];
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/EmpRolloutGroupImport.xlsx");
            List<EEmployee> employees = EmployeeManager.GetAllActiveEmployees();
            List<AppraisalRolloutGroup> GroupList= AppraisalManager.GetAllRolloutGroup();
            int empID = -1;
            int groupID = -1;

            if (!string.IsNullOrEmpty(EmpId))
            {
                empID = int.Parse(EmpId);
            }
            if (!string.IsNullOrEmpty(GroupId))
            {
                groupID = int.Parse(GroupId);
            }
            List<GetEmployeeAppraisalRolloutGroupListResult> list = AppraisalManager.GetEmployeeAppraisalRolloutGroup(0, 999999, empID, "", groupID);
            ExcelGenerator.ExportEmployeeAppraisalRolloutGroup(template, list, GroupList);
        }

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID();
        }




        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);
                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");
                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }
                int employeeId;
                int groupId;
                List<int> employeeIDList = GetEmployeeIdList();
                List<int> addedEmployeeIDList = new List<int>();
                //List<int> GroupList = AppraisalManager.GetAllRolloutGroup();
                int importedCount = 0;
                int employeeCount = 0;

                try
                {
                    List<AppraisalRolloutGroupEmployee> list = new List<AppraisalRolloutGroupEmployee>();
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == true)
                        {
                            if (addedEmployeeIDList.Contains(employeeId))
                            {
                                divWarningMsg.InnerHtml = "Employee ID " + employeeId + " has been repeated in the excel.";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            else
                                addedEmployeeIDList.Add(employeeId);

                            rowNumber += 1;

                            if (row["Group Name"].Equals(DBNull.Value))
                            {
                                divWarningMsg.InnerHtml = "Group Name can't be empty " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            groupId = EmployeeManager.GetRolloutGroupIdByName(row["Group Name"].ToString());

                            if (groupId.ToString() == null && groupId.ToString() == "")
                            {
                                divWarningMsg.InnerHtml = "Invalid Rollout Group Name in row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            importedCount += 1;
                            employeeCount += 1;

                            AppraisalRolloutGroupEmployee _dbobj = new AppraisalRolloutGroupEmployee();
                            _dbobj.EmployeeID = employeeId;
                            _dbobj.RolloutGroupRef_ID = groupId;

                            list.Add(_dbobj);
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                    }
                    Status status = AppraisalManager.SaveExcelSheetEmpRolloutGroup(list);
                    if (status.IsSuccess == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }
                    divMsgCtl.InnerHtml = string.Format("{0} Records has been imported", employeeCount);
                    divMsgCtl.Hide = false;
                    this.HasImport = true;
                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value, exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                divWarningMsg.InnerHtml = "Excel version should be 2007(.xlsx) or higher.";
                divWarningMsg.Hide = false;
            }
        }
    }
}

