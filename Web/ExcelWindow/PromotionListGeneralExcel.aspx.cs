﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class PromotionListGeneralExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       
        

        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/PromotionImport.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            //List<DAL.GetForDeviceMappingResult> list = TaxManager.GetForDeviceMapping("", 0, 999999, ref total);

            List<DAL.BLevel> list1 = new List<BLevel>();
            List<Grade> list2 = new List<Grade>();
            List<EDesignation> list3 = new List<EDesignation>();

            list1 = NewPayrollManager.GetAllParentLevelList();
            list2 = PayManager.GetAllGrade();
            list3 = new CommonManager().GetAllDesignations();

            ExcelGenerator.ExportPromotionList(template, list1 ,list3);
        }

        protected void btnExportWithData_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/PromotionImport.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            //List<DAL.GetForDeviceMappingResult> list = TaxManager.GetForDeviceMapping("", 0, 999999, ref total);

            List<DAL.BLevel> list1 = new List<BLevel>();
            List<EmployeeLevelGradeBO> list2 = new List<EmployeeLevelGradeBO>();
            List<EDesignation> list3 = new List<EDesignation>();

            list1 = NewPayrollManager.GetAllParentLevelList();
            list2 = NewPayrollManager.GetEmployeeCurrentLevelAndGradeForExportPromotion();
            list3 = new CommonManager().GetAllDesignations();

            ExcelGenerator.ExportPromotionWithCurrentLevel(template, list1 ,list3,list2);
        }
        

        private List<int> GetEmployeeIdList()
        {
            return NewPayrollManager.GetEmployeeCurrentLevelAndGradeForExportPromotion().Select(x => x.EIN).ToList();
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            LevelGradeChange change = new LevelGradeChange();
            
            change.Type = (int)LevelGradeChangeType.Promotion;


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<LevelGradeChangeDetail> claimLine = new List<LevelGradeChangeDetail>();
                LevelGradeChangeDetail claimInst = new LevelGradeChangeDetail();

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");

                //int employeeId;
                //string deviceId;
                //object value;
                //object excludeValue;

                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    string LevelName;
                    string GradeName;
                    string DesignationName;
                    BLevel level = new BLevel();
                    Grade grade = new Grade();
                    EDesignation desig = new EDesignation();

                    foreach (DataRow row in datatable.Rows)
                    {
                        if (!string.IsNullOrEmpty(row["EIN"].ToString()))
                        {
                            claimInst = new LevelGradeChangeDetail();



                            rowNumber += 1;
                            //int.TryParse(row["EIN"].ToString(), out employeeId);



                            importedCount += 1;


                            try
                            {
                                claimInst.EmployeeId = int.Parse(row["EIN"].ToString().Trim());
                            }
                            catch (Exception exp)
                            {
                                divWarningMsg.InnerHtml = "Invalid Employee ID At :" + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            if (employeeIDList.Contains(claimInst.EmployeeId.Value) == false)
                            {
                                divWarningMsg.InnerHtml = "Employee ID " + claimInst.EmployeeId.ToString() +  " in the row " + rowNumber + " is not of level type, is being changed from contract use search.";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(row["From Date"].ToString()))
                                {
                                    if (SessionManager.IsEnglish)
                                    {
                                        claimInst.FromDateEng = DateTime.Parse(row["From Date"].ToString());
                                        claimInst.FromDate = BLL.BaseBiz.GetAppropriateDate(claimInst.FromDateEng.Value);
                                    }
                                    else
                                    {

                                        claimInst.FromDate = row["From Date"].ToString();
                                        claimInst.FromDateEng = GetEngDate(claimInst.FromDate);
                                    }
                                }
                            }
                            catch (Exception exp)
                            {
                                divWarningMsg.InnerHtml = "Invalid Date ID At :" + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            if (!string.IsNullOrEmpty(row["New Level"].ToString()))
                            {
                                LevelName = row["New Level"].ToString();
                                level = CommonManager.getLevelByName(LevelName);

                                if (level != null)
                                {
                                    claimInst.LevelId = level.LevelId;
                                }
                                else
                                {
                                    divWarningMsg.InnerHtml = "Invalid Level At :" + rowNumber;
                                    divWarningMsg.Hide = false;
                                    return;
                                }

                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(row["New Grade"].ToString()))
                                {

                                    GradeName = row["New Grade"].ToString();

                                    claimInst.StepGrade = double.Parse(GradeName);
                                    //grade = CommonManager.getGradeByName(GradeName);

                                    //if (grade != null)
                                    //{
                                    //    claimInst.StepGrade = grade.GradeId;
                                    //}

                                }
                            }
                            catch (Exception ex)
                            {

                                divWarningMsg.InnerHtml = "Invalid Grade At :" + rowNumber;
                                divWarningMsg.Hide = false;
                                return;

                            }

                            if (!string.IsNullOrEmpty(row["New Designation"].ToString()))
                            {

                                DesignationName = row["New Designation"].ToString();
                                desig = CommonManager.getDesignationByName(DesignationName, claimInst.LevelId.Value);

                                if (desig != null)
                                {
                                    claimInst.DesignationId = desig.DesignationId;
                                }
                                else
                                {
                                    divWarningMsg.InnerHtml = "Invalid Designation At :" + rowNumber;
                                    divWarningMsg.Hide = false;
                                    return;
                                }
                            }


                            claimInst.Type = (int)LevelGradeChangeType.Promotion;
                            claimInst.Status = (int)LevelGradeChangeStatusType.Saved;

                            claimInst.StatusChangeOn = CommonManager.GetCurrentDateAndTime();
                            claimInst.StatusChangeBy = SessionManager.CurrentLoggedInUserID;

                            //claimInst.SequenceNo = (rowNumber++);
                            //change.LevelGradeChangeDetails.Add(claimInst);

                            claimLine.Add(claimInst);
                        }



                    }

                    if (claimLine != null)
                    {
                        //Session["test"] = myList;
                        //var list = (List<int>)Session["test"];

                        Session["PromotionList"] = claimLine;
                        JavascriptHelper.DisplayClientMsg("Promotion Chnage has temprorarily been saved to the grid", this, "closePopup();\n");

                    }

                    //bool isInsert = true;

                    //Status status = SalaryManager.InsertUpdatePromotion(change, ref isInsert);

                    //if (status.IsSuccess)
                    //{

                    //}
                    //else
                    //{
                    //    divMsgCtl.InnerHtml = status.ErrorMessage;
                    //    divMsgCtl.Hide = false;
                    //}



                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

