﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class OTTotalHourExcel : BasePage
    {
        int overtimeId;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            overtimeId = int.Parse(Request.QueryString["id"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/OTTotalHourImport.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
           // List<DAL.GetForTaxHistoryResult> list = TaxManager.GetForTaxHistory("", 0, 999999, ref total);

            List<GetGeneratedOvertimeListResult> list = CommonManager.GetOvertimeList(overtimeId
                  , 0, 99999, ref total, ""
                  , 1, -1,-1);

            ExcelGenerator.ExportOTTotalHour(template, list);

        }


        

        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            List<GetGeneratedOvertimeListResult> list = CommonManager.GetOvertimeList(overtimeId
                  , 0, 99999, ref total, ""
                  , 1, -1, -1);
            List<int> ids = new List<int>();
            foreach (GetGeneratedOvertimeListResult item in list)
            {
                ids.Add(item.EIN);
            }
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            bool status = CommonManager.IsOvertimeEditable(overtimeId);

            if (status == false)
            {
                divWarningMsg.InnerHtml = "Overtime already distributed, can not be changed.";
                divWarningMsg.Hide = false;
                return;
            }


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                int employeeId;
                decimal? adjustment;// grossAmount,pf,cit,insurance,sst,tds;
               
                List<int> employeeIDList = GetEmployeeIdList();
                string empList = "";
                int importedCount = 0;

                List<GetGeneratedOvertimeListResult> list = new List<GetGeneratedOvertimeListResult>();

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);
                        
                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                            //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            //divWarningMsg.Hide = false;
                            //return;
                        }



                        adjustment = ExcelGenerator.GetAmountValueFromCell("Total OT Hrs", row, employeeId);
                        //Days_At_Kirne = ExcelGenerator.GetValueFromCell("Days_At_Kirne", row, employeeId);

                        if (empList == "")
                            empList = employeeId.ToString();
                        else
                            empList += "," + employeeId.ToString();


                        GetGeneratedOvertimeListResult item = new DAL.GetGeneratedOvertimeListResult();
                        item.EIN = employeeId;
                        item.NewOTHour = adjustment == null ? 0 : (double)adjustment;


                        list.Add(item);
                        
                        importedCount += 1;

                    }

                    if (importedCount > 0)
                    {
                        LeaveAttendanceManager.UpdateOTTotalHours(list, overtimeId);

                       
                    }


                    //xml.Append("</root>");

                    //TaxManager.SaveForTax(xml.ToString());

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

