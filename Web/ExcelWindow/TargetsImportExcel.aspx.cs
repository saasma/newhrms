﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;

namespace Web.CP
{
    public partial class TargetsImportExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public string strEmployee = "";
        public bool isDeduction = false;
        public List<int> incomeIDs = new List<int>();
        int month = 0;
        int year = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(Request.QueryString["EmpID"]))
                strEmployee = Request.QueryString["EmpID"];

            month = int.Parse(Request.QueryString["month"]);
            year = int.Parse(Request.QueryString["year"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/TargetsImport.xlsx");
            List<PIncome> incomes = PayManager.GetVariableIncomes(incomeIDs,isDeduction);
            List<EEmployee> employees = EmployeeManager.GetAllActiveEmployees();

            if (!string.IsNullOrEmpty(strEmployee))
            {
                employees = employees.Where(X => X.EmployeeId == int.Parse(strEmployee)).ToList();
            }

            //PayManager.GetEmployeeForVariableIncomes(strIncomeID,isDeduction);
          // List<GetEmployeesIncomeForVariableIncomesResult> incomeList = PayManager.GetEmployeesIncomeForVariableIncomes(strIncomeID,isDeduction); ;
            List<AppraisalEmployeeTarget> _AppraisalEmployeeTarget = AppraisalManager.GetAppraisalEmployeeTargets(month,year);

            if (!string.IsNullOrEmpty(strEmployee))
            {
                _AppraisalEmployeeTarget = _AppraisalEmployeeTarget.Where(x => x.EmployeeId == int.Parse(strEmployee)).ToList();
                    
            }

            List<AppraisalTarget> _AppraisalList = AppraisalManager.GetAllAppraisalTargets();
            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteVariableTargetsForExportExcel(template,_AppraisalList,employees, _AppraisalEmployeeTarget);
        }

        private List<int> GetEmployeeIdList()
        {
          return EmployeeManager.GetAllEmployeeID();
        }

        
        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

            
                int employeeId;
                decimal? value;
                //float? Days_At_Palati, Days_At_Kirne, On_Call, Shift_Afternoon, Shift_Night, Split_Shift_Morning, Split_Shift_Evening, OT_150, OT_200, OT_300, Public_Holiday,
                //    Other_Leave, Snacks_Allowance, Special_Compensation_Allowance, Meal_Allowance, TADA, Vehicle_Allowance;
               
                List<int> employeeIDList = GetEmployeeIdList();
                List<int> addedEmployeeIDList = new List<int>();
                int importedCount = 0;
                int totalMismatchEmployeeCount = 0;
                int employeeCount = 0;

                try
                {
                    List<AppraisalEmployeeTarget> _AppraisalEmployeeTarget = new List<AppraisalEmployeeTarget>();
                    List<AppraisalTarget> _AppraisalTarget = AppraisalManager.GetAllAppraisalTargets();


                    string name = "";
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false
                             && ( row["Employee"].Equals(DBNull.Value) || string.IsNullOrEmpty(row["Employee"].ToString()))
                            )
                        {
                            continue;
                        }

                        if (addedEmployeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Employee ID " + employeeId + " has been repeated in the excel.";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                            addedEmployeeIDList.Add(employeeId);

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        name = row["Employee"].ToString();

                        // for custom role only allow to import permissible emp only
                        if (SessionManager.IsCustomRole && employeeIDList.Contains(employeeId)==false)
                        {
                            continue;
                        }

                        importedCount += 1;
                        employeeCount += 1;
                        foreach (var _Targets in _AppraisalTarget)
                        {
                            value = ExcelGenerator.GetAmountValueFromCell(_Targets.Name, row, employeeId);
                            if (value == null)
                                value = (decimal)0;
                            if (value != null)
                            {

                                AppraisalEmployeeTarget _AppraisalEmployee = new AppraisalEmployeeTarget();
                                _AppraisalEmployee.EmployeeId = employeeId;
                                _AppraisalEmployee.TargetID = _Targets.TargetID;
                                _AppraisalEmployee.Value = (double)value;
                                _AppraisalEmployee.Year = year;
                                _AppraisalEmployee.Month = month;

                               
                                {
                                    if (value.Value < 0 && isDeduction == true)
                                    {
                                        divWarningMsg.InnerHtml = string.Format("Employee {0} contains negative value, so please correct it & import the excel.", name);
                                        divWarningMsg.Hide = false;
                                        return;
                                    }
                                }

                                _AppraisalEmployeeTarget.Add(_AppraisalEmployee);
                            }
                        }



                    }

                    int count = 0;
                    ResponseStatus status = AppraisalManager.SaveVariableAppraisalTarget(_AppraisalEmployeeTarget, isDeduction, ref count,month,year);

                    if (status.IsSuccessType == false)
                    {

                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }


                    divMsgCtl.InnerHtml = string.Format("{0} Records has been imported",
                                                            employeeCount);
                    divMsgCtl.Hide = false;
                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
            else
            {
                divWarningMsg.InnerHtml = "Excel version should be 2007(.xlsx) or higher.";
                divWarningMsg.Hide = false;
            }
        }
        
     
       
    }

   
}

