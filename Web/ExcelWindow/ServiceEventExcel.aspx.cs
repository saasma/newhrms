﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using Ext.Net;

namespace Web.CP
{
    public partial class ServiceEventExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       
        

        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/ServiceHistoryImport.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            //List<DAL.GetForDeviceMappingResult> list = TaxManager.GetForDeviceMapping("", 0, 999999, ref total);

            List<HR_Service_Event> eventList = CommonManager.GetServieEventTypes().OrderBy(x => x.Name).ToList();

            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList();
            List<Department> depList = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            List<EDesignationBO> desigList = CommonManager.GetDesignationLevelList().OrderBy(x => x.Name).ToList();

            JobStatus status = new JobStatus();
            List<KeyValue> list = status.GetMembers();
            list.RemoveAt(0);


            ExcelGenerator.ExportServiceHistoryList(template, eventList, branchList, depList, desigList, list);

        }

        

       

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            List<HR_Service_Event> eventList = CommonManager.GetServieEventTypes().OrderBy(x => x.Name).ToList();

            List<Branch> branchList = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).OrderBy(x => x.Name).ToList();
            List<Department> depList = DepartmentManager.GetAllDepartments().OrderBy(x => x.Name).ToList();
            List<EDesignationBO> desigList = CommonManager.GetDesignationLevelList().OrderBy(x => x.Name).ToList();

            JobStatus status = new JobStatus();
            List<KeyValue> statusList = status.GetMembers();
            statusList.RemoveAt(0);


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<EmployeeServiceHistory> list = new List<EmployeeServiceHistory>();



                StringBuilder xml = new StringBuilder();
               


              
                int importedCount = 0;

                try
                {
                    int rowNumber = 0;

                    string eventName = "";
                    HR_Service_Event se = null;
                    string branch = "";
                    string dep = "";
                    string desig = "";
                    string serviceStatus = "";
                    Branch entityBranch = null;
                    Department entityDep = null;
                    EDesignationBO entityDesig = null;
                    KeyValue entityStatus = null;

                    foreach (DataRow row in datatable.Rows)
                    {
                        EmployeeServiceHistory history = new EmployeeServiceHistory();

                        if (!string.IsNullOrEmpty(row["EIN"].ToString()))
                        {
                         
                            rowNumber += 1;
                            //int.TryParse(row["EIN"].ToString(), out employeeId);



                            importedCount += 1;


                            try
                            {
                                history.EmployeeId = int.Parse(row["EIN"].ToString().Trim());
                            }
                            catch (Exception exp)
                            {
                                divWarningMsg.InnerHtml = "Invalid EIN at row :" + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            eventName = row["Event"].ToString().Trim();
                            se = eventList.FirstOrDefault(x => x.Name.ToLower() == eventName.ToLower());
                            if (se == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid event name " + eventName + " at row :" + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }
                            history.EventID =(short) se.EventID;

                            try
                            {
                                history.DateEng = Convert.ToDateTime(row["Eng From Date"]);
                                history.Date = BLL.BaseBiz.GetAppropriateDate(history.DateEng);

                            }
                            catch (Exception exp)
                            {
                                divWarningMsg.InnerHtml = "Invalid From Date at row :" + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(row["Eng To Date"].ToString()))
                                {
                                    history.ToDateEng = Convert.ToDateTime(row["Eng To Date"].ToString());
                                    history.ToDate = BLL.BaseBiz.GetAppropriateDate(history.ToDateEng.Value);
                                }
                            }
                            catch (Exception exp)
                            {
                                divWarningMsg.InnerHtml = "Invalid To Date at row :" + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            history.LetterNo = row["Letter No"].ToString().Trim();
                            try
                            {
                                if (!string.IsNullOrEmpty(row["Eng Letter Date"].ToString()))
                                {
                                    history.LetterDateEng = Convert.ToDateTime(row["Eng Letter Date"]);
                                    history.LetterDate = BLL.BaseBiz.GetAppropriateDate(history.LetterDateEng.Value);
                                }
                            }
                            catch (Exception exp)
                            {
                                divWarningMsg.InnerHtml = "Invalid Letter Date at row :" + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }

                            branch = row["Branch"].ToString().Trim();
                            if (!string.IsNullOrEmpty(row["Branch"].ToString()))
                            {
                                
                                entityBranch = branchList.FirstOrDefault(x => x.Name.ToLower() == branch.ToLower());

                                if (entityBranch == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid branch name " + branch + " at row :" + rowNumber;
                                    divWarningMsg.Hide = false;
                                    return;
                                }
                                history.BranchId = entityBranch.BranchId;
                            }

                            if (!string.IsNullOrEmpty(row["Department"].ToString()))
                            {
                                dep = row["Department"].ToString().Trim();
                                entityDep = depList.FirstOrDefault(x => x.Name.ToLower() == dep.ToLower());

                                if (entityDep == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid department name " + dep + " at row :" + rowNumber;
                                    divWarningMsg.Hide = false;
                                    return;
                                }
                                history.DepartmentId = entityDep.DepartmentId;

                                if (history.BranchId != null && CommonManager.CheckIfDepartmentExistsForBranch(history.BranchId.Value, history.DepartmentId.Value))
                                {
                                    divWarningMsg.InnerHtml = "Department " + dep + " does not come under the branch " + branch +  " at row :" + rowNumber;
                                    divWarningMsg.Hide = false;
                                    return;
                                }

                            }
                                
                            desig = row["Position - Designation"].ToString().Trim();
                            if (!string.IsNullOrEmpty(row["Position - Designation"].ToString()))
                            {

                                entityDesig = desigList.FirstOrDefault(x => x.LevelAndDesignation.ToLower() == desig.ToLower());

                                if (entityDesig == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid designation name " + desig + " at row :" + rowNumber;
                                    divWarningMsg.Hide = false;
                                    return;
                                }
                                history.LevelId = entityDesig.LevelId;
                                history.DesignationId = entityDesig.DesignationId;
                            }

                            serviceStatus = row["Service Status"].ToString().Trim();
                            if (!string.IsNullOrEmpty(row["Service Status"].ToString()))
                            {

                                entityStatus = statusList.FirstOrDefault(x => x.Value.ToLower() == serviceStatus.ToLower());

                                if (entityStatus == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid service status name " + entityStatus + " at row :" + rowNumber;
                                    divWarningMsg.Hide = false;
                                    return;
                                }
                                history.StatusId = int.Parse(entityStatus.Key);
                            }

                            history.Notes = row["Notes"].ToString();


                            // if same event exists then show warning for first time as user could refresh 
                            if (CommonManager.IsSaveEventExistsThenShowWarning(history))
                            {
                                string empName = EmployeeManager.GetEmployeeName(history.EmployeeId);
                                if (string.IsNullOrEmpty(isConfirm.Value.Trim()))
                                {
                                    X.MessageBox.Show(
                                     new MessageBoxConfig
                                     {
                                         Message = "Event " + eventName + " aleady exists for the date " + history.DateEng.ToString("yyyy-MMM-dd") + " for " + empName  
                                          + " or event could be repeated to other employees also, are you sure to repeat the event?",
                                         Buttons = MessageBox.Button.YESNO,
                                         Title = "Confirmation",
                                         Icon = MessageBox.Icon.INFO,
                                         MinWidth = 300,
                                         Fn = new JFunction("creditLimitCallback")
                                     });

                                  
                                    return;
                                }
                            }

                            list.Add(history);

                        }



                    }


                    bool statusImport = CommonManager.ImportServiceHistory(list);


                    if (statusImport)
                    {
                        JavascriptHelper.DisplayClientMsg("Service history imported for " + list.Count + " employees.",
                            Page, "closePopup();");
                    }
                    //isConfirm.Value = "";
                    ////if (status.IsSuccess)
                    ////{

                    ////}
                    ////else
                    ////{
                    ////    divMsgCtl.InnerHtml = status.ErrorMessage;
                    ////    divMsgCtl.Hide = false;
                    ////}

                    //divMsgCtl.InnerHtml = "Service history imported for " + list.Count + " employees.";
                    //divMsgCtl.Hide = false;

                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

