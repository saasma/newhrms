﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;

namespace Web.CP
{
    public partial class ExcludePFExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public string strIncomeID = "";
        public bool isDeduction = false;
        public List<int> incomeIDs = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            strIncomeID = Request.QueryString["IncomeIDList"];

            bool.TryParse( Request.QueryString["IsDeduction"],out isDeduction);
            
            string[] list = strIncomeID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string str in list)
            {
                int inccomeid;
                if (int.TryParse(str, out inccomeid))
                    incomeIDs.Add(inccomeid);
            }



            
           
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/PFExclude.xlsx");

            List<PIncome> incomes = PayManager.GetVariablePFIncomes();

            List<GetEmployeesForVariablePFIncomesResult> employees = PayManager.GetEmployeesForVariablePFIncomes();
            List<GetEmployeesIncomeForPFVariableIncomesResult> incomeList = PayManager.GetEmployeesIncomeForPFVariableIncomes();

            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteVariableIncomeForPFExportExcel(template, incomes, employees, incomeList);
        }


        private List<int> GetEmployeeIdList()
        {

            List<GetEmployeesForVariableIncomesResult> employees = PayManager.GetEmployeeForVariableIncomes(strIncomeID,isDeduction);

         
            List<int> ids = new List<int>();
            foreach (GetEmployeesForVariableIncomesResult item in employees)
            {
                ids.Add(item.EmployeeId);
            }
            return ids;

        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

            
                int employeeId;
                string value;
                //float? Days_At_Palati, Days_At_Kirne, On_Call, Shift_Afternoon, Shift_Night, Split_Shift_Morning, Split_Shift_Evening, OT_150, OT_200, OT_300, Public_Holiday,
                //    Other_Leave, Snacks_Allowance, Special_Compensation_Allowance, Meal_Allowance, TADA, Vehicle_Allowance;
               
                List<int> employeeIDList = GetEmployeeIdList();
                List<int> addedEmployeeIDList = new List<int>();
                int importedCount = 0;
                int employeeCount = 0;

                try
                {
                    List<GetEmployeesIncomeForPFVariableIncomesResult> projectPays = new List<GetEmployeesIncomeForPFVariableIncomesResult>();
                    List<PIncome> incomes = PayManager.GetVariablePFIncomes();


                    string name = "";
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false
                             && (row["Name"].Equals(DBNull.Value) || string.IsNullOrEmpty(row["Name"].ToString()))
                            )
                        {
                            continue;
                        }

                        if (addedEmployeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Employee ID " + employeeId + " has been repeated in the excel.";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                            addedEmployeeIDList.Add(employeeId);

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        name = row["Name"].ToString();

                        // for custom role only allow to import permissible emp only
                        if (SessionManager.IsCustomRole && employeeIDList.Contains(employeeId)==false)
                        {
                            continue;
                        }

                        importedCount += 1;
                        employeeCount += 1;
                        foreach (var project in incomes)
                        {
                            value = ExcelGenerator.GetStringValueFromCell(project.Title, row, employeeId);
                            if (value == null)
                                value = "";
                            if (value.ToLower() == "exclude")
                            {

                                GetEmployeesIncomeForPFVariableIncomesResult projectPay = new GetEmployeesIncomeForPFVariableIncomesResult();
                                
                                projectPay.EmployeeId = employeeId;
                                projectPay.IncomeId = project.IncomeId;
                              //  projectPay.Amount = (decimal)value;

                                //if (CommonManager.CompanySetting.WhichCompany != WhichCompany.HPL)
                                //{
                                //    if (value.Value < 0 && isDeduction == true)
                                //    {
                                //        divWarningMsg.InnerHtml = string.Format("Employee {0} contains negative value, so please correct it & import the excel.", name);
                                //        divWarningMsg.Hide = false;
                                //        return;
                                //    }
                                //}

                                projectPays.Add(projectPay);
                            }
                        }

                    }

                    int count = 0;
                    ResponseStatus status = PayManager.SaveVariablePFIncome(projectPays, isDeduction, ref count);

                    if (status.IsSuccessType == false)
                    {

                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }


                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                                                            employeeCount);
                    divMsgCtl.Hide = false;


                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
            else
            {
                divWarningMsg.InnerHtml = "Excel version should be 2007(.xlsx) or higher.";
                divWarningMsg.Hide = false;
            }
        }
        
     
       
    }

   
}

