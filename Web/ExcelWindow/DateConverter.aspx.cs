﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Ext.Net;
using Utils.Calendar;
using Utils;

namespace Web.CP
{
    public partial class DateConverter : BasePage
    {

       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {

                calEng.SelectTodayDate();
                calNep.SelectTodayDate();


                if (this.IsEnglish)
                {
                    cmb.SelectedItems.Add(new Ext.Net.ListItem { Index = 1 });
                    divEng.Hide();
                    txtNep.Hide();
                }
                else
                {
                    cmb.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });
                    divNep.Hide();
                    txtEng.Hide();
                }

            }
        }


        protected void cmb_Select(object sender, DirectEventArgs e)
        {
            if (cmb.SelectedItem != null && cmb.SelectedItem.Value != null)
            {
                int type = int.Parse(cmb.SelectedItem.Value);

                if (type == 2)
                {
                    //cmb.SelectedItems.Add(new Ext.Net.ListItem { Index = 1 });
                    divEng.Hide();
                    txtNep.Hide();

                    divNep.Show();
                    txtEng.Show();
                }
                else
                {
                    //cmb.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });
                    divEng.Show();
                    txtNep.Show();

                    divNep.Hide();
                    txtEng.Hide();
                }
            }
        }

        /// <summary>
        /// Convert Eng to Nep date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConvert_Click(object sender, DirectEventArgs e)
        {

            int type = int.Parse(cmb.SelectedItem.Value);


            try
            {

                if (type == 2)
                {
                    CustomDate engDate = CustomDate.ConvertNepToEng(calNep.SelectedDate);
                    txtEng.Text = engDate.ToString();

                    lblConertedDate.Text = engDate.EnglishDate.ToString("dd MMMM, yyyy");
                }
                else
                {
                    CustomDate nepDate = CustomDate.ConvertEngToNep(calEng.SelectedDate);
                    txtNep.Text = nepDate.ToString();


                    try
                    {


                        //CultureInfo culture = CultureInfo.GetCultureInfo("ne-NP");
                        CustomDate nepalidate = nepDate;

                        string[] yearNos = new string[4];
                        yearNos[0] = nepalidate.Year.ToString()[0].ToString();
                        yearNos[1] = nepalidate.Year.ToString()[1].ToString();
                        yearNos[2] = nepalidate.Year.ToString()[2].ToString();
                        yearNos[3] = nepalidate.Year.ToString()[3].ToString();

                        string[] dayNos = new string[2];
                        dayNos[0] = "";
                        dayNos[1] = "";


                        dayNos[0] = nepalidate.Day.ToString()[0].ToString();

                        lblConertedDate.Text = Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[0].ToString());
                        if (nepalidate.Day.ToString().Length >= 2)
                        {
                            dayNos[1] = nepalidate.Day.ToString()[1].ToString();
                            lblConertedDate.Text += Resources.NepaliCalendar.ResourceManager.GetString("N" + dayNos[1].ToString());
                        }

                        lblConertedDate.Text += " " + Resources.NepaliCalendar.ResourceManager.GetString(DateHelper.GetMonthName(nepalidate.Month, false))
                            + ", " +

                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[0]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[1]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[2]) +
                            Resources.NepaliCalendar.ResourceManager.GetString("N" + yearNos[3])
                            ;
                    }
                    catch (Exception exp1) { }
                }
            }
            catch (Exception e1)
            {
                Log.log("Error in date conversion", e1);
            }
        }

        
           
    }

   
}

