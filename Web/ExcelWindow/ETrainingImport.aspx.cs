﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class ETrainingImport : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            //if (SessionManager.User.URole.RoleId != (int)Role.Administrator)
            //{
            //    Response.Write("Not enough permisson.");
            //    Response.End();
            //    return;
            //}

        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmpTrainingImport.xlsx");

            string[] trainingTypes = ListManager.GetTrainingTypes().Select(x => x.TrainingTypeName).ToArray();
            string[] listCountries = new CommonManager().GetAllCountries().Select(x => x.CountryName).ToArray();

            string path = HttpContext.Current.Server.MapPath(template);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();
                    ExcelGenerator _objExcelGenerator = new ExcelGenerator();
                    ExcelGenerator.WriteIntoSheet(conn, "TrainingList", trainingTypes);
                    ExcelGenerator.WriteIntoSheet(conn, "CoutryList", listCountries);
                }
            }

            catch (Exception ex)
            {
                divWarningMsg.InnerHtml = ex.ToString();
                divWarningMsg.Hide = false;
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(template));

        }

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID();
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


                List<TrainingType> listTrainingTypes = ListManager.GetTrainingTypes();
                List<CountryList> listCountries = new CommonManager().GetAllCountries().ToList();

                int employeeId;

                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                List<HTraining> list = new List<HTraining>();

                string country = "", trainingTypeName = "";
                DateTime startDate, endDate;
               

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        HTraining obj = new HTraining();
                        obj.EmployeeId = employeeId;

                        trainingTypeName = row["Training Type"].ToString().Trim();
                        if (!string.IsNullOrEmpty(trainingTypeName))
                        {
                            TrainingType ojbTrainingType = listTrainingTypes.SingleOrDefault(x => x.TrainingTypeName.ToLower() == trainingTypeName.ToLower());
                            if (ojbTrainingType == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid Training Type for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            obj.TrainingTypeID = ojbTrainingType.TrainingTypeId;
                            obj.TrainingTypeName = ojbTrainingType.TrainingTypeName;
                        }

                        if (!string.IsNullOrEmpty(row["Training Name"].ToString().Trim()))
                            obj.TrainingName = row["Training Name"].ToString().Trim();
                        else
                        {
                            divWarningMsg.InnerHtml = "Training Name is required for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (!string.IsNullOrEmpty(row["Resource Person"].ToString().Trim()))
                            obj.ResourcePerson = row["Resource Person"].ToString().Trim();

                        if (!string.IsNullOrEmpty(row["Institute"].ToString().Trim()))
                            obj.InstitutionName = row["Institute"].ToString().Trim();

                        country = row["Country"].ToString().Trim();
                        if (!string.IsNullOrEmpty(country))
                        {
                            CountryList objCountry = listCountries.SingleOrDefault(x => x.CountryName.ToLower() == country.ToLower());
                            if (objCountry == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid Country for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            obj.Country = objCountry.CountryName;
                        }

                        if (!string.IsNullOrEmpty(row["Start Date"].ToString()))
                        {
                            obj.TrainingFrom = row["Start Date"].ToString();
                        }

                        if (!string.IsNullOrEmpty(row["End Date"].ToString()))
                        {
                            obj.TrainingTo = row["End Date"].ToString();
                        }
                        if (!string.IsNullOrEmpty(row["Duration"].ToString()))
                        {
                            int dur;
                            int DurationTypeID = 0;
                            if (int.TryParse(row["Duration"].ToString(), out dur))
                                obj.Duration = dur;
                            if (!string.IsNullOrEmpty(row["Duration Type"].ToString()))
                            {
                                if (row["Duration Type"].ToString().ToLower() == "year")
                                    DurationTypeID = 1;
                                if (row["Duration Type"].ToString().ToLower() == "month")
                                    DurationTypeID = 2;
                                if (row["Duration Type"].ToString().ToLower() == "week")
                                    DurationTypeID = 3;
                                if (row["Duration Type"].ToString().ToLower() == "day")
                                    DurationTypeID = 4;
                                obj.DurationTypeID = DurationTypeID;
                                obj.DurationTypeName = row["Duration Type"].ToString();
                            }

                        }



                        //For months
                        //obj.DurationTypeID = 2;
                        //obj.DurationTypeName = "Months";


                        if (!string.IsNullOrEmpty(row["Notes"].ToString().Trim()))
                            obj.Note = row["Notes"].ToString().Trim();

                        obj.Status = (int)HRStatusEnum.Approved;
                        obj.ApprovedBy = SessionManager.CurrentLoggedInUserID;
                        obj.ApprovedOn = BLL.BaseBiz.GetCurrentDateAndTime();

                        list.Add(obj);
                    }

                    importedCount = list.Select(x => x.EmployeeId).Distinct().Count();

                    Status status = NewHRManager.ImportEmployeeTrainings(list);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = string.Format("{0} training records imported successfully.", importedCount);
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }



    }


}

