﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;

namespace Web.CP
{
    public partial class EducationLocationScoreExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public string strEmployee = "";
        public bool isDeduction = false;
        public List<int> incomeIDs = new List<int>();
        int month = 0;
        int PeriodID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            PeriodID = int.Parse(Request.QueryString["PeriodId"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string template = ("~/App_Data/ExcelTemplate/EducationLocationScroeImport.xlsx");
          //  List<PIncome> incomes = PayManager.GetVariableIncomes(incomeIDs,isDeduction);
            //int AppraisalPeriod=0;
            List<EEmployee> employees = EmployeeManager.GetAllEmployees();

            

            List<AppraisalEmployeeEducationLocationScore> _AppraisalEmployeeTargetList = AppraisalManager.GetAppraisalEmployeeEducationLocation(PeriodID);

            //if (!string.IsNullOrEmpty(strEmployee))
            //{
            //    _AppraisalEmployeeTarget = _AppraisalEmployeeTarget.Where(x => x.EmployeeId == int.Parse(strEmployee)).ToList();
                    
            //}

         //   List<AppraisalTarget> _AppraisalList = AppraisalManager.GetAllAppraisalTargets();
            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteEducationLocationExportExcel(template, _AppraisalEmployeeTargetList,employees);
        }

        private List<int> GetEmployeeIdList()
        {
          return EmployeeManager.GetAllEmployeeID();
        }

        
        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");
            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);
                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

            
                int employeeId;
              
                List<int> employeeIDList = GetEmployeeIdList();
                List<int> addedEmployeeIDList = new List<int>();
                int importedCount = 0;
             

                try
                {
                    List<AppraisalEmployeeEducationLocationScore> _AppraisalEmployeeEducationLocationScore = new List<AppraisalEmployeeEducationLocationScore>();
                  //  List<AppraisalTarget> _AppraisalTarget = AppraisalManager.GetAllAppraisalTargets();

               
                    string name = "";
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        AppraisalEmployeeEducationLocationScore _objAppraisalEmployeeEducationLocationScore = new AppraisalEmployeeEducationLocationScore();
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false
                             && ( row["Employee"].Equals(DBNull.Value) || string.IsNullOrEmpty(row["Employee"].ToString()))
                            )
                        {
                            continue;
                        }

                        if (addedEmployeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Employee ID " + employeeId + " has been repeated in the excel.";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                            addedEmployeeIDList.Add(employeeId);

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        name = row["Employee"].ToString();
                        _objAppraisalEmployeeEducationLocationScore.EmployeeId = employeeId;
                        _objAppraisalEmployeeEducationLocationScore.AppraisalPeriodRef_ID = PeriodID;

                        double LocationScore = 0;
                        if (double.TryParse(row["Location Score"].ToString(), out LocationScore) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Location Score in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                            _objAppraisalEmployeeEducationLocationScore.LocationScore =  LocationScore;

                        double EducationScore = 0;
                        if (double.TryParse(row["Education Score"].ToString(), out EducationScore) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Education Score in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                            _objAppraisalEmployeeEducationLocationScore.EducationScore = EducationScore;

                        double SeniorityScore = 0;
                        if (double.TryParse(row["Seniority Score"].ToString(), out SeniorityScore) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid Education Score in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        else
                            _objAppraisalEmployeeEducationLocationScore.SeniorityScore = SeniorityScore;

                        // for custom role only allow to import permissible emp only
                        if (SessionManager.IsCustomRole && employeeIDList.Contains(employeeId)==false)
                        {
                            continue;
                        }

                        _AppraisalEmployeeEducationLocationScore.Add(_objAppraisalEmployeeEducationLocationScore);
                        importedCount += 1;
                      
                        



                    }


                    Status  status = AppraisalManager.InsertUpdateEducationLocationScore(_AppraisalEmployeeEducationLocationScore);

                    if (status.IsSuccess == false)
                    {

                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }


                    divMsgCtl.InnerHtml = string.Format("{0} Records has been imported",
                                                            importedCount);
                    divMsgCtl.Hide = false;
                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
            else
            {
                divWarningMsg.InnerHtml = "Excel version should be 2007(.xlsx) or higher.";
                divWarningMsg.Hide = false;
            }
        }
        
     
       
    }

   
}

