﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;

namespace Web.ExcelWindow
{
    public partial class EmployeeAwardExcel : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CashAwardTypeId"]))
                    hdnCashAwardTypeId.Value = Request.QueryString["CashAwardTypeId"];

                if (!string.IsNullOrEmpty(Request.QueryString["CalulationTypeId"]))
                    hdnCalculationTypId.Value = Request.QueryString["CalulationTypeId"];
                
                if (!string.IsNullOrEmpty(Request.QueryString["Date"]))
                    hdnDate.Value = Request.QueryString["Date"];
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmployeeAward.xlsx");

            int cashAwardTypeId = int.Parse(hdnCashAwardTypeId.Value);
            int calculationTypeId = int.Parse(hdnCalculationTypId.Value);
            string date = hdnDate.Value;

            ExcelGenerator.ExportEmployeeAward(template, GetEmployeeIdList(), cashAwardTypeId, calculationTypeId, date);
        }

        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            // Retrieve data from SQL Server table.
            List<int> ids = EmployeeManager.GetAllEmployeeID();
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }
                
                int employeeId;

                List<int> employeeIDList = GetEmployeeIdList();

                List<EmployeeCashAward> list = new List<EmployeeCashAward>();
                int importedCount = 0;

                decimal? amount;

                try
                {
                    int rowNumber = 1;

                    foreach (DataRow row in datatable.Rows)
                    {

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                            //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            //divWarningMsg.Hide = false;
                            //return;
                        }
                       

                        amount = ExcelGenerator.GetAmountValueFromCell("Amount/Rate", row, employeeId);
                        if (amount == null)
                            continue;

                        CustomDate date = new CustomDate();

                        if (row["Date"] != DBNull.Value && row["Date"].ToString() != "")
                        {
                            try
                            {
                                date = CustomDate.GetCustomDateFromString(row["Date"].ToString().Replace('-','/'), IsEnglish);                               
                            }
                            catch
                            {
                                divWarningMsg.InnerHtml = "Date is invalid for the employee having id : " + employeeId;
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Date is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                        }

                        EmployeeCashAward obj = new EmployeeCashAward();
                        obj.EmployeeId = employeeId;
                        obj.CashAwardTypeId = int.Parse(hdnCashAwardTypeId.Value);
                        obj.Month = date.Month;
                        obj.Year = date.Year;
                        obj.CalculationType = int.Parse(hdnCalculationTypId.Value);
                        obj.AmountOrRate = amount.Value;

                        if (row["Note"] != DBNull.Value && row["Note"].ToString() != "")
                            obj.Notes = row["Note"].ToString();

                        obj.CreatedBy = SessionManager.CurrentLoggedInUserID;
                        obj.CreatedOn = DateTime.Now;
                        obj.Date = date.ToString();
                        obj.DateEng = BLL.BaseBiz.GetEngDate(obj.Date, IsEnglish);

                        if(obj.AmountOrRate != null)
                            list.Add(obj);
                    }



                    int imported = 0;
                    Status status = AwardManager.SaveUpdateEmployeeAward(list, ref importedCount);

                    if (!status.IsSuccess)
                    {
                        divWarningMsg.InnerHtml = "Error in import.";
                        divWarningMsg.Hide = false;
                        return;
                    }

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }


    }
}