﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;
using System.Data.Linq;

namespace Web.CP
{
    public partial class EmpInsuranceClaimImport : BasePage
    {
        public string LotId;
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["LotID"]))
                LotId = Request.QueryString["LotID"];

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf1", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            int? totalRecords = 0;
            List<GetEmployeeInsuranceClaimListByLotResult> list = HRManager.GetEmployeeInsuranceClaimListByLot(-1, -1, -1, Convert.ToInt32(LotId), 1, 0, 99999999, ref totalRecords);
            string template = ("~/App_Data/ExcelTemplate/EmpInsuranceClaimImport.xlsx");

            ExcelGenerator.ExportEmployeeInsuranceClaim(template, list);
        }

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID();
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);
                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");
                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }
                int employeeId;
                List<int> employeeIDList = GetEmployeeIdList();



                int importedCount = 0;
                try
                {
                    List<SCClaim> list = new List<SCClaim>();
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        SCClaim _db = new SCClaim();
                        if (int.TryParse(row["Staff ID"].ToString(), out employeeId) == true)
                        {
                            if (!employeeIDList.Contains(employeeId))
                            {
                                divWarningMsg.InnerHtml = "Invalid staff id for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            else
                                _db.EmployeeId = employeeId;
                            _db.LotRef_ID = Convert.ToInt32(LotId);
                            rowNumber += 1;
                            if (!string.IsNullOrEmpty(row["ClaimID"].ToString().Trim()))
                            {
                                _db.ClaimID = Convert.ToInt32(row["ClaimID"].ToString());
                            }

                            if (!string.IsNullOrEmpty(row["Claim Amount received Date"].ToString().Trim()))
                            {
                                _db.ReceivedDate = DateTime.Parse(row["Claim Amount received Date"].ToString());
                            }

                            if (!string.IsNullOrEmpty(row["Received Amount"].ToString().Trim()))
                            {
                                _db.ReceivedAmount = Convert.ToDecimal(row["Received Amount"].ToString());
                            }
                            if (!string.IsNullOrEmpty(row["Remarks"].ToString().Trim()))
                            {
                                _db.Remarks = row["Remarks"].ToString();
                            }

                            importedCount += 1;

                            list.Add(_db);
                        }
                        else
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                    }
                    Status status = NewHRManager.UpdateEmployeeInsuranceClaimOnImport(list);
                    if (status.IsSuccess == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }
                    divMsgCtl.InnerHtml = string.Format("{0} Records has been imported", importedCount);
                    divMsgCtl.Hide = false;
                    this.HasImport = true;
                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value, exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                divWarningMsg.InnerHtml = "Excel version should be 2007(.xlsx) or higher.";
                divWarningMsg.Hide = false;
            }
        }
    }
}


