﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class TaxImportExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/TaxImport.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            List<DAL.GetForTaxHistoryResult> list = TaxManager.GetForTaxHistory("", 0, 999999, ref total);

            ExcelGenerator.ExportTax(template, list);
        }


        

        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            List<DAL.GetForTaxHistoryResult> list = TaxManager.GetForTaxHistory("", 0, 999999, ref total);
            List<int> ids = new List<int>();
            foreach (GetForTaxHistoryResult item in list)
            {
                ids.Add(item.EmployeeId);
            }
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = "Default sheet name 'Sheet1' can not be changed.";
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");

                int employeeId;
                decimal? grossAmount, pf, cit, insurance, sst, tds, remoteArea;

                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        // don't validate, while placing input should check for it
                        //if (!employeeIDList.Contains(employeeId))
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                        //    divWarningMsg.Hide = false;
                        //    return;

                        //    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}

                        importedCount += 1;

                        grossAmount = ExcelGenerator.GetAmountValueFromCell("Opening Gross Amount", row, employeeId);
                        //Days_At_Kirne = ExcelGenerator.GetValueFromCell("Days_At_Kirne", row, employeeId);

                        pf = ExcelGenerator.GetAmountValueFromCell("Opening PF", row, employeeId);

                        cit = ExcelGenerator.GetAmountValueFromCell("Opening CIT", row, employeeId);
                        insurance = ExcelGenerator.GetAmountValueFromCell("Opening Insurance", row, employeeId);
                        sst = ExcelGenerator.GetAmountValueFromCell("Opening SST", row, employeeId);
                        tds = ExcelGenerator.GetAmountValueFromCell("Opening Tax Paid", row, employeeId);

                        remoteArea = ExcelGenerator.GetAmountValueFromCell("Remote Area Opening", row, employeeId);

                        // if Remote area opening not enabled then don't allow to import
                        if (CommonManager.CompanySetting.RemoteAreaUsingProportionateDays == false
                            && remoteArea != null && remoteArea != 0)
                        {
                            divWarningMsg.InnerHtml = "Remote area opening not enabled so value can not be set for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        xml.Append(
                            string.Format("<row EmployeeId=\"{0}\" Amount=\"{1}\" PF=\"{2}\" CIT=\"{3}\"  Insurance=\"{4}\"  TaxPaid=\"{5}\" OpeningSST=\"{6}\"  OpeningRemoteArea=\"{7}\" /> ",
                                 employeeId, grossAmount, pf, cit, insurance, tds, sst, remoteArea)
                            );




                    }



                    xml.Append("</root>");

                    TaxManager.SaveForTax(xml.ToString());

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                divWarningMsg.InnerHtml = "xlsx file is required.";
                divWarningMsg.Hide = false;
            }
        }
        
     
       
    }

   
}

