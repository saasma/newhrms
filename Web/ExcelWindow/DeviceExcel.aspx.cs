﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class DeviceExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/DeviceImport.xlsx");

            int total = 0;
            // Retrieve data from SQL Server table.
            List<DAL.GetForDeviceMappingResult> list = TaxManager.GetForDeviceMapping("", 0, 999999, ref total);

            ExcelGenerator.ExportDevice(template, list);
        }




        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            List<DAL.GetForDeviceMappingResult> list = TaxManager.GetForDeviceMapping("", 0, 999999, ref total);
            List<int> ids = new List<int>();
            foreach (GetForDeviceMappingResult item in list)
            {
                ids.Add(item.EmployeeId);
            }
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");

                int employeeId;
                string deviceId;
                object value;
                object excludeValue;

                string previousEmployeeName = string.Empty;
                string previousEIN = string.Empty;

                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                List<string> deviceIdList = new List<string>();

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        string employeeName = row["Employee"].ToString();

                        //if (!employeeIDList.Contains(employeeId))
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                        //    divWarningMsg.Hide = false;
                        //    return;

                        //    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}

                        importedCount += 1;

                        value = row["Device Enroll No"];
                        //excludeValue = row["Exclude Device Attendance"];

                        if (value != null)
                            deviceId = value.ToString();
                        else
                            deviceId = "";

                        //Days_At_Kirne = ExcelGenerator.GetValueFromCell("Days_At_Kirne", row, employeeId);

                        if (value == null || string.IsNullOrEmpty(deviceId))
                        {

                            //if (excludeValue != null && excludeValue.ToString().ToLower() == "yes")
                            //{
                            //    divWarningMsg.InnerHtml = "Valid device value is required for employee id : " + employeeId + " of row " + rowNumber + " to exclude from attendance.";
                            //    divWarningMsg.Hide = false;
                            //    return;
                            //}

                            continue;
                            //divWarningMsg.InnerHtml = "Invalid device value for employee id : " + employeeId + " of row " + rowNumber;
                            //divWarningMsg.Hide = false;
                            //return;
                        }

                        //string strExclude = "";

                        //if (excludeValue != null)
                        //    strExclude = excludeValue.ToString().ToLower();

                        bool manualAtten = row["Enable Manual Attendance"].ToString().ToLower().Trim().Contains("yes") ? true : false;
                        bool excludeLateAbsentMail = row["Exclude Late Absent Mail"].ToString().ToLower().Trim().Contains("yes") ? true : false;
                        bool skipAbsentLateProcessing = row["Skip Absent/Late Deduction"].ToString().ToLower().Trim().Contains("yes") ? true : false;

                        if (deviceIdList.Contains(deviceId))
                        {
                            JavascriptHelper.DisplayClientMsg("Device id " + deviceId + " is already set for the employee " + previousEmployeeName + " with EIN " + previousEIN, Page);
                            return;
                        }

                        deviceIdList.Add(deviceId);


                        xml.Append(
                          string.Format("<row EmployeeId=\"{0}\" DeviceId=\"{1}\" IsEnabledManual=\"{2}\" IsEnabledSkipLateEntry=\"{3}\" IsExcelImport=\"{4}\" SkipAbsentAndLateEntryDeduction=\"{5}\" /> ",
                               employeeId, deviceId, manualAtten, excludeLateAbsentMail, "True", skipAbsentLateProcessing)
                          );

                        previousEmployeeName = employeeName;
                        previousEIN = row["EIN"].ToString();

                    }



                    xml.Append("</root>");

                    Status status = TaxManager.SaveForDeviceMapping(xml.ToString());
                    if (status.IsSuccess == true)
                    {
                        divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;
                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                    }

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }



    }


}

