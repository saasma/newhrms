﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class EEmployeeInfo : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            if (SessionManager.User.URole.RoleId != (int)Role.Administrator)
            {
                Response.Write("Not enough permisson.");
                Response.End();
                return;
            }

        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmployeeInfoImport.xlsx");

            string[] listCountries = new CommonManager().GetAllCountries().Select(x => x.CountryName).ToArray();
            string[] listBlood = CommonManager.GetAllBloodGroup().Select(x => x.BloodGroupName).ToArray();
            string[] listReligion = CommonManager.GetAllReligionList().Select(x => x.ReligionName).ToArray();
            string[] listDistict = new CommonManager().GetAllDisticts().Select(x => x.District).ToArray();
            string[] listNationality = new CommonManager().GetAllNationality().Select(x => x.Nationality).ToArray();

            string path = HttpContext.Current.Server.MapPath(template);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();
                    ExcelGenerator _objExcelGenerator = new ExcelGenerator();
                    ExcelGenerator.WriteIntoSheet(conn, "BloodGroupList", listBlood);
                    ExcelGenerator.WriteIntoSheet(conn, "CountrySheet", listCountries);
                    ExcelGenerator.WriteIntoSheet(conn, "ReligionList", listReligion);
                    ExcelGenerator.WriteIntoSheet(conn, "DistictList", listDistict);
                    ExcelGenerator.WriteIntoSheet(conn, "CitizenNationalityList", listNationality);
                }
            }

            catch (Exception ex)
            {
                divWarningMsg.InnerHtml = ex.ToString();
                divWarningMsg.Hide = false;
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(template));
        }

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID();
        }


        protected void btnImport_Click(object sender, EventArgs e)
        {

            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");
            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<CountryList> listCountries = new CommonManager().GetAllCountries().ToList();
                List<HBloodGroup> listBloodGroup = CommonManager.GetAllBloodGroup().ToList();
                List<HReligion> listReligion = CommonManager.GetAllReligionList().ToList();
                List<DistrictList> listDistrics = new CommonManager().GetAllDisticts().ToList();
                List<CitizenNationality> listCitNationality = new CommonManager().GetAllNationality().ToList();

                int employeeId;

                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;

                List<EEmployee> list = new List<EEmployee>();
                List<HCitizenship> citizenList = new List<HCitizenship>();
                List<HDrivingLicence> drivingList = new List<HDrivingLicence>();

                string country, distict, BloodGroup, Religion = "";//, level, faculty, division = "";
                DateTime daTime;

                try
                {
                    int rowNumber = 1;
                    #region loop
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);
                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        importedCount += 1;

                        EEmployee obj = new EEmployee();
                        obj.EHumanResources.Add(new EHumanResource { });
                        obj.HHumanResources.Add(new HHumanResource { });
                        obj.HPassports.Add(new HPassport { });

                        #region Ememployee
                        obj.EmployeeId = employeeId;
                        if (!string.IsNullOrEmpty(row["Birthmark"].ToString().Trim()))
                            obj.Birthmark = row["Birthmark"].ToString().Trim();

                        if (!string.IsNullOrEmpty(row["Marriage Anneversary"].ToString().Trim()))
                        {
                            try
                            {
                                obj.MarriageAniversaryEng = GetEngDate(row["Marriage Anneversary"].ToString());
                                obj.MarriageAniversary = row["Marriage Anneversary"].ToString();
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Invalid marriage anneversary date for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }

                        if (!string.IsNullOrEmpty(row["MotherTongue"].ToString().Trim()))
                            obj.MotherTongue = row["MotherTongue"].ToString().Trim();

                        #endregion

                        #region HCitizenship

                        HCitizenship citizenShip = new HCitizenship { EmployeeId = employeeId };
                        citizenList.Add(citizenShip);

                        country = row["Nationality"].ToString().Trim();
                        if (!string.IsNullOrEmpty(country))
                        {
                            CitizenNationality objCitNationality = listCitNationality.SingleOrDefault(x => x.Nationality.ToLower() == country.ToLower());
                            if (objCitNationality == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid Nationality for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            citizenShip.Nationality = objCitNationality.Nationality;
                        }


                        if (!string.IsNullOrEmpty(row["Citizenship No"].ToString().Trim()))
                        {
                            citizenShip.CitizenshipNo = row["Citizenship No"].ToString().Trim();
                        }


                        if (!string.IsNullOrEmpty(row["Citizenship Issue Date"].ToString().Trim()))
                        {
                            try
                            {
                                citizenShip.IssueDateEng = GetEngDate(row["Citizenship Issue Date"].ToString());
                                citizenShip.IssueDate = row["Citizenship Issue Date"].ToString();
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Invalid citizenship issue date for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }

                        distict = row["Citizenship Issue Place"].ToString().Trim();
                        if (!string.IsNullOrEmpty(distict))
                        {
                            DistrictList objDistric = listDistrics.SingleOrDefault(x => x.District.ToLower() == distict.ToLower());
                            // CountryList objCountry = listCountries.SingleOrDefault(x => x.CountryName.ToLower() == country.ToLower());
                            if (objDistric == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid citizenship issue place for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            citizenShip.Place = objDistric.District;
                        }
                        #endregion

                        #region HDrivingLicence
                        HDrivingLicence drivingShip = new HDrivingLicence { EmployeeId = employeeId };
                        drivingList.Add(drivingShip);

                        if (!string.IsNullOrEmpty(row["License Type"].ToString().Trim()))
                        {
                            drivingShip.LiscenceTypeName = row["License Type"].ToString().Trim();
                        }

                        if (!string.IsNullOrEmpty(row["License No"].ToString().Trim()))
                        {
                            drivingShip.DrivingLicenceNo = row["License No"].ToString().Trim();
                        }

                        if (!string.IsNullOrEmpty(row["License Issue Date"].ToString().Trim()))
                        {
                            try
                            {
                                drivingShip.DrivingLicenceIssueDate = row["License Issue Date"].ToString().Trim();
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Invalid license issue date for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }

                        country = row["License Issue Country"].ToString().Trim();
                        if (!string.IsNullOrEmpty(country))
                        {
                            CountryList objCountry = listCountries.SingleOrDefault(x => x.CountryName.ToLower() == country.ToLower());
                            if (objCountry == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid license issue country for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            drivingShip.IssuingCountry = objCountry.CountryName;
                        }

                        #endregion


                        #region EHumanResource
                        EHumanResource _objEHumanResource = new EHumanResource();
                        if (!string.IsNullOrEmpty(row["INO"].ToString().Trim()))
                        {
                            _objEHumanResource.IdCardNo = row["INO"].ToString().Trim();
                            obj.EHumanResources.Add(_objEHumanResource);
                        }
                        #endregion

                        #region HHumanResource
                        HHumanResource _objHHumanResource = new HHumanResource(); //obj.HHumanResources.FirstOrDefault(x => x.EmployeeId == employeeId);
                        BloodGroup = row["Blood Group"].ToString().Trim();
                        if (!string.IsNullOrEmpty(BloodGroup))
                        {
                            HBloodGroup objBloodGroup = listBloodGroup.SingleOrDefault(x => x.BloodGroupName.ToLower() == BloodGroup.ToLower());
                            if (objBloodGroup == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid blood group for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            _objHHumanResource.BloodGroup = objBloodGroup.Id.ToString();
                        }

                        Religion = row["Religion"].ToString().Trim();
                        if (!string.IsNullOrEmpty(Religion))
                        {
                            HReligion objReligion = listReligion.SingleOrDefault(x => x.ReligionName.ToLower() == Religion.ToLower());
                            if (objReligion == null)
                            {
                                divWarningMsg.InnerHtml = "Invalid religion group for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            _objHHumanResource.Religion = objReligion.ReligionId.ToString();
                        }
                        obj.HHumanResources.Add(_objHHumanResource);
                        #endregion

                        #region HPassport
                        HPassport _hpass = new HPassport(); //obj.HPassports.FirstOrDefault(x => x.EmployeeId == employeeId);
                        if (!string.IsNullOrEmpty(row["Passport No"].ToString().Trim()))
                        {
                            _hpass.PassportNo = row["Passport No"].ToString().Trim();

                        }

                        if (!string.IsNullOrEmpty(row["Passport Issue Date"].ToString().Trim()))
                        {
                            try
                            {
                                _hpass.IssuingDateEng = GetEngDate(row["Passport Issue Date"].ToString());
                                _hpass.IssuingDate = row["Passport Issue Date"].ToString();
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Invalid passport issue date for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                        }


                        if (!string.IsNullOrEmpty(row["Passport Valid Upto"].ToString().Trim()))
                        {
                            try
                            {
                                _hpass.ValidUptoEng = GetEngDate(row["Passport Valid Upto"].ToString());
                                _hpass.ValidUpto = row["Passport Valid Upto"].ToString();
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Invalid passport valid upto date for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }
                        obj.HPassports.Add(_hpass);
                        #endregion
                        list.Add(obj);
                    }
                    #endregion

                    #region saving and updating
                    importedCount = list.Select(x => x.EmployeeId).Distinct().Count();
                    Status status = NewHRManager.ImportEmployeeInfo(list, citizenList, drivingList);//have to change list
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;
                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }
                    #endregion

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}

