﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;

namespace Web.CP
{
    public partial class PastSalaryExcel : BasePage
    {
        int payrollPerodId = 0;
        int typeId=0;
        int sourceValueId=0;
        KeyValue keyValue = null;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.payrollPerodId = UrlHelper.GetIdFromQueryString("payrollPeriod");

            //string valueType = Request.QueryString["type"];
            //try
            //{
            //    string[] values = valueType.Split(new char[] { ':' });

            //    sourceValueId = int.Parse(values[0]);

            //    typeId = int.Parse(values[1]);
            //}
            //catch { }

            //List<KeyValue> incDedList = PayManager.GetEmployeesIncomeDeductionList(payrollPerodId);

            


            //foreach (KeyValue value in incDedList)
            //{
            //    if (value.Value == (sourceValueId + ":" + typeId))
            //    {
            //        keyValue = value;
            //        break;
            //    }
            //}
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //PayrollPeriod period = CommonManager.GetPayrollPeriod(payrollPerodId);
            //if(period != null)
            //{
            //btnImport.OnClientClick=
            //    string.Format("Are you sure you want to change the calculation for the month of {0}?", period.Name );
            //}
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/PastSalaryImport.xlsx");

           


           {
               ExcelGenerator.ExportPastSheet(template);
           }
        }


        

       

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xlsx");

           

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xlsx"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");

                int employeeId; string headerName = "", note = "";
                decimal? adjustment, adjusted,existingAmount;



                List<PastSalaryBO> adjustmentList = new List<PastSalaryBO>();

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;

                        PastSalaryBO salary = new PastSalaryBO();

                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        salary.EmployeeID = employeeId;
                        
                        importedCount += 1;

                        salary.CIT = ExcelGenerator.GetAmountValueFromCell("CIT", row, employeeId);
                        salary.SST = ExcelGenerator.GetAmountValueFromCell("SST", row, employeeId);
                        salary.TDS = ExcelGenerator.GetAmountValueFromCell("TDS", row, employeeId);

                        salary.IncomePF = ExcelGenerator.GetAmountValueFromCell("Income PF", row, employeeId);

                        salary.DeductionPF = ExcelGenerator.GetAmountValueFromCell("Deduction PF", row, employeeId);

                        adjustmentList.Add(salary);
                    
                    }




                    xml.Append("</root>");

                    if (importedCount > 0)
                    {
                        ResponseStatus status =  CalculationManager.ChangePastSavedSalary(adjustmentList,this.payrollPerodId);

                        if (status.IsSuccessType == false)
                        {
                            divWarningMsg.InnerHtml = status.ErrorMessage;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

