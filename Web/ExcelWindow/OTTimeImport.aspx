﻿<%@ Page Title="OT Time Import" Language="C#" MasterPageFile="~/Master/ForEmployeePopupPage.Master" AutoEventWireup="true" CodeBehind="OTTimeImport.aspx.cs" Inherits="Web.ExcelWindow.OTTimeImport" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">
    var skipLoadingCheck = true;
    function closePopup() {

        if (hasImport)
            window.opener.refreshWindow();
    }

    window.onunload = closePopup; 
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="server">

<ext:ResourceManager ID="ResourceManager1" runat="server" />

<div class="popupHeader" style="margin-top:-20px;">
        <h3>
            OT Time Import</h3>
    </div>
    <div class=" marginal" style='margin-top:0px'>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
      
        <div>
          <h2 class="templateHeader"> Step 1. Download our template file </h2>
          
            Start by downloading tax excel template file. This file has the correct column headings
            that needs to import your time attendance. 
            
            <div>
            <asp:LinkButton ID="btnExport"  CssClass=" excel marginRight" Style="float: left;margin-top:5px;margin-bottom:5px" runat="server" Text="Download Temp." OnClick="btnExport_Click" />
            </div>
            
         <h2 class="templateHeader">    Step 2. Copy your time attendance details into the template</h2>
         
            Using Excel or another spreadsheet editor, fill
            the template with your time attendance data. Make sure the time attendance data matches
            the column headings provided in the template. 
            
           <p class='templateWarning'> <em> IMPORTANT: </em>Do not change the column
            headings in the template file. These need to be unchanged for the import to work
            in the next step. Date should be english in 'yyyy/mm/dd' format. Start and End times should be in 24 hour format(hh:mm).
            Employee Name, and Date are compulsory. You must fill either Start Time or End Time or both.
            </p>

            
          <h2 class="templateHeader">   Step 3. Import the updated template file </h2> Choose a file to import
            & press "Import" button. The file you import must be an excel file.            
            
        </div>
        
        <div style='margin-top:10px;margin-bottom:10px;'>
        <asp:FileUpload ID="fupUpload" runat="server" />
        </div>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ControlToValidate="fupUpload"
            ValidationGroup="upload" ErrorMessage="File is required." runat="server" />
        <asp:Button ID="btnImport" runat="server" ValidationGroup="upload" Text="Import"
            CssClass="save" OnClick="btnImport_Click" />
        <asp:ValidationSummary ID="ValidationSummary1" ShowSummary="false" ValidationGroup="upload" ShowMessageBox="true"
            runat="server" />
    </div>


</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
