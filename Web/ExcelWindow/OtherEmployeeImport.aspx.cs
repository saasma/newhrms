﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class OtherEmployeeImport : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            if (SessionManager.User.URole.RoleId != (int)Role.Administrator)
            {
                Response.Write("Not enough permisson.");
                Response.End();
                return;
            }

        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/OtherImport.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            List<PPay> list = TaxManager.GetAccountListForExport();
            List<EFundDetail> fundDetals = BLL.BaseBiz.PayrollDataContext.EFundDetails.ToList();
            List<Bank> bankList = PayManager.GetBankList();
            List<BankBranch> bankBranchList = PayManager.GetBankBranchList();

            ExcelGenerator.ExportPay(template, list,bankList,bankBranchList,fundDetals);
        }


        

        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            List<PPay> list = TaxManager.GetAccountListForExport();
            List<int> ids = new List<int>();
            foreach (PPay item in list)
            {
                ids.Add(item.EmployeeId.Value);
            }
            return ids;
        }

        public string ValidatePFCITPANBankNo(List<PPay> list, PPay newEmp,List<EFundDetail> fundList,EFundDetail fund)
        {
            string bankNo = newEmp.BankACNo == null ? "" : newEmp.BankACNo.Trim().ToLower();
            //string loan = newEmp.LoanAccount == null ? "" : newEmp.LoanAccount.Trim().ToLower();
            //string advance = newEmp.AdvanceAccount == null ? "" : newEmp.AdvanceAccount.Trim().ToLower();
            string panNo = fund.PANNo;

            string pfNo = fund.PFRFNo;
            string citNo = fund.CITNo;
            string citCode = fund.CITCode;
            
            if (!string.IsNullOrEmpty(bankNo))
            {
                //foreach (PPay emp in list)
                {
                    PPay otherEmp = list.Where(x => x.BankACNo.Trim().ToLower() == bankNo)
                        .FirstOrDefault();
                    if (otherEmp != null)
                    {
                        return
                            "Bank AC Number \"" + newEmp.BankACNo + "\" already exists between employees " + otherEmp.EmployeeName + " and " + newEmp.EmployeeName + ".";
                    }
                }
            }

            //if (!string.IsNullOrEmpty(loan))
            //{
            //    foreach (PPay emp in list)
            //    {
            //        PPay otherEmp = list.Where(x => x.LoanAccount.Trim().ToLower() == loan)
            //            .FirstOrDefault();
            //        if (otherEmp != null)
            //        {
            //            return
            //                "Loan AC Number \"" + newEmp.LoanAccount + "\" already exists between employees " + otherEmp.EmployeeName + " and " + newEmp.EmployeeName + ".";
            //        }
            //    }
            //}

            //if (!string.IsNullOrEmpty(advance))
            //{
            //    foreach (PPay emp in list)
            //    {
            //        PPay otherEmp = list.Where(x => x.AdvanceAccount.Trim().ToLower() == advance)
            //            .FirstOrDefault();
            //        if (otherEmp != null)
            //        {
            //            return
            //                "Advance AC Number \"" + newEmp.AdvanceAccount + "\" already exists between employees " + otherEmp.EmployeeName + " and " + newEmp.EmployeeName + ".";
            //        }
            //    }
            //}

            if (!string.IsNullOrEmpty(pfNo))
            {
                //foreach (EFundDetail emp in fundList)
                {
                    EFundDetail otherEmp = fundList.Where(x => x.PFRFNo.Trim().ToLower() == pfNo)
                        .FirstOrDefault();
                    if (otherEmp != null)
                    {
                        return
                            "PF Number \"" + fund.PFRFNo + "\" already exists between employees " + otherEmp.EmployeeName + " and " + newEmp.EmployeeName + ".";
                    }
                }
            }

            if (!string.IsNullOrEmpty(panNo))
            {
                //foreach (EFundDetail emp in fundList)
                {
                    EFundDetail otherEmp = fundList.Where(x => x.PANNo.Trim().ToLower() == panNo)
                        .FirstOrDefault();
                    if (otherEmp != null)
                    {
                        return
                            "PAN Number \"" + fund.PANNo + "\" already exists between employees " + otherEmp.EmployeeName + " and " + newEmp.EmployeeName + ".";
                    }
                }
            }

            if (!string.IsNullOrEmpty(citNo))
            {
                //foreach (EFundDetail emp in fundList)
                {
                    EFundDetail otherEmp = fundList.Where(x => x.CITNo.Trim().ToLower() == citNo)
                        .FirstOrDefault();
                    if (otherEmp != null)
                    {
                        return
                            "CIT Number \"" + fund.CITNo + "\" already exists between employees " + otherEmp.EmployeeName + " and " + newEmp.EmployeeName + ".";
                    }
                }
            }

            if (!string.IsNullOrEmpty(citCode))
            {
                //foreach (EFundDetail emp in fundList)
                {
                    EFundDetail otherEmp = fundList.Where(x => x.CITCode.Trim().ToLower() == citCode)
                        .FirstOrDefault();
                    if (otherEmp != null)
                    {
                        return
                            "CIT Number \"" + fund.CITCode + "\" already exists between employees " + otherEmp.EmployeeName + " and " + newEmp.EmployeeName + ".";
                    }
                }
            }
            return "";

        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


                List<Bank> bankList = PayManager.GetBankList();
                List<BankBranch> bankBranchList = PayManager.GetBankBranchList();

                int employeeId;
                string paymentMode, bankSalaryAccount, loanAccount, advanceAccount, pfNo, citNo, citCode, panNo, otherFund;

                string bankName = "", bankBranch = "";

                List<int> employeeIDList = GetEmployeeIdList();
                List<PPay> list = new List<PPay>();
                List<EFundDetail> fundList = new List<EFundDetail>();
                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);
                        
                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                            //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            //divWarningMsg.Hide = false;
                            //return;
                        }

                        importedCount += 1;

                        paymentMode = row["Payment Mode"].ToString().Trim();
                        bankSalaryAccount = row["Bank/Salary Account No"].ToString().Trim();
                        loanAccount = row["Loan Account No"].ToString().Trim();
                        advanceAccount = row["Advance Account No"].ToString().Trim();
                        bankName = row["Bank Name"].ToString().Trim();
                        bankBranch = row["Bank Branch"].ToString().Trim();
                        otherFund = row["Other Retirement Fund"].ToString().Trim();

                        pfNo = row["PF Number"].ToString().Trim();
                        citNo = row["CIT Number"].ToString().Trim();
                        citCode = row["CIT Code"].ToString().Trim();
                        //Days_At_Kirne = ExcelGenerator.GetValueFromCell("Days_At_Kirne", row, employeeId);
                        panNo = row["PAN No"].ToString().Trim();

                        if (string.IsNullOrEmpty(paymentMode) ||
                            (paymentMode.ToLower() != PaymentMode.CASH.ToLower() && paymentMode.ToLower() != PaymentMode.BANK_DEPOSIT.ToLower()))
                        {
                            divWarningMsg.InnerHtml = "Invalid payment mode for row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (!string.IsNullOrEmpty(panNo))
                        {
                            if (!System.Text.RegularExpressions.Regex.IsMatch(panNo.ToString(), Utils.Config.PanNoRegExp))
                            {
                                //SetImportError("PAN No must be 9 digit", emp, index);
                                divWarningMsg.InnerHtml = "PAN no must be 9 digit value for row " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                        }



                        PPay item = new PPay();
                        item.EmployeeId = employeeId;
                        item.PaymentMode = paymentMode;
                        item.BankACNo = bankSalaryAccount;
                        item.LoanAccount = loanAccount;
                        item.AdvanceAccount = advanceAccount;
                        item.EmployeeName = row["Employee"].ToString().Trim();

                        if (item.PaymentMode == PaymentMode.BANK_DEPOSIT)
                        {
                            Bank bank = bankList.FirstOrDefault(x => x.Name.ToLower().Equals(bankName.ToLower()));
                            if (bank != null)
                                item.BankID = bank.BankID;
                            else
                            {
                                divWarningMsg.InnerHtml = "Invalid bank name for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }

                            if (!string.IsNullOrEmpty(bankBranch))
                            {
                                BankBranch bankbranchEntity = bankBranchList
                                    .FirstOrDefault(x => (x.Bank.Name + " - " + x.Name).ToLower().Equals(bankBranch.ToLower()));

                                if (bankbranchEntity == null)
                                {
                                    bankbranchEntity = bankBranchList.FirstOrDefault(x => x.Name.ToLower().Equals(bankBranch.ToLower())
                                        && x.BankID == item.BankID);
                                }

                                if (bankbranchEntity != null)
                                {
                                    item.BankBranchID = bankbranchEntity.BankBranchID;

                                    if (bankbranchEntity.BankID != item.BankID)
                                    {
                                        divWarningMsg.InnerHtml = "Branch name does not come under the selected bank for row number " + rowNumber + ".";
                                        divWarningMsg.Hide = false;
                                        return;
                                    }
                                }
                                else
                                {
                                    divWarningMsg.InnerHtml = "Invalid bank branch name for row number " + rowNumber + ".";
                                    divWarningMsg.Hide = false;
                                    return;
                                }
                            }
                        }

                        EFundDetail fund = new EFundDetail();
                        fund.EmployeeId = employeeId;
                        fund.PFRFNo = pfNo;
                        fund.CITNo = citNo;
                        fund.CITCode = citCode;
                        fund.PANNo = panNo;
                        fund.EmployeeName = row["Employee"].ToString().Trim();

                        if (otherFund.ToLower().Trim() == "yes")
                            fund.HasOtherPFFund = true;

                        string pfCITPANNo = ValidatePFCITPANBankNo(list, item,fundList,fund);
                        if (!string.IsNullOrEmpty(pfCITPANNo))
                        {
                            divWarningMsg.InnerHtml = pfCITPANNo;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        list.Add(item);
                        fundList.Add(fund);

                    }




                    TaxManager.SavePay(list,fundList);

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

