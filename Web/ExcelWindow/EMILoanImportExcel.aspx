﻿<%@ Page Title="Loan Import" Language="C#" MasterPageFile="~/Master/ForPopupPage.Master" AutoEventWireup="true" CodeBehind="EMILoanImportExcel.aspx.cs" Inherits="Web.ExcelWindow.EMILoanImportExcel" %>

<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function closePopup() {


            if (hasImport)
                window.opener.refreshWindow();
        }


        window.onunload = closePopup;

        var skipLoadingCheck = true;
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="popupHeader">
        <h3>
            Loan Import</h3>
    </div>
    <div class=" marginal" style='margin-top: 0px'>
        <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" Width="390px" runat="server" />
        <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" Width="390px"
            runat="server" />
        <div>
            <h2 class="templateHeader">
                Step 1. Download our template file
            </h2>
            <div>
                <asp:LinkButton ID="btnExport" CssClass=" excel marginRight" Style="float: left;
                    margin-top: 5px; margin-bottom: 5px" runat="server" Text="Download Temp." OnClick="btnExport_Click" />
            </div>
            <h2 class="templateHeader">
                Step 2. Copy your values into the template</h2>
            <p class='templateWarning'>
                <em>IMPORTANT: </em>Date should be placed in english in the format yyyy/mm/dd <br /><br />
                
            </p>
            <h2 class="templateHeader">
                Step 3. Import the updated template file
            </h2>
        </div>
        <div style='margin-top: 10px; margin-bottom: 10px;'>
            <asp:FileUpload ID="fupUpload" runat="server" />
        </div>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ControlToValidate="fupUpload"
            ValidationGroup="upload" ErrorMessage="File is required." runat="server" />
        <asp:Button ID="btnImport" runat="server" ValidationGroup="upload" Text="Import"
            CssClass="save" OnClick="btnImport_Click" />
        <asp:ValidationSummary ID="ValidationSummary1" ShowSummary="false" ValidationGroup="upload"
            ShowMessageBox="true" runat="server" />
    </div>
</asp:Content>