﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;

namespace Web.CP
{
    public partial class MGAllowanceSalaryExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public int IncomeId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            IncomeId = int.Parse(Request.QueryString["IncomeID"]);

          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            PIncome income = new PayManager().GetIncomeById(IncomeId);

            if (income != null)
            {


                string template = "";
                
                
                template = ("~/App_Data/ExcelTemplate/MGAllowanceAmount.xlsx");
              




                List<PEmployeeIncome> list = PayManager.GetPEmployeeIncomes(IncomeId);


                // Retrieve data from SQL Server table.
                ExcelGenerator.ExportMegaLikeChildrenLikeIncome(template, list, income);

            }

        }


        

     

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");
             PIncome income = new PayManager().GetIncomeById(IncomeId);

             if (income == null)
             {
                 return;
             }

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

            
                int employeeId;
                //float? value;
                //float? Days_At_Palati, Days_At_Kirne, On_Call, Shift_Afternoon, Shift_Night, Split_Shift_Morning, Split_Shift_Evening, OT_150, OT_200, OT_300, Public_Holiday,
                //    Other_Leave, Snacks_Allowance, Special_Compensation_Allowance, Meal_Allowance, TADA, Vehicle_Allowance;
               
                

                int importedCount = 0;
                //int totalMismatchEmployeeCount = 0;


                try
                {
                    List<PEmployeeIncome> list = new List<PEmployeeIncome>();
                    

                    string name = "";
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false
                             && (row["Employee"].Equals(DBNull.Value) || string.IsNullOrEmpty(row["Employee"].ToString()))
                            )
                        {
                            continue;
                        }

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        name = row["Employee"].ToString();

                        PEmployeeIncome empIncome = new PEmployeeIncome();
                        empIncome.EmployeeId = employeeId;
                        empIncome.Name = name;


                        empIncome.Amount = ExcelGenerator.GetAmountValueFromCell("BillReceived", row, employeeId);




                        importedCount += 1;

                        list.Add(empIncome);


                    }

                    int count = 0;
                    Status status = AllowanceManager.SaveAllowanceIncome(list, ref count, income);

                    if (status.IsSuccess == false)
                    {

                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }


                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                                                            count);
                    divMsgCtl.Hide = false;


                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

