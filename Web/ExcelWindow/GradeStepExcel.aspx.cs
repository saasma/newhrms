﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class GradeStepExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               this.CustomId = UrlHelper.GetIdFromQueryString("positionId");                
            }


        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/GradeStep.xlsx");


            List<Step> steps = PayManager.GetAllStep();
            List<Grade> grades = PayManager.GetAllGrade();

            List<PositionGradeStepAmount> amounts = PayManager.GetGradeStepAmounts(this.CustomId);
          
                 
            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteStepGradeExcel(template, steps, grades, amounts);
        }


        

        private List<int> GetEmployeeIdList()
        {
            List<GetValidEmployeesForPayrollPeriodResult> employees = EmployeeManager.GetValidEmployeesForPayrollPeriod(
                this.CustomId, -1, -1).OrderBy(ee => ee.EmployeeId).ToList();

         
            List<int> ids = new List<int>();
            foreach (GetValidEmployeesForPayrollPeriodResult item in employees)
            {
                ids.Add(item.EmployeeId);
            }
            return ids;
        }

        private int GetStepIdByName(string name, List<Step> steps)
        {
            foreach (Step step in steps)
                if (step.Name.Equals(name))
                    return step.StepId;
            return -1;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


               

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


                int stepId, gradeId;
                float? value;
              
  
                //List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                int totalMismatchEmployeeCount = 0;
                //float totalHoursOrDaysInAMonth = ProjectManager.TotalHourOrDaysInAMonth;
                try
                {
                    List<PositionGradeStepAmount> amounts = new List<PositionGradeStepAmount>();

                    List<Step> steps = PayManager.GetAllStep();
                    List<Grade> grades = PayManager.GetAllGrade();


                    //List<Project> projects = ProjectManager.GetProjectsForLatestPayroll();
                    //projects.RemoveAt(0);
                    string name = "";


                    foreach (DataRow row in datatable.Rows)
                    {
                        string stepColumnName = row[0].ToString();
                        stepId = GetStepIdByName(stepColumnName, steps);
                        if (stepId == -1)
                            continue;



                        

                        importedCount += 1;
                        
                        float totalHoursOrDays = 0;
                        foreach (var grade in grades)
                        {
                            value = ExcelGenerator.GetValueFromCell(grade.Name, row, stepId);
                            if (value == null)
                                value = (float)0;
                            if (value != null)
                            {

                                PositionGradeStepAmount amount = new PositionGradeStepAmount();
                                amount.PositionId = this.CustomId;
                                amount.StepId = stepId;
                                amount.GradeId = grade.GradeId;
                                
                                amount.Amount =(decimal) value;

                                //if (value.Value < 0)
                                //{
                                //    divWarningMsg.InnerHtml =string.Format("Employee {0} contains negative hours, so please correct it & import the excel.", name);
                                //    divWarningMsg.Hide = false;
                                //    return;
                                //}

                                amounts.Add(amount);
                            }
                        }

                        // add to total mis match employee count for showing message
                        //if (totalHoursOrDays != totalHoursOrDaysInAMonth)
                        //    totalMismatchEmployeeCount += 1;



                    }
        

                    PayManager.SaveUpdateGradeStepAmount(amounts);


                    divWarningMsg.InnerHtml = "Import successfull.";
                    divWarningMsg.Hide = false;
                    //if (totalMismatchEmployeeCount == 0)
                    //{
                    //    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                    //                                        importedCount);
                    //    divMsgCtl.Hide = false;
                    //}
                    //else
                    {
                        //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                        //                                        importedCount)
                        //                          + string.Format(" For {0} employees, total hours {1} doesn't match, please check in the details view.",totalMismatchEmployeeCount,totalHoursOrDaysInAMonth);
                        //divWarningMsg.Hide = false; 
                    }

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

