﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class PFCITOpeningImport : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/PFCITOpeningImport.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            List<DAL.GetOpeningImportResult> list = TaxManager.GetForPFOpening("", 0, 999, ref total);

            ExcelGenerator.ExportPFCITOpening(template, list);
        }


        

        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            List<DAL.GetOpeningImportResult> list = TaxManager.GetForPFOpening("", 0, 999, ref total);
            List<int> ids = new List<int>();
            foreach (var item in list)
            {
                ids.Add(item.EmployeeId);
            }
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {



            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xlsx");

            List<PFCITOpening> inserInstance = new List<PFCITOpening>();
            PFCITOpening openingInstance = new PFCITOpening();

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xlsx"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");

                int employeeId;
                decimal? openingPF, openingCIT, openingPFInterest, openingCITInterest;

                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row[0].ToString(), out employeeId);
                        
                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                            //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            //divWarningMsg.Hide = false;
                            //return;
                        }

                        importedCount += 1;

                        openingCIT = ExcelGenerator.GetAmountValueFromCell("OpeningCIT", row, employeeId);
                        openingCITInterest = ExcelGenerator.GetAmountValueFromCell("OpeningCITInterest", row, employeeId);
                        openingPF = ExcelGenerator.GetAmountValueFromCell("OpeningPF", row, employeeId);
                        openingPFInterest = ExcelGenerator.GetAmountValueFromCell("OpeningPFInterest", row, employeeId);

                        openingInstance = new PFCITOpening();
                        if(employeeId!=null)
                        {
                            openingInstance.EmployeeId = employeeId;
                            openingInstance.OpeningPF = openingPF == null ? 0 : openingPF.Value;
                            openingInstance.OpeningPFInterest = openingPFInterest == null ? 0 : openingPFInterest.Value;
                            openingInstance.OpeningCIT = openingCIT == null ? 0 : openingCIT.Value;
                            openingInstance.OpeningCITInterest = openingCITInterest == null ? 0 : openingCITInterest.Value;
                        }

                        if (openingInstance != null && employeeId != null & employeeId !=0)
                        {
                            inserInstance.Add(openingInstance);
                        }
                        
                        /*
                        xml.Append(
                            string.Format("<row EmployeeId=\"{0}\" Amount=\"{1}\" PF=\"{2}\" CIT=\"{3}\"  Insurance=\"{4}\"  TaxPaid=\"{5}\" OpeningSST=\"{6}\"  OpeningRemoteArea=\"{7}\" /> ",
                                 employeeId, grossAmount, pf, cit, insurance, tds, sst,remoteArea)
                            );

                    */
                      

                    }



                    //xml.Append("</root>");

                    //TaxManager.SaveForTax(xml.ToString());

                    Status status = TaxManager.ImportPFCITOpening(inserInstance);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = string.Format("PF/CIT Opening Imported", importedCount);
                        divMsgCtl.Hide = false;
                    }

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }


         
        
     
       
    }

   
}

