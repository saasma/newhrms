﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;

namespace Web.CP
{
    public partial class PartialTaxExcel : BasePage
    {
        int payrollPerodId = 0;
      
      //  KeyValue keyValue = null;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string pp = Request.QueryString["payrollPeriod"];
            if (pp.Contains(":"))
                payrollPerodId = int.Parse(pp.Substring(0, pp.IndexOf(":")));
            else

                payrollPerodId = int.Parse(pp);

          

        

            //List<KeyValue> incDedList = PayManager.GetEmployeesIncomeDeductionList(payrollPerodId);

            


            //foreach (KeyValue value in incDedList)
            //{
            //    if (value.Value == (sourceValueId + ":" + typeId))
            //    {
            //        keyValue = value;
            //        break;
            //    }
            //}
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/PartialTax.xlsx");

           
         
            int? totalRecords = 0;

            List<CalcGetHeaderListResult> headerList = CalculationManager.GetPartialTaxPaidHeaderList(payrollPerodId, true);

            List<CalcGetCalculationListResult> employeeList = CalculationManager.GetPartialPaidEmployeeList(SessionManager.CurrentCompanyId,
                        payrollPerodId, 0,
                        9999, ref totalRecords,-1,"");

            List<PartialPaidDetail> amountList = PartialAddOnManager.GetAddOnDetail(payrollPerodId);



            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteAddOnIncomeForExportExcel(template, headerList, employeeList, amountList);

        }


        

        private List<int> GetEmployeeIdList()
        {
            int? totalRecords = 0;
            List<CalcGetCalculationListResult> employeeList = CalculationManager.GetPartialPaidEmployeeList(SessionManager.CurrentCompanyId,
                        payrollPerodId, 0,
                        9999, ref totalRecords, -1,"");

            List<int> ids = new List<int>();
            foreach (CalcGetCalculationListResult item in employeeList)
            {
                ids.Add(item.EmployeeId.Value);
            }
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            //if (CalculationManager.IsCalculationSaved(payrollPerodId) != null)
            //{
            //    divWarningMsg.InnerHtml = "Salary already saved for this period.";
            //    divWarningMsg.Hide = false;
            //    return;
            //}


            if (CommonManager.GetLastPayrollPeriod().PayrollPeriodId != payrollPerodId)
            {
                divWarningMsg.InnerHtml = "Add on can be added for last payroll period only.";
                divWarningMsg.Hide = false;
                return;
            }

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }


                int employeeId;
                float? value;
                //float? Days_At_Palati, Days_At_Kirne, On_Call, Shift_Afternoon, Shift_Night, Split_Shift_Morning, Split_Shift_Evening, OT_150, OT_200, OT_300, Public_Holiday,
                //    Other_Leave, Snacks_Allowance, Special_Compensation_Allowance, Meal_Allowance, TADA, Vehicle_Allowance;

                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                int totalMismatchEmployeeCount = 0;


                try
                {
                    List<PartialPaidDetail> amountList = new List<PartialPaidDetail>();
                    List<CalcGetHeaderListResult> headerList = CalculationManager.GetPartialTaxPaidHeaderList(payrollPerodId, true);



                    string name = "";
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false
                             && (row["Employee"].Equals(DBNull.Value) || string.IsNullOrEmpty(row["Employee"].ToString()))
                            )
                        {
                            continue;
                        }

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        name = row["Employee"].ToString();



                        importedCount += 1;

                        foreach (var header in headerList)
                        {
                            value = ExcelGenerator.GetValueFromCell(header.HeaderName, row, employeeId);
                            if (value == null)
                                value = (float)0;
                            //if (value != null)
                            {

                                PartialPaidDetail projectPay = new PartialPaidDetail();
                                projectPay.EmployeeId = employeeId;
                                projectPay.PartialPaidHeader = new PartialPaidHeader();
                                projectPay.PartialPaidHeader.Type = header.Type.Value;
                                projectPay.PartialPaidHeader.SourceId = header.SourceId.Value;

                                projectPay.Amount = (decimal)value;



                                //if (value.Value < 0 && isDeduction == true)
                                //{
                                //    divWarningMsg.InnerHtml = string.Format("Employee {0} contains negative value, so please correct it & import the excel.", name);
                                //    divWarningMsg.Hide = false;
                                //    return;
                                //}

                                amountList.Add(projectPay);
                            }
                        }



                    }

                    int count = 0;
                    //ResponseStatus status = PayManager.SaveVariableIncome(amountList, isDeduction, ref count);

                    count = PayManager.SavePartialtaxList(amountList, payrollPerodId, amountList.Select(x=>x.EmployeeId).Distinct().ToList());


                    //if (status.IsSuccessType == false)
                    //{

                    //    divWarningMsg.InnerHtml = status.ErrorMessage;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}


                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                                                            importedCount);
                    divMsgCtl.Hide = false;


                    this.HasImport = true;

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
     
       
    }

   
}

