﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class CumulativeSummaryExcel : BasePage
    {
        int payrollPeriodID;


        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            payrollPeriodID = int.Parse(Request.QueryString["PayrollPeriodId"]);

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/CumulativeImport.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
           // List<DAL.GetForTaxHistoryResult> list = TaxManager.GetForTaxHistory("", 0, 999999, ref total);

            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(payrollPeriodID);


            int totalRecords = 0;
            List<Report_GetCumulativeSummaryResult> list = EmployeeManager.GetCumulativeSummaryReport
                (-1, -1, -1, -1, -1, 0, 9999, "", ref totalRecords
                , payrollPeriod.PayrollPeriodId, (int)PageViewType.Admin, SessionManager.CurrentLoggedInEmployeeId);


            ExcelGenerator.ExportCumulativeSummary(template, list, payrollPeriod.Name);

        }


        

        

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<CumulativeSummary> list = new List<CumulativeSummary>();

                int employeeId;
                double days;// grossAmount,pf,cit,insurance,sst,tds;
               
              
                string empList = "",value = "";
                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        DAL.CumulativeSummary item = new CumulativeSummary();


                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.EmployeeId = employeeId;




                        value = row["Late Days"].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (double.TryParse(value, out days) == false)
                            {
                                divWarningMsg.InnerHtml = "Invalid Late days in row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            item.LateDays = days;
                        }

                        value = row["Leave Days"].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (double.TryParse(value, out days) == false)
                            {
                                divWarningMsg.InnerHtml = "Invalid Leave days in row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            item.LeaveDays = days;
                        }

                        value = row["Half Days Leave Count"].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (double.TryParse(value, out days) == false)
                            {
                                divWarningMsg.InnerHtml = "Invalid Half days leave count in row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            item.HalfDayLeaveDaysCount = days;
                        }

                        item.PayrollPeriodId = payrollPeriodID;

                        item.CreatedBy = SessionManager.CurrentLoggedInUserID;
                        item.CreatedOn = DateTime.Now;
                        item.ModifiedBy = SessionManager.CurrentLoggedInUserID;
                        item.ModifiedOn = DateTime.Now;


                        list.Add(item);

                        importedCount += 1;

                    }

                   


                    //xml.Append("</root>");

                    Status status = AttendanceManager.SaveUpdateCumulativeSummary(list, payrollPeriodID);

                    if (status.IsSuccess == false)
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

