﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Web.CP
{
    public partial class HolidayImport : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            //if (SessionManager.User.URole.RoleId != (int)Role.Administrator)
            //{
            //    Response.Write("Not enough permisson.");
            //    Response.End();
            //    return;
            //}

        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"), true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/HolidaysImport.xlsx");

            List<HolidayGroup> lst = new List<HolidayGroup>();
            lst = CommonManager.GetAllHolidayGroups();
            lst.Add(new HolidayGroup() { GroupId = -1, Name = "All" });
            lst.Add(new HolidayGroup() { GroupId = 0, Name = "Female" });

            string[] listApply = lst.OrderBy(x => x.GroupId).Select(x => x.Name).ToArray();

            string path = HttpContext.Current.Server.MapPath(template);
            string copyPath = Path.GetDirectoryName(path) + "\\" + Guid.NewGuid() + ".xlsx";
            File.Copy(path, copyPath);

            string excelConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + copyPath + ";Extended Properties='Excel 12.0 Xml;HDR=Yes'";

            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelConnection))
                {
                    // Open the connection.
                    conn.Open();
                    ExcelGenerator _objExcelGenerator = new ExcelGenerator();
                    ExcelGenerator.WriteIntoSheet(conn, "AppliesList", listApply);
                }
            }

            catch (Exception ex)
            {
                divWarningMsg.InnerHtml = ex.ToString();
                divWarningMsg.Hide = false;
            }
            ExcelGenerator.WriteFile(copyPath, Path.GetFileName(template));

        }

        private List<int> GetEmployeeIdList()
        {
            return EmployeeManager.GetAllEmployeeID();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);

                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<HolidayGroup> listHolidayApplieList = CommonManager.GetAllHolidayGroups().ToList();
                int importedCount = 0;

                List<HolidayListImport> list = new List<HolidayListImport>();
                string holidayGroup = "";
                DateTime Fromdate, Todate;
                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        importedCount += 1;

                        HolidayListImport obj = new HolidayListImport();
                        if (!string.IsNullOrEmpty(row["Name"].ToString().Trim()))
                            obj.Description = row["Name"].ToString().Trim();

                        obj.CompanyId = SessionManager.CurrentCompanyId;

                        holidayGroup = row["Applies To"].ToString().Trim();
                        if (!string.IsNullOrEmpty(holidayGroup))
                        {
                            if (holidayGroup.ToLower() == "all")
                                obj.AppliesTo = -1;
                            else if (holidayGroup.ToLower() == "female")
                                obj.AppliesTo = 0;
                            else
                            {
                                HolidayGroup objHolidayGroup = listHolidayApplieList.SingleOrDefault(x => x.Name.ToLower() == holidayGroup.ToLower());
                                if (objHolidayGroup == null)
                                {
                                    divWarningMsg.InnerHtml = "Invalid holiday group name on Applies To column for row number " + rowNumber + ".";
                                    divWarningMsg.Hide = false;
                                    return;
                                }
                                obj.AppliesTo = objHolidayGroup.GroupId;
                            }
                        }

                        if (string.IsNullOrEmpty(row["From English Date"].ToString().Trim()))
                        {
                            divWarningMsg.InnerHtml = "From date can't be empty in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                        }

                        if (!string.IsNullOrEmpty(row["From English Date"].ToString().Trim()))
                        {
                            if (DateTime.TryParse(row["From English Date"].ToString().Trim(), out Fromdate) == false)
                            {
                                divWarningMsg.InnerHtml = "Invalid from date for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            obj.FromDate = Fromdate;
                            if (string.IsNullOrEmpty(row["To English Date"].ToString().Trim()))
                                obj.ToDate = Fromdate;
                        }


                        if (!string.IsNullOrEmpty(row["To English Date"].ToString().Trim()))
                        {
                            if (DateTime.TryParse(row["To English Date"].ToString().Trim(), out Todate) == false)
                            {
                                divWarningMsg.InnerHtml = "Invalid to date for row number " + rowNumber + ".";
                                divWarningMsg.Hide = false;
                                return;
                            }
                            obj.ToDate = Todate;
                        }

                        if (obj.FromDate > obj.ToDate)
                        {
                            divWarningMsg.InnerHtml = "Invalid to date range on row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }


                        if (!string.IsNullOrEmpty(row["National Holiday"].ToString().Trim()))
                        {
                            if (row["National Holiday"].ToString().ToLower() == "yes")
                                obj.IsNationalHoliday = true;
                            if (holidayGroup.ToLower() == "no")
                                obj.IsNationalHoliday = false;
                        }
                        list.Add(obj);
                    }

                    importedCount = list.Count();
                    Status status = NewHRManager.ImportHolidayList(list);
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = "Import successfully of " + importedCount + " rows."; //string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;
                        this.HasImport = true;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                        return;
                    }

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }



    }


}

