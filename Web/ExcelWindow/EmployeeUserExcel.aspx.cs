﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class EmployeeUserExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/EmployeeUser.xlsx");        
                 
            int total=0;
            // Retrieve data from SQL Server table.
            List<GetUserListResult> list = UserManager.GetUserList("", "", true, 0, 2000, ref total, true)
                .OrderBy(x => x.EmployeeId).ToList();

            ExcelGenerator.ExportUserEmployee(template, list);
        }




        private List<int> GetEmployeeIdList()
        {
            int total = 0;
            // Retrieve data from SQL Server table.
            List<GetUserListResult> list = UserManager.GetUserList("", "", true, 0, 2000, ref total, true);
            List<int> ids = new List<int>();
            foreach (GetUserListResult item in list)
            {
                ids.Add(item.EmployeeId.Value);
            }
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

               
                int employeeId;
                //decimal? grossAmount,pf,cit,insurance,sst,tds;

                string userName, activeDirectoryUserName;
                bool sendPwdInEmail;
                string password = "";
                List<int> employeeIDList = GetEmployeeIdList();
                List<GetUserListResult> users = new List<GetUserListResult>();
                int importedCount = 0;
                Dictionary<string, int> usernames = new Dictionary<string, int>();
                Dictionary<string, int> activeUserNames = new Dictionary<string, int>();

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        password = "";
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                            //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            //divWarningMsg.Hide = false;
                            //return;
                        }

                        if (DBNull.Value.Equals(row["User Name"]))
                        {
                            divWarningMsg.InnerHtml = "User name is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if (row["User Name"].ToString().Trim() == "")
                        {
                            divWarningMsg.InnerHtml = "User name is required for the employee having id : " + employeeId;
                            divWarningMsg.Hide = false;
                            return;
                        }


                        GetUserListResult user = new GetUserListResult();
                        user.EmployeeId = employeeId;
                        user.UserName = row["User Name"].ToString().Trim();

                        if (!row["Password"].Equals(DBNull.Value))
                        {
                            user.Password = row["Password"].ToString();
                        }

                        user.ActiveDirectoryName = row["Active Directory User Name"].ToString().Trim();

                        // if user name is repeated
                        if (!string.IsNullOrEmpty(user.UserName))
                        {
                            if (usernames.ContainsKey(user.UserName.ToLower()))
                            {
                                divWarningMsg.InnerHtml = string.Format("\"{0}\" user name already set for the Employee having ID {1}.",
                                   user.UserName,usernames[user.UserName.ToLower()] );
                                divWarningMsg.Hide = false;
                                return;
                            }
                            else
                                usernames[user.UserName.ToLower()] = employeeId;
                        }
                        // if AD is repeated
                        if (!string.IsNullOrEmpty(user.ActiveDirectoryName))
                        {
                            if (activeUserNames.ContainsKey(user.ActiveDirectoryName.ToLower()))
                            {
                                divWarningMsg.InnerHtml = string.Format("\"{0}\" AD name already set for the Employee having ID {1}.",
                                   user.ActiveDirectoryName, activeUserNames[user.ActiveDirectoryName.ToLower()]);
                                divWarningMsg.Hide = false;
                                return;
                            }
                            else
                                activeUserNames[user.ActiveDirectoryName.ToLower()] = employeeId;
                        }
                        users.Add(user);

                        //importedCount += 1;

                        

                        //grossAmount = ExcelGenerator.GetAmountValueFromCell("Opening Gross Amount", row, employeeId);
                        ////Days_At_Kirne = ExcelGenerator.GetValueFromCell("Days_At_Kirne", row, employeeId);

                        //pf = ExcelGenerator.GetAmountValueFromCell("Opening PF", row, employeeId);

                        //cit = ExcelGenerator.GetAmountValueFromCell("Opening CIT", row, employeeId);
                        //insurance = ExcelGenerator.GetAmountValueFromCell("Opening Insurance", row, employeeId);
                        //sst = ExcelGenerator.GetAmountValueFromCell("Opening SST", row, employeeId);
                        //tds = ExcelGenerator.GetAmountValueFromCell("Opening Tax Paid", row, employeeId);


                        //xml.Append(
                        //    string.Format("<row EmployeeId=\"{0}\" Amount=\"{1}\" PF=\"{2}\" CIT=\"{3}\"  Insurance=\"{4}\"  TaxPaid=\"{5}\" OpeningSST=\"{6}\"  /> ",
                        //         employeeId, grossAmount, pf, cit, insurance, tds, sst)
                        //    );

                    
                      

                    }



                    int imported = 0;
                    string msg;
                    msg =  UserManager.SaveImportedUser(users, ref imported);

                    if (!string.IsNullOrEmpty(msg))
                    {
                        divWarningMsg.InnerHtml = msg;
                        divWarningMsg.Hide = false;
                        return;
                    }

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, imported);
                    divMsgCtl.Hide = false;

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

