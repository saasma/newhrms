﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;

namespace Web.CP
{
    public partial class ShiftAllowanceExcel : BasePage
    {
        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                {
                    return false;
                }
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomId = UrlHelper.GetIdFromQueryString("payrollPeriod");
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdf", "var hasImport = " + (HasImport ? "true;" : "false;"), true);
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {
            var template = ("~/App_Data/ExcelTemplate/HPLAllowance.xlsx");
            var list = HPLAllowanceManager.HPL_GetEmployeeForAllowance(CustomId);

            ExcelGenerator.ExportShiftAllowanceExcel(template, list);
        }


        private List<int> GetEmployeeIdList()
        {
            var list = HPLAllowanceManager.HPL_GetEmployeeForAllowance(CustomId);
            var ids = new List<int>();
            foreach (HPL_GetEmployeeForAllowanceResult item in list)
            {
                ids.Add(item.EIN);
            }
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            var template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                var datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                var xml = new StringBuilder();
                xml.Append("<root>");

                int employeeId;
                float? Days_At_Palati, On_Call, Shift_Afternoon, Shift_Night, Split_Shift_Morning, Split_Shift_Evening, OT_150, OT_200, OT_300, Public_Holiday,
                Other_Leave, Snacks_Allowance, Special_Compensation_Allowance, Meal_Allowance, TADA, Vehicle_Allowance;
                var employeeIDList = GetEmployeeIdList();

                var importedCount = 0;

                try
                {
                    foreach (DataRow row in datatable.Rows)
                    {
                        int.TryParse(row["EIN"].ToString(), out employeeId);
                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            divWarningMsg.Hide = false;
                            return;
                        }

                        importedCount += 1;

                        Days_At_Palati = ExcelGenerator.GetValueFromCell("Days_At_Palati", row, employeeId);

                        On_Call = ExcelGenerator.GetValueFromCell("On_Call", row, employeeId);

                        Shift_Afternoon = ExcelGenerator.GetValueFromCell("Shift_Afternoon", row, employeeId);
                        Shift_Night = ExcelGenerator.GetValueFromCell("Shift_Night", row, employeeId);
                        Split_Shift_Morning = ExcelGenerator.GetValueFromCell("Split_Shift_Morning", row, employeeId);
                        Split_Shift_Evening = ExcelGenerator.GetValueFromCell("Split_Shift_Evening", row, employeeId);

                        OT_150 = ExcelGenerator.GetValueFromCell("OT_150", row, employeeId);
                        OT_200 = ExcelGenerator.GetValueFromCell("OT_200", row, employeeId);
                        OT_300 = ExcelGenerator.GetValueFromCell("OT_300", row, employeeId);
                        Public_Holiday = ExcelGenerator.GetValueFromCell("Public_Holiday", row, employeeId);

                        Other_Leave = ExcelGenerator.GetValueFromCell("Other_Leave", row, employeeId);

                        Snacks_Allowance = ExcelGenerator.GetValueFromCell("Snacks_Allowance", row, employeeId);
                        Special_Compensation_Allowance = ExcelGenerator.GetValueFromCell("Special_Compensation_Allowance", row, employeeId);
                        Meal_Allowance = ExcelGenerator.GetValueFromCell("Meal_Allowance", row, employeeId);
                        TADA = ExcelGenerator.GetValueFromCell("TADA", row, employeeId);
                        Vehicle_Allowance = ExcelGenerator.GetValueFromCell("Vehicle_Allowance", row, employeeId);

                        xml.AppendFormat(
                           @"<row PayrollPeriodId='{0}' EmployeeId='{1}' 
                                        {2} {3} 
                                        {4}

                                        {5} {6} {7} {8}

                                        {9} {10} {11} {12} 
                                        {13}
                                        {14} {15} {16} {17} {18} />",
                                        CustomId, employeeId,

                                   ExcelGenerator.GetColumnValue("Days_At_Palati", Days_At_Palati),
                                        ExcelGenerator.GetColumnValue("Days_At_Kirne", (decimal)0),

                                        ExcelGenerator.GetColumnValue("On_Call", On_Call),

                                        ExcelGenerator.GetColumnValue("Shift_Afternoon", Shift_Afternoon),
                                        ExcelGenerator.GetColumnValue("Shift_Night", Shift_Night),
                                        ExcelGenerator.GetColumnValue("Split_Shift_Morning", Split_Shift_Morning),
                                        ExcelGenerator.GetColumnValue("Split_Shift_Evening", Split_Shift_Evening),

                                        ExcelGenerator.GetColumnValue("OT_150", OT_150),
                                        ExcelGenerator.GetColumnValue("OT_200", OT_200),
                                        ExcelGenerator.GetColumnValue("OT_300", OT_300),
                                        ExcelGenerator.GetColumnValue("Public_Holiday", Public_Holiday),

                                        ExcelGenerator.GetColumnValue("Other_Leave", Other_Leave),

                                        ExcelGenerator.GetColumnValue("Snacks_Allowance", Snacks_Allowance),
                                        ExcelGenerator.GetColumnValue("Special_Compensation_Allowance", Special_Compensation_Allowance),
                                        ExcelGenerator.GetColumnValue("Meal_Allowance", Meal_Allowance),
                                        ExcelGenerator.GetColumnValue("TADA", TADA),
                                        ExcelGenerator.GetColumnValue("Vehicle_Allowance", Vehicle_Allowance));
                    }



                    xml.Append("</root>");

                    HPLAllowanceManager.HPL_SaveAllowance(xml.ToString()
                    , CustomId);

                    divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                    divMsgCtl.Hide = false;

                    HasImport = true;
                }
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
        }
    }
}

