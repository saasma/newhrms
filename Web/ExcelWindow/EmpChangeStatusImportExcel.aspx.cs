﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using BLL.Entity;
using Utils.Calendar;

namespace Web.CP
{
    public partial class EmpChangeStatusImportExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

          
        }

       
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/StatusImport.xlsx");

            List<HR_Service_Event> eventTypes = CommonManager.GetServieEventTypes();
            List<KeyValue> statusList = (new JobStatus()).GetMembers();
            statusList.RemoveAt(0);

            ExcelGenerator.WriteEmpChangeStatusExportSheet(template, eventTypes, statusList);
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xlsx");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xlsx"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                List<ECurrentStatus> list = new List<ECurrentStatus>();

                int employeeId;             

                object value;

                List<HR_Service_Event> eventTypes = CommonManager.GetServieEventTypes();
                List<KeyValue> statusList = (new JobStatus()).GetMembers();

                List<int> employeeIds = EmployeeManager.GetAllEmployeeID();

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        ECurrentStatus obj = new ECurrentStatus();

                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!employeeIds.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN for EIN : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        obj.EmployeeId = employeeId;

                        value = row["From Date"]; 
                        if (value == DBNull.Value || string.IsNullOrEmpty(value.ToString().Trim()))
                        {
                            divWarningMsg.InnerHtml = "From Date is required for EIN : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        obj.FromDate = value.ToString();

                        value = row["From Date Eng"];
                        if (value == DBNull.Value || string.IsNullOrEmpty(value.ToString().Trim()))
                        {
                            divWarningMsg.InnerHtml = "From Date Eng is required for EIN : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        try
                        {
                            obj.FromDateEng = DateTime.Parse(value.ToString());
                        }
                        catch (Exception ex)
                        {
                            divWarningMsg.InnerHtml = "Invalid From Date Eng for EIN : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }
                        

                        value = row["To Date"];

                        if (value != DBNull.Value && !string.IsNullOrEmpty(value.ToString().Trim()))
                        {
                            obj.ToDate = value.ToString();
                        }

                        value = row["To Date Eng"];

                        if (value != DBNull.Value && !string.IsNullOrEmpty(value.ToString().Trim()))
                        {
                            try
                            {
                                obj.ToDateEng = DateTime.Parse(value.ToString());
                                obj.DefineToDate = true;
                            }
                            catch (Exception ex)
                            {
                                divWarningMsg.InnerHtml = "Invalid To Date Eng for EIN : " + employeeId + " of row " + rowNumber;
                                divWarningMsg.Hide = false;
                                return;
                            }                           
                        }

                        if (!string.IsNullOrEmpty(obj.ToDate) && (obj.ToDateEng == null || obj.ToDateEng == new DateTime()))
                        {
                            divWarningMsg.InnerHtml = "To Date Eng is required for EIN : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        if(string.IsNullOrEmpty(obj.ToDate) && obj.ToDateEng != null && obj.ToDateEng != new DateTime())
                        {
                            divWarningMsg.InnerHtml = "To Date is required for EIN : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        value = row["Status"];

                        if (value == DBNull.Value || string.IsNullOrEmpty(value.ToString().Trim()))
                        {
                            divWarningMsg.InnerHtml = "Status is required for EIN : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        try
                        {
                            obj.CurrentStatus = Convert.ToInt32(statusList.SingleOrDefault(x => x.Value.ToString().ToLower().Trim() == value.ToString().ToLower().Trim()).Key.ToString());
                        }
                        catch (Exception ex)
                        {
                            divWarningMsg.InnerHtml = "Invalid Status for EIN : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }


                        value = row["Event Type"];
                        if (value == DBNull.Value || string.IsNullOrEmpty(value.ToString().Trim()))
                        {
                            divWarningMsg.InnerHtml = "Event Type is required for EIN : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        try
                        {
                            obj.EventID = eventTypes.SingleOrDefault(x => x.Name.ToLower().Trim() == value.ToString().ToLower().Trim()).EventID;
                        }
                        catch (Exception ex)
                        {
                            divWarningMsg.InnerHtml = "Invalid Event Type for EIN : " + employeeId + " of row " + rowNumber;
                            divWarningMsg.Hide = false;
                            return;
                        }

                        value = row["Note"];
                        if (value != DBNull.Value && string.IsNullOrEmpty(value.ToString().Trim()))
                            obj.Note = value.ToString();

                        list.Add(obj);

                    }

                    ResponseStatus status = EmployeeManager.SaveEmployeeChangeStatusImport(list);

                    if (status.IsSuccessType)
                    {
                        divMsgCtl.InnerHtml = "Employee status change imported successfully.";
                        divMsgCtl.Hide = false;

                        this.HasImport = true;  
                    }
                    else
                    {

                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                    }
                    

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
       
     
       
    }

   
}

