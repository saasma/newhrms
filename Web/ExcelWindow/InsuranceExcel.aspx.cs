﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Utils.Calendar;

namespace Web.CP
{
    public partial class InsuranceExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        int departmentId = -1;
        int yearId = -1;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["DepartmentId"] != null)
                departmentId = int.Parse(Request.QueryString["DepartmentId"]);

            yearId = int.Parse(Request.QueryString["yearid"]);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }
       
       


        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/InsuranceImport.xlsx");        
                 
            int total=0;

            string[] GetProjectList = new InsuranceManager().GetInsuranceInstitution().Select(x => x.InstitutionName).ToArray();

            List<IIndividualInsurance> list = InsuranceManager.GetCurrentYearInsuranceList(yearId,0, int.MaxValue, ref total, "", 0);


            ExcelGenerator.WriteInsuranceExcel(template, GetProjectList, list);
        }




       

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            int total = 0;
            

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

               
                int employeeId;

                string value;

                List<IIndividualInsurance> list = new List<IIndividualInsurance>();
                int importedCount = 0;
                

                try
                {
                    int rowNumber = 1;
                    int insuranceId ;
                    foreach (DataRow row in datatable.Rows)
                    {
                        IIndividualInsurance item = new IIndividualInsurance();
                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        
                        item.EmployeeId = employeeId;

                        if (!string.IsNullOrEmpty(row["Insurance ID"].ToString()))
                        {
                            int.TryParse(row["Insurance ID"].ToString(), out insuranceId);
                            //if (int.TryParse(row["Insurance ID"].ToString(), out insuranceId) == false)
                            //{
                            //    divWarningMsg.InnerHtml = "Invalid insurance id in row number " + rowNumber + ".";
                            //    divWarningMsg.Hide = false;
                            //    return;
                            //}
                            item.Id = insuranceId;
                        }

                        //value = row["Insurance Type"].ToString().Trim().ToLower();
                        //if (value != "personal insurance" && value != "family insurance")
                        //{
                        //    divWarningMsg.InnerHtml = "Invalid insurance type in row number " + rowNumber + ".";
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}
                        //item.IsPersonalInsurance = value == "personal insurance" ? true : false;

                        value = row["Insurance Company"].ToString().Trim().ToLower();
                        if (!string.IsNullOrEmpty(value))
                        {
                            IInsuranceInstitution incComp = InsuranceManager.GetInsuranceCompanyByName(value);
                            if (incComp != null)
                                item.InstitutionId = incComp.Id;
                            else
                                item.InstitutionId = -1;
                        }
                        else
                            item.InstitutionId = -1;

                        item.FinancialYearId = SessionManager.CurrentCompanyFinancialDate.FinancialDateId;
                        //item.StartDate = SessionManager.CurrentCompanyFinancialDate.StartingDate;
                        //item.StartDateEng = SessionManager.CurrentCompanyFinancialDate.StartingDateEng;
                        //item.EndDate = SessionManager.CurrentCompanyFinancialDate.EndingDate;
                        //item.EndDateEng = SessionManager.CurrentCompanyFinancialDate.EndingDateEng;

                        value = row["Start Date"].ToString().Trim();
                        DateTime date = DateTime.Now;
                        if (DateTime.TryParse(value, out date))
                            item.StartDateEng = date;

                        value = row["End Date"].ToString().Trim();
                        if (DateTime.TryParse(value, out date))
                            item.EndDateEng = date;

                        item.Note = row["Note"].ToString();

                        string healthValue = row["Health Insurance"].ToString();
                        if (healthValue.Trim().ToLower().Equals("yes"))
                            item.IsHealthInsurance = true;
                        //string start = row["Policy Start"].ToString().Trim().ToLower();
                        ////if (string.IsNullOrEmpty(start))
                        ////{
                        ////    divWarningMsg.InnerHtml = "Policy Start is required in row number " + rowNumber + ".";
                        ////    divWarningMsg.Hide = false;
                        ////    return;
                        ////}
                        //if (!string.IsNullOrEmpty(start))
                        //{
                        //    try
                        //    {
                        //        CustomDate date1 = CustomDate.GetCustomDateFromString(start, IsEnglish);
                        //        item.StartDate = new CustomDate(1, date1.Month, date1.Year, IsEnglish).ToString();
                        //        item.StartDateEng = GetEngDate(item.StartDate);

                        //    }
                        //    catch
                        //    {
                        //        //divWarningMsg.InnerHtml = "Invalid Policy start date in row number " + rowNumber + ".";
                        //        //divWarningMsg.Hide = false;
                        //        //return;
                        //    }
                        //}
                        //string end = row["Policy End"].ToString().Trim().ToLower();
                        //if (!string.IsNullOrEmpty(end))
                        //{
                        //    try
                        //    {
                        //        CustomDate date1 = CustomDate.GetCustomDateFromString(end, IsEnglish);
                        //        item.EndDate = new CustomDate(DateHelper.GetTotalDaysInTheMonth(date1.Year, date1.Month, IsEnglish)
                        //            , date1.Month, date1.Year, IsEnglish).ToString();
                        //        item.EndDateEng = GetEngDate(item.EndDate);

                        //    }
                        //    catch
                        //    {
                        //        //divWarningMsg.InnerHtml = "Invalid Policy start date in row number " + rowNumber + ".";
                        //        //divWarningMsg.Hide = false;
                        //        //return;
                        //    }
                        //}

                        ////if (item.StartDateEng > item.EndDateEng)
                        ////{
                        ////    divWarningMsg.InnerHtml = "Policy start can not be greater than end date in row number " + rowNumber + ".";
                        ////    divWarningMsg.Hide = false;
                        ////    return;
                        ////}

                        value = row["Paid By"].ToString().Trim().ToLower();
                        if (value == "company")
                        {
                            item.EmployeePaysPercent = 0;
                            item.EmployerPaysPercent = 100;
                        }
                        else //if (value == "employee")
                        {
                            item.EmployeePaysPercent = 100;
                            item.EmployerPaysPercent = 0;
                        }
                        
                        item.PolicyNo = row["Policy Number"].ToString().Trim();

                        decimal amount;
                        value = row["Policy Amount"].ToString().Trim().ToLower();
                        if (decimal.TryParse(value, out amount)==false)
                        {
                            //divWarningMsg.InnerHtml = "Invalid policy amount in row number " + rowNumber + ".";
                            //divWarningMsg.Hide = false;
                           // return;
                        }
                        item.PolicyAmount = amount;

                        value = row["Premium"].ToString().Trim().ToLower();
                        if (decimal.TryParse(value, out amount) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid premium amount in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        item.Premium = amount;


                        //value = row["Status"].ToString().Trim().ToLower();

                        //if (value != "expire" && value != "un expire")
                        //{
                        //    divWarningMsg.InnerHtml = "Status can be only \"Expire\" or \"Un Expire\" in row number " + rowNumber + ".";
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}

                        //if (value == "expire")
                        //    item.IsExpired = true;
                        //else
                        //    item.IsExpired = false;

                        list.Add(item);

                        importedCount += 1;

                        

                      

                    }



                    int imported = 0;
                    string msg;
                    Status status =  InsuranceManager.SaveImportedInsurance(list);

                    //if (!string.IsNullOrEmpty(msg))
                    //{
                    //    divWarningMsg.InnerHtml = msg;
                    //    divWarningMsg.Hide = false;
                    //    return;
                    //}
                    if (status.IsSuccess)
                    {
                        divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = status.ErrorMessage;
                        divWarningMsg.Hide = false;
                    }

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

