﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;

namespace Web.CP
{
    public partial class ProjectExcel : BasePage
    {

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        public int IncomeId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.IncomeId = UrlHelper.GetIdFromQueryString("incomeid");  
        
            if (!IsPostBack)
            {
                this.CustomId = UrlHelper.GetIdFromQueryString("payrollPeriod");                
            }

            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/ProjectContribution.xlsx");



            List<Project> projects = ProjectManager.GetProjectsForLatestPayroll();
            //List<GetValidEmployeesForPayrollPeriodResult> employees = EmployeeManager.GetValidEmployeesForPayrollPeriod(
            //    this.CustomId, -1, -1).OrderBy(ee => ee.EmployeeId).ToList();

            List<EEmployee> employees = ProjectManager.GetProjectPaysEmployeeList(this.CustomId
               , IncomeId);

            List<ProjectPayBO> projectPays = ProjectManager.GetProjectPays(this.CustomId, IncomeId);


            // Retrieve data from SQL Server table.
            ExcelGenerator.WriteProjectAndExportExcel(template, projects, employees, projectPays);


        }


        

        private List<int> GetEmployeeIdList()
        {
            List<EEmployee> employees = ProjectManager.GetProjectPaysEmployeeList(this.CustomId
           , IncomeId);

         
            List<int> ids = new List<int>();
            foreach (EEmployee item in employees)
            {
                ids.Add(item.EmployeeId);
            }
            return ids;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");


            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

            
                int employeeId;
                float? value;
                //float? Days_At_Palati, Days_At_Kirne, On_Call, Shift_Afternoon, Shift_Night, Split_Shift_Morning, Split_Shift_Evening, OT_150, OT_200, OT_300, Public_Holiday,
                //    Other_Leave, Snacks_Allowance, Special_Compensation_Allowance, Meal_Allowance, TADA, Vehicle_Allowance;
               
                List<int> employeeIDList = GetEmployeeIdList();

                int importedCount = 0;
                int totalMismatchEmployeeCount = 0;
                float totalHoursOrDaysInAMonth = ProjectManager.TotalHourOrDaysInAMonth;

                if (!CommonManager.CompanySetting.IsProjectInputType)
                    totalHoursOrDaysInAMonth = 100;

                try
                {
                    List<ProjectPay> projectPays = new List<ProjectPay>();
                    List<Project> projects = ProjectManager.GetProjectsForLatestPayroll();
                    projects.RemoveAt(0);
                    string name = "";
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {

                        rowNumber += 1;
                        if (int.TryParse(row["EIN"].ToString(), out employeeId) == false)
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;
                        }
                        name = row["Employee"].ToString();



                           //if (!employeeIDList.Contains(employeeId))
                        //{
                        //    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                        //    divWarningMsg.Hide = false;
                        //    return;
                        //}

                        importedCount += 1;

                        float totalHoursOrDays = 0;
                        foreach (var project in projects)
                        {
                            value = ExcelGenerator.GetValueFromCell(project.Name, row, employeeId);
                            if (value == null)
                                value = (float)0;
                            if (value != null)
                            {

                                ProjectPay projectPay = new ProjectPay();
                                projectPay.EmployeeId = employeeId;
                                projectPay.PayrollPeriodId = this.CustomId;
                                projectPay.ProjectId = project.ProjectId;
                                projectPay.ProjectName = project.Name;
                                projectPay.HoursOrDays = value;

                                totalHoursOrDays += value.Value;




                                if (value.Value < 0)
                                {
                                    divWarningMsg.InnerHtml = string.Format("Employee {0} contains negative value, so please correct it & import the excel.", name);
                                    divWarningMsg.Hide = false;
                                    return;
                                }

                                projectPays.Add(projectPay);
                            }
                        }


                        // add to total mis match employee count for showing message
                        if (totalHoursOrDays != totalHoursOrDaysInAMonth)
                            totalMismatchEmployeeCount += 1;



                    }
        

                    ProjectManager.SaveProjectPay(projectPays
                                                              , this.CustomId,this.IncomeId);

                    if (totalMismatchEmployeeCount == 0)
                    {
                        divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                                                            importedCount);
                        divMsgCtl.Hide = false;
                    }
                    else
                    {
                        divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees,
                                                                importedCount)
                                                  + string.Format(" For {0} employees, total value {1} doesn't match, please check in the details view.",totalMismatchEmployeeCount,totalHoursOrDaysInAMonth);
                        divWarningMsg.Hide = false; 
                    }

                    this.HasImport = true;  

                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                    //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }              
            }
        }
        
     
       
    }

   
}

