﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;
using Utils.Web;
using Utils.Helper;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL.BO;
using Web.Controls;

namespace Web.CP
{
    public partial class IncomeDeductionAdjustmentExcel : BasePage
    {
        int payrollPerodId = 0;
        int typeId=0;
        int sourceValueId=0;
        KeyValue keyValue = null;

        public bool HasImport
        {
            get
            {
                if (ViewState["import"] == null)
                    return false;
                return Convert.ToBoolean(ViewState["import"]);
            }
            set
            {
                ViewState["import"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.payrollPerodId = UrlHelper.GetIdFromQueryString("payrollPeriod");

            string valueType = Request.QueryString["type"];
            try
            {
                string[] values = valueType.Split(new char[] { ':' });

                sourceValueId = int.Parse(values[0]);

                typeId = int.Parse(values[1]);
            }
            catch { }

            List<KeyValue> incDedList = PayManager.GetEmployeesIncomeDeductionList(payrollPerodId);

            


            foreach (KeyValue value in incDedList)
            {
                if (value.Value == (sourceValueId + ":" + typeId))
                {
                    keyValue = value;
                    break;
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "sdfdsffd", "window.onunload = null;");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdf", "var hasImport = " + (this.HasImport ? "true;" : "false;"),true);
        }




        protected void btnExport_Click(object sender, EventArgs e)
        {

            string template = ("~/App_Data/ExcelTemplate/IncomeDeductionAdjustment.xlsx");

           
         
            int totalRecords = 0;

            List<GetIncomeDeductionAdjustmentEmployeesResult> list = PayManager.GetIncomeDeductionAdjustment(
            payrollPerodId, -1, -1,
             -1, sourceValueId, typeId,
            0, 99999, ref totalRecords);

          

           if (keyValue != null)
           {
               ExcelGenerator.ExportAdjustment(template, list,keyValue.Key);
           }
        }


        

        private List<int> GetEmployeeIdList()
        {
            int totalRecords = 0;
            List<GetIncomeDeductionAdjustmentEmployeesResult> list = PayManager.GetIncomeDeductionAdjustment(
           payrollPerodId, -1, -1,
            -1, sourceValueId, typeId,
           0, 99999, ref totalRecords);

            List<int> ids = new List<int>();
            foreach (GetIncomeDeductionAdjustmentEmployeesResult item in list)
            {
                ids.Add(item.EmployeeId);
            }
            return ids;
        }

        public bool IsNoSpecialCharacterInNotes(string notes, int empId, string name, WarningMsgCtl divWarningMsg,int index)
        {
            bool contains = false;

            if (notes.Contains("&") || notes.Contains("<") || notes.Contains(">") || notes.Contains("\"") || notes.Contains("'"))
            {
                divWarningMsg.InnerHtml ="Please remove the special character likes (& < > \" ') from the note column for the row  " + index + ".";
                divWarningMsg.Hide=false;
                return true;
            }

            return false;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/App_Data/ExcelTemplate/" + Guid.NewGuid() + ".xls");

            if(keyValue==null)
                return;

            if (fupUpload.HasFile && fupUpload.FileName.ToLower().Contains(".xls"))
            {
                fupUpload.SaveAs(template);


                DataTable datatable = ExcelGenerator.ImportFromExcel(template, "select * from [Sheet1$]");

                if (datatable == null)
                {
                    divWarningMsg.InnerHtml = Resources.Messages.HPL_Allowance_ImportFail;
                    divWarningMsg.Hide = false;
                    File.Delete(template);
                    return;
                }

                StringBuilder xml = new StringBuilder();
                xml.Append("<root>");

                int employeeId; string headerName = "", note = "";
                decimal? adjustment, adjusted, existingAmount;

                List<int> employeeIDList = GetEmployeeIdList();

                List<IncomeDeductionAdjustment> adjustmentList = new List<IncomeDeductionAdjustment>();

                int importedCount = 0;

                try
                {
                    int rowNumber = 1;
                    foreach (DataRow row in datatable.Rows)
                    {
                        rowNumber += 1;
                        int.TryParse(row["EIN"].ToString(), out employeeId);

                        if (!employeeIDList.Contains(employeeId))
                        {
                            divWarningMsg.InnerHtml = "Invalid EIN in row number " + rowNumber + ".";
                            divWarningMsg.Hide = false;
                            return;

                            //divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_EmployeeIDNotFound, employeeId);
                            //divWarningMsg.Hide = false;
                            //return;
                        }

                        importedCount += 1;

                        headerName = row["Income/Deduction Head"].ToString();

                        if (headerName.ToLower().Trim() != keyValue.Key.ToLower().Trim())
                        {
                            divWarningMsg.InnerHtml = "Invalid Income/Deduction Head.";
                            divWarningMsg.Hide = false;
                            return;
                        }

                        existingAmount = ExcelGenerator.GetAmountValueFromCell("Previous Amount", row, employeeId);
                        adjustment = ExcelGenerator.GetAmountValueFromCell("Adjustment", row, employeeId);


                        adjusted = ExcelGenerator.GetAmountValueFromCell("Adjusted Amount", row, employeeId);

                        note = row["Note"].ToString();

                        if (IsNoSpecialCharacterInNotes(note, employeeId, "", divWarningMsg, rowNumber))
                            return;


                        xml.Append(
                            string.Format("<row PayrollPeriodId=\"{0}\" EmployeeId=\"{1}\" IncomeDeductionId=\"{2}\" Type=\"{3}\"  ExistingAmount=\"{4}\" AdjustmentAmount=\"{5}\"   Note=\"{6}\" /> ",

                                payrollPerodId, employeeId, sourceValueId, typeId, existingAmount, adjustment, note.Trim()
                            ));

                    }




                    xml.Append("</root>");

                    if (importedCount > 0)
                    {
                        PayManager.SaveIncomeDeductionAdjustment(xml.ToString(), headerName);

                        divMsgCtl.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ImportedEmployees, importedCount);
                        divMsgCtl.Hide = false;

                        this.HasImport = true;
                    }
                }
                //if invalid value like string found
                catch (BLL.ExcelGenerator.InvalidValue exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_InvalidValue, exp.Value,
                        exp.ColumnName, exp.EIN);
                    divWarningMsg.Hide = false;
                }
                //sheet name changed error
                catch (OleDbException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_SheetNameError);
                    divWarningMsg.Hide = false;
                }
                catch (ArgumentException exp)
                {
                    divWarningMsg.InnerHtml = string.Format(Resources.Messages.HPL_Allowance_ColumnNameChangedError);
                    divWarningMsg.Hide = false;
                }
            }
            else
            {
                divWarningMsg.InnerHtml = "Excel version should be 2007(.xlsx) or higher.";
                divWarningMsg.Hide = false;
            }
        }
        
     
       
    }

   
}

