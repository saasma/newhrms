﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShiftLegendCtl.ascx.cs"
    Inherits="Web.Controls.ShiftLegendCtl" %>
<fieldset style='padding: 10px;padding-left:0px; float: left'>
    <div>
    <asp:DataList ID="dlstLegends" BorderWidth="1" BorderColor="Black" runat="server"
        RepeatColumns="4" RepeatDirection="Horizontal">
        <ItemTemplate>
            <table  style="margin-left:15px;">
                <tr>
                    <td>
                        <div style='border: 1px solid black; width: 10px; height: 10px; background-color: <%# Eval("Color") %>'>
                        </div>
                    </td>
                    <td style="width: 10px;">
                    </td>
                    <td>
                        <span>
                            <%#Eval("Name") %>
                            (<%# Eval("ShortName") %>) </span>
                    </td>
                    <td style="width: 20px;">
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList></div>
</fieldset>
