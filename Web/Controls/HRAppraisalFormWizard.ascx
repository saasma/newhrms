﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HRAppraisalFormWizard.ascx.cs"
    Inherits="Web.Controls.HRAppraisalFormWizard" %>
<div id="menu" style="margin-top: 10px;">
    <ul class="nav nav-justified nav-wizard nav-wizard-success customwiard" id="employeeMenuList"
        runat="server" style="margin-left: 0px; width: 1270px;">
        <li runat="server" id="li1"><a runat="server" isemployeedetails="true" id="menuEmployeePersonalDetails"
            href="~/Appraisal/Form/general_settings_1.aspx">Form_Name</a></li>
        <li runat="server" id="li2"><a runat="server" id="A7" href="~/Appraisal/Form/Introduction_2.aspx">
            Introduction</a></li>
        <li runat="server" id="li3"><a runat="server" id="A2" href="~/Appraisal/Form/Objectives_3.aspx">
            Objectives</a></li>
        <li runat="server" id="li4"><a runat="server" isemployeedetails="true" id="A5" href="~/Appraisal/Form/PerformanceSummary.aspx">
            Performance_Summary</a></li>
        <li runat="server" id="li5"><a runat="server" id="A8" href="~/Appraisal/Form/Activities_4.aspx">
            Activity_Summary</a></li>
        <li runat="server" id="liQualificationAndExp"><a runat="server" id="A12" href="~/Appraisal/Form/QualificationExpirenceHistory.aspx">
            Education,Location,Seniority </a></li>
        <li runat="server" id="li6"><a runat="server" id="A3" href="~/Appraisal/Form/CoreCompetencies_5.aspx">
            Competencies</a></li>
       
    </ul>
    <ul class="nav nav-justified nav-wizard nav-wizard-success customwiard" id="employeeMenuList2"
        runat="server" style="margin-left: 0px; width: 1270px;">
         <li runat="server" id="li66"><a runat="server" id="A11" href="~/Appraisal/Form/Targets_6.aspx">
            Targets</a></li>
        <li runat="server" id="li7"><a runat="server" isemployeedetails="true" id="A4" href="~/Appraisal/Form/Questionnaire.aspx">
            Questionnaire</a></li>
        <li runat="server" id="li8"><a runat="server" isemployeedetails="true" id="A9" href="~/Appraisal/Form/Review_7.aspx">
            Supervisor_Review</a></li>
        <li runat="server" id="li9"><a runat="server" isemployeedetails="true" id="A10" href="~/Appraisal/Form/Summary_8.aspx">
            Comments</a></li>
        <li runat="server" id="li10"><a runat="server" isemployeedetails="true" id="A6" href="~/Appraisal/Form/Signatures_9.aspx">
            Signatures</a></li>
        <li runat="server" id="li11"><a runat="server" isemployeedetails="true" id="A1" href="~/Appraisal/Preview.aspx">
            Preview</a></li>
    </ul>
    <div class="clearfix" style="clear: both">
    </div>
</div>
