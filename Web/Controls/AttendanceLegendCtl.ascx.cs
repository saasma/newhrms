﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;

namespace Web.Controls
{
    public partial class AttendanceLegendCtl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                List<LLeaveType> leaves = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
             

                dlstLegends.DataSource = leaves;
                dlstLegends.DataBind();

                //create legend for hlidays
                dlstLegendsHolidays.DataSource = LeaveAttendanceManager.GetLegendsForHolidays();
                dlstLegendsHolidays.DataBind();
            }
        }
    }
}