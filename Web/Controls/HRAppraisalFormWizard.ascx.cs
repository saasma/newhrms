﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ext.Net;
using BLL.Manager;
using DAL;
using BLL;

namespace Web.Controls
{
    public partial class HRAppraisalFormWizard : BLL.Base.BaseUserControl
    {
        HtmlGenericControl[] menuList = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack && !X.IsAjaxRequest)
                ShowHideEmployeeHRMenus();
        }





        public void ShowHideEmployeeHRMenus()
        {
            bool hasEmployeeId = false;
            int employeeId = 0;
            string pageUrl = CurrentPageUrl.ToLower();


            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                hasEmployeeId = true;
                employeeId = int.Parse(Request.QueryString["fid"]);
                //liQualificationAndExp.Visible = false;
                //if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil)
                //    liQualificationAndExp.Visible = true;
            }

            if (hasEmployeeId)
            {
                foreach (Control parent in employeeMenuList.Controls)
                {


                    {
                        foreach (Control ctl in parent.Controls)
                        {
                            //H
                            HtmlAnchor anchor = ctl as HtmlAnchor;
                            if (anchor != null)
                            {

                                {
                                    anchor.Attributes["href"] += "?fid=" + employeeId;
                                    anchor.Visible = true;

                                    if (anchor.HRef.ToLower().Contains(pageUrl))
                                    {
                                        HtmlGenericControl parent1 = anchor.Parent as HtmlGenericControl;
                                        parent1.Attributes["class"] = "active";
                                    
                                    }


                                }
                            }
                        }
                    }
                }

                foreach (Control parent in employeeMenuList2.Controls)
                {


                    {
                        foreach (Control ctl in parent.Controls)
                        {
                            //H
                            HtmlAnchor anchor = ctl as HtmlAnchor;
                            if (anchor != null)
                            {

                                {
                                    anchor.Attributes["href"] += "?fid=" + employeeId;
                                    anchor.Visible = true;

                                    if (anchor.HRef.ToLower().Contains(pageUrl))
                                    {
                                        HtmlGenericControl parent1 = anchor.Parent as HtmlGenericControl;
                                        parent1.Attributes["class"] = "active";

                                    }


                                }
                            }
                        }
                    }
                }
            }
        }
    }
}