﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleMenu.ascx.cs"
    Inherits="Web.Controls.ModuleMenu" %>
<ul class="nav nav-pills nav-stacked" runat="server" visible="false" id="menuHR">
    <li runat="server" id="liEmployeeList" visible='<%# IsAccessible("newhr/EmployeeListing.aspx") %>'>
        <a href="~/newhr/EmployeeListing.aspx" runat="server"><i class="fa fa-users"></i><span>
            Employee List</span></a></li>
    <li runat="server" id="liTransfer" class="parent"><a href="#"><i class="fa fa-random">
    </i><span>Transfer</span></a>
        <ul class="children">
            <li visible='<%# IsAccessible("CP/BranchTransferList.aspx") %>' runat="server"><a
                runat="server" href="~/cp/BranchTransferList.aspx">Branch Transfer</a></li>
            <li id="Li1" visible='<%# IsAccessible("CP/DepartmentTransferList.aspx") %>' runat="server">
                <a id="A1" runat="server" href="~/cp/DepartmentTransferList.aspx">Department Transfer</a></li>
            <li id="Li2" visible='<%# IsAccessible("CP/LocationTransferList.aspx") %>' runat="server">
                <a id="A2" runat="server" href="~/cp/LocationTransferList.aspx">Location Transfer</a></li>
        </ul>
    </li>
    <li runat="server" id="liDeputation" class="parent"><a href="#"><i class="fa fa-suitcase">
    </i><span>Deputation</span></a>
        <ul class="children">
            <li id="Li3" visible='<%# IsAccessible("newhr/DeputationList.aspx") %>' runat="server">
                <a id="A3" runat="server" href="~/newhr/DeputationList.aspx">Deputation</a></li>
            <li id="acting" visible='<%# IsAccessible("newhr/ActingList.aspx") %>' runat="server">
                <a id="A4" runat="server" href="~/newhr/ActingList.aspx">Acting</a></li>
        </ul>
    </li>
    <li runat="server" id="liCommunication" class="parent"><a href="#"><i class="fa  fa-comments-o">
    </i><span>Communication</span></a>
        <ul class="children">
            <li id="Li4" visible='<%# IsAccessible("cp/NoticeList.aspx") %>' runat="server"><a
                id="A5" runat="server" href="~/cp/NoticeList.aspx">Notice</a></li>
            <li id="Li5" visible='<%# IsAccessible("CP/ComposeMessage.aspx") %>' runat="server">
                <a id="A6" runat="server" href="~/CP/ComposeMessage.aspx">Send Message</a></li>
            <li id="Li6" visible='<%# IsAccessible("CP/Extension/MailMerge.aspx") %>' runat="server">
                <a id="A7" runat="server" href="~/CP/Extension/MailMerge.aspx">Mail Merge</a></li>
        </ul>
    </li>
    <li runat="server" id="liActivity" class="parent"><a href="#"><i class="fa fa-calendar-o"></i><span>Daily Activities</span></a>
        <ul class="children">
            <li id="Li7" visible='<%# IsAccessible("Activity/HRView.aspx") %>' runat="server"><a
                id="A8" runat="server" href="~/Activity/HRView.aspx">Activity Submitted</a></li>
            <li id="Li8" visible='<%# IsAccessible("Activity/DAR_Report.aspx") %>' runat="server">
                <a id="A9" runat="server" href="~/Activity/DAR_Report.aspx">Activity Report</a></li>
            <li id="Li9" visible='<%# IsAccessible("Activity/DAR_History.aspx") %>' runat="server">
                <a id="A10" runat="server" href="~/Activity/DAR_History.aspx">Activity History</a></li>
        </ul>
    </li>
</ul>
<ul class="nav nav-pills nav-stacked" runat="server" visible="false" id="menuSetting">
    <li runat="server" id="li10" visible='<%# IsAccessible("newhr/AllSettings.aspx") %>'>
        <a id="A11" href="~/newhr/AllSettings.aspx" runat="server"><i class="fa fa-users"></i>
            <span>Administrator Tools</span></a>
     </li>
</ul>
<ul class="nav nav-pills nav-stacked" runat="server" visible="false" id="menuPayroll">
    
    <li runat="server"  class="parent"><a href="#"><i class="fa fa-money"></i><span>Pay Run</span></a>
        <ul class="children">
            <li runat="server" visible='<%# IsAccessible("CP/Calculation.aspx") %>'><a runat="server" href="~/CP/Calculation.aspx">Pay Calculation</a></li>
        </ul>
    </li>
    <li  runat="server" class="parent"><a href="#"><i class="fa fa-file-excel-o"></i><span>Import</span></a>
        <ul class="children">
            <li id="Li11" runat="server" visible='<%# IsAccessible("CP/Extension/VariableSalaryImport.aspx") %>'><a id="A12" runat="server" href="~/CP/Extension/VariableSalaryImport.aspx">Salary Import</a></li>
            <li id="Li12" runat="server" visible='<%# IsAccessible("CP/Extension/VariableDeductionImport.aspx") %>'><a id="A13" runat="server" href="~/CP/Extension/VariableDeductionImport.aspx">Deduction Import</a></li>
            <li id="Li13" runat="server" visible='<%# IsAccessible("newhr/LoanRepayment.aspx") %>'><a id="A14" runat="server" href="~/newhr/LoanRepayment.aspx">Loan Repayment Import</a></li>
            <li id="Li14" runat="server" visible='<%# IsAccessible("CP/Extension/DeemedSalaryImport.aspx") %>'><a id="A15" runat="server" href="~/CP/Extension/DeemedSalaryImport.aspx">Deemed Import</a></li>
        </ul>
    </li>
     <li runat="server"  class="parent"><a href="#"><i class="fa fa-cut"></i><span>Adjustments</span></a>
        <ul class="children">
            <li id="Li15" runat="server" visible='<%# IsAccessible("CP/LoanAdjustment.aspx") %>'><a id="A16" runat="server" href="~/CP/LoanAdjustment.aspx">Loan Adjustment</a></li>
            <li id="Li16" runat="server" visible='<%# IsAccessible("CP/EMIAdjustment.aspx") %>'><a id="A17" runat="server" href="~/CP/EMIAdjustment.aspx">EMI Adjustment</a></li>
            <li id="Li17" runat="server" visible='<%# IsAccessible("CP/AdvanceAdjustment.aspx") %>'><a id="A18" runat="server" href="~/CP/AdvanceAdjustment.aspx">Advance Adjustment</a></li>
            <li id="Li18" runat="server" visible='<%# IsAccessible("CP/IncomeDeductionAdjustment.aspx") %>'><a id="A19" runat="server" href="~/CP/IncomeDeductionAdjustment.aspx">Income Adjustment</a></li>
        </ul>
    </li>
    <li runat="server"  class="parent"><a href="#"><i class="fa fa-suitcase"></i><span>Others</span></a>
        <ul class="children">
            <li id="Li19" runat="server" visible='<%# IsAccessible("CP/HoldPaymentPG.aspx") %>'><a id="A20" runat="server" href="~/CP/HoldPaymentPG.aspx">Hold Payment</a></li>
            <li id="Li20" runat="server" visible='<%# IsAccessible("CP/StopPaymentPG.aspx") %>'><a id="A21" runat="server" href="~/CP/StopPaymentPG.aspx">Stop Payment</a></li>
            <li id="Li21" runat="server" visible='<%# IsAccessible("CP/PayReportSendIntoMail.aspx") %>'><a id="A22" runat="server" href="~/CP/PayReportSendIntoMail.aspx">E-mail Payslip</a></li>
            <li id="Li22" runat="server" visible='<%# IsAccessible("CP/ManageMedicalTaxCredit.aspx") %>'><a id="A23" runat="server" href="~/CP/ManageMedicalTaxCredit.aspx">Medical Tax Credit</a></li>
        </ul>
    </li>
</ul>
