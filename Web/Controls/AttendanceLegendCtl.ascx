﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttendanceLegendCtl.ascx.cs"
    Inherits="Web.Controls.AttendanceLegendCtl" %>
<fieldset style='padding: 10px; float: left'>
    <legend style='font-size: 11px'>Leaves</legend>
    <asp:DataList ID="dlstLegends" BorderWidth="1" BorderColor="Black" runat="server"
        RepeatColumns="6" RepeatDirection="Horizontal">
        <ItemTemplate>
            <table>
                <tr>
                    <td>
                        <span title='<%#Eval("Title") %>'>
                            <%#Eval("Title") %>(<%# Eval("Abbreviation") %>) </span>
                    </td>
                    <td style="padding-right:10px;">
                        <div title='<%#Eval("Title") %>' style='border: 1px solid black; width: 10px; height: 10px;
                            background-color: <%# Eval("LegendColor") %>'>
                        </div>
                    </td">
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList>
</fieldset>
<fieldset style='padding: 10px; float: left'>
    <legend style='font-size: 11px'>Holidays</legend>
    <asp:DataList ID="dlstLegendsHolidays" BorderWidth="1" BorderColor="Black" runat="server"
        RepeatColumns="8" RepeatDirection="Horizontal">
        <ItemTemplate>
            <table>
                <tr>
                    <td>
                        <span title='<%#Eval("Title") %>'>
                            <%# Eval("Abbreviation") %>
                        </span>
                    </td>
                    <td>
                        <div title='<%#Eval("Title") %>' style='border: 1px solid black; width: 10px; height: 10px;
                            background-color: <%# Eval("LegendColor") %>'>
                        </div>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList>
</fieldset>
