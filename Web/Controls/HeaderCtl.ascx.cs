﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Security;
using BLL;
using BLL.Manager;
using System.Xml;
using Utils;
using System.IO;
using Utils.Helper;
using DAL;
using Ext.Net;
using System.Xml.Linq;
using Utils.Security;

namespace Web.Controls
{
    //dont use base control here as it is used in Error.aspx page also 
    public partial class HeaderCtl : System.Web.UI.UserControl
    {
        public string UnreadMessageCount
        {
            get { return this.ltUnreadMessageCounter.Text; }
            set { this.ltUnreadMessageCounter.Text = value; }
        }
      

        private string currentPage = "";


        public static string GetEmployeeMenuFileName()
        {
            if (CommonManager.CompanySetting.IsD2)
                return "~/EmpMenus/d2.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.PSI)
                return "~/EmpMenus/ps.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Century)
                return "~/EmpMenus/cc.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.AceTravels)
                return "~/EmpMenus/ace.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Mega)
                return "~/EmpMenus/mega.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NIBL)
                return "~/EmpMenus/nibl.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Sangrila)
                return "~/EmpMenus/sangrila.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Suvhidha)
                return "~/EmpMenus/suvhidha.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Citizen)
                return "~/EmpMenus/citizen.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Janata)
                return "~/EmpMenus/janata.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Care)
                return "~/EmpMenus/care.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Yeti)
                return "~/EmpMenus/yeti.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Rigo)
                return "~/EmpMenus/rigo.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.WDN)
                return "~/EmpMenus/wdn.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.DishHome)
                return "~/EmpMenus/dh.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.HPL)
                return "~/EmpMenus/hpl.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Prabhu)
                return "~/EmpMenus/prabhu.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Civil)
                return "~/EmpMenus/Civil.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.MBL)
                return "~/EmpMenus/MBL.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Sana)
                return "~/EmpMenus/sana.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.Oxfam)
                return "~/EmpMenus/oxfam.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.OtherTesting)
                return "~/EmpMenus/other.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.NCC)
                return "~/EmpMenus/ncc.xml";
            else if (CommonManager.CompanySetting.WhichCompany == WhichCompany.SaveTheChidlren)
                return "~/EmpMenus/save.xml";
            return "~/EmpMenus/EmployeeSiteMap.xml";

        }
        public static XmlDocument GetEmployeeMenuXml()
        {
            if (MyCache. GetFromGlobalCache("EmployeeMenu") != null)
                return MyCache.GetFromGlobalCache("EmployeeMenu") as XmlDocument;

            XmlDocument doc = new XmlDocument();

            doc.Load(HttpContext.Current.Server.MapPath(GetEmployeeMenuFileName()));

            MyCache.SaveToGlobalCache("EmployeeMenu", doc, MyCache.ExpirationType.Sliding, MyCache.veryLongTime);

            return doc;
        }

        protected void btnSwitch_Click(object sender, EventArgs e)
        {

            UUser user = UserManager.GetAdminUserForEmployee(SessionManager.CurrentLoggedInEmployeeId);

            if (user != null)
            {


                BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                BLL.BaseBiz.GetChangeUserActivityLog("User Switched from " + SessionManager.User.UserName + " to " + user.UserName + "."));
                BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                Session.Abandon();
                FormsAuthentication.SignOut();





                SecurityHelper.CreateCookie(user.UserName,
                    true, user.URole.Name, false, false);

                switch ((Role)user.RoleId)
                {
                    case Role.Administrator:

                        Response.Redirect("~/newhr/DashboardHR.aspx", true);
                        break;


                    default:


                        Response.Redirect("~/newhr/DashboardHR.aspx", true);

                        break;
                }



            }
        }


        public string InboxNavigation()
        {
            if (Page.User.IsInRole(Role.Employee.ToString()))
                return ResolveUrl("~/Employee/PayrollMessage.aspx");
            else
                return ResolveUrl("~/CP/PayrollMessage.aspx");
        }
        public void ReloadMenu()
        {
            Page_Load(null, null);
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {

            //extra protection so checking if the user is logged in or not
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/employee/"))
                {
                    //Response.Redirect("~/Employee/Default.aspx");
                    ReturnUrl();
                    Response.End();
                }
                else
                    Response.Redirect("~/Default.aspx");
            }
            // for authentication also
            else
            {
                //if user is authenticated & is employee then can only access the page with /employee/ in the url
                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                {
                    if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/employee/") == false)
                    {
                        Response.Redirect("~/Employee/Default.aspx");
                        //ReturnUrl();
                    }
                }
                else
                {
                    if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/employee/") == true)
                    {
                        Response.Redirect("~/Default.aspx");
                        //ReturnUrl();
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string url = Page.ResolveUrl("~/Employee/DateConverter.aspx");
                X.Js.AddScript("dateUrl = '" + url + "';");
            }
            catch { }

            //skip for some child master pages like popup
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Date2", string.Format("isEnglish = {0};",
                BLL.BaseBiz.IsEnglish.ToString().ToLower()), true);

            // appraisal employe core values submit then menu hidden or payslip load menu hidden or menu color white problem so in asp.net button postback also initiial method called
            //if (!X.IsAjaxRequest && !IsPostBack)
            if (!X.IsAjaxRequest)// && !IsPostBack)
            {


                //if (SessionManager.IsLDAPLogin)
                //    pwdLogoutSection.Visible = false;

                LoadMessage();

                currentPage = Path.GetFileName(Request.Url.AbsoluteUri.ToString()).ToLower();
                if (currentPage.IndexOf("?") >= 0)
                    currentPage = currentPage.Remove(currentPage.IndexOf("?"));


                linkChangePwd.Visible = !LdapHelper.IsLDAPAccessible();

                if(LdapHelper.IsLDAPAccessible() && string.IsNullOrEmpty( SessionManager.User.ActiveDirectoryName))
                {
                    linkChangePwd.Visible = true;
                }

                //no compnay exists so don't display menus
                if (SessionManager.CurrentCompanyId == 0)
                {
                }
                else
                {
                    if (SessionManager.CurrentCompanyId != -1)
                    {
                        companyName.InnerHtml = SessionManager.CurrentCompany.Name;
                        valueC.Value = SessionManager.CurrentCompany.CompanyId.ToString();
                    }
                    else
                        companyName.InnerHtml = "&nbsp;";

                    if (!IsPostBack)
                    {
                        GetUnreadMessage();


                    }
                    else
                    {
                        // validate for the default company id changed case 
                        int id;
                        if (int.TryParse(valueC.Value, out id))
                        {
                            if (id != SessionManager.CurrentCompanyId)
                            {
                                CompanyManager mgr = new CompanyManager();
                                Response.Clear();
                                Response.Write(
                                    string.Format("<font color='red'>Default company changed from \"{0}\" to \"{1}\". <br> Refresh the page to use.</font>"
                                    , (mgr.GetById(id) != null) ? mgr.GetById(id).Name : "", SessionManager.CurrentCompany.Name));
                                Response.End();

                            }
                        }
                    }

                    LoadMenus();



                }

                CheckPermission();
                CheckBrowserAndOthers();
                if (SessionManager.CurrentCompanyId != 0)
                    Registered();

                //if (!IsPostBack)
                {
                    UUser user = SessionManager.User;
                    if (user.IsEmployeeType == null || user.IsEmployeeType == false)
                        lblUserName.Text = user.UserName;
                    else
                    {

                        if (user.EmployeeId != null)
                            lblUserName.Text = EmployeeManager.GetEmployeeName(user.EmployeeId.Value);
                    }
                }

                if (SessionManager.CurrentLoggedInEmployeeId != 0 && AttendanceManager.IsAttendanceDeviceIntegrated()
                    && CommonManager.CompanySetting.WhichCompany != WhichCompany.NIBL)
                {
                    bool? isManualAtte =
                        EmployeeManager.GetEmployeeById(SessionManager.CurrentLoggedInEmployeeId).Branch.IsManualAttendance;
                    if (isManualAtte != null && isManualAtte.Value)
                    {
                        spanManualAttendance.Visible = true;
                        linkManualAttendance.Visible = true;
                    }
                }

            }
            //else
            //{
            //    LoadMenus();
            //    if (SessionManager.CurrentCompanyId != 0)
            //        Registered();
            //}
        }



        public void LoadMessage()
        {
            // Load Message
            int count = (int)LeaveAttendanceManager.GetUnreadMessageByUserName(SessionManager.UserName);
            ltUnreadMessageCounter.Text = count.ToString() == "0" ? "" : count.ToString();

            if (count <= 0)
                messageContainer.Style["display"] = "none";
            else
                messageContainer.Style["display"] = "";
        }

        void Registered()
        {
            StringBuilder str = new StringBuilder();

            try
            {
                //exception thrown if first company
                str.AppendFormat("var decimalPlaces = {0};", SessionManager.DecimalPlaces);
            }
            catch
            {
                str.AppendFormat("var decimalPlaces = {0};", 2);
            }

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "rounding1",
                                                        str.ToString()
                                                        , true);
        }

        void CheckBrowserAndOthers()
        {
            if (IsPostBack)
            {
                if (!js.Value.Equals("1"))
                {
                    Response.Clear();
                    Response.Write(
                        "<h1>Javascript is disabled, please enable Javascript in the browser setting to run the Payroll.</h1>");
                    Response.End();
                }
            }
        }

        /// <summary>
        /// Page permission checking logic
        /// </summary>
        public void CheckPermission()
        {
            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                UUser adminuser = UserManager.GetAdminUserForEmployee(SessionManager.CurrentLoggedInEmployeeId);
                if (adminuser == null)
                {
                    swithSep.Visible = false;
                    btnSwitch.Visible = false;
                }
                else
                {
                    btnSwitch.Visible = true;
                    swithSep.Visible = true;
                }
            }
           

            //for employee page access
            if (this.Request.Url.AbsoluteUri.ToString().ToLower().IndexOf("/employee/") > 0)
            {
                int empId = SessionManager.CurrentLoggedInEmployeeId;
                if (empId == 0)
                {
                    if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/employee/"))
                        ReturnUrl();
                    else
                        Response.Redirect("~/Default.aspx");
                }
            }
            else
            {
                int empId = SessionManager.CurrentLoggedInEmployeeId;
                if (empId != 0)
                {
                    //give permission to change password page only

                    if (currentPage != "changepassword.aspx" && currentPage != "payrollmessage.aspx")
                    {
                        if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/employee/"))
                            Response.Redirect("~/Employee/Default.aspx");
                        else
                            Response.Redirect("~/Default.aspx");
                    }

                }
            }
        }

        public string GetSelected(string url)
        {
            if (url.ToLower().Contains(currentPage.ToLower()))
                return "font-weight:bold";
            return "";
        }

        //to differtiate custom role user
        public bool isCustomRole = false;
        public string[] pagesPerimissible = null;

        public XmlDocument ProcessAsPerRole()
        {
            if (Page.User.IsInRole(Role.Administrator.ToString()))
            {
                return MyCache.GetMenuXml(SessionManager.User.UserID.ToString(), CommonManager.CompanySetting.HasLevelGradeSalary);
            }
            else if (Page.User.IsInRole(Role.Employee.ToString()))
            {
                List<EPermissionPage> list = UserManager.GetEPermissionPagesByCompanyId();
                if (list.Count > 0)
                {
                    return GetEmployeeXmlDocument(list);
                }
                else
                    return GetEmployeeMenuXml();
            }
            else if (SessionManager.CurrentCompanyId == -1) //if db connection gives error then this will hapeen
            {
                return MyCache.GetMenuXml(SessionManager.User.UserID.ToString(), CommonManager.CompanySetting.HasLevelGradeSalary);
            }
            else
            {
                //if (Page.User.Identity.IsAuthenticated == false)
                //    return null;

                //isCustomRole = true;
                //XmlDocument doc = MyCache.GetMenuXml(SessionManager.User.UserID.ToString());
                //UUser user = UserManager.GetUserByUserName(Page.User.Identity.Name);
                //pagesPerimissible = UserManager.GetAccessiblePagesForRole(user.RoleId.Value);

                //int currentPageId = UserManager.GetPageIdByPageUrl(currentPage);
                ////current page not accessible so do redirection
                //if (!pagesPerimissible.Contains(currentPageId) && currentPage != "changepassword.aspx" && currentPage != "payrollmessage.aspx"
                //     && currentPage != "allreports.aspx"
                //     && currentPage != "allsettings.aspx"
                //     && currentPage != "payrollsignoff.aspx")
                //{
                //    if (Request.UrlReferrer != null)
                //        Response.Redirect(Request.UrlReferrer.ToString());
                //    else
                //    {
                //        if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/employee/"))
                //            Response.Redirect("~/Employee/Default.aspx");
                //        else
                //            Response.Redirect("~/Default.aspx");
                //    }
                //}
                //return doc;
                return null;
            }

        }
        
        
        private void LoadMenus()
        {

            XmlDocument doc = ProcessAsPerRole();


            
            //node.AppendChild(newNode);


            XmlNodeList rootNodes =
             (((System.Xml.XmlDocument)((System.Xml.XmlNode)(doc))).ChildNodes);
            rootNodes = rootNodes[1].ChildNodes;

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                //load root menus
                rptMainMenu.DataSource = rootNodes;
                rptMainMenu.DataBind();
            }



            string rootMenuIdText = "span";
            //iterate main menus
            for (int i = 0; i < rootNodes.Count; i++)
            {

                XmlElement rootMenu = rootNodes[i] as XmlElement;

                //if root menu item selected
                if (rootMenu.Attributes["url"] != null && rootMenu.Attributes["url"].Value.ToLower().IndexOf(currentPage) >= 0)
                {
                    //HtmlAnchor control = (HtmlAnchor)this.rptMainMenu.Items[i].FindControl("span1");
                    //control.Attributes.Add("class", "buttonbg gradient_button gradient35 mainMenuSelected");
                    //HtmlGenericControl parentControl = control.Parent as HtmlGenericControl;
                    //parentControl.Attributes.Add("class", "topitem spaced_li mainMenuSelectedli");

                    HtmlGenericControl control = (HtmlGenericControl)this.rptMainMenu.Items[i].FindControl("span1"); //this.FindControl(rootMenuId) as HtmlGenericControl;
                    control.Attributes.Add("class", "buttonbg gradient_button gradient35 mainMenuSelected");
                    HtmlGenericControl parentControl = control.Parent as HtmlGenericControl;
                    parentControl.Attributes.Add("class", "topitem spaced_li mainMenuSelectedli");
                }

                //process for Employee menu if allowed for Leave Approval or not
                if (Page.User.IsInRole(Role.Employee.ToString()))
                {
                    //not if allowed to approval
                    if (rootMenu.Attributes["url"].Value.ToLower().IndexOf("leaveapproval.aspx") >= 0)
                    {
                        if (LeaveAttendanceManager.IsEmployeeAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId))
                            this.rptMainMenu.Items[i].FindControl("span1").Visible = true;
                        else
                            this.rptMainMenu.Items[i].FindControl("span1").Visible = false;
                    }
                    if (rootMenu.Attributes["url"].Value.ToLower().IndexOf("otheremployeeattendance.aspx") >= 0)
                    {
                        if (LeaveAttendanceManager.IsEmployeeAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId))
                            this.rptMainMenu.Items[i].FindControl("span1").Visible = true;
                        else
                            this.rptMainMenu.Items[i].FindControl("span1").Visible = false;
                    }
                    if (rootMenu.Attributes["url"].Value.ToLower().IndexOf("timesheetinputapproval.aspx?ap=") >= 0)
                    {
                        if (LeaveAttendanceManager.IsEmployeeAllowedToApproveTimesheet(SessionManager.CurrentLoggedInEmployeeId))
                            this.rptMainMenu.Items[i].FindControl("span1").Visible = true;
                        else
                            this.rptMainMenu.Items[i].FindControl("span1").Visible = false;
                    }
                    if (rootMenu.Attributes["url"].Value.ToLower().IndexOf("timesheetinputreview.aspx?rv=") >= 0)
                    {
                        if (LeaveAttendanceManager.IsEmployeeAllowedToReviewTimesheet(SessionManager.CurrentLoggedInEmployeeId))
                            this.rptMainMenu.Items[i].FindControl("span1").Visible = true;
                        else
                            this.rptMainMenu.Items[i].FindControl("span1").Visible = false;
                    }
                    // of emp is dep head or manager then only show Employee List page
                    if (rootMenu.Attributes["url"].Value.ToLower().IndexOf("otheremployeedetails.aspx") >= 0)
                    {
                        if (EmployeeManager.IsDepHeadOrManagerToViewEmployeeList(SessionManager.CurrentLoggedInEmployeeId))
                            this.rptMainMenu.Items[i].FindControl("span1").Visible = true;
                        else
                            this.rptMainMenu.Items[i].FindControl("span1").Visible = false;
                    }
                }



                //iterate sub menus
                if (SessionManager.CurrentLoggedInEmployeeId != 0)
                {
                    for (int j = 0; j < rootMenu.ChildNodes.Count; j++)
                    {

                        string currentSubMenuPage = Path.GetFileName(rootMenu.ChildNodes[j].Attributes["url"].Value.ToLower());
                        if (currentSubMenuPage.IndexOf("?") >= 0)
                            currentSubMenuPage = currentSubMenuPage.Remove(currentSubMenuPage.IndexOf("?"));

                        currentSubMenuPage = currentSubMenuPage.Trim();

                        //select main menu & load the sub-menus of that main menu if sub menu selected
                        if (currentSubMenuPage == currentPage)
                        {
                            HtmlGenericControl control = (HtmlGenericControl)this.rptMainMenu.Items[i].FindControl("span1"); //this.FindControl(rootMenuId) as HtmlGenericControl;
                            control.Attributes.Add("class", "buttonbg gradient_button gradient35 mainMenuSelected");
                            HtmlGenericControl parentControl = control.Parent as HtmlGenericControl;
                            parentControl.Attributes.Add("class", "topitem spaced_li mainMenuSelectedli");
                            rptSubMenus.DataSource = rootMenu.ChildNodes;
                            rptSubMenus.DataBind();
                            break;
                        }

                        //if mega menu then iterate sub child also
                        if (rootMenu.Attributes["isMegaMenu"] != null)
                        {

                            //iterate mega menus
                            for (int m = 0; m < rootMenu.ChildNodes[j].ChildNodes.Count; m++)
                            {
                                currentSubMenuPage = Path.GetFileName(rootMenu.ChildNodes[j].ChildNodes[m].Attributes["url"].Value.ToLower());
                                if (currentSubMenuPage.IndexOf("?") >= 0)
                                    currentSubMenuPage = currentSubMenuPage.Remove(currentSubMenuPage.IndexOf("?"));

                                currentSubMenuPage = currentSubMenuPage.Trim();


                                //select main menu & load the sub-menus of that main menu if sub menu selected
                                if (currentSubMenuPage == currentPage)
                                {


                                    HtmlAnchor control = this.rptMainMenu.Items[i].FindControl("span1") as HtmlAnchor; //this.FindControl(rootMenuId) as HtmlGenericControl;

                                    if (control == null)
                                        break;

                                    control.Attributes.Add("class", "mainMenuAnchor");
                                    HtmlGenericControl parentControl = control.Parent as HtmlGenericControl;
                                    parentControl.Attributes.Add("class", "mainMenuSelected has_mega_menu");


                                    rptSubMenus.DataSource = rootMenu.ChildNodes[j].ChildNodes;
                                    rptSubMenus.DataBind();


                                    break;
                                }

                            }
                        }

                    }

                }
            }

            if (SessionManager.CurrentLoggedInEmployeeId == 0)
            {
                //load breadcumbs
                XmlDocument docBreadcrumbs = MyCache.GetBreadCumbsXml();

                XmlNodeList rootNodesBreadcumbs =
                 (((System.Xml.XmlDocument)((System.Xml.XmlNode)(docBreadcrumbs))).ChildNodes);
                rootNodes = rootNodesBreadcumbs[1].ChildNodes;

                for (int i = 0; i < rootNodes.Count; i++)
                {
                    string rootMenuId = rootMenuIdText + (i + 1);
                    XmlElement rootMenu = rootNodes[i] as XmlElement;
                    for (int j = 0; j < rootMenu.ChildNodes.Count; j++)
                    {
                        if (rootMenu.ChildNodes[j].Attributes["url"].Value.ToLower().IndexOf(currentPage) >= 0)
                        {

                            switch (currentPage)
                            {
                                case "employee.aspx":
                                case "employeehrdetails.aspx":
                                case "managemedicaltaxcredit.aspx":
                                case "individualinsurance.aspx":
                                case "leaveopeningbal.aspx":
                                    int empId = UrlHelper.GetIdFromQueryString("Id");
                                    if (empId == 0)
                                        empId = UrlHelper.GetIdFromQueryString("EmpId");

                                    if (empId == 0)
                                    {
                                        //save mode so display hr link as readmode
                                        rootMenu.ChildNodes[1].Attributes["url"].Value = "";
                                    }
                                    else
                                    {
                                        //append emp id for all pages
                                        rootMenu.ChildNodes[0].Attributes["url"].Value += "?Id=" + empId;
                                        rootMenu.ChildNodes[1].Attributes["url"].Value += "?EmpId=" + empId;
                                        rootMenu.ChildNodes[2].Attributes["url"].Value += "?EmpId=" + empId;
                                        rootMenu.ChildNodes[3].Attributes["url"].Value += "?EmpId=" + empId;
                                        rootMenu.ChildNodes[4].Attributes["url"].Value += "?EmpId=" + empId;
                                    }
                                    break;
                            }

                            //rptBreadCumbs.DataSource = rootMenu.ChildNodes;
                            //rptBreadCumbs.DataBind();


                            break;
                        }
                    }
                }
            }

            //if no breadcumb hide the seciton
            //if (rptBreadCumbs.Items.Count <= 0)
            //    breadcumb.Visible = false;

            if (rptSubMenus.Items.Count <= 0)
                nav_bar_d.Visible = false;
        }

        public string GetUrl(XmlAttribute value)
        {
            if (value == null || string.IsNullOrEmpty(value.Value))
                return "javascript:void(0)";

            return value.Value;
        }

        public string GetToolTip(XmlAttribute value)
        {
            if (value == null)
                return "";

            return value.Value.ToString();
        }

        public bool IsAccessible(XmlAttribute value,XmlAttribute urlValue)
        {
           // return true;
            int pageId = 0;
            string url = "";

            if (value == null || string.IsNullOrEmpty(value.Value))
                ;//return false;
            else 
                pageId = int.Parse(value.Value);


            if (urlValue == null || string.IsNullOrEmpty(urlValue.Value))
            { }
            else
                url = urlValue.Value;

            //if (url.ToLower() == "~/cp/allreports.aspx" || url.ToLower()=="~/cp/allsettings.aspx")
            //    return true;

            if (url.ToLower().IndexOf("leaveapproval.aspx") >= 0 
                || url.ToLower().IndexOf("otheremployeeattendance.aspx") >= 0
                || url.ToLower().IndexOf("approvetimeattendance.aspx") >= 0
                || url.ToLower().IndexOf("overtimeapprovallist.aspx") >= 0
                || url.ToLower().IndexOf("ApproveTimeAttendanceComment.aspx".ToLower()) >= 0)
            {
                if (LeaveAttendanceManager.IsEmployeeAllowedToApproveLeave(SessionManager.CurrentLoggedInEmployeeId))
                    return true;
                else
                    return false;
            }

            return true;
        }

        public string GetBreadCumbClass(string url)
        {
            if (url.ToLower().IndexOf(currentPage.ToLower()) >= 0)
                return "selected";
            return "";
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            if (CheckForCheckedOut())
            {
                Session["logOut"] = 1;
                Response.Redirect("~/Employee/Dashboard.aspx", true);
            }

            int currentLoggedInEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            Session.Abandon();
            FormsAuthentication.SignOut();
            // Response.cookies.clear();
            //if (currentLoggedInEmployeeId == 0)
            Response.Redirect("~/Default.aspx", true);
            //else
            //    Response.Redirect("~/Employee/Default.aspx",true);
        }

        //public void DisplayBonus(int step)
        //{
        //    bonusBreadcumb.Visible = true;
        //    for (int i = 1; i <= 5; i++)
        //    {
        //        HtmlGenericControl stepControl = this.FindControl("step" + i) as HtmlGenericControl;

        //        if (i == step)
        //            stepControl.Attributes["class"] = "selected";
        //        else
        //        {
        //            stepControl.Attributes.Remove("class");
        //        }
        //    }
           
        //}

        public void GetUnreadMessage()
        {
            //int unreadMessageCount = PayrollMessageManager.GetUnreadPayrollMessage(SessionManager.UserName);
            //lblUnreadMessageCounter.Text = unreadMessageCount.ToString();
        }
        protected void ReturnUrl()
        {
            this.Response.Redirect("~/Employee/Default.aspx" +
               "?ReturnUrl=" + this.Request.Url.PathAndQuery);
        }

        private XmlDocument GetEmployeeXmlDocument(List<EPermissionPage> list)
        {
            XmlDocument doc = new XmlDocument();

            XmlDocument oldDoc = UserManager.GetEMpMenuXml();

            XmlNodeList moduleList = oldDoc.ChildNodes[1].ChildNodes;
           
            StringBuilder sb = new StringBuilder();

            List<EPermissionPage> listEPermission = UserManager.GetEPermissionPagesByCompanyId();
            
            sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.Append("<siteMapNode>");                    

            foreach (XmlNode module in moduleList)
            {
                int moduleId = int.Parse(module.Attributes["moduleId"].Value);

                if (listEPermission.Any(x => x.ModuleId == moduleId))
                {
                    sb.Append("<siteMapNode  title=\"" + module.Attributes["title"].Value + "\" url=\"" + module.Attributes["url"].Value + "\">");

                    List<EPermissionPage> listEPModule = listEPermission.Where(x => x.ModuleId == moduleId && x.IsPage == true).ToList();

                    foreach (EPermissionPage item in listEPModule)
                    {
                        sb.Append("<siteMapNode  title=\"" + item.Title + "\" url=\"~/" + item.Url + "\"></siteMapNode>");
                    }

                    sb.Append("</siteMapNode>");
                }

            }

            sb.Append("</siteMapNode>");

            doc.LoadXml(sb.ToString());

            return doc;
        }

        private bool CheckForCheckedOut()
        {
            Setting setting = OvertimeManager.GetSetting();
            if (setting != null && setting.ShowCheckInCheckOutPopup != null && setting.ShowCheckInCheckOutPopup.Value)
            {
                bool? isManualAtte = new AttendanceManager().IsManualAttendancePossible(SessionManager.CurrentLoggedInEmployeeId);

                if (isManualAtte != null && isManualAtte.Value == true)
                {
                    if (!AttendanceManager.IsCheckedInOrCheckedOut((int)ChkINOUTEnum.ChkOut))
                    {
                        TimeSpan EndTimeSpan = OvertimeManager.getOfficeEndTime(SessionManager.CurrentLoggedInEmployeeId);
                        DateTime EndTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, EndTimeSpan.Hours, EndTimeSpan.Minutes, EndTimeSpan.Seconds);

                        if (EndTime <= DateTime.Now)
                            return true;

                        TimeSpan ts = EndTime - DateTime.Now;
                        if (ts.TotalHours < 1)
                            return true;
                    }
                }
            }
            return false;
        }
        
       
    }
}