﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utils.Calendar;
using BLL.Base;


namespace Web.Controls
{
    public partial class ContentHeader : BaseUserControl
    {
        public Ext.Net.ResourceManager ExtResourceManager
        {
            set { }
            get { return ResourceManager1; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //skip for some child master pages like popup
            ResourceManager1.RegisterClientInitScript("Date1", string.Format("isEnglish = {0};", this.IsEnglish.ToString().ToLower()));


            

            ResourceManager1.RegisterClientInitScript("Date2", string.Format("todayDate = '{0}';function calendarOnClick(event, sourceElement, e3){{}};",
                                                                      CustomDate.GetTodayDate(this.IsEnglish).ToString()));

        }

        public void HideResource()
        {
            ResourceManager1.Visible = false;
        }
    }
}