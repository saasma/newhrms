﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuHRAppraisalForm.ascx.cs"
    Inherits="Web.Controls.MenuHRAppraisalForm" %>
<style type="text/css">
    .menuActiveA
    {
        background-color: lightgray;
    }
</style>
<div id="menu" class="hidden-phone">
    <div id="search">
    </div>
    <ul runat="server" id="employeeMenuList">
        <li runat="server" id="menu1" class="hasSubmenu glyphicons user  active"><a id="anchor1"
            data-toggle="collapse" href="#menu_index"><i></i><span>Appraisal Form</span></a>
            <ul class="in collapse" id="menu_index" runat="server">
                <li class=""><a runat="server" isemployeedetails="true" id="menuEmployeePersonalDetails"
                    href="~/Appraisal/Form/general_settings_1.aspx"><span>Form Name</span></a></li>
                <li class=""><a visible="false" runat="server" id="A7" href="~/Appraisal/Form/Introduction_2.aspx">
                    <span>Introduction</span></a></li>
                <li class=""><a visible="false" runat="server" id="A2" href="~/Appraisal/Form/Objectives_3.aspx">
                    <span>Objectives</span></a></li>
                <li class=""><a visible="false" runat="server" isemployeedetails="true" id="A5" href="~/Appraisal/Form/PerformanceSummary.aspx">
                    <span>Performance Summary</span></a></li>
                <li class=""><a visible="false" runat="server" id="A8" href="~/Appraisal/Form/Activities_4.aspx">
                    <span>Activity Summary</span></a></li>
                <li class=""><a visible="false" runat="server" id="A3" href="~/Appraisal/Form/CoreCompetencies_5.aspx">
                    <span>Competencies</span></a></li>
                <li class=""><a visible="false" runat="server" isemployeedetails="true" id="A4" href="~/Appraisal/Form/Questionnaire.aspx">
                    <span>Questionnaire</span></a></li>
                <li class=""><a visible="false" runat="server" isemployeedetails="true" id="A9" href="~/Appraisal/Form/Review_7.aspx">
                    <span>Supervisor Review</span></a></li>
                <li class=""><a visible="false" runat="server" isemployeedetails="true" id="A10"
                    href="~/Appraisal/Form/Summary_8.aspx"><span>Comments</span></a></li>
                <li class=""><a visible="false" runat="server" isemployeedetails="true" id="A6" href="~/Appraisal/Form/Signatures_9.aspx">
                    <span>Signatures</span></a></li>
                <li class=""><a visible="false" runat="server" isemployeedetails="true" id="A1" href="~/Appraisal/Preview.aspx">
                    <span>Preview</span></a></li>
            </ul>
        </li>
    </ul>
    <div class="clearfix" style="clear: both">
    </div>
</div>
