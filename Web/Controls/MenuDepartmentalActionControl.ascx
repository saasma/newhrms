﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuDepartmentalActionControl.ascx.cs"
    Inherits="Web.Controls.MenuDepartmentalActionControl" %>
<div id="menu" class="hidden-phone">
    <div id="search">
        <asp:TextBox ID="txtEmpSearchText" Width="160px" runat="server"></asp:TextBox>
        <cc1:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtEmpSearchText"
            WatermarkText="Employee Name" WatermarkCssClass="searchBoxText" />
        <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
            runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
            TargetControlID="txtEmpSearchText" OnClientItemSelected="ACE_item_selected" CompletionSetCount="10"
            CompletionInterval="250" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
            CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
        </cc1:AutoCompleteExtender>
        <%--   <input type="text" placeholder="Quick search ..." />--%>
        <button class="glyphicons search">
            <i></i>
        </button>
    </div>
    <ul runat="server" id="employeeMenuList">
        <li runat="server" id="menu1" class="hasSubmenu glyphicons user  active"><a 
            id="anchor1" data-toggle="collapse" href="#menu_index"><i></i><span>Departmental Action</span></a>
            <ul  class="in collapse" id="menu_index">
                <li class=""><a runat="server" isemployeedetails="true" id="menuEmployeePersonalDetails"
                    href="javascript:void(0)"><span>Initiate Action</span></a></li>
            </ul>
        </li>
        <li runat="server" id="menu2" class="hasSubmenu glyphicons nameplate active"><a runat="server"
            id="anchor2" data-toggle="collapse" href="#Ul2"><i></i><span>Active Departmental Action</span></a>
            <ul class="in collapse" id="Ul2">
                <li class=""><a runat="server" isemployeedetails="true" id="menuEmployeePositionAndJob"
                    href="javascript:void(0)"><span>Waiting for Defence</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A4" href="javascript:void(0)">
                    <span>Waiting for Clarification</span></a></li>
            </ul>
        </li>
        <li runat="server" id="menu3" class="hasSubmenu glyphicons briefcase active"><a runat="server"
            id="anchor3" data-toggle="collapse" href="#Ul3"><i></i><span>View Departmental Actions</span></a>
            <ul  class="in collapse" id="Ul3">
                <li class=""><a runat="server" isemployeedetails="true" id="menuEmployeeEducationTraining"
                    href="javascript:void(0)"><span>Warnings Issued</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A1" href="javascript:void(0)">
                    <span>Punishments</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A2" href="javascript:void(0)">
                    <span>Salary / Grade Stopped </span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A3" href="javascript:void(0)">
                    <span>Demotions</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A5" href="javascript:void(0)">
                    <span>Dismissals</span></a></li>
            </ul>
        </li>
    </ul>
    <div class="clearfix" style="clear: both">
    </div>
</div>
