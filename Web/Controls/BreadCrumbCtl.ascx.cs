﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using BLL;
using BLL.Base;
using System.Xml.Linq;
using Ext.Net;

namespace Web.Controls
{

    public class BreadCumBO
    {
        public string url { get; set; }
        public string title { get; set; }
    }

    public partial class BreadCrumbCtl : BaseUserControl
    {
        public int count = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !X.IsAjaxRequest)
            {
                Load();
            }
        }


        public new void Load()
        {

            //load breadcumbs
            XmlDocument docBreadcrumbs = MyCache.GetBreadCumbsXml();
            string currentPage = CurrentPageUrl.ToString().ToLower();
           // int CurrentPageModelID = BLL.Manager.UserManager.GetPageModuleId(CurrentPageUrl.ToString().ToLower(),0);

            XmlNodeList rootNodesBreadcumbs =
             (((System.Xml.XmlDocument)((System.Xml.XmlNode)(docBreadcrumbs))).ChildNodes);

            XmlNodeList rootNodes = rootNodesBreadcumbs[1].ChildNodes;

            for (int i = 0; i < rootNodes.Count; i++)
            {
                XmlElement rootMenu = rootNodes[i] as XmlElement;
                for (int j = 0; j < rootMenu.ChildNodes.Count; j++)
                {

                    XmlElement rootMenu1 = rootNodes[i].ChildNodes[j] as XmlElement;
                    for (int k = 0; k < rootMenu1.ChildNodes.Count; k++)
                    {

                        string title = rootMenu1.ChildNodes[k].Attributes["title"].Value.ToString();

                        XmlElement rootMenu2 = rootNodes[i].ChildNodes[k] as XmlElement;
                        if (rootMenu2 == null)
                            break;
                        for (int l = 0; l < rootMenu2.ChildNodes.Count; l++)
                        {

                            if (rootMenu2.ChildNodes[l].Attributes["url"] != null)
                            {
                                if (rootMenu2.ChildNodes[l].Attributes["url"].Value.ToLower().Split('/').Last()==currentPage)
                                   // && int.Parse(rootMenu2.ChildNodes[l].Attributes["moduleId"].Value) == CurrentPageModelID)
                                {

                                    lnkParentPage.HRef = rootMenu.Attributes["url"].Value;
                                    lnkParentPageText.InnerText = rootMenu.Attributes["title"].Value;
                                    TitleCurrentPage.InnerText = rootMenu2.ChildNodes[l].Attributes["title"].Value;
                                    breadcumb.Visible = true;
                                    MainDiv.Style["height"] = "25px";
                                    return;
                                }

                            }

                        }
                    }

                    //if ( rootMenu.ChildNodes[i].ChildNodes[j].Attributes["url"]!= null)
                    //{
                    //    if (rootMenu.ChildNodes[i].ChildNodes[j].Attributes["url"].Value.ToLower().IndexOf(currentPage) >= 0)
                    //    {

                    //        count = 0;
                    //        rptBreadCumbs.DataSource = rootMenu.ChildNodes;
                    //        rptBreadCumbs.DataBind();
                    //        break;
                    //    }
                    //}
                }
            }
                  
            //if no breadcumb hide the seciton
            //if (rptBreadCumbs.Items.Count <= 0)
            //    breadcumb.Visible = false;
        }

     
        public bool IsFirstAndIncrement()
        {
            if (++count == 1)
                return true;
            return false;
        }

        public string GetBreadhcumbDivider()
        {
            if (count == 0)
                return "";
            return "<li class='divider'></li>";
        }

        public bool IsCurrentPage(object page)
        {
            if (page.ToString().ToLower().Contains(CurrentPageUrl.ToString().ToLower()))
            {
                return true;
            }
            return false;
        }

    }
}