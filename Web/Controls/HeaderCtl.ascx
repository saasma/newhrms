﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="true" CodeBehind="HeaderCtl.ascx.cs"
    Inherits="Web.Controls.HeaderCtl" %>
<%@ Import Namespace="System.Xml" %>
<style type="text/css">
    .counter
    {
        background-color: #CE7870;
        border: 2px solid #fff;
        border-radius: 12px;
        padding: 0 4px;
        position: relative;
        top: -5px;
        right: -11px;
    }
    .left_call
    {
        background: url(<%= Page.ResolveUrl("~/scripts/calendar/2left_cal.png") %>) no-repeat center left;
    }
    
    .right_cal
    {
        background: url(<%= Page.ResolveUrl("~/scripts/calendar/right_cal.png") %>) no-repeat center left;
    }
    
    .right_call
    {
        background: url(<%= Page.ResolveUrl("~/scripts/calendar/2right_cal.png") %>) no-repeat center left;
    }
    
    .left_cal
    {
        background: url(<%= Page.ResolveUrl("~/scripts/calendar/left_cal.png") %>) no-repeat center left;
    }
    
    
    .subSectionMenu
    {
        color: lightgray !important;
        border: 1px solid #2F7CD3;
    }
    .subSectionMenu:hover
    {
    }
    .fav
    {
        color: rgb(22, 192, 22) !important;
        font-size: 14px !important;
        font-weight: bold !important;
        display: block !important;
        vertical-align: middle !important;
        border-bottom: 1px inset #4E82C2 !important;
    }
    
    
    .fav:hover
    {
        color: #339900 !important;
        font-size: 14px !important;
        font-weight: bold !important;
        display: block !important;
        vertical-align: middle !important;
        background-color: #1193C3 !important;
        z-index: 300px !important;
        border-color: #1193C3 !important;
        border-bottom: 1px inset #4E82C2 !important;
    }
    
    .fav a
    {
        font-family: Gill Sans, Verdana !important;
        text-shadow: -1px -1px 1px #000000, 1px 1px 1px #CCCCCC;
    }
    .fav a
    {
        /*background: #CCCCCC;*/
        text-shadow: 0 1px 0 #FFFFFF;
    }
    #mbmcpebul_table li div.buttonbg
    {
        width:130px;
        margin-bottom:1px;
    }

</style>
<!-- corner script-->
<asp:HiddenField runat="server" ID="js" Value="0" />
<script type="text/javascript">
     function popupDate() {
            
        //calculate center left/top   
        var left = parseInt((screen.availWidth / 2) - (405 / 2));
        var top = parseInt((screen.availHeight / 2) - (320 / 2));
        var myRand = Math.random(50000);
        var myWindow;

        myWindow = window.open(dateUrl + '?rand=' + myRand, '', 'height=350,scrollbars=1,width=505,toolbar=no,directories=no,status=no,menubar=no,center=yes,left=' + left + ',top=' + top);

        try {
            myWindow.focus();
        }
        catch (err) {
        }
    }

    var companyNameId = '<%= companyName.ClientID %>';
    document.getElementById('<%= js.ClientID %>').value = "1";

    function loadMessage(messageCount) {
        var messageContainer = <%= messageContainer.ClientID %>;
        
        if( messageCount <= 0 )
            messageContainer.style.display='none';
        else
            messageContainer.style.display='';

        <%=ltUnreadMessageCounter.ClientID%>.textContent = messageCount;
    }

</script>
<style type="text/css">
    /*chrome specific combo box image height*/
</style>
<asp:HiddenField ID="valueC" runat="server" />
<div id="header">
    <div class="header">
        <div class="top_head">
            <div class="top_text" style="min-width: 300px; text-align: left">
                <span runat="server" id="companyName" style='font-weight: bolder;'></span>             
            </div>
            <div style="padding-top: 10px; position: relative; left: 0px;" class='below_text'>
                <a href='<%=InboxNavigation()%>' style="width:7px; height:5px;">
                    <span class="counter" id="messageContainer" runat="server">
                        <asp:Label ID="ltUnreadMessageCounter" runat="server" /></span>
                    <span>
                        <img src='<%= Page.ResolveUrl("~/images/mail.png") %>' style="display: inline; margin-bottom: -2px!important;" />
                    </span></a>
            
                <asp:Label ID="lblUserName" runat="server" /> <span runat="server" id="pwdLogoutSection">
                    |
                       <asp:HyperLink onclick='popupDate();return false;' href='javascript:void(0)' ID="btnDateConverter" runat="server">Date Converter</asp:HyperLink>
                  
                    <asp:HyperLink  ID="linkChangePwd" runat="server" Text="Change Password" NavigateUrl="~/Employee/ChangePassword.aspx" />
                    |
                     <asp:LinkButton  ID="btnSwitch" runat="server" CausesValidation="false" OnClick="btnSwitch_Click">Switch User</asp:LinkButton>
                     <span runat="server" id="swithSep">|</span>
                    <asp:HyperLink ID="linkManualAttendance"  visible="false" runat="server" Text="Submit Attendance" NavigateUrl="~/Employee/SubmitAttendance.aspx" />
                   <span runat="server" id="spanManualAttendance" visible="false" > |</span>
                    <asp:LinkButton ID="btnLogout" runat="server" CausesValidation="false" OnClick="btnLogout_Click">Logout</asp:LinkButton>
                </span>              

            </div>
            <!-- my navigation -->
            <div style="text-align: left; clear: both">
                <ul id="mbmcpebul_table" class="mbmcpebul_menulist css_menu" style="text-align: left;margin-top:12px;">
                    <asp:Repeater ID="rptMainMenu" runat="server">
                        <ItemTemplate>
                            <li runat="server" visible='<%# IsAccessible(((XmlElement)Container.DataItem).Attributes["pageId"],((XmlElement)Container.DataItem).Attributes["url"]) %>'
                                style='<%# (((XmlElement)Container.DataItem).Attributes["visible"] == null) ? "": "display:none" %>'
                                class='<%# (((XmlElement)Container.DataItem).Attributes["title"].Value == "Home") ? "topitem spaced_li first_button": ( (((XmlElement)Container.DataItem).Attributes["title"].Value == "Help") ? "topitem spaced_li last_button" : "topitem spaced" ) %>'>
                                <%--main menu text/link--%>
                                <div runat="server" id="span1" style='<%# BLL.SessionManager.CurrentLoggedInEmployeeId != 0 && (  ((XmlElement)Container.DataItem).Attributes["url"].Value.ToLower() == "~/employee/timesheetinputapproval.aspx?ap=" || ((XmlElement)Container.DataItem).Attributes["url"].Value.ToLower() == "~/employee/timesheetinputreview.aspx?rv=" ) ? "width:180px": ""  %>'
                                    class="buttonbg gradient_button gradient35">
                                    <div class='<%# ((XmlElement)Container.DataItem).ChildNodes.Count > 0 ? "arrow" : "" %>'>
                                        <a runat="server" href='<%# GetUrl( ((XmlElement)Container.DataItem).Attributes["url"] )%>'>
                                            <%# ((XmlElement)Container.DataItem).Attributes["title"].Value %></a></div>
                                </div>                             
                                <%--for normal dropdown menu--%>
                                <ul id='ff' runat="server" visible='<%# ((XmlElement)Container.DataItem).ChildNodes.Count > 0  ? true : false %>'>
                                    <asp:Repeater ID="rpt1" DataSource='<%# Eval("ChildNodes") %>' runat="server" Visible='<%# (((XmlElement)Container.DataItem).Attributes["isMegaMenu"] == null) ? true: false %>'>
                                        <ItemTemplate>
                                            <li runat="server" visible='<%# ((XmlElement)Container.DataItem).Attributes["url"].Value == "AllReports.aspx" || ((XmlElement)Container.DataItem).Attributes["url"].Value == "AllSettings.aspx" ? true : false  %>'
                                                style="background-image: url('../styles/images/menuseparator.png')" class="menuSeparator">
                                                <span>&nbsp;</span> </li>
                                            <li id="Li1" runat="server" visible='<%# IsAccessible(((XmlElement)Container.DataItem).Attributes["pageId"],((XmlElement)Container.DataItem).Attributes["url"]) %>'
                                                style='<%# (((XmlElement)Container.DataItem).Attributes["visible"] == null) ? "": "display:none" %>'
                                                class='<%# ((XmlElement)Container.DataItem).Attributes["title"].Value == "Favourite" ? "fav" : "t"  %>'>
                                                <asp:HyperLink ToolTip='<%# GetToolTip( ((XmlElement)Container.DataItem).Attributes["tooltip"]) %>'
                                                    ID="HyperLink1" runat="server" NavigateUrl='<%# ((XmlElement)Container.DataItem).Attributes["url"].Value %>'><%# ((XmlElement)Container.DataItem).Attributes["title"].Value%></asp:HyperLink>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>                                
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="nav_bar_d" runat="server" class="nav_bar_d">
    <asp:Repeater ID="rptSubMenus" runat="server">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li id="Li2" runat="server" visible='<%# IsAccessible(((XmlElement)Container.DataItem).Attributes["pageId"],((XmlElement)Container.DataItem).Attributes["url"]) %>'>
                <asp:HyperLink ID="HyperLink1" Style='<%# GetSelected(((XmlElement)Container.DataItem).Attributes["url"].Value) %>'
                    runat="server" Visible='<%# ((XmlElement)Container.DataItem).Attributes["visible"] == null %>'
                    NavigateUrl='<%# ((XmlElement)Container.DataItem).Attributes["url"].Value %>'>
				<%# ((XmlElement)Container.DataItem).Attributes["title"].Value%>
                </asp:HyperLink>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>