﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpDownCtl.ascx.cs" Inherits="Web.Controls.UpDownCtl" %>
<SCRIPT language=Javascript>
      <!--
      function isNumberKey(evt) {
        

         var charCode = (evt.which) ? evt.which : event.keyCode

         //if( typeof(allowDecimal) != 'undefined' && allowDecimal == true)
         {

            if (charCode == 46)
                return true
         }

         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
      //-->
   </SCRIPT>
<div style="position: relative; width: 120px; font-size: 20px; height: 20px; padding-right: 24px;
    display: inline-block;">
    <table cellspacing="0" cellpadding="0" style="border-collapse: collapse; display: inline;
        position: relative;">
        <tbody>
            <tr>
                <td rowspan="2" style="vertical-align: middle; padding: 0pt; margin: 0pt;">
                    <asp:TextBox ID="txt" onkeypress="return isNumberKey(event)" style='width:40px;height:18px' value="0" runat="server" />
                </td>
                <td style="vertical-align: middle; padding: 0pt; margin: 0pt;">
                    <asp:ImageButton ID="imgEUp" Width="16px" Height="11px" ImageUrl="~/images/imgUp.png"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align: middle; padding: 0pt; margin: 0pt;">
                    <asp:ImageButton ID="imgEDown" Width="16px" Height="11px" ImageUrl="~/images/imgDown.png"
                        runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</div>
<cc1:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server"   TargetControlID="txt"
    Step="1" TargetButtonDownID="imgEDown"  TargetButtonUpID="imgEUp" Minimum="0" />

