﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ext.Net;

namespace Web.Controls
{
    public partial class MenuDepartmentalActionControl : BLL.Base.BaseUserControl
    {
        HtmlGenericControl[] menuList = null;

        protected void Page_Load(object sender, EventArgs e)
        {
          
           
        }

        void setMenuLink()
        {
            foreach (HtmlGenericControl ctlMain in menuList)
            {
                int index = int.Parse(ctlMain.ID[ctlMain.ID.Length - 1].ToString());

                HtmlAnchor anchor = ctlMain.Parent.Parent.FindControl("anchor" + index) as HtmlAnchor;

                if(anchor != null)
                {
                    anchor.Attributes["href"] = "#" + ctlMain.ClientID;
                }
            }
        }

     


        /// <summary>
        /// Iterate all page anchor tags & set active class to its first parent control for menu selection
        /// </summary>
        public void SelectPageMenu()
        {
            string currentPageUrl = this.CurrentPageUrl.ToLower();

            foreach (HtmlGenericControl ctlMain in menuList)
            {
               
                {

                    foreach (Control ctl in ctlMain.Controls)
                    {
                        HtmlAnchor anchor = ctl as HtmlAnchor;
                        if (anchor != null && !string.IsNullOrEmpty(anchor.HRef))
                        {
                            if (anchor.HRef.ToLower().Contains(currentPageUrl))
                            {
                                anchor.Attributes["class"] = "collapse in";

                                HtmlGenericControl parentliControl = anchor.Parent.Parent as HtmlGenericControl;
                                parentliControl.Attributes["class"] += " active";

                                ctlMain.Attributes["class"] = "collpase in";

                                break;
                            }
                        }
                    }
                }
            }
        }

        public void ShowHideEmployeeHRMenus()
        {
            bool hasEmployeeId = false;
            int employeeId = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hasEmployeeId = true;
                employeeId = int.Parse(Request.QueryString["ID"]);
            }

            foreach (HtmlGenericControl ctlMain in menuList)
            {
                        

                {
                    foreach (Control ctl in ctlMain.Controls)
                    {
                        //H
                        HtmlAnchor anchor = ctl as HtmlAnchor;
                        if (anchor != null)
                        {
                            if (hasEmployeeId == false && !string.IsNullOrEmpty(anchor.Attributes["IsEmployeeDetails"]))
                            {
                                anchor.Visible = false;
                            }
                            else if (!string.IsNullOrEmpty(anchor.Attributes["IsEmployeeDetails"]))
                            {
                                anchor.Attributes["href"] += "?ID=" + employeeId;
                            }
                        }
                    }
                }
            }
        }

    }
}