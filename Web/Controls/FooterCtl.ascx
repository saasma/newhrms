﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterCtl.ascx.cs" Inherits="Web.Controls.FooterCtl" %>
<div id="footer" style='clear: both; text-align: center; height: 34px; vertical-align: middle;
    background: #eaeaea; border-top: 1px solid lightgray;'>
    <div class="footer_content">
        <div class="colRights">
            <span style="">Human Resource Management System by</span> <a href="http://rigonepal.com">
                Rigo Technologies </a>
        </div>
    </div>
</div>
