﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FilterCtl.ascx.cs" Inherits="Web.Controls.FilterCtl" %>
<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/MultiCheckCombo.ascx" TagName="MultiCheckCombo" TagPrefix="uc3" %>
<script type="text/javascript">
    function showFilter() {
        document.getElementById('<%=rowOtherFilter.ClientID %>').style.display = "";
        document.getElementById('<%=filterShown.ClientID %>').value = 'true';
    }
</script>
<div class="attribute">
    <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoads" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel runat="server" DefaultButton="btnLoads">
                <table class="fieldTable" runat="server">
                    <tr>
                        <td runat="server" id="rowBranch1">
                            <strong>Branch</strong>
                        </td>
                        <td runat="server" id="rowDep1">
                            <strong>Department</strong>
                        </td>
                        <td runat="server" id="rowSubDep1" visible="false">
                            <strong>Sub Department</strong>
                        </td>
                        <td runat="server" id="rowLevel1" visible="false">
                            <strong runat="server" id="levelName">Level/Position</strong>
                        </td>
                        <td runat="server" id="rowDesignation1" visible="false">
                            <strong>Position - Designation</strong>
                        </td>
                        <td runat="server" id="rowDesignationOnly1" visible="false">
                            <strong>Designation</strong>
                        </td>
                        <td runat="server" id="rowStatus1" visible="false">
                            <strong>Status</strong>
                        </td>
                        <%-- <td runat="server" id="rowWorkShift1" visible="false">
                            <strong>Work Shift</strong>
                        </td>--%>
                        <td runat="server" id="rowIDCardNo1" visible="false">
                            <strong>I No</strong>
                        </td>
                        <td runat="server" id="rowEmpName1" visible="false">
                            <strong>Search</strong>
                        </td>
                        <td runat="server" id="rowCITRoundOff" visible="false">
                            <strong>CIT Rounding-Off</strong>
                        </td>
                        <td rowspan="2" valign="bottom">
                            <asp:Button ID="btnLoads" CssClass="btn btn-default btn-sect btn-sm" Style="width: 80px"
                                OnClick="btnLoad_Click" runat="server" Text="Load"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td runat="server" id="rowBranch2">
                            <asp:DropDownList ID="ddlBranch" Width="120px" DataTextField="Name" DataValueField="BranchId"
                                AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowDep2">
                            <asp:DropDownList ID="ddlDepartment" Width="130px" DataTextField="Name" DataValueField="DepartmentId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowSubDep2" visible="false">
                            <asp:DropDownList ID="ddlSubDepartment" Width="100px" DataTextField="Name" DataValueField="SubDepartmentId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowLevel2" visible="false">
                            <asp:DropDownList ID="ddlLevel" Width="130px" DataTextField="Name" DataValueField="LevelId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowDesignation2" visible="false">
                            <asp:DropDownList ID="ddlDesignation" Width="130px" DataTextField="LevelAndDesignation"
                                DataValueField="DesignationId" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowDesignationOnly2" visible="false">
                            <asp:DropDownList ID="ddlDesignationOnly" Width="120px" DataTextField="Text" DataValueField="Text"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="All" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowStatus2" visible="false">
                            <%--   <asp:DropDownList ID="ddlStatus" Width="110px" DataTextField="Key" DataValueField="KeyValueCombined"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>--%>
                            <uc3:MultiCheckCombo ID="multiStatus" runat="server" />
                        </td>
                        <td runat="server" id="rowIDCardNo2" visible="false">
                            <asp:TextBox Width="60px" ID="txtIDCardNo" runat="server"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtIDCardNo"
                                WatermarkText="I No" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtender1" runat="server"
                                MinimumPrefixLength="1" ServiceMethod="GetEmployeeByIdNo" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtIDCardNo" CompletionSetCount="10" CompletionInterval="250"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td runat="server" id="rowEmpName2" visible="false">
                            <asp:TextBox Width="130px" ID="txtEmployeeName" AutoPostBack="true" runat="server"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmployeeName"
                                WatermarkText="Employee" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="1" ServiceMethod="GetEmployeeNames" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtEmployeeName" CompletionSetCount="10" CompletionInterval="250"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td runat="server" id="rowCITRoundOffValue" visible="false">
                            <asp:DropDownList ID="ddlRounding" runat="server" Width="120">
                                <asp:ListItem Text="0" Value="0" Selected="True" />
                                <asp:ListItem Text="100" Value="100" />
                                <asp:ListItem Text="500" Value="500" />
                                <asp:ListItem Text="1000" Value="1000" />
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:HiddenField runat="server" ID="filterShown" />
                            <i class="fa fa-caret-down" onclick='showFilter();' style='font-size: 18px; padding: 2px;
                                cursor: pointer; width: 24px; padding-left: 7px;'></i>
                        </td>
                    </tr>
                    
                </table>
                <table>
                
                    <tr id='rowOtherFilter' runat="server" tooltip='more filter' style='display: none'>
                        <td runat="server" id="tdGroup" style='padding-top: 10px; padding-left: 10px;'>
                            <span style='display: block;'>Group</span>
                            <asp:DropDownList ID="ddlGroups" Style='margin-top: 5px' Width="120px" DataTextField="Name"
                                DataValueField="LevelGroupId" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="All" Value="-1" />
                            </asp:DropDownList>
                        </td>

                        <td visible="false"  runat="server" id="tdPayGroup" style='padding-top: 10px; padding-left: 10px;'>
                            <span style='display: block;'>Pay Group</span>
                            <asp:DropDownList ID="ddlPayGroup" Style='margin-top: 5px' Width="120px" DataTextField="Name"
                                DataValueField="PayGroupID" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="All" Value="-1" />
                            </asp:DropDownList>
                        </td>
                    
                        <td  visible="false"  runat="server" id="tdUnit" style='padding-top: 10px; padding-left: 10px;'>
                            <span style='display: block;'>Unit</span>
                            <asp:DropDownList ID="ddlUnit" Style='margin-top: 5px' Width="120px" DataTextField="Name"
                                DataValueField="UnitID" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="All" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
