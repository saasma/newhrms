﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Controls
{
    public partial class InfoMsgCtl : System.Web.UI.UserControl
    {
        public string InnerHtml
        {
            get
            {
                return msgText.InnerHtml;
            }
            set
            {
                msgText.InnerHtml = value;
            }
        }
       
        public bool Hide
        {
            set
            {
                msg.Style.Remove("display");
                if(value)
                    msg.Style.Add("display", "none");
            }
        }

        public string Width
        {
            set
            {
                msg.Style["width"] = value;
            }

        }



        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
    }
}