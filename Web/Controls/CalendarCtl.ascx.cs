﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Base;
using Utils.Calendar;
using System.Web.UI.HtmlControls;
using Utils.Web;

namespace Web.UserControls
{
    /// <summary>
    /// Represents eng/nep calendar, to extend more years change in the following
    /// 1) Page_Load for items in year drop down
    /// 3) In CustomDate csharp class which is used for conversion
    /// </summary>
    public partial class CalendarCtl : BaseUserControl
    {

        public const int StartNepYear = 2000;
        private readonly int EndNepYear = 2105;
        public const int StartEngYear = 1944;
        private readonly int EndEngYear = 2045;


        public  bool IsSkipDay
        {
            get
            {
                if (!string.IsNullOrEmpty(calIsSkipDay.Value))
                    return Boolean.Parse(calIsSkipDay.Value);
                return false;
            }
            set
            {
                if (calIsSkipDay.Value != value.ToString())
                {
                    calIsSkipDay.Value = value.ToString();
                }     
            }
        }

        public string ToolTip
        {
            set
            {
                this.date.ToolTip = value;
                this.dated.Attributes.Add("title", value);
                this.datem.Attributes.Add("title", value);
                this.datey.Attributes.Add("title", value);
            }
        }

        public bool IsSkipYear
        {
            get
            {
                if (!string.IsNullOrEmpty(calIsSkipYear.Value))
                    return Boolean.Parse(calIsSkipYear.Value);
                return false;
            }
            set
            {
                if (calIsSkipYear.Value != value.ToString())
                {
                    calIsSkipYear.Value = value.ToString();
                }
            }
        }
        public bool IsSkipMonth
        {
            get
            {
                if (!string.IsNullOrEmpty(calIsSkipMonth.Value))
                    return Boolean.Parse(calIsSkipMonth.Value);
                return false;
            }
            set
            {
                if (calIsSkipMonth.Value != value.ToString())
                {
                    calIsSkipMonth.Value = value.ToString();
                }
            }
        }
       public bool IsEnglishCalendar
        {
            get
            {
                            
                //if (value != null)
                //    return Boolean.Parse(value);
                //else
                   return  Boolean.Parse(calType.Value);
            }
            set
            {
                if (calType.Value != value.ToString())
                {
                    calType.Value = value.ToString();
                    LoadYear();
                    LoadMonth();
                }     
                
            }
        }
       
        public bool Enabled
        {
            get
            {
                
                //if (value != null)
                //    return Boolean.Parse(value);
                //else
                    return Boolean.Parse(calEnabled.Value);                
            }
            set
            {
                calEnabled.Value = value.ToString();
                //date.Enabled = value;
            }
        }

        public CustomDate GetTodayDate()
        {
            DateTime now = Utils.Helper.Util.GetCurrentDateAndTime();
            CustomDate date = new CustomDate(now.Day, now.Month, now.Year, true);
            if (IsEnglishCalendar == false)
            {
                date = CustomDate.ConvertEngToNep(date);
            }
            return date;
        }

        public void SelectTodayDate()
        {
            DateTime now = BLL.BaseBiz.GetCurrentDateAndTime();
            CustomDate date = new CustomDate(now.Day,now.Month,now.Year,true);
            ListItem item;

            if(IsEnglishCalendar==false)
            {
                date =  CustomDate.ConvertEngToNep(date);
            }
            
            item = dated.Items.FindByValue(date.Day.ToString());
            if (item != null)
            {
                ClearSelection(dated);
                item.Selected = true;
            }

            item = datem.Items.FindByValue(date.Month.ToString());
            if (item != null)
            {
                ClearSelection(datem);
                item.Selected = true;
            }
            item = datey.Items.FindByValue(date.Year.ToString());
            if (item != null)
            {
                ClearSelection(datey);
                item.Selected = true;
            }
        }

        /// <summary>
        /// Get the selected date
        /// </summary>
        public CustomDate SelectedDate
        {
            get
            {
                CustomDate date = new CustomDate();
                date.Day = int.Parse(dated.Value);
                date.Month = int.Parse(datem.Value);
                date.Year = int.Parse(datey.Value);
                date.IsEnglishDate = this.IsEnglishCalendar;
                return date;
            }
        }

        public CustomDate SelectedDateEng
        {
            get
            {
                CustomDate date = this.SelectedDate;

                if(this.IsEnglishCalendar)
                    return date;

                return CustomDate.ConvertNepToEng(date);
            }
        }

        /// <summary>
        /// Get the first day in selected day, mainly used with IsDaySkip
        /// </summary>
        public  CustomDate SelectedDateAsFirstDay
        {
            get
            {
                CustomDate date = new CustomDate();
                date.Day = 1;
                date.Month = int.Parse(datem.Value);
                date.Year = int.Parse(datey.Value);
                date.IsEnglishDate = this.IsEnglishCalendar;
                return date;
            }
        }

        /// <summary>
        /// Get the first day in selected day, mainly used with IsDaySkip
        /// </summary>
        public CustomDate SelectedDateAsLastDay
        {
            get
            {


                CustomDate date = new CustomDate();

                date.Month = int.Parse(datem.Value);
                date.Year = int.Parse(datey.Value);
                date.Day =
                  DateHelper.GetTotalDaysInTheMonth(date.Year, date.Month, IsEnglishCalendar);
                date.IsEnglishDate = this.IsEnglishCalendar;
                return date;
            }
        }

        private void ClearSelection(HtmlSelect select)
        {
            if (select.SelectedIndex >= 0 && select.SelectedIndex < select.Items.Count)
            {
                ListItem item = select.Items[select.SelectedIndex];
                item.Selected = false;
            }


            //foreach (ListItem item in select.Items)
            //{                
            //    if (item.Selected)
            //        item.Selected = false;
            //}
        }

        public void SetSelectedDate(string dateStr, bool isEnglishDate)
        {
            if (string.IsNullOrEmpty(dateStr))
                return;

            //this.IsValueAlreadySet = true;
            this.IsEnglishCalendar = isEnglishDate;

            CustomDate date = CustomDate.GetCustomDateFromString(dateStr, isEnglishDate);
            ListItem item;

            item = dated.Items.FindByValue(date.Day.ToString());
            if (item != null)
            {
                ClearSelection(dated);
                item.Selected = true;
            }
            item = datem.Items.FindByValue(date.Month.ToString());
            if (item != null)
            {
                ClearSelection(datem);
                item.Selected = true;
            }

            item = datey.Items.FindByValue(date.Year.ToString());
            if (item != null)
            {
                ClearSelection(datey);
                item.Selected = true;
            }
            
        }




        protected void Page_Init(object sender, EventArgs e)
        {
            string value = Request.Form[calType.UniqueID];
            if (value != null)
                this.IsEnglishCalendar = Boolean.Parse(value);
            value = Request.Form[calEnabled.UniqueID];
            if (value != null)
                this.Enabled = Boolean.Parse(value);

            value = Request.Form[calIsSkipDay.UniqueID];
            if (!string.IsNullOrEmpty(value))
                this.IsSkipDay = Boolean.Parse(value);

            value = Request.Form[calIsSkipYear.UniqueID];
            if (!string.IsNullOrEmpty(value))
                this.IsSkipYear = Boolean.Parse(value);

            value = Request.Form[calIsSkipMonth.UniqueID];
            if (!string.IsNullOrEmpty(value))
                this.IsSkipMonth = Boolean.Parse(value);

            LoadYear();
            LoadMonth();
            RestoreSelectedValues();
        }


        void Page_PreRender(object sender, EventArgs e)
        {
            RegisterScript();
            date.Enabled = this.Enabled;
            if (date.Enabled == false)
            {
                dated.Attributes.Add("disabled", "disabled");
                datem.Attributes.Add("disabled", "disabled");
                datey.Attributes.Add("disabled", "disabled");
            }

            calPreviousValue.Value = this.SelectedDate.ToString();
            
            if( this.IsSkipDay)
                dated.Style.Add("display","none");
            if( this.IsSkipYear)
                datey.Style.Add("display","none");
            if (this.IsSkipMonth)
                datem.Style.Add("display", "none");

            UIHelper.SetInputControlsHighlight(this, "x-form-focus", false);
        }

        void RestoreSelectedValues()
        {
            // set selected
            string day = Request.Form[dated.UniqueID];
            string month = Request.Form[datem.UniqueID];
            string year = Request.Form[datey.UniqueID];

            CustomDate date = null;
            if (!string.IsNullOrEmpty(Request.Form[calPreviousValue.UniqueID]))
            {
                date = CustomDate.GetCustomDateFromString(Request.Form[calPreviousValue.UniqueID],
                    this.IsEnglishCalendar);

                day = date.Day.ToString();
                month = date.Month.ToString();
                year = date.Year.ToString();

            }

            ListItem item;
            if (day != null)
            {
                item = dated.Items.FindByValue(day);
                if (item != null)
                {
                    ClearSelection(dated);
                    item.Selected = true;
                }
            }


           
            if (month != null)
            {
                item = datem.Items.FindByValue(month);
                if (item != null)
                {
                    ClearSelection(datem);
                    item.Selected = true;
                }
            }


            
            if (year != null)
            {
                item = datey.Items.FindByValue(year);
                if (item != null)
                {
                    ClearSelection(datey);
                    item.Selected = true;
                }
            }

        }

        void LoadYear()
        {
            datey.Items.Clear();

            int start, end;

            if (this.IsEnglishCalendar)
            {
                start = StartEngYear;
                end = EndEngYear;
            }
            else
            {
                start = StartNepYear;
                end = EndNepYear;
            }
            for (int i = start; i <= end; i++)
            {
                datey.Items.Add(new ListItem(i.ToString(), i.ToString()));                    
            }
        }

        void LoadMonth()
        {
            datem.Items.Clear();

            string[] months;

            if (this.IsEnglishCalendar)
                months = DateHelper.EngShortMonths;
            else
                months = DateHelper.NepMonths;

            //foreach (string str in months)
            for(int i=1;i<=12;i++)
            {
                datem.Items.Add(new ListItem(months[i], i.ToString()));
            }
        }

        

        void RegisterScript()
        {          
            string pre;
            if (this.IsEnglishCalendar)
                pre = "EN";
            else
                pre = "NE";

            string setDays = string.Format("setdays{1}('{0}', 1)", date.ClientID,pre);

            dated.Attributes.Add("onchange", setDays);
            datem.Attributes.Add("onchange", setDays);
            datey.Attributes.Add("onchange", setDays);

            AdjustMonth();
        }


        /// <summary>
        /// Remove total days for the current month
        /// </summary>
        void AdjustMonth()
        {          
            CustomDate currentDate = SelectedDate;
            int totalDays;
            totalDays = DateHelper.GetTotalDaysInTheMonth(currentDate.Year, currentDate.Month, IsEnglishCalendar);
            
            for (int i = totalDays+1; i <=32; i++)
            {
                ListItem item = dated.Items.FindByValue(i.ToString());
                if (item != null)
                    dated.Items.Remove(item);
            }
        }

        
    }


 
}