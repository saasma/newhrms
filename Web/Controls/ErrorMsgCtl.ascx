﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorMsgCtl.ascx.cs"
    Inherits="Web.Controls.ErrorMsgCtl" %>
<div runat="server" id="msg" class="notify bg-red bigger">
    <em>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;</em>
    <div runat="server" id="msgText" class="message" style='display: inline!important;
        width: 88%'>
    </div>
    <ul runat="server" enableviewstate="false" id="msgList">
    </ul>
</div>
