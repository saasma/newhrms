﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;

namespace Web.UserControls
{
    public partial class CurrentCompanyName : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = Request.Url.ToString().ToLower();
            //handle for company deleting page
            if (url.IndexOf("companydel.aspx") > 0 || url.IndexOf("companydelconf.aspx") > 0)
            {
                CompanyManager mgr = new CompanyManager();
                Company c = mgr.GetById(SessionManager.CompanyDeleteId);
                if (c != null)
                {
                    lbl.Text = c.Name;
                }
            }
            else
            {
                if (SessionManager.CurrentCompany != null)
                {
                    lbl.Text = SessionManager.CurrentCompany.Name;
                }
            }
        }
    }
}