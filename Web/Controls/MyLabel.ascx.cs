﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.UserControls
{
    public partial class MyLabel : System.Web.UI.UserControl
    {
        public bool _showAstrick;
        public bool ShowAstrick
        {
            get { return _showAstrick; }
            set { _showAstrick = value; }
        }

        public string Text
        {
            get { return lbl.Text; }
            set { lbl.Text = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ast.Visible = ShowAstrick;
        }
    }
}