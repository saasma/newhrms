﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Controls
{
    public delegate void ShowRecordsChanged();

    public delegate void NextRecordChanged();

    public delegate void PrevRecordChanged();

    public delegate void ChangeToFixedPage();

    public partial class PagingCtl : System.Web.UI.UserControl
    {
        public event ShowRecordsChanged DropDownShowRecordsChanged;
        public event NextRecordChanged NextRecord;
        public event PrevRecordChanged PrevRecord;
        public event ChangeToFixedPage ChangePage;

        private int _tempCurrentPage = 1; // Default initialisation
        //private int _tempCount;


        
        protected override void OnInit(EventArgs e)
        {
            if (Session["PageSize"] != null)
            {
                int size;
                if (int.TryParse(Session["PageSize"].ToString(), out size))
                {
                    ddlRecords.SelectedValue = size.ToString();
                }
            }

            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }
        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            _tempCurrentPage = (int)rgState[1];

        }
        protected override object SaveControlState()
        {
            object[] rgState = new object[3];
            rgState[0] = base.SaveControlState();
            rgState[1] = _tempCurrentPage;
            return rgState;
        }
       
        

        public string PageSizeChangedJS
        {
            set
            {
                ddlRecords.Attributes.Add("onchange", value);
                btnNext.OnClientClick = value;
                btnPrevious.OnClientClick = value;
            }
        }

        public int CurrentPage
        {
            get { return _tempCurrentPage; }

            set { _tempCurrentPage = value; }
        }

        public Label LabelCurrentPage
        {
            get
            {
                
                return lblCurrentPage;
            }
        }
        public Label LabelTotalPage
        {
            get { return lblTotalPage; }
        }

        public DropDownList DDLRecords
        {
            get { return ddlRecords; }
        }
        public Label LabelTotalRecords
        {
            get { return lblTotalRecords; }
        }
        public LinkButton ButtonPrev
        {
            get { return btnPrevious; }
        }
        public  LinkButton ButtonNext
        {
            get { return btnNext; }
        }
        public bool ShowDropDown
        {
            set
            {
                if (value)
                {
                    ddlPageNumber.Style["display"] = "";
                    lblCurrentPage.Style["display"] = "none";
                }
            }
        }

        public void EnableDisablePreviousButton(bool enable)
        {
            if (enable)
            {
                ButtonPrev.Enabled = true;
                ButtonPrev.CssClass = "";
            }
            else
            {
                ButtonPrev.Enabled = false;
                ButtonPrev.CssClass = "PaginationBarDisableNextPrev";
            }
        }

        public void EnableDisableNextButton(bool enable)
        {
            if (enable)
            {
                ButtonNext.Enabled = true;
                ButtonNext.CssClass = "";
            }
            else
            {
                ButtonNext.Enabled = false;
                ButtonNext.CssClass = "PaginationBarDisableNextPrev";
            }
        }
        public DropDownList DDLPageNumber
        {
            get
            {
                return ddlPageNumber;
            }
        }
        public void UpdatePagingBar(int totalRecords)
        {
            if (totalRecords <= 0)
            {
                this.Visible = false;
            }
            else
            {
                this.Visible = true;
            }
            int totalPages = (int)CalculateTotalPages(totalRecords);
            this.LabelCurrentPage.Text = _tempCurrentPage.ToString();

            this.LabelTotalPage.Text = (totalPages).ToString();

            this.lblTotalRecords.Text = string.Format(" ({0})",totalRecords);

            // Update Pge numbers
            if (ddlPageNumber.Items.Count != totalPages)
            {
                ddlPageNumber.Items.Clear();
                for (int i = 1; i <= totalPages; i++)
                {
                    ddlPageNumber.Items.Add(i.ToString());
                }
            }
            if (this.ddlPageNumber.Items.FindByValue(_tempCurrentPage.ToString()) != null)
                this.ddlPageNumber.SelectedValue = _tempCurrentPage.ToString();
            

            if (_tempCurrentPage <= 1)
            {
                this.EnableDisablePreviousButton(false);
            }
            else
            {
                this.EnableDisablePreviousButton(true);
            }

            if (_tempCurrentPage < totalPages)
                this.EnableDisableNextButton(true);
            //pagintCtl.ButtonNext.Enabled = true;
            else
                this.EnableDisableNextButton(false);
        }

        private int CalculateTotalPages(double totalRows)
        {
            int totalPages = (int)Math.Ceiling(totalRows / int.Parse(this.DDLRecords.SelectedValue));

            return totalPages;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
         
        }

        protected void ddlPageNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ChangePage != null)
            {
                this.CurrentPage = int.Parse(ddlPageNumber.SelectedValue);

                ChangePage();
            }
        }

        protected void ddlRecords_SelectedIndexChanged(object sender, EventArgs e)
        {

            Session["PageSize"] = ddlRecords.SelectedValue;

            DropDownShowRecordsChanged();

        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            PrevRecord();
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            NextRecord();
        }


    }
}