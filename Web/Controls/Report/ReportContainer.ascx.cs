﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Web.CP.Report.Templates.HR;
using Web.ReportDataSetTableAdapters;
using DevExpress.XtraReports.Web;
using System.IO;

namespace Web.Controls.Report
{
    public partial class ReportContainer : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    LoadReport();
        //}

        public ReportToolbar ReportToolbar
        {
            get
            {
                return ReportToolbar1;
            }
           
        }
        
        public bool SinglePage
        {
            set
            {
                if (value)
                {
                    rptViewer.PageByPage = false;
                    rptViewer.AutoSize = true;
                }
            }
        }

        public void RemovePDFExport()
        {
            ReportToolbarComboBox toolbar = ReportToolbar1.Items[16] as ReportToolbarComboBox;
            toolbar.Elements.RemoveAt(0);
        }


        public void BindLeaves()
        {

           

            filterPayroll.BindLeaves();
        }

        public string Message
        {
            set
            {
                msg.InnerHtml = value;
            }
        }

        public bool ShowCompanySpecificExport
        {
            set { filterPayroll.ShowCompanySpecificExport = value; filterPayroll.ShowCompanySpecificExportButton(value); }
            get { return filterPayroll.ShowCompanySpecificExport; } 
        }
        //private bool ClearCache = false;
        public bool clearCache
        {
            get
            {
                if (ViewState["ClearCache"] == null)
                    return false;
                return bool.Parse(ViewState["ClearCache"].ToString());
            }
            set
            {
                ViewState["ClearCache"] = value;
            }
        }

        public Unit ViewerPaddingLeft
        {
            get
            {
                return this.rptViewer.Paddings.PaddingLeft;
            }
            set
            {
                this.rptViewer.Paddings.PaddingLeft = value;
            }

        }

        public void SelectLastPayrollAndDisableDropDown()
        {
            filterPayroll.SelectLastPayrollAndDisableDropDown();
        }
        public void Page_Load(Object sender, EventArgs args)
        {
            if (!IsPostBack)
            {


                //register postback script as pay report is not working
                //in first load
                //TODO : remove this code later
                // Page.ClientScript.RegisterClientScriptBlock
                //   (this.GetType(), "sdfsdf", " $(document).ready( function(){ __doPostBack('','');  } );", true);
            }
          
        }

        public ReportViewer ReportViewer
        {
            get
            {
                return rptViewer;
            }
        }

           public event LoadReport ReloadReport;


           #region OnReloadReport
           /// <summary>
           /// Triggers the ReloadReport event.
           /// </summary>
           public  void LoadReport()
           {
           
              
               if (ReloadReport != null)
               {
                   this.clearCache = true;
                   ReloadReport();
               }
           }
           #endregion

           public event CompanySpecificExport CompanyExport;


           #region OnReloadReport
           /// <summary>
           /// Triggers the ReloadReport event.
           /// </summary>
           public void Export()
           {


               if (CompanyExport != null)
               {
                  
                   CompanyExport();
               }
           }
           #endregion

        public void DisplayReport(DevExpress.XtraReports.UI.XtraReport xtraReport)
        {

          


            this.rptViewer.Report = xtraReport;
            this.ReportToolbar1.ReportViewer = this.rptViewer;

        }

        public ReportFilterBranchDep Filter
        {
            get
            {
                return this.filterPayroll;
            }
        }

        protected void ReportViewer1_CacheReportDocument(object sender, CacheReportDocumentEventArgs e)
        {
            return;
            ///won't work for export so commented
            //get page name as key
            //e.Key = this.Page.ToString();// Guid.NewGuid().ToString();
            //Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
            //this.clearCache = false;

        }
        protected void ReportViewer1_RestoreReportDocumentFromCache(object sender, RestoreReportDocumentFromCacheEventArgs e)
        {
            return;
            ///won't work for export so commented

            //if (!this.clearCache)
            //{
            //    Stream stream = Page.Session[e.Key] as Stream;
            //    if (stream != null)
            //        e.RestoreDocumentFromStream(stream);
            //}
        }
    }
}