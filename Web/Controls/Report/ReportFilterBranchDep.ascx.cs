﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using Utils.Calendar;
using Web.Controls.Report;
using DevExpress.Web;
using Web.UserControls;
using BLL.BO;
using BLL.Base;
using Utils.Web;
using Utils.Helper;

namespace Web.Controls
{
    
    public delegate void LoadReport();
    public delegate void CompanySpecificExport(); 

    public partial class ReportFilterBranchDep : BaseUserControl
    {
        public event LoadReport ReloadReport;
        public event CompanySpecificExport CompanySpecificExportReport = null;

        public bool ShowCompanySpecificExport;

        public bool IncludeRetiredEmployeeInSearch { get; set; }

        public void btnCompanySepecificExport_Click(object sender, EventArgs e)
        {
            if (CompanySpecificExportReport != null)
                CompanySpecificExportReport();

           
        }

        public void SetEditHeaderFooter(ReportHeaderFooterEnum type)
        {
            btnEditHeaderFooter.OnClientClick = "popupReportEditCall(" + ((int)type) +  ");return false;";
            btnEditHeaderFooter.Visible = true;
        }

        public void ShowCompanySpecificExportButton(bool show)
        {
            if (show && (CommonManager.CompanySetting.WhichCompany == WhichCompany.GMC
               || CommonManager.CompanySetting.IsD2))
                btnCompanySpecificExport.Visible = show;
        }

        public bool ShowDynamic
        {
            set
            {
                rowDynamic1.Visible = value;
                rowDynamic2.Visible = value;
            }
        }

        public DropDownList DDLDynamic
        {
            get
            {
                return ddlDynamic;
            }
        }
        public string DynamicLabel
        {
            set
            {
                rowDynamic1.InnerHtml = "<strong>" + value + "</strong>";
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (rowPayrollTo1.Visible == false)
                rowPayrollFrom1.InnerHtml = "<strong>Payroll</strong>";


            
            if (BLL.BaseBiz.PayrollDataContext.ECostCodes.Any()==false)
            {
                this.CostCode = false;
            }

            if (BLL.BaseBiz.PayrollDataContext.EProgrammes.Any() == false)
            {
                this.Program = false;
            }

            if (BLL.BaseBiz.PayrollDataContext.SubDepartments.Any() == false)
            {
                this.SubDepartment = false;
            }
        }

        public bool ShowGratuityGenerateButton
        {
            set { btnReGenerateGratuity.Visible = true; }
            get
            {
                return btnReGenerateGratuity.Visible;
            }
        }

        public string WarningMessage
        {
            set
            {
                divWarningMsg.InnerHtml = value;
                divWarningMsg.Hide = false;
            }
        }

        public void btnReGenerateGratuity_Click(object sender, EventArgs e)
        {
            PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(StartDate.Month,
                                                                        StartDate.Year);
            if (payrollPeriod == null)
            {
                divWarningMsg.InnerHtml = "Period does not exists.";
                divWarningMsg.Hide = false;
                return;
            }

            PayrollPeriod currentPeriod = CommonManager.GetLastPayrollPeriod();
            PayrollPeriod prevPeriod = BLL.BaseBiz.PayrollDataContext.PayrollPeriods
                .Where(x => x.PayrollPeriodId != currentPeriod.PayrollPeriodId)
                .OrderByDescending(x => x.EndDateEng).FirstOrDefault();

            //if (this.EmployeeId == 0)
            //{
            //    divWarningMsg.InnerHtml = "Employee should be selected for report generation as report can be regenerated for single employee only.";
            //    divWarningMsg.Hide = false;
            //    return;

            //}

            if (payrollPeriod.PayrollPeriodId != currentPeriod.PayrollPeriodId &&
                payrollPeriod.PayrollPeriodId != prevPeriod.PayrollPeriodId)
            {


                divWarningMsg.InnerHtml = "Report can be generated for last two period only.";
                divWarningMsg.Hide = false;
                return;

            }
            else
            {
                int? empId = this.EmployeeId;
                if(empId==0)
                    empId = null;
                BLL.BaseBiz.PayrollDataContext.GenerateGratuityReport(payrollPeriod.PayrollPeriodId, empId,SessionManager.CurrentLoggedInUserID);



                // log for gratuity regenration
                if (this.EmployeeId == 0)
                {
                    BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                            BLL.BaseBiz.GetChangeUserActivityLog("Gratuity for " + payrollPeriod.Name + " recalculated for all employees."));
                }
                else
                {
                    BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                            BLL.BaseBiz.GetChangeUserActivityLog("Gratuity for " + EmployeeManager.GetEmployeeName(this.EmployeeId) + ", "  + payrollPeriod.Name + " recalculated for all employees."));
                }

                BLL.BaseBiz.PayrollDataContext.SubmitChanges();


                divMsgCtl.InnerHtml = "Gratuity report generated.";
                divMsgCtl.Hide = false;
                return;
            }

        }

        public void SelectLastPayrollAndDisableDropDown()
        {

            PayrollPeriod payroll =  CommonManager.GetLastPayrollPeriod();
            if (payroll != null)
            {
                foreach (ListItem itemMonth in ddlPayrollFromMonth.Items)
                {
                    if (itemMonth.Value == payroll.Month.ToString())
                    {
                        ddlPayrollFromMonth.ClearSelection();
                        itemMonth.Selected = true;
                        break;
                    }
                }
                foreach (ListItem itemMonth in ddlPayrollFromYear.Items)
                {
                    if (itemMonth.Value == payroll.Year.ToString())
                    {
                        ddlPayrollFromYear.ClearSelection();
                        itemMonth.Selected = true;
                        break;
                    }
                }
            }

            if (SessionManager.CurrentLoggedInEmployeeId != 0)
            {
                ddlPayrollFromYear.Enabled = false;
                ddlPayrollFromMonth.Enabled = false;
            }
        }
        public void SelectLastPayroll()
        {

            PayrollPeriod payroll = CommonManager.GetLastPayrollPeriod();
            if (payroll != null)
            {
                foreach (ListItem itemMonth in ddlPayrollFromMonth.Items)
                {
                    if (itemMonth.Value == payroll.Month.ToString())
                    {
                        ddlPayrollFromMonth.ClearSelection();
                        itemMonth.Selected = true;
                        break;
                    }
                }
                foreach (ListItem itemMonth in ddlPayrollFromYear.Items)
                {
                    if (itemMonth.Value == payroll.Year.ToString())
                    {
                        ddlPayrollFromYear.ClearSelection();
                        itemMonth.Selected = true;
                        break;
                    }
                }
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();

                //call to load report in first time
                btnLoad_Click(null, null);


            }
            else
            {
                ReloadMultipleCheckListDepartment();
            }
            LoadMultiBranch(null, null);

            JavascriptHelper.AttachPopUpCode(Page, "reportEdit", "../Popup/ReportHeaderFooterEdit.aspx", 900, 650);
        }

        public bool FromCalendar
        {
            set
            {
                rowFromDate1.Visible = value;
                rowFromDate2.Visible = value;
            }
        }
        public bool ToCalendar
        {
            set
            {
                rowToDate1.Visible = value;
                rowToDate2.Visible = value;
            }
        }
        public bool SSTTDSType
        {
            set
            {
                rowSSTTDSType1.Visible = value;
                rowSSTTDSType2.Visible = value;
            }
        }
        public void SetFromPayrollPeriod(PayrollPeriod startPayroll)
        {
            ddlPayrollFromMonth.SelectedValue = startPayroll.Month.ToString();
            ddlPayrollFromYear.SelectedValue = startPayroll.Year.ToString();
        }

        public void Initialise()
        {

            if (this.IncludeRetiredEmployeeInSearch)
            {
                AutoCompleteExtenderOrganization.ServiceMethod = "GetEmployeeNamesWithRetiredAlso";
            }

            bool isDateReverse = CommonManager.CompanySetting.IsHRInOppositeDate;

            calFromDate.IsEnglishCalendar = isDateReverse ? !SessionManager.CurrentCompany.IsEnglishDate : SessionManager.CurrentCompany.IsEnglishDate;
            calToDate.IsEnglishCalendar = isDateReverse ? !SessionManager.CurrentCompany.IsEnglishDate : SessionManager.CurrentCompany.IsEnglishDate;

            if (DateFromControlIsEnglish)
            {
                calFromDate.IsEnglishCalendar = true;
                calToDate.IsEnglishCalendar = true;
            }

            calFromDate.SelectTodayDate();
            calToDate.SelectTodayDate();

            if (SessionManager.CurrentCompany.PFRFFunds.Count >= 1
              && SessionManager.CurrentCompany.PFRFFunds[0].HasOtherPFFund != null
              && SessionManager.CurrentCompany.PFRFFunds[0].HasOtherPFFund.Value)
            {

                if (this.CurrentPageUrl.ToLower().Contains("paycit"))
                    ddlPFType.Items[1].Text = "CIT";
                else
                    ddlPFType.Items[1].Text = SessionManager.CurrentCompany.PFRFAbbreviation;

                ddlPFType.Items[2].Text = SessionManager.CurrentCompany.PFRFFunds[0].OtherPFFundName;
                ddlPFType.SelectedIndex = 1;

            }

            CommonManager.SetFirstAndLastFinalSavedPayrollPeriod(ddlPayrollFromYear,ddlPayrollFromMonth);
            CommonManager.SetFirstAndLastFinalSavedPayrollPeriod(ddlPayrollToYear, ddlPayrollToMonth);

                payrollChanged_SelectedIndexChanged(null, null);

            ddlBranch.DataSource
                = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);
            ddlBranch.DataBind();

            // For d2 as only one branch so select it by default & hide it
             if (CommonManager.CompanySetting.HideBranchIfOnlyOne &&
                BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId).Count <= 1)
            {
                ddlBranch.SelectedIndex = 1;
                rowBranch1.Style["display"] = "none";
                rowBranch2.Style["display"] = "none";
            }

            ddlDepartment_SelectedIndexChanged(null, null);

            //ddlDepartment.DataSource
            //    = DepartmentManager.GetCommonDepartmentList(-1);
            //ddlDepartment.DataBind();

            ddlSubDepartment_SelectedIndexChanged(null, null);

            ddlLeaves.DataSource = LeaveAttendanceManager.GetAllLeaves(SessionManager.CurrentCompanyId);
            ddlLeaves.DataBind();


            ddlIncomes.DataSource = PayManager.GetIncomeListByCompany(SessionManager.CurrentCompanyId);
            ddlIncomes.DataBind();


            ddlCostCode.DataSource = CommonManager.GetAllCostCodes();
            ddlCostCode.DataBind();

            ddlProgram.DataSource = (new CommonManager()).GetAllProgrammes();
            ddlProgram.DataBind();

            if (ddlGratuityType.Items.Count <= 1)
            {
                ddlGratuityType.Items.Add("Covered");
                ddlGratuityType.Items.Add("Not Covered");
            }

            JobStatus statues = new JobStatus();
            List<KeyValue> list = statues.GetMembers();
            //list.RemoveAt(0);
            ddlStatus.DataSource = list;
            ddlStatus.DataValueField = "Key";
            ddlStatus.DataTextField = "Value";

            ddlStatus.DataBind();

            List<Bank> bankList = PayManager.GetBankList();
            ddlBank.DataSource = bankList;
            ddlBank.DataBind();

            if (bankList.Count <= 0)
            {
                spanBank.Visible = false;
                ddlBank.Visible = false;
            }

            List<FinancialDate> yearlist = new CommonManager().GetAllFinancialDates(); ;
            foreach (FinancialDate item in yearlist)
                item.SetName(IsEnglish);
            ddlYear.DataSource = yearlist;
            ddlYear.DataBind();
            ddlYear.SelectedIndex = yearlist.Count;


            // for phl we will be loading all project
            if (CommonManager.CalculationConstant.CompanyIsHPL != null && CommonManager.CalculationConstant.CompanyIsHPL.Value)
            {
                int total=0;
                List<Project> proList = ProjectManager.GetProjects();
                proList.RemoveAt(0);
                ddlProject.DataSource = proList;
                ddlProject.DataBind();
            }

            List<PayrollPeriodBO> periodList = CommonManager.GetAllYear(SessionManager.CurrentCompanyId);

            ddlPayrollPeriod.Items.Clear();
            ddlPayrollPeriod.Items.AddRange(CommonManager.AppendMiddleTaxPaidList(periodList, true,true).ToArray());

            CommonManager.AppendMiddleTaxPaidList(periodList, true, true);


            List<UnitList> units = CommonManager.GetAllUnitList();
            if (units.Count >= 1)
            {

                ddlUnit.DataSource = units;
                ddlUnit.DataBind();

            }



            ddlDepartment_SelectedIndexChanged(null, null);


            //ddlPayrollPeriod.DataSource = periodList;
            //ddlPayrollPeriod.DataBind();

            if (periodList.Count > 0)
            {
                ddlPayrollPeriod.SelectedIndex = ddlPayrollPeriod.Items.Count-1;
            }

            // remove total,adjusted case
            if (this.CurrentPageUrl.ToLower().Contains("payslip"))
            {
                ddlPaySummaryType.Items.RemoveAt(1);
                ddlPaySummaryType.Items.RemoveAt(2);
            }


            // add faimily type
            List<FixedValueFamilyRelation> relations = CommonManager.GetRelationList().Where(x => x.Name.Trim().ToLower() != "father" 
                && x.Name.Trim().ToLower() != "mother")
                .ToList();

            relations.Add(new FixedValueFamilyRelation { Name="Father" });
            relations.Add(new FixedValueFamilyRelation { Name = "Mother" });

            foreach (var item in relations.OrderBy(x=>x.Name))
            {
                multiFamiltyType.AddItems(item.Name,item.Name);
            }
        }

        protected void chkIsEnglishDate_Checked(object sender, EventArgs e)
        {

            CustomDate date1 = calFromDate.SelectedDate;
            CustomDate date2 = calToDate.SelectedDate;

            if (date1.IsEnglishDate != null && date1.IsEnglishDate.Value)
            {
                date1 = CustomDate.ConvertEngToNep(date1);
                date2 = CustomDate.ConvertEngToNep(date2);
            }
            else
            {
                date1 = CustomDate.ConvertNepToEng(date1);
                date2 = CustomDate.ConvertNepToEng(date2);
            }

            calFromDate.IsEnglishCalendar = chkIsEnglishDate.Checked;
            calToDate.IsEnglishCalendar = chkIsEnglishDate.Checked;

            calFromDate.SelectTodayDate();
            calToDate.SelectTodayDate();

            //calFromDate.SetSelectedDate(date1.ToString(), !date1.IsEnglishDate.Value);
            //calToDate.SetSelectedDate(date2.ToString(), !date1.IsEnglishDate.Value);
        }

        public void BindLeaves()
        {

            ddlLeaves.Items.Insert(1, new ListItem { Value="0",Text="All Leave Sum" });
          
           
        }

        public DateTime StartDate
        {
            get
            {
                if (ddlPayrollFromYear.SelectedValue == "")
                    return BLL.BaseBiz. GetCurrentDateAndTime();

                return new DateTime(
                    int.Parse(ddlPayrollFromYear.SelectedValue),
                    int.Parse(ddlPayrollFromMonth.SelectedValue),
                    1);
            }
        }

        

        public DateTime EndDate
        {
            get
            {
                if (ddlPayrollToYear.SelectedValue == "")
                    return BLL.BaseBiz.GetCurrentDateAndTime();

                return new DateTime(
                    int.Parse(ddlPayrollToYear.SelectedValue),
                    int.Parse(ddlPayrollToMonth.SelectedValue),
                    1);
            }
        }

        public DateTime StartDateEng
        {
            get
            {
                if (SessionManager.CurrentCompany.IsEnglishDate)
                {
                    //sometime selected value is coming empty so prevent error
                    if (ddlPayrollFromMonth.SelectedValue == "")
                        return BLL.BaseBiz.GetCurrentDateAndTime();


                    return new DateTime(
                        int.Parse(ddlPayrollFromYear.SelectedValue),
                        int.Parse(ddlPayrollFromMonth.SelectedValue),
                        1);
                }
                else
                {
                    //sometime selected value is coming empty so prevent error
                    if (ddlPayrollFromMonth.SelectedValue == "")
                        return BLL.BaseBiz.GetCurrentDateAndTime();

                    CustomDate date = new CustomDate(1, int.Parse(ddlPayrollFromMonth.SelectedValue),
                        int.Parse(ddlPayrollFromYear.SelectedValue), false);
                    date = CustomDate.ConvertNepToEng(date);
                    return new DateTime(date.Year, date.Month, date.Day);

                }
            }
        }

        public DateTime EndDateEng
        {
            get
            {
                if (SessionManager.CurrentCompany.IsEnglishDate)
                {

                    //sometime selected value is coming empty so prevent error
                    if (ddlPayrollToMonth.SelectedValue == "")
                        return BLL.BaseBiz.GetCurrentDateAndTime();

                    return new DateTime(
                        int.Parse(ddlPayrollToYear.SelectedValue),
                        int.Parse(ddlPayrollToMonth.SelectedValue),
                        1);

                }
                else
                {
                    //sometime selected value is coming empty so prevent error
                    if (ddlPayrollToMonth.SelectedValue == "")
                        return BLL.BaseBiz.GetCurrentDateAndTime();

                    CustomDate date = new CustomDate(1, int.Parse(ddlPayrollToMonth.SelectedValue),
                        int.Parse(ddlPayrollToYear.SelectedValue), false);
                    date = CustomDate.ConvertNepToEng(date);
                    return new DateTime(date.Year, date.Month, date.Day);

                }
            }

           
        }




        public int DepartmentId
        {
            get
            {
                return int.Parse(ddlDepartment.SelectedValue);
            }
        }
        public int YearId
        {
            get
            {
                return int.Parse(ddlYear.SelectedValue);
            }
        }
        public int BankID
        {
            get
            {
                return int.Parse(ddlBank.SelectedValue);
            }
        }
        public string FamiltyTypeValue
        {
            get
            {
                return multiFamiltyType.Text;
            }
        }
        public bool? PFTypeValue
        {
            get
            {
                string value = ddlPFType.SelectedValue;
                if (value == null || value=="" || value=="all")
                    return null;
                return bool.Parse(ddlPFType.SelectedValue);
            }
        }
        public bool Bank
        {
            set
            {
                if (CommonManager.HasBankList() == false && value == true)
                    value = false;

                rowBank1.Visible = value;
                rowBank2.Visible = value;
            }
        }

        public int LoanTypeValue
        {
            set { }
            get
            {
                return int.Parse(ddlLoanType.SelectedValue);
            }
        }
        public bool LoanType
        {
            set
            {
                rowLoanType1.Visible = value;
                rowLoanType2.Visible = value;
            }
        }

        public int SubDepartmentId
        {
            get
            {
                return int.Parse(ddlSubDepartment.SelectedValue);
            }
        }

        public int ProjectId
        {
            get
            {
                return int.Parse(ddlProject.SelectedValue);
            }
        }

        public int UnitId
        {
            get
            {
                return int.Parse(ddlUnit.SelectedValue);
            }
        }

        public string ProjectName
        {
            get
            {
                return (ddlProject.SelectedItem.Text);
            }
        }

        public int[] PayrollPeriodValue
        {
            get
            {


                if (string.IsNullOrEmpty(ddlPayrollPeriod.SelectedValue))
                    return null;
                int period = 0, type = 0, addOnId = 0;

                SplitPeriod(ddlPayrollPeriod.SelectedValue, ref period, ref type,ref addOnId);
                return new int[] {period,type,addOnId };



            }
        }

        void SplitPeriod(string value, ref int payrollPeriodId, ref int periodType, ref int addOnId)
        {
            string[] values = value.Split(new char[] { ':' });
            payrollPeriodId = int.Parse(values[0]);
            periodType = int.Parse(values[1]);

            if (values.Length >= 3 && int.Parse(values[1]) == (int)PayrollPeriodType.NewAddOn)
            {
                // assign add on id
                //payrollPeriodId = int.Parse(values[2]);
                periodType = 4;
                addOnId = int.Parse(values[2]);
            }
        }

        public int BranchId
        {
            get
            {
                return int.Parse(ddlBranch.SelectedValue);
            }
        }

        public bool GroupByIncomeOrName
        {
            get
            {
                return bool.Parse(ddlGroupBy.SelectedValue);
            }
        }

        public string BranchName
        {
            get
            {
                return ddlBranch.SelectedItem.Text;
            }
        }
        public string DepartmentName
        {
            get
            {
                return ddlDepartment.SelectedItem.Text;
            }
        }
        public bool Department
        {
            set
            {
                rowDep1.Visible = value;
                rowDep2.Visible = value;
            }
        }

        public bool Unit
        {
            set
            {
                rowUnit1.Visible = value;
                rowUnit2.Visible = value;
            }
        }
        public bool Year
        {
            set
            {
                rowYear1.Visible = value;
                rowYear2.Visible = value;
            }
        }
        public bool FamiltyType
        {
            set
            {
                rowFamiltyType1.Visible = value;
                rowFamiltyType2.Visible = value;
            }
        }
        public bool PFType
        {
            set
            {
                rowPFType1.Visible = value;
                rowPFType2.Visible = value;
            }
        }
        public bool PayrollPeriod
        {
            set
            {
                rowPayrollPeroid1.Visible = value;
                rowPayrollPeroid2.Visible = value;
            }
        }
        public bool SubDepartment
        {
            set
            {
                rowSubDep1.Visible = value;
                rowSubDep2.Visible = value;
            }
        }

        public bool RetiredOnly
        {
            set
            {
                rowRegularOrRetired1.Visible = value;
                rowRegularOrRetired2.Visible = value;
            }
        }
        public int RetiredOnlySelectedIndex
        {
            set
            {
                ddlRetiredOnly.SelectedIndex = value;
            }
            get
            {
                return ddlRetiredOnly.SelectedIndex;
            }
        }
        public CheckBox EnglishDateControl
        {
            get
            {
                return chkIsEnglishDate;
            }
        }
        public bool IsEnglishDate
        {
            get
            {
                return chkIsEnglishDate.Checked;
            }
        }
        public bool EnglishDate
        {
            set
            {
                rowIsEnglish1.Visible = value;
                rowIsEnglish2.Visible = value;


            }
        }
        public bool FilterBlockMode
        {
            set
            {
                rowFilter1.Visible = value;
                rowFilter2.Visible = value;
            }
        }

        public string FromDateLabel
        {
            set
            {
                this.lblFromDate.InnerHtml = value;
            }
        }
        public bool DateFromControl
        {
         
            set

            {
                rowFromDate1.Visible = value;
                rowFromDate2.Visible = value;
            }
        }


        private bool isDateReverted = false;
        public bool DateFromControlIsEnglish
        {
            get
            {
                return isDateReverted;
            }
            set
            {
                isDateReverted = value;
            }
        }

        public bool DateToControl
        {

            set
            {
                rowToDate1.Visible = value;
                rowToDate2.Visible = value;
            }
        }

        public CalendarCtl ToDate
        {
            get
            {
                return this.calToDate;
            }
        }

        public CustomDate DateFrom
        {
         
            get
            {
                return calFromDate.SelectedDate;
            }
        }


        public bool PaySummaryType
        {
            set
            {
                rowPaySummaryType1.Visible = value;
                rowPaySummaryType2.Visible = value;
            }
        }

        public string PaySummaryTypeText
        {
            get
            {
                return ddlPaySummaryType.SelectedItem.Text;
            }
        }

        public int AddOnId
        {
            get
            {
                return int.Parse(ddlAddOnName.SelectedValue);
            }
        }
        public bool AddOn
        {
            set
            {
                rowAddOn1.Visible = value;
                rowAddOn2.Visible = value;
            }
        }
        public void LoadAddOn(int payrollPeriodId)
        {
            ListItem item = ddlAddOnName.SelectedItem;

            ListItem first = ddlAddOnName.Items[0];

            ddlAddOnName.DataSource = PayManager.GetAddOnForPayrollPeriod(payrollPeriodId);
            ddlAddOnName.DataBind();

            ddlAddOnName.Items.Insert(0, first);


            if (item != null && item.Selected)
            {
                foreach (ListItem selItem in ddlAddOnName.Items)
                {
                    if (selItem.Value == item.Value)
                        selItem.Selected = true;
                }
            }
        }

        public bool LeaveRequestType
        {
            set
            {
                rowLeaveRequestType1.Visible = value;
                rowLeaveRequestType2.Visible = value;
            }
        }

        public string LeaveRequestTypeValue
        {
            get
            {
                return this.ddlLeaveRequestType.SelectedValue;
            }
        }

        public int PaySummaryTypeValue
        {
            get
            {
                return int.Parse(ddlPaySummaryType.SelectedValue);
            }

        }

        public bool SSTTDSTypeValue
        {
            get
            {
                return bool.Parse(ddlSSTTDSType.SelectedValue);
            }
        }

        public CustomDate DateTo
        {

            get
            {
                return calToDate.SelectedDate;
            }
        }

        public bool Employee
        {
            set
            {
                rowEmp1.Visible = value;
                rowEmp2.Visible = value;
            }
        }
        //  show with " - EIN" so can be used where EIN display is reqquired
        public bool RetiredEmployee
        {
            set
            {
                rowRetiredEmp1.Visible = value;
                rowRetiredEmp2.Visible = value;
            }
        }
        public DropDownList DDLCostCode
        {
            get
            {
                return this.ddlCostCode;
            }
        }

        public bool GroupBy
        {
            set
            {
                this.rowGroupByHeadOrName1.Visible = value;
                this.rowGroupByHeadOrName2.Visible = value;
            }
        }

        public bool CostCode
        {
            set
            {
                rowCostCode1.Visible = value;
                rowCostCode2.Visible = value;

            }
        }

        public bool? RetiredOnlyValue
        {
            get
            {
                if (ddlRetiredOnly.SelectedIndex == 0)
                    return null;
                return bool.Parse(ddlRetiredOnly.SelectedValue);
            }
        }

        public bool Program
        {
            set
            {
                rowProgram1.Visible = value;
                rowProgram2.Visible = value;
            }
        }

        public bool Leave
        {
            set
            {
                rowLeave1.Visible = value;
                rowLeave2.Visible = value;
            }
        }
        public bool GratuityType
        {
            set
            {
                rowGratuityType1.Visible = value;
                rowGratuityType2.Visible = value;
            }
        }

        public bool Income
        {
            set
            {
                rowIncome1.Visible = value;
                rowIncome2.Visible = value;
            }
        }
        public bool Status
        {
            set
            {
                rowStatus1.Visible = value;
                rowStatus2.Visible = value;
            }
        }
        public int LeaveTypeId
        {
            get
            {
                return int.Parse(ddlLeaves.SelectedValue);
            }
        }

        public int CostCodeId
        {
            get
            {
                return int.Parse(ddlCostCode.SelectedValue);
            }
        }
        public int StatusId
        {
            get
            {
                if (ddlStatus.SelectedItem == null)
                    return -1;
                return int.Parse(ddlStatus.SelectedValue);
            }
        }


        public int ProgramId
        {
            get
            {
                return int.Parse(ddlProgram.SelectedValue);
            }
        }

        public string GratuityTypeValue
        {
            get
            {
                return ddlGratuityType.SelectedValue.ToString();
            }
        }

        public int IncomeId
        {
            get
            {
                return int.Parse(ddlIncomes.SelectedValue);
            }
        }

        public bool Branch
        {
            set
            {
                rowBranch1.Visible = value;
                rowBranch2.Visible = value;
            }
        }

        public bool Project
        {
            set
            {
                rowProject1.Visible = value;
                rowProject2.Visible = value;

                // Change payroll AutoPostback as the projects depends upon Payroll Period
                if(value)
                {
                    ddlPayrollFromMonth.AutoPostBack = true;
                    ddlPayrollFromYear.AutoPostBack = true;
                   
                }
                else
                {
                    ddlPayrollFromMonth.AutoPostBack = false;
                    ddlPayrollFromYear.AutoPostBack = false;
                }
            }
        }

        public bool MultiSelectionDropDown
        {
            set
            {
                rowMultiSelectionDep1.Visible = value;
                rowMultiSelectionDep2.Visible = value;
            }
        }
        public bool MultiSelectionBranch
        {
            set
            {
                rowMultiSelectionBranch1.Visible = value;
                rowMultiSelectionBranch2.Visible = value;
            }
        }
        public string MultiSelectionDropDownValues
        {
            get
            {
                ASPxListBox listBox = (ASPxListBox)this.ddlMultiSelectDepartment.FindControl("listBox");
                string str="";
                if (listBox != null && listBox.SelectedValues != null)
                {
                    foreach (object value in listBox.SelectedValues)
                    {
                        if (value != null)
                        {
                            if (str == "")
                                str = value.ToString();
                            else
                                str += "," + value.ToString();
                        }
                    }
                }
                return str;
            }
        }

        public string MultiSelectionBranchValues
        {
            get
            {
                ASPxListBox listBox = (ASPxListBox)this.ddlMultiSelectBranch.FindControl("listBoxBranch");
                string str = "";
                if (listBox != null && listBox.SelectedValues != null)
                {
                    foreach (object value in listBox.SelectedValues)
                    {
                        if (value != null)
                        {
                            if (str == "")
                                str = value.ToString();
                            else
                                str += "," + value.ToString();
                        }
                    }
                }
                return str;
            }
        }
        public ASPxListBox FiterListBox
        {
            get
            {
                ASPxListBox listBox = (ASPxListBox)this.excludeFilterBlocks.FindControl("filterListBox");
                //string str = "";
                //if (listBox != null && listBox.SelectedValues != null)
                //{
                //    foreach (object value in listBox.SelectedValues)
                //    {
                //        if (value != null)
                //        {
                //            if (str == "")
                //                str = value.ToString();
                //            else
                //                str += "," + value.ToString();
                //        }
                //    }
                //}
                return listBox;
            }
        }


        public bool PayrollFrom
        {
            set
            {
                rowPayrollFrom1.Visible = value;
                rowPayrollFrom2.Visible = value;
            }
        }

        public bool PayrollTo
        {
            set
            {
                rowPayrollTo1.Visible = value;
                rowPayrollTo2.Visible = value;
            }
        }

        public Literal DateToControlText
        {
            get
            {
                return this.litToDate;
            }
        }
        public string EmployeeName
        {
            get
            {
                if (txtEmpSearch.Text.Trim() == TextBoxWatermarkExtender1.WatermarkText)
                    return "";
                return txtEmpSearch.Text.Trim();
            }
        }

        public void SetSelectedEmployee(int empid, string name)
        {
            txtEmpSearch.Text = name;
            hiddenEmployeeID.Value = empid.ToString();
        }
        public void SetSelectedRetEmployee(int empid, string name)
        {
            txtRetiredEmployee.Text = name;
            hiddenEmployeeID.Value = empid.ToString();
        }
        public int EmployeeId
        {
            get
            {
                if (txtEmpSearch.Text.Trim() == "" || txtEmpSearch.Text.Trim() == "Search Text")
                    return 0;
                if (!string.IsNullOrEmpty(hiddenEmployeeID.Value))
                    return int.Parse(hiddenEmployeeID.Value);
                return 0;
            }
        }
        public int RetiredEmployeeId
        {
            get
            {
                if (txtRetiredEmployee.Text.Trim() == "" || txtRetiredEmployee.Text.Trim() == "Search Text")
                    return 0;
                if (!string.IsNullOrEmpty(hiddenEmployeeID.Value))
                    return int.Parse(hiddenEmployeeID.Value);
                return 0;
            }
        }
        protected void btnLoad_Click(object sender, EventArgs e)
        {
            if (ReloadReport != null)
            {
                (this.Parent as ReportContainer).clearCache = true;
                
                //ReloadReport();

                // LoadSubDepForMultipleDepCase
                LoadSubDepForMultipleDepCase();
            }

        }

        void LoadSubDepForMultipleDepCase()
        {
            ASPxListBox listBox = (ASPxListBox)this.ddlMultiSelectDepartment.FindControl("listBox");
            List<int> depList = new List<int>();
            if (listBox != null && listBox.SelectedValues != null)
            {
                foreach (object value in listBox.SelectedValues)
                {
                    if (value != null)
                    {
                        depList.Add(int.Parse( value.ToString()));
                    }
                }
            }

            string selVal = ddlSubDepartment.SelectedValue;

            ListItem item = ddlSubDepartment.Items[0];

            ddlSubDepartment.DataSource = DepartmentManager.GetAllSubDepartmentsByDepartment(depList);
            ddlSubDepartment.DataBind();

            ddlSubDepartment.Items.Insert(0, item);

            foreach (ListItem item1 in ddlSubDepartment.Items)
            {
                if (item1.Value == selVal)
                    item1.Selected = true;
            }
        
        }

        protected void payrollChanged_SelectedIndexChanged(object sender, EventArgs e)
        {
            // for phl we will be loading all project
            if (CommonManager.CalculationConstant.CompanyIsHPL != null && CommonManager.CalculationConstant.CompanyIsHPL.Value)
            {
                return;
            }
         
            ListItem firstItem = ddlProject.Items[0];
            ddlProject.Items.Clear();

            if (ddlPayrollFromMonth.SelectedValue != "")
            {
                PayrollPeriod payrollPeriod = CommonManager.GetPayrollPeriod(int.Parse(ddlPayrollFromMonth.SelectedValue),
                                                                           int.Parse(ddlPayrollFromYear.SelectedValue));
                if (payrollPeriod == null)
                {
                    ddlProject.DataSource = null;
                    ddlProject.DataBind();
                }
                else
                {
                    ddlProject.DataSource = ProjectManager.GetProjectListForReport(payrollPeriod.PayrollPeriodId);
                    ddlProject.DataBind();
                }
            }
            ddlProject.Items.Insert(0,firstItem);
        }

        protected void ddlSubDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem firstItem = ddlSubDepartment.Items[0];

            ddlSubDepartment.Items.Clear();
          
            ddlSubDepartment.DataSource = DepartmentManager.GetAllSubDepartmentsByDepartment(int.Parse(ddlDepartment.SelectedValue));
            ddlSubDepartment.DataBind();




            ddlSubDepartment.Items.Insert(0, firstItem);
        }

        protected void ReloadMultipleCheckListDepartment()
        {
            ASPxListBox listBox = (ASPxListBox)this.ddlMultiSelectDepartment.FindControl("listBox");
            listBox.ValueType = typeof(int);

            List<Department> list = DepartmentManager.GetCommonDepartmentList(int.Parse(ddlBranch.SelectedValue));

            listBox.DataSource = list;
            listBox.DataBind();

            listBox.Items.Insert(0, new ListEditItem { Text = "(Select All)", Value = "" });


        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem firstItem = ddlDepartment.Items[0];
            ASPxListBox listBox = (ASPxListBox)this.ddlMultiSelectDepartment.FindControl("listBox");
            listBox.ValueType = typeof(int);
          
            ddlDepartment.Items.Clear();
            int depId = 0;
            

            {
                List<Department> list = DepartmentManager.GetCommonDepartmentList(int.Parse(ddlBranch.SelectedValue));
                ddlDepartment.DataSource = list;
                ddlDepartment.DataBind();
                listBox.DataSource = ddlDepartment.DataSource;
                listBox.DataBind();

                listBox.Items.Insert(0, new ListEditItem { Text = "(Select All)", Value="" });
              
            }


            

            ddlDepartment.Items.Insert(0,firstItem);
        }

        protected void LoadMultiBranch(object sender, EventArgs e)
        {
            //ListItem firstItem = ddlBranch.Items[0];
            ASPxListBox listBox = (ASPxListBox)this.ddlMultiSelectBranch.FindControl("listBoxBranch");
            listBox.ValueType = typeof(int);

            //ddlDepartment.Items.Clear();
            int depId = 0;

            List<DAL.Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            listBox.DataSource = list;
            listBox.DataBind();

            listBox.Items.Insert(0, new ListEditItem { Text = "(Select All)", Value = "" });


        }
    }
}