﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportFilterBranchDep.ascx.cs"
    Inherits="Web.Controls.ReportFilterBranchDep" %>
<%@ Register Src="~/Controls/CalendarCtl.ascx" TagName="Calendar" TagPrefix="UC1" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Src="~/Controls/WarningMsgCtl.ascx" TagName="WarningCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/InfoMsgCtl.ascx" TagName="MsgCtl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/MultiCheckCombo.ascx" TagName="MultiCheckCombo" TagPrefix="uc3" %>
<script type="text/javascript">
    var selEmpId = null;
    function ACE_item_selected(source, eventArgs) {
        var value = eventArgs.get_value();
        selEmpId = value;
        document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = selEmpId;
    }
    function ACE_item_selected_Retired(source, eventArgs) {
        var value = eventArgs.get_value();
        selEmpId = value;
        document.getElementById('<%= hiddenEmployeeID.ClientID %>').value = selEmpId;
    }


    var textSeparator = ";";
    function OnListBoxSelectionChanged(listBox, args) {
        if (args.index == 0)
            args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        UpdateSelectAllItemState();
        UpdateText();
    }
    function UpdateSelectAllItemState() {
        IsAllSelected() ? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
    }
    function IsAllSelected() {
        var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
        return checkListBox.GetSelectedItems().length == selectedDataItemCount;
    }
    function UpdateText() {
        var selectedItems = checkListBox.GetSelectedItems();
        checkComboBox.SetText(GetSelectedItemsText(selectedItems));
    }

    function SynchronizeListBoxValues(dropDown, args) {
        checkListBox.UnselectAll();
        var texts = dropDown.GetText().split(textSeparator);
        var values = GetValuesByTexts(texts);
        checkListBox.SelectValues(values);
        UpdateSelectAllItemState();
        UpdateText(); // for remove non-existing texts
    }



    function GetSelectedItemsText(items) {
        var texts = [];
        for (var i = 0; i < items.length; i++)
            if (items[i].index != 0)
                texts.push(items[i].text);
        return texts.join(textSeparator);
    }
    function GetValuesByTexts(texts) {
        var actualValues = [];
        var item;
        for (var i = 0; i < texts.length; i++) {
            item = checkListBox.FindItemByText(texts[i]);
            if (item != null)
                actualValues.push(item.value);
        }
        return actualValues;
    }

    // for branch
    function OnListBoxSelectionChangedBranch(listBox, args) {
        if (args.index == 0)
            args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        UpdateSelectAllItemStateBranch();
        UpdateTextBranch();
    }
    function UpdateSelectAllItemStateBranch() {
        IsAllSelectedBranch() ? checkListBoxBranch.SelectIndices([0]) : checkListBoxBranch.UnselectIndices([0]);
    }
    function IsAllSelectedBranch() {
        var selectedDataItemCount = checkListBoxBranch.GetItemCount() - (checkListBoxBranch.GetItem(0).selected ? 0 : 1);
        return checkListBoxBranch.GetSelectedItems().length == selectedDataItemCount;
    }
    function UpdateTextBranch() {
        var selectedItems = checkListBoxBranch.GetSelectedItems();
        checkComboBoxBranch.SetText(GetSelectedItemsTextBranch(selectedItems));
    }

    function SynchronizeListBoxValuesBranch(dropDown, args) {
        checkListBoxBranch.UnselectAll();
        var texts = dropDown.GetText().split(textSeparator);
        var values = GetValuesByTextsBranch(texts);
        checkListBoxBranch.SelectValues(values);
        UpdateSelectAllItemStateBranch();
        UpdateTextBranch(); // for remove non-existing texts
    }



    function GetSelectedItemsTextBranch(items) {
        var texts = [];
        for (var i = 0; i < items.length; i++)
            if (items[i].index != 0)
                texts.push(items[i].text);
        return texts.join(textSeparator);
    }
    function GetValuesByTextsBranch(texts) {
        var actualValues = [];
        var item;
        for (var i = 0; i < texts.length; i++) {
            item = checkListBoxBranch.FindItemByText(texts[i]);
            if (item != null)
                actualValues.push(item.value);
        }
        return actualValues;
    }

    /*for other dropdown*/
    function GetFilterSelectedItemsText(items) {
        var texts = [];
        for (var i = 0; i < items.length; i++)
            if (items[i].index != 0)
                texts.push(items[i].text);
        return texts.join(textSeparator);
    }
    function GetFilterValuesByTexts(texts) {
        var actualValues = [];
        var item;
        for (var i = 0; i < texts.length; i++) {
            item = filterListBox.FindItemByText(texts[i]);
            if (item != null)
                actualValues.push(item.value);
        }
        return actualValues;
    }
    function FilterUpdateText() {
        var selectedItems = filterListBox.GetSelectedItems();
        filterCheckComboBox.SetText(GetFilterSelectedItemsText(selectedItems));
    }
    function SynchronizeFilterListBoxValues(dropDown, args) {
        filterListBox.UnselectAll();
        var texts = dropDown.GetText().split(textSeparator);
        var values = GetFilterValuesByTexts(texts);
        filterListBox.SelectValues(values);
        FilterUpdateSelectAllItemState();
        FilterUpdateText(); // for remove non-existing texts
    }
    function FilterUpdateSelectAllItemState() {
        FilterIsAllSelected() ? filterListBox.SelectIndices([0]) : filterListBox.UnselectIndices([0]);
    }
    function FilterIsAllSelected() {
        var selectedDataItemCount = filterListBox.GetItemCount() - (filterListBox.GetItem(0).selected ? 0 : 1);
        return filterListBox.GetSelectedItems().length == selectedDataItemCount;
    }
    function OnFilterListBoxSelectionChanged(listBox, args) {
        if (args.index == 0)
            args.isSelected ? filterListBox.SelectAll() : filterListBox.UnselectAll();
        FilterUpdateSelectAllItemState();
        FilterUpdateText();
    }
    var ddHelper = {
        previousIdx: null,
        store: function (e, dd) {
            this.previousIdx = dd.selectedIndex;
        },
        confirm: function (e, dd) {
            if (!window.confirm('Change the type?')) {
                window.setTimeout(function () { // Think you need setTimeout for some browsers
                    dd.selectedIndex = ddHelper.previousIdx;
                }, 0);
            }
        }
    };

    function popupReportEditCall(reportType) {
        var ret = reportEdit('id=' + reportType);


        return false;
    }

</script>
<div class="attribute" style="padding-top: 0px">
    <uc2:MsgCtl ID="divMsgCtl" EnableViewState="false" Hide="true" runat="server" />
    <uc2:WarningCtl ID="divWarningMsg" EnableViewState="false" Hide="true" runat="server" />
    <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoad" />
            <asp:PostBackTrigger ControlID="btnCompanySpecificExport" />
            <asp:PostBackTrigger ControlID="btnReGenerateGratuity" />
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField ID="hiddenEmployeeID" runat="server" />
            <asp:Panel runat="server" DefaultButton="btnLoad">
                <table cellpadding="3" cellspacing="0" class="fieldTable">
                    <tr>
                        <td runat="server" id="rowIsEnglish1" visible="false">
                        </td>
                        <td runat="server" id="rowFromDate1" visible="false">
                            <strong runat="server" id="lblFromDate">From Date</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowToDate1" visible="false">
                            <strong>
                                <asp:Literal runat="server" ID="litToDate">To Date</asp:Literal></strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowPayrollFrom1">
                            <strong>From</strong>
                        </td>
                        <td class="filterHeader" runat="server" visible="false" id="rowPayrollPeroid1">
                            <strong>Period/Month</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowPayrollTo1">
                            <strong>To </strong>
                        </td>
                           <td runat="server" id="rowYear1" visible="false">
                            <strong>Year</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowEmp1">
                            <strong>Employee Name</strong>
                        </td>
                        <td class="filterHeader" visible="false" runat="server" id="rowRetiredEmp1">
                            <strong>Employee Name</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowLeave1" visible="false">
                            <strong>Leave type</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowGratuityType1" visible="false">
                            <strong>Gratuity Type</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowIncome1" visible="false">
                            <strong>Income</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowBranch1">
                            <strong>Branch</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowMultiSelectionBranch1" visible="false">
                            <strong>Branch</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowDep1">
                            <strong>Department</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowMultiSelectionDep1" visible="false">
                            <strong>Department</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowSubDep1" visible="false">
                            <strong>Sub-Department</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowProject1" visible="false">
                            <strong>Project</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowCostCode1" visible="false">
                            <strong>Cost Code</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowProgram1" visible="false">
                            <strong>Program</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowRegularOrRetired1" visible="false">
                            <strong>Regular/Retired</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowGroupByHeadOrName1" visible="false">
                            <strong>Group By</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowPaySummaryType1" visible="false">
                            <strong>Type</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowStatus1" visible="false">
                            <strong>Status</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowFilter1" visible="false">
                            <strong>Exclude Block</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowLeaveRequestType1" visible="false">
                            <strong>Type</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowLoanType2" visible="false">
                            <strong>Loan Type</strong>
                        </td>
                        <td class="filterHeader" runat="server" id="rowBank1" visible="false">
                            <strong runat="server" id="spanBank">Bank</strong>
                        </td>
                        <td runat="server" id="rowAddOn1" visible="false">
                            <strong>Add On</strong>
                        </td>
                        <td runat="server" id="rowSSTTDSType1" visible="false">
                            <strong>SST/TDS</strong>
                        </td>
                        <td runat="server" id="rowFamiltyType1" visible="false">
                            <strong>Familty Type</strong>
                        </td>
                         <td runat="server" id="rowUnit1" visible="false">
                            <strong>Unit</strong>
                        </td>
                        <td runat="server" id="rowPFType1" visible="false">
                            <strong>Type</strong>
                        </td>
                     
                         <td runat="server" id="rowDynamic1" visible="false">
                            <strong></strong>
                        </td>
                        <td rowspan="2" valign="bottom">
                            <asp:Button ID="btnLoad" Style='float: left' CssClass="load" runat="server" Text="Load"
                                OnClick="btnLoad_Click" />
                            <asp:Button ID="btnReGenerateGratuity" Visible="false" OnClientClick="return confirm('Confirm Re-Generate the gratuity report?');"
                                Style='float: left; margin-left: 10px;' CssClass="load" runat="server" Text="Re-Generate Report"
                                OnClick="btnReGenerateGratuity_Click" />
                            <asp:LinkButton ID="btnCompanySpecificExport" Visible="false" runat="server" Text="Export to Excel"
                                OnClick="btnCompanySepecificExport_Click" CssClass=" excel marginRight" Style="float: left;" />
                        </td>
                        <td rowspan="2" valign="bottom" style='padding-left:20px;'>
                            <asp:Button ID="btnEditHeaderFooter" Visible="false" OnClientClick='return false;' Width="140" Style='float: right' CssClass="save" runat="server"
                                Text="Edit Header/Footer"  />
                        </td>
                    </tr>
                    <tr>
                        <td runat="server" id="rowIsEnglish2" visible="false">
                            <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chkIsEnglishDate_Checked"
                                ID="chkIsEnglishDate" Text="English Date" />
                        </td>
                        <td runat="server" id="rowFromDate2" visible="false">
                            <UC1:Calendar runat="server" Id="calFromDate" />
                        </td>
                        <td runat="server" id="rowToDate2" visible="false">
                            <UC1:Calendar runat="server" Id="calToDate" />
                        </td>
                        <td runat="server" id="rowPayrollFrom2">
                            <asp:DropDownList ID="ddlPayrollFromMonth" OnSelectedIndexChanged="payrollChanged_SelectedIndexChanged"
                                runat="server">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlPayrollFromYear" OnSelectedIndexChanged="payrollChanged_SelectedIndexChanged"
                                runat="server">
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowPayrollPeroid2" visible="false">
                            <asp:DropDownList ID="ddlPayrollPeriod" DataTextField="Name" DataValueField="PayrollPeriodId"
                                runat="server">
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowPayrollTo2">
                            <asp:DropDownList ID="ddlPayrollToMonth" runat="server">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlPayrollToYear" runat="server">
                            </asp:DropDownList>
                        </td>
                         <td runat="server" id="rowYear2" visible="false">
                          <asp:DropDownList ID="ddlYear" AutoPostBack="true" runat="server" AppendDataBoundItems="true"
                        Width="120px" DataValueField="FinancialDateId"  DataTextField="Name">
                        <asp:ListItem Text="--Select--" Value="-1" />
                    </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowEmp2">
                            <asp:TextBox Width="180px" ID="txtEmpSearch" runat="server" />
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEmpSearch"
                                WatermarkText="Search Text" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                                runat="server" MinimumPrefixLength="2" ServiceMethod="GetEmployeeNamesWithRetiredAlso"
                                ServicePath="~/PayrollService.asmx" TargetControlID="txtEmpSearch" OnClientItemSelected="ACE_item_selected"
                                CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td runat="server" width="180px" visible="false" id="rowRetiredEmp2">
                            <asp:TextBox ID="txtRetiredEmployee" runat="server" />
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtRetiredEmployee"
                                WatermarkText="Search Text" WatermarkCssClass="searchBoxText" />
                            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtender1" runat="server"
                                MinimumPrefixLength="2" ServiceMethod="GetRetiredEmployeeNamesWithID" ServicePath="~/PayrollService.asmx"
                                TargetControlID="txtRetiredEmployee" OnClientItemSelected="ACE_item_selected_Retired"
                                CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                            </cc1:AutoCompleteExtender>
                        </td>
                        <td runat="server" id="rowLeave2" visible="false">
                            <asp:DropDownList ID="ddlLeaves" Width="120px" DataTextField="Title" DataValueField="LeaveTypeId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowGratuityType2" visible="false">
                            <asp:DropDownList ID="ddlGratuityType" Width="120px" DataTextField="Title" DataValueField="LeaveTypeId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowIncome2" visible="false">
                            <asp:DropDownList ID="ddlIncomes" Width="120px" DataTextField="Title" DataValueField="IncomeId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowBranch2">
                            <asp:DropDownList ID="ddlBranch" Width="120px" DataTextField="Name" DataValueField="BranchId"
                                AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowMultiSelectionBranch2" visible="false">
                            <dx:ASPxDropDownEdit ClientInstanceName="checkComboBoxBranch" ID="ddlMultiSelectBranch"
                                Width="200px" runat="server" EnableAnimation="False">
                                <DropDownWindowStyle BackColor="#EDEDED" />
                                <DropDownWindowTemplate>
                                    <dx:ASPxListBox Width="100%" Height="300" TextField="Name" ValueField="BranchID"
                                        ID="listBoxBranch" ClientInstanceName="checkListBoxBranch" SelectionMode="CheckColumn"
                                        runat="server">
                                        <Border BorderStyle="None" />
                                        <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                        <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChangedBranch" />
                                    </dx:ASPxListBox>
                                </DropDownWindowTemplate>
                                <ClientSideEvents TextChanged="SynchronizeListBoxValuesBranch" DropDown="SynchronizeListBoxValuesBranch" />
                            </dx:ASPxDropDownEdit>
                        </td>
                        <td runat="server" id="rowDep2">
                            <asp:DropDownList ID="ddlDepartment" Width="120px" DataTextField="Name" DataValueField="DepartmentId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowMultiSelectionDep2" visible="false">
                            <dx:ASPxDropDownEdit  ClientInstanceName="checkComboBox" ID="ddlMultiSelectDepartment"
                                Width="150px" runat="server" EnableAnimation="False">
                                <DropDownWindowStyle BackColor="#EDEDED" />
                                <DropDownWindowTemplate>
                                    <dx:ASPxListBox Width="100%" TextField="Name" ValueField="DepartmentId" ID="listBox"
                                        ClientInstanceName="checkListBox" SelectionMode="CheckColumn" runat="server">
                                        <Border BorderStyle="None" />
                                        <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                        <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                    </dx:ASPxListBox>
                                </DropDownWindowTemplate>
                                <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                            </dx:ASPxDropDownEdit>
                        </td>
                        <td runat="server" id="rowSubDep2" visible="false">
                            <asp:DropDownList ID="ddlSubDepartment" Width="120px" DataTextField="Name" DataValueField="SubDepartmentId"
                                runat="server">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowProject2" visible="false">
                            <asp:DropDownList ID="ddlProject" Width="120px" DataTextField="Name" DataValueField="ProjectId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowCostCode2" visible="false">
                            <asp:DropDownList ID="ddlCostCode" Width="120px" DataTextField="Name" DataValueField="CostCodeId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowProgram2" visible="false">
                            <asp:DropDownList ID="ddlProgram" Width="120px" DataTextField="Name" DataValueField="ProgrammeId"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowRegularOrRetired2" visible="false">
                            <asp:DropDownList ID="ddlRetiredOnly" Width="120px" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="All" Value="-1" />
                                <asp:ListItem Text="Regular only" Selected="True" Value="false" />
                                <asp:ListItem Text="Retired only" Value="true" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowGroupByHeadOrName2" visible="false">
                            <asp:DropDownList ID="ddlGroupBy" Width="120px" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="Income Head" Value="true" />
                                <asp:ListItem Text="Employee Name" Value="false" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowPaySummaryType2" visible="false">
                            <asp:DropDownList ID="ddlPaySummaryType" Width="100px" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="Total" Value="1" Selected="True" />
                                <asp:ListItem Text="Regular" Value="2" />
                                <asp:ListItem Text="Adjusted" Value="3" />
                                <asp:ListItem Text="Arrear" Value="4" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowStatus2" visible="false">
                            <asp:DropDownList ID="ddlStatus" Width="120px" DataTextField="Name" DataValueField="StatusId"
                                runat="server" AppendDataBoundItems="true">
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowFilter2" visible="false">
                            <dx:ASPxDropDownEdit ClientInstanceName="filterCheckComboBox" ID="excludeFilterBlocks"
                                Width="150px" runat="server" EnableAnimation="False">
                                <DropDownWindowStyle BackColor="#EDEDED" />
                                <DropDownWindowTemplate>
                                    <dx:ASPxListBox Width="100%" TextField="Name" ValueField="DepartmentId" ID="filterListBox"
                                        ClientInstanceName="filterListBox" SelectionMode="CheckColumn" runat="server">
                                        <Border BorderStyle="None" />
                                        <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                        <Items>
                                            <dx:ListEditItem Text="(Select all)" />
                                            <dx:ListEditItem Text="Address" Value="Address" />
                                            <dx:ListEditItem Text="HR" Value="HR" />
                                            <dx:ListEditItem Text="Qualification" Value="Qualification" />
                                            <dx:ListEditItem Text="Skillset" Value="Skillset" />
                                            <dx:ListEditItem Text="Pay" Value="Pay" />
                                        </Items>
                                        <ClientSideEvents SelectedIndexChanged="OnFilterListBoxSelectionChanged" />
                                    </dx:ASPxListBox>
                                </DropDownWindowTemplate>
                                <ClientSideEvents TextChanged="SynchronizeFilterListBoxValues" DropDown="SynchronizeFilterListBoxValues" />
                            </dx:ASPxDropDownEdit>
                        </td>
                        <td runat="server" id="rowLeaveRequestType2" visible="false">
                            <asp:DropDownList ID="ddlLeaveRequestType" Width="120px" runat="server">
                                <asp:ListItem Text="Approved" Value="2" Selected="True" />
                                <asp:ListItem Text="Request" Value="1,4" />
                                <asp:ListItem Text="Recommend" Value="6" />
                                <asp:ListItem Text="Cancelled" Value="5" />
                                <asp:ListItem Text="Denied" Value="3" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowLoanType1" visible="false">
                            <asp:DropDownList ID="ddlLoanType" Width="120px" runat="server">
                                <asp:ListItem Text="All" Value="1" Selected="True" />
                                <asp:ListItem Text="This Month Started/Ending" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowBank2" visible="false">
                            <asp:DropDownList ID="ddlBank" Width="200px" DataTextField="Name" DataValueField="BankID"
                                runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowAddOn2" visible="false">
                            <asp:DropDownList ID="ddlAddOnName" runat="server" Width="120px" DataTextField="Name"
                                DataValueField="AddOnId">
                                <asp:ListItem Text="--Select AddOn--" Value="-1" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowSSTTDSType2" visible="false">
                            <asp:DropDownList ID="ddlSSTTDSType" runat="server" Width="120px">
                                <asp:ListItem Text="TDS" Value="true" />
                                <asp:ListItem Text="SST" Value="false" />
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowFamiltyType2" visible="false">
                            <uc3:MultiCheckCombo ID="multiFamiltyType" runat="server" />
                        </td>
                         <td runat="server" id="rowUnit2" visible="false">
                             <asp:DropDownList ID="ddlUnit" AutoPostBack="true" AppendDataBoundItems="true"
                                 DataTextField="Name" DataValueField="UnitID" runat="server" Width="100px">
                                 <asp:ListItem Text="--Select--" Value="-1" Selected="True" />
                             </asp:DropDownList>
                        </td>
                        <td runat="server" id="rowPFType2" visible="false">
                            <asp:DropDownList ID="ddlPFType" runat="server" Width="120px">
                                <asp:ListItem Text="All" Value="all" />
                                <asp:ListItem Text="" Value="false" />
                                <asp:ListItem Text="" Value="true" />
                            </asp:DropDownList>
                        </td>
                       
                        <td runat="server" id="rowDynamic2" visible="false">
                            <asp:DropDownList ID="ddlDynamic" runat="server" Width="140px">

                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
