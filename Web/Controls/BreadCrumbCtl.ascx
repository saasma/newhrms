﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BreadCrumbCtl.ascx.cs"
    Inherits="Web.Controls.BreadCrumbCtl" %>
<style type="text/css">
    .breadcrumb
    {
        float: none;
        width: 100%;
        padding: 10px 1%;
        text-align: left;
        font: 11px Arial,Helvetica,sans-serif;
        color: #333;
        padding: 8px 15px;
        margin-bottom: 20px;
        list-style: none;
        background-color: #f5f5f5;
        border-radius: 4px;
        
    }
    .breadcrumb a
    {
        color:#428bca!important;
    }
</style>
<%--<ul class="breadcrumb">
    <li><a href="index.html?lang=en" class="glyphicons home"><i></i>Smashing</a></li>
    <li class="divider"></li>
    <li>Bookings</li>
</ul>
--%>
<div id="MainDiv" runat="server">
    <div class="media">
        <div class="media-body">
            <ul class="breadcrumb" runat="server" id='breadcumb' visible="false">
                <li><a  runat="server" id="lnkParentPageHome"><i class="glyphicon glyphicon-home">
                </i></a></li>
                <li><a href="#" id="lnkParentPage" runat="server"><span id="lnkParentPageText" runat="server"></span></a></li>
                <li id="TitleCurrentPage" runat="server"></li>
            </ul>
           <%-- <h4 id="BreadCrumbPageTitle" runat="server">
                Employee List</h4>--%>
        </div>
    </div>
    <!-- media -->
</div>
<%--
<ul class="breadcrumb" runat="server" id='breadcumb' visible="false">
                                    <li><a href="#" runat="server" id="lnkParentPage"><i class='<%# IsFirstAndIncrement()==true ? "glyphicons home" : "" %>'></i></a></li>
                                    <li><a href="#"><span id="lnkParentPageText" runat="server"></span></a></li>
                                    <li id="TitleCurrentPage" runat="server"></li>
                                </ul>--%>
<%--<ul class="breadcrumb" runat="server" id='breadcumb' visible="false">
    <li>
        <asp:HyperLink ID="lnkPage" Text="HR Dashboard" class='<%# IsFirstAndIncrement()==true ? "glyphicons home" : "" %>'
            runat="server" NavigateUrl="~/newhr/DashboardHR.aspx">
        </asp:HyperLink><span> /</span>
        <asp:Label ID="lblMainPage" Text="" runat="server"></asp:Label>
    </li>
</ul>--%>
<%--<ul class="breadcrumb" runat="server" id='breadcumb'>
<asp:Label ID="lblMainPage" runat="server"></asp:Label> <span>"/"</span>
 <asp:HyperLink 
                    ID="lnkPage" class='<%# IsFirstAndIncrement()==true ? "glyphicons home" : "" %>'
                    runat="server" NavigateUrl=>
							<i></i><%#Eval("title")%>
                </asp:HyperLink>

  <%--  <asp:Repeater ID="rptBreadCumbs" runat="server"> 
        <ItemTemplate>
            <%# GetBreadhcumbDivider() %>
          <%--  <li>
                <asp:HyperLink Visible='<%# !IsCurrentPage(((System.Xml.XmlElement)Container.DataItem).Attributes["url"].Value)  %>'
                    ID="HyperLink1" class='<%# IsFirstAndIncrement()==true ? "glyphicons home" : "" %>'
                    runat="server" NavigateUrl='<%# ((System.Xml.XmlElement)Container.DataItem).Attributes["url"].Value %>'>
							<i></i><%# ((System.Xml.XmlElement)Container.DataItem).Attributes["title"].Value %>
                </asp:HyperLink>
                <li runat="server" visible='<%# IsCurrentPage(((System.Xml.XmlElement)Container.DataItem).Attributes["url"].Value)  %>'>
                    <%# ((System.Xml.XmlElement)Container.DataItem).Attributes["title"].Value %></li>
            </li>--%>
<%--        <li>
                <asp:HyperLink Visible="false" 
                    ID="HyperLink2" class='<%# IsFirstAndIncrement()==true ? "glyphicons home" : "" %>'
                    runat="server" NavigateUrl='<%#Eval("url")%>'>
							<i></i><%#Eval("title")%>
                </asp:HyperLink>
                
                <li id="Li1" runat="server" visible="true">
                   <%#Eval("title")%></li>
            </li>
--%>
<%-- <li class="divider"></li>
            <li>Bookings</li>--%>
<%-- </ItemTemplate>--%>
<%--</asp:Repeater>--%>
<%-- </ul>--%>
