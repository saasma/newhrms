﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InfoMsgCtl.ascx.cs"
    Inherits="Web.Controls.InfoMsgCtl" %>
<div runat="server" id="msg"  class="notify bg-info bigger">
    <em>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;</em>
    
  
    
    <div runat="server" id="msgText" class="message" style='color:#3A9C00;display:inline!important;width:88%'>
    </div>
</div>
<style>
div.bg-info {
    background-color: #F9FEF6;
    border-color: #64B23B;
}

div.bg-info em {
    background-position: 0 0px;
}
</style>