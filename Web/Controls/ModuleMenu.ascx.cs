﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using BLL.Manager;
using BLL;
using System.Web.UI.HtmlControls;

namespace Web.Controls
{
    public partial class ModuleMenu : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest && !IsPostBack)
            {
                BasePage basePage = this.Page as BasePage;
                if (basePage != null)
                {
                    if (basePage.MenuType == BLL.MenuTypeEnum.HumanResource)
                        menuHR.Visible = true;
                    else if (basePage.MenuType == BLL.MenuTypeEnum.Payroll)
                        menuPayroll.Visible = true;
                    else if (basePage.MenuType == BLL.MenuTypeEnum.AdministratorTools)
                        menuSetting.Visible = true;
                }

                if (CommonManager.CompanySetting.HasLevelGradeSalary==false)
                {
                    acting.Visible = false;
                }

                SelectMenu();
            }
        }



        public void SelectMenu()
        {
            MenuTypeEnum menuType = MenuTypeEnum.NotSet;
            HtmlGenericControl menu = null;
            // Select current page menu
            BasePage basePage = this.Page as BasePage;
            if (basePage != null)
            {
                menuType = basePage.MenuType;
            }

            switch (menuType)
            {
                case MenuTypeEnum.HumanResource:
                    menu = menuHR;
                    break;
                case MenuTypeEnum.Payroll:
                    menu = menuPayroll;
                    break;
                case MenuTypeEnum.AdministratorTools:
                    menu = menuSetting;
                    break;      
            }


            if (menu != null)
            {
                string currentPageUrl = this.CurrentPageUrl.ToLower();

                foreach (Control ctlMain in menu.Controls)
                {

                    foreach (Control ctl in ctlMain.Controls)
                    {

                        // select for child
                        foreach (Control ctlChild in ctl.Controls)
                        {
                            HtmlAnchor anchorChild = ctlChild as HtmlAnchor;
                            if (anchorChild != null && !string.IsNullOrEmpty(anchorChild.HRef))
                            {
                                if (anchorChild.HRef.ToLower().Contains(currentPageUrl))
                                {


                                    HtmlGenericControl parentliControl = anchorChild.Parent as HtmlGenericControl;
                                    parentliControl.Attributes["class"] += " active";

                                    HtmlGenericControl parentliParentControl = anchorChild.Parent.Parent as HtmlGenericControl;
                                    parentliParentControl.Attributes["class"] += " active";

                                    //ctlMain.Attributes["class"] = "collpase in";

                                    return;
                                }
                            }
                        }

                        // select for root
                        HtmlAnchor anchor = ctl as HtmlAnchor;
                        if (anchor != null && !string.IsNullOrEmpty(anchor.HRef))
                        {
                            if (anchor.HRef.ToLower().Contains(currentPageUrl))
                            {
                               
                                HtmlGenericControl parentliControl = anchor.Parent as HtmlGenericControl;
                                parentliControl.Attributes["class"] += " active";

                                //ctlMain.Attributes["class"] = "collpase in";

                                //if (acting.Parent.Parent != null)
                                //{
                                //    HtmlGenericControl parentliParentControl = anchor.Parent.Parent as HtmlGenericControl;
                                //    parentliParentControl.Attributes["class"] += " active";

                                //}

                                return;
                            }
                        }


                        

                    }
                }
            }
        }

        public bool IsAccessible(string url)
        {
            return UserManager.IsPageAccessible(url);

        }


    }
}