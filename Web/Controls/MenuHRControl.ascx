﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuHRControl.ascx.cs"
    Inherits="Web.Controls.MenuHRControl" %>
<style type="text/css">
    .AutoExtender
    {
        z-index: 999999 !important;
    }
</style>
<div id="menu" class="hidden-phone">
   
    <ul runat="server" id="employeeMenuList">
        <li runat="server" id="menu1" class="hasSubmenu glyphicons user active"><a  data-toggle="collapse1" href="javascript:void(0)"><i></i><span>Employee Details</span></a>
            <ul runat="server" class="in1 collapse1" id="menuItemList1">
                <li class=""><a runat="server" isemployeedetails="true" id="menuEmployeePersonalDetails"
                    href="~/newhr/PersonalDetail.aspx"><span>Personal Details</span></a></li>
                <li class="in collapse"><a runat="server" isemployeedetails="true" id="menuEmployeeAddress"
                    href="~/newhr/Address.aspx"><span>Address</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="menuEmployeeHealth" href="~/newhr/Health.aspx">
                    <span>Health</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A51" href="~/newhr/DocumentListing.aspx">
                    <span>Document</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A41" href="~/newhr/Family.aspx">
                    <span>Family</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A11" href="~/newhr/Identification.aspx">
                    <span>Identification</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A31" href="~/newhr/PreviousEmployment.aspx">
                    <span>Previous Employment</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="menuEmployeeEducationTraining"
                    href="~/newhr/EducationTraining.aspx"><span>Education and Skills</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A1" href="~/newhr/EmployeeLeave.aspx">
                    <span>Leave Information</span></a></li>
                <li class=""><a runat="server" isemployeedetails="true" id="A4" href="~/newhr/EmployeePayroll.aspx">
                    <span>Payroll Information</span></a></li>
                <li class=""><a runat="server" id="liReward" isemployeedetails="true"  href="~/newhr/EmployeeGradeReward.aspx">
                    <span>Reward / Additional</span></a></li>
                <li class=""><a runat="server" id="A2" isemployeedetails="true"  href="~/newhr/OtherHRDetails.aspx">
                    <span>Additional Information</span></a></li>
            </ul>
        </li>
    </ul>
    <div class="clearfix" style="clear: both">
    </div>
</div>
