﻿<%@ Control Language="C#" EnableViewState="true" AutoEventWireup="true" CodeBehind="CalendarCtl.ascx.cs"
    Inherits="Web.UserControls.CalendarCtl" %>
<asp:HiddenField ID="calType" Value="True" runat="server" />
<asp:HiddenField ID="calEnabled" Value="True" runat="server" />
<asp:HiddenField ID="calPreviousValue" Value="" runat="server" />
<asp:HiddenField ID="calIsSkipDay" Value="" runat="server" />
<asp:HiddenField ID="calIsSkipYear" Value="" runat="server" />
<asp:HiddenField ID="calIsSkipMonth" Value="" runat="server" />
<asp:PlaceHolder ID="container" runat="server" EnableViewState="false">
    <asp:Panel ID="date" runat="server" Style="display: inline">
        <select runat="server" id="datey">
        </select>
        <select runat="server" id="datem" style='margin-right: 2px'>
        </select>
        <select runat="server" id="dated" style='margin-right: 2px'>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>
            <option value="17">17</option>
            <option value="18">18</option>
            <option value="19">19</option>
            <option value="20">20</option>
            <option value="21">21</option>
            <option value="22">22</option>
            <option value="23">23</option>
            <option value="24">24</option>
            <option value="25">25</option>
            <option value="26">26</option>
            <option value="27">27</option>
            <option value="28">28</option>
            <option value="29">29</option>
            <option value="30">30</option>
            <option value="31">31</option>
            <option value="32">32</option>
        </select>
    </asp:Panel>
</asp:PlaceHolder>
