﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Controls
{
    public partial class WarningMsgCtl : System.Web.UI.UserControl
    {

        public string InnerHtml
        {
            get
            {
                return msgText.InnerHtml;
            }
            set
            {
                msgText.InnerHtml = value;
            }
        }
        public string List
        {
            get
            {
                return msgList.InnerHtml;
            }
            set
            {
                msgList.InnerHtml = value;
            }
        }

        public bool HideMsgText
        {
            set
            {
                msgText.Visible = false;
            }
        }
        public bool Hide
        {
            set
            {
                msg.Style.Remove("display");
                if (value)
                    msg.Style.Add("display", "none");
            }
        }

        public string Width
        {
            set
            {
                msg.Style["width"] = value;
            }

        }

    }
}