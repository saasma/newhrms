﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Controls
{
    public partial class UpDownCtl : System.Web.UI.UserControl
    {
        private bool _allowDecimal = false;
        public bool AllowDecimal
        {
            get
            {
                return _allowDecimal;
            }
            set
            {
                _allowDecimal = value;
            }
        }

        public double Value
        {
            get
            {
                return double.Parse(this.txt.Text);
            }
            set
            {
                txt.Text = value.ToString();
            }
        }

        public TextBox TextBox
        {
            get
            {
                return this.txt;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "sdfsdfds324324", "var allowDecimal=" + this.AllowDecimal.ToString().ToLower() + ";", true);
        }
    }
}