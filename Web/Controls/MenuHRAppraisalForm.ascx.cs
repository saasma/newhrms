﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ext.Net;

namespace Web.Controls
{
    public partial class MenuHRAppraisalForm : BLL.Base.BaseUserControl
    {
        //HtmlGenericControl[] menuList = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack && !X.IsAjaxRequest)
                ShowHideEmployeeHRMenus();
        }

       



        public void ShowHideEmployeeHRMenus()
        {
            bool hasEmployeeId = false;
            int employeeId = 0;
            string pageUrl = CurrentPageUrl.ToLower();


            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                hasEmployeeId = true;
                employeeId = int.Parse(Request.QueryString["fid"]);
            }

            if (hasEmployeeId)
            {
                //foreach (HtmlGenericControl ctlMain in menuList)
                {


                    {
                        foreach (Control ctl in menu_index.Controls)
                        {
                            //H
                            HtmlAnchor anchor = ctl as HtmlAnchor;
                            if (anchor != null)
                            {
                                
                                {
                                    anchor.Attributes["href"] += "?fid=" + employeeId;
                                    anchor.Visible = true;

                                    if (anchor.HRef.ToLower().Contains(pageUrl))
                                    {
                                        anchor.Attributes["class"] = "menuActiveA";
                                    }

                                  
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}