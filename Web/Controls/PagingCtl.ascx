﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagingCtl.ascx.cs" Inherits="Web.Controls.PagingCtl" %>
<div class="PaginationBar">
    <span style='padding-left: 10px;'>Page &nbsp;</span>
    <asp:DropDownList AutoPostBack="true" ID="ddlPageNumber" Style='display: none' runat="server"
        OnSelectedIndexChanged="ddlPageNumber_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:Label Text="1" ID="lblCurrentPage" Style='font-weight: bold' runat="server"></asp:Label>
    of
    <asp:Label ID="lblTotalPage" Text="10" Style='font-weight: bold' runat="server"></asp:Label>
    <asp:Label ID="lblTotalRecords" Text="" Style='font-weight: bold' runat="server"></asp:Label>
    &nbsp;&nbsp;&nbsp; <span class="PaginationFadedText">|</span>&nbsp;&nbsp;&nbsp;
    <span class="PaginationFadedText">Show</span>
    <asp:DropDownList AutoPostBack="true" ID="ddlRecords" runat="server" OnSelectedIndexChanged="ddlRecords_SelectedIndexChanged">
        <asp:ListItem Text="20">20</asp:ListItem>
        <asp:ListItem Text="30">30</asp:ListItem>
        <asp:ListItem Text="50">50</asp:ListItem>
        <asp:ListItem Text="100">100</asp:ListItem>
        <asp:ListItem Text="200">200</asp:ListItem>
         <asp:ListItem Text="300">300</asp:ListItem>
    </asp:DropDownList>
    <span class="PaginationFadedText" style="padding-right: 10px;">records per page</span>
    <span class="PaginationFadedText">|</span> <span style="padding-right: 20px; margin-left: 15px;
        font: bold 11px arial;">
        <asp:LinkButton ID="btnPrevious" runat="server" Text="&laquo; Prev" OnClick="btnPrevious_Click"
            Style="padding-right: 10px;" />
        &nbsp;&nbsp;
        <asp:LinkButton ID="btnNext" runat="server" Text="Next &raquo;" OnClick="btnNext_Click" />
    </span>
</div>
<div style="clear: both">
</div>
