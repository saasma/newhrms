﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL;
using System.Web.Security;
using BLL.Manager;
using Utils.Security;
using Utils.Helper;
using Ext.Net;
using System.IO;

namespace Web.Controls
{
    public partial class ModuleListMenu : System.Web.UI.UserControl
    {
        string[] empSearchUrl = new string[] { 
            "PersonalDetail.aspx", "Address.aspx", "Health.aspx", "DocumentListing.aspx", 
            "Family.aspx", "Identification.aspx", "PreviousEmploymen.aspxt", "EducationTraining.aspx", 
            "EmployeeLeave.aspx", "EmployeePayroll.aspx", "OtherHRDetails.aspx" };

        public string CurrentPageUrl
        {
            get
            {
                string currentPage = Path.GetFileName(Request.Url.AbsoluteUri.ToString()).ToLower();
                if (currentPage.IndexOf("?") >= 0)
                    currentPage = currentPage.Remove(currentPage.IndexOf("?"));
                return currentPage;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {

                LoadMessage();
                SetName();

                try
                {
                    string url = Page.ResolveUrl("~/excelwindow/DateConverter.aspx");
                    X.Js.AddScript("dateUrl = '" + url + "';");
                }
                catch { }

                if (!IsPostBack)
                {


                    btnChangePwd.OnClientClick = "window.location = '" +
                        UrlHelper.GetRootUrl() + "CP/ChangePassword.aspx';return false;";


                    foreach (string item in empSearchUrl)
                    {
                        if(item.Trim().ToLower().Equals(this.CurrentPageUrl.Trim().ToLower()))
                        {
                            Page.ClientScript.RegisterClientScriptBlock(GetType(), "sdfsfsdfsf",
                                "empSearchUrl='NewHR/" + item + "';", true);
                        }
                    }

                    if (SessionManager.IsCustomRole)
                    {
                        if (UserManager.IsPageAccessible("newhr/EmployeeDetails.aspx") == false
                            && UserManager.IsPageAccessible("newhr/PREmployeeListing.aspx") == false)
                            divEmpSearch.Visible = false;

                        if (UserManager.IsModuleAccessible(ModuleEnum.AdministratorTools) == false)
                            moduleAdministratorTools.Visible = false;
                        if (UserManager.IsModuleAccessible(ModuleEnum.HumanResource) == false)
                            moduleHR.Visible = false;
                        if (UserManager.IsModuleAccessible(ModuleEnum.EmployeeAppraisal) == false)
                            moduleAppraisal.Visible = false;
                        if (UserManager.IsModuleAccessible(ModuleEnum.LeaveAndAttendance) == false)
                            moduleLeave.Visible = false;
                        if (UserManager.IsModuleAccessible(ModuleEnum.PayrollManagement) == false)
                            modulePayroll.Visible = false;
                        if (UserManager.IsModuleAccessible(ModuleEnum.Retirement) == false)
                            moduleRetirement.Visible = false;
                        if (UserManager.IsModuleAccessible(ModuleEnum.ProjectAndTimesheet) == false)
                            moduleProject.Visible = false;

                        if (UserManager.IsModuleAccessible(ModuleEnum.DocumentManagement) == false)
                            moduleDocManagement.Visible = false;

                        //moduleProject.Visible = ProjectManager.GetAllProjectList().Count == 0;
                    }

                    if (CommonManager.Setting.ShowDocumentModule != null && CommonManager.Setting.ShowDocumentModule.Value)
                    { 
                        
                    }
                    else
                        moduleDocManagement.Visible = false;

                    // if no project then hide Project/Timesheet Module
                    if (BLL.BaseBiz.PayrollDataContext.Projects.Any() == false)
                        moduleProject.Visible = false;


                    if (CommonManager.ShowAppraisalDashboard() == false)
                        moduleAppraisal.Visible = false;
                    // hide Appraisal for Janata
                    //moduleAppraisal.Visible = (CommonManager.CompanySetting.WhichCompany != WhichCompany.Janata);

                }



                if (SessionManager.CurrentCompany != null && !BLL.BaseBiz.IsEnglish)
                {
                    listDatePref.Visible = true;

                    UUser objUUser = UserManager.GetUUser();
                    if ((objUUser.IsEnglishPreferred != null) && (objUUser.IsEnglishPreferred == true))
                        btnEngPref.Text = " <i class='glyphicon glyphicon-star'></i>Switch to Nepali date";
                    else
                        btnEngPref.Text = " <i class='glyphicon glyphicon-star'></i>Switch to English date";
                }
                else
                    listDatePref.Visible = false;
            }
        }

        public void LoadMessage()
        {
           
            if (SessionManager.CurrentCompany != null)
                companyName.InnerHtml = SessionManager.CurrentCompany.Name;

            if (SessionManager.User.UserMappedEmployeeId != null && SessionManager.User.UserMappedEmployeeId != 0
                && SessionManager.User.UserMappedEmployeeId != -1)
            {

            }
            else
            {
                btnSwitch.Style["display"] = "none";
            }
        }

        private void SetName()
        {
            UUser user = SessionManager.User;
            if (user.IsEmployeeType == null || user.IsEmployeeType == false)
                spanUserName.InnerHtml = "Welcome, " + user.UserName;
            else
            {

                spanUserName.InnerHtml = "Welcome, " + user.EEmployee.Name;
            }
        }

        protected void btnSwitch_Click(object sender, EventArgs e)
        {
            int? employeeId = SessionManager.User.UserMappedEmployeeId;
            if (employeeId != null && employeeId != 0 && employeeId != 0)
            {
                UUser user = UserManager.GetUserByEmployee(employeeId.Value);
                if (user != null)
                {

                    BLL.BaseBiz.PayrollDataContext.ChangeUserActivities.InsertOnSubmit(
                  BLL.BaseBiz.GetChangeUserActivityLog("User Switched from " + SessionManager.User.UserName + " to " + user.UserName + "."));
                    BLL.BaseBiz.PayrollDataContext.SubmitChanges();

                    Session.Abandon();
                    FormsAuthentication.SignOut();





                    SecurityHelper.CreateCookie(user.UserName,
                        true, user.URole.Name, false, false);

                    Response.Redirect("~/Employee/Dashboard.aspx", true);

                }
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            int currentLoggedInEmployeeId = SessionManager.CurrentLoggedInEmployeeId;
            Session.Abandon();
            FormsAuthentication.SignOut();
            // Response.cookies.clear();
            if (currentLoggedInEmployeeId == 0)
                Response.Redirect("~/Default.aspx", true);
            else
                Response.Redirect("~/Employee/Default.aspx", true);
        }

        protected void btnEngPref_Click(object sender, EventArgs e)
        {
            bool val = false;

            if (SessionManager.User.IsEnglishPreferred != null && SessionManager.User.IsEnglishPreferred == true)
                val = true;
            else
                val = false;


            if (UserManager.UpdateUUserForEnglishPreferred(!val))
            {
                //if (val)
                //    DatePref.InnerText = " Switch to Nepali date";
                //else
                //    DatePref.InnerText = " Switch to English date";

                string url = Request.Url.ToString();
                Response.Redirect(url);
            }
        }
    }
}