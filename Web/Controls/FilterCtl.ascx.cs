﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Manager;
using DAL;
using BLL.Base;

namespace Web.Controls
{

    public delegate void FilterChanged();

    public partial class FilterCtl : BaseUserControl
    {
        public event FilterChanged FilterChanged;

        protected void Page_Init(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                Initialise();

            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (BLL.BaseBiz.PayrollDataContext.SubDepartments.Any() == false)
                {
                    rowSubDep1.Visible = false;
                    rowSubDep2.Visible = false;
                }
            }

            if (filterShown.Value.Equals("true"))
                rowOtherFilter.Style["display"] = "block";
        }

        public void Initialise()
        {

            //levelName.InnerText = CommonManager.GetLevelName;

             List<Branch> list = BranchManager.GetBranchesByCompany(SessionManager.CurrentCompanyId);

            ddlBranch.DataSource
                = list;
            ddlBranch.DataBind();

            ddlSubDepartment.DataSource = new DepartmentManager().GetSubDepartmentsByCompany(SessionManager.CurrentCompanyId);
            ddlSubDepartment.DataBind();

            // For d2 as only one branch so select it by default & hide it
            if (CommonManager.CompanySetting.HideBranchIfOnlyOne
                && list.Count <= 1)
            {
                ddlBranch.SelectedIndex = 1;
                rowBranch1.Style["display"] = "none";
                rowBranch2.Style["display"] = "none";
            }

            ddlGroups.DataSource = NewPayrollManager.GetLevelGroup().OrderBy(x => x.Order).ToList();
            ddlGroups.DataBind();

            ddlBranch_SelectedIndexChanged(null, null);

            if (CommonManager.Setting.EnableWeeklyLikePay != null && CommonManager.Setting.EnableWeeklyLikePay.Value)
            {
                List<PayGroup> payList = PeriodicPayManager.GetPayGroup(); ;
                payList.Insert(0, new PayGroup { PayGroupID = 0, Name = "Regular Pay" });
                ddlPayGroup.DataSource = payList;
                ddlPayGroup.DataBind();
                tdPayGroup.Visible = true;
            }

            List<UnitList> units = CommonManager.GetAllUnitList();
            if (units.Count >=1)
            {

                ddlUnit.DataSource = units;
                ddlUnit.DataBind();
                tdUnit.Visible = true;
            }

            CommonManager mgr  = new CommonManager();
            ddlDesignation.DataSource = mgr.GetAllDesignationsNew().OrderBy(x => x.LevelAndDesignation).ToList();
            ddlDesignation.DataBind();


            ddlLevel.DataSource = NewPayrollManager.GetAllParentLevelList()
                .OrderBy(x => x.Name).ToList();
            ddlLevel.DataBind();


            ddlDesignationOnly.DataSource = CommonManager.GetUniqueDesignationList();
            ddlDesignationOnly.DataBind();



            JobStatus statues = new JobStatus();
            List<KeyValue> statusList = statues.GetMembers();

            statusList.RemoveAt(0);

            foreach (KeyValue item in statusList)
            {
                multiStatus.AddItems(item.Value, item.KeyValueCombined);
            }
        }

        public bool SearchEmployeeTextboxAutoPostback
        {
            get
            {
                return txtEmployeeName.AutoPostBack;
            }
            set
            {
                txtEmployeeName.AutoPostBack = value;
            }
        }

        public int CITRoundOffValue
        {
            get
            {
                return int.Parse(ddlRounding.SelectedValue);
            }
        }
        
        public int DepartmentId
        {
            get
            {
                return int.Parse(ddlDepartment.SelectedValue);
            }
        }
        public int LevelGroupID
        {
            get
            {
                return int.Parse(ddlGroups.SelectedValue);
            }
        }
        public int SubDepartmentId
        {
            get
            {
                return int.Parse(ddlSubDepartment.SelectedValue);
            }
        }
        public int LevelId
        {
            get
            {
                return int.Parse(ddlLevel.SelectedValue);
            }
        }
        public int BranchId
        {
            get
            {
                return int.Parse(ddlBranch.SelectedValue);
            }
        }
        public int PayGroupID
        {
            get
            {
                return int.Parse(ddlPayGroup.SelectedValue);
            }
        }
        public int UnitId
        {
            get
            {
                return int.Parse(ddlUnit.SelectedValue);
            }
        }
        public int DesignationId
        {
            get
            {
                return int.Parse(ddlDesignation.SelectedValue);
            }
        }
        //public int StatusID
        //{
        //    get
        //    {
        //        string[] value = ddlStatus.SelectedValue.Split(new char[] { ':' });
        //        return int.Parse(value[0]);
        //    }
        //}
        public string StatusIDList
        {
            get
            {
                // Process for Status Filter
                JobStatus statues = new JobStatus();
                List<KeyValue> statusList = statues.GetMembers();
                string[] statusTexts = multiStatus.Text.Split(new char[] { ',' });
                string statusIDTexts = "";
                foreach (string item in statusTexts)
                {
                    string text = item.ToString().ToLower().Trim();
                    KeyValue status = statusList.FirstOrDefault(x => x.Value.ToString().ToLower().Trim() == text);
                    if (status != null)
                    {
                        if (statusIDTexts == "")
                            statusIDTexts = status.Key;
                        else
                            statusIDTexts += "," + status.Key;
                    }
                }

                return statusIDTexts;
            }
        }
        public string DesignationName
        {
            get
            {
                string name = ddlDesignationOnly.SelectedValue;



                if (name.ToLower() == "all")
                    name = "";

                return name;
            }
        }
        //public int WorkShiftId
        //{
        //    get
        //    {
        //        return int.Parse(ddlWorkShift.SelectedValue);
        //    }
        //}

        public bool CITRoundOff
        {
            set
            {
                this.rowCITRoundOff.Visible = value;
                this.rowCITRoundOffValue.Visible = value;
            }
        }    
        public void SetFilterEmployee(int empId, string name)
        {
            txtEmployeeName.Text = name;
        }
        public string EmployeeNameValue
        {
            get
            {
                return txtEmployeeName.Text.Trim();
            }
        }

        public string EmployeeIDCardNoValue
        {
            get
            {
                return txtIDCardNo.Text.Trim();
            }
        }

        public bool Department
        {
            set
            {
                rowDep1.Visible = value;
                rowDep2.Visible = value;
            }
        }
      
        public bool SubDepartment
        {
            set
            {
                rowSubDep1.Visible = value;
                rowSubDep2.Visible = value;
            }
        }
        public bool ShowDesignationName
        {
            set
            {
                rowDesignationOnly1.Visible = value;
                rowDesignationOnly2.Visible = value;
            }
        }
        public bool Status
        {
            set
            {
                rowStatus1.Visible = value;
                rowStatus2.Visible = value;
            }
        }

        public bool Level
        {
            set
            {
                rowLevel1.Visible = value;
                rowLevel2.Visible = value;
            }
        }
        public bool Branch
        {
            set
            {
                rowBranch1.Visible = value;
                rowBranch2.Visible = value;
            }
        }
        public bool Designation
        {
            set
            {
                rowDesignation1.Visible = value;
                rowDesignation2.Visible = value;
            }
        }
        //public bool WorkShift
        //{
        //    set
        //    {
        //        rowWorkShift1.Visible = value;
        //        rowWorkShift2.Visible = value;
        //    }
        //}

        public bool EmployeeName
        {
            set
            {
                rowEmpName1.Visible = value;
                rowEmpName2.Visible = value;
            }
        }
        public bool EmployeeIDCardNo
        {
            set
            {
                rowIDCardNo1.Visible = value;
                rowIDCardNo2.Visible = value;
            }
        }


        protected void btnLoad_Click(object sender, EventArgs e)
        {
            if (FilterChanged != null)
                FilterChanged();
        }

        protected void ddlLevel_Change(object sender, EventArgs e)
        {
            //ListItem firstItem = ddlSubDepartment.Items[0];
            //ddlSubDepartment.Items.Clear();

             CommonManager mgr  = new CommonManager();
            


            int levelId = int.Parse(ddlLevel.SelectedValue);
            if (levelId == -1)
            {
                ddlDesignation.DataSource = mgr.GetAllDesignationsNew();
                ddlDesignation.DataBind();
            }
            else
            {
                ddlDesignation.DataSource = mgr.GetAllDesignationsNew().Where(x => x.LevelId == levelId).ToList();
                ddlDesignation.DataBind();
            }

            upd.Update();
            //ddlSubDepartment.DataSource
            //    = DepartmentManager.GetAllSubDepartmentsByDepartment(int.Parse(ddlDepartment.SelectedValue));

            //ddlSubDepartment.DataBind();

            //ddlSubDepartment.Items.Insert(0, firstItem);
        }

        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem firstItem = ddlDepartment.Items[0];
            ddlDepartment.Items.Clear();



            ddlDepartment.DataSource
                = DepartmentManager.GetAllDepartments();

            ddlDepartment.DataBind();

            ddlDepartment.Items.Insert(0, firstItem);


        }
    }
}