﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleListMenu.ascx.cs"
    Inherits="Web.Controls.ModuleListMenu" %>
<style type="text/css">
    .searchMenuCss
    {
        -moz-border-radius: 15px;
        border-radius: 0px !important;
        padding-left: 25px !important;
        height: 32px !important;
    }
    
    .menuSearchCss
    {
        float: left;
        padding-right: 10px;
    }
    
    .menuSearchCss:before
    {
        position: absolute !important;
        top: 6px !important;
        left: 10px !important;
        font-family: 'Glyphicons Halflings' !important;
        content: '\e003' !important;
        color: #666 !important;
    }
    
    .removeBorder
    {
        border-radius: 0px !important;
    }
    
    .itemCssClass
    {
        color: Blue;
        padding-left: 10px;
    }
    
    .itemCssClass:hover
    {
        text-decoration: underline;
    }
    
    .itemCssClass:visited
    {
        text-decoration: underline;
    }
</style>
<script type="text/javascript">

    

    function ACE_item_selected_Global(source, eventArgs) {
        var value = eventArgs.get_value();
        if (value != null) {
            if(typeof(empSearchUrl) != "undefined")
                window.location = url + empSearchUrl + "?ID=" + value;
            else
                window.location = url + "newhr/EmployeeDetails.aspx?ID=" + value;
        } else {
            alert("First select the employee from the list.");
        }
    }


    function OnClientPopulated(sender, eventArgs) {


        //Find the autocompleteextender list
        //var autoList = $find("AutoCompleteExtenderOrganization").get_completionList();
        var autoList = sender.get_completionList();
        for (i = 0; i < autoList.childNodes.length; i++) {
            // Consider value as image path

            var name = autoList.childNodes[i].firstChild.nodeValue;

            var spl = name.split(':');

            var imgeUrl = spl[0].toString();
            var name = spl[1].toString();

            //autoList.childNodes[i].innerHTML = "<img height=25 width=30 src=" +  imgeUrl + " /> " + name;

            if (imgeUrl == 'femalepic') {
                autoList.childNodes[i].innerHTML = "<img height=25 width=30 src=" + '<%= Page.ResolveUrl("~/images/female.png") %>' + " /> " + name;

            }
            else if (imgeUrl == 'malepic') {
                autoList.childNodes[i].innerHTML = "<img height=25 width=30 src=" + '<%= Page.ResolveUrl("~/images/male.png") %>' + " /> " + name;
            }
            else {
                autoList.childNodes[i].innerHTML = "<img height=25 width=30 src=" + imgeUrl + " /> " + name;
            }

        }
    }

    function MenuItemSelected(source, e) {


        var value = e.get_value();

        if (!value) {


            if (e._item.parentElement && e._item.parentElement.tagName == "LI") {

                value = e._item.parentElement._value;
            }
            else {
                value = null;
            }
        }

        if (value != null) {
            window.location = url + value.substring(2);
        } else {
            document.getElementById('<%= txtMenuSearchAuto.ClientID %>').value = '';
            alert("First select the page from the list.");
        }

    }


    function OnClientPopulatedMenuList(sender, eventArgs) {


        //Find the autocompleteextender list
        //var autoList = $find("AutoCompleteExtenderOrganization").get_completionList();
        var autoList = sender.get_completionList();

        for (i = 0; i < autoList.childNodes.length; i++) {

            var name = autoList.childNodes[i].firstChild.nodeValue;

            var spl = name.split(':');

            var moduleId = spl[0].toString();
            var title = spl[1].toString();

            if (moduleId == 0) {
                autoList.childNodes[i].innerHTML = '<span class="itemCssClass">' + '>' + title + '</span>';

            }
            else {
                autoList.childNodes[i].innerHTML = '<span style="color:blue; font-style: italic; background-color:#e4e7ea;">' + title + '</span>';
            }

        }
    }

    function popupDate() {
            
        //calculate center left/top   
        var left = parseInt((screen.availWidth / 2) - (405 / 2));
        var top = parseInt((screen.availHeight / 2) - (320 / 2));
        var myRand = Math.random(50000);
        var myWindow;

        myWindow = window.open(dateUrl + '?rand=' + myRand, 'Date Converter', 'height=350,scrollbars=1,width=505,toolbar=no,directories=no,status=no,menubar=no,center=yes,left=' + left + ',top=' + top);

        try {
            myWindow.focus();
        }
        catch (err) {
        }
    }

</script>
<div class="headerwrapper collapsed">
    <div class="header-left">
        <%-- <a href="index-2.html" class="logo">
            <img src="images/logo.png" alt="" />
        </a>--%>
        <div class="pull-right">
            <a href="#" class="menu-collapse"><i class="fa fa-bars"></i></a>
        </div>
    </div>
    <!-- header-left -->
    <div class="header-right">
        <div class="btn-group btn-group-option" style="margin-left: -160px;">
            <button type="button" class="btn btn-default dropdown-toggle" style="padding-right: 6px;"
                data-toggle="dropdown">
                <i class="fa fa-caret-down" style="margin-right: 7px"></i>Modules&nbsp;
            </button>
            <ul class="dropdown-menu pull-right" style="padding-left: 15px;" role="menu">
                <li id="moduleHR" runat="server">
                    <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/newhr/DashboardHR.aspx">Human Resource</asp:HyperLink></li>
                <li id="moduleLeave" runat="server">
                    <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/dashboard/DashboardLeave.aspx">Leave and Attendance</asp:HyperLink></li>
                <li id="modulePayroll" runat="server">
                    <asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="~/Dashboard/DashboardPR.aspx">Payroll Management</asp:HyperLink></li>
                <li id="moduleProject" runat="server">
                    <asp:HyperLink ID="linkProject" runat="server" NavigateUrl="~/Dashboard/DashboardProject.aspx">Project and Timesheet</asp:HyperLink></li>
                <li id="moduleAppraisal" runat="server">
                    <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/dashboard/DashboardAppraisal.aspx">Employee Appraisal</asp:HyperLink></li>
                <li id="moduleRetirement" runat="server">
                    <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/Dashboard/DashboardRetirement.aspx">Retirement</asp:HyperLink></li>
                <li runat="server" id="moduleDocManagement">
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Dashboard/DashboardDocument.aspx">Document Management</asp:HyperLink></li>
                <li runat="server" id="moduleAdministratorTools">
                    <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/newhr/AllSettings.aspx">Administration Tools</asp:HyperLink></li>
            </ul>
        </div>
        <span style="color: #a9cfef; margin-right: 15px; line-height: 28px; font-size: 18px;"
            runat="server" id="companyName"></span>
        <div class="pull-right">
            <div id="divMenuSearch" class="form form-search" runat="server" >
                <asp:TextBox ID="txtMenuSearchAuto" runat="server" Width="250px" placeholder="Search Menu Items"
                    CssClass="form-control removeBorder" />
                <cc1:AutoCompleteExtender EnableCaching="true" ID="menuSearchAutoComplete" OnClientPopulated="OnClientPopulatedMenuList"
                    runat="server" MinimumPrefixLength="3" ServiceMethod="GetMenuSearchList" ServicePath="~/PayrollService.asmx"
                    TargetControlID="txtMenuSearchAuto" OnClientItemSelected="MenuItemSelected" CompletionSetCount="30"
                    CompletionInterval="250" CompletionListCssClass="AutoExtender AutoExtenderIncreaseWidth"
                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                </cc1:AutoCompleteExtender>
            </div>
            <div class="form form-search" runat="server" id="divEmpSearch">
                <asp:TextBox ID="txtEmpSearchText" Style="width: 230px" class="form-control removeBorder"
                    placeholder="Search Employee" runat="server"></asp:TextBox>
            </div>
            <cc1:AutoCompleteExtender EnableCaching="true" ID="AutoCompleteExtenderOrganization"
                OnClientPopulated="OnClientPopulated" runat="server" MinimumPrefixLength="2"
                ServiceMethod="GetEmployeeNamesWithIDAndINoWithEmpPhoto" ServicePath="~/PayrollService.asmx"
                TargetControlID="txtEmpSearchText" OnClientItemSelected="ACE_item_selected_Global"
                CompletionSetCount="10" CompletionInterval="250" CompletionListCssClass="AutoExtender"
                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
            </cc1:AutoCompleteExtender>
            <span style="float: left; color: #a9cfef; line-height: 30px;" runat="server" id="spanUserName">
            </span>
            <!-- btn-group -->
            <div class="btn-group btn-group-option">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <asp:LinkButton ID="btnSwitch" runat="server" CausesValidation="false" OnClick="btnSwitch_Click"><i class="glyphicon glyphicon-star"></i>Switch User</asp:LinkButton>
                    </li>
                     <li>
                        <asp:LinkButton OnClientClick='popupDate();return false;' ID="btnDateConverter" runat="server" CausesValidation="false"><i class="glyphicon glyphicon-star"></i>Date Converter</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="btnChangePwd" runat="server" OnClientClick='' CausesValidation="false"
                            OnClick="btnSwitch_Click"><i class="glyphicon glyphicon-cog"></i>Change Password</asp:LinkButton>
                    </li>
                    <li id="listDatePref" runat="server" visible="false">
                        <asp:LinkButton ToolTip="Date will be changed to the selected type in certain HR pages"
                            ID="btnEngPref" runat="server" CausesValidation="false" OnClick="btnEngPref_Click">
                            
                        </asp:LinkButton>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <asp:LinkButton ID="btnLogout" runat="server" CausesValidation="false" OnClick="btnLogout_Click">
                        <i class="glyphicon glyphicon-log-out"></i>
                        Logout</asp:LinkButton>
                    </li>
                </ul>
            </div>
            <!-- btn-group -->
        </div>
        <!-- pull-right -->
    </div>
    <!-- header-right -->
</div>
<!-- headerwrapper -->
<ul class="topnav pull-right" style="display: none">
    <li></li>
    <li class="visible-desktop">
        <ul class="notif">
            <li><a id="lblMessage" runat="server" href="~/CP/Message1.aspx" class="glyphicons envelope"
                data-toggle="tooltip" data-placement="bottom" data-original-title="5 new messages">
                <i></i>5</a></li>
        </ul>
    </li>
    <li class="account"><a data-toggle="dropdown" href="javascript:void(0)" onclick='document.getElementById("logoutList").style.display="block"; stopPropagation(event);return false;'
        class="glyphicons logout lock">mosaicpro</span><i></i> </a>
        <ul class="dropdown-menu pull-right" id="logoutList">
            <li><span></span><span></span></li>
        </ul>
    </li>
</ul>
