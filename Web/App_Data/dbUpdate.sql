﻿
if not exists(select top 1 * from ColorList)
Begin

INSERT ColorList(ColorCode,ColorName) VALUES('#F0F8FF','Alice Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#FAEBD7','Antique White')
INSERT ColorList(ColorCode,ColorName) VALUES('#00FFFF','Aqua')
INSERT ColorList(ColorCode,ColorName) VALUES('#7FFFD4','Aquamarine')
INSERT ColorList(ColorCode,ColorName) VALUES('#F0FFFF','Azure')
INSERT ColorList(ColorCode,ColorName) VALUES('#F5F5DC','Beige')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFE4C4','Bisque')
INSERT ColorList(ColorCode,ColorName) VALUES('#000000','Black')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFEBCD','Blanched Almond')
INSERT ColorList(ColorCode,ColorName) VALUES('#0000FF','Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#8A2BE2','Blue Violet')
INSERT ColorList(ColorCode,ColorName) VALUES('#A52A2A','Brown')
INSERT ColorList(ColorCode,ColorName) VALUES('#DEB887','Burly Wood')
INSERT ColorList(ColorCode,ColorName) VALUES('#5F9EA0','Cadet Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#7FFF00','Chartreuse')
INSERT ColorList(ColorCode,ColorName) VALUES('#D2691E','Chocolate')
INSERT ColorList(ColorCode,ColorName) VALUES('#FF7F50','Coral')
INSERT ColorList(ColorCode,ColorName) VALUES('#6495ED','Cornflower Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFF8DC','Cornsilk')
INSERT ColorList(ColorCode,ColorName) VALUES('#DC143C','Crimson')
INSERT ColorList(ColorCode,ColorName) VALUES('#00FFFF','Cyan')
INSERT ColorList(ColorCode,ColorName) VALUES('#00008B','Dark Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#008B8B','Dark Cyan')
INSERT ColorList(ColorCode,ColorName) VALUES('#B8860B','Dark Golden Rod')
INSERT ColorList(ColorCode,ColorName) VALUES('#A9A9A9','Dark Gray')
INSERT ColorList(ColorCode,ColorName) VALUES('#006400','Dark Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#BDB76B','Dark Khaki')
INSERT ColorList(ColorCode,ColorName) VALUES('#8B008B','Dark Magenta')
INSERT ColorList(ColorCode,ColorName) VALUES('#556B2F','Dark Olive Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#FF8C00','Dark Orange')
INSERT ColorList(ColorCode,ColorName) VALUES('#9932CC','Dark Orchid')
INSERT ColorList(ColorCode,ColorName) VALUES('#8B0000','Dark Red')
INSERT ColorList(ColorCode,ColorName) VALUES('#E9967A','Dark Salmon')
INSERT ColorList(ColorCode,ColorName) VALUES('#8FBC8F','Dark Sea Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#483D8B','Dark Slate Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#2F4F4F','Dark Slate Gray')
INSERT ColorList(ColorCode,ColorName) VALUES('#00CED1','Dark Turquoise')
INSERT ColorList(ColorCode,ColorName) VALUES('#9400D3','Dark Violet')
INSERT ColorList(ColorCode,ColorName) VALUES('#FF1493','Deep Pink')
INSERT ColorList(ColorCode,ColorName) VALUES('#00BFFF','Deep Sky Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#696969','Dim Gray')
INSERT ColorList(ColorCode,ColorName) VALUES('#1E90FF','Dodger Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#B22222','Fire Brick')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFFAF0','Floral White')
INSERT ColorList(ColorCode,ColorName) VALUES('#228B22','Forest Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#FF00FF','Fuchsia')
INSERT ColorList(ColorCode,ColorName) VALUES('#DCDCDC','Gainsboro')
INSERT ColorList(ColorCode,ColorName) VALUES('#F8F8FF','Ghost White')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFD700','Gold')
INSERT ColorList(ColorCode,ColorName) VALUES('#DAA520','Golden Rod')
INSERT ColorList(ColorCode,ColorName) VALUES('#808080','Gray')
INSERT ColorList(ColorCode,ColorName) VALUES('#008000','Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#ADFF2F','Green Yellow')
INSERT ColorList(ColorCode,ColorName) VALUES('#F0FFF0','Honey Dew')
INSERT ColorList(ColorCode,ColorName) VALUES('#FF69B4','Hot Pink')
INSERT ColorList(ColorCode,ColorName) VALUES('#CD5C5C','Indian Red ')
INSERT ColorList(ColorCode,ColorName) VALUES('#4B0082','Indigo ')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFFFF0','Ivory')
INSERT ColorList(ColorCode,ColorName) VALUES('#F0E68C','Khaki')
INSERT ColorList(ColorCode,ColorName) VALUES('#E6E6FA','Lavender')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFF0F5','Lavender Blush')
INSERT ColorList(ColorCode,ColorName) VALUES('#7CFC00','Lawn Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFFACD','Lemon Chiffon')
INSERT ColorList(ColorCode,ColorName) VALUES('#ADD8E6','Light Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#F08080','Light Coral')
INSERT ColorList(ColorCode,ColorName) VALUES('#E0FFFF','Light Cyan')
INSERT ColorList(ColorCode,ColorName) VALUES('#FAFAD2','Light Golden Rod Yellow')
INSERT ColorList(ColorCode,ColorName) VALUES('#D3D3D3','Light Gray')
INSERT ColorList(ColorCode,ColorName) VALUES('#90EE90','Light Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFB6C1','Light Pink')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFA07A','Light Salmon')
INSERT ColorList(ColorCode,ColorName) VALUES('#20B2AA','Light Sea Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#87CEFA','Light Sky Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#778899','Light Slate Gray')
INSERT ColorList(ColorCode,ColorName) VALUES('#B0C4DE','LightSteelBlue')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFFFE0','LightYellow')
INSERT ColorList(ColorCode,ColorName) VALUES('#00FF00','Lime')
INSERT ColorList(ColorCode,ColorName) VALUES('#32CD32','LimeGreen')
INSERT ColorList(ColorCode,ColorName) VALUES('#FAF0E6','Linen')
INSERT ColorList(ColorCode,ColorName) VALUES('#FF00FF','Magenta')
INSERT ColorList(ColorCode,ColorName) VALUES('#800000','Maroon')
INSERT ColorList(ColorCode,ColorName) VALUES('#66CDAA','Medium Aqua Marine')
INSERT ColorList(ColorCode,ColorName) VALUES('#0000CD','Medium Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#BA55D3','Medium Orchid')
INSERT ColorList(ColorCode,ColorName) VALUES('#9370DB','Medium Purple')
INSERT ColorList(ColorCode,ColorName) VALUES('#3CB371','Medium Sea Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#7B68EE','Medium Slate Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#00FA9A','Medium Spring Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#48D1CC','Medium Turquoise')
INSERT ColorList(ColorCode,ColorName) VALUES('#C71585','Medium Violet Red')
INSERT ColorList(ColorCode,ColorName) VALUES('#191970','Midnight Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#F5FFFA','Mint Cream')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFE4E1','Misty Rose')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFE4B5','Moccasin')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFDEAD','Navajo White')
INSERT ColorList(ColorCode,ColorName) VALUES('#000080','Navy')
INSERT ColorList(ColorCode,ColorName) VALUES('#FDF5E6','OldLace')
INSERT ColorList(ColorCode,ColorName) VALUES('#808000','Olive')
INSERT ColorList(ColorCode,ColorName) VALUES('#6B8E23','Olive Drab')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFA500','Orange')
INSERT ColorList(ColorCode,ColorName) VALUES('#FF4500','Orange Red')
INSERT ColorList(ColorCode,ColorName) VALUES('#DA70D6','Orchid')
INSERT ColorList(ColorCode,ColorName) VALUES('#EEE8AA','Pale Golden Rod')
INSERT ColorList(ColorCode,ColorName) VALUES('#98FB98','Pale Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#AFEEEE','Pale Turquoise')
INSERT ColorList(ColorCode,ColorName) VALUES('#DB7093','Pale Violet Red')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFEFD5','Papaya Whip')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFDAB9','Peach Puff')
INSERT ColorList(ColorCode,ColorName) VALUES('#CD853F','Peru')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFC0CB','Pink')
INSERT ColorList(ColorCode,ColorName) VALUES('#DDA0DD','Plum')
INSERT ColorList(ColorCode,ColorName) VALUES('#B0E0E6','Powder Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#800080','Purple')
INSERT ColorList(ColorCode,ColorName) VALUES('#FF0000','Red')
INSERT ColorList(ColorCode,ColorName) VALUES('#BC8F8F','Rosy Brown')
INSERT ColorList(ColorCode,ColorName) VALUES('#4169E1','Royal Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#8B4513','Saddle Brown')
INSERT ColorList(ColorCode,ColorName) VALUES('#FA8072','Salmon')
INSERT ColorList(ColorCode,ColorName) VALUES('#F4A460','Sandy Brown')
INSERT ColorList(ColorCode,ColorName) VALUES('#2E8B57','Sea Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFF5EE','Sea Shell')
INSERT ColorList(ColorCode,ColorName) VALUES('#A0522D','Sienna')
INSERT ColorList(ColorCode,ColorName) VALUES('#C0C0C0','Silver')
INSERT ColorList(ColorCode,ColorName) VALUES('#87CEEB','Sky Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#6A5ACD','Slate Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#708090','Slate Gray')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFFAFA','Snow')
INSERT ColorList(ColorCode,ColorName) VALUES('#00FF7F','Spring Green')
INSERT ColorList(ColorCode,ColorName) VALUES('#4682B4','Steel Blue')
INSERT ColorList(ColorCode,ColorName) VALUES('#D2B48C','Tan')
INSERT ColorList(ColorCode,ColorName) VALUES('#008080','Teal')
INSERT ColorList(ColorCode,ColorName) VALUES('#D8BFD8','Thistle')
INSERT ColorList(ColorCode,ColorName) VALUES('#FF6347','Tomato')
INSERT ColorList(ColorCode,ColorName) VALUES('#40E0D0','Turquoise')
INSERT ColorList(ColorCode,ColorName) VALUES('#EE82EE','Violet')
INSERT ColorList(ColorCode,ColorName) VALUES('#F5DEB3','Wheat')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFFFFF','White')
INSERT ColorList(ColorCode,ColorName) VALUES('#F5F5F5','White Smoke')
INSERT ColorList(ColorCode,ColorName) VALUES('#FFFF00','Yellow')
INSERT ColorList(ColorCode,ColorName) VALUES('#9ACD32','Yellow Green')
End
go

-- insert for Email content
IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] = 10)
BEGIN

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES ( N'Please complete your Appraisal form for the period from {From Date} to {To Date}', N'<div style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px; "><p class="MsoNormal">Dear {Employee Name},<o:p></o:p></p><p class="MsoNormal"><br></p><p class="MsoNormal">As part of {Organizations Name}’s appraisal process, you are requested to review your appraisal form for the period {From Date} to {To Date} . You should submit the appraisal form before {Due Date}.<br></p><p class="MsoNormal"><br><o:p></o:p></p><p class="MsoNormal">This is an automatic message from Rigo HRMS, please do not reply this message.<i><o:p></o:p></i></p><p class="MsoNormal"><i><br></i></p><p class="MsoNormal">Regards,<o:p></o:p></p><p class="MsoNormal">Human Resources Department<o:p></o:p></p><p class="MsoNormal"><span style="font-size: 11pt; line-height: 15.693333625793457px; font-family: Calibri, sans-serif;">{Organizations Name}</span></p></div>', 10, 2)
INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES (N'{Title Employee Name} has completed {his/her} appraisal form and requested your review', N'<div><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;">Dear {Supervisor Name},<o:p></o:p></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;"><br></p><p class="MsoNormal"><font face="tahoma, arial, helvetica, sans-serif">{Title Employee Name} has completed {his/her} appraisal form and requested your review</font></p><p class="MsoNormal"><br></p><p class="MsoNormal">This is an automatic message from Rigo HRMS, please do
not reply this message.<i><o:p></o:p></i></p><p class="MsoNormal"><i><br></i></p><p class="MsoNormal">Regards,<o:p></o:p></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;">Human Resources Department<o:p></o:p></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;"><span style="font-size: 11pt; line-height: 15.693333625793457px; font-family: Calibri, sans-serif;">{Organizations Name}</span></p></div>', 11, 2)
INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES (N'Your Appraisal Form has been reviewed by your supervisor.', N'<p class="MsoNormal">Dear {Employee Name},<o:p></o:p></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;"><span style="font-family: Helvetica;"><br></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;"><span style="font-family: Helvetica;">Your Appraisal Form has been reviewed by your supervisor.
Please review the supervisor’s comments and give your consent on supervisor’s
review. You have the option to agree or disagree on the supervisor’s review.
Please provide your comments for the disagreement, if any.</span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;"><br></p><p class="MsoNormal"><o:p></o:p></p><p class="MsoNormal">This is an automatic message from Rigo HRMS, please do
not reply this message.<i><o:p></o:p></i></p><p class="MsoNormal"><i><br></i></p><p class="MsoNormal">Regards,<o:p></o:p></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;">Human Resources Department<o:p></o:p></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;"><span style="font-size: 11pt; line-height: 15.693333625793457px; font-family: Calibri, sans-serif;">{Organizations Name}</span></p>', 12, 2)
INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES (N'Please Review {Employee Name}’s Appraisal Form', N'<div><span style="font-size:11.0pt;line-height:107%;
font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:
Calibri;mso-fareast-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Mangal;mso-bidi-theme-font:minor-bidi;mso-ansi-language:
EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA"><br></span></div><font face="Calibri, sans-serif"><span style="font-size: 15px; line-height: 15.693333625793457px;">Dear {Authority Name},</span></font><div><font face="Calibri, sans-serif"><span style="font-size: 15px; line-height: 15.693333625793457px;"><br></span></font><p class="MsoNormal">Please review {Employee Name}’s Appraisal form and put your
comment.<o:p></o:p></p><p class="MsoNormal"><br></p><p class="MsoNormal"><o:p></o:p></p><p class="MsoNormal">This is an automatic message from Rigo HRMS, please do
not reply this message.<i><o:p></o:p></i></p><p class="MsoNormal"><br></p><p class="MsoNormal">Regards,<br></p><p class="MsoNormal"><o:p></o:p></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;">Human Resources Department<o:p></o:p></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;"><span style="font-size: 11pt; line-height: 15.693333625793457px; font-family: Calibri, sans-serif;">{Organizations Name}</span></p></div>', 13, 2)


END
go

IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] = 15)
BEGIN
INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES ( N'Please Review {Employee Name}’s Travel Request', N'<div><br></div><div><font face="Calibri, sans-serif"><span style="font-size: 15px; line-height: 15.693333625793457px;">Dear {Authority Name},<br><br></span></font><div><p class="MsoNormal">Please review {Employee Name}’s Travel Request form and put your comment.<o:p></o:p></p><p class="MsoNormal"><span style="font-family: Helvetica; font-size: 12px;"><br>Regards,</span><br></p><p class="MsoNormal"><o:p></o:p></p><p class="MsoNormal" style="font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;">Human Resources Department<o:p></o:p></p><p class="MsoNormal" style="font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;"><span style="font-size: 11pt; line-height: 15.693333625793457px; font-family: Calibri, sans-serif;">{Organizations Name}</span></p></div></div>', 15, 2)
INSERT [dbo].[EmailContent] ( [Subject], [Body], [ContentType], [CompayId]) VALUES ( N'Travel Advance Settlement Notification', N'<div><br></div><div><font face="Calibri, sans-serif"><span style="font-size: 15px; line-height: 15.693333625793457px;">Dear&nbsp;</span></font>{Employee Name}<font face="Calibri, sans-serif"><span style="font-size: 15px; line-height: 15.693333625793457px;">,<br><br></span></font><div><p class="MsoNormal">You have received advance for your travel to&nbsp;<font face="Calibri, sans-serif"><span style="font-size: 15px; line-height: 15.6933336257935px;">&nbsp;</span></font>{Location}.&nbsp;</p><p class="MsoNormal">Please settle your advance as soon as you return from your travel.<o:p></o:p></p><p class="MsoNormal"><span style="font-family: Helvetica; font-size: 12px;"><br>Regards,</span><br></p><p class="MsoNormal"><o:p></o:p></p><p class="MsoNormal" style="font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;">Human Resources Department<o:p></o:p></p><p class="MsoNormal" style="font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;"><span style="font-size: 11pt; line-height: 15.693333625793457px; font-family: Calibri, sans-serif;">{Organizations Name}</span></p></div></div>', 21, 2)

END


go


IF NOT EXISTS(SELECT TOP 1 * FROM [FixedValueEducationLevel])
BEGIN

/****** Object:  Table [dbo].[FixedValueEducationLevel]    Script Date: 06/06/2014 14:53:54 ******/
INSERT [dbo].[FixedValueEducationLevel] ([ID], [Name]) VALUES (1, N'Master Degree')
INSERT [dbo].[FixedValueEducationLevel] ([ID], [Name]) VALUES (2, N'Graduate')
INSERT [dbo].[FixedValueEducationLevel] ([ID], [Name]) VALUES (3, N'MPhil')
INSERT [dbo].[FixedValueEducationLevel] ([ID], [Name]) VALUES (4, N'Intermediate / +2')
INSERT [dbo].[FixedValueEducationLevel] ([ID], [Name]) VALUES (5, N'School')
INSERT [dbo].[FixedValueEducationLevel] ([ID], [Name]) VALUES (6, N'Professional')
INSERT [dbo].[FixedValueEducationLevel] ([ID], [Name]) VALUES (7, N'Vocational')
/****** Object:  Table [dbo].[FixedValueEducationFaculty]    Script Date: 06/06/2014 14:53:54 ******/
INSERT [dbo].[FixedValueEducationFaculty] ([ID], [Name]) VALUES (1, N'Management')
INSERT [dbo].[FixedValueEducationFaculty] ([ID], [Name]) VALUES (2, N'Administration')
INSERT [dbo].[FixedValueEducationFaculty] ([ID], [Name]) VALUES (3, N'Engineering')
INSERT [dbo].[FixedValueEducationFaculty] ([ID], [Name]) VALUES (4, N'Humanities')
INSERT [dbo].[FixedValueEducationFaculty] ([ID], [Name]) VALUES (5, N'Science')

END

go

IF NOT EXISTS(SELECT TOP 1 * FROM [FixedValueFamilyRelation])
BEGIN
SET IDENTITY_INSERT [dbo].[FixedValueFamilyRelation] ON
INSERT [dbo].[FixedValueFamilyRelation] ([ID], [Name]) VALUES (1, N'Daughter')
INSERT [dbo].[FixedValueFamilyRelation] ([ID], [Name]) VALUES (2, N'Grandfather')
INSERT [dbo].[FixedValueFamilyRelation] ([ID], [Name]) VALUES (3, N'Son')
INSERT [dbo].[FixedValueFamilyRelation] ([ID], [Name]) VALUES (4, N'Spouse')
SET IDENTITY_INSERT [dbo].[FixedValueFamilyRelation] OFF
end

go

if not exists(select  * from MailMergeTemplateHeaders)
Begin

INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'Name', -2)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'EID', -1)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'Phone', 1)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'IM', 2)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'Address', 3)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'Address2', 4)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'Column_5', 5)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'Column_6', 6)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'Column_7', 7)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'Column_8', 8)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'Column_9', 9)
INSERT [dbo].[MailMergeTemplateHeaders] ([HeaderText], [Type]) VALUES (N'Column_10', 10)

End
go


DECLARE @LeaveTypeId INT

IF NOT EXISTS( SELECT * FROM dbo.LLeaveEncashmentIncome)
BEGIN
	
	DECLARE db_cursor CURSOR FAST_FORWARD FOR
			Select LeaveTypeId FROM dbo.LLeaveType WHERE UnusedLeave ='Encase' OR   UnusedLeave ='LapseEncase' 
	
	OPEN db_cursor
		FETCH NEXT FROM db_cursor INTO @LeaveTypeId
		WHILE @@FETCH_STATUS = 0
		BEGIN
	
		INSERT INTO dbo.LLeaveEncashmentIncome
		        ( LeaveId, IncomeId )
			SELECT @LeaveTypeId,IncomeId FROM dbo.PIncome
			WHERE dbo.PIncome.IsCountForLeaveEncasement=1
	
	
		FETCH NEXT FROM db_cursor INTO @LeaveTypeId
	END		  
	
	CLOSE db_cursor
	DEALLOCATE db_cursor

	
	
END


IF NOT EXISTS(SELECT TOP 1 * FROM [ExperienceCategory])
BEGIN
SET IDENTITY_INSERT [dbo].[ExperienceCategory] ON
INSERT [dbo].[ExperienceCategory] ([CategoryID], [Name]) VALUES (1, N'Banking')
INSERT [dbo].[ExperienceCategory] ([CategoryID], [Name]) VALUES (2, N'Non-Banking')
SET IDENTITY_INSERT [dbo].[ExperienceCategory] OFF
end

go

IF NOT EXISTS(SELECT TOP 1 * FROM [HBloodGroup])
BEGIN
SET IDENTITY_INSERT [dbo].[HBloodGroup] ON
INSERT [dbo].[HBloodGroup] ([Id], [BloodGroupName]) VALUES (1, N'A +ve')
INSERT [dbo].[HBloodGroup] ([Id], [BloodGroupName]) VALUES (2, N'A -ve')
INSERT [dbo].[HBloodGroup] ([Id], [BloodGroupName]) VALUES (3, N'AB +ve')
INSERT [dbo].[HBloodGroup] ([Id], [BloodGroupName]) VALUES (4, N'AB -ve')
INSERT [dbo].[HBloodGroup] ([Id], [BloodGroupName]) VALUES (5, N'B +ve')
INSERT [dbo].[HBloodGroup] ([Id], [BloodGroupName]) VALUES (6, N'B -ve')
INSERT [dbo].[HBloodGroup] ([Id], [BloodGroupName]) VALUES (7, N'O +ve')
INSERT [dbo].[HBloodGroup] ([Id], [BloodGroupName]) VALUES (8, N'O -ve')
SET IDENTITY_INSERT [dbo].[HBloodGroup] OFF
end

go

IF NOT EXISTS(SELECT TOP 1 * FROM [DocumentDocumentType])
BEGIN
SET IDENTITY_INSERT [dbo].[DocumentDocumentType] ON
INSERT [dbo].[DocumentDocumentType] ([DocumentTypeId], [DocumentTypeName]) VALUES (1, N'General')
INSERT [dbo].[DocumentDocumentType] ([DocumentTypeId], [DocumentTypeName]) VALUES (2, N'Certificate')
SET IDENTITY_INSERT [dbo].[DocumentDocumentType] OFF
end

go

IF NOT EXISTS(SELECT TOP 1 * FROM [HealthCondition])
BEGIN
SET IDENTITY_INSERT [dbo].[HealthCondition] ON
INSERT [dbo].[HealthCondition] ([ConditionTypeId], [ConditionTypeName]) VALUES (1, N'Symptom')
INSERT [dbo].[HealthCondition] ([ConditionTypeId], [ConditionTypeName]) VALUES (2, N'Contact')
INSERT [dbo].[HealthCondition] ([ConditionTypeId], [ConditionTypeName]) VALUES (3, N'Severe')
INSERT [dbo].[HealthCondition] ([ConditionTypeId], [ConditionTypeName]) VALUES (4, N'Cured')
SET IDENTITY_INSERT [dbo].[HealthCondition] OFF
end

go

IF NOT EXISTS(SELECT TOP 1 * FROM [CitizenNationality])
BEGIN
SET IDENTITY_INSERT [dbo].[CitizenNationality] ON
INSERT [dbo].[CitizenNationality] ([NationalityId], [Nationality]) VALUES (1, N'Nepali')
INSERT [dbo].[CitizenNationality] ([NationalityId], [Nationality]) VALUES (2, N'Indian')
SET IDENTITY_INSERT [dbo].[CitizenNationality] OFF
end

go

IF NOT EXISTS(SELECT TOP 1 * FROM [TrainingType])
BEGIN
SET IDENTITY_INSERT [dbo].[TrainingType] ON
INSERT [dbo].[TrainingType] ([TrainingTypeId], [TrainingTypeName]) VALUES (1, N'Organization Sponsored')
INSERT [dbo].[TrainingType] ([TrainingTypeId], [TrainingTypeName]) VALUES (2, N'Third Party Sponsored')
INSERT [dbo].[TrainingType] ([TrainingTypeId], [TrainingTypeName]) VALUES (3, N'Self Funded')
SET IDENTITY_INSERT [dbo].[TrainingType] OFF
end

go

IF NOT EXISTS(SELECT TOP 1 * FROM [PublicationType])
BEGIN
SET IDENTITY_INSERT [dbo].[PublicationType] ON
INSERT [dbo].[PublicationType] ([PublicationTypeId], [PublicationTypeName]) VALUES (1, N'Book')
INSERT [dbo].[PublicationType] ([PublicationTypeId], [PublicationTypeName]) VALUES (2, N'Research')
INSERT [dbo].[PublicationType] ([PublicationTypeId], [PublicationTypeName]) VALUES (3, N'Article')
INSERT [dbo].[PublicationType] ([PublicationTypeId], [PublicationTypeName]) VALUES (4, N'Report')
SET IDENTITY_INSERT [dbo].[PublicationType] OFF
end

go

IF NOT EXISTS(SELECT TOP 1 * FROM [HireMethod])
BEGIN
SET IDENTITY_INSERT [dbo].[HireMethod] ON
INSERT [dbo].[HireMethod] ([HireMethodId], [HireMethodName]) VALUES (1, N'Free Competition')
INSERT [dbo].[HireMethod] ([HireMethodId], [HireMethodName]) VALUES (2, N'Direct Hire')
INSERT [dbo].[HireMethod] ([HireMethodId], [HireMethodName]) VALUES (3, N'Dalit Kota')
SET IDENTITY_INSERT [dbo].[HireMethod] OFF
end

go

IF NOT EXISTS(SELECT TOP 1 * FROM [HReligion])
BEGIN
SET IDENTITY_INSERT [dbo].[HReligion] ON
INSERT [dbo].[HReligion] ([ReligionId], [ReligionName]) VALUES (1, N'Hindu')
INSERT [dbo].[HReligion] ([ReligionId], [ReligionName]) VALUES (2, N'Buddhism')
INSERT [dbo].[HReligion] ([ReligionId], [ReligionName]) VALUES (3, N'Atheist)')
SET IDENTITY_INSERT [dbo].[HReligion] OFF
end

go

IF NOT EXISTS(SELECT TOP 1 * FROM [HrFamilyOccupation])
BEGIN
SET IDENTITY_INSERT [dbo].[HrFamilyOccupation] ON
INSERT [dbo].[HrFamilyOccupation] ([OccupationId], [Occupation]) VALUES (1, N'Business')
INSERT [dbo].[HrFamilyOccupation] ([OccupationId], [Occupation]) VALUES (2, N'Government Job')
INSERT [dbo].[HrFamilyOccupation] ([OccupationId], [Occupation]) VALUES (3, N'Private Job')
INSERT [dbo].[HrFamilyOccupation] ([OccupationId], [Occupation]) VALUES (4, N'Bank Job')
INSERT [dbo].[HrFamilyOccupation] ([OccupationId], [Occupation]) VALUES (5, N'Agriculture')
INSERT [dbo].[HrFamilyOccupation] ([OccupationId], [Occupation]) VALUES (6, N'None')
INSERT [dbo].[HrFamilyOccupation] ([OccupationId], [Occupation]) VALUES (7, N'Teaching')
SET IDENTITY_INSERT [dbo].[HrFamilyOccupation] OFF
end

go

IF NOT EXISTS(SELECT TOP 1 * FROM [FixedValueDivision])
BEGIN
SET IDENTITY_INSERT [dbo].[FixedValueDivision] ON
INSERT [dbo].[FixedValueDivision] ([DivisionId], [Division]) VALUES (1, N'Distinction')
INSERT [dbo].[FixedValueDivision] ([DivisionId], [Division]) VALUES (2, N'First')
INSERT [dbo].[FixedValueDivision] ([DivisionId], [Division]) VALUES (3, N'Second')
INSERT [dbo].[FixedValueDivision] ([DivisionId], [Division]) VALUES (4, N'Third')
INSERT [dbo].[FixedValueDivision] ([DivisionId], [Division]) VALUES (5, N'Pass')
INSERT [dbo].[FixedValueDivision] ([DivisionId], [Division]) VALUES (6, N'Not Specified')
SET IDENTITY_INSERT [dbo].[FixedValueDivision] OFF
end

go


go

-- insert for Email content
IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] = 17)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N'Timesheet approval request for #Requester# for #YearMonth#','<div><font color="#333333" face="helvetica, arial, verdana, sans-serif"><span style="font-size: 13px;"><br></span></font></div><div><font color="#333333" face="helvetica, arial, verdana, sans-serif"><span style="font-size: 13px;">#Requester# has submmited the timesheet for the month #YearMonth# for Approval</span></font></div><div><font color="#333333" face="helvetica, arial, verdana, sans-serif"><span style="font-size: 13px;"><br></span></font></div><div><font color="#333333" face="helvetica, arial, verdana, sans-serif"><span style="font-size: 13px;">Regards</span></font></div><div><font color="#333333" face="helvetica, arial, verdana, sans-serif"><span style="font-size: 13px;">HR</span></font></div><div style="color: rgb(51, 51, 51); font-family: helvetica, arial, verdana, sans-serif; font-size: 13px;"><br></div>' , 17, @Company)

END

go


go

-- insert for Email content
IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] = 18)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N'Timesheet #Status# for the month #YearMonth#',
'<div><br></div><div>Your time sheet for the month &nbsp;#YearMonth# &nbsp;has been &nbsp;#Status#..<br></div><div><br></div><div>#Reason#<br></div><div><br></div><div><br></div><div>Regards</div><div>HR</div>' , 18, @Company)

END

go


go

-- insert for Email content
IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] = 20)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) 

VALUES ( N'Cancel Leave Request From  #RequesterName#', N'<font face="helvetica, arial, verdana, sans-serif" size="2" style="font-size: 13px;">Cancel Leave Request from #RequesterName#</font><br style="font-family: Helvetica;"><br style="font-family: Helvetica;"><b style="font-size: 13px;">From</b><strong style="font-size: 13px;">:</strong><font face="helvetica, arial, verdana, sans-serif" size="2" style="font-size: 13px;">&nbsp; &nbsp; &nbsp; #StartDate# To #EndDate#</font><br style="font-family: Helvetica;"><br style="font-family: Helvetica;"><b style="font-size: 13px;">Day(s)</b><font face="helvetica, arial, verdana, sans-serif" size="2" style="font-size: 13px;">: &nbsp; &nbsp;#Days#</font><br style="font-family: Helvetica;"><br style="font-family: Helvetica;"><b style="font-size: 13px;">Reason</b><strong style="font-size: 13px;">:</strong><font face="helvetica, arial, verdana, sans-serif" size="2" style="font-size: 13px;">&nbsp; #Reason#</font><div><font size="2"><br></font></div><div><b style="font-size: 13px;">Leave Type</b><strong style="font-size: 13px;">:</strong><font face="helvetica, arial, verdana, sans-serif" style="font-size: 13px;">&nbsp; #LeaveType#</font><font size="2"><br></font><div style="font-size: 13px;"><br></div><div style="font-size: 13px; font-family: Helvetica;"><font face="helvetica, arial, verdana, sans-serif" size="2">#RecommendedBy#</font><br></div><div style="font-size: 13px;"><br><a href="#Url#">Click here to navigate approval page</a></div></div>', 20, NULL)


END

go





-- For NIBL
IF NOT EXISTS(SELECT * FROM dbo.HR_Service_Event) And 
	cast((Select Value from CompanySetting where [key]='Company') as int) = 34
BEGIN

/****** Object:  Table [dbo].[HR_Service_Event]    Script Date: 04/24/2015 15:58:07 ******/
SET IDENTITY_INSERT [dbo].[HR_Service_Event] ON
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (1, N'Appointment', NULL, NULL, 1, 1, 1, 1, 1, 1,1)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (2, N'Re-Appointment', NULL, NULL, 1, 1, 1, 1, 1, 100,2)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (3, N'Termination', NULL, NULL, 0, 0, 0, 0, 1, 8,16)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (4, N'Disciplinary Action', NULL, NULL, 0, 0, 0, 0, 1, 9,4)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (5, N'Increment', NULL, NULL, 0, 0, 0, 0, 1, 12,5)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (6, N'Resign', NULL, NULL, 0, 0, 0, 0, 1, 23,16)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (7, N'Placement', NULL, NULL, 1, 1, 1, 1, 1, 13,40)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (8, N'Extension', NULL, NULL, 1, 1, 1, 1, 1, 14,40)
--INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (9, N'Extension', NULL, NULL, 1, 1, 1, 1, 1, 22)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (10, N'Contract Termination', NULL, NULL, 0, 0, 0, 0, 1, 24,16)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (11, N'Renewal', NULL, NULL, 0, 1, 1, 1, 1, 25,40)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (12, N'Promotion', NULL, NULL, 0, 1, 1, 1, 1, 3,12)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (13, N'Conf. Of The Position', NULL, NULL, 0, 0, 0, 1, 1, 20,40)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (15, N'Transfer', NULL, NULL, 1, 1, 0, 1, 0, 4,15)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (16, N'Retirement', NULL, NULL, 0, 0, 0, 0, 1, 5,16)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (17, N'Confirmation', NULL, NULL, 0, 1, 1, 1, 1, 2,40)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (18, N'Assignment', NULL, NULL, 0, 0, 0, 0, 0, 10,40)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (19, N'Demotion', NULL, NULL, 0, 1, 1, 1, 1, 7,19)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (20, N'Inter Branch Transfer without increment', NULL, NULL, 0, 1, 1, 1, 0, 11,15)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (21, N'Pay Stop', NULL, NULL, 0, 0, 0, 0, 1, 50,21)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (22, N'Study Leave (Unpaid)', NULL, NULL, 0, 0, 0, 0, 0, 51,40)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (23, N'Deployment', NULL, NULL, 0, 1, 1, 0, 1, 123,40)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (24, N'Annual Increment', NULL, NULL, 0, 0, 0, 0, 1, 16,5)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (25, N'Cash Reward', NULL, NULL, 0, 0, 0, 0, 0, 17,25)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (26, N'Inter Dept Transfer', NULL, NULL, 0, 1, 1, 0, 0, 101,15)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (27, N'Salary Revised', NULL, NULL, 0, 0, 0, 0, 1, 18,5)
SET IDENTITY_INSERT [dbo].[HR_Service_Event] OFF


END

go

-- For Care
IF NOT EXISTS(SELECT * FROM dbo.HR_Service_Event) And 
	cast((Select Value from CompanySetting where [key]='Company') as int) = 33
BEGIN


/****** Object:  Table [dbo].[HR_Service_Event]    Script Date: 04/24/2015 15:58:07 ******/
SET IDENTITY_INSERT [dbo].[HR_Service_Event] ON
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (1, N'Appointment', NULL, NULL, 1, 1, 1, 1, 1, 1,1)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (2, N'Re-Appointment', NULL, NULL, 1, 1, 1, 1, 1, 100,2)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (4, N'Disciplinary Action', NULL, NULL, 0, 0, 0, 0, 1, 9,4)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (5, N'Increment', NULL, NULL, 0, 0, 0, 0, 1, 12,5)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (11, N'Renewal', NULL, NULL, 0, 1, 1, 1, 1, 25,40)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (12, N'Promotion', NULL, NULL, 0, 1, 1, 1, 1, 3,12)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (15, N'Transfer', NULL, NULL, 1, 1, 0, 1, 0, 4,15)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (16, N'Retirement', NULL, NULL, 0, 0, 0, 0, 1, 8,16)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (17, N'Resignation', NULL, NULL, 0, 0, 0, 0, 1, 9,16)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (18, N'End of Contract', NULL, NULL, 0, 0, 0, 0, 1, 12,16)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (19, N'Retrenchment', NULL, NULL, 0, 0, 0, 0, 1, 23,16)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (20, N'Death', NULL, NULL, 1, 1, 1, 1, 1, 13,16)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (24, N'Annual Increment', NULL, NULL, 0, 0, 0, 0, 1, 16,5)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (26, N'Inter Dept Transfer', NULL, NULL, 0, 1, 1, 0, 0, 101,15)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence],GroupID) VALUES (27, N'Salary Revised', NULL, NULL, 0, 0, 0, 0, 1, 18,5)
SET IDENTITY_INSERT [dbo].[HR_Service_Event] OFF
END
go

go

-- For Other Company other than NIBL and Care
IF NOT EXISTS(SELECT * FROM dbo.HR_Service_Event) 
	And cast((Select Value from CompanySetting where [key]='Company') as int) <> 33
	And cast((Select Value from CompanySetting where [key]='Company') as int) <> 34
BEGIN


/****** Object:  Table [dbo].[HR_Service_Event]    Script Date: 04/24/2015 15:58:07 ******/
SET IDENTITY_INSERT [dbo].[HR_Service_Event] ON
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (1, N'Appointment', NULL, NULL, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (2, N'Re-Appointment', NULL, NULL, 1, 1, 1, 1, 1, 100)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (4, N'Disciplinary Action', NULL, NULL, 0, 0, 0, 0, 1, 9)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (5, N'Increment', NULL, NULL, 0, 0, 0, 0, 1, 12)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (11, N'Renewal', NULL, NULL, 0, 1, 1, 1, 1, 25)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (12, N'Promotion', NULL, NULL, 0, 1, 1, 1, 1, 3)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (15, N'Transfer', NULL, NULL, 1, 1, 0, 1, 0, 4)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (16, N'Retirement', NULL, NULL, 0, 0, 0, 0, 1, 8)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (17, N'Resignation', NULL, NULL, 0, 0, 0, 0, 1, 9)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (18, N'End of Contract', NULL, NULL, 0, 0, 0, 0, 1, 12)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (24, N'Annual Increment', NULL, NULL, 0, 0, 0, 0, 1, 16)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (26, N'Inter Dept Transfer', NULL, NULL, 0, 1, 1, 0, 0, 101)
INSERT [dbo].[HR_Service_Event] ([EventID], [Name], [CreatedBy], [CreatedOn], [Change_Branch], [Change_Department], [Change_Status], [Change_Designation], [Change_Payroll], [Sequence]) VALUES (27, N'Salary Revised', NULL, NULL, 0, 0, 0, 0, 1, 18)
SET IDENTITY_INSERT [dbo].[HR_Service_Event] OFF
END
go


-- For Other Company other than NIBL and Care
IF  cast((Select Value from CompanySetting where [key]='Company') as int) <> 33
	And cast((Select Value from CompanySetting where [key]='Company') as int) <> 34
BEGIN


UPDATE dbo.HR_Service_Event SET GroupID = 1 WHERE EventID=1
UPDATE dbo.HR_Service_Event SET GroupID = 2 WHERE EventID=2
UPDATE dbo.HR_Service_Event SET GroupID = 4 WHERE EventID=4
UPDATE dbo.HR_Service_Event SET GroupID = 5 WHERE EventID=5
UPDATE dbo.HR_Service_Event SET GroupID = 40 WHERE EventID=11
UPDATE dbo.HR_Service_Event SET GroupID = 12 WHERE EventID=12
UPDATE dbo.HR_Service_Event SET GroupID = 15 WHERE EventID=15
UPDATE dbo.HR_Service_Event SET GroupID = 16 WHERE EventID=16
UPDATE dbo.HR_Service_Event SET GroupID = 16 WHERE EventID=17
UPDATE dbo.HR_Service_Event SET GroupID = 16 WHERE EventID=18
UPDATE dbo.HR_Service_Event SET GroupID = 5 WHERE EventID=24
UPDATE dbo.HR_Service_Event SET GroupID = 15 WHERE EventID=26
UPDATE dbo.HR_Service_Event SET GroupID = 5 WHERE EventID=27
END
go

go

IF cast((Select Value from CompanySetting where [key]='Company') as int) = 33
BEGIN


UPDATE dbo.EHumanResource
SET RetirementType=17 WHERE RetirementType=1

UPDATE dbo.EHumanResource
SET RetirementType=18 WHERE RetirementType=2


End
go

UPDATE dbo.BranchDepartmentHistory
SET EventID = (CASE WHEN IsDepartmentTransfer=1 THEN 26 ELSE 15  End)
WHERE EventID IS NULL

go


UPDATE dbo.DesignationHistory
SET EventID = 12
WHERE EventID IS NULL

go

-- change rehire event id
UPDATE dbo.EmployeeServiceHistory
SET eventid = 2
WHERE EventID=101

-- change retired eventid
UPDATE dbo.EmployeeServiceHistory
SET eventid = 16
WHERE EventID=100

go


GO

UPDATE dbo.AttendanceCheckInCheckOut
SET [date]= CAST(datetime AS DATE)
WHERE [date] IS NULL


GO

---LateIn attendance Mail content




go
-- Timesheet set as Draft for PSI
delete FROM EmailContent WHERE [ContentType] =31
IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] =31)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N'Late in office (#TodayDate#)',
'<div>Dear #Employee# ,</div><br><div>You are late in office today (#TodayDate#) by #LateHour#.<br><br> Click link to enter late entry <br>#url#</div><div>Regards</div><div>HR</div>' , 31, @Company)

END



GO 

IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] =32)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N' Absent in office (#TodayDate#)',
'<div>Dear #Employee# ,</div><br><div>You are absent in office today (#TodayDate#)<br><br></div><div>Regards</div><div>HR</div>' , 32, @Company)

END




GO 

IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] =33)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N' Absent in office this week',
'<div>Dear #Employee# ,</div><br><div>You are absent in office(#Dates#)<br><br></div><div>Regards</div><div>HR</div>' , 33, @Company)

END


GO 

IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] =34)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N' List of employee Absent in office this week',
'<div>List of employee Absent in office this week ,</div><br><div>#List#<br><br></div><div>Regards,</div><div>HR</div>' , 34, @Company)

END

go

GO 

IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] =41)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N' Absent Employee List in office (#TodayDate#)',
'Following employees are absent on office today <div>#List#<br><br></div><div>Regards,</div><div>HR</div>' , 41, @Company)

END

go



-- update leave encasement TDS
UPDATE dbo.SettlementLeaveDetail
SET tds = (LeaveEncashmentAmount + ISNULL(RetirementAdjustment,0)) * 15.0 / 100.0
WHERE ISNULL(tds,0)=0




go

-- insert for Email content
IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] = 30)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N'Timesheet reverted for the month #YearMonth#',
'<div><br></div><div>Your time sheet for the month &nbsp;#YearMonth# &nbsp;has been Reverted.Please resubmit your timesheet.</div><div><br></div><div><br></div><div>Regards</div><div>HR</div>' , 30, @Company)

END

go

---time attendance comment 




go

-- insert for Email content
IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] = 35)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N'Late Attendance Request #Status# for  #Date#',
'<div><br></div><div>Your Late Attendance request for &nbsp;#Date# &nbsp;has been &nbsp;#Status#.<br></div><div><br></div><div>#Reason#<br></div><div><br></div><div></div><div>Regards</div><div>HR</div>' , 35, @Company)

END

go






go

-- insert for Email content FOR UserNameChanged
IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] = 5)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N'Payroll UserName changed',
'<div>Dear #Name#,</div><div><br></div><div><br></div><div>Your user name for the payroll has been changed to #UserName#.</div><div><br></div><div>You can login from the url : &lt;a href=''#Url#''&gt;#Url#&lt;/a&gt;</div>' , 5, @Company)

END

go

--



go

-- insert for Email content
IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] = 36)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N'Time Attendance Request #Status# for  #Date#',
'<div><br></div><div>Your Time Attendance request for &nbsp;#Date# &nbsp;has been &nbsp;#Status#.<br></div><div><br></div><div>#Reason#<br></div><div><br></div><div></div><div>Regards</div><div>HR</div>' , 36, @Company)

END

go





go

-- insert for Email content
IF NOT EXISTS(SELECT TOP 1 * FROM EmailContent WHERE [ContentType] = 37)
BEGIN

declare @Company int
set @Company = (select  top 1 CompanyId from Company)

INSERT [dbo].[EmailContent] ([Subject], [Body], [ContentType], [CompayId]) VALUES 
(N'Birthday on office',
'
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <meta content="width=device-width" name="viewport">

  <title>Rigo Technologies</title>
  <style type="text/css">
#outlook a {
    padding: 0;
    }
    .ReadMsgBody {
    width: 100%;
    }
    .ExternalClass {
    width: 100%;
    }
    .ExternalClass {
    line-height: 100%;
    }
    .ExternalClass p {
    line-height: 100%;
    }
    .ExternalClass span {
    line-height: 100%;
    }
    .ExternalClass font {
    line-height: 100%;
    }
    .ExternalClass td {
    line-height: 100%;
    }
    .ExternalClass div {
    line-height: 100%;
    }
    body {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    table {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    td {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    p {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    a {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    li {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    blockquote {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    table {
    mso-table-lspace: 0pt;
    mso-table-rspace: 0pt;
    }
    td {
    mso-table-lspace: 0pt;
    mso-table-rspace: 0pt;
    }
    img {
    -ms-interpolation-mode: bicubic;
    }
    body {
    margin: 0;
    padding: 0;
    }
    img {
    border: 0;
    height: auto;
    line-height: 100%;
    outline: none;
    text-decoration: none;
    vertical-align:middle;
    }
    table {
    border-collapse: collapse !important;
    }
    body {
    height: 100% !important;
    margin: 0;
    padding: 0;
    width: 100% !important;
    }
    #bodyTable {
    height: 100% !important;
    margin: 0;
    padding: 0;
    width: 100% !important;
    }
    #bodyCell {
    height: 100% !important;
    margin: 0;
    padding: 0;
    width: 100% !important;
    }
    @media only screen and (max-width: 640px) {
    body {
    width: auto!important;
    }
    table[class="container"] {
    width: 100%!important;
    padding-left: 20px!important;
    padding-right: 20px!important;
    }
    table[class="body"] {
    width: 100%!important;
    }
    table[class="row"] {
    width: 100% !important;
    }
    td[class="side-pad"] {
    padding-left: 20px !important;
    padding-right: 20px !important;
    }
    .img-responsive {
    width: 100% !important;
    }
    .img-center {
    margin: 0 auto !important;
    }
    }
    @media only screen and (max-width: 589px) {
    table[class="body"] .collapse {
    width: 100% !important;
    }
    table[class="body"] .column {
    width: 100% !important;
    display: block !important;
    }
    .center {
    text-align: center !important;
    }
    table[class="body"]
    }
    table[class="body"] .remove {
    display: none !important;
    }
    }
  </style>
  <style type="text/css">
body {
  background-color: #FFFFFF;
  }
  table.c14 {background-color: #FFFFFF}
  table.c13 {border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;}
  tr.c11 {background-color: #414242}
  td.c15 {font-size:10px; color:#6D7073; font-weight:normal; text-align:left; line-height:0px;}
  td.c10 {background-color: #ECF4FA}
  td.c9 {font-size:14px; color:#6D7073; font-weight:normal; text-align:left; line-height:0px;}
  td.c8 {background-color: #428BCA}
  td.c12 {background-color: #84b4dd}
  td.c7 {font-size:14px; color:#6D7073; font-weight:normal; text-align:left; line-height:20px;}
  td.c6 {font-size:18px; color:#6D7073; font-weight:normal; text-align:left;}
  td.c5 {background-color: #14709e;}
  td.c4 {background-color: #428BCA}
  table.c3 {mso-table-lspace:0;mso-table-rspace:0;}
  td.c2 {font-size:18px; line-height:22px; color:#ffffff; font-weight:normal; text-align:left;}
  td.c1 {background-color: #ECF4FA}
  td.c0 {background-color: #ECF4FA}
  a {text-decoration: none}
  </style>



  <br>

  <table class="c14" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #FFFFFF;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr>
      <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
        <table class="body c13" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="640">
          <tbody><tr>
            <td class="side-pad c0" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #ECF4FA;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" height="5">&nbsp;</td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>

          <tr>
            <td class="side-pad c4" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #428BCA;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                          <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody><tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center" height="10" valign="top">&nbsp;</td>
                            </tr>

                            <tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" valign="top">
                                <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody><tr>
                                    <td class="column" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="left" valign="top" width="280">
                                      <table class="collapse c3" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody><tr>
                                          <td class="center c2" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 18px;line-height: 22px;color: #ffffff;font-weight: normal;text-align: left;">Birthday in office today</td>
                                        </tr>

                                        <tr>
                                          <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" height="20">&nbsp;</td>
                                        </tr>
                                      </tbody></table>
                                    </td>
                                  </tr>
                                </tbody></table>
                              </td>
                            </tr>
                          </tbody></table>
                        </td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>

          <tr>
            <td class="side-pad c1" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #ECF4FA;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                          <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody><tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center" height="20" valign="top">&nbsp;</td>
                            </tr>

                            <tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" valign="top">
                                <table class="c3" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: collapse !important;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody><tr>
                                    <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" valign="top" width="100%">
                                      <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody><tr>
                                          <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                            <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tbody><tr>
                                                <td class="column" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="left" valign="top" width="100%">
                                                  <table class="collapse c3" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody><tr>
                                                      <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" valign="top" width="100%">
                                                        <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                          <tbody><tr>
                                                            <td class="c6" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 18px;color: #6D7073;font-weight: normal;text-align: left;" valign="top">Birthday on office today :<br><br></td>
                                                          </tr>

                                                          <tr>
                                                            <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" height="8" valign="top">{employeelist}&nbsp;</td>
                                                          </tr>

                                                          <tr>
                                                            <td class="c7" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 14px;color: #6D7073;font-weight: normal;text-align: left;line-height: 20px;" valign="top"><p style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;"><br>
                                                              <i>This is an automatic mail from
                                                              HRMS, please do not reply.</i><br>
                                                              <br>
                                                              _________________________<br>
                                                              Human Resource Department<br>
																  {Organizations Name}</p>
                                                            </td>
                                                          </tr>
                                                        </tbody></table>
                                                      </td>
                                                    </tr>
                                                  </tbody></table>
                                                </td>
                                              </tr>
                                            </tbody></table>
                                          </td>
                                        </tr>
                                      </tbody></table>
                                    </td>
                                  </tr>
                                </tbody></table>
                              </td>
                            </tr>

                            <tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" height="10"><br></td>
                            </tr>
                          </tbody></table>
                        </td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>

          <tr>
            <td class="side-pad c8" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #428BCA;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center" height="1" valign="top">&nbsp;</td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>

          <tr class="c12">
            <td class="side-pad c12" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #84b4dd;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center" height="1" valign="top">&nbsp;</td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>

          <tr class="c11" style="background-color: #414242;">
            <td class="side-pad c10" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #ECF4FA;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center" height="1" valign="top">&nbsp;</td>
                      </tr>

                      <tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" valign="middle">
                          <table class="collapse center" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="right" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br></td>

                              <td class="c15" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 10px;color: #6D7073;font-weight: normal;text-align: left;line-height: 0px;">
                                <img alt="Rigo Logo" src="http://rigonepal.com/rigo.png" style="-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;vertical-align: middle;" height="15" width="15"> Powered by <a href="http://rigonepal.com" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;text-decoration: none;">RIGO</a>
                              </td>
                            </tr>
                          </tbody></table>
                        </td>
                      </tr>

                      <tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" height="1">&nbsp;</td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>
        </tbody></table>
      </td>
    </tr>
  </tbody></table><br>' , 37, @Company)

END

go

-- insert for ActivityType if no record exists
IF NOT EXISTS(SELECT TOP 1 * FROM ActivityType)
BEGIN

INSERT [dbo].[ActivityType] ([Name]) VALUES('Other')
END

go

-- update script for employee activity detail
UPDATE
    NewActivityDetail
SET
    NewActivityDetail.EmployeeId = m.EmployeeId,
    NewActivityDetail.Date = m.Date,
    NewActivityDetail.DateEng = m.DateEng,
    NewActivityDetail.ClientSoftwareIds = d.ClientSoftwareId,
    NewActivityDetail.Status = m.Status
    
FROM
    NewActivityDetail d
INNER JOIN
    NewActivity m
ON 
    d.ActivityId = m.ActivityId
Where ClientSoftwareIds is null
go


go


if not exists(Select top 1 1 From EmailContent where ContentType = 40)
Begin

INSERT [dbo].[EmailContent] ( [Subject], [Body], [ContentType], [CompayId]) VALUES ( 
N'Training', N'<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <meta content="width=device-width" name="viewport">

  <title>Rigo Technologies</title>
  <style type="text/css">
#outlook a {
    padding: 0;
    }
    .ReadMsgBody {
    width: 100%;
    }
    .ExternalClass {
    width: 100%;
    }
    .ExternalClass {
    line-height: 100%;
    }
    .ExternalClass p {
    line-height: 100%;
    }
    .ExternalClass span {
    line-height: 100%;
    }
    .ExternalClass font {
    line-height: 100%;
    }
    .ExternalClass td {
    line-height: 100%;
    }
    .ExternalClass div {
    line-height: 100%;
    }
    body {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    table {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    td {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    p {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    a {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    li {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    blockquote {
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-family: "Arial", Helvetica, sans-serif;
    }
    table {
    mso-table-lspace: 0pt;
    mso-table-rspace: 0pt;
    }
    td {
    mso-table-lspace: 0pt;
    mso-table-rspace: 0pt;
    }
    img {
    -ms-interpolation-mode: bicubic;
    }
    body {
    margin: 0;
    padding: 0;
    }
    img {
    border: 0;
    height: auto;
    line-height: 100%;
    outline: none;
    text-decoration: none;
    vertical-align:middle;
    }
    table {
    border-collapse: collapse !important;
    }
    body {
    height: 100% !important;
    margin: 0;
    padding: 0;
    width: 100% !important;
    }
    #bodyTable {
    height: 100% !important;
    margin: 0;
    padding: 0;
    width: 100% !important;
    }
    #bodyCell {
    height: 100% !important;
    margin: 0;
    padding: 0;
    width: 100% !important;
    }
    @media only screen and (max-width: 640px) {
    body {
    width: auto!important;
    }
    table[class="container"] {
    width: 100%!important;
    padding-left: 20px!important;
    padding-right: 20px!important;
    }
    table[class="body"] {
    width: 100%!important;
    }
    table[class="row"] {
    width: 100% !important;
    }
    td[class="side-pad"] {
    padding-left: 20px !important;
    padding-right: 20px !important;
    }
    .img-responsive {
    width: 100% !important;
    }
    .img-center {
    margin: 0 auto !important;
    }
    }
    @media only screen and (max-width: 589px) {
    table[class="body"] .collapse {
    width: 100% !important;
    }
    table[class="body"] .column {
    width: 100% !important;
    display: block !important;
    }
    .center {
    text-align: center !important;
    }
    table[class="body"]
    }
    table[class="body"] .remove {
    display: none !important;
    }
    }
  </style>
  <style type="text/css">
body {
  background-color: #FFFFFF;
  }
  table.c14 {background-color: #FFFFFF}
  table.c13 {border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;}
  tr.c11 {background-color: #414242}
  td.c15 {font-size:10px; color:#6D7073; font-weight:normal; text-align:left; line-height:0px;}
  td.c10 {background-color: #ECF4FA}
  td.c9 {font-size:14px; color:#6D7073; font-weight:normal; text-align:left; line-height:0px;}
  td.c8 {background-color: #428BCA}
  td.c12 {background-color: #84b4dd}
  td.c7 {font-size:14px; color:#6D7073; font-weight:normal; text-align:left; line-height:20px;}
  td.c6 {font-size:18px; color:#6D7073; font-weight:normal; text-align:left;}
  td.c5 {background-color: #14709e;}
  td.c4 {background-color: #428BCA}
  table.c3 {mso-table-lspace:0;mso-table-rspace:0;}
  td.c2 {font-size:18px; line-height:22px; color:#ffffff; font-weight:normal; text-align:left;}
  td.c1 {background-color: #ECF4FA}
  td.c0 {background-color: #ECF4FA}
  a {text-decoration: none}
  </style>



  <br>

  <table class="c14" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #FFFFFF;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr>
      <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
        <table class="body c13" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="640">
          <tbody><tr>
            <td class="side-pad c0" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #ECF4FA;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" height="5">&nbsp;</td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>

          <tr>
            <td class="side-pad c4" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #428BCA;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                          <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody><tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center" height="10" valign="top">&nbsp;</td>
                            </tr>

                            <tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" valign="top">
                                <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody><tr>
                                    <td class="column" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="left" valign="top" width="280">
                                      <table class="collapse c3" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody><tr>
                                          <td class="center c2" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 18px;line-height: 22px;color: #ffffff;font-weight: normal;text-align: left;">Training Details</td>
                                        </tr>

                                        <tr>
                                          <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" height="20">&nbsp;</td>
                                        </tr>
                                      </tbody></table>
                                    </td>
                                  </tr>
                                </tbody></table>
                              </td>
                            </tr>
                          </tbody></table>
                        </td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>

          <tr>
            <td class="side-pad c1" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #ECF4FA;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                          <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody><tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center" height="20" valign="top">&nbsp;</td>
                            </tr>

                            <tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" valign="top">
                                <table class="c3" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: collapse !important;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody><tr>
                                    <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" valign="top" width="100%">
                                      <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody><tr>
                                          <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                            <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tbody><tr>
                                                <td class="column" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="left" valign="top" width="100%">
                                                  <table class="collapse c3" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0;mso-table-rspace: 0;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody><tr>
                                                      <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" valign="top" width="100%">
                                                        <table style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                          <tbody><tr>
                                                            <td class="c6" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 18px;color: #6D7073;font-weight: normal;text-align: left;" valign="top"></td></tr><tr><td class="c7" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(109, 112, 115); text-align: left; line-height: 20px;" valign="top"><span style="font-weight: normal;">Training Name :&nbsp;</span><b>#Name#<br></b>Training Period:&nbsp;<b>#Period#</b><br>Training Time:&nbsp;<b>#Time#</b><br>Venue:&nbsp;<b>#Venue#<br></b>Resource Person:&nbsp;<b>#ResourcePerson#</b><br><b style="font-weight: normal;"></b><p style="font-weight: normal; font-family: Arial, Helvetica, sans-serif;">Notes:<br>#Notes#</p><p style="font-weight: normal; font-family: Arial, Helvetica, sans-serif;"><br>
                                                              Human Resource Department</p></td></tr></tbody></table></td></tr></tbody></table>
                                                </td>
                                              </tr>
                                            </tbody></table>
                                          </td>
                                        </tr>
                                      </tbody></table>
                                    </td>
                                  </tr>
                                </tbody></table>
                              </td>
                            </tr>

                            <tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" height="10"><br></td>
                            </tr>
                          </tbody></table>
                        </td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>

          <tr>
            <td class="side-pad c8" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #428BCA;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center" height="1" valign="top">&nbsp;</td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>

          <tr class="c12">
            <td class="side-pad c12" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #84b4dd;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center" height="1" valign="top">&nbsp;</td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>

          <tr class="c11" style="background-color: #414242;">
            <td class="side-pad c10" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #ECF4FA;" align="center">
              <table class="container" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody><tr>
                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                    <table class="row" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                      <tbody><tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center" height="1" valign="top">&nbsp;</td>
                      </tr>

                      <tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" valign="middle">
                          <table class="collapse center" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="right" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                              <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><br></td>

                              <td class="c15" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 10px;color: #6D7073;font-weight: normal;text-align: left;line-height: 0px;">
                                Powered by <a href="http://rigonepal.com" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;text-decoration: none;">RIGO</a>
                              </td>
                            </tr>
                          </tbody></table>
                        </td>
                      </tr>

                      <tr>
                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: &quot;Arial&quot;, Helvetica, sans-serif;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" height="1">&nbsp;</td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>
        </tbody></table>
      </td>
    </tr>
  </tbody></table><br>', 40, NULL)


End

Go