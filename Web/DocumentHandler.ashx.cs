﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using BLL;
using BLL.Manager;
using Utils.Security;
using DAL;
namespace Web
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class DocumentHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                context.Response.Write("");
                context.Response.End();
                return;
            }


            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");

            //string path = context.Server.MapPath(context.Request.Url.AbsolutePath);
            string path = "";

            string id = context.Request.QueryString["id"];

            if (id == null)
                return;

            path = context.Server.MapPath("~/uploads/" + id + ".ashx");

            if (File.Exists(path))
            {
                HRManager mgr = new HRManager();

                EncryptorDecryptor enc = new EncryptorDecryptor();

                string name = "";
                string contentType = "";
                HDocument doc = mgr.GetDocument(Path.GetFileName(path));

                if (doc == null)
                {
                    DepositDocumentAttachment doc1 = HRManager.GetDepositDocument(Path.GetFileName(path));
                    if (doc1 != null)
                    {
                        //if emp access
                        if (SessionManager.CurrentLoggedInEmployeeId != 0)
                        {
                            context.Response.End();
                        }
                        contentType = doc1.ContentType;
                        name = doc1.Name;
                    }
                }
                else
                {
                    contentType = doc.ContentType;
                    name = doc.Name;
                }

                if (doc != null && SessionManager.CurrentLoggedInEmployeeId != 0 && SessionManager.CurrentLoggedInEmployeeId != doc.EmployeeId)
                {
                     context.Response.End();
                     return;
                }


                


                byte[] bytes = enc.DecryptAsStream(path);



                context.Response.Clear();
                context.Response.ClearHeaders();
                context.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
                // Response.AppendHeader("Content-Length", bytes.Length.ToString());
                context.Response.ContentType = contentType;
                context.Response.BinaryWrite(bytes);
                context.Response.End();


            }
        }
        
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
