﻿<%@ Page Title="Request Form" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="RequestForm.aspx.cs" Inherits="Web.DynamicForms.RequestForm" %>

<%@ Register Assembly="Utils" Namespace="Utils.Web" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .ddlGrid
        {
            font-style:normal;
        }
        
    </style>

     <script type="text/javascript" language="javascript">
         function EnableTextbox(ddl, txt) {
             var ddlControlType = document.getElementById(ddl);
             var txtControlValues = document.getElementById(txt);
             var ddl_Value = ddlControlType.value;
             if (ddl_Value == "1" || ddl_Value == "3") {
                 txtControlValues.disabled = true;
             }
             else {
                 txtControlValues.disabled = false;
             }
         }

        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<div class="innerLR">

<div class="widget">
<asp:HiddenField ID="hdnFormId" runat="server" />

            <div class="widget-head">
                <h4 class="heading">
                    Request Form</h4>
            </div>
            <div class="widget-body">
                
                <table>
                    <tr>
                        <td style="width:100px;">
                            Form Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtFormName" Width="200px" runat="server" />  
                            <asp:RequiredFieldValidator Display="None" ID="rfvFormName" runat="server" ValidationGroup="SaveDynamicForm"
                                ControlToValidate="txtFormName"  ErrorMessage="Form Name is required." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Show
                        </td>
                        <td>
                            <asp:CheckBox ID="chkShow" runat="server" />
                        </td>
                    </tr>
                </table>

                </br>
             <div class="clear" style="margin-top: 10px">
                    <cc2:EmptyDisplayGridView CssClass="tableLightColor" ShowHeaderWhenEmpty="True" 
                        ID="gvw" ClientIDMode="Static" 
                    runat="server" DataKeyNames="ControlID" AutoGenerateColumns="False" CellPadding="4"
                    GridLines="None" ShowFooterWhenEmpty="False" 
                    EnableModelValidation="True" onrowdeleting="gvw_RowDeleting" onrowdatabound="gvw_RowDataBound" >
                        <RowStyle BackColor="#E3EAEB" />
                                                                                                                                                                                                 <Columns>
                    <asp:TemplateField HeaderText="Label" HeaderStyle-Width="200px" HeaderStyle-CssClass="hiddenCol"
                        HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddenCol">
                        <ItemTemplate>
                            <asp:TextBox CssClass="item" ID="txtLabel" Width="200" runat="server" Text='<%# Eval("LabelName") %>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" CssClass="hiddenCol" Width="200px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Left" CssClass="hiddenCol"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Control Type" HeaderStyle-Width="130px" HeaderStyle-CssClass="hiddenCol"
                        HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddenCol">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlControlType" CssClass="ddlGrid" AppendDataBoundItems="true" Width="130px" runat="server">
                                <asp:ListItem Text="TextBox" Value="1" />
                                <asp:ListItem Text="DropDownList"  Value="2"/>
                                <asp:ListItem Text="CheckBox" Value="3" />
                                <asp:ListItem Text="CheckBoxList" Value="4" />
                                <asp:ListItem Text="RadioButtonList" Value="5" />
                            </asp:DropDownList>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" CssClass="hiddenCol" Width="130px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Left" CssClass="hiddenCol"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="100px" HeaderText="Is Compulsory">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlIsCompulsory" AppendDataBoundItems="true" Width="100px" runat="server">
                                <asp:ListItem Text="Yes" Value="Yes" />
                                <asp:ListItem Text="No" Value="No" />
                            </asp:DropDownList>
                        </ItemTemplate>
                        <HeaderStyle Width="100px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="300px" HeaderText="Control Values">
                        <ItemTemplate>
                            <asp:TextBox runat="server" CssClass="note" Width="300px" Style="text-align: left;
                                float: right;" ID="txtControlValues" Text='<%# Eval("ControlValues") %>'></asp:TextBox>
                        </ItemTemplate>
                        <HeaderStyle Width="300px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/retdelete.png" ShowCancelButton="False"                        ShowDeleteButton="True" />
                </Columns>
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <EmptyDataTemplate>
                        <b>No list. </b>
                    </EmptyDataTemplate>
                </cc2:EmptyDisplayGridView>
                 </div>
            </div>

            </br>
        <table>
            <tr>
                <td>
                    <div id="btnAddNewLine"  runat="server">
                        <asp:Button Text="+Add New Line" runat="server" ID="btnAddRow"  
                           onclick="btnAddRow_Click" 
                            ></asp:Button>
                    </div>
                </td>
            </tr>
        </table>

        </br>
         <div id="btnDiv" class="buttonsDiv" runat="server">
            <asp:Button Text="Save" runat="server" ID="btnSave" CausesValidation="true" OnClientClick="valGroup='SaveDynamicForm'; return CheckValidation();" 
                  onclick="btnSave_Click"
                ></asp:Button>
           
        </div>


</div>

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">

<script type="text/javascript" language="javascript">

    $(document).ready(function () {

        $(".ddlGrid").change(function () {

            if ($(this).val() == "1" || $(this).val() == "3") {
                $(this).parent().next().find("input:text").attr("disabled", true);
            }

            else {
                $(this).parent().next().find("input:text").attr("disabled", false);
            }
        });

    });
    
    </script>
</asp:Content>
