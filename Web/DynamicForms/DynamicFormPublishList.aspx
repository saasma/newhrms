﻿<%@ Page Title="Dynamic Form Publish List" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="DynamicFormPublishList.aspx.cs" Inherits="Web.DynamicForms.DynamicFormPublishList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.PublishID);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }
             
        
    </script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">
    
    <ext:Hidden runat="server" ID="hiddenValue" />
        <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
            <DirectEvents>
                <Click OnEvent="btnEdit_Click">
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:LinkButton>
        <ext:Button ID="btnDelete" runat="server" Hidden="true">
            <DirectEvents>
                <Click OnEvent="btnDelete_Click">
                    <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the form?" />
                    <EventMask ShowMask="true" />
                </Click>
            </DirectEvents>
        </ext:Button>

    <div class="separator bottom">
    </div>

       <div class="innerLR">
   
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Dynamic Form Publish List</h4>
            </div>
            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gvDynamicFormPublish" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeDynamicForm" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="PublishID">
                                    <Fields>
                                        <ext:ModelField Name="PublishID" Type="Int" />
                                        <ext:ModelField Name="FormName" Type="String" />
                                        <ext:ModelField Name="BranchName" Type="String" />
                                        <ext:ModelField Name="DepartmentName" Type="String" />
                                        <ext:ModelField Name="LevelName" Type="String" />
                                        <ext:ModelField Name="EmployeeName" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="colFormId" Sortable="false" MenuDisabled="true" runat="server" Text="Publish Id" Visible="false"
                                Width="10" Align="Left" DataIndex="PublishID" />

                            <ext:Column ID="colFormName" Sortable="false" MenuDisabled="true" runat="server" Text="Form Name"
                                Width="160" Align="Left" DataIndex="FormName" />

                            <ext:Column ID="colBranch" Sortable="false" MenuDisabled="true" runat="server" Text="Branch"
                                Width="160" Align="Left" DataIndex="BranchName" />

                            <ext:Column ID="colDepartment" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                                Width="160" Align="Left" DataIndex="DepartmentName" />

                            <ext:Column ID="colLevel" Sortable="false" MenuDisabled="true" runat="server" Text="Level"
                                Width="160" Align="Left" DataIndex="LevelName" />

                            <ext:Column ID="colEmployee" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                Width="160" Align="Left" DataIndex="EmployeeName" />

                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center" Width="80">
                                <Commands>                                   
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" ToolTip-Text="Edit" CommandName="Edit" />
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" ToolTip-Text="Delete" CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>

                <div class="buttonBlock">
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAdd" Cls="btnFlat"
                        BaseCls="btnFlat" Text="<i></i>Add New Dynamic Form Publish">
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </div>

            </div>
        </div>
        
    </div>
    <br />

    <ext:Window ID="WDynamicFormPub" runat="server" Title="Add/Edit Dynamic Publish Form Details" Icon="Application"
    Height="280" Width="800"  BodyPadding="10"
    Hidden="true" Modal="true">
    <Content>
        <table class="fieldTable">
            <td>
                <ext:ComboBox Hidden="false" QueryMode="Local" ID="cmbForm" FieldLabel="Form" LabelAlign="Left" 
                                runat="server" DisplayField="Name" Width="350" ValueField="FormID">
                                <Store>
                                    <ext:Store ID="storeForm" runat="server" >
                                    
                                        <Fields>
                                            <ext:ModelField Name="FormID"/>
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                                </Listeners>
                 </ext:ComboBox>
                <asp:RequiredFieldValidator Display="None" ID="rfvForm" runat="server" ValidationGroup="Save"
                    ControlToValidate="cmbForm"  ErrorMessage="Please select Form." />
            </td>
            <td>
                <ext:ComboBox Hidden="false" QueryMode="Local" ID="cmbBranch" LabelAlign="Left" FieldLabel="Branch" 
                                runat="server" DisplayField="Name" Width="350" ValueField="BranchId">
                                <Store>
                                    <ext:Store ID="storeBranch" runat="server" >
                                    
                                        <Fields>
                                            <ext:ModelField Name="BranchId"/>
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                 <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                                </Listeners>
                            </ext:ComboBox>
            </td>
            </tr>
            <tr>
                <td style="padding-top: 10px; padding-bottom: 10px;">

                   <ext:ComboBox Hidden="false" QueryMode="Local" ID="cmbDepartment" LabelAlign="Left" FieldLabel="Department" 
                                runat="server" DisplayField="Name" Width="350" ValueField="DepartmentId">
                                <Store>
                                    <ext:Store ID="storeDepartment" runat="server" >
                                    
                                        <Fields>
                                            <ext:ModelField Name="DepartmentId"/>
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                 <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                                </Listeners>
                            </ext:ComboBox>
                </td>
                <td>
                    
                    <ext:ComboBox Hidden="false" QueryMode="Local" ID="cmbLevel" LabelAlign="Left" FieldLabel="Level" 
                                runat="server" DisplayField="GroupLevel" Width="350" ValueField="LevelId">
                                <Store>
                                    <ext:Store ID="storeLevel" runat="server" >
                                    
                                        <Fields>
                                            <ext:ModelField Name="LevelId"/>
                                            <ext:ModelField Name="GroupLevel" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                 <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                                </Listeners>
                            </ext:ComboBox>

                </td>
            </tr>

            <tr>
                <td>
                    <ext:ComboBox Hidden="false" QueryMode="Local" ID="cmbEmployee" LabelAlign="Left" FieldLabel="Employee Name" 
                                runat="server" DisplayField="Name" Width="350" ValueField="EmployeeId">
                                <Store>
                                    <ext:Store ID="storeEmployee" runat="server" >
                                    
                                        <Fields>
                                            <ext:ModelField Name="EmployeeId"/>
                                            <ext:ModelField Name="Name" />
                                        </Fields>
                                    </ext:Store>
                                </Store>
                                 <Triggers>
                                    <ext:FieldTrigger Icon="Clear" HideTrigger="true" />
                                </Triggers>
                                <Listeners>
                                    <Select Handler="this.getTrigger(0).show();" />
                                    <BeforeQuery Handler="this.getTrigger(0)[this.getRawValue().toString().length == 0 ? 'hide' : 'show']();" />
                                    <TriggerClick Handler="if (index == 0) { 
                                                               this.clearValue(); 
                                                               this.getTrigger(0).hide();
                                                           }" />
                                </Listeners>
                            </ext:ComboBox>
                </td>

                <td>
                </td>

            </tr>
            
            <tr>
                <td valign="bottom" colspan="2">

                    <div class="popupButtonDiv">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnSave" Cls="btnFlat"
                            BaseCls="btnFlat" Text="<i></i>Save">
                            <DirectEvents>
                                <Click OnEvent="btnSave_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                            <Listeners>
                                <Click Handler="valGroup = 'Save'; return CheckValidation();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                        <div class="btnFlatOr">
                            or
                        </div>
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="LinkButton2" Cls="btnFlatLeftGap"
                            Text="<i></i>Cancel">
                            <Listeners>
                                <Click Handler="#{WDynamicFormPub}.hide();">
                                </Click>
                            </Listeners>
                        </ext:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</ext:Window>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
