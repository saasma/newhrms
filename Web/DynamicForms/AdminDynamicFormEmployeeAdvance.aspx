﻿<%@ Page Title="Dynamic Form Employee Advance" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="AdminDynamicFormEmployeeAdvance.aspx.cs" Inherits="Web.DynamicForms.AdminDynamicFormEmployeeAdvance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.FormID);
            <%= hiddenValueFormEmpId.ClientID %>.setValue(record.data.DFEID);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
             }
             
        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">


<ext:Hidden runat="server" ID="hiddenValue" />
<ext:Hidden runat="server" ID="hiddenValueFormEmpId"></ext:Hidden>
<ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>


    <div class="separator bottom">
        </div>
        <div class="innerLR">
            <h4 class="heading">
             Dynamic Form Employee for Advance</h4>

        <table class="fieldTable firsttdskip">
                <tr>
                    
                    <td>
                        <ext:ComboBox FieldLabel="Form Name" ID="cmbForm" Width="180px" runat="server" ValueField="FormID"
                            DisplayField="FormName" LabelAlign="Top" LabelSeparator="" ForceSelection="true"
                            QueryMode="Local">
                            <Store>
                                <ext:Store ID="Store2" runat="server">
                                    <Model>
                                        <ext:Model ID="Model2" IDProperty="StepID" runat="server">
                                            <Fields>
                                                <ext:ModelField Name="FormID" />
                                                <ext:ModelField Name="FormName" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <%--<DirectEvents>
                                <Select OnEvent="FormName_Change">
                                    <EventMask ShowMask="true" />
                                </Select>
                            </DirectEvents>--%>
                        </ext:ComboBox>
                    </td>

                    <td>
                        <ext:ComboBox FieldLabel="Status" ID="cmbStatus" Width="180px" runat="server" LabelAlign="Top"
                            LabelSeparator="" ForceSelection="true" QueryMode="Local">
                            <Items>
                                <ext:ListItem Text="All" Value="-1" />
                                <ext:ListItem Text="Advance Not Set" Value="0" />
                                <ext:ListItem Text="Advance Set" Value="1" />
                                <ext:ListItem Text="Employee Settled" Value="2" />
                                <ext:ListItem Text="HR Settled" Value="3" />
                            </Items>
                            <SelectedItems>
                                <ext:ListItem Index="0" />
                            </SelectedItems>
                        </ext:ComboBox>
                    </td>
                    <td style="padding-top: 20px">
                        <ext:LinkButton runat="server" StyleSpec="padding:0px;margin-top:20px;" ID="btnLoad"
                            Cls="btnFlat" BaseCls="btnFlat" Text="<i></i>Load" runat="server">
                            <DirectEvents>
                                <Click OnEvent="btnLoad_Click">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>
                        </ext:LinkButton>
                    </td>
                </tr>
            </table>

            <div class="widget-body" style="width: auto;">
               <ext:GridPanel StyleSpec="margin-top:15px;" ID="gvDynamicFormEmployee" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeDynamicFormEmployee" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="DFEID">
                                    <Fields>
                                         <ext:ModelField Name="DFEID" Type="Int" />
                                         
                                        <ext:ModelField Name="FormID" Type="Int" />
                                        <ext:ModelField Name="DateEng" Type="Date" />
                                        <ext:ModelField Name="Name" Type=String />
                                        <ext:ModelField Name="EmpName" Type="String" />
                                        <ext:ModelField Name="Position" Type="String" />
                                        <ext:ModelField Name="Department" Type="String" />
                                        <ext:ModelField Name="StatusName" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="colDFEID" Sortable="false" MenuDisabled="true" runat="server" Visible="false"
                                Width="100" Align="Left" DataIndex="DFEID" />

                            <ext:Column ID="colFormId" Sortable="false" MenuDisabled="true" runat="server" Text="Form Id"
                                Width="100" Align="Left" DataIndex="FormID" />            

                            <ext:DateColumn ID="DateColumn1" runat="server" Text="Date" Sortable="true" DataIndex="DateEng"
                                Width="100" Align="Left" Format="MM/dd/yyyy">
                            </ext:DateColumn>

                            <ext:Column ID="colFormName" Sortable="false" MenuDisabled="true" runat="server" Text="Form Name"
                                Width="100" Align="Left" DataIndex="Name" />                                         

                             <ext:Column ID="colEmployeeName" Sortable="false" MenuDisabled="true" runat="server" Text="Employee Name"
                                Width="150" Align="Left" DataIndex="EmpName" />

                            <ext:Column ID="colPosition" Sortable="false" MenuDisabled="true" runat="server" Text="Position"
                                Width="100" Align="Left" DataIndex="Position" />

                            <ext:Column ID="colDepartment" Sortable="false" MenuDisabled="true" runat="server" Text="Department"
                                Width="100" Align="Left" DataIndex="Department" />

                            <ext:Column ID="colStatus" Sortable="false" MenuDisabled="true" runat="server" Text="Status"
                                Width="150" Align="Left" DataIndex="StatusName" />

                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center" Width="150">
                                <Commands>
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" CommandName="Edit" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>
            </div>



    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
