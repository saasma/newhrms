﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.DynamicForms
{
    public partial class DynamicFormApprovalFlow : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        public void Initialise()
        {
            storeTravelOrder.DataSource = TravelAllowanceManager.getTravelOrderListForCombo();
            storeTravelOrder.DataBind();

            storeAuthorityType.DataSource = TravelAllowanceManager.getAuthorityTypeListForCombo((int)FlowTypeEnum.Appraisal);
            storeAuthorityType.DataBind();

            storeEmployee.DataSource = EmployeeManager.GetAllEmployees();
            storeEmployee.DataBind();

            storeDesignation.DataSource = new CommonManager().GetAllDesignations();
            storeDesignation.DataBind();

            if (Request.QueryString["formId"] != null)
            {
                hiddenFormId.Value = Request.QueryString["formId"].ToString();
                BindGrid();
            }

        }

        private int GetFormId()
        {
            return hiddenFormId.Value.ToString() == "" ? 0 : int.Parse(hiddenFormId.Value.ToString());
        }

        private void BindGrid()
        {
            gridDynamicApproval.GetStore().DataSource = DynamicFormManager.GetDynamicApprovalFlow(GetFormId());
            gridDynamicApproval.GetStore().DataBind();
        }  
      
        public void ClearLevelFields()
        {
            hiddenValue.Text = "";
        }       

        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            List<DAL.DynamicApprovalFlow> lines = new List<DynamicApprovalFlow>();

            string json = e.ExtraParams["GridValues"];
            if (string.IsNullOrEmpty(json))
            {
                return;
            }
            Status isValidToInsert = new Status();

            lines = JSON.Deserialize<List<DAL.DynamicApprovalFlow>>(json);
            for (int i = 0; i < lines.Count; i++)
            {
                
                if (string.IsNullOrEmpty(lines[i].StepID.ToString().Trim()))
                {
                    isValidToInsert.IsSuccess = false;
                    isValidToInsert.ErrorMessage = "Step can not be empty for the row " + (i + 1) + ". Please remove empty rows.";
                }

                if (string.IsNullOrEmpty(lines[i].AuthorityType.ToString().Trim()))
                {
                    isValidToInsert.IsSuccess = false;
                    isValidToInsert.ErrorMessage = "Authority can not be empty for the row " + (i + 1) + ". Please remove empty rows.";
                }

                if (string.IsNullOrEmpty(lines[i].StepName.ToString().Trim()))
                {
                    isValidToInsert.IsSuccess = false;
                    isValidToInsert.ErrorMessage = "Step name can not be empty for the row " + (i + 1) + ". Please remove empty rows.";
                }

                if (string.IsNullOrEmpty(lines[i].AuthorityTypeDisplayName.ToString().Trim()))
                {
                    isValidToInsert.IsSuccess = false;
                    isValidToInsert.ErrorMessage = "Authority display name can not be empty for the row " + (i + 1) + ". Please remove empty rows.";
                }
                //if (i != 0 && lines[i].StepID != null && lines[i - 1].StepID != null)
                //{
                //    if (lines[i].StepID < lines[i - 1].StepID)
                //    {
                //        isValidToInsert.IsSuccess = false;
                //        isValidToInsert.ErrorMessage = "Steps are not in Correct Assending Order.";
                //    }
                //}


            }

            for (int i = 0; i < lines.Count; i++)
            {
                for (int j = i + 1; j < lines.Count; j++)
                {
                    if (lines[i].StepID > lines[j].StepID)
                    {
                        isValidToInsert.IsSuccess = false;
                        isValidToInsert.ErrorMessage = "Step sequence must be maintained.";
                    }
                }
            }

            if (lines.Count > 0)
            {
                if (lines[0].StepID != (int)FlowStepEnum.Step1SaveAndSend)
                {
                    isValidToInsert.IsSuccess = false;
                    isValidToInsert.ErrorMessage = "First step must be Step 1.";
                }
                if (lines[lines.Count-1].StepID != (int)FlowStepEnum.Step15End)
                {
                    isValidToInsert.IsSuccess = false;
                    isValidToInsert.ErrorMessage = "Last step must be Step 10.";
                }
            }

            int row =1;
            foreach (DAL.DynamicApprovalFlow item in lines)
            {
                item.FormID = GetFormId();
                if (item.AuthorityType != 6)
                {
                    item.Person1ID = null;
                    item.Person2ID = null;
                }

                if (item.AuthorityType == 6 &&
                    (item.Person1ID == null && item.Person2ID == null))
                {
                    isValidToInsert.ErrorMessage = "For \"Specific Person\", Employee 1 or Employee 2 must be selected for the row " + row+ ".";
                    isValidToInsert.IsSuccess = false;
                }

                item.SequenceNo = lines.IndexOf(item) + 1;
                int count = lines.Where(x => x.StepID == item.StepID).Count();
                if (count > 1)
                {
                    isValidToInsert.ErrorMessage = "Steps can be repeated. Please Check";
                    isValidToInsert.IsSuccess = false;
                }

                row += 1;
            }
            if (isValidToInsert.IsSuccess)
            {
                Status status = DynamicFormManager.UpdateDynamicApprovalFlow(lines, GetFormId());

                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Dynamic Approval Flow saved");
                    BindGrid();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
            else
            {
                NewMessage.ShowWarningMessage(isValidToInsert.ErrorMessage);
            }

        }

    }
}
