﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.DynamicForms
{
    public partial class DynamicFormList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                LoadDynamicForms();
            }            
        }

        private void LoadDynamicForms()
        {
            gvDynamicForm.GetStore().DataSource = DynamicFormManager.GetDynamicForms();
            gvDynamicForm.GetStore().DataBind();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int formId = int.Parse(hiddenValue.Text.Trim());
            Status status = DynamicFormManager.DeleteDynamicForm(formId);
            if (status.IsSuccess)
            {
                LoadDynamicForms();
                NewMessage.ShowNormalMessage("Dynamic form deleted");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int formId = int.Parse(hiddenValue.Text.Trim());
            Response.Redirect("~/DynamicForms/DynamicRequestForm.aspx?formId=" + formId.ToString());
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("~/DynamicForms/DynamicRequestForm.aspx");
        }

    }
}