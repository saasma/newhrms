﻿<%@ Page Title="Dynamic Form List" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="DynamicFormList.aspx.cs" Inherits="Web.DynamicForms.DynamicFormList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">

        var CommandHandler1 = function(command, record){
            <%= hiddenValue.ClientID %>.setValue(record.data.FormID);
                if(command=="Edit")
                {
                    <%= btnEdit.ClientID %>.fireEvent('click');
                }
                else if(command == "Load")
                {
                    window.location = "DynamicRequestedForm.aspx?formId="+record.data.FormID;
                }
                else if(command == "Approval")
                {
                    window.location = "DynamicFormApprovalFlow.aspx?formId=" + record.data.FormID;
                }
                else
                {
                    <%= btnDelete.ClientID %>.fireEvent('click');
                }

             }
             
        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<ext:Hidden runat="server" ID="hiddenValue" />
    <ext:LinkButton runat="server" Hidden="true" ID="btnEdit">
        <DirectEvents>
            <Click OnEvent="btnEdit_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>
    <ext:Button ID="btnDelete" runat="server" Hidden="true">
        <DirectEvents>
            <Click OnEvent="btnDelete_Click">
                <Confirmation ConfirmRequest="true" Message="Are you sure, you want to delete the form?" />
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:Button>

    <div class="separator bottom">
    </div>

      <div class="innerLR">
   
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Dynamic Form List</h4>
            </div>
            <div class="widget-body">
                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gvDynamicForm" runat="server" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeDynamicForm" runat="server">
                            <Model>
                                <ext:Model ID="Model4" runat="server" IDProperty="FormID">
                                    <Fields>
                                        <ext:ModelField Name="FormID" Type="Int" />
                                        <ext:ModelField Name="Name" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel>
                        <Columns>
                            <ext:Column ID="colFormId" Sortable="false" MenuDisabled="true" runat="server" Text="Form Id"
                                Width="100" Align="Left" DataIndex="FormID" />

                            <ext:Column ID="Column1" Sortable="false" MenuDisabled="true" runat="server" Text="Form Name"
                                Width="400" Align="Left" DataIndex="Name" />

                            <ext:CommandColumn ID="CommandColumn1" runat="server" Text="Actions" Align="Center" Width="250">
                                <Commands>
                                    <ext:GridCommand Text="Preview" CommandName="Load" Cls="item"/>
                                    <ext:GridCommand Text="Approval Flow" CommandName="Approval" Cls="item" />
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons pencil btn-success" ToolTip-Text="Edit" CommandName="Edit" />
                                    <ext:GridCommand Text="<i></i>" Cls="btn-action glyphicons remove_2 btn-danger" ToolTip-Text="Delete" CommandName="Delete" />
                                </Commands>
                                <Listeners>
                                    <Command Handler="CommandHandler1(command,record);" />
                                </Listeners>
                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" Mode="Single" />
                    </SelectionModel>
                </ext:GridPanel>

                <div class="buttonBlock">
                    <ext:LinkButton runat="server" StyleSpec="padding:0px;" ID="btnAdd" Cls="btnFlat"
                        BaseCls="btnFlat" Text="<i></i>Add New Dynamic Form">
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                <EventMask ShowMask="true" />
                            </Click>
                        </DirectEvents>
                    </ext:LinkButton>
                </div>

            </div>
        </div>
        
    </div>
    <br />

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
