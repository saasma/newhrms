﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.DynamicForms
{
    public partial class DynamicFormPublishList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            storeForm.DataSource = DynamicFormManager.GetDynamicForms();
            storeForm.DataBind();

            storeBranch.DataSource = DynamicFormManager.GetBranches();
            storeBranch.DataBind();

            storeDepartment.DataSource = DynamicFormManager.GetDepartments();
            storeDepartment.DataBind();

            storeLevel.DataSource = DynamicFormManager.GetAllLevels();
            storeLevel.DataBind();

            storeEmployee.DataSource = DynamicFormManager.GetAllEmployees();
            storeLevel.DataBind();

            BindGrid();
        }

        private void ClearFields()
        {
            cmbForm.Value = "";
            cmbBranch.Value = "";
            cmbDepartment.Value = "";
            cmbLevel.Value = "";
            cmbEmployee.Value = "";
        }

        private int GetPublishID()
        {
            return hiddenValue.Value.ToString() == "" ? 0 : int.Parse(hiddenValue.Value.ToString());
        }

        private void BindGrid()
        {
            gvDynamicFormPublish.GetStore().DataSource = DynamicFormManager.GetAllDynamicFormPublish();
            gvDynamicFormPublish.GetStore().DataBind();
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
            WDynamicFormPub.Show();
        }

        protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            DynamicFormPublish entity = DynamicFormManager.GetDynamicFormPublishUsingID(GetPublishID());
            Status status = DynamicFormManager.DeleteDynamicFormPublish(entity);
            if (status.IsSuccess)
            {
                BindGrid();
                NewMessage.ShowNormalMessage("Dynamic form deleted");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            ClearFields();
           
            DynamicFormPublish entity = DynamicFormManager.GetDynamicFormPublishUsingID(GetPublishID());

            cmbForm.SetValue(entity.FormID);

            cmbForm.Disabled = true;

            cmbBranch.SetValue(entity.BranchId);
            cmbDepartment.SetValue(entity.DepartmentId);
            cmbLevel.SetValue(entity.LevelId);
            cmbEmployee.SetValue(entity.EmployeeId);

            WDynamicFormPub.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Page.Validate("Save");
            if (Page.IsValid)
            {
                DynamicFormPublish entity = new DynamicFormPublish();
                entity.FormID = int.Parse(cmbForm.SelectedItem.Value);

                if(!string.IsNullOrEmpty(cmbBranch.SelectedItem.Value))
                    entity.BranchId = int.Parse(cmbBranch.SelectedItem.Value);

                if(!string.IsNullOrEmpty(cmbDepartment.SelectedItem.Value))
                    entity.DepartmentId = int.Parse(cmbDepartment.SelectedItem.Value);

                if(!string.IsNullOrEmpty(cmbLevel.SelectedItem.Value))
                    entity.LevelId = int.Parse(cmbLevel.SelectedItem.Value);

                if(!string.IsNullOrEmpty(cmbEmployee.SelectedItem.Value))
                    entity.EmployeeId = int.Parse(cmbEmployee.SelectedItem.Value);

                entity.CreatedBy = Guid.NewGuid();
                entity.CreatedOn = System.DateTime.Now;

                if (GetPublishID() != 0)
                    entity.PublishID = GetPublishID();

                Status status = DynamicFormManager.SaveUpdateDynamicFormPublish(entity);
                if (status.IsSuccess)
                {
                    WDynamicFormPub.Hide();
                    BindGrid();
                    NewMessage.ShowNormalMessage("Dynamic Form Publish saved.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }

            }
        }
    }
}