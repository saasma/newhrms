﻿<%@ Page Title="Dynamic Requested Form" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="DynamicRequestedForm.aspx.cs" Inherits="Web.DynamicForms.DynamicRequestedForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .tblDynamic
    {
        margin-left:20px;
        padding-bottom:5px;
    }
    
    .btnSave
    {
        margin-left:20px;
        padding-left:5px;
        padding-right:5px;
    }
    
    .rbl input[type="radio"]
    {
       margin-left: 10px;
       margin-right: 2px;
    }
    
    .chk input[type="checkbox"]
    {
       margin-left: 10px;
       margin-right: 2px;
    }
   
</style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<div class="widget-head">
                <h4 class="heading">
                    <div style="margin-left:20px;">
                        <asp:Label ID="lblFormName" Text="" runat="server" />
                         </div>
                </h4>
                   
            </div>
            <div class="widget-body">
            <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:Panel ID="pnl" runat="server">


                </asp:Panel>

                </br>

                <div id="btnDiv" class="buttonsDiv" runat="server">
                    <asp:Button Text="Save" runat="server" ID="btnSave" CausesValidation="true" CssClass="btnSave"
                        onclick="btnSave_Click" OnClientClick="valGroup='DynamicForm'; return CheckValidation();"
                        ></asp:Button>
           
                </div>


    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
