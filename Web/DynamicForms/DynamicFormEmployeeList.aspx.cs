﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.DynamicForms
{
    public partial class DynamicFormEmployeeList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                LoadDynamicFormEmployees();
            }
        }

        private void LoadDynamicFormEmployees()
        {
            gvDynamicFormEmployee.GetStore().DataSource = DynamicFormManager.GetDynamicFormEmpList(SessionManager.CurrentLoggedInEmployeeId);
            gvDynamicFormEmployee.GetStore().DataBind();
        }

         protected void btnDelete_Click(object sender, DirectEventArgs e)
        {
            int formEmpId = int.Parse(hiddenValueFormEmpId.Text.Trim());
            Status status = DynamicFormManager.DeleteDynamicFormEmployee(formEmpId);
            if (status.IsSuccess)
            {
                LoadDynamicFormEmployees();
                NewMessage.ShowNormalMessage("Dynamic Form Employee deleted");
            }
            else
            {
                NewMessage.ShowWarningMessage(status.ErrorMessage);
            }
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int formId = int.Parse(hiddenValue.Text.Trim());
            int formEmpId = int.Parse(hiddenValueFormEmpId.Text.Trim());
            //Response.Redirect(string.Format("~/DynamicForms/DynamicRequestedForm.aspx?formId={0}&formEmpId={1}", formId.ToString(), formEmpId.ToString()));
            //TestDynamicFormEmp
            Response.Redirect(string.Format("~/DynamicForms/TestDynamicFormEmp.aspx?formId={0}&formEmpId={1}", formId.ToString(), formEmpId.ToString()));
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("~/DynamicForms/DynamicRequestedForm.aspx");
        }

    
    }
}
