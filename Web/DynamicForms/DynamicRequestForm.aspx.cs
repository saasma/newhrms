﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.DynamicForms
{
    public partial class DynamicRequestForm : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private int GetFormId()
        {
            return hdnFormId.Value.ToString() == "" ? 0 : int.Parse(hdnFormId.Value.ToString());
        }

        private void Initialise()
        {
            BindStores();

            if (Request.QueryString["formID"] != null)
            {
                hdnFormId.Value = Request.QueryString["formId"].ToString();

                DynamicForm entity = DynamicFormManager.GetDynamicFormUsingFormId(GetFormId());
                txtFormName.Text = entity.Name.ToString();

                BindGrid();

                bool Exists = DynamicFormManager.CheckDynamicFormUsedInEmployee(GetFormId());
                if (Exists)
                {
                    gridRequestForm.Enabled = false;
                    btnAddNewLineToMain.Enabled = false;                    
                }

                if (!string.IsNullOrEmpty(Request.QueryString["type"]))
                {
                    if (Request.QueryString["type"].ToString() == "saved")
                        lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), "Request form has been saved.");
                    else
                        lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), "Request form has been updated.");
                }
            }

        }

        private void BindStores()
        {
            storeControlType.DataSource = new object[]
            {
                new object[]{"1","TextBox"},
                new object[]{"2","DropDownList"},
                new object[]{"3","CheckBox"},
                new object[]{"4","CheckBoxList"},
                new object[]{"5","RadioButtonList"}
            };
            storeControlType.DataBind();

            storeIsCompulsory.DataSource = new object[]
            {
                new object[]{"true","Yes"},
                new object[]{"false","No"}
            };           
        }

        private void BindGrid()
        {
            gridRequestForm.GetStore().DataSource = DynamicFormManager.GetDynamicFormControlList(GetFormId());
            gridRequestForm.GetStore().DataBind();
            
        }

        protected void ButtonNext_Click(object sender, DirectEventArgs e)
        {
            List<DAL.DynamicFormControl> lines = new List<DynamicFormControl>();
            string json = e.ExtraParams["GridValues"];

            if (string.IsNullOrEmpty(json))
                return; ;
            Status isValidToInsert = new Status();
            lines = JSON.Deserialize<List<DAL.DynamicFormControl>>(json);

            if (lines.Count == 0)
            {
                NewMessage.ShowWarningMessage("No of rows is 0.");
                return;
            }

            for (int i = 0; i < lines.Count; i++)
            {
                if (string.IsNullOrEmpty(lines[i].LabelName.Trim().ToString()))
                {
                    isValidToInsert.ErrorMessage = "Label Name is required for row " + i +1;
                    isValidToInsert.IsSuccess = false;
                    break;
                }

                if (lines[i].ControlTypeName.ToString() == "2" || lines[i].ControlTypeName.ToString() == "4" || lines[i].ControlTypeName.ToString() == "5")
                {
                    if(string.IsNullOrEmpty(lines[i].ControlValues.ToString()))
                    {
                        isValidToInsert.ErrorMessage = "Control Values are required for row " + i +1;
                        isValidToInsert.IsSuccess = false;
                        break;
                    }
                }
            }

            if (!isValidToInsert.IsSuccess)
            {
                NewMessage.ShowWarningMessage(isValidToInsert.ErrorMessage);
                return;
            }

            DynamicForm entity = new DynamicForm()
            {
                Name = txtFormName.Text.Trim()
            };

            List<DynamicFormControl> list = new List<DynamicFormControl>();
            int j = 1;

            for (int i = 0; i < lines.Count; i++)
            {
                DynamicFormControl objDynamicFormControl = new DynamicFormControl();
                objDynamicFormControl.LabelName = lines[i].LabelName.ToString();
                objDynamicFormControl.ControlType = int.Parse(lines[i].ControlType.ToString());
                objDynamicFormControl.IsRequired = lines[i].IsRequired.ToString().ToLower() == "true" ? true : false;
                objDynamicFormControl.Sequence = j;
                j++;

                List<DynamicFormControlValue> listControlValue = new List<DynamicFormControlValue>();

                if (!string.IsNullOrEmpty(lines[i].ControlValues.Trim().ToString()))
                {
                    //string[] controlValues = txtControlValues.Text.Trim().Split(',');
                    string[] controlValues = lines[i].ControlValues.Trim().ToString().Split(new string[] { ",," }, StringSplitOptions.None);
                    for (int k = 0; k < controlValues.Length; k++)
                    {
                        DynamicFormControlValue entityValue = new DynamicFormControlValue()
                        {
                            ControlValue = controlValues[k].ToString()
                        };
                        listControlValue.Add(entityValue);
                    }
                }
                if (listControlValue.Count != 0)
                    objDynamicFormControl.DynamicFormControlValues.AddRange(listControlValue);

                list.Add(objDynamicFormControl);
               
            }


            if (GetFormId() != 0)
                entity.FormID = GetFormId();
            Status status = DynamicFormManager.InsertUpdateDynamicForm(entity, list);
            string formId = status.ErrorMessage;

            if (GetFormId().Equals(0))
                Response.Redirect("~/DynamicForms/DynamicRequestForm.aspx?formId=" + formId + "&type=saved");
            else
                Response.Redirect("~/DynamicForms/DynamicRequestForm.aspx?formId=" + formId + "&type=updated");

        }

    

       
    }
}