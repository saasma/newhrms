﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.DynamicForms.UserControls
{
    public partial class DynamicFormEmp : BaseUserControl
    {
        Table tbl = new Table();

        protected void Page_Init(object sender, EventArgs e)
        {
            string a = string.Empty;
            if (Request.QueryString["formId"] != null)
            {
                hdnValue.Value = Request.QueryString["formId"].ToString();

                lblFormName.Text = DynamicFormManager.GetDynamicFormNameUsingFormId(GetFormId());

                FillControls();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["formEmpId"] != null)
                {

                    int dfeid = int.Parse(Request.QueryString["formEmpId"].ToString());
                    FillControlValues(dfeid);

                    div1.Visible = true;
                    div2.Visible = false;
                    div4.Visible = false;
                    

                    //div=2 DynamicFormEmployeeHRView
                    //div=3 DynamicFormEmpListingAdminView
                    if (Request.QueryString["div"] != null)
                    {
                        string divId = Request.QueryString["div"].ToString();

                        if (divId == "1")
                        {
                            div1.Visible = true;
                            div2.Visible = false;
                            div4.Visible = false;
                            txtHRComment.Visible = false;
                        }
                        else if (divId == "2")
                        {
                            div1.Visible = false;
                            div2.Visible = true;
                            div4.Visible = false;

                            divComment.Visible = true;
                            divComment1.Visible = true;
                            txtHRComment.Visible = true;
                            LoadEmployeeInfo(dfeid);
                            LoadDataForForwarNAdvance(dfeid);

                            btnAdvanace.Visible = false;
                        }
                        else if (divId == "3")
                        {
                            div1.Visible = false;
                            div2.Visible = false;
                            divComment.Visible = true;
                            divComment1.Visible = true;
                            txtHRComment.Visible = true;
                            LoadEmployeeInfo(dfeid);
                        }
                        else if (divId == "4")
                        {
                            div1.Visible = false;
                            div2.Visible = false;
                            div4.Visible = true;
                            divComment.Visible = true;
                            divComment1.Visible = false;
                            txtHRComment.Visible = false;                            
                            LoadEditDataForAdminAdvance(dfeid);
                        }
                    }

                    if (txtHRComment.Visible)
                    {
                        RequiredFieldValidator rfvComment = new RequiredFieldValidator();
                        rfvComment.ID = "rfvComment";
                        rfvComment.ValidationGroup = "DynamicForm";
                        rfvComment.ErrorMessage = "Comment is required";
                        rfvComment.Display = ValidatorDisplay.None;
                        rfvComment.EnableClientScript = true;
                        rfvComment.ControlToValidate = "txtHRComment";
                        pnl.Controls.Add(rfvComment);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["formId"]))
                    {
                        div1.Visible = true;
                        div2.Visible = false;
                        div4.Visible = false;
                    }
                }
            }
        }


        private int GetFormId()
        {
            return int.Parse(hdnValue.Value.ToString());
        }

        private void FillControls()
        {
            List<DynamicFormControl> list = DynamicFormManager.GetDynamicFormControlList(GetFormId());
            int i = 0;

            tbl.Style.Add("border", "3");
            tbl.Style.Add("cellspacing", "2");
            tbl.Style.Add("cellpadding", "3");
            tbl.Style.Add("padding-bottom", "5");
            RequiredFieldValidator rfv;
            if (list.Count != 0)
            {

                foreach (var item in list)
                {

                    TableCell cell1 = new TableCell();
                    cell1.Style.Add("width", "200px");
                    TableCell cell2 = new TableCell();
                    TableRow tr = new TableRow();
                    Label lbl = new Label();
                    lbl.Style.Add("width", "200px");
                    lbl.ID = "lbl" + item.Sequence.ToString();
                    lbl.Text = item.LabelName;
                    cell1.Controls.Add(lbl);

                    rfv = new RequiredFieldValidator();
                    List<DynamicFormControlValue> listDynamicFormControlValue = new List<DynamicFormControlValue>();

                    if (item.ControlType == (int)ControlType.DropDownList || item.ControlType == (int)ControlType.CheckBoxList || item.ControlType == (int)ControlType.RadioButtonList)
                    {
                        listDynamicFormControlValue = DynamicFormManager.GetDynamicFormControlValueList(item.ControlID);
                    }

                    if (item.IsRequired == true && item.ControlType != (int)ControlType.CheckBox && item.ControlType != (int)ControlType.CheckBoxList)
                    {
                        rfv.ID = "rfv" + item.Sequence.ToString();
                        rfv.ValidationGroup = "DynamicForm";
                        rfv.ErrorMessage = item.LabelName + " is required.";
                        rfv.Display = ValidatorDisplay.None;
                        rfv.EnableClientScript = true;
                    }

                    if (item.ControlType == (int)ControlType.TextBox)
                    {
                        TextBox txtBox = new TextBox();
                        txtBox.ID = "txt" + item.Sequence.ToString();
                        txtBox.Attributes.Add("controlId", item.ControlID.ToString());

                        if (item.IsRequired == true)
                        {
                            //txtBox.CausesValidation = true;                            
                            rfv.ControlToValidate = txtBox.ID;
                        }

                        cell2.Controls.Add(txtBox);
                    }
                    else if (item.ControlType == (int)ControlType.CheckBox)
                    {
                        CheckBox chkBox = new CheckBox();
                        chkBox.ID = "chkBox" + item.Sequence.ToString();
                        chkBox.Attributes.Add("controlId", item.ControlID.ToString());
                        chkBox.Text = item.LabelName;

                        if (item.IsRequired == true)
                        {
                            //chkBox.CausesValidation = true;
                            //rfv.ControlToValidate = chkBox.ID;
                        }

                        cell2.Controls.Add(chkBox);
                    }
                    else if (item.ControlType == (int)ControlType.DropDownList)
                    {
                        DropDownList ddl = new DropDownList();
                        ddl.ID = "ddl" + item.Sequence.ToString();
                        ddl.Attributes.Add("controlId", item.ControlID.ToString());

                        ddl.Items.Insert(0, "Select");
                        foreach (var obj in listDynamicFormControlValue)
                        {
                            ddl.Items.Add(obj.ControlValue);
                        }

                        if (item.IsRequired == true)
                        {
                            //ddl.CausesValidation = true;
                            rfv.ControlToValidate = ddl.ID;
                            rfv.InitialValue = "Select";
                        }
                        cell2.Controls.Add(ddl);
                    }
                    else if (item.ControlType == (int)ControlType.CheckBoxList)
                    {
                        CheckBoxList chkBoxList = new CheckBoxList();
                        chkBoxList.ID = "chkBoxList" + item.Sequence.ToString();
                        chkBoxList.Attributes.Add("controlId", item.ControlID.ToString());

                        foreach (var obj in listDynamicFormControlValue)
                        {
                            chkBoxList.Items.Add(obj.ControlValue);
                        }

                        if (item.IsRequired == true)
                        {
                            //chkBocList.CausesValidation = true;
                            //rfv.ControlToValidate = chkBocList.ID;
                        }

                        cell2.Controls.Add(chkBoxList);
                    }
                    else if (item.ControlType == (int)ControlType.RadioButtonList)
                    {
                        RadioButtonList rbList = new RadioButtonList();
                        rbList.ID = "rbList" + item.Sequence.ToString();
                        rbList.Attributes.Add("controlId", item.ControlID.ToString());

                        foreach (var obj in listDynamicFormControlValue)
                        {
                            rbList.Items.Add(obj.ControlValue);
                        }

                        if (item.IsRequired == true)
                        {
                            //rbList.CausesValidation = true;
                            rfv.ControlToValidate = rbList.ID;
                        }

                        cell2.Controls.Add(rbList);
                    }

                    tr.Cells.Add(cell1);
                    tr.Cells.Add(cell2);
                    tbl.Rows.Add(tr);

                    if (item.IsRequired.Value)
                    {
                        if (item.ControlType != (int)ControlType.CheckBox && item.ControlType != (int)ControlType.CheckBoxList)
                        {
                            pnl.Controls.Add(rfv);
                        }
                    }
                }

                pnl.Controls.Add(tbl);
            }
        }

        private List<DynamicFormEmployeeValue> GetDynamicForEmployeeValueList()
        {
            List<DynamicFormEmployeeValue> listDynamicFormEmpValue = new List<DynamicFormEmployeeValue>();

            List<DynamicFormControl> list = DynamicFormManager.GetDynamicFormControlList(GetFormId());
            foreach (var item in list)
            {
                DynamicFormEmployeeValue obj = new DynamicFormEmployeeValue();

                if (item.ControlType == (int)ControlType.TextBox)
                {
                    TextBox txt = (TextBox)tbl.FindControl("txt" + item.Sequence.ToString());
                    obj.ControlID = int.Parse(txt.Attributes["controlId"].ToString());
                    if (!string.IsNullOrEmpty(txt.Text))
                        obj.Value = txt.Text.Trim();
                }
                else if (item.ControlType == (int)ControlType.DropDownList)
                {
                    DropDownList ddl = (DropDownList)tbl.FindControl("ddl" + item.Sequence.ToString());
                    obj.ControlID = int.Parse(ddl.Attributes["controlId"].ToString());
                    if (ddl.SelectedIndex != 0)
                        obj.Value = ddl.SelectedItem.Text;
                }
                else if (item.ControlType == (int)ControlType.CheckBox)
                {
                    CheckBox chk = (CheckBox)tbl.FindControl("chkBox" + item.Sequence.ToString());
                    obj.ControlID = int.Parse(chk.Attributes["controlId"].ToString());
                    if (chk.Checked)
                        obj.Value = "Yes";
                    else
                        obj.Value = "No";
                }
                else if (item.ControlType == (int)ControlType.CheckBoxList)
                {
                    CheckBoxList chkboxlist = (CheckBoxList)tbl.FindControl("chkBoxList" + item.Sequence.ToString());
                    obj.ControlID = int.Parse(chkboxlist.Attributes["controlId"].ToString());

                    string a = "";

                    foreach (ListItem it in chkboxlist.Items)
                    {
                        if (it.Selected)
                        {
                            a = a + it.ToString() + ",";
                        }
                    }

                    obj.Value = a.Substring(0, a.Length - 1).ToString();

                }
                else if (item.ControlType == (int)ControlType.RadioButtonList)
                {
                    RadioButtonList rbList = (RadioButtonList)tbl.FindControl("rbList" + item.Sequence.ToString());
                    obj.ControlID = int.Parse(rbList.Attributes["controlId"].ToString());

                    foreach (ListItem it in rbList.Items)
                    {
                        if (it.Selected)
                        {
                            obj.Value = it.Text;
                        }
                    }
                }

                listDynamicFormEmpValue.Add(obj);
            }

            return listDynamicFormEmpValue;
        }

        protected void btnSaveAndLater_Click(object sender, EventArgs e)
        {
            Page.Validate("DynamicForm");
            if (Page.IsValid)
            {
                Button btn = (Button)sender;

                DynamicFormEmployee entity = new DynamicFormEmployee();
                DynamicFormEmployeeStatusHistory hist = new DynamicFormEmployeeStatusHistory();
                hist.Status = 0;

                entity.FormID = GetFormId();
                entity.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;
                entity.DateEng = SessionManager.GetCurrentDateAndTime();

                bool isEdit = false;
                if (!string.IsNullOrEmpty(Request.QueryString["formEmpId"]))
                    isEdit = true;

                entity.Date = BLL.BaseBiz.GetAppropriateDate(SessionManager.GetCurrentDateAndTime());                

                if (sender == btnSaveAndLater)
                {
                    entity.Status = 0;
                    entity.StatusName = "Saved";
                }
                else if(sender == btnSaveAndSend)
                {
                    entity.Status = (int)FlowStepEnum.Step1SaveAndSend;
                    entity.StatusName = "Step1SaveAndSend";
                    hist.Status = (int)FlowStepEnum.Step1SaveAndSend;
                }

                List<DynamicFormEmployeeValue> listDynamicFormEmpValue = GetDynamicForEmployeeValueList();               

                entity.DynamicFormEmployeeValues.AddRange(listDynamicFormEmpValue);

                Status status = new Status();

                if (Request.QueryString["formEmpId"] != null)
                    entity.DFEID = int.Parse(Request.QueryString["formEmpId"].ToString());

                status = DynamicFormManager.InsertUpdateDynamicRequestForm(entity, hist);

                if (status.IsSuccess)
                {
                    if (entity.Status != (int)FlowStepEnum.Saved)
                    {
                        btnSaveAndLater.Visible = false;
                        btnSaveAndSend.Visible = false;
                    }

                    if (!isEdit)
                    {
                        if (entity.Status == (int)FlowStepEnum.Saved)
                        {
                            Response.Redirect(string.Format("~/Employee/Forms/EmployeeDynamicForm.aspx?formId={0}&formEmpId={1}&type=1", GetFormId().ToString(), entity.DFEID.ToString()));
                        }
                        else
                        {
                            Response.Redirect(string.Format("~/Employee/Forms/EmployeeDynamicForm.aspx?formId={0}&formEmpId={1}&type=2", GetFormId().ToString(), entity.DFEID.ToString()));
                        }
                    }
                    else
                    {
                        if (entity.Status == (int)FlowStepEnum.Saved)
                        {
                            lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), "Dynamic from employee has been updated.");
                        }
                        else
                        {
                            lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), "Dynamic Form employee has been updated and forwarded.");
                        }
                    }

                    
                }
                else
                {
                    lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "WarningForDetail").ToString(), status.ErrorMessage);
                }
            }


        }

        private void FillControlValues(int dynamicFormEmpId)
        {
            List<DynamicFormControl> list = DynamicFormManager.GetDynamicFormControlList(GetFormId());

            string value = string.Empty;
            foreach (var item in list)
            {
                if (item.ControlType == (int)ControlType.TextBox)
                {
                    TextBox txt = (TextBox)tbl.FindControl("txt" + item.Sequence.ToString());
                    value = DynamicFormManager.GetDynamicFormEmpValues(dynamicFormEmpId, int.Parse(txt.Attributes["controlId"].ToString()));
                    if (!string.IsNullOrEmpty(value))
                        txt.Text = value;
                }
                else if (item.ControlType == (int)ControlType.DropDownList)
                {
                    DropDownList ddl = (DropDownList)tbl.FindControl("ddl" + item.Sequence.ToString());
                    value = DynamicFormManager.GetDynamicFormEmpValues(dynamicFormEmpId, int.Parse(ddl.Attributes["controlId"].ToString()));
                    if (!string.IsNullOrEmpty(value))
                        ddl.Text = value;
                }
                else if (item.ControlType == (int)ControlType.CheckBox)
                {
                    CheckBox chk = (CheckBox)tbl.FindControl("chkBox" + item.Sequence.ToString());
                    value = DynamicFormManager.GetDynamicFormEmpValues(dynamicFormEmpId, int.Parse(chk.Attributes["controlId"].ToString()));
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (value == "Yes")
                            chk.Checked = true;
                        else
                            chk.Checked = false;
                    }
                }
                else if (item.ControlType == (int)ControlType.CheckBoxList)
                {
                    CheckBoxList chkboxlist = (CheckBoxList)tbl.FindControl("chkBoxList" + item.Sequence.ToString());
                    value = DynamicFormManager.GetDynamicFormEmpValues(dynamicFormEmpId, int.Parse(chkboxlist.Attributes["controlId"].ToString()));
                    if (!string.IsNullOrEmpty(value))
                    {
                        string[] indValue = value.Split(',');
                        for (int i = 0; i < indValue.Length; i++)
                        {
                            foreach (ListItem itemCheckboxList in chkboxlist.Items)
                            {
                                if (itemCheckboxList.Text == indValue[i])
                                    itemCheckboxList.Selected = true;
                            }
                        }
                    }
                }
                else if (item.ControlType == (int)ControlType.RadioButtonList)
                {
                    RadioButtonList rbList = (RadioButtonList)tbl.FindControl("rbList" + item.Sequence.ToString());
                    value = DynamicFormManager.GetDynamicFormEmpValues(dynamicFormEmpId, int.Parse(rbList.Attributes["controlId"].ToString()));

                    if (!string.IsNullOrEmpty(value))
                    {
                        foreach (ListItem itemRb in rbList.Items)
                        {
                            if (itemRb.Text == value)
                                itemRb.Selected = true;
                        }
                    }
                }
            }
            
            string divIdString = "";
            if (!string.IsNullOrEmpty(Request.QueryString["div"]))
                divIdString = Request.QueryString["div"].ToString();
           

            if (divIdString == "1" || divIdString == "")
            {
                DynamicFormEmployee obj = DynamicFormManager.GetDynamicFormEmpoyeeUsingID(int.Parse(Request.QueryString["formEmpId"].ToString()));
                if (obj.Status != (int)FlowStepEnum.Saved)
                {
                    lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "WarningForDetail").ToString(), "Forwarded dynamic form can not be changed.");
                    lblMsg.Focus();

                    btnSaveAndLater.Visible = false;
                    btnSaveAndSend.Visible = false;
                }


                string msgType = Request.QueryString["type"];
                if (!string.IsNullOrEmpty(msgType))
                {
                    if (msgType == "1")
                    {
                        lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), "Dynamic form employee has been saved.");
                        lblMsg.Focus();
                    }
                    else if (msgType == "2")
                    {
                        lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), "Dynamic form employee has been saved & forwarded.");
                        lblMsg.Focus();
                    }
                }
            }
            
        }

        private void Clear()
        {
            List<DynamicFormControl> list = DynamicFormManager.GetDynamicFormControlList(GetFormId());

            string value = string.Empty;
            foreach (var item in list)
            {
                if (item.ControlType == (int)ControlType.TextBox)
                {
                    TextBox txt = (TextBox)tbl.FindControl("txt" + item.Sequence.ToString());
                    txt.Text = "";
                }
                else if (item.ControlType == (int)ControlType.DropDownList)
                {
                    DropDownList ddl = (DropDownList)tbl.FindControl("ddl" + item.Sequence.ToString());
                    ddl.SelectedIndex = 0;
                }
                else if (item.ControlType == (int)ControlType.CheckBox)
                {
                    CheckBox chk = (CheckBox)tbl.FindControl("chkBox" + item.Sequence.ToString());
                    if(chk.Checked)
                        chk.Checked = false;
                }
                else if (item.ControlType == (int)ControlType.CheckBoxList)
                {
                    CheckBoxList chkboxlist = (CheckBoxList)tbl.FindControl("chkBoxList" + item.Sequence.ToString());

                    foreach (ListItem itemChkBoxList in chkboxlist.Items)
                    {
                        if (itemChkBoxList.Selected)
                            itemChkBoxList.Selected = false;
                    }
                }
                else if (item.ControlType == (int)ControlType.RadioButtonList)
                {
                    RadioButtonList rbList = (RadioButtonList)tbl.FindControl("rbList" + item.Sequence.ToString());

                    foreach (ListItem itemRb in rbList.Items)
                    {
                        if (itemRb.Selected)
                            itemRb.Selected = false;
                    }
                }
            }
        }

        private void LoadDataForForwarNAdvance(int dfeid)
        {

            DynamicFormEmployee entity = DynamicFormManager.GetDynamicFormEmpoyeeUsingID(dfeid);
            if (DynamicFormManager.CanEmpChangeDocument(entity.Status.Value, entity.EmployeeID.Value, SessionManager.CurrentLoggedInEmployeeId, GetFormId()))
            {
                btnForward.Visible = true;
            }
            else
            {
                btnForward.Visible = false;
            }
        }

        protected void btnForward_Click(object sender, EventArgs e)
        {
            Page.Validate("DynamicForm");
            if (Page.IsValid)
            {
                DynamicFormEmployee entity = new DynamicFormEmployee();

                DynamicFormEmployeeStatusHistory hist = new DynamicFormEmployeeStatusHistory();


                entity.FormID = GetFormId();
                
                List<DynamicFormEmployeeValue> listDynamicFormEmpValue = GetDynamicForEmployeeValueList();
                entity.DynamicFormEmployeeValues.AddRange(listDynamicFormEmpValue);

                DynamicFormEmployee dbEntity = DynamicFormManager.GetDynamicFormEmpoyeeUsingID(int.Parse(Request.QueryString["formEmpId"].ToString()));

                GetDocumentNextStepUsingDynamicApprovalFlowResult nextStep = DynamicFormManager.GetDocumentNextStepDynamicApproval(dbEntity.Status.Value, dbEntity.EmployeeID.Value, int.Parse(Request.QueryString["formId"].ToString()));
                if (nextStep != null)
                {
                    entity.Status = nextStep.NextStepID;
                    entity.StatusName = nextStep.StepStatusName;
                }
                entity.DFEID = int.Parse(Request.QueryString["formEmpId"].ToString());

                hist.ApprovalRemarks = txtHRComment.Text;

                Status status = DynamicFormManager.InsertUpdateDynamicRequestForm(entity, hist);
                if (status.IsSuccess)
                {
                    lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), "Dynamic form employee forwarded.");
                    div2.Visible = false;
                }
                else
                {
                    lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "WarningForDetail").ToString(), status.ErrorMessage);
                }

              
            }
        }

        protected void btnAdvanace_Click(object sender, EventArgs e)
        {
             Page.Validate("DynamicForm");
             if (Page.IsValid)
             {
                 DynamicFormEmployee entity = new DynamicFormEmployee();

                 DynamicFormEmployeeStatusHistory hist = new DynamicFormEmployeeStatusHistory();

                 entity.DFEID = int.Parse(Request.QueryString["formEmpId"].ToString());

                 List<DynamicFormEmployeeValue> listDynamicFormEmpValue = GetDynamicForEmployeeValueList();

                 entity.DynamicFormEmployeeValues.AddRange(listDynamicFormEmpValue);

                 Status status = DynamicFormManager.UpdateAdvanceDynamicFormEmployeeReq(entity);
                 if (status.IsSuccess)
                 {
                     lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), "Dynamic employee form advanced.");
                     div1.Visible = false;
                     div2.Visible = false;
                     div4.Visible = false;
                 }
                 else
                 {
                     lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "WarningForDetail").ToString(), status.ErrorMessage);
                 }

             }
        }

        private void LoadEmployeeInfo(int dfeid)
        {

            List<DynamicFormEmployeeStatusHistory> list = DynamicFormManager.GetDynamicFormEmpStatusHist(dfeid);

            string body = "";         

            foreach (var status in list)
            {
                body +=
                    string.Format("<div class='commentBlock'>{0}</div>", status.ApprovalDisplayTitle + "'s Comment")
                    +
                    string.Format("<div class='nameBlock'>{0}</div>", status.ApprovalRemarks + " - " + status.ApprovedOn.Value.ToShortDateString());
            }

            lblCommentSection.Text = body;
            

        }

        private void LoadEditDataForAdminAdvance(int dfeid)
        {
            DynamicFormEmployee entity = DynamicFormManager.GetDynamicFormEmpoyeeUsingID(dfeid);

            //if (request.AdvanceStatus == null || request.AdvanceStatus == (int)AdvanceStatusEnum.HRAdvance)
            //if (entity.Status == (int)AdvanceStatusEnum.HRAdvance)
            if(entity.Status.Value == 15)
            {
                LoadEmployeeInfo(dfeid);
                LoadDataForForwarNAdvance(dfeid);
            }
            else
            {
                if (entity.Status == (int)AdvanceStatusEnum.HRSettle)
                {
                    btnSaveAndLaterSett.Visible = false;
                    txtHRComment.Visible = false;
                }
                else
                {
                    if (entity.Status == (int)AdvanceStatusEnum.EmployeeSettle || entity.Status == (int)AdvanceStatusEnum.HRSettle)
                    {
                        btnSaveAndLaterSett.Visible = true;
                    }
                }
            }

          
        }

        protected void btnSaveAndLaterSett_Click(object sender, EventArgs e)
        {
            DynamicFormEmployee entity = new DynamicFormEmployee();
            DynamicFormEmployeeStatusHistory hist = new DynamicFormEmployeeStatusHistory();

            int dfeid = int.Parse(Request.QueryString["fromEmpId"].ToString());

            entity.DFEID = dfeid;
            entity.FormID = GetFormId();
            entity.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;
            entity.DateEng = System.DateTime.Now;
            entity.Date = BLL.BaseBiz.GetAppropriateDate(entity.DateEng.Value);

            entity = DynamicFormManager.GetDynamicFormEmpoyeeUsingID(dfeid);

            List<DynamicFormEmployeeValue> listDynamicFormEmpValue = GetDynamicForEmployeeValueList();

            entity.DynamicFormEmployeeValues.AddRange(listDynamicFormEmpValue);
            entity.Status = (int)AdvanceStatusEnum.HRSettle;

            Status status = DynamicFormManager.SettleDynamicFormEmployee(entity, hist);
            if (status.IsSuccess)
            {
                lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "InformationForDetail").ToString(), "Dynamic Form Employee settled.");
                div1.Visible = false;
                div2.Visible = false;
                div4.Visible = false;
            }
            else
            {
                lblMsg.Text = string.Format(HttpContext.GetGlobalResourceObject("HTMLMessage", "WarningForDetail").ToString(), status.ErrorMessage);               
            }
        }


        
    }
}