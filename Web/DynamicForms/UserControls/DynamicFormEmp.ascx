﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DynamicFormEmp.ascx.cs" Inherits="Web.DynamicForms.UserControls.DynamicFormEmp" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<style>
    .commentBlock
    {
        font-size: 13px;
        margin-top: 5px;
    }
    .nameBlock
    {
        font-size: 13px;
        color: #3399FF; 
        font-style: italic;
    }
    
    
    .createbtnsUC{
		display:inline-block;
		text-decoration:none;
		width:90px;
		height:18px;
		outline:none;
		cursor:pointer;
		font:bold 12px/1em HelveticaNeue,Arial,sans-serif;
		padding:6px 10px;
		margin-right:15px;
		color:#fff;
		text-align:center;
		background:#ee8f1f;
		background:-webkit-gradient(linear,left top,left bottom,from(#f5b026),to(#f48423));
		background:-moz-linear-gradient(top,#f5b026,#f48423);
		border:1px solid #e6791c;
		border-color:#e6791c #e6791c #d86f15;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
		border-radius:4px;
		-webkit-box-shadow:0 1px 2px #d7e9a4,inset 0 1px 0 #f8d898;
		-moz-box-shadow:0 1px 2px #d7e9a4,inset 0 1px 0 #f8d898;
		box-shadow:0 1px 2px #d7e9a4,inset 0 1px 0 #f8d898
}

.createbtnsUC:hover,.createbtnsUC:focus{
		color:#fff;
		background:#e38512;
		background:-webkit-gradient(linear,left top,left bottom,from(#ffbb33),to(#eb7b1a));
		background:-moz-linear-gradient(top,#ffbb33,#eb7b1a);
		border-color:#d0680c;
		-webkit-box-shadow:0 0 1px #d6d6d6,inset 0 1px 0 #ffdf9e;
		-moz-box-shadow:0 0 1px #d6d6d6,inset 0 1px 0 #ffdf9e;
		box-shadow:0 0 1px #d6d6d6,inset 0 1px 0 #ffdf9e
}

.createbtnsUC:active{
		position:relative;
		top:1px;
		color:#fff;
		background:#ee8f1f;
		border-color:#d0680c;
		-webkit-box-shadow:0 1px 0 #fff,inset 0 0 5px #d0680c;
		-moz-box-shadow:0 1px 0 #fff,inset 0 0 5px #d0680c;
		box-shadow:0 1px 0 #fff,inset 0 0 5px #d0680c
}
</style>




<div class="widget-head">
                <h4 class="heading">
                    <asp:Label ID="lblFormName" Text="" Font-Size="Larger" runat="server" />
                    </h4>
            </div>
            <div class="widget-body">

            <asp:Label ID="lblMsg" runat="server"></asp:Label>
          
           <br />

            <asp:HiddenField ID="hdnValue" runat="server" />
                <asp:Panel ID="pnl" runat="server">


                </asp:Panel>

                </br>

                <div id="divComment" runat="server" visible="false">
                    <table>
                         <tr>
                        <td>                           
                            <asp:Label ID="lblCommentSection" Text="" runat="server" />
                        </td>
                        <td>
                            
                        </td>
                    </tr>
                    </table>
                </div>

                <div id="divComment1" runat="server" visible="false">
                    <table>
                    <tr>
                        <td style="width:200px;">
                            Your Comment
                        </td>
                        <td>
                            <asp:TextBox ID="txtHRComment" TextMode="MultiLine" Height="100px" Width="400px" runat="server" />
                        </td>
                    </tr>
                    </table>
                </div>
                
               <div id="div1" runat="server">
                    <table>
                    <tr>
                        <td>
                          <asp:Button ID="btnSaveAndLater" Text="&nbsp;&nbsp;Save and Finish Later&nbsp;&nbsp;" runat="server"  CausesValidation="true" Width="150px" Height="30px"
                             CssClass="createbtns" onclick="btnSaveAndLater_Click" OnClientClick="valGroup='DynamicForm'; return CheckValidation();"
                            ></asp:Button>
                        </td>

                        <td>
                        <asp:Button ID="btnSaveAndSend" Text="&nbsp;&nbsp;Save and Send &nbsp;&nbsp;" Width="150px" Height="30px"
                            runat="server" CausesValidation="true" CssClass="createbtns  floatleft"
                            OnClientClick="valGroup='DynamicForm'; return CheckValidation();" 
                            onclick="btnSaveAndLater_Click" />
                        </td>
                    </tr>
                    </table>
              </div>



              <div id="div2" runat="server">
                    <table>
                        <tr>
                            <td>

                                <asp:Button ID="btnForward" runat="server" Text="&nbsp;Forward&nbsp;" CssClass="createbtns  floatleft"  
                                    Width="150px" Height="30px"
                                OnClientClick="valGroup='DynamicForm'; return CheckValidation();" 
                                    onclick="btnForward_Click" />

                      
                            </td>

                            <td>
                                 <asp:Button ID="btnAdvanace" runat="server" Text="&nbsp;Set Advance&nbsp;" CssClass="createbtns  floatleft" 
                                     Width="150px" Height="30px"
                                OnClientClick="valGroup='DynamicForm'; return CheckValidation();" 
                                     onclick="btnAdvanace_Click" />
                            </td>
                        </tr>
                    </table>
              </div>

              <div id="div4" runat="server">

                <table>
                        <tr>
                            <td>

                                <asp:Button ID="btnAdminAdvance" Text="&nbsp;Set Advance&nbsp;" Width="150px" Height="30px"  CssClass="createbtnsUC" runat="server" OnClientClick="valGroup='DynamicForm'; return CheckValidation();" 
                                     onclick="btnAdvanace_Click" />

                      
                            </td>

                            <td>
                                   <asp:Button ID="btnSaveAndLaterSett" runat="server" Text="&nbsp;&nbsp;Settle&nbsp;&nbsp;" CssClass="createbtnsUC"  
                                    Width="150px" Height="30px"
                                OnClientClick="valGroup='DynamicForm'; return CheckValidation();" 
                                    onclick="btnSaveAndLaterSett_Click" />
                            </td>
                        </tr>
                    </table>
              
              </div>


    </div>