﻿<%@ Page Title="Dynamic Form Request" Language="C#" MasterPageFile="~/Master/HR.Master" AutoEventWireup="true" CodeBehind="DynamicRequestForm.aspx.cs" Inherits="Web.DynamicForms.DynamicRequestForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="header1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">
             var ControlTypeRender = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeControlType.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.ControlTypeName;
                };

            var IsRequiredRender = function (value, metaData, record, rowIndex, colIndex, store) {
                    var r = <%= storeIsCompulsory.ClientID %>.getById(value.toString().toLowerCase());
                    if (Ext.isEmpty(r)) {
                        return "";
                    }
                    return r.data.IsRequiredOrNot;
                };
        
        var addNewRow = function (grid) {
            var newRow = new SettingModel();

            newRow.data.ControlID = "";

            var rowIndex = grid.getStore().data.items.length;

            //gridProjectSplitter.stopEditing();
            grid.getStore().insert(rowIndex, newRow);
        }

        var RemoveItemLine = function (column, command, record, recordIndex, cellIndex) {
            var store = this.grid.store;
            store.remove(record);
        };


             
         


    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Main" runat="server">

<asp:HiddenField ID="hdnFormId" runat="server" />

         <ext:Store runat="server" ID="storeControlType">
            <Model>
                <ext:Model ID="modelControlType" IDProperty="ControlType" runat="server">
                    <Fields>
                        <ext:ModelField Name="ControlType" Type="String" />
                        <ext:ModelField Name="ControlTypeName" />
                        
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>

        <ext:Store runat="server" ID="storeIsCompulsory">
            <Model>
                <ext:Model ID="modelIsCompulsory" IDProperty="IsRequired" runat="server">
                    <Fields>
                        <ext:ModelField Name="IsRequired" Type="String" />
                        <ext:ModelField Name="IsRequiredOrNot" />
                        
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>

    

    <%--<ext:LinkButton runat="server" Hidden="true" ID="btnEditLevel">
        <DirectEvents>
            <Click OnEvent="btnEditLevel_Click">
                <EventMask ShowMask="true" />
            </Click>
        </DirectEvents>
    </ext:LinkButton>--%>

  <div class="separator bottom">
    </div>
    
   <div class="innerLR">
       
        <div class="widget">
            <div class="widget-head">
                <h4 class="heading">
                    Request Form</h4>
            </div>

           <asp:Label ID="lblMsg" runat="server"></asp:Label>

             <div class="widget-body">
                <table>
                    <tr>
                        <td>
                            <ext:TextField ID="txtFormName" LabelSeparator="" runat="server" FieldLabel="Form Name" LabelAlign="Left"
                                Width="300">
                            </ext:TextField>
                            <asp:RequiredFieldValidator Display="None" ID="rfvFormName" runat="server" ValidationGroup="SaveUpdate"
                                ControlToValidate="txtFormName"  ErrorMessage="Form Name is required." />
                        </td>
                    </tr>
                    
                </table>

                </br>

                <table>
                     <tr>
                        <td>

                            <i>Note: Control values should be separated by double commas(,,).</i>
                        </td>
                    </tr>
                </table>
             </div>

            <div class="widget-body">
              


                <ext:GridPanel StyleSpec="margin-top:15px;" ID="gridRequestForm" runat="server" Height="300" EnableDragDrop="true"
                    DDGroup="ddGroup" Cls="itemgrid">
                    <Store>
                        <ext:Store ID="storeRequestForm" runat="server">
                            <Model>
                                <ext:Model ID="Model1" Name="SettingModel" runat="server" >
                                    <Fields>
                                       
                                        <ext:ModelField Name="LabelName" Type="String" />
                                        <ext:ModelField Name="ControlType" Type="String" />
                                        <ext:ModelField Name="ControlTypeName" Type="String" />
                                        <ext:ModelField Name="IsRequired" Type="String" />
                                        <ext:ModelField Name="IsRequiredOrNot" Type="String" />
                                        <ext:ModelField Name="ControlValues" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <Plugins>
                        <ext:CellEditing ID="CellEditing1" ClicksToEdit="1" runat="server">
                        </ext:CellEditing>
                    </Plugins>
                   <%-- <Listeners>
                        <BeforeEdit Fn="beforeEdit" />
                    </Listeners>--%>
                    <ColumnModel>
                        <Columns>
                           
                            
                            <ext:Column ID="colLabelName" runat="server" Text="Label" Sortable="false" MenuDisabled="true"
                                Width="150" DataIndex="LabelName" Border="false">
                                
                                <Editor>
                                    <ext:TextField ID="txtLabelName" ClientIDMode="Static" runat="server"/>
                                    
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="colControlType" runat="server" Text="Control Type" Sortable="false"
                                MenuDisabled="true" Width="140" DataIndex="ControlType" Border="false">
                                <Renderer Fn="ControlTypeRender" />
                                <Editor>                        
                              

                                    <ext:ComboBox DisplayField="ControlTypeName" QueryMode="Local" ForceSelection="true" ValueField="ControlType"
                                        StoreID="storeControlType" ID="cmbControlType" runat="server">
                                    </ext:ComboBox>

                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column1" runat="server" Text="Is Compulsory" Sortable="false" MenuDisabled="true"
                                Width="120" DataIndex="IsRequired" Border="false">
                                <Renderer Fn="IsRequiredRender" />
                                <Editor>
                                    
                                    <ext:ComboBox DisplayField="IsRequiredOrNot" QueryMode="Local" ForceSelection="true"
                                        ValueField="IsRequired" StoreID="storeIsCompulsory" ID="cmbIsCompulsory" runat="server">
                                    </ext:ComboBox>

                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column6" runat="server" Text="Control Values" Sortable="false"
                                MenuDisabled="true" Width="500" DataIndex="ControlValues" Border="false">
                                <Editor>
                                    <ext:TextField ID="txtControlValues" runat="server" />
                                </Editor>
                            </ext:Column>
                            
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Sortable="false" MenuDisabled="true"
                                Width="30" Align="Center">
                                <Commands>
                                    <ext:GridCommand Icon="Delete" CommandName="Delete" Cls="deleteicon" />
                                </Commands>
                                <Listeners>
                                    <Command Fn="RemoveItemLine">
                                    </Command>

                                  <%--  <Command Handler="PositionCommandHandler(command,record);" />--%>

                                </Listeners>

                            </ext:CommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Single" />
                    </SelectionModel>

                     <View>
                        <ext:GridView ID="GridView1" runat="server">
                            <Plugins>
                                <ext:GridDragDrop ID="GridDragDrop1" runat="server" DragGroup="ddGroup" DropGroup="ddGroup" />
                            </Plugins>
                            <Listeners>
                                <%--<BeforeItemClick Fn="process" />
                                <Drop Fn="process" />--%>
                                <%-- <AfterRender Handler="this.plugins[0].dragZone.getDragText = getDragDropText;" />--%>
                                <%--  <Drop Handler="var dropOn = overModel ? ' ' + dropPosition + ' ' + overModel.get('Name') : ' on empty view'; 
                                               Ext.net.Notification.show({title:'Drag from right to left', html:'Dropped ' + data.records[0].get('Name') + dropOn});" />--%>
                            </Listeners>
                        </ext:GridView>
                    </View>

                </ext:GridPanel>
                <ext:Button ID="btnAddNewLineToMain" StyleSpec="margin-top:10px;" runat="server"
                    Icon="Add" Height="26" Text="Add New Row" Cls="btnblock bluecolor">
                    <Listeners>
                        <Click Handler="addNewRow(#{gridRequestForm});" />
                    </Listeners>
                </ext:Button>
                <div style="margin-top: 25px; width: 200px;">
                    <ext:Button ID="Button1" runat="server" Height="40" Text="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        Cls="btnFlat" BaseCls="btnFlat">
                        <DirectEvents>
                            <Click OnEvent="ButtonNext_Click">
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="GridValues" Value="Ext.encode(#{gridRequestForm}.getRowsValues({selectedOnly : false}))"
                                        Mode="Raw" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="valGroup = 'SaveUpdate'; return CheckValidation();">
                            </Click>
                        </Listeners>
                    </ext:Button>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageScriptsContent" runat="server">
</asp:Content>
