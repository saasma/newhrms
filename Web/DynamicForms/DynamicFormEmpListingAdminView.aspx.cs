﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.DynamicForms
{
    public partial class DynamicFormEmpListingAdminView : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                Initialise();
            }
        }

        private void Initialise()
        {
            cmbForm.Store[0].DataSource = DynamicFormManager.GetAllPublishedDynamicForms().Distinct();
            cmbForm.Store[0].DataBind();
        }

        protected void FormName_Change(object sender, DirectEventArgs e)
        {
            int formId = int.Parse(cmbForm.SelectedItem.Value);
            List<DAL.DynamicApprovalFlow> list = DynamicFormManager.GetDynamicApprovalFlowList(formId);

            list.Insert(0, new DAL.DynamicApprovalFlow { StepID = 0, StepName = "Pending/Saved" });
            list.Insert(0, new DAL.DynamicApprovalFlow { StepID = -1, StepName = "All" });


            cmbStatus.SelectedItems.Add(new Ext.Net.ListItem { Index = 0 });

            cmbStatus.Store[0].DataSource = list;
            cmbStatus.Store[0].DataBind();

            cmbStatus.SetValue(-1);

            //cmbStatus.SelectedItem.Text = "All";
        }

        private void BindGrid()
        {
            if (string.IsNullOrEmpty(cmbForm.SelectedItem.Text) || string.IsNullOrEmpty(cmbStatus.SelectedItem.Text))
                return;
            int status = -1;
            if (X.IsAjaxRequest)
            {
                status = int.Parse(cmbStatus.SelectedItem.Value);
            }
            int formID = int.Parse(cmbForm.SelectedItem.Value);
            gvDynamicFormEmployee.GetStore().DataSource = DynamicFormManager.GetAllDynamicFormEmployeeForAdmin(status, formID);
            gvDynamicFormEmployee.GetStore().DataBind();
        }

        public void btnLoad_Click(object sender, DirectEventArgs e)
        {
            BindGrid();
        }

        protected void btnEdit_Click(object sender, DirectEventArgs e)
        {
            int formId = int.Parse(hiddenValue.Text.Trim());
            int formEmpId = int.Parse(hiddenValueFormEmpId.Text.Trim());
          
            Response.Redirect(string.Format("~/DynamicForms/AdminDynamicFormEmloyee.aspx?formId={0}&formEmpId={1}&div=3", formId.ToString(), formEmpId.ToString()));
        }
    }
}