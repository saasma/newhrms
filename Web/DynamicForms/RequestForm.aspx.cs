﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using Ext.Net;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;

namespace Web.DynamicForms
{
    public partial class RequestForm : BasePage
    {
        List<DynamicFormControl> list;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialise();
            }
        }

        private int GetFormId()
        {
            return hdnFormId.Value.ToString() == "" ? 0 : int.Parse(hdnFormId.Value.ToString());
        }

        private void Initialise()
        {
            if (Request.QueryString["formId"] != null)
            {
                hdnFormId.Value = Request.QueryString["formId"].ToString();
                BindGrid();

                DynamicForm entity = DynamicFormManager.GetDynamicFormUsingFormId(GetFormId());

                txtFormName.Text = entity.Name.ToString();

                bool Exists = DynamicFormManager.CheckDynamicFormUsedInEmployee(GetFormId());
                if (Exists)
                {
                    gvw.Enabled = false;
                    btnAddRow.Enabled = false;
                }
            
            }
            else
            {
                BindEmptyGrid();
            }
           
        }

        private void BindGrid()
        {
            int formId = GetFormId();
            
            gvw.DataSource = DynamicFormManager.GetDynamicFormControlList(formId);
            gvw.DataBind();

            int i = 0;
            List<DynamicFormControl> list = DynamicFormManager.GetDynamicFormControlList(formId);
            

            foreach (GridViewRow row in gvw.Rows)
            {
                TextBox txtLabel = (TextBox)row.FindControl("txtLabel");
                DropDownList ddlControlType = (DropDownList)row.FindControl("ddlControlType");
                DropDownList ddlIsCompulsory = (DropDownList)row.FindControl("ddlIsCompulsory");

                ddlControlType.SelectedValue = list[i].ControlType.ToString();
                ddlIsCompulsory.SelectedValue = list[i].IsRequired == true ? "Yes" : "No";
                i++;

            }

            
        }

        private void BindEmptyGrid()
        {
            list = new List<DynamicFormControl>();
            for (int i = 0; i < 5; i++)
            {
                DynamicFormControl entity = new DynamicFormControl();
                list.Add(entity);
            }

            gvw.DataSource = list;
            gvw.DataBind();
        }

        protected void gvw_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            list = GetGridList().Where(x => x.RowIndex != e.RowIndex).ToList();
            gvw.DataSource = list;
            gvw.DataBind();

            int i = 0;
            foreach (var item in list)
            {
                DropDownList ddlControlType = (DropDownList)gvw.Rows[i].FindControl("ddlControlType");
                DropDownList ddlIsCompulsory = (DropDownList)gvw.Rows[i].FindControl("ddlIsCompulsory");

                ddlControlType.SelectedValue = item.ControlType.ToString();
                ddlIsCompulsory.SelectedValue = item.IsRequired.ToString();
                i++;
            }
           
        }

        private List<DynamicFormControl> GetGridList()
        {
            int rowIndex = 0;
           
            list = new List<DynamicFormControl>();
            for (int i = 1; i <= gvw.Rows.Count; i++)
            {
                TextBox txtLabel = (TextBox)gvw.Rows[rowIndex].Cells[0].FindControl("txtLabel");
                DropDownList ddlControlType = (DropDownList)gvw.Rows[rowIndex].Cells[1].FindControl("ddlControlType");
                DropDownList ddlIsCompulsory = (DropDownList)gvw.Rows[rowIndex].Cells[2].FindControl("ddlIsCompulsory");
                TextBox txtControlValues = (TextBox)gvw.Rows[rowIndex].Cells[3].FindControl("txtControlValues");
                DynamicFormControl entity = new DynamicFormControl();

                if (!string.IsNullOrEmpty(txtLabel.Text))
                {
                    entity.LabelName = txtLabel.Text;
                    entity.ControlType = int.Parse(ddlControlType.Text);
                    entity.IsRequired = ddlIsCompulsory.Text == "Yes" ? true : false;
                    entity.ControlValues = txtControlValues.Text;
                    entity.RowIndex = rowIndex;
                    list.Add(entity);
                }

                rowIndex++;
            }

            return list;
        }

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            list = GetGridList();
            list.Add(new DynamicFormControl { });
            gvw.DataSource = list;
            gvw.DataBind();
            int i = 0;
            foreach (var item in list)
            {
                DropDownList ddlControlType = (DropDownList)gvw.Rows[i].FindControl("ddlControlType");
                DropDownList ddlIsCompulsory = (DropDownList)gvw.Rows[i].FindControl("ddlIsCompulsory");
                ddlControlType.SelectedValue = item.ControlType.ToString();
                ddlIsCompulsory.SelectedValue = item.IsRequired == true ? "Yes" : "No";
                i++;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Page.Validate("SaveDynamicForm");
            if (Page.IsValid)
            {
                var item = GetDynamicForm();
                DynamicForm entity = new DynamicForm()
               {
                   Name = txtFormName.Text.Trim()
               };
                if (GetFormId() != 0)
                    entity.FormID = GetFormId();
                Status status = DynamicFormManager.InsertUpdateDynamicForm(entity, item);
                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("New Form saved successfully.");
                    Clear();
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }
        }


        private List<DynamicFormControl> GetDynamicForm()
        {
           
            List<DynamicFormControl> list = new List<DynamicFormControl>();
            int j = 1;
            foreach (GridViewRow row in gvw.Rows)
            {
                TextBox txtLabel = (TextBox)row.FindControl("txtLabel");
                DropDownList ddlControlType = (DropDownList)row.FindControl("ddlControlType");
                DropDownList ddlIsCompulsory = (DropDownList)row.FindControl("ddlIsCompulsory");
                TextBox txtControlValues = (TextBox)row.FindControl("txtControlValues");

                if (!string.IsNullOrEmpty(txtLabel.Text))
                {
                    DynamicFormControl entity = new DynamicFormControl();
                    entity.LabelName = txtLabel.Text.Trim();
                    entity.ControlType = int.Parse(ddlControlType.SelectedValue);
                    entity.IsRequired = ddlIsCompulsory.SelectedValue == "Yes" ? true : false;
                    entity.Sequence = j;
                    j++;
                    List<DynamicFormControlValue> listControlValue = new List<DynamicFormControlValue>();

                    if (!string.IsNullOrEmpty(txtControlValues.Text.Trim()))
                    {
                        //string[] controlValues = txtControlValues.Text.Trim().Split(',');
                        string[] controlValues = txtControlValues.Text.Trim().Split(new string[] { ",," }, StringSplitOptions.None);
                        for (int i = 0; i < controlValues.Length; i++)
                        {
                            DynamicFormControlValue entityValue = new DynamicFormControlValue()
                            {
                                ControlValue = controlValues[i].ToString()
                            };
                            listControlValue.Add(entityValue);
                        }
                    }
                    if (listControlValue.Count != 0)
                        entity.DynamicFormControlValues.AddRange(listControlValue);

                    list.Add(entity);
                }
            }
            return list;
        }

        private void Clear()
        {
            BindEmptyGrid();
            txtFormName.Text = "";
            if (chkShow.Checked)
                chkShow.Checked = false;
        }

        protected void gvw_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    TextBox txtControlValues = (TextBox)e.Row.FindControl("txtControlValues");
            //    DropDownList ddlControlType = (DropDownList)e.Row.FindControl("ddlControlType");
            //    if (ddlControlType.SelectedValue == "1" || ddlControlType.SelectedValue == "3")
            //    {
            //        txtControlValues.Enabled = false;
            //    }
            //    else
            //        txtControlValues.Enabled = true;
                
            //    //txt_Value.Attributes.Add("readonly", "readonly");
            //    ddlControlType.Attributes.Add("onchange", "javascript:EnableTextbox('" + ddlControlType.ClientID + "','" + txtControlValues.ClientID + "')");
            //}
            
        }

    }
}