﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Base;
using DAL;
using BLL.Manager;
using BLL;
using System.Xml;
using System.Text;

namespace Web.DynamicForms
{
    public partial class DynamicRequestedForm : BasePage
    {
        Table tbl = new Table();  
      
        protected void Page_Init(object sender, EventArgs e)
        {
            string a = string.Empty;
            if (Request.QueryString["formId"] != null)
            {
                hdnValue.Value = Request.QueryString["formId"].ToString();

                lblFormName.Text = DynamicFormManager.GetDynamicFormNameUsingFormId(GetFormId());

                FillControls();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if(Request.QueryString["formEmpId"] !=null)
                    FillControlValues(int.Parse(Request.QueryString["formEmpId"].ToString()));
            }
        }

        private int GetFormId()
        {
            return int.Parse(hdnValue.Value.ToString());
        }

        private void FillControls()
        {
            List<DynamicFormControl> list = DynamicFormManager.GetDynamicFormControlList(GetFormId());
            int i = 0;

            //tbl.Attributes.Add("class", "table");
            tbl.Attributes.Add("class", "tblDynamic");
            tbl.CellPadding = 5;
            tbl.CellSpacing = 10;

            RequiredFieldValidator rfv;
            if (list.Count != 0)
            {
               
                foreach (var item in list)
                {

                    TableCell cell1 = new TableCell();
                    cell1.Style.Add("width", "150px");
                    TableCell cell2 = new TableCell();
                    TableRow tr = new TableRow();
                    tr.Attributes.Add("height", "15px");

                    Label lbl = new Label();
                    lbl.Style.Add("width", "150px");
                    lbl.ID = "lbl" + item.Sequence.ToString();
                    lbl.Text = item.LabelName;
                    cell1.Controls.Add(lbl);

                    rfv = new RequiredFieldValidator();
                    List<DynamicFormControlValue> listDynamicFormControlValue = new List<DynamicFormControlValue>();

                    if (item.ControlType == (int)ControlType.DropDownList || item.ControlType == (int)ControlType.CheckBoxList || item.ControlType == (int)ControlType.RadioButtonList)
                    {
                        listDynamicFormControlValue = DynamicFormManager.GetDynamicFormControlValueList(item.ControlID);
                    }

                    if (item.IsRequired == true && item.ControlType != (int)ControlType.CheckBox && item.ControlType != (int)ControlType.CheckBoxList)
                    {
                        rfv.ID = "rfv" + item.Sequence.ToString();
                        rfv.ValidationGroup = "DynamicForm";
                        rfv.ErrorMessage = item.LabelName + " is required.";
                        rfv.Display = ValidatorDisplay.None;
                        rfv.EnableClientScript = true;
                    }

                    if (item.ControlType == (int)ControlType.TextBox)
                    {
                        TextBox txtBox = new TextBox();
                        txtBox.ID = "txt" + item.Sequence.ToString();
                        txtBox.Attributes.Add("controlId", item.ControlID.ToString());

                        if (item.IsRequired == true)
                        {
                            rfv.ControlToValidate = txtBox.ID; 
                        }

                        cell2.Controls.Add(txtBox);
                    }
                    else if (item.ControlType == (int)ControlType.CheckBox)
                    {
                        CheckBox chkBox = new CheckBox();
                        chkBox.ID = "chkBox" + item.Sequence.ToString();
                        chkBox.Attributes.Add("controlId", item.ControlID.ToString());
                        chkBox.Text = item.LabelName;

                        if (item.IsRequired == true)
                        {
                            //rfv.ControlToValidate = chkBox.ID;
                        }

                        cell2.Controls.Add(chkBox);
                    }
                    else if (item.ControlType == (int)ControlType.DropDownList)
                    {
                        DropDownList ddl = new DropDownList();
                        ddl.ID = "ddl" + item.Sequence.ToString();
                        ddl.Attributes.Add("controlId", item.ControlID.ToString());

                        ddl.Items.Insert(0, "Select");
                        foreach (var obj in listDynamicFormControlValue)
                        {
                            ddl.Items.Add(obj.ControlValue);
                        }

                        if (item.IsRequired == true)
                        {
                            rfv.ControlToValidate = ddl.ID;
                            rfv.InitialValue = "Select";
                        }
                        cell2.Controls.Add(ddl);
                    }
                    else if (item.ControlType == (int)ControlType.CheckBoxList)
                    {
                        CheckBoxList chkBocList = new CheckBoxList();
                        chkBocList.ID = "chkBoxList" + item.Sequence.ToString();
                        chkBocList.Attributes.Add("controlId", item.ControlID.ToString());
                        chkBocList.RepeatColumns = 3;
                        chkBocList.RepeatDirection = RepeatDirection.Vertical;
                        chkBocList.Attributes.Add("class", "chk");

                        foreach (var obj in listDynamicFormControlValue)
                        {
                            chkBocList.Items.Add(obj.ControlValue);
                        }

                        if (item.IsRequired == true)
                        {
                            //rfv.ControlToValidate = chkBocList.ID;
                        }

                        cell2.Controls.Add(chkBocList);
                    }
                    else if (item.ControlType == (int)ControlType.RadioButtonList)
                    {
                        RadioButtonList rbList = new RadioButtonList();
                        rbList.ID = "rbList" + item.Sequence.ToString();
                        rbList.Attributes.Add("controlId", item.ControlID.ToString());
                        rbList.RepeatColumns = 3;
                        rbList.RepeatDirection = RepeatDirection.Vertical;
                        rbList.Attributes.Add("class", "rbl");
                        //rbList.RepeatLayout = RepeatLayout.Table;
                        //rbList.CellSpacing = 50;
                        //rbList.TextAlign = TextAlign.Right;

                        foreach (var obj in listDynamicFormControlValue)
                        {
                            rbList.Items.Add(obj.ControlValue);
                        }

                        if (item.IsRequired == true)
                        {
                            rfv.ControlToValidate = rbList.ID;
                        }

                        cell2.Controls.Add(rbList);
                    }

                    tr.Cells.Add(cell1);
                    tr.Cells.Add(cell2);
                    tbl.Rows.Add(tr);

                    if (item.IsRequired.Value)
                    {
                        if (item.ControlType != (int)ControlType.CheckBox && item.ControlType != (int)ControlType.CheckBoxList)
                        {
                            pnl.Controls.Add(rfv);
                        }
                    }
                }
               
                pnl.Controls.Add(tbl);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Page.Validate("DynamicForm");
            if(Page.IsValid)
            {
                DynamicFormEmployee entity = new DynamicFormEmployee();
                entity.FormID = GetFormId();
                entity.EmployeeID = SessionManager.CurrentLoggedInEmployeeId;
                entity.DateEng = System.DateTime.Now;

                entity.Date = BLL.BaseBiz.GetAppropriateDate(System.DateTime.Now);
                   
                entity.Status = 0;
                entity.StatusName = "";

                List<DynamicFormEmployeeValue> listDynamicFormEmpValue = new List<DynamicFormEmployeeValue>();

                List<DynamicFormControl> list = DynamicFormManager.GetDynamicFormControlList(GetFormId());
                foreach (var item in list)
                {
                    DynamicFormEmployeeValue obj = new DynamicFormEmployeeValue();

                    if (item.ControlType == (int)ControlType.TextBox)
                    {
                        TextBox txt = (TextBox)tbl.FindControl("txt" + item.Sequence.ToString());
                        obj.ControlID = int.Parse(txt.Attributes["controlId"].ToString());
                        if(!string.IsNullOrEmpty(txt.Text))
                            obj.Value = txt.Text.Trim();
                    }
                    else if (item.ControlType == (int)ControlType.DropDownList)
                    {
                        DropDownList ddl = (DropDownList)tbl.FindControl("ddl" + item.Sequence.ToString());
                        obj.ControlID = int.Parse(ddl.Attributes["controlId"].ToString());
                        if(ddl.SelectedIndex != 0)
                            obj.Value = ddl.SelectedItem.Text;
                    }
                    else if (item.ControlType == (int)ControlType.CheckBox)
                    {
                        CheckBox chk = (CheckBox)tbl.FindControl("chkBox" + item.Sequence.ToString());
                        obj.ControlID = int.Parse(chk.Attributes["controlId"].ToString());
                        if (chk.Checked)
                            obj.Value = "Yes";
                        else
                            obj.Value = "No";
                    }
                    else if (item.ControlType == (int)ControlType.CheckBoxList)
                    {
                        CheckBoxList chkboxlist = (CheckBoxList)tbl.FindControl("chkBoxList" + item.Sequence.ToString());
                        obj.ControlID = int.Parse(chkboxlist.Attributes["controlId"].ToString());

                        string a = "";

                        foreach (ListItem it in chkboxlist.Items)
                        {
                            if(it.Selected)
                            {
                                a = a + it.ToString() + ",";
                            }
                        }
                        
                        obj.Value = a.Substring(0, a.Length - 1).ToString();

                        
                    }
                    else if (item.ControlType == (int)ControlType.RadioButtonList)
                    {
                        RadioButtonList rbList = (RadioButtonList)tbl.FindControl("rbList" + item.Sequence.ToString());
                        obj.ControlID = int.Parse(rbList.Attributes["controlId"].ToString());

                        foreach(ListItem it in rbList.Items)
                        {
                            if (it.Selected)
                            {
                                obj.Value = it.Text;
                            }

                        }
                       
                    }

                    listDynamicFormEmpValue.Add(obj);
                }

                entity.DynamicFormEmployeeValues.AddRange(listDynamicFormEmpValue);

                Status status = new Status();

                DynamicFormEmployeeStatusHistory hist = new DynamicFormEmployeeStatusHistory();

                if (Request.QueryString["formEmpId"] != null)
                    entity.DFEID = int.Parse(Request.QueryString["formEmpId"].ToString());
                status = DynamicFormManager.InsertUpdateDynamicRequestForm(entity, hist);

                if (status.IsSuccess)
                {
                    NewMessage.ShowNormalMessage("Dynamic Form Employee saved successfully.");
                }
                else
                {
                    NewMessage.ShowWarningMessage(status.ErrorMessage);
                }
            }

           
        }

        private void FillControlValues(int dynamicFormEmpId)
        {
            List<DynamicFormControl> list = DynamicFormManager.GetDynamicFormControlList(GetFormId());

            string value = string.Empty;
            foreach (var item in list)
            {
                if (item.ControlType == (int)ControlType.TextBox)
                {
                    TextBox txt = (TextBox)tbl.FindControl("txt" + item.Sequence.ToString());
                    value = DynamicFormManager.GetDynamicFormEmpValues(dynamicFormEmpId, int.Parse(txt.Attributes["controlId"].ToString()));
                    if(!string.IsNullOrEmpty(value))
                        txt.Text = value;
                }
                else if (item.ControlType == (int)ControlType.DropDownList)
                {
                    DropDownList ddl = (DropDownList)tbl.FindControl("ddl" + item.Sequence.ToString());
                    value = DynamicFormManager.GetDynamicFormEmpValues(dynamicFormEmpId, int.Parse(ddl.Attributes["controlId"].ToString()));
                    if (!string.IsNullOrEmpty(value))
                        ddl.Text = value;
                }
                else if (item.ControlType == (int)ControlType.CheckBox)
                {
                    CheckBox chk = (CheckBox)tbl.FindControl("chkBox" + item.Sequence.ToString());
                    value = DynamicFormManager.GetDynamicFormEmpValues(dynamicFormEmpId, int.Parse(chk.Attributes["controlId"].ToString()));
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (value == "Yes")
                            chk.Checked = true;
                        else
                            chk.Checked = false;
                    }
                }
                else if (item.ControlType == (int)ControlType.CheckBoxList)
                {
                    CheckBoxList chkboxlist = (CheckBoxList)tbl.FindControl("chkBoxList" + item.Sequence.ToString());
                    value = DynamicFormManager.GetDynamicFormEmpValues(dynamicFormEmpId, int.Parse(chkboxlist.Attributes["controlId"].ToString()));
                    if (!string.IsNullOrEmpty(value))
                    {
                        string[] indValue = value.Split(',');
                        for (int i = 0; i < indValue.Length; i++)
                        {
                            foreach (ListItem itemCheckboxList in chkboxlist.Items)
                            {
                                if (itemCheckboxList.Text == indValue[i])
                                    itemCheckboxList.Selected = true;
                            }
                        }
                    }
                }
                else if (item.ControlType == (int)ControlType.RadioButtonList)
                {
                    RadioButtonList rbList = (RadioButtonList)tbl.FindControl("rbList" + item.Sequence.ToString());
                    value = DynamicFormManager.GetDynamicFormEmpValues(dynamicFormEmpId, int.Parse(rbList.Attributes["controlId"].ToString()));
                   
                    if (!string.IsNullOrEmpty(value))
                    {
                        foreach (ListItem itemRb in rbList.Items)
                        {
                            if (itemRb.Text == value)
                                itemRb.Selected = true;
                        }
                    }
                }


            }
        }

    }
}