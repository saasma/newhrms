﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Xml;



namespace RigoAttendanceService
{
    class Program
    {
 public Services services = new Services();
        private static XmlDocument appDoc = new XmlDocument();
        /// <summary>
        /// This method will be called if the user closes the console window or presses CTRL+C
        /// </summary>
        /// <param name="ctrlType"></param>
        /// <returns>always true</returns>
        private static bool ConsoleCtrlCheck(CtrlTypes ctrlType)
        {
            Program prog = new Program();
            return true;
        }

        static void Main(string[] args)
        {
           string Arg = "";
            if (args.Length > 0)
            {
                Console.WriteLine("args..." + args[0]);
                Arg = args[0];
            }

            //if (Arg == "")
            //    return;

            string interval = "60";// default 60 min
            Console.WriteLine("Starting...");
            try
            {

                appDoc.Load(@"RigoAttendanceService.exe.config");
                try
                {
                    interval = appDoc.SelectSingleNode("//configuration/" + "/@" + "interval").SelectSingleNode(".").Value;
                }
                catch
                {
                    interval = "60";
                }
            }
            catch (Exception exp)
            {

            }

            //HandlerRoutine hr = new HandlerRoutine(ConsoleCtrlCheck);
            //// we have to keep the handler routine alive during the execution of the program,
            //// because the garbage collector will destroy it after any CTRL event
            //GC.KeepAlive(hr);
            //SetConsoleCtrlHandler(hr, true);


            //Services services = new Services();
            //services.ConnectionStart();

            //Timer t = new Timer(TimerObject, null, 0, (int)TimeSpan.FromMinutes(double.Parse(interval)).TotalMilliseconds);

            ////loop here forever         
            //for (; ; ) { System.Threading.Thread.Sleep(10000000); }


            TimerObject(null, Arg);
        }

        private static void TimerObject(Object state,string Arg)
        {
            Services services = new Services();
            services.ConnectionStart(Arg);
        }

        #region Helper Class [Disconnects the device At App Close]

        /// <summary>
        /// This function sets the handler for kill events.
        /// </summary>
        /// <param name="Handler"></param>
        /// <param name="Add"></param>
        /// <returns></returns>
        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);

        //delegate type to be used of the handler routine
        public delegate bool HandlerRoutine(CtrlTypes CtrlType);

        // control messages
        public enum CtrlTypes
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }

        #endregion


        
    }

}

/*
<Device>
    <sn>1</sn>
    <IPAddress>192.168.0.25</IPAddress>
    <Port>5005</Port>
	<DeviceType>ZDC</DeviceType>
  </Device>
 */
