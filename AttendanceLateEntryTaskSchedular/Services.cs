﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using System.Net;

using System.Net.Mail;
using System.ServiceModel;

using System.Xml;
using System.IO;
using NLog;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

using RigoAttendanceService.AttServices;


using System.Web.Configuration;
using System.Net.Configuration;
using System.Configuration;
using System.ServiceModel.Configuration;



namespace RigoAttendanceService
{


    public class Services
    {


        private static XmlDocument appDoc = new XmlDocument();
       // private static Logger logger = LogManager.GetCurrentClassLogger();
        AttServices.AttendanceImportNewSoapClient client1 = new AttServices.AttendanceImportNewSoapClient();
        public string keyValue = "SendMailToLateEntryAttendanceKey";
        public static string API_KEY = "";

        //First Methods that is called by the program, That establishes the connection.
        //TODO: IP address of the adjacent FP device is hardcoded for my ease in the prototype build.
        public void ConnectionStart(string argmnt)
        {

            try
            {

                API_KEY = ConfigurationManager.AppSettings["apiKey"].ToString();
            }
            catch
            {
                Console.WriteLine("API key not defined in the configuration file.");
                WriteToLogFile("API key not defined in the configuration file.");
                
                return;
            }

            
            string Arg = argmnt.ToLower();

      
            if (Arg == "birthday")
            {
                BirthdayScheduler();
            }
            if (Arg == "birthdaywishmailtoeachemployee")
            {
                BirthdayWishScheduler();
            }
            if (Arg == "dailyabs")
            {
                AbsentSchedulerOfDays();//previous day and today 
            }

            else if (Arg == "weeklyabs")
            {
                WeeklyAbsentScheduler();
            }

            else if (Arg == "late")
            {
                LateEntryScheduler();
            }
            else if (Arg == "today-lateandabs")
            {
               LateEntryScheduler();
                AbsentSchedulerDaily();
            }

            else if (Arg == "today-lateandabs-new")
            {
                client1.LateEntryScheduler_DailyNew(API_KEY);
                client1.AbsentScheduler_DailyNew(API_KEY);
            }

            else if (Arg == "weekly-late-mail-to-employees")
            {
                WeeklyLateMailToEmployees();
            }
            //else //for citizen already exits scheduler same as before
            //{
            //    if (client1.EnableWeeklyAbsentScheduler())
            //        WeeklyAbsentScheduler();
            //    if (client1.EnableDailyLateEntryWithAbsent())
            //    {
            //        LateEntryScheduler();
            //        AbsentSchedulerDaily();//previous day and today
            //    }
            //}
        }

        protected void AbsentSchedulerOfDays()
        {

            try
            {
                SetHTTPS();
                DateTime StartDayOfWeek = client1.GetCurrentDateAndTime().AddDays(-1).Date;
                DateTime EndDateOfWeek = client1.GetCurrentDateAndTime().Date;
                List<RigoAttendanceService.AttServices.GetAbsentListForEmailResult> ListAbsentEmployes = client1.GetAbsentListForEmail(StartDayOfWeek, EndDateOfWeek, keyValue).ToList();
                List<RigoAttendanceService.AttServices.AbsentEmpIDBO> _AbsentEmpIDList = client1.GetAbsentEmpIDList(StartDayOfWeek, EndDateOfWeek, keyValue).ToList();
                List<RigoAttendanceService.AttServices.SupervisorEmails> _SupervisorEmails = client1.GetSupervisorEmailListForMail(StartDayOfWeek, EndDateOfWeek, keyValue).ToList();

                if (ListAbsentEmployes.Count > 0)
                {
                    sendMailAbsentWeeklyForEmployee(ListAbsentEmployes.Where(x => !string.IsNullOrEmpty(x.Email)).ToList(), _AbsentEmpIDList);
                    sendMailAbsentWeeklyForSupervisor(ListAbsentEmployes.Where(x => !string.IsNullOrEmpty(x.SupervisorEmail) && string.IsNullOrEmpty(x.Email)).ToList(), _SupervisorEmails); ;
                }
            }

            catch (Exception exp)
            {
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }

        /// <summary>
        /// If employee late for more than for the weekly peirod send the list of lates to the employees
        /// </summary>
        public void WeeklyLateMailToEmployees()
        {
            try
            {
                SetHTTPS();

                GetAutoWeeklyLateMailListToEmployeeResult[] list = client1.GetAutoWeeklyMailListToEmployee(keyValue);


                if (list.Count() == 0)
                {
                    WriteToLogFile("\n not Late employee found : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n");
                }
                else
                {
                    WriteToLogFile("\n Late employee found : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n");
                }

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                string SenderEmail = settings.Smtp.From;

                DateTime endDate = DateTime.Now.Date; //friday
                DateTime startDate = DateTime.Now.Date.AddDays(-5); // sunday

                string subject = "Your Weekly late mail";

                // sent to HR
                string bodyTemplate =
                    @"
                    Dear #Name#,
                    <br><br>
                    Please note your repeated late attendance as following for your needful attention.
                    We strictly advise all to be abided with CC#033/072/73– “Office Timing and Lunch Hour”, 
                    and carry out your office sign in by 9:30 AM.
                    <br><br>
                    #List#
                    <br><br>
                    Regards <br>
                    HR
                ";

                List<int> mailSentEINList = new List<int>();

                if (list.Length > 0)
                {

                    // 1. Mail to each Employee
                    for (int i = 0; i < list.Length; i++)
                    {
                        GetAutoWeeklyLateMailListToEmployeeResult emp = list[i];

                        bool exists = false;
                        // should be repated then only send the mail so check for repeation
                        for (int j = i + 1; j < list.Length; j++)
                        {
                            if (list[j].EIN == emp.EIN)
                            {
                                exists = true;
                                break;
                            }
                        }

                        // now process to send the mail to the Employee
                        if (exists)
                        {
                            string table = "<table style='border:1px solid'> <tr> <th>Date</th> <th>Day Type</th><th> In </th><th> Out </th>  </tr>";

                            List<GetAutoWeeklyLateMailListToEmployeeResult> newList = list.Where(x => x.EIN == emp.EIN).ToList();

                            foreach (var item in newList)
                            {
                                table +=
                                    string.Format("<tr> <td style='padding:5px'>{0}</td> <td style='padding:5px'>{1}</td><td style='padding:5px'> {2} </td><td style='padding:5px'> {3} </td>  </tr>"
                                    , item.Date.ToString("yyyy-MMM-dd"),
                                    item.DayType,
                                    item.ClockInText,
                                    item.ClockOutText
                                    );

                            }

                            table += "</table>";

                            // mail to each employee
                            string body = bodyTemplate.Replace("#Name#", emp.Name)
                                            .Replace("#List#", table);

                            if (!string.IsNullOrEmpty(emp.EMail))
                            {
                                client1.SendMailAsync(emp.EMail, body, subject, "", SenderEmail);
                            }
                        }


                        mailSentEINList.Add(emp.EIN);
                    }


                    //string body = "<br/><br/>Following employees are absent today <br/><br/>";
                    //body += "<table>";

                    //foreach (var emp in mail.EmployeeList)
                    //{
                    //    body += "<tr> <td> " + emp.Name + " </td> </tr>";
                    //}

                    //body += "</table>";

                    //string errorMsg = "";
                    //if (!string.IsNullOrEmpty(mail.HREmail))
                    //{
                    //    SMTPHelper.SendMail(mail.HREmail, body, subject, ref errorMsg);

                    //}






                    // 2. Sent to each Manager or Approval 1
                    List<string> managerEmails = list.Where(x => x.ApprovalEMail != null)
                        .Select(x => x.ApprovalEMail).Distinct().ToList();

                    foreach (string managerEmail in managerEmails)
                    {


                        string body = string.Format("<br/><br/>Following employees are late for the week {0} to {1} <br/><br/>",
                            startDate.ToString("yyyy-MMM-dd"), endDate.ToString("yyyy-MMM-dd"));
                        body += "<table style='border:1px solid'> <tr> <th>EIN</th> <th>Name</th> <th>Date</th> <th>Day Type</th><th> In </th><th> Out </th>  </tr>";

                        bool hasEmp = false;
                        foreach (var emp in list.Where(x => x.ApprovalEMail != null && x.ApprovalEMail.Equals(managerEmail)))
                        {
                            hasEmp = true;


                            body +=
                                    string.Format("<tr> <td style='padding:5px'>{0}</td> <td style='padding:5px'>{1}</td> <td style='padding:5px'>{2}</td> <td style='padding:5px'>{3}</td><td style='padding:5px'> {4} </td><td style='padding:5px'> {5} </td>  </tr>"
                                    ,
                                    emp.EIN,
                                    emp.Name,
                                    emp.Date.ToString("yyyy-MMM-dd"),
                                    emp.DayType,
                                    emp.ClockInText,
                                    emp.ClockOutText
                                    );

                        }

                        body += "</table>";


                        if (hasEmp)
                        {
                            if (!string.IsNullOrEmpty(managerEmail))
                            {
                                //SMTPHelper.SendMail(managerEmail, body, subject, ref errorMsg);
                                client1.SendMailAsync(managerEmail, body, "Weekly late list", "", SenderEmail);
                            }
                        }
                    }



                    // 3. Sent all employee late mail list to sachin.rayamajhi@prabhubank.com,riwas.shrestha@prabhubank.com

                    string body1 = string.Format("<br/><br/>Following employees are late for the week {0} to {1} <br/><br/>",
                        startDate.ToString("yyyy-MMM-dd"), endDate.ToString("yyyy-MMM-dd"));
                    body1 += "<table style='border:1px solid'> <tr> <th>EIN</th> <th>Name</th> <th>Date</th> <th>Day Type</th><th> In </th><th> Out </th>  </tr>";


                    foreach (var emp in list)
                    {

                        body1 +=
                                string.Format("<tr> <td style='padding:5px'>{0}</td> <td style='padding:5px'>{1}</td> <td style='padding:5px'>{2}</td> <td style='padding:5px'>{3}</td><td style='padding:5px'> {4} </td><td style='padding:5px'> {5} </td>  </tr>"
                                ,
                                emp.EIN,
                                emp.Name,
                                emp.Date.ToString("yyyy-MMM-dd"),
                                emp.DayType,
                                emp.ClockInText,
                                emp.ClockOutText
                                );

                    }

                    body1 += "</table>";



                    // (!string.IsNullOrEmpty(managerEmail))
                    {
                        //SMTPHelper.SendMail(managerEmail, body, subject, ref errorMsg);
                        client1.SendMailAsync("sachin.rayamajhi@prabhubank.com,riwas.shrestha@prabhubank.com", body1, "Weekly late list", "", SenderEmail);
                        // client1.SendMailAsync("purushottam@rigonepal.com,purushottam.dangal@gmail.com", body1, "Weekly late list", "", SenderEmail);
                        WriteToLogFile("\n Late Entry Email Sent : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n");
                    }
                }
            }
            catch (Exception exp)
            {
                // Console.WriteLine(exp.ToString());
                //logger.Log(LogLevel.Error, "\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }

        }
       
        protected void AbsentSchedulerDaily()
        {

            try
            {

                SetHTTPS();
                List<RigoAttendanceService.AttServices.AttendanceAllEmployeeReportResult> ListDailyAttendance = client1.GetAttendanceDailyReport(client1.GetCurrentDateAndTime().Date, 0, "", keyValue).ToList();
                List<RigoAttendanceService.AttServices.AttendanceAllEmployeeReportResult> ListAbsentEmployes = ListDailyAttendance.Where(x => x.AtteState == 9 && x.Type != 3).ToList();

                if (ListAbsentEmployes.Count == 0)
                {

                    WriteToLogFile("\n Absent employee not found." + DateTime.Now.ToString() + "  : \n");
                }

                if (ListAbsentEmployes.Count > 0)
                    sendMailAbsent(ListAbsentEmployes);
            }

            catch (Exception exp)
            {
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }

       

        protected void LateEntryScheduler()
        {

            try
            {

                SetHTTPS();
                List<RigoAttendanceService.AttServices.AttendanceAllEmployeeReportResult> ListDailyAttendance = client1.GetAttendanceDailyReport(client1.GetCurrentDateAndTime().Date, 0, "", keyValue).ToList();
                List<RigoAttendanceService.AttServices.AttendanceAllEmployeeReportResult> ListLateEmployes = ListDailyAttendance.Where(x => (x.InState == 3) && x.AtteState == 11 && x.Type != 3).ToList();
                if (ListLateEmployes.Count == 0)
                    WriteToLogFile("\n Late In employee not found." + DateTime.Now.ToString() + "  : \n");

                if (ListLateEmployes.Count > 0)
                    sendMailLateEntry(ListLateEmployes);
            }
            catch (Exception exp)
            {
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }

        /// <summary>
        /// Sends Birthday employees list to all employees
        /// </summary>
        protected void BirthdayScheduler()
        {

            try
            {

                SetHTTPS();


                List<RigoAttendanceService.AttServices.BirthDayBo> ListBirthDay = client1.GetBirthDayEmploye(keyValue).ToList();

                if (ListBirthDay.Count == 0)
                    WriteToLogFile("\n No Birthday today." + DateTime.Now.ToString() + "  : \n");

                if (ListBirthDay.Count > 0)
                    sendBirthdayMail(ListBirthDay);
            }
            catch (Exception exp)
            {
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }

        /// <summary>
        /// Sends Birthday employees list to all employees
        /// </summary>
        protected void BirthdayWishScheduler()
        {

            try
            {

                SetHTTPS();


                List<RigoAttendanceService.AttServices.BirthDayBo> ListBirthDay = client1.GetBirthDayEmploye(keyValue).ToList();

                if (ListBirthDay.Count == 0)
                    WriteToLogFile("\n No Birthday today." + DateTime.Now.ToString() + "  : \n");

                if (ListBirthDay.Count > 0)
                    sendBirthdayWishMail(ListBirthDay);
            }
            catch (Exception exp)
            {
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }

        protected void WeeklyAbsentScheduler()
        {
            try
            {
                SetHTTPS();

                DateTime StartDayOfWeek = client1.GetCurrentDateAndTime().AddDays(-6).Date;
                DateTime EndDateOfWeek = client1.GetCurrentDateAndTime().Date;
                
                List<RigoAttendanceService.AttServices.GetAbsentListForEmailResult> ListAbsentEmployes = client1.GetAbsentListForEmail(StartDayOfWeek, EndDateOfWeek, keyValue).ToList();
                List<RigoAttendanceService.AttServices.AbsentEmpIDBO> _AbsentEmpIDList = client1.GetAbsentEmpIDList(StartDayOfWeek, EndDateOfWeek, keyValue).ToList();
                List<RigoAttendanceService.AttServices.SupervisorEmails> _SupervisorEmails = client1.GetSupervisorEmailListForMail(StartDayOfWeek, EndDateOfWeek, keyValue).ToList();

                if (ListAbsentEmployes.Count > 0)
                {
                    sendMailAbsentWeeklyForEmployee(ListAbsentEmployes.Where(x => !string.IsNullOrEmpty(x.Email)).ToList(), _AbsentEmpIDList);
                    sendMailAbsentWeeklyForSupervisor(ListAbsentEmployes.Where(x => !string.IsNullOrEmpty(x.SupervisorEmail) && string.IsNullOrEmpty(x.Email)).ToList(), _SupervisorEmails); ;
                }
            }

            catch (Exception exp)
            {
               // Console.WriteLine(exp.ToString());
                //logger.Log(LogLevel.Error, "\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }


        protected void sendBirthdayMail(List<RigoAttendanceService.AttServices.BirthDayBo> ListBirthdayEmployee)
        {

            try
            {
                RigoAttendanceService.AttServices.EmailContent dbMailContent = client1.GetMailContent(client1.GetBirthDayEmailContentID(), keyValue);

                string birthDayEmployee = "";
                foreach (RigoAttendanceService.AttServices.BirthDayBo _item in ListBirthdayEmployee)
                {
                    birthDayEmployee = birthDayEmployee + _item.Name + "<br>";
                }

                List<int> AllEmpIDs = client1.GetAllEmployeeIDs(keyValue).ToList();
                if (dbMailContent == null)
                {
                    WriteToLogFile("\n Birthday Template not defined" + DateTime.Now.ToString() + "  : \n");
                    return;
                }

                string todayDate = client1.GetCurrentDateAndTime().ToString("yyyy/MMM/d");
                string subject = dbMailContent.Subject;
                string body = dbMailContent.Body;
                int count = 0;
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                string SenderEmail = settings.Smtp.From;
                 body = body.Replace("{employeelist}", birthDayEmployee);
                 foreach (int _Empid in AllEmpIDs)
                 {
                    
                         string EmailTo = client1.GetEmployeeCIEmail(_Empid);
                         if (string.IsNullOrEmpty(EmailTo))
                         {
                             // WriteToLogFile("\n Email address not found for Employee " + EmployeeName + " " + DateTime.Now.ToString() + "  : \n");
                         }
                         else
                         {
                             bool isSendSuccess = client1.SendMailAsync(EmailTo, body, subject, "", SenderEmail);
                             if (isSendSuccess)
                                 count++;
                         }
                     
                 }

                if (count > 0)
                {
                    WriteToLogFile("\n Email Send successfully for " + count + "Employees " + DateTime.Now.ToString() + "  : \n");
                    return;
                }
                else
                {
                    WriteToLogFile("\n error while sending email " + DateTime.Now.ToString() + "  : \n");
                    return;
                }
            }

            catch (Exception exp)
            {
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }

        protected void sendBirthdayWishMail(List<RigoAttendanceService.AttServices.BirthDayBo> ListBirthdayEmployee)
        {

            try
            {

                RigoAttendanceService.AttServices.EmailContent dbMailContent = client1.GetMailContent(client1.GetBirthDayWishEmailContentID(), keyValue);


                if (dbMailContent == null)
                {
                    WriteToLogFile("\n Birthday Template not defined" + DateTime.Now.ToString() + "  : \n");
                    return;
                }

                string todayDate = client1.GetCurrentDateAndTime().ToString("yyyy/MMM/d");
                string subject = dbMailContent.Subject;
                
                int count = 0;
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                string SenderEmail = settings.Smtp.From;
                //body = body.Replace("{employeelist}", birthDayEmployee);
                foreach (var item in ListBirthdayEmployee)
                {
                    string body = dbMailContent.Body;
                    body = body.Replace("#Employee Name#", item.Name);

                    if(!string.IsNullOrEmpty(item.Email))
                    {
                        bool isSendSuccess = client1.SendMailAsync(item.Email, body, subject, "", SenderEmail);
                        if (isSendSuccess)
                            count++;
                    }

                }

                if (count > 0)
                {
                    WriteToLogFile("\n Email Send successfully for " + count + "Employees " + DateTime.Now.ToString() + "  : \n");
                    return;
                }
                else
                {
                    WriteToLogFile("\n error while sending email " + DateTime.Now.ToString() + "  : \n");
                    return;
                }
            }

            catch (Exception exp)
            {
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }
        protected void sendMailLateEntry(List<RigoAttendanceService.AttServices.AttendanceAllEmployeeReportResult> ListLateEmployes)
        {

            try
            {

                RigoAttendanceService.AttServices.EmailContent dbMailContent = client1.GetMailContent(client1.GetLateEntryEmailContentID(), keyValue);
                if (dbMailContent == null)
                {
                    //Console.WriteLine("Late Entry Template not defined.");
                    //logger.Log(LogLevel.Error, "\n Late Entry Template not defined" + DateTime.Now.ToString() + "  : \n");
                    WriteToLogFile("\n Late Entry Template not defined" + DateTime.Now.ToString() + "  : \n");
                    return;
                }
                string todayDate = client1.GetCurrentDateAndTime().ToString("yyyy/MMM/d");
                string subject = dbMailContent.Subject.Replace("#TodayDate#", todayDate);
                int count = 0;

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                string SenderEmail = settings.Smtp.From;

                List<RigoAttendanceService.AttServices.GetForDeviceMappingResult> _GetForDeviceMappingResult = client1.GetDeviceMapping("SendMailToLateEntryAttendanceKey").ToList();
                
                foreach (RigoAttendanceService.AttServices.AttendanceAllEmployeeReportResult _item in ListLateEmployes)
                {

                    if (_item.Type == 2 && _item.DayValue != "WH/2")//weekly holiday dot not send mail
                    {
                    }
                    else
                    {
                        RigoAttendanceService.AttServices.GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == _item.EmployeeId);
                        if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                        {
                            if (!_DeviceMappingResult.IsEnabledSkipLateEntry.Value)
                            {
                                

                                string body = dbMailContent.Body.Replace("#LateHour#", _item.InRemarksText.Replace("Late In", "").Trim());
                                body = body.Replace("#TodayDate#", todayDate);
                                body = body.Replace("#Employee#", _item.EmplooyeeName);
                                body = body.Replace("#url#", getLateEntryLink());
                                string EmailTo = client1.GetEmployeeCIEmail(_item.EmployeeId.Value);
                                if (string.IsNullOrEmpty(EmailTo))
                                {
                                   // Console.WriteLine("Email address not found for Employee.");
                                    //logger.Log(LogLevel.Error, "\n Email address not found for Employee" + DateTime.Now.ToString() + "  : \n");
                                    WriteToLogFile("\n Email address not found for Employee " + _item.EmplooyeeName + " " + DateTime.Now.ToString() + "  : \n");
                                    //  return;
                                }
                                else
                                {
                                    bool isSendSuccess = client1.SendMailAsync(EmailTo, body, subject, "", SenderEmail);
                                    
                                    if (isSendSuccess)
                                        count++;
                                }

                            }
                        }
                    }

                   
                }

                if (count > 0)
                {

                    // Console.WriteLine("Email Send successfully for " + count + " Late Entry Employees");
                    //logger.Log(LogLevel.Error, "\n Email Send successfully for " + count + "Late Entry Employees" + DateTime.Now.ToString() + "  : \n");
                    WriteToLogFile("\n Email Send successfully for " + count + "Late Entry Employees " + DateTime.Now.ToString() + "  : \n");
                    return;
                }
                else
                {
                    // logger.Log(LogLevel.Error, "\n error while sending email " + DateTime.Now.ToString() + "  : \n");
                    //Console.WriteLine("error while sending email " + DateTime.Now.ToString());
                    WriteToLogFile("\n error while sending email " + DateTime.Now.ToString() + "  : \n");
                    return;
                }
            }

            catch (Exception exp)
            {
                //Console.WriteLine(exp.ToString());
                //logger.Log(LogLevel.Error, "\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }


        protected string getLateEntryLink()
        {

            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
            ServiceModelSectionGroup serviceModelSectionGroup = ServiceModelSectionGroup.GetSectionGroup(configuration);
            ClientSection clientSection = serviceModelSectionGroup.Client;
            var el = clientSection.Endpoints[0];

            string[] endpointStr = el.Address.ToString().Split('/');
            string formatteURL = "";
            if (endpointStr.Length > 0)
            {
                string url = endpointStr[0] + "//" + endpointStr[2] + "/" + endpointStr[3] + "/Employee/EmpAttTimeCommentRequestList.aspx?linkreq=true";
                 formatteURL = "<a href=" + url + ">" + url + "</a>";
            }

            return formatteURL;
        }

        protected void sendMailAbsentWeeklyForEmployee(List<GetAbsentListForEmailResult> ListAbsentEmployes, List<AbsentEmpIDBO> AbsentEmpIDList)
        {
            try
            {
                if (!ListAbsentEmployes.Any())
                    return;

                if (!AbsentEmpIDList.Any())
                    return;

                RigoAttendanceService.AttServices.EmailContent dbMailContentForEmployee = client1.GetMailContent(client1.GetWeeklyAbsentEmailContentIDForEmployee(), keyValue);

                if (dbMailContentForEmployee == null)
                {
                   // Console.WriteLine("Weekly Absent Template not defined for employee.");
                    //logger.Log(LogLevel.Error, "\n Weekly Absent Template not defined for employee" + DateTime.Now.ToString() + "  : \n");
                    WriteToLogFile("\n Weekly Absent Template not defined for employee " + DateTime.Now.ToString() + "  : \n");
                    return;
                }

                string todayDate = client1.GetCurrentDateAndTime().ToString("yyyy/MMM/d");

                string BodyEmployee = dbMailContentForEmployee.Body;
                string body = string.Empty;
                string subject = string.Empty;
                int count = 0;

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                string SenderEmail = settings.Smtp.From;

                List<RigoAttendanceService.AttServices.GetForDeviceMappingResult> _GetForDeviceMappingResult = client1.GetDeviceMapping("SendMailToLateEntryAttendanceKey").ToList();
                foreach (AbsentEmpIDBO EmpIDInfo in AbsentEmpIDList)
                {

                    RigoAttendanceService.AttServices.GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == EmpIDInfo.EmpID);
                    List<GetAbsentListForEmailResult> _ListOfSelectedEmployee = ListAbsentEmployes.Where(x => x.EmployeeId == EmpIDInfo.EmpID).ToList();


                    string strDates = string.Empty;
                    if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                    {
                        if (!_DeviceMappingResult.IsEnabledSkipLateEntry.Value)
                        {
                            foreach (GetAbsentListForEmailResult ListAbs in _ListOfSelectedEmployee)
                            {
                                if (string.IsNullOrEmpty(strDates))
                                    strDates = ListAbs.Date.ToString("MMM/dd/yyyy");
                                else
                                    strDates += "," + ListAbs.Date.ToString("MMM/dd/yyyy");
                            }

                            body = BodyEmployee.Replace("#Dates#", strDates);
                            body = body.Replace("#Employee#", EmpIDInfo.Name);

                            if (string.IsNullOrEmpty(EmpIDInfo.Email))
                            {
                                WriteToLogFile("\n Email address not found for Employee " + EmpIDInfo.Name + " " + DateTime.Now.ToString() + "  : \n");
                            }
                            else
                            {

                                bool isSendSuccess = client1.SendMailAsync(EmpIDInfo.Email, body, dbMailContentForEmployee.Subject, "", SenderEmail);
                                if (isSendSuccess)
                                    count++;
                            }
                        }
                    }
                }

                if (count > 0)
                {

                    WriteToLogFile("\n Email Send successfully for " + count + " Absent Employees " + DateTime.Now.ToString() + "  : \n");
                    return;
                }
                else
                {
                    WriteToLogFile("\n error while sending email " + DateTime.Now.ToString() + "  : \n");
                    return;
                }
            }
            catch (Exception exp)
            {
                //Console.WriteLine(exp.ToString());
                //logger.Log(LogLevel.Error, "\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }

        protected void sendMailAbsentWeeklyForSupervisor(List<GetAbsentListForEmailResult> ListAbsentEmployes, List<SupervisorEmails> ListSupervisorEmails)
        {
            try
            {

                if (!ListAbsentEmployes.Any())
                    return;

                if (!ListSupervisorEmails.Any())
                    return;

                RigoAttendanceService.AttServices.EmailContent dbMailContentForSupervisor = client1.GetMailContent(client1.GetWeeklyAbsentEmailContentIDForSupervisor(), keyValue);

                if (dbMailContentForSupervisor == null)
                {
                   // Console.WriteLine("Weekly Absent Template not defined for supervisor.");
                    //logger.Log(LogLevel.Error, "\n Weekly Absent Template not defined for supervisor" + DateTime.Now.ToString() + "  : \n");
                    WriteToLogFile("\n Weekly Absent Template not defined for supervisor" + DateTime.Now.ToString() + "  : \n");
                    return;
                }

                string todayDate = client1.GetCurrentDateAndTime().ToString("yyyy/MMM/d");
                int count = 0;
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                string SenderEmail = settings.Smtp.From;

                List<RigoAttendanceService.AttServices.GetForDeviceMappingResult> _GetForDeviceMappingResult = client1.GetDeviceMapping("SendMailToLateEntryAttendanceKey").ToList();

                //send mail for Employee---------------------------------------

                string strHTMLTbl = string.Empty;
                foreach (SupervisorEmails _SupervisorEmails in ListSupervisorEmails)
                {

                    List<GetAbsentListForEmailResult> _ListOfSelectedEmployee = ListAbsentEmployes.Where(x => x.SupervisorEmail == _SupervisorEmails.SupervisorEmail).ToList();
                    string strDates = string.Empty;
                    //  body = "<br/><br/>Following employees are absent today <br/><br/>";
                    strHTMLTbl += "<table>";
                    foreach (GetAbsentListForEmailResult ListAbs in _ListOfSelectedEmployee)
                    {
                        RigoAttendanceService.AttServices.GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == ListAbs.EmployeeId);
                        if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                        {
                            if (!_DeviceMappingResult.IsEnabledSkipLateEntry.Value)
                            {
                                if (string.IsNullOrEmpty(strHTMLTbl))
                                    strHTMLTbl = "<tr>" + "<td> " + ListAbs.NAME + "    </td>" + "<td> " + ListAbs.Date.ToString("MMM/dd/yyyy") + " </td>" + "</tr>";
                                // strDates = ListAbs.Date.ToShortDateString();
                                else
                                    //strDates += "," + ListAbs.Date.ToShortDateString();
                                    strHTMLTbl += "<tr>" + "<td> " + ListAbs.NAME + "    </td>" + "<td> " + ListAbs.Date.ToString("MMM/dd/yyyy") + " </td>" + "</tr>";
                            }
                        }
                    }

                    strHTMLTbl += "</table>";
                    string bodycontents = dbMailContentForSupervisor.Body.Replace("#List#", strHTMLTbl);
                    string[] Emails = _SupervisorEmails.SupervisorEmail.Split(',');

                    //string EmailTo = client1.GetEmployeeCIEmail(_SupervisorEmails.EmpID);

                    //if (string.IsNullOrEmpty(EmailTo))
                    //{

                    //    Console.WriteLine("Email address not found for Employee.");
                    //    logger.Log(LogLevel.Error, "\n Email address not found for Employee" + DateTime.Now.ToString() + "  : \n");
                    //    // return;
                    //}
                    //else
                    //{


                    foreach (string ToEmail in Emails)
                    {
                        bool isSendSuccess = client1.SendMailAsync(ToEmail, bodycontents, dbMailContentForSupervisor.Subject, "", SenderEmail);
                        if (isSendSuccess)
                            count++;
                        // }
                    }
                }

                if (count > 0)
                {
                    //Console.WriteLine("Email Send successfully for  " + count + " Supervisor");
                    //logger.Log(LogLevel.Error, "\n Email Send successfully for " + count + " Supervisor" + DateTime.Now.ToString() + "  : \n");
                    WriteToLogFile("\n Email Send successfully for " + count + " Supervisor" + DateTime.Now.ToString() + "  : \n");
                    return;
                }

                else
                {
                    //logger.Log(LogLevel.Error, "\n error while sending email" + DateTime.Now.ToString() + "  : \n");
                    //Console.WriteLine("error while sending email " + DateTime.Now.ToString());
                    WriteToLogFile("\n error while sending email" + DateTime.Now.ToString() + "  : \n");
                    return;
                }
            }
            catch (Exception exp)
            {
                //Console.WriteLine(exp.ToString());
                //logger.Log(LogLevel.Error, "\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }


        protected void sendMailAbsent(List<RigoAttendanceService.AttServices.AttendanceAllEmployeeReportResult> ListAbsentEmployes)
        {

            try
            {
                RigoAttendanceService.AttServices.EmailContent dbMailContent = client1.GetMailContent(client1.GetAbsentEmailContentID(), keyValue);


                if (dbMailContent == null)
                {
                   // Console.WriteLine("Absent Template not defined.");
                    //logger.Log(LogLevel.Error, "\n Absent Template not defined" + DateTime.Now.ToString() + "  : \n");
                    WriteToLogFile("\n Absent Template not defined" + DateTime.Now.ToString() + "  : \n");
                    return;
                }

                string todayDate = client1.GetCurrentDateAndTime().ToString("yyyy/MMM/d");
                string subject = dbMailContent.Subject.Replace("#TodayDate#", todayDate);
                int count = 0;

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                string SenderEmail = settings.Smtp.From;

                List<RigoAttendanceService.AttServices.GetForDeviceMappingResult> _GetForDeviceMappingResult = client1.GetDeviceMapping("SendMailToLateEntryAttendanceKey").ToList();


                foreach (RigoAttendanceService.AttServices.AttendanceAllEmployeeReportResult _item in ListAbsentEmployes)
                {

                    if (_item.Type == 2 && _item.DayValue != "WH/2")//weekly holiday dot not send mail
                    {

                    }
                    else
                    {

                        RigoAttendanceService.AttServices.GetForDeviceMappingResult _DeviceMappingResult = _GetForDeviceMappingResult.SingleOrDefault(x => x.EmployeeId == _item.EmployeeId);
                        if (_DeviceMappingResult != null && _DeviceMappingResult.IsEnabledSkipLateEntry != null)
                        {
                            if (!_DeviceMappingResult.IsEnabledSkipLateEntry.Value)
                            {

                                string body = dbMailContent.Body.Replace("#TodayDate#", todayDate);
                                body = body.Replace("#Employee#", _item.EmplooyeeName);
                                string EmailTo = client1.GetEmployeeCIEmail(_item.EmployeeId.Value);
                                if (string.IsNullOrEmpty(EmailTo))
                                {

                                   // Console.WriteLine("Email address not found for Employee.");
                                    //logger.Log(LogLevel.Error, "\n Email address not found for Employee" + DateTime.Now.ToString() + "  : \n");
                                    WriteToLogFile("\n Email address not found for Employee" + DateTime.Now.ToString() + "  : \n");
                                    // return;
                                }
                                else
                                {

                                    bool isSendSuccess = client1.SendMailAsync(EmailTo, body, subject, "", SenderEmail);
                                    if (isSendSuccess)
                                        count++;
                                }

                            }
                        }
                    }
                }


                if (count > 0)
                {

                   WriteToLogFile("\n Email Send successfully for " + count + " Absent Employees" + DateTime.Now.ToString() + "  : \n");
                    return;
                }
                else
                {
                    WriteToLogFile("\n error while sending email" + DateTime.Now.ToString() + "  : \n");
                    return;
                }

            }

            catch (Exception exp)
            {
               // Console.WriteLine(exp.ToString());
                //logger.Log(LogLevel.Error, "\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
                WriteToLogFile("\n Error for : " + client1.GetCurrentDateAndTime().Date.ToString() + "  : \n" + exp.ToString());
            }
        }


        //Console.ReadLine();//don't plae as blocked for schedular

        public void SetHTTPS()
        {
            ServicePointManager.ServerCertificateValidationCallback =
                delegate(Object ob, X509Certificate cert, X509Chain chain, SslPolicyErrors erros)
                {
                    return (true);
                };
        }

        public void WriteToLogFile(string strMessage)
        {

            //StreamWriter log;
            string TodayDate = client1.GetCurrentDateAndTime().Date.ToString("MM-dd-yyyy");

            //string outputFile = System.IO.Path.Combine("Log", "Log" + "_" + TodayDate + ".txt");

            //if (!File.Exists("Log" + "_" + TodayDate + ".txt"))
            //{
            //    log = new StreamWriter("Log" + "_" + TodayDate);
            //}

            //if (!System.IO.File.Exists("Log"))
            //{
            //    System.IO.Directory.CreateDirectory("Log");
            //}

            //File.AppendText(AppDomain.CurrentDomain.BaseDirectory + @"\" + TodayDate + ".txt",
            //      strMessage);


         string logFile =   AppDomain.CurrentDomain.BaseDirectory + @"\Log _" + TodayDate + ".txt";



         string outputFile = logFile;//"Log" + "_" + TodayDate + ".txt";
            // Write to the file:
            string line = DateTime.Now.ToString() + " | ";
            line += strMessage;

            FileStream fs = new FileStream(outputFile, FileMode.Append, FileAccess.Write, FileShare.None);
            StreamWriter swFromFileStream = new StreamWriter(fs);
            swFromFileStream.WriteLine(line);
            swFromFileStream.Flush();
            swFromFileStream.Close();
        }
    }


}



