﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace APIService.Model
{
    public class LoginResult
    {
        public string ErrorMessage { get; set; }
        public bool IsSuccess { get; set; }
        public string FullName { get; set; }
        public string PhotoUrl { get; set; }

        public int MyLeaveRequestCount { get; set; }
        public int ApproveLeaveRequestCount { get; set; }

        public int CurrentEmployeeId { get; set; }

        public string Key { get; set; }

        public List<TextValue> myLeaveBalance = new List<TextValue>();
    }

    public class TextValue
    {
        public int ID { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class ServiceStatus
    {
        public ServiceStatus()
        {
            this.IsSuccess = true;
            this.ErrorMessage = "";
        }

        public ServiceStatus(string errorMessage)
        {
            IsSuccess = false;
            this.ErrorMessage = errorMessage;
        }

        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }
    }
    public class LeaveApprovalBO
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public string LeaveName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public string FromDateActual { get; set; }

        public string ToDateActual { get; set; }

        public string Reason { get; set; }

        public string Date { get; set; }
        public string Day { get; set; }
        public int LeaveRequestId { get; set; }

        public DateTime CreatedOn { get; set; }
    }

    public class ColleagueBO
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public string PhotoUrl { get; set; }

        public bool IsGroup { get; set; }
        public string Description { get; set; }
    }

    public class ColleagueBOModel : ObservableCollection<ColleagueBO>
    {
        public string LongName { get; set; }
        public string ShortName { get; set; }
    }

    public class OTAllowanceBO
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public string OTAllowanceName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Date { get; set; }
        public string Day { get; set; }
        public int Id { get; set; }

        public bool IsOT { get; set; }
    }

    /// <summary>
    /// Represent list of valid month for the company with year
    /// </summary>
    public class Period
    {
        public List<string> Months { get; set; }
        public List<string> Years { get; set; }

        public string CurrentMonth { get; set; }
        public string CurrentYear { get; set; }

    }

    public class LeaveRequestBO
    {
        public string Reason { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int LeaveTypeId { get; set; }
        public bool IsHalfDayLeave { get; set; }
        public string HalfDayType { get; set; }

        public int RecommendEmployeeId { get; set; }
        public int ApprovalEmployeeId { get; set; }

    }
}