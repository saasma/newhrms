﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL;
using System.Xml.Linq;
using System.Data.Linq;
using System.Linq;

using BLL.Manager;
using APIService.Helper;

using System.Web;
using Utils.Security;
using Utils.Calendar;
using BLL;
using APIService.Model;
using Utils;
using BLL.Entity;

namespace APIService.Controllers
{
    /// <summary>
    /// Finance API
    /// </summary>
    public class ServiceController : ApiController
    {
        // GET api/finance
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/finance/5
        //public Customer Get(string id)
        //{
        //    return new Customer { Name = "ram" };
        //}



        //// POST api/finance
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/finance/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/finance/5
        //public void Delete(int id)
        //{
        //}

        // GET api/documentation
        /// <summary>
        /// List of interest rates with downpayment percentage
        /// </summary>
        //public List<InterestBO> GetInterestRates(string key)
        //{
        //    if (UserManager.IsValidKey(key) == false)
        //        APIHelper.ThrowError("Invalid api key.");

        //    List<InterestRate> rates =
        //        FinanceSchemeManager.GetInterestRates();

        //    return
        //        (
        //            from l in rates
        //            select new InterestBO
        //            {
        //                start_dp = l.StartDPPercent,
        //                end_dp = (l.EndDPPercent == 100 ? l.EndDPPercent : l.EndDPPercent - 1),
        //                interest_rate = l.InterestRateValue
        //            }
        //        ).ToList();
        //}


        ////[HttpGet]
        //// finance/GetModel?key={0}&modelid={1}&othervalue={2}
        ///// <summary>
        ///// Model detail information
        ///// </summary>
        //public ModelBO GetModel(string key, string modelId)
        //{
        //    if (UserManager.IsValidKey(key) == false)
        //        APIHelper.ThrowError("Invalid api key.");

        //    Guid bikeID = Guid.Empty;

        //    if (Guid.TryParse(modelId, out bikeID))
        //    {
        //        BikeMain l = Finance_BikeManager.GetBikeById(bikeID);
        //        if (l == null)
        //            return null;

        //        return
        //            (
        //               new ModelBO
        //               {
        //                   id = l.BikeID.ToString(),
        //                   product = l.BikeName,
        //                   price = l.Price == null ? 0 : l.Price.Value,
        //                   default_downpayment_percent = l.DownPaymentPercent == null ? 0 : l.DownPaymentPercent.Value,

        //                   registration_year1 = l.RegistrationFeeYrs1,
        //                   registration_year2 = l.RegistrationFeeYrs2,
        //                   registration_year3 = l.RegistrationFeeYrs3,
        //                   insurance = l.ModelFeeLines.FirstOrDefault().Amount,
        //                   updated_on = (l.ModifiedOn == null ? null : l.ModifiedOn.Value.ToString())
        //               }
        //            );
        //    }
        //    else
        //    {

        //        APIHelper.ThrowError("No model with id " + modelId);
        //    }

        //    return null;
        //}

        //// GET finance/GetAllModels?key={0}
        ///// <summary>
        ///// List of models
        ///// </summary>
        //public List<ModelBO> GetAllModels(string key)
        //{
        //    if (UserManager.IsValidKey(key) == false)
        //        APIHelper.ThrowError("Invalid api key.");

        //    List<BikeMain> list = Finance_BikeManager.GetAllBikeModels();

        //    return
        //        (
        //            from l in list
        //            select new ModelBO
        //            {
        //                id = l.BikeID.ToString(),
        //                product = l.BikeName,
        //                price = l.Price == null ? 0 : l.Price.Value,
        //                default_downpayment_percent = l.DownPaymentPercent == null ? 0 : l.DownPaymentPercent.Value,

        //                registration_year1 = l.RegistrationFeeYrs1,
        //                registration_year2 = l.RegistrationFeeYrs2,
        //                registration_year3 = l.RegistrationFeeYrs3,
        //                insurance = l.ModelFeeLines.FirstOrDefault().Amount,
        //                updated_on = (l.ModifiedOn == null ? null : l.ModifiedOn.Value.ToString())

        //            }
        //        ).ToList();

        //}


        [HttpPost]
        public ServiceStatus ApproveLeaveRequest(string key,int leaveRequestId)
        {
            UUser user = null;
            ServiceStatus serviceStatus = new ServiceStatus();
           
         
            if (APIHelper.IsValidKey(key,ref user) == false)
                APIHelper.ThrowError("Invalid key");

            APIHelper.Login(key);

            ResponseStatus status = LeaveAttendanceManager.ApproveLeaveRequest(leaveRequestId);

            if (status.IsSuccessType == false)
                return new ServiceStatus(status.ErrorMessage);

            return serviceStatus;
        }

        [HttpPost]
        public ServiceStatus CreateLeaveRequest(string key, LeaveRequestBO entity)
        {
            UUser user = null;
            ServiceStatus serviceStatus = new ServiceStatus();
            
            if (APIHelper.IsValidKey(key, ref user) == false)
                APIHelper.ThrowError("Invalid key");

            APIHelper.Login(key);

            LeaveRequest leave = new LeaveRequest();
            leave.Reason = entity.Reason;
            leave.FromDate = entity.FromDate;
            leave.ToDate = entity.ToDate;
            leave.LeaveTypeId = entity.LeaveTypeId;
            leave.IsHalfDayLeave = entity.IsHalfDayLeave;
            leave.RecommendEmployeeId = entity.RecommendEmployeeId;
            leave.SupervisorEmployeeIdInCaseOfSelection = entity.ApprovalEmployeeId;
            leave.Status = (int)LeaveRequestStatusEnum.Request;

            Status status = LeaveAttendanceManager.CreateLeaveRequest(leave);

            if (status.IsSuccess == false)
                return new ServiceStatus(status.ErrorMessage);

            return serviceStatus;
        }

        [HttpGet]
        public string GetPayslipImageUrl(string key,string month,string year)
        {
            UUser user = null;
            if (APIHelper.IsValidKey(key, ref user) == false)
                APIHelper.ThrowError("Invalid key");

            if (user == null)
                return "";

            key = SecurityHelper.EncryptServiceKey(user.UserID.ToString());

            return
                 Config.UploadLocation + "&payslip=true&key=" + key + "&year=" + year + "&month=" + month;
        }

        [HttpGet]
        public List<LeaveApprovalBO> GetLeaveRequestsListForApproval(string key, int supervisorEmpId)
        {

            if (APIHelper.IsValidKey(key) == false)
                APIHelper.ThrowError("Invalid key");

            //used for get Leave Request
            //-1 signifies that it will fetch data only for current and next month.
            // else wise fetch data for starting payrollperioddate till date.
            List<GetLeaveByDateApprovalResult> list = LeaveAttendanceManager
                .GetLeaveByDateApproval("1-6", 0, -1, 0, 99999, -1, -1, "", "",supervisorEmpId);

            List<LeaveApprovalBO> newList = new List<LeaveApprovalBO>();

            foreach (GetLeaveByDateApprovalResult item in list)
            {
               
                LeaveApprovalBO leave = new LeaveApprovalBO();
                leave.EmployeeId = item.EmployeeId;
                leave.Name = item.EmployeeName;

                bool hasPhoto = false;
                string photoUrl = EmployeeManager.GetEmployeePhotoThumbnail(item.EmployeeId); ;
                if (!string.IsNullOrEmpty(photoUrl))
                {
                    photoUrl = Config.UploadLocation + photoUrl + "&key=" + key;
                    hasPhoto = true;
                }

                if (!hasPhoto)
                {
                    photoUrl = Config.UploadLocation + "../images/male.png";
                }

                leave.PhotoUrl = photoUrl;
                leave.LeaveName = item.Title;
                leave.FromDate = item.FromDate.Value;
                leave.ToDate = item.ToDate.Value;

                leave.CreatedOn = (item.CreatedOn == null ? DateTime.Now : item.CreatedOn.Value);
                leave.Reason = item.Reason;

                if (BLL.BaseBiz.IsEnglish == false)
                {
                    CustomDate date = CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(leave.FromDate), false);
                    leave.FromDateActual = DateHelper.GetMonthName(date.Month, false) + " " + date.Day + ", " + date.Year;

                    date = CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(leave.ToDate), false);
                    leave.ToDateActual = DateHelper.GetMonthName(date.Month, false) + " " + date.Day + ", " + date.Year;
                }

                if (item.FromDate.Equals(item.ToDate))
                    leave.Date = APIHelper.FormatDate(item.FromDate);
                else
                    leave.Date = APIHelper.FormatDate( item.FromDate) + " to " + APIHelper.FormatDate(item.ToDate);


                if (item.DaysOrHours == 0.5)
                {
                    if (!string.IsNullOrEmpty(item.HalfDayType))
                    {
                        if (item.HalfDayType.Equals("AM"))
                            leave.Day = "First Half";
                        else
                            leave.Day = "Second Half";
                    }
                    else
                        leave.Day = "Half day";
                }
                else if (item.IsHour != null && item.IsHour.Value)
                    leave.Day = item.DaysOrHours + " hours";
                else
                    leave.Day = item.DaysOrHours + " " + (item.DaysOrHours <= 1 ? "day" : "days");
                leave.LeaveRequestId = item.LeaveRequestId;

                newList.Add(leave);
            }

            int total = 0;
            if (list.Count > 0)
                total = list[0].TotalRows.Value;

            return newList;
        }

        [HttpGet]
        public List<ColleagueBO> GetColleagueList(string key, int supervisorEmpId)
        {

            UUser user = null;

            if (APIHelper.IsValidKey(key, ref user) == false)
                APIHelper.ThrowError("Invalid key");

            APIHelper.Login(key);


            int totalRecords = 0;
            EmployeeManager empMgr = new EmployeeManager();

            List<EEmployee> list = empMgr.GetAllEmployees(1, 9999, ref totalRecords, "",
               SessionManager.CurrentCompanyId, "",
               "", false, -1, -1, -1, -1, null
                , "", -1, -1, -1, "", -1).OrderBy(x => x.Name).ToList();


            List<ColleagueBO> newList = new List<ColleagueBO>();

            foreach (var item in list)
            {

                ColleagueBO entity = new ColleagueBO();
                entity.ID = item.EmployeeId;
                entity.Name = item.Name;
                entity.Description = item.DepartmentValue + ", " + item.DesignationValue;

                bool hasPhoto = false;
                string photoUrl = (item.URLPhoto == null ? "" : item.URLPhoto)
                    .Replace("~/Uploads/", "").Replace("~/images/","");

                if (!string.IsNullOrEmpty(photoUrl))
                {
                    photoUrl = Config.UploadLocation + photoUrl;// + "&key=" + key;
                    hasPhoto = true;
                }

                if (!hasPhoto)
                {
                    photoUrl = Config.UploadLocation + "../images/male.png";
                }

                entity.PhotoUrl = photoUrl;
                

                newList.Add(entity);
            }

          


            return newList;
        }

        [HttpGet]
        public List<LeaveApprovalBO> GetMyLeaveRequests(string key, int supervisorEmpId)
        {

            if (APIHelper.IsValidKey(key) == false)
                APIHelper.ThrowError("Invalid key");

            //used for get Leave Request
            //-1 signifies that it will fetch data only for current and next month.
            // else wise fetch data for starting payrollperioddate till date.
            List<BLL.BO.LeaveRequestBO> list = LeaveAttendanceManager.GetUnApprovedLeaveRequest(supervisorEmpId);

            List<LeaveApprovalBO> newList = new List<LeaveApprovalBO>();

            foreach (BLL.BO.LeaveRequestBO item in list)
            {

                LeaveApprovalBO leave = new LeaveApprovalBO();
                
               
                leave.LeaveName = item.LeaveName;
                leave.FromDate = Convert.ToDateTime( item.FromDate);
                leave.ToDate = Convert.ToDateTime(item.ToDate);

                leave.CreatedOn = item.CreatedOn;
                leave.Reason = item.Reason;

                if (BLL.BaseBiz.IsEnglish == false)
                {
                    CustomDate date = CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(leave.FromDate), false);
                    leave.FromDateActual = DateHelper.GetMonthName(date.Month, false) + " " + date.Day + ", " + date.Year;

                    date = CustomDate.GetCustomDateFromString(BLL.BaseBiz.GetAppropriateDate(leave.ToDate), false);
                    leave.ToDateActual = DateHelper.GetMonthName(date.Month, false) + " " + date.Day + ", " + date.Year;
                }

                if (item.FromDate.Equals(item.ToDate))
                    leave.Date = APIHelper.FormatDate(leave.FromDate);
                else
                    leave.Date = APIHelper.FormatDate(leave.FromDate) + " to " + APIHelper.FormatDate(leave.ToDate);


                if (item.DaysOrHours == 0.5)
                {
                    //if (!string.IsNullOrEmpty(item.HalfDayType))
                    //{
                    //    if (item.HalfDayType.Equals("AM"))
                    //        leave.Day = "First Half";
                    //    else
                    //        leave.Day = "Second Half";
                    //}
                    //else
                        leave.Day = "Half day";
                }
                //else if (item.IsHour != null && item.IsHour.Value)
                //    leave.Day = item.DaysOrHours + " hours";
                else
                    leave.Day = item.DaysOrHours + " " + (item.DaysOrHours <= 1 ? "day" : "days");
                leave.LeaveRequestId = item.LeaveRequestId;

                newList.Add(leave);
            }

          

            return newList;
        }


        [HttpGet]
        public Period GetPeriod(string key)
        {
            if (APIHelper.IsValidKey(key) == false)
                APIHelper.ThrowError("Invalid key");

            Period period = new Period();
            period.Years = new List<string>();
            period.Months = new List<string>();

            PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();

            period.CurrentMonth = DateHelper.GetMonthName(lastPayroll.Month, SessionManager.IsEnglish);
            period.CurrentYear = lastPayroll.Year.ToString();

            //calDate.Text = CustomDate.GetTodayDate(IsEnglish).ToString();
            foreach(var item in CommonManager.GetYearList().Select(x=>x.Year.ToString()).Distinct().ToList())
            {
                period.Years.Add(item.ToString());
            }
       

            foreach (var item in DateManager.GetCurrentMonthList())
            {
                period.Months.Add(item.Value);
            }

            

            return period;
        }

        //// GET Service/AuthenticateUser?username={0}&password={1}
        [HttpGet]
        public LoginResult AuthenticateUser(string username,string password)
        {
            LoginResult result = new LoginResult();
            UserManager mgr = new UserManager();
            string role = "";
            UUser user = null;
            int roleId = 0;
            bool isEmpRetired = false;
            var isEmpPortalDisabled = false;
            if (mgr.ValidateUser(username, password, ref role, ref roleId, ref user, false, ref isEmpRetired, ref isEmpPortalDisabled))
            {
                if(roleId == (int)Role.Administrator)
                {
                    result.ErrorMessage =  "Admin login not allowed";
                    result.IsSuccess = false;
                }
                else
                {

                    int empId = user.EmployeeId.Value;
                    result.CurrentEmployeeId = empId;
                    result.FullName = EmployeeManager.GetEmployeeName(empId);
                    result.PhotoUrl = EmployeeManager.GetEmployeePhotoThumbnail(empId);

                    // encrypt
                    //result.Key = HttpContext.Current.Server.UrlEncode(SecurityHelper.EncryptServiceKey(user.UserID.ToString()));

                    result.Key =  SecurityHelper.CreateCookie(user.UserName, true, role, false, false);


                    bool hasPhoto = false;

                    if (!string.IsNullOrEmpty(result.PhotoUrl))
                    {
                        result.PhotoUrl =  Config.UploadLocation + result.PhotoUrl + "&key=" + result.Key;
                        hasPhoto = true;
                    }

                    if (!hasPhoto)
                    {
                        result.PhotoUrl = Config.UploadLocation + "../images/male.png";

                    }

                    // set leave counting
                    PayrollPeriod lastPayroll = CommonManager.GetLastPayrollPeriod();
                    List<GetLeaveListAsPerEmployeeResult> list = LeaveAttendanceManager
                            .GetAllLeavesForLeaveRequest(empId, lastPayroll.PayrollPeriodId, true)
                            .Where(x => x.IsParentGroupLeave == false).ToList();
                    foreach(var item in list)
                    {
                        result.myLeaveBalance.Add(new TextValue
                        {
                            ID  = item.LeaveTypeId,
                            Text = item.Title,
                            Value = string.Format("{0:0.##}", item.NewBalance)
                        });
                    }



                    result.IsSuccess = true;
                }
            }
            else
            {
                if (isEmpPortalDisabled)
                {
                    result.ErrorMessage = "Employee portal login has been disabled, please contact HR for details.";
                    result.IsSuccess = false;


                }
                else
                {
                    result.ErrorMessage = "Invalid username or password";
                    result.IsSuccess = false;
                }
            }
            return result;
        }
       

    }
}
