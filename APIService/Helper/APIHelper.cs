﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using Utils.Security;
using BLL.Manager;
using DAL;
using System.Web.Security;
using System.Security.Principal;

namespace APIService.Helper
{
    public class APIHelper
    {

        public static string FormatDate(DateTime? date)
        {
            if (date == null)
                return "";
            return date.Value.ToString("MMM dd, yyyy");
        }
        public static bool IsValidKey(string key)
        {
            try
            {
                FormsAuthenticationTicket authTicket = null;
                try
                {
                    authTicket = FormsAuthentication.Decrypt(key);
                }
                catch (Exception ex)
                {
                    // Log exception details (omitted for simplicity)
                    return false;
                }
                if (null == authTicket)
                {
                    // Cookie failed to decrypt.
                    return false;
                }

                UUser user = UserManager.GetUserByUserName(authTicket.Name);


                if (user != null)
                    return true;

                return false;
            }
            catch
            {
                return false;
            }
        }

        public static void Login(string key)
        {
            FormsAuthenticationTicket authTicket = null;
            try
            {
                authTicket = FormsAuthentication.Decrypt(key);
            }
            catch (Exception ex)
            {
                // Log exception details (omitted for simplicity)
                return;
            }
            if (null == authTicket)
            {
                // Cookie failed to decrypt.
                return;
            }
            // When the ticket was created, the UserData property wasassigned
            // a pipe delimited string of role names.
            string[] roles = new string[1];
            roles[0] = authTicket.UserData.ToString().Split(new char[] { ':' })[0];



            // Create an Identity object
            FormsIdentity id = new FormsIdentity(authTicket);
            // This principal will flow throughout the request.
            GenericPrincipal principal = new GenericPrincipal(id, roles);
            // Attach the new principal object to the current HttpContextobject
            HttpContext.Current.User = principal;

        }

        public static bool IsValidKey(string key,ref UUser user)
        {
            try
            {
                FormsAuthenticationTicket authTicket = null;
                try
                {
                    authTicket = FormsAuthentication.Decrypt(key);
                }
                catch (Exception ex)
                {
                    // Log exception details (omitted for simplicity)
                    return false;
                }
                if (null == authTicket)
                {
                    // Cookie failed to decrypt.
                    return false;
                }

                user = UserManager.GetUserByUserName(authTicket.Name);

                if (user != null)
                    return true;

                return false;
            }
            catch
            {
                return false;
            }
        }



        public static void ThrowError(string message)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.NotFound);

            resp.Content = new StringContent(message);
            resp.ReasonPhrase = "Error";

            throw new HttpResponseException(resp);
        }
    }
}