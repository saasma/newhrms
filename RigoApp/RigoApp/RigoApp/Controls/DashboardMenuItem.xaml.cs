﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RigoApp
{
    public partial class DashboardMenuItem : ContentView
    {
        /// <summary>
        /// http://www.kymphillpotts.com/common-ui-patterns-in-xamarin-forms-part-1-springboards/
        /// </summary>
        
        public DashboardMenuItem()
        {
            InitializeComponent();
        }


        public ImageSource Icon
        {
            get { return MyIcon.Source; }
            set { MyIcon.Source = value; }
        }

        public string Label
        {
            get
            {
                return MyLabel.Text;
            }
            set
            {
                MyLabel.Text = value;

            }
        }
    }
}
