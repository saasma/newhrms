﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIService.Model;
using Xamarin.Forms;

namespace RigoApp
{
    public partial class LoginPage : ContentPage
    {
        RestServiceManager service = new RestServiceManager();
        public LoginPage()
        {
           

            InitializeComponent();

            this.Title = "Rigo HR Login";
        }

        async void Login_Clicked(object sender,EventArgs e)
        {
            //((sender) as Button).Text = "CLicked";
            
            if(txtUserName.Text == null || txtPassword.Text == null)
            {
                lblResult.Text = "Username or password can not be blank.";
                return;
            }

            //return;
            string user = txtUserName.Text.Trim();
            string pwd = txtPassword.Text.Trim();

            lblResult.Text = "Connecting..." + RestServiceManager.rootUrl ;

            ((sender) as Button).IsEnabled = false;

            LoginResult result = await service.AuthenticateUser(user, pwd);

            ((sender) as Button).IsEnabled = true;

            if (!result.IsSuccess)
                lblResult.Text = result.ErrorMessage;
            else
            {
                lblResult.Text = "Connected";

                SessionManager.UserName = user.Trim();
                SessionManager.UserData = result;
                //SessionManager.UserData.FullName = result.FullName;
                //SessionManager.UserData.PhotoUrl = result.PhotoUrl;
                //SessionManager.UserData.CurrentEmployeeId = result.CurrentEmployeeId;

                Dashboard dashboard = new Dashboard();
                dashboard.Title = "My Dashboard";
                dashboard.SetDislay(result.FullName, result.PhotoUrl);

                //App.Current.MainPage = dashboard;

            App.Current.MainPage= new NavigationPage(dashboard);

            }
        }
    }
}
