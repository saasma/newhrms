﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RigoApp.View.Leave;
using UXDivers.Gorilla.Samples;
using Xamarin.Forms;

namespace RigoApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new LoginPage();

            MainPage = new NavigationPage(new LoginPage());
            //{
            //    BarBackgroundColor = Color.Blue,
            //    BarTextColor = Color.Pink
            //};
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
