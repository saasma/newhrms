﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIService.Model;
using Xamarin.Forms;

namespace RigoApp.View.Leave
{
    public partial class MyRequestList : TabbedPage
    {
        public List<LeaveApprovalBO> Leaves = new List<LeaveApprovalBO>();

        public MyRequestList()
        {
            InitializeComponent();

            this.Title = "My Requests";
           
        }

        bool processing = false;

        async void OnSelection(object sender, ItemTappedEventArgs e)
        {
            //if (e.Item == null || processing)
            //{
            //    return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            //}
            //LeaveApprovalBO selectedLeave = e.Item as LeaveApprovalBO;

            //processing = true;

            //LeaveDetail page = new LeaveDetail(selectedLeave);
            //page.BindData();


            //await Navigation.PushModalAsync(new NavigationPage(page));

            //processing = false;

        }

        async void btnCreateLeaveRequest_Clicked(object sender,EventArgs e)
        {
            MyNewLeaveRequest page = new MyNewLeaveRequest();
            await Navigation.PushModalAsync(new NavigationPage(page));
        }


        public void Approve_Clicked(object sender,EventArgs e)
        {
            DisplayAlert("approve clicked", "Message", "Ok");
        }

        //async protected override void OnAppearing()
        //{
        //    base.OnAppearing();

        //    //RestServiceManager mgr = new RestServiceManager();
        //    //List<APIService.Model.LeaveApprovalBO> list = await mgr.GetLeaveRequestsForApproval();


        //    //this.Leaves = list;
        //    //this.BindData();
        //}

        public void BindData()
        {
            //this.Title = "Leave Approval" + " (" + Leaves.Count + ")";
            lblLeaves.Text = "LEAVE REQUESTS (" + Leaves.Count + ")";

            listView.ItemsSource = Leaves;
        }
    }
}
