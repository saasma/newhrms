﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIService.Model;
using Xamarin.Forms;

namespace RigoApp.View.Leave
{
    public partial class LeaveDetail : ContentPage
    {
        public LeaveApprovalBO leaveDetail = new LeaveApprovalBO();

        public LeaveDetail(LeaveApprovalBO leaveDetail)
        {
            InitializeComponent();

            this.leaveDetail = leaveDetail;
            this.Title = "Leave Details";
            //NavigationPage.SetHasNavigationBar(this, false);

            this.BindingContext = leaveDetail;
        }

        


        async void btnConfirm_Clicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Approve Request?", "Confirm approve the leave request?", "Yes", "No");
          
            if(answer)
            {
              

                RestServiceManager mgr = new RestServiceManager();

                btnDeny.IsEnabled = false;
                btnApprove.IsEnabled = false;
               

                ServiceStatus status = await mgr.ApproveLeaveRequest(leaveDetail.LeaveRequestId);

                // after leave has been approved, close current page and reload previous leave listing page
                if(status.IsSuccess)
                {
                    List<APIService.Model.LeaveApprovalBO> list = await mgr.GetLeaveRequestsForApproval();

                    await Navigation.PopModalAsync();
                    
                   

                    LeaveApproval page = SessionManager.LeaveApprovalPage;
                    if (page != null)
                    {
                        page.Leaves = list;
                        page.BindData();
                    }
                   
                }
                else
                {
                    await DisplayAlert("Leave Approval Error", status.ErrorMessage, "Ok");
                }


            }
        }

        public void BindData()
        {

            imgPhoto.Source = ImageSource.FromUri(new Uri(leaveDetail.PhotoUrl));
            //lblName.Text = leaveDetail.Name;
            lblLeaveTitle.Text = leaveDetail.LeaveName;

            lblAppliedDate.Text = Helper.FormatDate(leaveDetail.CreatedOn);

            lblDays.Text = leaveDetail.Day;
            lblNote.Text = leaveDetail.Reason;

            lblFromDate.Text = Helper.FormatDate(leaveDetail.FromDate);
            lblToDate.Text = Helper.FormatDate(leaveDetail.ToDate);

            if (!string.IsNullOrEmpty(leaveDetail.ToDateActual))
            {
                lblFromNep.Text = leaveDetail.FromDateActual;
                lblToNep.Text = leaveDetail.ToDateActual;
            }
            else
            {
                lblFromNep.Text = "";
                lblToNep.Text = "";
            }
            //lblTitle.Text += " (" + Leaves.Count + ")";

            //listView.ItemsSource = Leaves;
        }
    }
}
