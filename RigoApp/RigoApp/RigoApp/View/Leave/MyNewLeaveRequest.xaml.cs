﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIService.Model;
using Xamarin.Forms;

namespace RigoApp.View.Leave
{
    public partial class MyNewLeaveRequest : ContentPage
    {
        public LeaveApprovalBO leaveDetail = new LeaveApprovalBO();

        public List<TextValue> leaves = new List<TextValue>();

        public MyNewLeaveRequest()//LeaveApprovalBO leaveDetail)
        {
            InitializeComponent();

            this.leaves = leaves;
           // this.leaveDetail = leaveDetail;
            this.Title = "New Leave Request";
            //NavigationPage.SetHasNavigationBar(this, false);

            //this.BindingContext = leaveDetail;
            foreach (var item in SessionManager.UserData.myLeaveBalance)
                pickerLeaveType.Items.Add(item.Text + " (" + item.Value + ")");
        }

        


        async void btnSave_Click(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Create Request?", "Confirm create the leave request?", "Yes", "No");
          
            if(answer)
            {

                LeaveRequestBO leave = new LeaveRequestBO();

                if(pickerLeaveType.SelectedIndex <= - 1)
                {
                    await DisplayAlert("Validation failure!", "Leave type is required.", "OK");
                    return;
                }

                // validation
                leave.LeaveTypeId  = SessionManager.UserData.myLeaveBalance[pickerLeaveType.SelectedIndex].ID;
                leave.FromDate = lblFromDate.Date.ToString();
                leave.ToDate = lblToDate.Date.ToString();
                leave.Reason = lblNote.Text;
                leave.IsHalfDayLeave = chkIsHalfDay.IsToggled;


                RestServiceManager mgr = new RestServiceManager();

               
               

                ServiceStatus status = await mgr.CreateLeaveRequest(leave);

                // after leave has been approved, close current page and reload previous leave listing page
                if(status.IsSuccess)
                {
                    List<APIService.Model.LeaveApprovalBO> list = await mgr.GetMyLeaveRequests();

                    await Navigation.PopModalAsync();



                    MyRequestList page = SessionManager.MyRequestPage;
                    if (page != null)
                    {
                        page.Leaves = list;
                        page.BindData();
                    }
                   
                }
                else
                {
                    await DisplayAlert("Leave Request Error", status.ErrorMessage, "Ok");
                }


            }
        }

        public void BindData()
        {

            //imgPhoto.Source = ImageSource.FromUri(new Uri(leaveDetail.PhotoUrl));
            ////lblName.Text = leaveDetail.Name;
            //lblLeaveTitle.Text = leaveDetail.LeaveName;

            //lblAppliedDate.Text = Helper.FormatDate(leaveDetail.CreatedOn);

            //lblDays.Text = leaveDetail.Day;
            //lblNote.Text = leaveDetail.Reason;

            //lblFromDate.Text = Helper.FormatDate(leaveDetail.FromDate);
            //lblToDate.Text = Helper.FormatDate(leaveDetail.ToDate);

            //if (!string.IsNullOrEmpty(leaveDetail.ToDateActual))
            //{
            //    lblFromNep.Text = leaveDetail.FromDateActual;
            //    lblToNep.Text = leaveDetail.ToDateActual;
            //}
            //else
            //{
            //    lblFromNep.Text = "";
            //    lblToNep.Text = "";
            //}
            //lblTitle.Text += " (" + Leaves.Count + ")";

            //listView.ItemsSource = Leaves;
        }
    }
}
