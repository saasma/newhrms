﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIService.Model;
using Xamarin.Forms;

namespace RigoApp.View.Leave
{
    public partial class ColleaguesList : ContentPage
    {
        public List<ColleagueBO> list = new List<ColleagueBO>();

        public ColleaguesList()
        {
            InitializeComponent();

            this.Title = "Colleagues";
           
        }

        bool processing = false;

        async void OnSelection(object sender, ItemTappedEventArgs e)
        {
            //if (e.Item == null || processing)
            //{
            //    return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            //}
            //LeaveApprovalBO selectedLeave = e.Item as LeaveApprovalBO;

            //processing = true;

            //LeaveDetail page = new LeaveDetail(selectedLeave);
            //page.BindData();


            //await Navigation.PushModalAsync(new NavigationPage(page));

            //processing = false;

            //DisplayAlert("Item Selected", e.SelectedItem.ToString(), "Ok");
            //((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
        }


       



        public void BindData()
        {
            //this.Title = "Leave Approval" + " (" + Leaves.Count + ")";
            //lblLeaves.Text = "LEAVE REQUESTS (" + Leaves.Count + ")";

            // process to group by first characters

            ColleagueBOModel modelA = new ColleagueBOModel { LongName = "A", ShortName = "a" };
            ColleagueBOModel other = new ColleagueBOModel { LongName="Other",ShortName="o" };
            foreach(var item in list)
            {
                if (item.Name.ToLower().StartsWith("a"))
                    modelA.Add(item);
                else
                    other.Add(item);
            }

            var all = new ObservableCollection<ColleagueBOModel>(); 
            all.Add(modelA);
            all.Add(other);

            listView.ItemsSource = all;
        }
    }
}
