﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIService.Model;
using Xamarin.Forms;

namespace RigoApp.View.PaySalary
{
    public partial class MyPaySlip : ContentPage
    {

        public Period period = new Period();

        public MyPaySlip()
        {
            InitializeComponent();

            this.Title = "My Payslip";
        }


        async void btnLoad_Clicked(object sender,EventArgs e)
        {
            string month = pickerMonth.Items[pickerMonth.SelectedIndex].ToString();
            string year = pickerYear.Items[pickerYear.SelectedIndex].ToString();

            RestServiceManager mgr = new RestServiceManager();
            ((sender) as Button).IsEnabled = false;
            string url = await mgr.GetPayslipUrl(year, month);
            ((sender) as Button).IsEnabled = true;

            // some how url contains \" so replace value
            url = url.Replace("\"", "");


            image.Source =  ImageSource.FromUri(new Uri(url));

        }

        public void BindData()
        {
            int yearSelectedIndex = -1;
            int monthSelectedIndex = -1;

            if (period.Years != null)
            {
                for (int i = 0; i < period.Years.Count; i++)// var item in period.Years)
                {
                    var item = period.Years[i];

                    if (item.Equals(period.CurrentYear))
                        yearSelectedIndex = i;

                    pickerYear.Items.Add(item);
                }
            }

            if (period.Months != null)
            {
                for (int i = 0; i < period.Months.Count; i++)// var item in period.Years)
                {
                    var item = period.Months[i];

                    if (item.Equals(period.CurrentMonth))
                        monthSelectedIndex = i;

                    pickerMonth.Items.Add(item);
                }
                //lblTitle.Text += " (" + Leaves.Count + ")";

            }
            pickerYear.SelectedIndex = yearSelectedIndex;
            pickerMonth.SelectedIndex = monthSelectedIndex;

            //listView.ItemsSource = Leaves;
        }
    }
}
