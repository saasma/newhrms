﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using Xamarin.Forms;

namespace RigoApp
{
    public class Helper
    {

        public static string FormatDate(DateTime? date)
        {
            if (date == null)
                return "";
            return date.Value.ToString("MMM dd, yyyy");
        }

        public static Xamarin.Forms.Page GetCurrentPage()
        {
            Page currPage = null;

            if (Application.Current.MainPage.Navigation.NavigationStack.Count > 0)
            {
                //LIFO is the only game in town! - so send back the last page

                int index = Application.Current.MainPage.Navigation.NavigationStack.Count - 1;

                currPage = Application.Current.MainPage.Navigation.NavigationStack[index];

            }

            return currPage;
        }


    }
}