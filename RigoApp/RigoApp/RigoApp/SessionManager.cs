﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIService.Model;
using RigoApp.View.Leave;

namespace RigoApp
{
    public class SessionManager
    {
        public static string UserName { get; set; }

        public static LoginResult UserData = new LoginResult();

        public static LeaveApproval LeaveApprovalPage = null;

        public static MyRequestList MyRequestPage = null;
    }
}
