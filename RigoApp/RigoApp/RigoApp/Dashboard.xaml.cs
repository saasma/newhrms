﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIService.Model;
using RigoApp.View;
using RigoApp.View.Leave;
using RigoApp.View.PaySalary;
using Xamarin.Forms;

namespace RigoApp
{
    public partial class Dashboard : ContentPage
    {
        public Dashboard()
        {
            InitializeComponent();

            this.Title = "My Dashboard";

        }
        

        async void btnPayslip_Clicked(object sender, EventArgs e)
        {
            if (((sender) as DashboardMenuItem).IsEnabled == false)
                return;
            ((sender) as DashboardMenuItem).IsEnabled = false;

            MyPaySlip page = new MyPaySlip();

            RestServiceManager mgr = new RestServiceManager();
            

            Period period = await mgr.GetPeriod();

           

            
            page.period = period;
            page.BindData();
            //App.Current.MainPage = approvalPage;
            await Navigation.PushModalAsync(new NavigationPage(page));

            ((sender) as DashboardMenuItem).IsEnabled = true;
        }

        

        async void btnMyRequests_Clicked(object sender, EventArgs e)
        {
            if (((sender) as DashboardMenuItem).IsEnabled == false)
                return;
            ((sender) as DashboardMenuItem).IsEnabled = false;

            SessionManager.MyRequestPage = new MyRequestList();

            RestServiceManager mgr = new RestServiceManager();


            List<APIService.Model.LeaveApprovalBO> list = await mgr.GetMyLeaveRequests();

            TestPage p1 = new TestPage();

            SessionManager.MyRequestPage.Leaves = list;
            SessionManager.MyRequestPage.BindData();
            //App.Current.MainPage = approvalPage;
            await Navigation.PushModalAsync(new NavigationPage(SessionManager.MyRequestPage));

            ((sender) as DashboardMenuItem).IsEnabled = true;
        }

        

        async void btnColleagues_Clicked(object sender, EventArgs e)
        {
            if (((sender) as DashboardMenuItem).IsEnabled == false)
                return;
            ((sender) as DashboardMenuItem).IsEnabled = false;

         

            RestServiceManager mgr = new RestServiceManager();


            List<APIService.Model.ColleagueBO> list = await mgr.GetColleagueList();

            ColleaguesList page = new ColleaguesList();

            page.list = list;
            page.BindData();
            //App.Current.MainPage = approvalPage;
            await Navigation.PushModalAsync(new NavigationPage(page));

            ((sender) as DashboardMenuItem).IsEnabled = true;
        }

        async void btnLeaveApproval_Clicked(object sender, EventArgs e)
        {
            if (((sender) as DashboardMenuItem).IsEnabled == false)
                return;
            ((sender) as DashboardMenuItem).IsEnabled = false;

            SessionManager.LeaveApprovalPage = new LeaveApproval();

            RestServiceManager mgr = new RestServiceManager();
            

            List<APIService.Model.LeaveApprovalBO> list = await mgr.GetLeaveRequestsForApproval();

           

            SessionManager.LeaveApprovalPage.Leaves = list;
            SessionManager.LeaveApprovalPage.BindData();
            //App.Current.MainPage = approvalPage;
            await Navigation.PushModalAsync(new NavigationPage(SessionManager.LeaveApprovalPage));

            ((sender) as DashboardMenuItem).IsEnabled = true;
        }

        public void SetDislay(string name,string photo)
        {
            lblName.Text = name;
            imgPhoto.Source = ImageSource.FromUri(new Uri(photo));

            // display my leave balace
            //LayoutOptions.Center
            int row = 0;
            int column = 0;
            foreach(var item in SessionManager.UserData.myLeaveBalance)
            {

                StackLayout stack = new StackLayout();
                listMyLeaveBalance.Children.Add(stack);

                Label leaveBalance = new Label { TextColor=Color.FromHex("#2194B4"), Text = item.Value, HorizontalTextAlignment = TextAlignment.Center };
                stack.Children.Add(leaveBalance);

               

                Label leaveTitle = new Label { TextColor=Color.FromHex("#78B2BF"), Text = item.Text,HorizontalTextAlignment=TextAlignment.Center };
                stack.Children.Add(leaveTitle);


                // box separator
                listMyLeaveBalance.Children.Add(new BoxView { Color = Color.FromHex("#E6E7E8"), WidthRequest = 1 });


            }

        }
    }
}
