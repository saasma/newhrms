﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using APIService.Model;
using Newtonsoft.Json;

namespace RigoApp
{
    class RestServiceManager
    {
        //https://developer.xamarin.com/guides/xamarin-forms/web-services/consuming/rest/

            // change required in api service or hr serivce appsetting for web url also
            // for image like url


        //public static string rootUrl = "https://hr.rigopayroll.com/hrservice/Service/";
        //public static string rootUrl = "http://192.168.1.2/hrservice/service/";

        public static string rootUrl = "https://ac.rigonepal.com/rigotechservice/service/";

        HttpClient client;

        public RestServiceManager()
        {
            //var authData = string.Format("{0}:{1}", Constants.Username, Constants.Password);
            //var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        }

    public string GetUrl()
        {
            string url = rootUrl + string.Format("AuthenticateUser?username={0}&password={1}"
          , "", "");

            return url;
        }

        public async Task<LoginResult> AuthenticateUser(string username,string password)
        {
            string url =
                rootUrl + string.Format("AuthenticateUser?username={0}&password={1}"
                ,username,password);

            LoginResult result = new LoginResult();

            try
            {
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<LoginResult>(content);

                }
                else
                {
                    result.ErrorMessage = response.ToString();// server error if comes here
                    result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                //errorMessage = ex.Message;
                result.ErrorMessage = ex.Message;
                result.IsSuccess = false;
            }

            return result;
        }
        
              public async Task<List<ColleagueBO>> GetColleagueList()
        {
            string url =
                rootUrl + string.Format("GetColleagueList?key={0}&supervisorEmpId={1}"
                , SessionManager.UserData.Key, SessionManager.UserData.CurrentEmployeeId);


            List<ColleagueBO> result = new List<ColleagueBO>();

            try
            {
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<List<ColleagueBO>>(content);

                   


                }
                else
                {
                    //result.ErrorMessage = "Error connecting to server.";
                    // result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                //errorMessage = ex.Message;
                //result.ErrorMessage = ex.Message;
                // result.IsSuccess = false;
            }

            return result;
        }

        public async Task<List<LeaveApprovalBO>> GetLeaveRequestsForApproval()
        {
            string url =
                rootUrl + string.Format("GetLeaveRequestsListForApproval?key={0}&supervisorEmpId={1}"
                , SessionManager.UserData.Key, SessionManager.UserData.CurrentEmployeeId);


            List<LeaveApprovalBO> result = new List<LeaveApprovalBO>();

            try
            {
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<List<LeaveApprovalBO>>(content);

                    foreach (var item in result)
                        item.Date += " - " + item.Day;

                }
                else
                {
                    //result.ErrorMessage = "Error connecting to server.";
                   // result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                //errorMessage = ex.Message;
                //result.ErrorMessage = ex.Message;
               // result.IsSuccess = false;
            }

            return result;
        }
        public async Task<List<LeaveApprovalBO>> GetMyLeaveRequests()
        {
            string url =
                rootUrl + string.Format("GetMyLeaveRequests?key={0}&supervisorEmpId={1}"
                , SessionManager.UserData.Key, SessionManager.UserData.CurrentEmployeeId);


            List<LeaveApprovalBO> result = new List<LeaveApprovalBO>();

            try
            {
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<List<LeaveApprovalBO>>(content);

                    foreach (var item in result)
                        item.Date += " - " + item.Day;

                }
                else
                {
                    //result.ErrorMessage = "Error connecting to server.";
                    // result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                //errorMessage = ex.Message;
                //result.ErrorMessage = ex.Message;
                // result.IsSuccess = false;
            }

            return result;
        }
        public async Task<Period> GetPeriod()
        {
            string url =
                rootUrl + string.Format("GetPeriod?key={0}"
                , SessionManager.UserData.Key);


            Period period = new Period();

            try
            {
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    period = JsonConvert.DeserializeObject<Period>(content);

                


                }
                else
                {
                    //result.ErrorMessage = "Error connecting to server.";
                    // result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                //errorMessage = ex.Message;
                //result.ErrorMessage = ex.Message;
                // result.IsSuccess = false;
            }

            return period;
        }


        public async Task<string> GetPayslipUrl(string year,string month)
        {
            string url =
                rootUrl + string.Format("GetPayslipImageUrl?key={0}&month={1}&year={2}"
                , SessionManager.UserData.Key,month,year);


            string value = "";

            try
            {
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    value = content;
                }
                else
                {
                    //result.ErrorMessage = "Error connecting to server.";
                    // result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                //errorMessage = ex.Message;
                //result.ErrorMessage = ex.Message;
                // result.IsSuccess = false;
            }

            return value;
        }

        public async Task<ServiceStatus> ApproveLeaveRequest(int leaveRequestId)
        {
            string url =
                rootUrl + string.Format("ApproveLeaveRequest?key={0}&leaveRequestId={1}"
                , SessionManager.UserData.Key, leaveRequestId);


            ServiceStatus value = new ServiceStatus();

            try
            {
                var response = await client.PostAsync(url,null);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    value = JsonConvert.DeserializeObject<ServiceStatus>(content);
                }
                else
                {
                    //result.ErrorMessage = "Error connecting to server.";
                    // result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                //errorMessage = ex.Message;
                //result.ErrorMessage = ex.Message;
                // result.IsSuccess = false;
                value.ErrorMessage = ex.Message;
            }

            return value;
        }

        public async Task<ServiceStatus> CreateLeaveRequest(LeaveRequestBO item)
        {
            string url =
                rootUrl + string.Format("CreateLeaveRequest?key={0}"
                , SessionManager.UserData.Key);

            var json = JsonConvert.SerializeObject(item);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            ServiceStatus value = new ServiceStatus();

            try
            {
                var response = await client.PostAsync(url, content);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    value = JsonConvert.DeserializeObject<ServiceStatus>(result);
                }
                else
                {
                    //result.ErrorMessage = "Error connecting to server.";
                    // result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                //errorMessage = ex.Message;
                //result.ErrorMessage = ex.Message;
                // result.IsSuccess = false;
                value.ErrorMessage = ex.Message;
            }

            return value;
        }
    }
}
