using System;
namespace DAL
{
    partial class Setting
    {
    }

    partial class BLevelDate
    {
    }

    partial class Attendence
    {
    }

    partial class PayrollDataContext
    {
        
    }

    partial class MailMerge
    {
        public string CreatedByName { get; set; }
    }

    
    
    partial class AppraisalRollout
    {
        public string LevelName { get; set; }
        public string FormName { get; set; }
        public string StatusModified { get; set; }
        public string EmployeeName { get; set; }
    }

    partial class GetAppraisalRolloutSPResult
    {
        //public string LevelName { get; set; }
        //public string FormName { get; set; }
        public string StatusModified { get; set; }
       
    }

    partial class AppraisalFormQuestionnaire
    {
        public string CategoryName { get; set; }
        public int ? CategorySequence { get; set; }
        public string SN { get; set; }
        public bool IsCategory { get; set; }
    }


    partial class GetTravelRequestsForFinanceResult
    {
        public string AdvanceStatusModified { get; set; }
        public decimal ToBeRecieved { get; set; }
        public decimal ToBePaid { get; set; }
    }


    partial class DashboardOnLeaveThisMonthResult
    {
        public string SubstituteEmpName { get; set; }
    }

    public partial class GetTravelRequestAdvanceResult
    {
        public decimal EstimatedTotal { get; set; }
        public string AdvanceStatusText { get; set; }
    }

    public partial class GetTravelRequestSettledResult
    {
        public decimal SettledTotal { get; set; }
        public decimal EstimatedTotal { get; set; }
        public decimal ToBePaid { get; set; }
        public decimal ToBeRecovered { get; set; }
    }



    public partial class TARequestLine 
    {

        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int AllowType { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ApprovedAmout { get; set; }
        public bool IsEditable { get; set; }

        public decimal HRTotal { get; set; }
    }

    public partial class Resource
    {
        public string ResourceCategoryName;
    }

    public partial class TARequestStatusHistory
    {
        public string whoseComment { get; set; }
        public string ApprovedByName { get; set; }
    }

    public partial class BranchDepartment
    {
        public string BranchName { get; set; }
        public string DepartmentName { get; set; }
        public string HeadName { get; set; }
    }
}
