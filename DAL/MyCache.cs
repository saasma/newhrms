using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Xml;
using DAL;
using System.Xml.Linq;
using System.Collections;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Web.Caching;







namespace BLL
{
	/// <summary>
	/// Summary description for MyCache.
	/// </summary>
	public class MyCache
    {
        //public static string Cache_Common_Key = "Common_{0}";

       
        //public static string Cache_Common_Key_BLL = "Common_BLL_{0}";

        public static void SaveToCacheFromBLL(string key, object value)
        {
            SaveToGlobalCache(key, value, ExpirationType.Sliding, veryLongTime);
        }
        public static object GetFromCacheFromBLL(string key)
        {
            return GetFromGlobalCache(key);
        }

        public static void SaveToCache<T>(string cacheId, T value)
        {
            SaveToGlobalCache(cacheId, value, ExpirationType.Sliding, veryLongTime);
        }
        public static T GetFromCache<T>(string cacheId)
        {
            object value = GetFromGlobalCache(cacheId);
            if (value != null)
                return (T)value;

            return default(T);
        }


        public static XmlDocument GetBreadCumbsXml()
        {
            if (GetFromGlobalCache("BreadCumb") != null)
                return (GetFromGlobalCache("BreadCumb") as XmlDocument) as XmlDocument;


            XmlDocument doc = new XmlDocument();
            //doc.Load(HttpContext.Current.Server.MapPath("~/BreadCrumb.xml"));
            doc.Load(HttpContext.Current.Server.MapPath("~/SiteMap.xml"));

            SaveToGlobalCache("BreadCumb", doc, ExpirationType.Sliding, veryLongTime);

            return doc.Clone() as XmlDocument;
        }

      

        public static XmlDocument GetMenuXml(string userGuid,bool hasLevelGrade)
        {
            if (GetFromGlobalCache("Menu" + userGuid) != null)
                return GetFromGlobalCache("Menu" + userGuid) as XmlDocument;
            

            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath("~/SiteMap.xml"));
            //doc = AppendFavouriteMenuItem(doc);
            SetVisiblityFromDB(doc.ChildNodes, hasLevelGrade);

            SaveToGlobalCache("Menu" + userGuid, doc, ExpirationType.Sliding, veryLongTime);

            return doc;
        }

        public static List<UPage> GetHiddenPageList()
        {
            return
                BaseBiz.PayrollDataContext.UPages.Where(p => p.IsVisible == false).ToList();
        }

        /// <summary>
        /// To make the visible or not using db field, mainly used for hiding some pages to some company like,
        /// hiding Overtime.aspx page to HPL
        /// </summary>
        /// <param name="doc"></param>
        private static void SetVisiblityFromDB(XmlNodeList childNodes,bool hasLevelGrade)
        {
            try
            {

                //iterate each page & if has "checkVisibilityInDB" attribute then query the db to make the page visible or not

                foreach (XmlNode node in childNodes)
                {

                    //if (node.Attributes != null && hasLevelGrade == false
                    //    && node.Attributes["hideForNonLevelCompany"] != null && node.Attributes["hideForNonLevelCompany"].Value.ToLower().Trim() == "true")
                    //{
                    //    node.Attributes["visible"].Value = "false";
                    //}

                    if (node.Attributes != null && node.Attributes["checkVisibilityInDB"] != null)
                    {
                        if (hiddenPages == null)
                        {
                            hiddenPages = GetHiddenPageList();
                        }

                        //now set visiblity attribute from db
                        if (node.Attributes["pageId"] != null)
                        {
                            bool visible = GetPageVisibilityAttribute(node.Attributes["pageId"].Value);

                            if (visible == true)
                            {
                                if (node.Attributes["visible"] != null)
                                    node.Attributes.Remove(node.Attributes["visible"]);
                            }
                            else
                                node.Attributes["visible"].Value = "false";
                        }
                    }

                    SetVisiblityFromDB(node.ChildNodes, hasLevelGrade);
                }
            }
            catch { }
        }

        private static bool GetPageVisibilityAttribute(string pageId)
        {
            int id = int.Parse(pageId);

            UPage page = hiddenPages.Find(p => p.PageId == id);

            if (page != null && page.IsVisible == false)
                return false;

            return true;
        }

        private static System.Collections.Generic.List<UPage> hiddenPages = null;

        

        

        //public const string Key_BrandList = "BrandList";
        //public const string Key_ModelList = "ModelList_BrandId{0}";
        //public const string Key_CitiesList = "CityList";

        //public const int Brand_Model_Car_Minutes = 120;

        public const int veryLongTime = 24*60;
		
		
		/// <summary>
		/// Remember the global cache object
		/// 
		/// System.Web.Caching.Cache
		/// One instance of this class is created per application domain, and it remains valid 
		/// as long as the application domain remains active. Information about an instance of 
		/// this class is available through the Cache property of the HttpContext object or the 
		/// Cache property of the Page object.
		/// </summary>

		public static Cache globalApplicationCache = null;



		private static void InitGlobalApplicationCache() {

			if (globalApplicationCache == null) {
				lock (typeof (MyCache)) {
					if (globalApplicationCache == null) {
						//if (HttpContext.Current != null && HttpContext.Current.Cache != null) {
                        if( HttpRuntime.Cache !=null){
                            globalApplicationCache = HttpRuntime.Cache;//HttpContext.Current.Cache;
						} 
					}
				}
			} 

		}

		public enum ExpirationType
		{
			Sliding,
			Absolute
		}

		// public MyCache() {}

		public static void Reset() {
			InitGlobalApplicationCache();
			if (globalApplicationCache != null) {
				lock (globalApplicationCache) {
					foreach (DictionaryEntry entry in globalApplicationCache) globalApplicationCache.Remove(entry.Key.ToString());
				}
			}
           
		}

		public static void SaveToGlobalCache(string key_, object value_, ExpirationType expiration, int minutes) {
			InitGlobalApplicationCache();
			if (globalApplicationCache != null) {
				// absoluteExpiration must be DateTime.MaxValue or slidingExpiration must be timeSpan.Zero. 
				// keep for sliding 2 hours
				switch (expiration) {
                    case ExpirationType.Absolute:
						globalApplicationCache.Insert(key_, value_, null,
						                              DateTime.Now.AddMinutes(minutes), TimeSpan.Zero);
						break;
                    case ExpirationType.Sliding:
						globalApplicationCache.Insert(key_, value_, null,
						                              DateTime.MaxValue, new TimeSpan(0, minutes, 0));
						break;
				}
				
			}
		}


		public static object GetFromGlobalCache(string key_) {
			InitGlobalApplicationCache();
			if (globalApplicationCache == null) return null;
			else {
				object res = globalApplicationCache[key_];
				return res;
			}
		}

		public static void DeleteFromGlobalCache(string key_) {
			InitGlobalApplicationCache();
			if (globalApplicationCache != null) {
				globalApplicationCache.Remove(key_);
			}
		}

		public static void DeleteFromObjectCache(string type, string id) {
			InitGlobalApplicationCache();

			string key_ = type + id;

			if (globalApplicationCache != null) {
				globalApplicationCache.Remove(key_);
			}
		}


		public static void SaveToObjectCache(string type, string id, object value_, int slidingMinutes) {
			InitGlobalApplicationCache();

			string key_ = type + id;

			if (globalApplicationCache != null) {
				// absoluteExpiration must be DateTime.MaxValue or slidingExpiration must be timeSpan.Zero. 
				// keep for sliding 10 minutes
				globalApplicationCache.Insert(key_, value_, null,
				                              DateTime.MaxValue, new TimeSpan(0, slidingMinutes, 0));
				
			}
		}

		public static object GetFromObjectCache(string type, string id) {
			InitGlobalApplicationCache();

			string key_ = type + id;

			if (globalApplicationCache == null) return null;
			else {
				object res = globalApplicationCache[key_];
				
				return res;
			}
		}


		public static object GetFromObjectCacheAndForget(string type, string id) {
			InitGlobalApplicationCache();

			string key_ = type + id;

			if (globalApplicationCache == null) return null;
			else {
				object res = globalApplicationCache[key_];
				globalApplicationCache.Remove(key_);
			
				return res;
			}
		}

		public static void SaveToUserCacheAbsolute(string user, string key_, object value_) {
			InitGlobalApplicationCache();
			Debug.Assert(user.Length > 0);
			if (globalApplicationCache != null) {
				// keep for 2 minutes and not sliding (mostly intended to keep over requests)
				globalApplicationCache.Insert(user + "<>" + key_, value_, null,
				                              DateTime.Now.AddMinutes(2), TimeSpan.Zero);
				
			}
		}

		public static void SaveToUserCacheSliding(string user, string key_, object value_) {
			InitGlobalApplicationCache();
			Debug.Assert(user.Length > 0);
			if (globalApplicationCache != null) {
				// keep for 60 minutes sliding (mostly intended to keep heavy operations cached, but save memory when logged out)
				globalApplicationCache.Insert(user + "<>" + key_, value_, null,
				                              DateTime.MaxValue, TimeSpan.FromMinutes(60));
				
			}
		}

		public static object GetFromUserCache(string user, string key_) {
			InitGlobalApplicationCache();
			Debug.Assert(user.Length > 0);
			if (globalApplicationCache == null) return null;
			else {
				object res = globalApplicationCache[user + "<>" + key_];
				
				return res;
			}
		}

		/// <summary>
		/// Remove all cached values for a user.
		/// Call this on explicit logout, so that the cache is fresh when the user logs in again
		/// Do not call this on time-out logouts, since we want the user to login fast.
		/// </summary>
		/// <param name="user"></param>
		public static void ResetUserCache(string user) {
			InitGlobalApplicationCache();
			Debug.Assert(user.Length > 0);
			if (globalApplicationCache != null) {
				foreach (DictionaryEntry entry in globalApplicationCache) {
					string key = entry.Key.ToString();
					if (key.StartsWith(user + "<>")) globalApplicationCache.Remove(entry.Key.ToString());
				}
			}
		}

		/// <summary>
		/// Remove all cached values if the key contains a substring for example ACL<>accountid
		/// </summary>
		/// <param name="pattern"></param>
		public static void ResetCacheWithPattern(string pattern) {
			InitGlobalApplicationCache();
			Debug.Assert(pattern.Length > 0);
			if (globalApplicationCache != null) {
				foreach (DictionaryEntry entry in globalApplicationCache) {
					string key = entry.Key.ToString();
					if (key.IndexOf(pattern) >= 0) globalApplicationCache.Remove(entry.Key.ToString());
				}
			}
		}

		/// <summary>
		/// Remove all cached values if the key contains a substring for example ACL<>accountid
		/// </summary>
		/// <param name="pattern"></param>
		public static void ResetCacheWithPattern(Regex pattern) {
			InitGlobalApplicationCache();
			if (globalApplicationCache != null) {
				foreach (DictionaryEntry entry in globalApplicationCache) {
					string key = entry.Key.ToString();
					bool match = pattern.IsMatch(key);
					if (match) {
						globalApplicationCache.Remove(entry.Key.ToString());
					}
				}
			}
		}

	
	}
}