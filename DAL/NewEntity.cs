﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{

  

    public partial class GetEmployeesByBranchResult
    {
        public string IDAndName
        {
            set { }
            get
            {
                return this.Name + " - " + this.EmployeeId;
            }
        }
    }


    public partial class NoticeBoard
    {
        public string StatusStr { get; set; }
        public string PublishedDateStr { get; set; }
        
    }



    public class TrainingFeedbackBo
    {
        public string SN { get; set; }
        public string Name { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Rating { get; set; }
        public string SLBefore { get; set; }
        public string SLAfter { get; set; }
        public string Comment { get; set; }

    }


    public partial class EveningCounterRequest
    {
        public string EmpName { get; set; }
        public bool IsApprove { get; set; }
        public bool IsRecomend { get; set; }
        public string StatusStr { get; set; }
        public string EveningCounterTypeStr { get; set; }
    }

    public partial class GetGradeRewardListResult
    {
        public string StatusText { get; set; }
    }

    public partial class GetEveningCounterRequestForEmpSPResult
    {
        public string StatusStr { get; set; }
        public string EveningCounterTypeStr { get; set; }
        public string FormattedStartDate { get; set; }
        public string FormattedEndDate { get; set; }
    }

    public partial class BLevel
    {
        public string GroupName { get; set; }
        public string ParentName { get; set; }
        public string LoanAmount { get; set; }
        public string GroupLevel
        {
            get
            {
                return this.BLevelGroup.Name + " - " + this.Name;
            }
            set { }
        }
        public string ClericalTypeName
        {
            get
            {
                return this.ClericalType== null ? "" : (this.ClericalType == 1 ? "Clerical" : "Non Clerical");
            }
            set { }
        }
    }

    public partial class Branch
    {
        public string DepartmentList { get; set; }
        public string NameAndCode
        {
            get
            {
                if (string.IsNullOrEmpty(this.Code))
                    return this.Name;

                return this.Code + " - " + this.Name;
            }
            set { }
        }
        public string BranchName
        {
            get
            {
                if (this.IsActive != null && this.IsActive.Value == false)
                    return this.Name + "(Inactive)";

                return this.Name;
            }
            set { }
        }
    }
    public partial class AppraisalCompetency
    {
        public string CategoryName { get; set; }
    }

    public class NewHREmployeeSkillSetBO
    {
        public string SkillSetName { get; set; }
        public int EmployeeId { get; set; }
        public int SkillSetId { get; set; }
        public string LevelOfExpertise { get; set; }
        public int IsEditable { get; set; }
    }
    public class NewHREmployeeLanguageSetBO
    {
        public string LanguageSetName { get; set; }
        public int EmployeeId { get; set; }
        public int LanguageSetId { get; set; }
        public string FluencySpeakName { get; set; }
        public string FluencyWriteName { get; set; }
        public bool IsNativeLanguage { get; set; }
        public int IsEditable { get; set; }
    }

    public partial class EDesignation
    {
        public string LevelName { get; set; }
        public string Category { get; set; }
        public string Group { get; set; }
    }
    public partial class Deputation
    {
        public string AttendanceInText { get; set; }
        public string TravellingAllowanceText { get; set; }
        public string DailyAllowanceText { get; set; }
        public string ApplicableSalaryAllowanceText { get; set; }
    }

    public partial class DeputationEmployee
    {
        public string EmployeeName { get; set; }
        public string DeputationText { get; set; }
        public string ToBranch { get; set; }
        public string ToDepartment { get; set; }
        public string ApprovedByText { get; set; }
    }
    public partial class ActingEmployee
    {
        public string EmployeeName { get; set; }

        public string FromLevel { get; set; }
        public string ToLevel { get; set; }
        public string INo { get; set; }
    }
    public partial class RewardEmployee
    {
        public string EmployeeName { get; set; }

        public string RecommendedBy { get; set; }
        public string RewardCashOrGrade { get; set; }
    }
    public partial class HDocument
    {
        public string TypeName { get; set; }
    }

    public partial class darGetNotSubmittedDatesResult
    {
        public int SN { get; set; }
    }
    public partial class Activity
    {
        public string Statuss
        {
            get;
            set;
        }
    }
    public partial class Training
    {
        public string TrainingType { get; set; }
        public int LeftSeats { get; set; }
        public int BookedSeats { get; set; }
        public string FromDateEnlgish { get; set; }
        public string ToDateEnglish { get; set; }
    }

    public  class ResourcePersongBO
    {
        public int ResourcePersonId { get; set; }
        public string Name { get; set; }
    }

    public partial class TrainingRequest
    {
        public string Name { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string TrainingType { get; set; }
        public double Days { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string EmployeeName { get; set; }
        public string ApprovedByName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int SN { get; set; }
    }
    partial class HExtraActivity
    {
        public int IsEditable { get; set; }
    }
    partial class HrNominee
    {
        public string SN { get; set; }
        public string Relation { get; set; }
        public int IsEditable { get; set; }
    }

    partial class EmployeeCashAwardType
    {
        public string IncomeName { get; set; }
    }

    partial class EmployeeCashAward
    {
        public string SN { get; set; }
        public string EmployeeName { get; set; }
        public string AwardName { get; set; }
        public string Description { get; set; }
    }
    public partial class NewActivity
    {
        public string ClientName { get; set; }
        public string SoftwareName { get; set; }
        public int IsEditable { get; set; }
        public string DurationTime { get; set; }
        public int ContainsFile { get; set; }
    }
    public partial class NewActivityDetail
    {
        public string DurationTime { get; set; }
        public int SN { get; set; }
        public DateTime StartTimeDate { get; set; }
        public DateTime? EndTimeDate { get; set; }
        public string ClientName { get; set; }
        public string ActivityName { get; set; }
        public string Name { get; set; }
    }
    public partial class GetNewActivityListResult
    {
        public string DurationTime { get; set; }
    }
    public partial class NonMonetaryAward
    {
        public int IsEditable { get; set; }
    }
    public partial class EHobby
    {
        public int IsEditable { get; set; }
        public string HobbyName { get; set; }
    }

    public partial class GetPrefixedEmpNamesWithIDAndINoResult
    {
        public string URLPhoto { get; set; }
    }

    public partial class EmployeePlanning
    {
        public string br { get; set; }
        public string dep { get; set; }
    }

   
    public class MenuSearchClass
    {
        public int moduleId { get; set; }
        public bool IsModule { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string cssClass { get; set; }
    }

    public partial class TimeRequestLine
    {
        public string DayName { get; set; }
        public string InTimeString { get; set; }
        public string OutTimeString { get; set; }
        public string WorkHoursString { get; set; }
        public DateTime? InTimeDT { get; set; }
        public DateTime? OutTimeDT { get; set; }
        public int SN { get; set; }
        public int EmpId { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public string SubmittedOnString { get; set; }
    }


    public partial class GetTrainingEmpAttendnaceResult
    {
        public DateTime? InTimeDT { get; set; }
        public DateTime? OutTimeDT { get; set; }
        public DateTime? DeviceIn { get; set; }
        public DateTime? DeviceOut { get; set; }
        public string Department { get; set; }
    }
    


    public partial class TrainingPlanningLineBO
    {
        public string LineID { get; set; }
        public DateTime? DateEng { get; set; }
        public string Day { get; set; }
        //public string StartTime { get; set; }
        //public string EndTime { get; set; }
        public string InTimeDT { get; set; }
        public string OutTimeDT { get; set; }
        public string Hours { get; set; }
        public string IsTraining { get; set; }
        public string ResourcePerson { get; set; }
    }


    public class TrainingInviteEmpBo
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
    }

    public  class TrainingTitleBO
    {
        public string Name { get; set; }
        public string DateInfo{get;set;}
        public string Host { get; set; }
        public string BookingInfo { get; set; }
        public string Seats { get; set; }
        public string Booked { get; set; }
        public string Avilable { get; set; }
        public string ViewLink { get; set; }
    }

    public partial class TrainingPlanningLine
    {
      
        public DateTime? InTimeDT { get; set; }
        public DateTime? OutTimeDT { get; set; }
    }



    public partial class AttendanceEmpComment
    {
        public string StatusName { get; set; }
        public string Name { get; set; }
    }
    public partial class TimeRequest
    {
        public string StatusName { get; set; }
        public string EmployeeName { get; set; }

        public string StartDateEngText
        {
            set { }
            get { return this.StartDateEng.Value.ToString("yyyy-MM-dd"); }
        }
        public string EndDateEngText
        {
            set { }
            get { return this.EndDateEng.Value.ToString("yyyy-MM-dd"); }
        }
        public string SubmittedOnEngText
        {
            set { }
            get { return this.SubmittedOnEng.Value.ToString("yyyy-MM-dd"); }
        }
    }
    public partial class GetChildrenAllowanceResult
    {
        public string StartDateEngText
        {
            set { }
            get { return this.StartDate.ToString("yyyy-MM-dd"); }
        }
        public string EndDateEngText
        {
            set { }
            get { return this.EndDate.ToString("yyyy-MM-dd"); }
        }
        public string DOBEngText
        {
            set { }
            get { return this.Dob.ToString("yyyy-MM-dd"); }
        }
    }

    public partial class GetOvertimeRequestForManagerResult
    {
        public bool CanRecOrAppRec { get; set; }
    }

    public partial class BranchDepartmentHistory
    {
        public string FromDateEngFormatted { get; set; }
    }

    public partial class GetDesignationTransfersResult
    {
        public string FromDateEngFormatted { get; set; }
    }

    public partial class GetLocationTransfersResult
    {
        public string FromDateEngFormatted { get; set; }
    }

    public partial class GetTravelRequestsForAdminResult
    {
        public string TotalInString { get; set; }
    }

    public partial class GetTravelRequestsForAdminResult
    {
        public string FromDateEngFormatted { get; set; }
        public string ToDateEngFormatted { get; set; }
        public string ExtendedFormatted { get; set; }
    }

    public partial class GetTravelRequestAdvanceResult
    {
        public string FromDateEngFormatted { get; set; }
        public string ToDateEngFormatted { get; set; }
    }

    public partial class GetTravelRequestsByUserResult
    {
        public string FromDateEngFormatted { get; set; }
        public string ToDateEngFormatted { get; set; }
    }

    public partial class GetAllEmployeeSearchNewResult
    {
        public string ServicePeriodTillNow
        {
            set { }
            get
            {
                int years, months, days, hours;
                int minutes, seconds, milliseconds;

                
                //if (firstStatus != null)
                {
                    string WorkingFor = string.Empty;
                    NewEntityHelper.GetElapsedTime(this.JoinDateEng.Value, DateTime.Today.Date, out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                    string text = "";
                    if (years != 0)
                        text = " " + years + " years";
                    if (months != 0)
                        text += " " + months + " months";
                    if (days != 0)
                        text += " " + days + " days";

                    WorkingFor += text;

                    return WorkingFor;
                   
                }

            }

        }

        public string JoinDateEngText { get; set; }
    }

    public partial class GetOvertimeRequestForManagerNewResult
    {
        public bool CanRecOrAppRec { get; set; }
    }

    public partial class GetTimeRequestsForApprovalResult
    {
        public string StartDateEngText
        {
            set { }
            get { return this.StartDateEng.Value.ToString("yyyy-MM-dd"); }
        }
        public string EndDateEngText
        {
            set { }
            get { return this.EndDateEng.Value.ToString("yyyy-MM-dd"); }
        }
        public string SubmittedOnEngText
        {
            set { }
            get { return this.SubmittedOnEng.Value.ToString("yyyy-MM-dd"); }
        }
        public string DateRange { get; set; }
        public string ReqType { get; set; }
        public string Reason { get; set; }
    }

    public partial class ActivityComment
    {
        public string Name { get; set; }
    }

    public partial class PDeduction
    {
        public string CalculationType { get; set; }
        public bool DeductionDeletable { get; set; }
        public string IsEnabledValue { get; set; }
        public bool CanBeEnabledDisabled { get; set; }
    }

    public partial class GetWeeklyHolidayResult
    {
        public string DayofWeekName { get; set; }
        public string IsFullDayValue { get; set; }
    }

    public partial class DocSubCategory
    {
        public string CategoryName { get; set; }
    }

    public partial class DocComment
    {
        public string imageUrl { get; set; }
        public string TimeDetail { get; set; }
    }

    public partial class AttendanceAllEmployeeReportNewResult
    {
        public string InRemarksText { get; set; }
        public string OutRemarksText { get; set; }
        public string InTimeString
        {
            set { }
            get { return (this.InTime == null ? "" : DateTime.Parse(this.InTime.Value.ToString()).ToString("hh:mm tt")); }
        }
        public string OutTimeString
        {
            set { }
            get { return (this.OutTime == null ? "" : DateTime.Parse(this.OutTime.Value.ToString()).ToString("hh:mm tt")); }
        }
        public string LunchInString
        {
            set { }
            get { return (this.LunchIn == null ? "" : DateTime.Parse(this.LunchIn.Value.ToString()).ToString("hh:mm tt")); }
        }
        public string LunchOutString
        {
            set { }
            get { return (this.LunchOut == null ? "" : DateTime.Parse(this.LunchOut.Value.ToString()).ToString("hh:mm tt")); }
        }
    }

    public class EmpBranchAttendance
    {
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public int TotalEmployeesCount { get; set; }
        public int PresentEmployeesCount { get; set; }
        public string Date { get; set; }
    }

    public class EmpBranchAttendanceDetails
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string UrlPhoto { get; set; }
        public string InTimeValue { get; set; }
        public string OutTimeValue { get; set; }
        public string cssCls { get; set; }
        public int SN { get; set; }
        public string cssCls2 { get; set; }
        public string cssCls3 { get; set; }
    }
    public partial class GetActivityListForAdminResult
    {
        public int SN { get; set; }
        public string ClsName { get; set; }
    }
    public partial class EmpActivityCommentCls
    {
        public string CommentBy { get; set; }
        public string Comment { get; set; }
        public DateTime CommentedOn { get; set; }
    }

}
