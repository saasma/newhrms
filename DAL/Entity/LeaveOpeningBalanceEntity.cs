﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Entity
{
    /// <summary>
    /// Serialized entity class as sometime we have to save in viewstate or passed to client side
    /// </summary>
    [Serializable]
    public class LeaveOpeningBalEntity
    {
        public int LeaveTypeId { get; set; }
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public double OpeningBalance { get; set; }
        public double CurrentYearBalance { get; set; }
        public double OpeningTaken { get; set; }
        public bool IsOpeningBalanceSaved { get; set; }
        public bool IsPaymentStopped { get; set; }
        public bool IsEditMode { get; set; }
        public string LeaveName { get; set; }
        public bool IsAttedanceSaved { get; set; }
        public long RowNumber { get; set; }
        public int TotalRecords { get; set; }

        public string StatusText { get; set; }
    }

    [Serializable]
    public  class LeaveOpeningBalEntityCol : List<LeaveOpeningBalEntity>
    {
        
    }
}
