﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Entity
{
    public enum ProjecColumnType
    {
        HoursWorked = -1,
        //PF = 0,
        Income = 1,
        IncomeRateHour = 2,
        IncomeAmount = 3,
        PF=4,
        TotalSalary = 5,
        
    }
}
