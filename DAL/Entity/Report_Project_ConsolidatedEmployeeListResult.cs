﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Base;


namespace DAL
{
    public partial class Report_Project_ConsolidatedEmployeeListResult
    {

        private List<CalculationValue> list = null;

        private decimal totalIncome = 0;
        private decimal totalDeduciton = 0;
        
        ///// <summary>
        ///// Returns the comment if Adjustment column
        ///// </summary>
        //public string GetComment(int type, int sourceId)
        //{
        //    if (list != null)
        //    {
        //        foreach (var calculationValue in list)
        //        {
        //            if (!string.IsNullOrEmpty(calculationValue.AdjustmentComment)
        //                && calculationValue.Type == type && calculationValue.SourceId == sourceId)
        //            {
        //                return calculationValue.AdjustmentComment;
        //            }
        //        }

        //    }
        //    return string.Empty;
        //}

        //public string GetList(int type, int sourceId)
        //{
        //    if (list != null)
        //    {
        //        foreach (var calculationValue in list)
        //        {
        //            if (calculationValue.Type == type && calculationValue.SourceId == sourceId)
        //            {
        //                return calculationValue.ListOfId;
        //            }
        //        }

        //    }
        //    return string.Empty;
        //}

        /// <summary>
        /// Returns the cell value
        /// </summary>
        public decimal? GetCellValue(int type, int sourceId,int projectId, int decimalPlaces, Dictionary<string, string> headerList)
        {


            if (!string.IsNullOrEmpty(this.List))
            {
                if (list == null)
                {
                    list = new List<CalculationValue>();

                    string[] values = this.List.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    //incomeValues = new string[values.Length, 2];
                    CalculationValue value = null;
                    foreach (string valueEach in values)
                    {
                        //Generally cellvalues contains like 
                        // Type:SourceId:Value:[Adj comment if Adjustment type | List of Special Event if SpecialEvent type]
                        string[] cellvalues = valueEach.Split(new char[] { ':' });

                        // if - comes from db then it is error or value is not set
                        if (cellvalues[2].Equals("-"))
                        {
                            //throw new ArgumentException("Values not set.");

                            value = new CalculationValue()
                            {
                                Type = int.Parse(cellvalues[0]),
                                SourceId = int.Parse(cellvalues[1]),
                                ProjectId = int.Parse(cellvalues[2]),
                                Amount = -1 //-1 for invalid error type
                            };
                        }
                        else
                        {


                            value = new CalculationValue()
                            {
                                Type = int.Parse(cellvalues[0]),
                                SourceId = int.Parse(cellvalues[1]),
                                ProjectId = int.Parse(cellvalues[2]),
                                Amount = decimal.Parse(cellvalues[3])
                               
                            };



                        }

                        //if ((value.ColumnType == CalculationColumnType.IncomeAdjustment ||
                        //         value.ColumnType == CalculationColumnType.DeductionAdjustment)
                        //        && cellvalues.Length > 3)
                        //    value.AdjustmentComment = cellvalues[3];

                        //For Special event & normal income, 4 value contains id of special event or increment id to
                        //be saved in CCalculationIncluded table
                        if ((value.ColumnType == CalculationColumnType.Income
                                //|| value.ColumnType == CalculationColumnType.IncomeInsurance
                                || value.ColumnType == CalculationColumnType.DeductionInsurance) && cellvalues.Length > 3)
                        {
                            value.ListOfId = cellvalues[3];
                        }

                        list.Add(value);

                    }

                }


                CalculationColumnType columnType = (CalculationColumnType)type;

                // If income total then generate total
                if (columnType == CalculationColumnType.IncomeGross && totalIncome == 0)
                {
                    foreach (var calculationValue in list)
                    {
                        //if income type & its header exists then sum
                        if (CalculationValue.IsColumTypeIncome(calculationValue.ColumnType) &&
                            ((headerList != null && headerList.ContainsKey(calculationValue.Type + ":" + calculationValue.SourceId)) || headerList == null)
                            )
                        {
                            if (calculationValue.Amount != -1)
                                totalIncome +=
                                    decimal.Parse(BaseHelper.GetCurrency(calculationValue.Amount, decimalPlaces));
                        }
                    }
                    return totalIncome;
                }
                // Deduction total
                else if (columnType == CalculationColumnType.DeductionTotal && totalDeduciton == 0)
                {
                    foreach (var calculationValue in list)
                    {
                        if (CalculationValue.IsColumTypeDeduction(calculationValue.ColumnType) &&
                            ((headerList != null && headerList.ContainsKey(calculationValue.Type + ":" + calculationValue.SourceId)) || headerList == null)
                            )
                        {
                            if (calculationValue.Amount != -1)// -1 is Invalid case
                            {
                                //skip adding -ve tds in deduction
                                if (
                                    (calculationValue.ColumnType == CalculationColumnType.TDS || calculationValue.ColumnType == CalculationColumnType.SST)
                                    && calculationValue.Amount <= 0) { }
                                else
                                    totalDeduciton += decimal.Parse(BaseHelper.GetCurrency(calculationValue.Amount, decimalPlaces));
                            }
                        }
                    }
                    return totalDeduciton;
                }
                else if (columnType == CalculationColumnType.NetSalary)
                {
                    return totalIncome - totalDeduciton;
                }
                else
                {

                    foreach (var calculationValue in list)
                    {
                        if (calculationValue.Type == type && calculationValue.SourceId == sourceId &&  calculationValue.ProjectId == projectId)
                        {
                            return calculationValue.Amount;
                        }
                    }
                }
            }
            return null;
        }

      

    }
}
