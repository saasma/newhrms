﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace DAL
{
    public partial class BranchDepartmentHistory
    {
        public string TimeElapsed { get; set; }
        public string INo { get; set; }
    }
}
