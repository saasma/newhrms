﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Calendar;
using Utils.Security;
using System.Web;



namespace DAL
{
    public partial class GetMonetoryChangeLogsResult
    {
        public string ActionName { get; set; }
        public string TypeName { get; set; }
    }
    public partial class AppraisalRolloutFlow
    {
        public string StepIDName { get; set; }
        public int StepID { get; set; }
    }
    public partial class AppraisalFormDistribution
    {
        public string Name { get; set; }

    }
    public partial class QueryBuilderList
    {
        public int SN { get; set; }
    }
    public partial class GetGlobusSalaryVoucherReportResult
    {
        //public string TranCodeCr { get; set; }
    }
    public partial class PayGroupCalculation
    {
        public string PayName { get; set; }
    }
    public partial class PayGroup
    {
        public string Income { get; set; }
    }
    public partial class Report_HR_TeamEmployeeLeaveBalanceResult
    {

        public double? LeaveBalance1 { get; set; }
        public double? LeaveBalance2 { get; set; }
        public double? LeaveBalance3 { get; set; }
        public double? LeaveBalance4 { get; set; }
        public double? LeaveBalance5 { get; set; }
        public double? LeaveBalance6 { get; set; }
        public double? LeaveBalance7 { get; set; }
        public double? LeaveBalance8 { get; set; }
        public double? LeaveBalance9 { get; set; }
        public double? LeaveBalance10 { get; set; }
        public double? LeaveBalance11 { get; set; }
        public double? LeaveBalance12 { get; set; }
        public double? LeaveBalance13 { get; set; }
        public double? LeaveBalance14 { get; set; }
        public double? LeaveBalance15 { get; set; }

        public double? LeaveRequestCount { get; set; }
        public double? LeaveRecommendedCount { get; set; }

        public double? TimeRequestCount { get; set; }
        public double? TimeRecommendedCount { get; set; }



    }
    public partial class AppraisalRolloutEmployeeListResult
    {
        public AppraisalEmployeeForm EmployeeForm { get; set; }
    }
    //public partial class GetLeaveApprovalApplyToCCForEmployeeResult
    //{
    //    public string ApplyToOtherName { get; set; }
    //    public string CC1OtherName { get; set; }
    //}
    public class FamilyHeader
    {
        public string RelationName { get; set; }
        public string RelationNameWithIndex { get; set; }
    }
    public partial class GetFamilyListReportResult
    {
        public int SN { get; set; }
        public string Family1 { get; set; }
        public string Family2 { get; set; }
        public string Family3 { get; set; }
        public string Family4 { get; set; }
        public string Family5 { get; set; }
        public string Family6 { get; set; }
        public string Family7 { get; set; }
        public string Family8 { get; set; }
        public string Family9 { get; set; }
        public string Family10 { get; set; }
        public string Family11 { get; set; }
        public string Family12 { get; set; }
        public string Family13 { get; set; }
        public string Family14 { get; set; }
        public string Family15 { get; set; }
        public string Family16 { get; set; }
        public string Family17 { get; set; }
        public string Family18 { get; set; }

        public string Family19 { get; set; }
        public string Family20 { get; set; }
        public string Family21 { get; set; }
        public string Family22 { get; set; }
        public string Family23 { get; set; }
        public string Family24 { get; set; }
        public string Family25 { get; set; }
    }
    public partial class GetFamilyListForInsurenceReportResult
    {
        public int SN { get; set; }
        public string Family1 { get; set; }
        public string Family2 { get; set; }
        public string Family3 { get; set; }
        public string Family4 { get; set; }
        public string Family5 { get; set; }
        public string Family6 { get; set; }
        public string Family7 { get; set; }
        public string Family8 { get; set; }
        public string Family9 { get; set; }
        public string Family10 { get; set; }
        public string Family11 { get; set; }
        public string Family12 { get; set; }
        public string Family13 { get; set; }
        public string Family14 { get; set; }
        public string Family15 { get; set; }
        public string Family16 { get; set; }
        public string Family17 { get; set; }
        public string Family18 { get; set; }

        public string Family19 { get; set; }
        public string Family20 { get; set; }
        public string Family21 { get; set; }
        public string Family22 { get; set; }
        public string Family23 { get; set; }
        public string Family24 { get; set; }
        public string Family25 { get; set; }
    }

    public partial class Company
    {
        public bool HasOtherPFFund { get; set; }
        public string OtherPFFundName { get; set; }
    }
    public partial class PastRetirementAdjustment
    {
        public string Name { get; set; }
        public string PeriodName { get; set; }
    }
    public partial class EmployeeServiceHistory
    {
        public int? FromBranchId { get; set; }
        public int? FromDepId { get; set; }
    }

    public partial class LeaveFareEmployee
    {
        //public decimal Amount{get;set;}
        public string IsFull { get; set; }
    }
    public partial class GetLeaveFareEmployeeListResult
    {
        public string LeaveStatusName { get; set; }
        public int PayrollPeriodId { get; set; }
    }
    public partial class GetEmployeeLeaveDaysForPeriodResult
    {
        //public int EmployeeId { get; set; }
        //public string Name { get; set; }
        public double TotalDays { get; set; }
        public double WH { get; set; }
        public double PH { get; set; }
        public double StdDays { get; set; }
        public double LT1 { get; set; }
        public double LT2 { get; set; }
        public double LT3 { get; set; }
        public double LT4 { get; set; }
        public double LT5 { get; set; }
        public double LT6 { get; set; }
        public double LT7 { get; set; }
        public double LT8 { get; set; }
        public double LT9 { get; set; }
        public double LT10 { get; set; }
        public double LT11 { get; set; }
        public double LT12 { get; set; }
        public double UPL { get; set; }
        public double AbsDays { get; set; }
        public double LeaveDays { get; set; }
        public double WorkDays { get; set; }
        public double Percentage { get; set; }

        //public double TotalDays { get; set; }
        //public double StdDays { get; set; }
        //public double LeaveDays { get; set; }
        //public double WorkDays { get; set; }
        //public double Percentage { get; set; }
    }
    public partial class AppraisalPeriod
    {
        public string StartNepDate { get; set; }
        public string EndNepDate { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string StatusName { get; set; }
    }
    public partial class Report_CompleteYearTaxReportResult
    {
        public decimal SSTTotalAmount { get; set; }
        public decimal TDSTotalAmount { get; set; }
        public decimal TotalSST { get; set; }
        public decimal TotalTDS { get; set; }
        public decimal TotalAddOnSST { get; set; }
        public decimal TotalAddOnTDS { get; set; }
    }
    public partial class AppraisalCategory
    {
        public double Weight { get; set; }
        public string FormattedName { get; set; }

        public double EmployeePoints { get; set; }
        public double SupervisorPoints { get; set; }

    }
    public partial class AppraisalFormPoint
    {
        public string Name { get; set; }
        public bool IsParent { get; set; }
        public bool IsEditable { get; set; }
        public bool IsCompetencyTotal { get; set; }
        public bool IsQuestionaireTotal { get; set; }
        public bool IsTotalGroup { get; set; }
        public bool ShowEmptyInPointColumn { get; set; }
        public bool ShowEmptyInScoreColumn { get; set; }
        public string PointsValue { get; set; }
        public string PercentageValue { get; set; }
        public bool IsTextRow { get; set; }

        public int Type { get; set; }
        public int SourceId { get; set; }
        public double Points { get; set; }
        public double Percentage { get; set; }
    }
    public partial class AttendanceEmpComment
    {
        public string EmployeeName { get; set; }
    }
    public partial class GetFestivalPaymentEmployeeListResult
    {
        public string JoinDateText
        {
            set { }
            get
            {
                return this.JoinDate.Value.ToString("dd-MMM-yyyy");
            }
        }
        public string PreventResetText
        {
            set { }
            get
            {
                return this.PreventReset == true ? "Yes" : "";
            }
        }
    }
    public partial class AppraisalRatingScaleLine
    {
        public string LabelScore
        {
            set { }
            get
            {
                return this.Label + " (" + this.Score.ToString() + ")";
            }
        }
    }
    public partial class AppraisalEmployeeFormTarget
    {
        public string Name { get; set; }
        public double DefaultTargetValue { get; set; }
        public double Achievement { get; set; }

        public double NewWeight { get; set; }
        public string TargetTextValue { get; set; }

        public string TargetForDisplay { get; set; }
        public string AchievementForDisplay { get; set; }
    }
    public partial class AppraisalFormTarget
    {
        public int? TargetID { get; set; }
        public string Name { get; set; }
        public double Achievement { get; set; }
        public double DefaultTargetValue { get; set; }

        public double NewWeight { get; set; }
        public string TargetTextValue { get; set; }
        public string AchievementTextValue { get; set; }
    }
    public partial class Report_GetLoanRepaymentResult
    {
        public string TakenOnText { get; set; }
        public string TakenOnEngText { get; set; }
        public string EndDateText { get; set; }
        public string EndDateEngText { get; set; }
    }
    public partial class AttendanceReportResult
    {
        public string ActualDate { get; set; }
        public string RefinedInRemarks { get; set; }
        public string RefinedOutRemarks { get; set; }
        public string RefinedWorkHour { get; set; }
        public string DateEngText
        {
            set { }
            get
            {
                return (this.DateEng == null ? "" : this.DateEng.Value.ToString("yyyy-MM-dd"));
            }
        }
    }
    public partial class GetValidEmployeesForPayrollPeriodResult
    {
        public string NameEIN
        {
            set { }
            get
            {
                return this.Name + " - " + this.EmployeeId;
            }
        }
    }
    public partial class GetLeaveByDateApprovalResult
    {

        public string Created { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Range { get; set; }
    }
    public partial class LoanAdjustment
    {
        public LoanSIChangedHistory FreshLoanHistory { get; set; }
    }
    public partial class GetGeneratedOvertimeListResult
    {
        public double TotalOTHour
        {
            set { }
            get
            {


                return Math.Round(TimeSpan.FromMinutes(this.TotalOTMinute).TotalHours, 2);
            }

        }

        public double NewOTHour { get; set; }
    }
    public partial class GetOvertimeRequestForAdminResult
    {
        public string NepDate { get; set; }
        public string DateEngFormatted { get; set; }
    }
    public partial class OvertimeAutoGroup
    {
        public string LevelList { get; set; }
    }
    public partial class Report_StopHoldPaymentResult
    {
        public string StartDateText
        {
            set { }
            get
            {
                return (this.StartDate == null ? "" : this.StartDate.Value.ToString("yyyy-MM-dd"));
            }
        }
        public string EndDateText
        {
            set { }
            get
            {
                return (this.EndDate == null ? "" : this.EndDate.Value.ToString("yyyy-MM-dd"));
            }
        }

    }
    public partial class GetEmployeeLatestEventDetailsResult
    {

        public string DateOfBirthEngText
        {
            set { }
            get
            {
                return (this.DateOfBirthEng == null ? "" : this.DateOfBirthEng.Value.ToString("yyyy-MM-dd"));
            }
        }
        public string LatestEventDateText
        {
            set { }
            get
            {
                return (this.LatestEventDate == null ? "" : this.LatestEventDate.Value.ToString("yyyy-MM-dd"));
            }
        }
        public string AppointmentDateText
        {
            set { }
            get
            {
                return (this.AppointmentDate == null ? "" : this.AppointmentDate.Value.ToString("yyyy-MM-dd"));
            }
        }
    }
    public partial class Report_EmployeeReferenceResult
    {
        public string JoinDateText
        {
            set { }
            get
            {
                return (this.JoinDate == null ? "" : this.JoinDate.Value.ToString("yyyy-MM-dd"));
            }
        }
    }
    public partial class Report_AgeWiseEmployeeResult
    {

        public string BirthDateText
        {
            set { }
            get
            {
                return (this.BirthDate == null ? "" : this.BirthDate.Value.ToString("yyyy-MM-dd"));
            }
        }
        public string JoinDateText
        {
            set { }
            get
            {
                return (this.JoinDate == null ? "" : this.JoinDate.Value.ToString("yyyy-MM-dd"));
            }
        }
    }
    public partial class Report_GetEmployeeCountResult
    {
        public int SN { get; set; }
    }
    public partial class Report_GetEmployeeCountDetailsResult
    {
        public int SN { get; set; }
    }
    public partial class RemoteArea
    {
        public string Name { get; set; }
    }
    public partial class ELocation
    {
        public string RemoteArea { get; set; }
    }
    public partial class Report_GetHomeSalaryResult
    {
        public decimal AIncome1 { get; set; }
        public decimal AIncome2 { get; set; }
        public decimal AIncome3 { get; set; }
        public decimal AIncome4 { get; set; }
        public decimal AIncome5 { get; set; }
        public decimal AIncome6 { get; set; }
        public decimal APFIncome { get; set; }
        public decimal AGrossSalary
        {
            get
            {
                return AIncome1 + AIncome2 + AIncome3 + AIncome4 + AIncome5 + AIncome6 + APFIncome;
            }
            set { }

        }
        public decimal APFDeduction { get; set; }
        public decimal ACIT { get; set; }
        public decimal ATax { get; set; }
        public decimal AnnualNetSalary
        {
            get
            {
                return AGrossSalary - APFDeduction - ATax;
            }
            set { }

        }


        public decimal Income1 { get; set; }
        public decimal Income2 { get; set; }
        public decimal Income3 { get; set; }
        public decimal Income4 { get; set; }
        public decimal Income5 { get; set; }
        public decimal Income6 { get; set; }
        public decimal PFIncome { get; set; }
        public decimal GrossSalary
        {
            get
            {
                return Income1 + Income2 + Income3 + Income4 + Income5 + Income6 + PFIncome;
            }
            set { }

        }
        public decimal PFDeduction { get; set; }
        public decimal CIT { get; set; }
        public decimal Tax { get; set; }
        public decimal NetSalary
        {
            get
            {
                return GrossSalary - PFDeduction - Tax;
            }
            set { }

        }



        public void SetAnnual(int? incomeId1, int? incomeId2, int? incomeId3, int? incomeId4, int? incomeId5, int? incomeID6)
        {
            //happens due to new employee with no basic salary like 
            if (this.TaxList == null)
                return;


            string value = "", description, amount, currentAmount;
            int type, sourceId;
            decimal amountValue, amount1, amount2, currentAmountValue;
            //if (list == null)
            {
                //list = new Dictionary<string, string>();


                string[] values = this.TaxList.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string valueEach in values)
                {
                    string[] items = valueEach.Split(new char[] { ':' });

                    value = "";

                    // if has more than 5 then only the line is valid as other has not Type/SourceId info
                    if (items.Length >= 5)
                    {
                        type = int.Parse(items[4]);
                        sourceId = int.Parse(items[5]);
                        amount = items[1];
                        description = items[2];

                        currentAmountValue = 0;
                        // for current month amount
                        if (description.Contains(","))
                            description = description.Substring(description.IndexOf(",") + 1);
                        string[] amounts = description.Split(new char[] { '+' }, StringSplitOptions.RemoveEmptyEntries);
                        if (amounts.Length >= 3 && decimal.TryParse(amounts[1], out currentAmountValue))
                        {
                        }


                        PIncome income = BaseBiz.PayrollDataContext.PIncomes.SingleOrDefault(x => x.IncomeId == sourceId);

                        if (type == (int)CalculationColumnType.DeductionPF && sourceId == (int)CalculationColumnType.DeductionPF)
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.APFDeduction = amountValue;
                            }
                            this.PFDeduction = currentAmountValue;
                        }
                        if (type == (int)CalculationColumnType.IncomePF && sourceId == (int)CalculationColumnType.IncomePF)
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.APFIncome = amountValue;
                            }
                            this.PFIncome = currentAmountValue;
                        }
                        else if (type == (int)CalculationColumnType.DeductionCIT && sourceId == (int)CalculationColumnType.DeductionCIT)
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.ACIT = amountValue;
                            }
                            this.CIT = currentAmountValue;
                        }

                        else
                        {   //if already added like due to some logic dashain can come twice but only one can have value
                            decimal.TryParse(amount, out amountValue);

                            if (sourceId == incomeId1)
                            {
                                this.AIncome1 = Convert.ToDecimal(this.AIncome1) + amountValue;
                                this.Income1 = this.Income1 + currentAmountValue;
                            }
                            if (sourceId == incomeId2)
                            {
                                this.AIncome2 = Convert.ToDecimal(this.AIncome2) + amountValue;
                                this.Income2 = this.Income2 + currentAmountValue;
                            }
                            if (sourceId == incomeId3)
                            {
                                this.AIncome3 = Convert.ToDecimal(this.AIncome3) + amountValue;
                                this.Income3 = this.Income3 + currentAmountValue;
                            }
                            if (sourceId == incomeId4)
                            {
                                this.AIncome4 = Convert.ToDecimal(this.AIncome4) + amountValue;
                                this.Income4 = this.Income4 + currentAmountValue;
                            }
                            if (sourceId == incomeId5)
                            {
                                this.AIncome5 = Convert.ToDecimal(this.AIncome5) + amountValue;
                                this.Income5 = this.Income5 + currentAmountValue;
                            }
                            if (sourceId == incomeID6)
                            {
                                this.AIncome6 = Convert.ToDecimal(this.AIncome6) + amountValue;
                                this.Income6 = this.Income6 + currentAmountValue;
                            }
                        }



                    }
                    else
                    {

                        if (decimal.TryParse(items[1], out amountValue))
                        {
                            // Calculation for other fixed also
                            if (valueEach.ToLower().Contains("sst for the year"))
                            {
                                this.ATax += amountValue;
                            }
                            else if (valueEach.ToLower().Contains("tds for the year"))
                            {
                                this.ATax += amountValue;
                            }
                            else if (valueEach.ToLower().Contains("sst this month"))
                            {
                                this.Tax += amountValue;
                            }
                            else if (valueEach.ToLower().Contains("tds this month"))
                            {
                                this.Tax += amountValue;
                            }

                        }
                    }
                }


            }


        }

        public void Process(int? incomeId1, int? incomeId2, int? incomeId3, int? incomeId4, int? incomeId5, int? incomeID6)
        {

            // process for annual 
            SetAnnual(incomeId1, incomeId2, incomeId3, incomeId4, incomeId5, incomeID6);



            // process for monthly
        }

    }
    public partial class CITChangeRequest
    {
        public string EmployeeName { get; set; }
        public string Type { get; set; }

    }
    public partial class BonusBO
    {
        public int BonusId { get; set; }

        public int YearID { get; set; }

        public string Year { get; set; }

        public string PaidPeriod { get; set; }

        public string DistributionDate { get; set; }

        public System.DateTime DistributionDateEng { get; set; }

        public decimal BonusAmount { get; set; }
        public decimal? DistributedAmount { get; set; }
        public int NoOfEmployees { get; set; }

        public string Name { get; set; }

        public decimal? TotalAnnualSalary { get; set; }
        public decimal? BonusPercent { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
    public partial class OvertimeBO
    {
        public int OvertimeID { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Name { get; set; }

        public string Period { get; set; }
        public int? Status { get; set; }
    }
    public partial class GetTransferListResult
    {
        public string FromDateEngFormatted { get; set; }
        public string AppointmentEngDateText { get; set; }
    }
    public partial class GetEmployeeListForMassIncrementResult
    {
        public decimal NewIncome { get; set; }
        public decimal IncreasedBy { get; set; }
    }
    public partial class GetAttendanceListForOvernightMarkingNewResult
    {
        public string InTimeText { get; set; }
        public string OutTimeText { get; set; }
        public string WorkHourText { get; set; }
    }
    public partial class AttendanceCheckInCheckOutView
    {
        public string Time { get; set; }
    }
    public partial class GetAttendanceListForOvernightShiftMarkingResult
    {
        public string DateText
        {
            get
            {
                return this.Date.Value.ToString("yyyy-MMM-dd");
            }
            set { }
        }

        public string DateTimeText
        {
            get
            {
                return this.DateTime.ToString("yyyy-MMM-dd hh:mm tt");
            }
            set { }
        }

    }
    public partial class GetAttendanceEmployeeReportSummaryResult
    {
        public string QueryString { get; set; }
        public string MissingOrExtraText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.MissingOrExtra == null ? 0 : this.MissingOrExtra.Value);

                bool isNegative = this.MissingOrExtra < 0;

                return
                    (isNegative ? "-" : "") +
                    Math.Abs((t.Days * 24) + t.Hours) + ":" + Math.Abs(t.Minutes).ToString("00");
            }
        }
        public string AvgMinText
        {
            set { }
            get
            {
                double min = (double)this.AvgMin;
                TimeSpan t = TimeSpan.FromMinutes(min);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }
        public string StandardMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.StandardMin == null ? 0 : this.StandardMin.Value);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
                //return t.Hours.ToString("00") + ":" + t.Minutes.ToString("00");
            }
        }
        public string LateMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.LateMin == null ? 0 : this.LateMin.Value);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
                //return t.Hours.ToString("00") + ":" + t.Minutes.ToString("00");
            }
        }
        public string WorkedMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.WorkedMin == null ? 0 : this.WorkedMin.Value);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }
        public string OTMinuteText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.OTMinute == null ? 0 : this.OTMinute.Value);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }
    }

    public partial class GetAutoWeeklyLateMailListToEmployeeResult
    {
        public string ClockInText
        {
            set { }
            get
            {
                if (this.ClockIn == null)
                    return "";
                return (this.ClockIn.Value.Days * 24) + this.ClockIn.Value.Hours + ":" + this.ClockIn.Value.Minutes.ToString("00");
            }
        }
        public string ClockOutText
        {
            set { }
            get
            {
                if (this.ClockOut == null)
                    return "";
                return (this.ClockOut.Value.Days * 24) + this.ClockOut.Value.Hours + ":" + this.ClockOut.Value.Minutes.ToString("00");
            }
        }
    }
    public partial class GetAttendanceEmployeeReportDetailsResult
    {
        public string QueryString { get; set; }
        public string NepDate { get; set; }
        public int? LeaveTypeId { get; set; }
        public string MissingOrExtraText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.MissingOrExtra == null ? 0 : this.MissingOrExtra.Value);

                bool isNegative = this.MissingOrExtra < 0;

                return
                    (isNegative ? "-" : "") +
                    Math.Abs((t.Days * 24) + t.Hours) + ":" + Math.Abs(t.Minutes).ToString("00");
            }
        }

        public string StandardMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.StandardMin == null ? 0 : this.StandardMin.Value);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
                //return t.Hours.ToString("00") + ":" + t.Minutes.ToString("00");
            }
        }

        public string LateMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.LateMin == null ? 0 : this.LateMin.Value);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
                //return t.Hours.ToString("00") + ":" + t.Minutes.ToString("00");
            }
        }

       

        public string EngDate
        {
            set { }
            get
            {
                if (this.Date == DateTime.MinValue)
                    return "";

                return this.Date.ToString("dd-MMM-yyyy");
            }
        }
        public string WorkedMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.WorkedMin);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }

        public string IsLate
        {
            get
            {
                return IslateIn ? "Yes" : "";
            }
            set { }
        }
        public string ClockInText
        {
            set { }
            get
            {
                if (this.ClockIn == null)
                    return "";
                return (this.ClockIn.Value.Days * 24) + this.ClockIn.Value.Hours + ":" + this.ClockIn.Value.Minutes.ToString("00");
            }
        }
        public string ClockOutText
        {
            set { }
            get
            {
                if (this.ClockOut == null)
                    return "";
                return (this.ClockOut.Value.Days * 24) + this.ClockOut.Value.Hours + ":" + this.ClockOut.Value.Minutes.ToString("00");
            }
        }
        public string OTMinuteText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.OTMinute == null ? 0 : this.OTMinute.Value);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }
    }
    public partial class GetHolidayAdditionalPayListResult
    {

        public string DayValueTextAdded { get; set; }
        public string Date
        {
            set { }
            get
            {
                if (this.DateEng == DateTime.MinValue)
                    return "";

                return this.DateEng.ToString("dd-MMM-yyyy");
            }
        }
        public string WorkedMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.WorkedMin);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }



        public string ClockInText
        {
            set { }
            get
            {
                if (this.ClockIn == null)
                    return "";
                return (this.ClockIn.Value.Days * 24) + this.ClockIn.Value.Hours + ":" + this.ClockIn.Value.Minutes.ToString("00");
            }
        }
        public string ClockOutText
        {
            set { }
            get
            {
                if (this.ClockOut == null)
                    return "";
                return (this.ClockOut.Value.Days * 24) + this.ClockOut.Value.Hours + ":" + this.ClockOut.Value.Minutes.ToString("00");
            }
        }

    }
    public partial class GetListForCumulativeLateProcessingResult
    {
        public string RecordID
        {
            set;
            get;
        }
        public string NepDate { get; set; }

        public string EngDate
        {
            set { }
            get
            {
                if (this.Date == DateTime.MinValue)
                    return "";

                return this.Date.ToString("dd-MMM-yyyy");
            }
        }
        public string WorkedMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.WorkedMin);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }

        public string CumLateMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.CumLateMin == null ? 0 : this.CumLateMin);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }
        public string LateMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.LateMin == null ? 0 : this.LateMin.Value);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }
        public string ClockInText
        {
            set { }
            get
            {
                if (this.ClockIn == null)
                    return "";
                return (this.ClockIn.Value.Days * 24) + this.ClockIn.Value.Hours + ":" + this.ClockIn.Value.Minutes.ToString("00");
            }
        }
        public string ClockOutText
        {
            set { }
            get
            {
                if (this.ClockOut == null)
                    return "";
                return (this.ClockOut.Value.Days * 24) + this.ClockOut.Value.Hours + ":" + this.ClockOut.Value.Minutes.ToString("00");
            }
        }
        public string OfficeInText
        {
            set { }
            get
            {
                if (this.OfficeIn == null)
                    return "";
                return (this.OfficeIn.Value.Days * 24) + this.OfficeIn.Value.Hours + ":" + this.OfficeIn.Value.Minutes.ToString("00");
            }
        }
        public string OfficeOutText
        {
            set { }
            get
            {
                if (this.OfficeOut == null)
                    return "";
                return (this.OfficeOut.Value.Days * 24) + this.OfficeOut.Value.Hours + ":" + this.OfficeOut.Value.Minutes.ToString("00");
            }
        }
    }
    public partial class GetListForLateProcessingResult
    {
        public string RecordID
        {
            set;
            get;
        }
        public string NepDate { get; set; }

        public string EngDate
        {
            set { }
            get
            {
                if (this.Date == DateTime.MinValue)
                    return "";

                return this.Date.ToString("dd-MMM-yyyy");
            }
        }
        public string WorkedMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.WorkedMin);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }

        public string LateMinText
        {
            set { }
            get
            {
                TimeSpan t = TimeSpan.FromMinutes(this.LateMin == null ? 0 : this.LateMin.Value);

                return (t.Days * 24) + t.Hours + ":" + t.Minutes.ToString("00");
            }
        }
        public string ClockInText
        {
            set { }
            get
            {
                if (this.ClockIn == null)
                    return "";
                return (this.ClockIn.Value.Days * 24) + this.ClockIn.Value.Hours + ":" + this.ClockIn.Value.Minutes.ToString("00");
            }
        }
        public string ClockOutText
        {
            set { }
            get
            {
                if (this.ClockOut == null)
                    return "";
                return (this.ClockOut.Value.Days * 24) + this.ClockOut.Value.Hours + ":" + this.ClockOut.Value.Minutes.ToString("00");
            }
        }
        public string OfficeInText
        {
            set { }
            get
            {
                if (this.OfficeIn == null)
                    return "";
                return (this.OfficeIn.Value.Days * 24) + this.OfficeIn.Value.Hours + ":" + this.OfficeIn.Value.Minutes.ToString("00");
            }
        }
        public string OfficeOutText
        {
            set { }
            get
            {
                if (this.OfficeOut == null)
                    return "";
                return (this.OfficeOut.Value.Days * 24) + this.OfficeOut.Value.Hours + ":" + this.OfficeOut.Value.Minutes.ToString("00");
            }
        }
    }
    public partial class NIBLVoucherHead
    {
        public string TypeSourceID
        {
            get
            {
                return this.Type + ":" + this.SourceId;
            }
        }
    }
    public partial class NIBLVoucherGroup
    {
        public string Status { get; set; }
        public string Level { get; set; }
        public string IncomeDeduction { get; set; }
        public string VoucherType { get; set; }
    }
    public partial class Variance_GetPayVarianceReportEmployeeListDetailsResult
    {
        public string Reason1 { get; set; }
        public string Reason2 { get; set; }
    }
    public partial class GetEmployeeLeaveBalanceResult
    {
        public int EmployeeId { get; set; }
    }
    public partial class GetServiceHistoryListResult
    {
        public int SN { get; set; }
    }
    public partial class GetEmployeeLeaveBalanceResult
    {
        public bool IsParentGroupLeave { get; set; }
        public double CurrentBalance { get; set; }
    }
    public partial class GetEveningCounterRequestForManagerResult
    {
        public string StartDateNep { get; set; }
        public string EndDateNep { get; set; }
    }
    public partial class GetEveningCounterRequestForAdminResult
    {
        public string StartDateNep { get; set; }
        public string EndDateNep { get; set; }
        public string StartDateEngFormatted { get; set; }
        public string EndDateEngFormatted { get; set; }
    }
    public partial class LeaveApprovalPreDefineEmployee
    {
        public string ApplyToBranchHeadAlsoText { get; set; }
    }
    public partial class GetServiceYearReportResult
    {
        public string ServicePeriod { get; set; }
    }
    public partial class GetEmployeeListForProjectEmpResult
    {
        public string ProjectNameList { get; set; }
    }
    public partial class GetTimeSheetListForApprovalResult
    {
        public string StatusText { get; set; }
        public string WeekText { get; set; }
        public string Month { get; set; }
    }

    public partial class GetOvertimeForApprovalResult
    {
        public string StatusText { get; set; }
        public string OTHours { get; set; }
        public string ApprovedHrsFormatted { get; set; }
        public string ApprovedMinFormatted { get; set; }
        public string NepDate { get; set; }

    }

    public partial class GetMyOvertimeListResult
    {
        public string StatusText { get; set; }
        public string OTHours { get; set; }
        public string ApprovedHrsFormatted { get; set; }
        public string ApprovedMinFormatted { get; set; }


    }
    public partial class GetTimeSheetListResult
    {
        public string StatusText { get; set; }
        public string WeekText { get; set; }
        public string Month { get; set; }
    }

    public partial class GetTimeSheetListForEmployeeResult
    {
        public string StatusText { get; set; }
        public string WeekText { get; set; }
        public string StartDateText { get; set; }
        public string Month { get; set; }
    }

    public partial class AttendanceInOutTime
    {
        public string ShiftName { get; set; }
    }
    public partial class PPay
    {
        public string EmployeeName { get; set; }
    }
    public partial class EFundDetail
    {
        public string EmployeeName { get; set; }
    }
    public partial class EveningCounterType
    {
        public string Income { get; set; }
        public string IsProportionate { get; set; }
    }
    public partial class MannualLeaveAccure
    {

        public string Type { get; set; }
        public string DateDisplay { get; set; }
        public double Balance { get; set; }
    }
    public partial class OvertimeType
    {
        public string Income { get; set; }
    }
    public partial class GetEmployeeListForProvisionalCITResult
    {
        public decimal Month4 { get; set; }
        public decimal Month5 { get; set; }
        public decimal Month6 { get; set; }
        public decimal Month7 { get; set; }

        public decimal Month8 { get; set; }
        public decimal Month9 { get; set; }
        public decimal Month10 { get; set; }
        public decimal Month11 { get; set; }

        public decimal Month12 { get; set; }
        public decimal Month1 { get; set; }
        public decimal Month2 { get; set; }
        public decimal Month3 { get; set; }

        public bool Month4Enabled { get; set; }
        public bool Month5Enabled { get; set; }
        public bool Month6Enabled { get; set; }
        public bool Month7Enabled { get; set; }

        public bool Month8Enabled { get; set; }
        public bool Month9Enabled { get; set; }
        public bool Month10Enabled { get; set; }
        public bool Month11Enabled { get; set; }

        public bool Month12Enabled { get; set; }
        public bool Month1Enabled { get; set; }
        public bool Month2Enabled { get; set; }
        public bool Month3Enabled { get; set; }

        public void ProcessRow()
        {
            string[] values = this.CITTable.Split(new char[] { ';' });


            this.Month4Enabled = true;
            this.Month5Enabled = true;
            this.Month6Enabled = true;
            this.Month7Enabled = true;
            this.Month8Enabled = true;
            this.Month9Enabled = true;

            this.Month10Enabled = true;
            this.Month11Enabled = true;
            this.Month12Enabled = true;
            this.Month1Enabled = true;
            this.Month2Enabled = true;
            this.Month3Enabled = true;




            for (int i = values.Length - 1; i >= 0; i--)
            {
                bool isEnabled = true;

                string value = values[i];

                string[] data = value.Split(new char[] { ':' });

                if (data.Length <= 0 || value == "")
                    continue;

                int month = Convert.ToInt32(data[0]);

                if (Convert.ToBoolean(data[1]) == false)
                    isEnabled = Convert.ToBoolean(data[1]);

                decimal amount = Convert.ToDecimal(data[2]);

                switch (month)
                {
                    case 4: Month4 = amount; Month4Enabled = isEnabled; break;
                    case 5: Month5 = amount; Month5Enabled = isEnabled; break;
                    case 6: Month6 = amount; Month6Enabled = isEnabled; break;
                    case 7: Month7 = amount; Month7Enabled = isEnabled; break;

                    case 8: Month8 = amount; Month8Enabled = isEnabled; break;
                    case 9: Month9 = amount; Month9Enabled = isEnabled; break;
                    case 10: Month10 = amount; Month10Enabled = isEnabled; break;
                    case 11: Month11 = amount; Month11Enabled = isEnabled; break;

                    case 12: Month12 = amount; Month12Enabled = isEnabled; break;
                    case 1: Month1 = amount; Month1Enabled = isEnabled; break;
                    case 2: Month2 = amount; Month2Enabled = isEnabled; break;
                    case 3: Month3 = amount; Month3Enabled = isEnabled; break;

                }

            }


        }

    }
    public partial class Branch
    {
        public string Head { get; set; }
        public string RegionHead { get; set; }
    }
    public partial class Activiti
    {
        public string Statuss { get; set; }
    }
    public partial class GetTADARequestForAdminResult
    {
        public string StatusText { get; set; }
        public string MonthText { get; set; }
    }
    public partial class IIndividualInsurance
    {
        public string Name { get; set; }
        public string InsuranceType { get; set; }
        public string InsuranceCompany { get; set; }
        public string PaidBy { get; set; }
    }

    public partial class GetTADARequestForManagerResult
    {
        public string StatusText { get; set; }
        public string MonthText { get; set; }
    }
    public partial class DHTADAClaimLine
    {
        public bool IsTotal { get; set; }
    }
    public partial class DHTADAClaim
    {
        public string MonthText { get; set; }
        public string Name { get; set; }
        public string StatusText { get; set; }
    }
    public partial class OvertimeTypeLevel
    {
        public string LevelName { get; set; }
    }
    public partial class EveningCounterTypeLevel
    {
        public string LevelName { get; set; }
    }
    public partial class MailMerge
    {
        public string EmployeeName { get; set; }
    }
    public partial class Appraisal_GetDetailReportResult
    {
        public Dictionary<int, string> commentList = new Dictionary<int, string>();



        public void SetComment()
        {
            if ((commentList == null || commentList.Count == 0) && this.Comments != null)
            {
                commentList = new Dictionary<int, string>();

                string[] values = this.Comments.Split(new string[] { ";;;" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string value in values)
                {
                    string[] data = value.Split(new string[] { ":::" }, StringSplitOptions.RemoveEmptyEntries);
                    if (data.Length >= 2 && commentList.ContainsKey(int.Parse(data[0])) == false)
                    {
                        commentList.Add(int.Parse(data[0]), data[1]);
                    }
                }

            }
        }


        public string Step3Comment
        {
            get
            {
                if (commentList != null && commentList.ContainsKey(3))
                    return commentList[3];
                return "";
            }
            set { }
        }
        public string Step4Comment
        {
            get
            {
                if (commentList != null && commentList.ContainsKey(4))
                    return commentList[4];
                return "";
            }
            set { }
        }
        public string Step5Comment
        {
            get
            {
                if (commentList != null && commentList.ContainsKey(5))
                    return commentList[5];
                return "";
            }
            set { }
        }
        public string Step6Comment
        {
            get
            {
                if (commentList != null && commentList.ContainsKey(6))
                    return commentList[6];
                return "";
            }
            set { }
        }
        public string Step7Comment
        {
            get
            {
                if (commentList != null && commentList.ContainsKey(7))
                    return commentList[7];
                return "";
            }
            set { }
        }
        public string Step8Comment
        {
            get
            {
                if (commentList != null && commentList.ContainsKey(8))
                    return commentList[8];
                return "";
            }
            set { }
        }
        public string Step9Comment
        {
            get
            {
                if (commentList != null && commentList.ContainsKey(9))
                    return commentList[9];
                return "";
            }
            set { }
        }
        public string Step10Comment
        {
            get
            {
                if (commentList != null && commentList.ContainsKey(10))
                    return commentList[10];
                return "";
            }
            set { }
        }
        public string Step11Comment
        {
            get
            {
                if (commentList != null && commentList.ContainsKey(11))
                    return commentList[11];
                return "";
            }
            set { }
        }
        public string Step15Comment
        {
            get
            {
                if (commentList != null && commentList.ContainsKey(15))
                    return commentList[15];
                return "";
            }
            set { }
        }
    }
    public partial class MailMergeExcelTemp
    {
        public string Email { get; set; }
        public int SN { get; set; }
    }
    public partial class HTraining
    {
        public string DurationText
        {
            set { }
            get
            {
                if (this.Duration != null)
                {
                    return this.Duration.Value + " " + this.DurationTypeName;
                }
                return "";
            }
        }

        public int ContainsFile { get; set; }
    }
    public partial class AppraisalFormQuestionnaireCategory
    {
        public string Number { get; set; }
        public double EmployeePoints { get; set; }
        public double SupervisorPoints { get; set; }
    }
    public partial class AppraisalForm
    {
        public string RollOutTimes { get; set; }
        public int TotalReview { get; set; }
        public int TotalCompetency { get; set; }
        public int TotalQuestion { get; set; }
    }


    public partial class GetAppraisalListAdminSPResult
    {
        public string RollOutTimes { get; set; }
        public int TotalReview { get; set; }
        public int TotalCompetency { get; set; }
        public int TotalQuestion { get; set; }
    }

    public partial class GetRolloutEmployeesListResult
    {
        public DateTime? DueDate { get; set; }
        public string DaysRemaining { get; set; }

        public string SelfScoreText { get; set; }
        public string SupervisorScoreText { get; set; }

        public string EmployeeCompetencyGrade { get; set; }
        public string SupervisorCompetencyGrade { get; set; }
    }
    public partial class DynamicFormControl
    {
        public string ControlValues { get; set; }
        public int RowIndex { get; set; }
    }

    public partial class Candidate
    {
        public string CandidateName { get; set; }
    }

    public partial class CEducationalDetail
    {
        public string CountryName { get; set; }
    }

    public partial class RecruitmentOpening
    {
        public string Branch { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
    }
    public partial class AppraisalEmployeeForm
    {
        public string EmployeeName { get; set; }
        public string AppraisalFormName { get; set; }
        public DateTime? DueDate { get; set; }
        public string DaysRemaining { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }

    public partial class GetAppraisalListForReviewResult
    {
        public string PeriodName { get; set; }
        public string AppraisalFormName { get; set; }
        public string EmployeeName { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }

    public partial class GetYourAppraisalSPResult
    {
        public string PeriodName { get; set; }
        public string EmployeeName { get; set; }
        public string AppraisalFormName { get; set; }
        public DateTime? DueDate { get; set; }
        public string DaysRemaining { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }

    public partial class ApprovalFlow
    {
        public string AuthorityTypeName { get; set; }

        public string StepIDName { get; set; }
        public string StepStatusName { get; set; }
        //public string AuthorityTypeName { get; set; }

        public int? EmployeeId { get; set; }
        public string AdditionStep { get; set; }

        public bool IsDeletable { get; set; }

    }
    public partial class Charity
    {
        public string Name { get; set; }
    }
    public partial class MG_Opening
    {
        public string Name { get; set; }
    }
    public partial class MG_AllowanceSetting
    {
        public string Name { get; set; }
    }
    public partial class GetEmployee_ShiftsResult
    {
        public int? DefaultWorkShiftId { get; set; }
    }
    public partial class EWorkShiftHistory
    {
        public int? DefaultWorkShiftId { get; set; }
    }
    public partial class GetLeaveRequestListForHRResult
    {
        public string CreatedDate { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

    }
    public partial class GradeAdditionlBasic
    {
        public string Level { get; set; }
        public string Income { get; set; }
    }
    public partial class LLeaveType
    {
        public double EmployeeBalanceRemaining { get; set; }
        public double TakenBalanceInThisRequest { get; set; }
        public string EncashmentIncomes { get; set; }
    }
    public partial class LeaveAdjustment
    {
        public string Name { get; set; }
        public double RetirementEncash { get; set; }
        public string EncashmentIncomes { get; set; }
    }
    public partial class LevelGradeChangeDetail
    {
        public string OldLevel { get; set; }
        public string NewLevel { get; set; }
        public string ChangeType { get; set; }
        public string StatusText { get; set; }
        public bool IsUsedInSalary { get; set; }
        public string Name { get; set; }
        public bool IsBackdatedPromotion { get; set; }
    }

    //
    public partial class GetLevelGradeChangeDetailsSPResult
    {

        public string ChangeType { get; set; }
        public string StatusText { get; set; }

        public string Arrear
        {
            get
            {
                if (this.IsBackdatedPromotion == 1)
                    return "Yes";

                return "";
            }
            set { }
        }

    }


    public partial class GetContractExpiringEmpListResult
    {
        public string DueDays { get; set; }
        public string StatusName { get; set; }
        public string ServicePeriod
        {
            set { }
            get
            {
                int years, months, days, hours;
                int minutes, seconds, milliseconds;

                ECurrentStatus firstStatus = BaseBiz.PayrollDataContext.ECurrentStatus
                    .OrderBy(x => x.FromDateEng).FirstOrDefault(x => x.EmployeeId == this.EmployeeId);
                if (firstStatus != null)
                {
                    string WorkingFor = string.Empty;
                    NewEntityHelper.GetElapsedTime(firstStatus.FromDateEng, Utils.Helper.Util.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                    string text = "";
                    if (years != 0)
                        text = " " + years + " years";
                    if (months != 0)
                        text += " " + months + " months";
                    if (days != 0)
                        text += " " + days + " days";

                    WorkingFor += text;

                    return WorkingFor;
                    //WorkingFor += ", Since " +
                    //    (HttpContext.Current.SessionManager.CurrentCompany.IsEnglishDate ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");

                }
                else return "";

            }
        }
    }

    public partial class GetContractExpiringEmpListDetailsResult
    {
        public string DueDays { get; set; }
        public string StatusName { get; set; }
        public string ServicePeriod
        {
            set { }
            get
            {
                int years, months, days, hours;
                int minutes, seconds, milliseconds;

                ECurrentStatus firstStatus = BaseBiz.PayrollDataContext.ECurrentStatus
                    .OrderBy(x => x.FromDateEng).FirstOrDefault(x => x.EmployeeId == this.EmployeeId);
                if (firstStatus != null)
                {
                    string WorkingFor = string.Empty;
                    NewEntityHelper.GetElapsedTime(firstStatus.FromDateEng, Utils.Helper.Util.GetCurrentDateAndTime(), out years, out months, out days, out hours, out minutes, out seconds, out milliseconds);
                    string text = "";
                    if (years != 0)
                        text = " " + years + " years";
                    if (months != 0)
                        text += " " + months + " months";
                    if (days != 0)
                        text += " " + days + " days";

                    WorkingFor += text;

                    return WorkingFor;
                    //WorkingFor += ", Since " +
                    //    (HttpContext.Current.SessionManager.CurrentCompany.IsEnglishDate ? firstStatus.FromDateEng.ToShortDateString() : firstStatus.FromDate + " (" + firstStatus.FromDateEng.ToShortDateString() + ")");

                }
                else return "";

            }
        }
    }
    public partial class BankBranch
    {
        public string BankName { get; set; }
    }
    public partial class GetEmployeesCatelogueResult
    {
        public bool IsHeader { get; set; }
        public bool IsFooter { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }
        public string Since { get; set; }
    }
    public partial class GetLoanAdjustmentInstallmentsResult
    {
        public decimal EMI { get; set; }
        public decimal ClosingBalance { get; set; }
        public string MonthName
        {
            get
            {
                if (this.Month < 0 || this.Month > 12)
                    return "";
                return Utils.Calendar.DateHelper.GetMonthName(this.Month, false);
            }
            set { }
        }
    }
    public partial class GetMonetoryChangeLogsForSignOffResult
    {
        public string ActionValue { get; set; }
        public string TypeValue { get; set; }
    }

    public partial class DesignationHistory
    {
        public string Designation { get; set; }
    }
    public partial class BranchDepartmentHistory
    {
        public string Branch { get; set; }
        public string Department { get; set; }

        public string EmployeeName { get; set; }
        public string FromBranch { get; set; }
        public string ToBranch { get; set; }
        public string FromDepartment { get; set; }
        public string ToDepartment { get; set; }
        public string Type { get; set; }

    }
    public partial class LeaveDetail { public bool IsHoliday { get; set; } public bool IsPrevMonth { get; set; } public bool IsWeeklyHoliday { get; set; } }
    public partial class AttedenceDetail
    {
        public bool IsHoliday { get; set; }
        public bool IsPrevMonth { get; set; }

        // if multiple leave case or first leave already approved, now second leave is being approved
        public bool HasMulitpleLeave { get; set; }
        public int OtherLeaveTypeId { get; set; }
    }

    public partial class PEmployeeIncome
    {
        public string Name { get; set; }
        public string IDCardNo { get; set; }
    }
    public partial class MedicalTaxCreditDetail
    {
        public int EmployeeId { get; set; }
    }
    public partial class TimesheetProject
    {
        public bool IsPreviousMonth { get; set; }
    }
    public partial class TimesheetLeave
    {
        public bool IsPreviousMonth { get; set; }
    }
    public partial class GetEmployeeMasterD2Result
    {

        public int SpacePosition(int startLastIndexPos)
        {
            if (startLastIndexPos == -1)
                startLastIndexPos = this.Name.Length - 1;

            for (int i = startLastIndexPos; i >= 0; i--)
            {
                if (Char.IsWhiteSpace(this.Name[i]))
                {
                    return i;
                }
            }
            return -1;
        }

        public string FirstName
        {
            get
            {
                int lastSpaceIndex = SpacePosition(-1);// this.Name.LastIndexOf(' ');
                if (lastSpaceIndex != -1)
                {
                    string firstName = this.Name.Substring(0, lastSpaceIndex);
                    string lastName = this.Name.Substring(lastSpaceIndex).Trim();

                    int lastNameNumber;
                    //for cases like Ram Kumar 2
                    if (int.TryParse(lastName, out lastNameNumber) || lastName.ToLower() == "a" || lastName.ToLower() == "b" || lastName.ToLower() == "c")
                    {
                        lastSpaceIndex = SpacePosition(lastSpaceIndex - 1); //this.Name.LastIndexOf(' ', lastSpaceIndex-1);
                        if (lastSpaceIndex != -1)
                        {
                            return this.Name.Substring(0, lastSpaceIndex);
                        }
                    }
                    return firstName;

                }
                return this.Name;
            }
            set { }
        }

        public string LastName
        {
            get
            {
                int lastSpaceIndex = SpacePosition(-1); //this.Name.LastIndexOf(' ');
                if (lastSpaceIndex != -1)
                {
                    string firstName = this.Name.Substring(0, lastSpaceIndex);
                    string lastName = this.Name.Substring(lastSpaceIndex).Trim();

                    int lastNameNumber;
                    //for cases like Ram Kumar 2
                    if (int.TryParse(lastName, out lastNameNumber) || lastName.ToLower() == "a" || lastName.ToLower() == "b" || lastName.ToLower() == "c")
                    {
                        lastSpaceIndex = SpacePosition(lastSpaceIndex - 1); //this.Name.LastIndexOf(' ', lastSpaceIndex - 1);
                        if (lastSpaceIndex != -1)
                        {
                            return this.Name.Substring(lastSpaceIndex);
                        }
                    }
                    return lastName;
                }
                return "";
            }
            set { }
        }

        public DateTime? DOB
        {
            get
            {
                CustomDate date = null;

                if (this.IsEnglishDOB == null)
                {
                    if (!string.IsNullOrEmpty(this.DateOfBirth))
                    {
                        date = CustomDate.GetCustomDateFromString(this.DateOfBirth, false);
                        return DateTime.Parse(date.Month + "/" + date.Day + "/" + date.Year);
                    }
                    return null;
                }

                if (this.IsEnglishDOB.Value)
                {
                    date = CustomDate.GetCustomDateFromString(this.DateOfBirth, true);
                    return DateTime.Parse(date.Month + "/" + date.Day + "/" + date.Year);
                }
                else
                {
                    date = CustomDate.GetCustomDateFromString(this.DateOfBirth, false);
                    return DateTime.Parse(date.Month + "/" + date.Day + "/" + date.Year);
                }
            }
            set { }
        }
    }


    public partial class EDesignation
    {
        public string CodeAndName
        {
            get
            {
                if (string.IsNullOrEmpty(this.Code))
                    return this.Name;
                return string.Format("{0} : {1}", this.Code, this.Name);
            }
        }
        public string LevelAndDesignation
        {
            get
            {
                BLevel level = DAL.BaseBiz.PayrollDataContext.BLevels.FirstOrDefault(x => x.LevelId == this.LevelId);
                return string.Format("{0} - {1}", level.Name, this.Name);
            }
        }
    }
    public partial class GetHolidaysForAttendenceResult
    {
        public string EngDateLongForDashboard { get; set; }
        public string NepDateLongForDashboard { get; set; }
        public string DaysLeftForDashboard { get; set; }
        public string Note1ForDashboard { get; set; }
        public int MaxMonthDays { get; set; }
        public bool IsWeelyHalfDayHoliday { get; set; }
        public string DateEngText
        {
            get
            {
                if (this.DateEng.HasValue)
                    return this.DateEng.Value.ToShortDateString();
                return "";
            }
            set { }
        }

        private List<int> _branchIDList = null;
        public List<int> BranchList
        {
            get
            {
                if (_branchIDList == null)
                {
                    _branchIDList = new List<int>();
                    if (!string.IsNullOrEmpty(this.BranchIDList))
                    {
                        _branchIDList = new List<int>(this.BranchIDList.Split(',').Select(int.Parse));
                    }
                }
                return _branchIDList;
            }
            set { }
        }
    }

    public partial class GetEmployeeLeaveBalanceResult
    {
        public string FreqOfAccrual { get; set; }
    }
    public partial class PEmployeeIncome
    {
        public decimal DeemedAmount
        {
            set { }
            get
            {
                return Convert.ToDecimal(this.MarketValue) - Convert.ToDecimal(this.Amount);
            }
        }
        public decimal? ActualAmount
        {
            get
            {
                if (this.PIncome.IsGrade.HasValue == true && this.PIncome.IsGrade == true)
                {
                    PositionGradeStepAmount entityAmount = BaseBiz.PayrollDataContext.PositionGradeStepAmounts.
                        SingleOrDefault(a => a.PositionId == this.PositionId && a.GradeId == this.GradeId && a.StepId == this.GradeId);

                    if (entityAmount == null)
                        return 0;

                    return entityAmount.Amount;
                }
                return this.Amount;
            }
            set { }
        }
    }

    public partial class FinancialDate
    {
        public string Name { get; set; }
        public void SetName(bool isEnglish)
        {
            CustomDate d1 = CustomDate.GetCustomDateFromString(StartingDate, isEnglish);
            CustomDate d2 = CustomDate.GetCustomDateFromString(EndingDate, isEnglish);


            Name = d1.Year + "/" + d2.Year;

        }
    }

    public partial class UUser
    {
        public string EncryptedUserName
        {
            set { }
            get
            {
                return this.UserName;

                //return HttpContext.Current.Server.UrlEncode(AsymmCrypto.Encrypt(this.UserName));

                //return AsymmCrypto.Encrypt(this.UserName);

            }
        }
        public string NewUserName { get; set; }
    }






    public partial class Department
    {
        public string DepartmentAndBrachName
        {
            set { }
            get
            {
                return this.Name;
                //return this.Name + " - " + this.Branch.Name;
                //return this.Branch.Name + " - " + this.Name;
            }
        }
        public string LocationBranch { get; set; }
    }

    public partial class SubDepartment
    {
        public string HierarchyName
        {
            set { }
            get
            {
                return this.Name;
                //return this.Name + " - " + this.Branch.Name;
                //return this.Department.Branch.Name + " - " + this.Department.Name + " - " + this.Name;
            }
        }
    }




    public partial class AppraisalFormCompetency
    {

        //public string Name
        //{
        //    set { }
        //    get
        //    {

        //        return BaseBiz.PayrollDataContext.AppraisalCompetencies.SingleOrDefault(x => x.CompetencyID == this.CompetencyRef_ID).Name;
        //    }
        //}

        //public string CompetencyID
        //{
        //    set { }
        //    get
        //    {

        //        return this.CompetencyRef_ID.ToString();
        //    }
        //}


    }


    public partial class AppraisalCompetency
    {
        public string Weight { get; set; }



    }

    public partial class DynamicFormControl
    {
        public string ControlTypeName { get; set; }
        public string IsRequiredOrNot { get; set; }
    }

    public partial class DynamicFormPublish
    {
        public string FormName { get; set; }
        public string BranchName { get; set; }
        public string DepartmentName { get; set; }
        public string LevelName { get; set; }
        public string EmployeeName { get; set; }
    }
    public class NewEntityHelper
    {

        // Return the number of years, months, days, hours, minutes, seconds,
        // and milliseconds you need to add to from_date to get to_date.
        public static void GetElapsedTime(DateTime from_date, DateTime to_date,
            out int years, out int months, out int days, out int hours,
            out int minutes, out int seconds, out int milliseconds)
        {
            // If from_date > to_date, switch them around.
            if (from_date > to_date)
            {
                GetElapsedTime(to_date, from_date,
                    out years, out months, out days, out hours,
                    out minutes, out seconds, out milliseconds);
                years = -years;
                months = -months;
                days = -days;
                hours = -hours;
                minutes = -minutes;
                seconds = -seconds;
                milliseconds = -milliseconds;
            }
            else
            {
                // Handle the years.
                years = to_date.Year - from_date.Year;

                // See if we went too far.
                DateTime test_date = from_date.AddMonths(12 * years);
                if (test_date > to_date)
                {
                    years--;
                    test_date = from_date.AddMonths(12 * years);
                }

                // Add months until we go too far.
                months = 0;
                while (test_date <= to_date)
                {
                    months++;
                    test_date = from_date.AddMonths(12 * years + months);
                }
                months--;

                // Subtract to see how many more days,
                // hours, minutes, etc. we need.
                from_date = from_date.AddMonths(12 * years + months);
                TimeSpan remainder = to_date - from_date;
                days = remainder.Days;
                hours = remainder.Hours;
                minutes = remainder.Minutes;
                seconds = remainder.Seconds;
                milliseconds = remainder.Milliseconds;
            }

            if (days < 0)
                days = 0;
        }
    }

    public partial class HFamily
    {
        public int IsEditable { get; set; }
        public string Occupation { get; set; }
    }

    public partial class HEducation
    {
        public int IsEditable { get; set; }
        public int ContainsFile { get; set; }
    }

    public partial class HTraining
    {
        public int IsEditable { get; set; }
    }

    public partial class HSeminar
    {
        public int IsEditable { get; set; }
    }

    public partial class HPublication
    {
        public int IsEditable { get; set; }
    }

    public partial class HPreviousEmployment
    {
        public int IsEditable { get; set; }
        public string Category { get; set; }
    }

    public partial class HHumanResource
    {
        public string Name { get; set; }
        public string imageUrl { get; set; }
    }

    public partial class ECurrentStatus
    {
        public string StatusName { get; set; }
    }

    public partial class PEmployeeDeduction
    {
        public string EmployeeName { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string INo { get; set; }
        public string TakenDate { get; set; }
        public bool GenerateUniqueIDForFreshLoan { get; set; }
    }
    public partial class GetTimeSheetListResult
    {
        public string TimeSheetIDEmployeeID
        {
            set { }
            get
            {
                return this.TimesheetId.ToString() + "," + EmployeeId;
            }
        }
    }

    public partial class GetServiceYearReportResult
    {
        public string StatusName
        {
            set { }
            get
            {
                return DAL.JobStatus.GetStatusForDisplay(this.Status);
            }
        }
    }

    public partial class GetRetiredEmployeeListResult
    {
        public string JoinDateText
        {
            set { }
            get
            {
                return (this.JoinDateEng == null ? "" : this.JoinDateEng.Value.ToString("yyyy-MM-dd"));
            }
        }
        public string ConfirmationDateText
        {
            set { }
            get
            {
                return (this.ConfirmationDateEng == null ? "" : this.ConfirmationDateEng.Value.ToString("yyyy-MM-dd"));
            }
        }
        public string RetirementDateText
        {
            set { }
            get
            {
                return (this.RetirementDateEng == null ? "" : this.RetirementDateEng.Value.ToString("yyyy-MM-dd"));
            }
        }
        public string StopPayDateText
        {
            set { }
            get
            {
                return (this.StopPayDate == null ? "" : this.StopPayDate.Value.ToString("yyyy-MM-dd"));
            }
        }
    }

    public partial class AttendanceReportForTimeRequestResult
    {
        public string DeviceInString { get; set; }
        public string DeviceOutString { get; set; }
        public string RequestInString { get; set; }
        public string RequestOutString { get; set; }
        public string ActualDate { get; set; }
        public string EngDateFormatted { get; set; }
    }

    public partial class HolidayListImport
    {
        public string Description { get; set; }
        public int CompanyId { get; set; }
        public bool IsNationalHoliday { get; set; }
        public int AppliesTo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

}



