﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public partial class Report_HR_YearlyLeaveBalanceResult
    {

        public double L1LastYear { get; set; }
        public double L1CurrentYear { get; set; }
        public double L1Taken { get; set; }
        public double L1Adjustment { get; set; }
        public double L1Lapsed { get; set; }
        public double L1Encashed { get; set; }
        //public double L1Balance{get;set;}
        public double L2LastYear { get; set; }
        public double L2CurrentYear { get; set; }
        public double L2Taken { get; set; }
        public double L2Adjustment { get; set; }
        public double L2Lapsed { get; set; }
        public double L2Encashed { get; set; }
        //public double L2Balance{get;set;}
        public double L3LastYear { get; set; }
        public double L3CurrentYear { get; set; }
        public double L3Taken { get; set; }
        public double L3Adjustment { get; set; }
        public double L3Lapsed { get; set; }
        public double L3Encashed { get; set; }
        //public double L3Balance{get;set;}
        public double L4LastYear { get; set; }
        public double L4CurrentYear { get; set; }
        public double L4Taken { get; set; }
        public double L4Adjustment { get; set; }
        public double L4Lapsed { get; set; }
        public double L4Encashed { get; set; }
        //public double L4Balance{get;set;}
        public double L5LastYear { get; set; }
        public double L5CurrentYear { get; set; }
        public double L5Taken { get; set; }
        public double L5Adjustment { get; set; }
        public double L5Lapsed{ get; set; }
        public double L5Encashed { get; set; }
        //public double L5Balance{get;set;}
        public double L6LastYear { get; set; }
        public double L6CurrentYear { get; set; }
        public double L6Taken { get; set; }
        public double L6Adjustment { get; set; }
        public double L6Lapsed { get; set; }
        public double L6Encashed { get; set; }
        //public double L6Balance{get;set;}
        public double L7LastYear { get; set; }
        public double L7CurrentYear { get; set; }
        public double L7Taken { get; set; }
        public double L7Adjustment { get; set; }
        public double L7Lapsed { get; set; }
        public double L7Encashed { get; set; }
        //public double L7Balance{get;set;}
        public double L8LastYear { get; set; }
        public double L8CurrentYear { get; set; }
        public double L8Taken { get; set; }
        public double L8Adjustment { get; set; }
        public double L8Lapsed { get; set; }
        public double L8Encashed { get; set; }
        //public double L8Balance{get;set;}
        public double L9LastYear { get; set; }
        public double L9CurrentYear { get; set; }
        public double L9Taken { get; set; }
        public double L9Adjustment { get; set; }
        public double L9Lapsed { get; set; }
        public double L9Encashed { get; set; }
        //public double L9Balance{get;set;}
        public double L10LastYear { get; set; }
        public double L10CurrentYear { get; set; }
        public double L10Taken { get; set; }
        public double L10Adjustment { get; set; }
        public double L10Lapsed { get; set; }
        public double L10Encashed { get; set; }
        //public double L10Balance{get;set;}
        public double L11LastYear { get; set; }
        public double L11CurrentYear { get; set; }
        public double L11Taken { get; set; }
        public double L11Adjustment { get; set; }
        public double L11Lapsed { get; set; }
        public double L11Encashed { get; set; }
        // public double L11Balance{get;set;}

        public double L1Balance
        {
            get;
            set;
        }
        public double L2Balance
        {
            get;
            set;
        }
        public double L3Balance
        {
            get;
            set;
        }
        public double L4Balance
        {
            get;
            set;
        }
        public double L5Balance
        {
            get;
            set;
        }
        public double L6Balance
        {
            get;
            set;
        }
        public double L7Balance
        {
            get;
            set;
        }
        public double L8Balance
        {
            get;
            set;
        }
        public double L9Balance
        {
            get;
            set;
        }
        public double L10Balance
        {
            get;
            set;
        }
        public double L11Balance
        {
            get;
            set;
        }




        public void Process(List<LLeaveType> leaveList, List<Report_HR_YearlyLeaveBalanceDetailsResult> details)
        {
            List<Report_HR_YearlyLeaveBalanceDetailsResult> empDetails = details.Where(x => x.EmployeeId == this.EmployeeId)
                .ToList();

            bool isNibl = DAL.BaseBiz.PayrollDataContext.CompanySettings.Where(
                x => x.Value == "34" && x.Key == "Company").Any();

            int index = 0;

            foreach (LLeaveType item in leaveList)
            {
                index += 1;

                Report_HR_YearlyLeaveBalanceDetailsResult leave = empDetails.FirstOrDefault(x => x.LeaveTypeId == item.LeaveTypeId);
                if (leave == null)
                    continue;

                switch (index)
                {
                    case 1:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L1LastYear = Convert.ToDouble(leave.LastYear);
                        this.L1CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L1Taken = Convert.ToDouble(leave.Taken);
                        this.L1Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L1Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L1Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ((isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L1Balance = Convert.ToDouble(leave.Balance);
                        break;
                    case 2:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L2LastYear = Convert.ToDouble(leave.LastYear);
                        this.L2CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L2Taken = Convert.ToDouble(leave.Taken);
                        this.L2Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L2Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L2Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L2Balance = Convert.ToDouble(leave.Balance);
                        break;
                    case 3:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L3LastYear = Convert.ToDouble(leave.LastYear);
                        this.L3CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L3Taken = Convert.ToDouble(leave.Taken);
                        this.L3Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L3Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L3Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L3Balance = Convert.ToDouble(leave.Balance);
                        break;
                    case 4:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L4LastYear = Convert.ToDouble(leave.LastYear);
                        this.L4CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L4Taken = Convert.ToDouble(leave.Taken);
                        this.L4Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L4Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L4Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L4Balance = Convert.ToDouble(leave.Balance);
                        break;
                    case 5:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L5LastYear = Convert.ToDouble(leave.LastYear);
                        this.L5CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L5Taken = Convert.ToDouble(leave.Taken);
                        this.L5Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L5Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L5Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L5Balance = Convert.ToDouble(leave.Balance);
                        break;
                    case 6:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L6LastYear = Convert.ToDouble(leave.LastYear);
                        this.L6CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L6Taken = Convert.ToDouble(leave.Taken);
                        this.L6Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L6Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L6Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L6Balance = Convert.ToDouble(leave.Balance);
                        break;
                    case 7:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L7LastYear = Convert.ToDouble(leave.LastYear);
                        this.L7CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L7Taken = Convert.ToDouble(leave.Taken);
                        this.L7Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L7Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L7Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L7Balance = Convert.ToDouble(leave.Balance);
                        break;
                    case 8:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L8LastYear = Convert.ToDouble(leave.LastYear);
                        this.L8CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L8Taken = Convert.ToDouble(leave.Taken);
                       this.L8Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L8Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L8Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L8Balance = Convert.ToDouble(leave.Balance);
                        break;
                    case 9:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L9LastYear = Convert.ToDouble(leave.LastYear);
                        this.L9CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L9Taken = Convert.ToDouble(leave.Taken);
                        this.L9Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L9Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L9Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ((isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))// && leave.Balance != null)
                        //    this.L9Balance = (double)leave.Balance.Value;
                        //else
                            this.L9Balance = Convert.ToDouble(leave.Balance);
                        break;
                    case 10:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L10LastYear = Convert.ToDouble(leave.LastYear);
                        this.L10CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L10Taken = Convert.ToDouble(leave.Taken);
                        this.L10Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L10Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L10Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L10Balance = Convert.ToDouble(leave.Balance);
                        break;
                    case 11:
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L11LastYear = Convert.ToDouble(leave.LastYear);
                        this.L11CurrentYear = Convert.ToDouble(leave.CurrentYear);
                        this.L11Taken = Convert.ToDouble(leave.Taken);
                        this.L11Adjustment = Convert.ToDouble(leave.Adjustment);
                        this.L11Lapsed = Convert.ToDouble(leave.Lapsed);
                        this.L11Encashed = Convert.ToDouble(leave.Encashed);
                        // skip balance for Unpaid and NIBL compensatory leave
                        if ( (isNibl == false || item.FreqOfAccrual != LeaveAccrue.COMPENSATORY))
                            this.L11Balance = Convert.ToDouble(leave.Balance);
                        break;
                }

            }


        }
        public void ProcessLeaveTaken(List<LLeaveType> leaveList, List<Report_HR_PeriodicLeaveTakenDetailsResult> details)
        {
            List<Report_HR_PeriodicLeaveTakenDetailsResult> empDetails = details.Where(x => x.EmployeeId == this.EmployeeId)
                .ToList();

            bool isNibl = DAL.BaseBiz.PayrollDataContext.CompanySettings.Where(
                x => x.Value == "34" && x.Key == "Company").Any();

            int index = 0;

            foreach (LLeaveType item in leaveList)
            {
                index += 1;

                Report_HR_PeriodicLeaveTakenDetailsResult leave = empDetails.FirstOrDefault(x => x.LeaveTypeId == item.LeaveTypeId);
                if (leave == null)
                    continue;

                switch (index)
                {
                    case 1:
                       
                        this.L1Taken = Convert.ToDouble(leave.Taken);
                        break;
                    case 2:
                       
                        this.L2Taken = Convert.ToDouble(leave.Taken);
                        break;
                    case 3:
                        
                        this.L3Taken = Convert.ToDouble(leave.Taken);
                        break;
                    case 4:
                       
                        this.L4Taken = Convert.ToDouble(leave.Taken);
                        break;
                    case 5:
                        
                        this.L5Taken = Convert.ToDouble(leave.Taken);
                        break;
                    case 6:
                       
                        this.L6Taken = Convert.ToDouble(leave.Taken);
                        break;
                    case 7:
                        
                        this.L7Taken = Convert.ToDouble(leave.Taken);
                        break;
                    case 8:
                        
                        this.L8Taken = Convert.ToDouble(leave.Taken);
                        break;
                    case 9:
                       
                        this.L9Taken = Convert.ToDouble(leave.Taken);
                      
                        break;
                    case 10:
                      
                        this.L10Taken = Convert.ToDouble(leave.Taken);
                        
                        break;
                    case 11:
                     
                        this.L11Taken = Convert.ToDouble(leave.Taken);
                        
                        break;
                }

            }


        }



    }
}
