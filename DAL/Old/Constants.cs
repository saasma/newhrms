﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using DAL;
using System.Configuration;
using Utils;
using BLL;
namespace DAL
{
    public enum TimeAttendanceAuthenticationType
    {
        //public enum AttendanceAuthenticationTypeEnum
        Device = 1,
        SupervisorAdminModified = 77,
        SelfManualEntryFromSoftware = 78,
        TimeRequest = 101,
        Training = 102,
        OTTimeImport = 150,
        TravelRequest = 151

        // if any other authenticaty type like 1,2,0, and other, it will be direct device upload
    }
    public enum TimeAttendenceDayType
    {
        WorkingDay = 0, // if in or out or in/out time exists
        Leave = 1,
        WeeklyHoliday = 2,
        WeeklyHalfdayHoliday = 4,
        Holiday = 3,

        Training = 5,
        Absent = 6 // if no in/out time,leave,holiday not exists for the day
        , HalfDayLeave = 7
        , HalfDayLeaveWithWeeklyHalfDayHoliday = 8

    }
    public enum AttendenceCellType
    {
        Other = 0,
        Leave = 1,
        WeeklyHoliday = 2,
        WeeklyHalfdayHoliday = 4,
        Holiday = 3
    }
    public enum SalaryType
    {
        Combined = 1, //Salary adjustment amount
        Regular = 2, // Normal salary without adjustment
        Adjustment = 3, // Salary adjustment
        RetrospectArrear = 4
    }

    public enum CacheKey
    {
        DecimalPlaces,
        CurrentYearPayrollPeriodList,
        PayrollPeriodPayHeaderList,
        CalculationConstant
    }

    public abstract class ISequence
    {
        public abstract List<KeyValue> GetMembers();
        public string GetRes(string resKey)
        {
            // FixedValues = Resource file name
            object value = HttpContext.GetGlobalResourceObject("FixedValues", resKey);
            if (value == null)
                return "";
            return value.ToString();

        }

        public virtual string Get(string key)
        {

            foreach (KeyValue kv in GetMembers())
            {
                if (kv.Key == key)
                    return kv.Value;
            }
            return key;
        }

    }



    public class HolidaysConstant
    {
        public const string National_Holiday = "NH";
        public const string Female_Holiday = "FH";
        public const string Caste_Holiday = "CH";
        public const string Weekly_Holiday = "WH";
        public const string HalfWeekly_Holiday = "WH/2";

        public static string GetColor(string value)
        {
            if (value == "P")
                return "";

            if (value == Weekly_Holiday)
                return "#CE7870";
            if (value == HalfWeekly_Holiday)
                return "#FABF8F";

            string color = ConfigurationManager.AppSettings[value];

            if (color == null)
                return "";

            return color;
        }
    }

    public class Gender : ISequence
    {
        public const string Male = "Male";
        public const string Female = "Female";
        public const string Third_Gender = "Third Gender";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(Male, Male));
            list.Add(new KeyValue(Female, Female));
            list.Add(new KeyValue(Third_Gender, Third_Gender));

            return list;
        }

        public string GetText(int value)
        {
            if (value == 1)
                return Male;
            else if (value == 0)
                return Female;
            else if (value == 2)
                return Third_Gender;
            else
                return "";
            throw new ArgumentException("Invalid Gender value.");
        }
        public int GetValue(string key)
        {
            if (key.ToLower() == Male.ToLower())
                return 1;
            else if (key.ToLower() == Female.ToLower())
                return 0;
            else if (key.ToLower() == Third_Gender.ToLower())
                return 2;

            throw new ArgumentException("Invalid Gender value.");
        }
    }

    public class Title : ISequence
    {
        public const string MR = "Mr";
        public const string MRS = "Mrs";
        public const string MISS = "Miss";
        public const string EMPTY = "";


        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(MR, MR));
            list.Add(new KeyValue(MRS, MRS));
            list.Add(new KeyValue(MISS, MISS));

            List<TitleList> titles = BaseBiz.PayrollDataContext.TitleLists.OrderBy(x => x.Name).ToList();
            foreach (var item in titles)
            {
                list.Add(new KeyValue(item.Name, item.Name));
            }

            list.Add(new KeyValue(EMPTY, EMPTY));
            return list;
        }

    }



    public class EmployeeStatus : ISequence
    {
        public const string ACTIVE = "Active";
        public const string INACTIVE = "Inactive";
        public const string RETIRED = "Retired";
        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(ACTIVE, ACTIVE));
            list.Add(new KeyValue(INACTIVE, INACTIVE));
            list.Add(new KeyValue(RETIRED, RETIRED));
            return list;
        }
    }

    public enum JobStatusEnum
    {
        AllEmployees = 0,
        Irregular = 1,
        Trainee = 2,
        Probation = 3,
        Contract = 4,
        Temporary = 5,
        Permanent = 6
    }



    /// <summary>
    /// Employee Status &    effective statues
    /// </summary>
    public class JobStatus : ISequence
    {
        public const string AllEmployees = "AllEmployees";
        public const string Irregular = "Irregular";
        public const string TRAINEE = "Trainee";
        public const string PROBATION = "Probation";
        public const string CONTRACT = "Contract";
        public const string TEMPORARY = "Temporary";
        public const string PERMANENT = "Permanent";


        public static string GetStatusForDisplay(object status)
        {
            if (status == null)
                return "";
            foreach (KeyValue kv in new JobStatus().GetMembers())
            {
                if (kv.Key == status.ToString())
                    return kv.Value;
            }
            return "";
        }

        public static string GetValueForDisplay(int status)
        {
            foreach (KeyValue value in new JobStatus().GetMembers())
            {
                if (value.Key == status.ToString())
                    return value.Value;
            }
            return "";// ((JobStatusEnum)status).ToString();
        }

        public override string Get(string key)
        {

            foreach (KeyValue kv in GetMembersForHirarchyView())
            {
                if (kv.Key == key)
                    return kv.Value;
            }
            return key;
        }

        public List<DAL.StatusName> GetStatusList()
        {
            List<DAL.StatusName> list = MyCache.GetFromCache<List<DAL.StatusName>>("StatusList");

            if (list != null)
                return list;


            List<StatusName> dbList = BaseBiz.PayrollDataContext.StatusNames.ToList();


            MyCache.SaveToCache<List<DAL.StatusName>>("StatusList", dbList);

            return dbList;

        }

        public override List<KeyValue> GetMembers()
        {
            List<StatusName> dbList = GetStatusList();


            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(((int)JobStatusEnum.AllEmployees).ToString(), GetRes("AllEmployees")));

            if (dbList.Count > 0)
            {
                // if Irregular saved in db
                list.AddRange(
                    (
                    from s in dbList
                    orderby s.StatusId
                    select new KeyValue
                    {
                        Key = s.StatusId.ToString(),
                        Value = s.Name
                    }
                    ).ToList());



            }
            else
            {
                list.Add(new KeyValue(((int)JobStatusEnum.Irregular).ToString(), Irregular));
                list.Add(new KeyValue(((int)JobStatusEnum.Trainee).ToString(), TRAINEE));
                list.Add(new KeyValue(((int)JobStatusEnum.Probation).ToString(), PROBATION));
                list.Add(new KeyValue(((int)JobStatusEnum.Contract).ToString(), CONTRACT));
                list.Add(new KeyValue(((int)JobStatusEnum.Temporary).ToString(), TEMPORARY));
                list.Add(new KeyValue(((int)JobStatusEnum.Permanent).ToString(), PERMANENT));
            }
            return list;
        }


        public List<KeyValue> GetMembersForHirarchyView()
        {
            List<StatusName> dbList = GetStatusList();


            List<KeyValue> list = new List<KeyValue>();

            if (dbList.Count > 0)
            {
                //list.Add(new KeyValue(((int)JobStatusEnum.Irregular).ToString(), "All employees"));

                list.AddRange(
                  (
                  from s in dbList
                  orderby s.StatusId
                  select new KeyValue
                  {
                      Key = s.StatusId.ToString(),
                      Value = s.Name
                  }
                  ).ToList());

                for (int i = 0; i < list.Count; i++)
                {
                    if (i == 0)
                        list[i].Value = "All employees";
                    else if (i == list.Count - 1)//last one
                        list[i].Value += " only";
                    else
                        list[i].Value += " and Above";
                }


            }
            else
            {
                list.Add(new KeyValue(((int)JobStatusEnum.Irregular).ToString(), "All employees"));
                list.Add(new KeyValue(((int)JobStatusEnum.Trainee).ToString(), "Trainee and Above"));
                list.Add(new KeyValue(((int)JobStatusEnum.Probation).ToString(), "Probation and Above"));
                list.Add(new KeyValue(((int)JobStatusEnum.Contract).ToString(), "Contract and Above"));
                list.Add(new KeyValue(((int)JobStatusEnum.Temporary).ToString(), "Temporary and Above"));
                list.Add(new KeyValue(((int)JobStatusEnum.Permanent).ToString(), "Permanent only"));
            }
            return list;
        }

        public List<KeyValue> GetMembersForHirarchyViewWithOnlyText()
        {
            List<StatusName> dbList = GetStatusList();


            List<KeyValue> list = new List<KeyValue>();

            if (dbList.Count > 0)
            {
                //list.Add(new KeyValue(((int)JobStatusEnum.Irregular).ToString(), "All employees"));

                list.AddRange(
                  (
                  from s in dbList
                  orderby s.StatusId
                  select new KeyValue
                  {
                      Key = s.StatusId.ToString(),
                      Value = s.Name
                  }
                  ).ToList());

                for (int i = list.Count - 1; i >= 0; i--)
                {

                    //if (i == 0)
                    //    list[i].Value = "All employees";
                    if (i == list.Count - 1)//last one
                        list[i].Value += " Only";
                    else
                    {
                        KeyValue only = new KeyValue { Key = list[i].Key, Value = list[i].Value + " Only" };

                        list[i].Value += " and Above";



                        list.Insert(i, only);
                        //i--;


                    }
                }


            }
            else
            {
                list.Add(new KeyValue(((int)JobStatusEnum.Irregular).ToString(), "All employees"));
                list.Add(new KeyValue(((int)JobStatusEnum.Trainee).ToString(), "Trainee Only"));
                list.Add(new KeyValue(((int)JobStatusEnum.Trainee).ToString(), "Trainee and Above"));
                list.Add(new KeyValue(((int)JobStatusEnum.Probation).ToString(), "Probation Only"));
                list.Add(new KeyValue(((int)JobStatusEnum.Probation).ToString(), "Probation and Above"));
                list.Add(new KeyValue(((int)JobStatusEnum.Contract).ToString(), "Contract Only"));
                list.Add(new KeyValue(((int)JobStatusEnum.Contract).ToString(), "Contract and Above"));
                list.Add(new KeyValue(((int)JobStatusEnum.Temporary).ToString(), "Temporary Only"));
                list.Add(new KeyValue(((int)JobStatusEnum.Temporary).ToString(), "Temporary and Above"));
                list.Add(new KeyValue(((int)JobStatusEnum.Permanent).ToString(), "Permanent Only"));
            }
            return list;
        }
    }

    public class PaymentFrequency : ISequence
    {
        //public const string DAILY = "Daily";
        //public const string WEEKLY = "Weekly";
        public const string MONTHLY = "Monthly";
        //public const string QUARTERLY = "Quarterly";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(MONTHLY, MONTHLY));
            return list;
        }
    }

    public class EMILoadPaymentTerm : ISequence
    {
        public const string MONTHLY = "Monthly";
        public const string BI_MONTHLY = "BiMonthly";
        public const string Quarterely = "Quarterely";
        public const string HALF_YEARLY = "HalfYearly";
        public const string YEARLY = "Yearly";

        public string GetPaymentTerm(int paymentTerm)
        {
            switch (paymentTerm)
            {
                case 1:
                    return MONTHLY;
                    //break;
                case 2:
                    return GetRes(BI_MONTHLY);
                    //break;
                case 3:
                    return Quarterely;
                    //break;
                case 4:
                    return GetRes(HALF_YEARLY);
                   // break;
                case 5:
                    return YEARLY;
                    //break;
            }
            return "";
        }

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue("1", MONTHLY));
            list.Add(new KeyValue("2", GetRes(BI_MONTHLY)));
            list.Add(new KeyValue("4", Quarterely));
            list.Add(new KeyValue("6", GetRes(HALF_YEARLY)));
            list.Add(new KeyValue("12", YEARLY));
            return list;
        }
    }

    public class PaymentMode : ISequence
    {
        public const string CASH = "Cash";
        // public const string CHEQUE = "Cheque";
        public const string BANK_DEPOSIT = "Bank Deposit";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(CASH, CASH));
            //list.Add(new KeyValue(CHEQUE, CHEQUE));
            list.Add(new KeyValue(BANK_DEPOSIT, GetRes("PaymentModeBankDeposit")));
            return list;
        }
    }

    public class CompanyType : ISequence
    {
        public const string REGULAR = "Regular";
        public const string EXEMPTED = "Exempted";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(REGULAR, REGULAR));
            list.Add(new KeyValue(EXEMPTED, EXEMPTED));
            return list;
        }
    }

    public class Religion : ISequence
    {
        public const string Hindu = "Hindu";
        public const string Buddhism = "Buddhism";
        public const string Atheist = "Atheist";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(Hindu, Hindu));
            list.Add(new KeyValue(Buddhism, Buddhism));
            list.Add(new KeyValue(Atheist, Atheist));
            return list;
        }
    }

    public class Ethinicity : ISequence
    {
        public const string Khas = "Khas";
        public const string Madhesi = "Madhesi";
        public const string Janajati = "Janajati";
        public const string Dalit = "Dalit";
        public const string Adibasi = "Adibasi";


        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(Khas, Khas));
            list.Add(new KeyValue(Madhesi, Madhesi));
            list.Add(new KeyValue(Janajati, Janajati));
            list.Add(new KeyValue(Dalit, Dalit));
            list.Add(new KeyValue(Adibasi, Adibasi));
            return list;
        }
    }

    public class FestivalAllowanceCountWorkdaysFrom : ISequence
    {
        public const string DateOfJoining = "DateOfJoining";
        public const string SpecificDate = "SpecificDate";
        public const string FromMinimumStatusChange = "FromMinimumStatusChange";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(DateOfJoining, "Date of Joining"));
            list.Add(new KeyValue(SpecificDate, "Specific date"));
            list.Add(new KeyValue(FromMinimumStatusChange, "From Status Change"));
            return list;
        }
    }

    public class IncomeCalculation : ISequence
    {
        public const string PERCENT_OF_INCOME = "PercentOfIncome";
        //public const string HOURLY = "HourlyRate";
        //public const string DAILY = "DailyRate";
        public const string VARIABLE_AMOUNT = "VariableAmount";
        public const string FIXED_AMOUNT = "FixedAmount";
        //public const string PERCENTAGE_OF_SALES = "PercentageOfSales";
        public const string DEEMED_INCOME = "DeemedIncome";
        public const string FESTIVAL_ALLOWANCE = "FestivalAllowance";
        public const string Unit_Rate = "UnitRate";
        //public const string PIECE = "PieceRate";
        //public const string ANNUAL = "AnnualSalary";




        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(PERCENT_OF_INCOME, GetRes(PERCENT_OF_INCOME)));
            //list.Add(new KeyValue(HOURLY, GetRes(HOURLY)));
            //list.Add(new KeyValue(DAILY, GetRes(DAILY)));
            list.Add(new KeyValue(VARIABLE_AMOUNT, GetRes(VARIABLE_AMOUNT)));
            list.Add(new KeyValue(FIXED_AMOUNT, GetRes(FIXED_AMOUNT)));
            //list.Add(new KeyValue(PERCENTAGE_OF_SALES, GetRes(PERCENTAGE_OF_SALES)));
            list.Add(new KeyValue(DEEMED_INCOME, GetRes(DEEMED_INCOME)));
            list.Add(new KeyValue(FESTIVAL_ALLOWANCE, GetRes(FESTIVAL_ALLOWANCE)));
            list.Add(new KeyValue(Unit_Rate, "Unit Rate"));
            return list;
        }
    }

    //public class IncomeApplyOnceForList : ISequence
    //{
    //    public const string MONTH_LIST = "MonthList";
    //    public override List<KeyValue> GetMembers()
    //    {
    //        List<KeyValue> list = new List<KeyValue>();
    //        list.Add(new KeyValue(MONTH_LIST, GetRes(MONTH_LIST)));

    //        return list;
    //    }
    //}

    public class DeductionCalculation : ISequence
    {
        public const string VARIABLE = "VariableAmount";
        public const string FIXED = "FixedAmount";
        public const string PERCENT = "PercentOfIncome";
        public const string LOAN = "LoanInstallment";

        public const string Advance = "Advance";
        public const string LOAN_Repayment = "LoanRepayment";


        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(VARIABLE, GetRes(VARIABLE)));
            list.Add(new KeyValue(FIXED, GetRes(FIXED)));
            list.Add(new KeyValue(PERCENT, GetRes(PERCENT)));
            list.Add(new KeyValue(LOAN, GetRes(LOAN)));

            list.Add(new KeyValue(Advance, Advance));
            list.Add(new KeyValue(LOAN_Repayment, "Loan Repayment"));
            return list;
        }


    }

    public class LoanCalculation : ISequence
    {
        public const string DEFINED = "DefinedAmount";
        public const string CALCULATED = "CalculatedAmount";




        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(DEFINED, GetRes(DEFINED)));
            list.Add(new KeyValue(CALCULATED, GetRes(CALCULATED)));
            return list;
        }


    }
    public class MaritalStatus : ISequence
    {
        public const string SINGLE = "Single";
        public const string MARRIED = "Married";
        public const string DIVORCED = "Divorced";
        public const string WIDOW = "Widow";
        public const string WIDOWER = "Widower";
        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(SINGLE, SINGLE));
            list.Add(new KeyValue(MARRIED, MARRIED));
            list.Add(new KeyValue(DIVORCED, DIVORCED));
            list.Add(new KeyValue(WIDOW, WIDOW));
            list.Add(new KeyValue(WIDOWER, WIDOWER));
            return list;
        }
    }

    public class PaidBy : ISequence
    {
        public const string EMPLOYEE = "Employee";
        public const string EMPLOYER = "Employer";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(EMPLOYEE, EMPLOYEE));
            list.Add(new KeyValue(EMPLOYER, EMPLOYER));

            return list;

        }


    }


    public class LeaveAccrue : ISequence
    {

        public const string MONTHLY = "Monthly";
        public const string YEARLY = "Yearly";
        public const string HALFYEARLY = "HalfYearly";
        public const string MANUALLY = "Manually";
        public const string Based_On_WorkDays = "BasedOnWorkDays";
        public const string COMPENSATORY = "Compensatory";
        //public const string DAYS_WORKED = "DaysWorked";



        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();

            list.Add(new KeyValue(MONTHLY, MONTHLY));
            list.Add(new KeyValue(YEARLY, YEARLY));
            //list.Add(new KeyValue(HALFYEARLY, "Half Yearly"));
            list.Add(new KeyValue(MANUALLY, GetRes(MANUALLY)));
            list.Add(new KeyValue(Based_On_WorkDays, "Based On Work Days"));
            list.Add(new KeyValue(COMPENSATORY, COMPENSATORY));
            return list;

        }
    }


    public class LeaveAccrueFrom : ISequence
    {
        public const string JOINING_DATE = "JoiningDate";
        public const string PROBATION_COMPLETION_DATE = "ProbationCompletionDate";
        public const string CONFIRMATION_DATE = "ConfirmationDate";


        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(JOINING_DATE, GetRes(JOINING_DATE)));
            list.Add(new KeyValue(PROBATION_COMPLETION_DATE, GetRes(PROBATION_COMPLETION_DATE)));
            list.Add(new KeyValue(CONFIRMATION_DATE, GetRes(CONFIRMATION_DATE)));
            return list;

        }
    }

    public class UnusedLeave : ISequence
    {
        public const string LAPSE = "Lapse";
        public const string ENCASE = "Encase";
        public const string LAPSEENCASE = "LapseEncase";
        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(LAPSE, LAPSE));
            list.Add(new KeyValue(ENCASE, "Encash"));
            list.Add(new KeyValue(LAPSEENCASE, "Lapse and Encash"));
            return list;
        }
    }

    public class HalfDayLeave : ISequence
    {
        public const string ALLOWED = "Allowed";
        public const string NOTALLOWED = "NotAllowed";
        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(ALLOWED, ALLOWED));
            list.Add(new KeyValue(NOTALLOWED, "Not Allowed"));
            return list;
        }
    }
    public class LeaveType : ISequence
    {
        public const string FULLYPAID = "FullyPaid";
        public const string HALFPAID = "HalfPaid";
        public const string UNPAID = "Unpaid";
        public const string NON_LEAVE_ABSENCE = "NonLeaveAbsence";
        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(FULLYPAID, "Fully Paid"));
            list.Add(new KeyValue(HALFPAID, "Half Paid"));
            list.Add(new KeyValue(UNPAID, UNPAID));
            list.Add(new KeyValue(NON_LEAVE_ABSENCE, "Non-Leave Absence"));
            return list;
        }
    }

    public class LeaveAutoUpdate : ISequence
    {
        public const string NOT_UPDATED = "NotUpdated";
        public const string DAYS_WORKED = "DaysWorked";
        public const string BY_HOURS_ENTERED = "ByHoursEntered";


        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(NOT_UPDATED, GetRes(NOT_UPDATED)));
            list.Add(new KeyValue(DAYS_WORKED, GetRes(DAYS_WORKED)));
            list.Add(new KeyValue(BY_HOURS_ENTERED, GetRes(BY_HOURS_ENTERED)));
            return list;

        }
    }

    public class LeaveEncashment : ISequence
    {
        public const string ON_SPECIFIC_MONTH = "OnSpecificMonth";
        public const string END_OF_YEAR = "EndOfYear";
        public const string ON_RETIREMENT = "OnRetirement";


        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(ON_SPECIFIC_MONTH, GetRes(ON_SPECIFIC_MONTH)));
            list.Add(new KeyValue(END_OF_YEAR, GetRes(END_OF_YEAR)));
            list.Add(new KeyValue(ON_RETIREMENT, GetRes(ON_RETIREMENT)));
            return list;

        }
    }



    public class Relation : ISequence
    {
        public const string WIFE = "Wife";
        public const string HUSBAND = "Husband";
        public const string FATHER = "Father";
        public const string MOTHER = "Mother";
        public const string SON = "Son";
        public const string DAUGHTER = "Daughter";
        public const string BROTHER = "Brother";
        public const string SISTER = "Sister";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(WIFE, WIFE));
            list.Add(new KeyValue(HUSBAND, HUSBAND));
            list.Add(new KeyValue(FATHER, FATHER));
            list.Add(new KeyValue(MOTHER, MOTHER));
            list.Add(new KeyValue(SON, SON));
            list.Add(new KeyValue(DAUGHTER, DAUGHTER));
            list.Add(new KeyValue(BROTHER, BROTHER));
            list.Add(new KeyValue(SISTER, SISTER));


            return list;
            // return new string[] { WIFE, HUSBAND, FATHER, MOTHER, SON, DAUGHTER, BROTHER, SISTER };
        }
    }
    public class PAN : ISequence
    {
        public const string HASPAN = "HasPAN";
        public const string APPLIEDPAN = "AppliedForPAN";
        public const string NOPAN = "NoPAN";//"Does not have PAN";
        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(HASPAN, GetRes(HASPAN)));
            list.Add(new KeyValue(APPLIEDPAN, GetRes(APPLIEDPAN)));
            list.Add(new KeyValue(NOPAN, GetRes(NOPAN)));


            return list;
            //return new string[] { HASPAN, APPLIEDPAN, NOPAN };
        }
    }

    public class GratuityApplicableTo : ISequence
    {
        //public const string ALL_EMPLOYEES = "AllEmployees";
        //public const string COVERED_UNDER_GRATUITY_RULE = "CoveredUnderGratuityRule";
        public const string NOT_COVERED_UNDER_GRATUITY_RULE = "NotCoveredUnderGratuityRule";//"Does not have PAN";
        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            //list.Add(new KeyValue(ALL_EMPLOYEES, GetRes(ALL_EMPLOYEES)));
            //list.Add(new KeyValue(COVERED_UNDER_GRATUITY_RULE, GetRes(COVERED_UNDER_GRATUITY_RULE)));
            list.Add(new KeyValue(NOT_COVERED_UNDER_GRATUITY_RULE, GetRes(NOT_COVERED_UNDER_GRATUITY_RULE)));


            return list;
        }

    }


    public class CompanyNetSalaryRoundOff : ISequence
    {
        /// <summary>
        /// No decimal, Means if 5.6 => 6
        /// </summary>
        public const string NEAREST_RUPEE = "NearestRupee";
        /// <summary>
        /// Decimal with two places
        /// </summary>
        public const string TWO_DECIMAL = "TwoDecimal";
        //public const string NEAREST_TEN_RUPEE = "NearestTenRupee";
        //public const string HIGHER_TEN_RUPEE = "HigherTenRupee";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(NEAREST_RUPEE, GetRes(NEAREST_RUPEE)));
            list.Add(new KeyValue(TWO_DECIMAL, GetRes(TWO_DECIMAL)));
            //list.Add(new KeyValue(NEAREST_TEN_RUPEE, GetRes(NEAREST_TEN_RUPEE)));
            //list.Add(new KeyValue(HIGHER_TEN_RUPEE, GetRes(HIGHER_TEN_RUPEE)));

            return list;

        }
    }



    public class ShiftAllowanceName : ISequence
    {

        public const string Morning_Shift = "Morning Shift";
        public const string Evening_Shift = "Evening Shift";

        public const string Night_Shift = "Night Shift";
        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(Morning_Shift, Morning_Shift));
            list.Add(new KeyValue(Evening_Shift, Evening_Shift));
            list.Add(new KeyValue(Night_Shift, Night_Shift));


            return list;
        }


    }



    public class Visibility : ISequence
    {
        public const string Private = "Private";
        public const string Library = "Library";


        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(Private, Private));
            list.Add(new KeyValue(Library, Library));

            return list;
            // return new string[] { WIFE, HUSBAND, FATHER, MOTHER, SON, DAUGHTER, BROTHER, SISTER };
        }
    }




    #region "OT"

    public class OTTimeSlot : ISequence
    {

        public const string Late_Hours = "Late Hours";
        public const string Night = "Night";
        public const string Festival = "Festival";
        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(Late_Hours, Late_Hours));
            list.Add(new KeyValue(Night, Night));
            list.Add(new KeyValue(Festival, Festival));


            return list;
        }


    }

    public enum OTTreatmentTypeEnum
    {
        Overtime = 1,
        AddLeave = 2
    }

    public class OTTreatmentType : ISequence
    {

        public const string Overtime = "Overtime";
        public const string Add_Leave = "Add Leave";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(((int)OTTreatmentTypeEnum.Overtime).ToString(), Overtime));
            list.Add(new KeyValue(((int)OTTreatmentTypeEnum.AddLeave).ToString(), Add_Leave));


            return list;
        }


    }

    #endregion


    #region "Holiday"
    public class DaysOfWeek : ISequence
    {
        public readonly static string SUNDAY = "0";
        public readonly static string MONDAY = "1";
        public readonly static string TUESDAY = "2";
        public readonly static string WEDNESDAY = "3";
        public readonly static string THURSDAY = "4";
        public readonly static string FRIDAY = "5";
        public readonly static string SATURDAY = "6";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(SUNDAY, "Sunday"));
            list.Add(new KeyValue(MONDAY, "Monday"));
            list.Add(new KeyValue(TUESDAY, "Tuesday"));
            list.Add(new KeyValue(WEDNESDAY, "Wednesday"));
            list.Add(new KeyValue(THURSDAY, "Thursday"));
            list.Add(new KeyValue(FRIDAY, "Friday"));
            list.Add(new KeyValue(SATURDAY, "Saturday"));
            return list;
        }

        public static string Get(int weekDay)
        {
            foreach (KeyValue item in new DaysOfWeek().GetMembers())
            {
                if (item.Key.Equals(weekDay.ToString()))
                    return item.Value;
            }
            return string.Empty;
        }
    }

    public class HolidayType : ISequence
    {
        public readonly static string FULL_DAY = "FULL_DAY";
        public readonly static string SECOND_HALF = "SECOND_HALF";


        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            list.Add(new KeyValue(FULL_DAY, "Full Day"));
            list.Add(new KeyValue(SECOND_HALF, "Second Half"));
            return list;
        }
    }


    public class PremiumPaymentFrequency : ISequence
    {
        //public readonly static string MONTHLY = "Monthly";      
        public readonly static string Quarterely = "Quarterely";
        public readonly static string HALF_YEARLY = "HalfYearly";
        public readonly static string YEARLY = "Yearly";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            // list.Add(new KeyValue(MONTHLY, MONTHLY));
            //list.Add(new KeyValue(Quarterely, Quarterely));
            //list.Add(new KeyValue(HALF_YEARLY, GetRes(HALF_YEARLY)));
            list.Add(new KeyValue(YEARLY, YEARLY));
            return list;
        }
    }

    public class DepartmentaActionType : ISequence
    {
        public readonly static string IssueWarning = "IssueWarning";
        public readonly static string Punishment = "Punishment";
        public readonly static string StopSalaryGrade = "StopSalaryGrade";
        public readonly static string Demotion = "Demotion";
        public readonly static string Dismiss = "Dismiss";

        public override List<KeyValue> GetMembers()
        {
            List<KeyValue> list = new List<KeyValue>();
            // list.Add(new KeyValue(MONTHLY, MONTHLY));
            //list.Add(new KeyValue(Quarterely, Quarterely));
            //list.Add(new KeyValue(HALF_YEARLY, GetRes(HALF_YEARLY)));
            list.Add(new KeyValue(IssueWarning, GetRes(IssueWarning)));
            list.Add(new KeyValue(Punishment, GetRes(Punishment)));
            list.Add(new KeyValue(StopSalaryGrade, GetRes(StopSalaryGrade)));
            list.Add(new KeyValue(Demotion, GetRes(Demotion)));
            list.Add(new KeyValue(Dismiss, GetRes(Dismiss)));
            return list;
        }

    }
    #endregion

}
