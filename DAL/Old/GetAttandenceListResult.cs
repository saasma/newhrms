﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public partial class GetAttandenceListResult
    {
        private string[] values = null;
        private string[] leaveRequestValues = null;

        private Dictionary<string, int> leaveRequestKeys = new Dictionary<string, int>();

        /// <summary>
        /// holds the cell disable start day
        /// </summary>
        private int disableCellStartDay = 0;
        /// <summary>
        /// holds the cell disable end day
        /// </summary>
        private int disableCellEndDay = 0;

        private void SetDisableDays()
        {
            if (disableCellEndDay == disableCellStartDay)
            {
                string[] startEndValues = this.DisableCellRange.Split(new char[] { ':'});
                disableCellStartDay = int.Parse(startEndValues[0]);
                disableCellEndDay = int.Parse(startEndValues[1]);
            }
        }



        public int DisableCellStartDay
        {
            get
            {
                SetDisableDays();
                return disableCellStartDay;
            }
            set { }
        }

        public int DisableCellEndDay
        {
            get
            {
                SetDisableDays();
                return disableCellEndDay;
            }
            set { }
        }



        private string[] GetCellValue(string dayNumber)
        {
            int column;
            if (int.TryParse(dayNumber, out column))
            {
                if (values == null && this.AttendenceList != null)
                    values = this.AttendenceList.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (leaveRequestValues == null && this.LeaveRequest != null)
                {
                    leaveRequestValues = this.LeaveRequest.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (leaveRequestValues.Length > 0)
                    {
                        // Save Leave Request in the List
                        foreach (string cell in leaveRequestValues)
                        {
                            string[] cellSplit = cell.Split(new char[] { ':' });
                            // if multiple leave approved for same day like d2 problem so prevent it
                            // ,should have been prevented from logic but somehow coming the case
                            if (leaveRequestKeys.ContainsKey(cellSplit[0]) == false)
                                leaveRequestKeys.Add(cellSplit[0], 0);
                        }
                    }
                }

                // If has leave request
                if (leaveRequestValues != null && leaveRequestValues.Length > 0)
                {
                    string[] cellSplit = null;

                    //iterate each leave request day & if the day matches then return the cell info
                    foreach (string cell in leaveRequestValues)
                    {
                        string[] tempcellSplit = cell.Split(new char[] { ':' });
                        if (tempcellSplit[0] == column.ToString())
                        {
                            if (cellSplit == null)
                                cellSplit = tempcellSplit;
                            else
                            {
                                // found second leave so return both combining
                                string[] newcellSplit = new string[10];
                                newcellSplit[0] = cellSplit[0];
                                newcellSplit[1] = cellSplit[1];
                                newcellSplit[2] = cellSplit[2];
                                newcellSplit[3] = cellSplit[3];
                                newcellSplit[4] = cellSplit[4];

                                newcellSplit[5] = tempcellSplit[0];
                                newcellSplit[6] = tempcellSplit[1];
                                newcellSplit[7] = tempcellSplit[2];
                                newcellSplit[8] = tempcellSplit[3];
                                newcellSplit[9] = tempcellSplit[4];

                                return newcellSplit;
                            }
                        }
                    }

                    // if single leave then return that one
                    if (cellSplit != null)
                        return cellSplit;
                
                }

                if (values != null && values.Length >= column)
                {
                    //iterate each attendance day & if the day matches then return the cell info
                    foreach (string cell in values)
                    {
                        string[] cellSplit = cell.Split(new char[] { ':' });
                        if (cellSplit[0] == column.ToString())
                        {
                            return cellSplit;
                        }
                    }
                    //string[] cells = values[column - 1].Split(new char[] { ':' });
                    //return cells;
                }
            }
            return null;
        }

        /// <summary>
        /// Get the actual value for the cell/day
        /// </summary>
        /// <param name="dayNumber"></param>
        /// <returns></returns>
        public string GetDayValue(string dayNumber)
        {
            string[] cells = GetCellValue(dayNumber);
            
            if (cells != null)
            {
                // if multiple leave
                //if (cells.Length > 5)
                //{
                //    return cells[1] + ";" + cells[6];
                //}
                //else
                    return cells[1];
            }

            return string.Empty;
        }


        public bool HasLeaveRequestOnThisDay(string dayNumber)
        {
            if (leaveRequestKeys.ContainsKey(dayNumber))
                return true;
            return false;
        }

        public string GetType(string dayNumber)
        {
            string[] cells = GetCellValue(dayNumber);
            if (cells != null)
            {
                // if multiple leave
                if (cells.Length > 5)
                {
                    return cells[2] + ":" + cells[3] + ":" + cells[4] + ";" + cells[7] + ":" + cells[8] + ":" + cells[9];
                }
                else
                    return cells[2] + ":" + cells[3] + ":" + cells[4];
            }

            return string.Empty; ;
        }


        //public string GetTypeValue(string dayNumber)
        //{
        //    string[] cells = GetCellValue(dayNumber);
        //    if (cells != null)
        //    {
        //        return cells[3];
        //    }

        //    return string.Empty;
        //}

        //public string GetDeductDays(string dayNumber)
        //{

        //    string[] cells = GetCellValue(dayNumber);
        //    if (cells != null)
        //    {
        //        return cells[4];
        //    }

        //    return string.Empty;
        //}
    }
}
