﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Base;
using Utils;


namespace DAL
{


    public partial class GetEmployeeDeductionDetailsListResult
    {
        /// <summary>
        /// Access to rate or amount to be displayed in Deduction list for Employee
        /// </summary>
        /// 
        public string RateAmountString { get; set; }
        public string CalculationString { get; set; }
        public string RowBackColor { get; set; }
        public bool IsDeletable { get; set; }

        public string RateAmount(int decimalPlaces,int currentPayrollPeriodId)
        {

            if (this.IsValid)
            {
                //return 0;
                if (this.Calculation == DeductionCalculation.FIXED
                    || this.Calculation == DeductionCalculation.VARIABLE
                    || this.Calculation==DeductionCalculation.LOAN_Repayment)
                    return BaseHelper.GetCurrency(this.Amount == null ? 0  : this.Amount.Value, decimalPlaces);
                else if (this.Calculation == DeductionCalculation.PERCENT)
                    return Convert.ToInt32(this.Rate == null ? 0 : this.Rate.Value) + "%";
                else if (this.Calculation == DeductionCalculation.Advance)
                {
                    //PayrollDataContext context = new PayrollDataContext(Config
                    //using (context)
                    {
                        try
                        {
                            CalcGetLoanAdvanceInstallmentAmountResult result = BaseBiz.PayrollDataContext.CalcGetLoanAdvanceInstallmentAmount(
                                                                                          this.EmployeeId, this.DeductionId, currentPayrollPeriodId).SingleOrDefault();
                            if (result != null && result.LoanAmount.HasValue)
                                return BaseHelper.GetCurrency(result.LoanAmount, decimalPlaces);
                            else
                                return "-";
                                //return BaseHelper.GetCurrency(this.InstallmentAmount, decimalPlaces);
                        }
                        catch
                        {
                            return BaseHelper.GetCurrency(this.InstallmentAmount, decimalPlaces);
                        }
                    }
                }

                else if (this.Calculation == DeductionCalculation.LOAN)
                {


                    if (this.HasVariableAmount.Value)
                        return BaseHelper.GetCurrency(this.Amount == null ? 0 : this.Amount.Value, decimalPlaces);
                    else
                        return BaseHelper.GetCurrency(this.InstallmentAmount, decimalPlaces);
                }
            }

            return "";

        }
    }
}
