﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public partial class VoucherHeadGroup
    {
        public string TypeSourceId
        {
            get
            {
                return this.Type + ":" + this.SourceId;
            }
            set { }
        }
    }
    public partial class GetYearEndSummaryHeaderResult
    {
        public string PeriodTypeSourceId
        {
            get
            {
                return this.PayrollPeriodId + ":" +  this.Type + ":" + this.SourceId;
            }
            set { }
        }
    }
    public partial class VoucherHeadGroup
    {
        public string CalculationText
        {
            get;
            set;
        }
        public int SN { get; set; }
    }
    public partial class CalcGetHeaderListResult
    {
        public int ID { get; set; }
        public int SN { get; set; }
        public string Value { get; set; }
        public bool IsRetirement { get; set; } // For retirement table to track if retiremnt columnr or regular column
        public bool? AddToRegularSalary { get; set; }
        public bool UseIncomeSetting { get; set; }
        public int VoucherGroupID { get; set; }
        public CalculationColumnType ColumnType
        {
            get { return (CalculationColumnType) this.Type; }
            set { }
        }

        public int Order { get; set; }
        public string TypeSourceId
        {
            get
            {
                return this.Type + ":" + this.SourceId;
            }
            set { }
        }
        public string RetirementTypeSourceId
        {
            get
            {
                return this.IsRetirement + ":" + this.Type + ":" + this.SourceId;
            }
            set { }
        }
        public int PartialPaidHeaderId { get; set; }

        public int AddOnHeaderId { get; set; }
        public string Calculation
        {
            get
            {
                if (this.Type == 1)
                {
                    return DAL.BaseBiz.PayrollDataContext.PIncomes.FirstOrDefault(
                        x => x.IncomeId == this.SourceId).Calculation;
                }
                return "";
            }
            set { }
        }

        public string CalculationText
        {
            get;
            set;
        }

    }
}
