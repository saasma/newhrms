﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Base;


namespace DAL
{
    public class DepartwiseSalaryBO
    {
        public int DepartmentId { get; set; }
        public decimal Amount { get; set; }
    }
    public partial class Report_Pay_SalaryExtractDepartmentWiseResult
    {

        private List<DepartwiseSalaryBO> list = null;

      
        ///// <summary>
        ///// Returns the comment if Adjustment column
        ///// </summary>
        //public string GetComment(int type, int sourceId)
        //{
        //    if (list != null)
        //    {
        //        foreach (var calculationValue in list)
        //        {
        //            if (!string.IsNullOrEmpty(calculationValue.AdjustmentComment)
        //                && calculationValue.Type == type && calculationValue.SourceId == sourceId)
        //            {
        //                return calculationValue.AdjustmentComment;
        //            }
        //        }

        //    }
        //    return string.Empty;
        //}

        //public string GetList(int type, int sourceId)
        //{
        //    if (list != null)
        //    {
        //        foreach (var calculationValue in list)
        //        {
        //            if (calculationValue.Type == type && calculationValue.SourceId == sourceId)
        //            {
        //                return calculationValue.ListOfId;
        //            }
        //        }

        //    }
        //    return string.Empty;
        //}

        /// <summary>
        /// Returns the cell value
        /// </summary>
        public decimal? GetCellValue(int departmentId, int decimalPlaces)
        {


            if (!string.IsNullOrEmpty(this.DepartmentWiseList))
            {
                if (list == null)
                {
                    list = new List<DepartwiseSalaryBO>();

                    string[] values = this.DepartmentWiseList.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                


                    //incomeValues = new string[values.Length, 2];
                    DepartwiseSalaryBO value = null;
                    foreach (string valueEach in values)
                    {
                        //Generally cellvalues contains like 
                        // Type:SourceId:Value:[Adj comment if Adjustment type | List of Special Event if SpecialEvent type]
                        string[] cellvalues = valueEach.Split(new char[] { ':' });

                        {


                            value = new DepartwiseSalaryBO()
                            {
                                DepartmentId = int.Parse(cellvalues[0]),

                                Amount = decimal.Parse(cellvalues[1] == "" ? "0" : cellvalues[1])
                            };



                        }

                       

                       

                        list.Add(value);

                    }

                }



                {

                    foreach (var calculationValue in list)
                    {
                        if (calculationValue.DepartmentId == departmentId)
                        {
                            return calculationValue.Amount;
                        }
                    }
                }
            }
            return null;
        }

      

    }
}
