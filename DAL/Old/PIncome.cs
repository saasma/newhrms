﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public partial class PIncome
    {
        public int SN { get; set; }
        public int Type { get; set; }
        public int SourceId { get; set; }

        public bool NoPFIncome { get; set; }
        public bool? AddToRegularSalary { get; set; }

        public string TypeSourceId
        {
            get
            {
                return this.Type + ":" + this.SourceId;
            }
            set { }
        }

        public string TitleWithPF
        {
            set { }
            get
            {

                if (NoPFIncome)
                    return this.Title;

                if (this.IsPFCalculated)
                    return this.Title + " + PF";
                return this.Title;
            }
        }

        public int PartialPaidHeaderId { get; set; }
        public int AddOnHeaderId { get; set; }
        public bool UseIncomeSettings { get; set; }
        public bool IncomeDeletable { get; set; }
        public string IsUpdatedWithLeaveValue { get; set; }
        public string IsPFCalculatedValue { get; set; }
        public string CalculationType { get; set; }
        public string IsDefinedTypeValue { get; set; }
        public string IsTaxableValue { get; set; }
        public string IsEnabledValue { get; set; }

    }
}
