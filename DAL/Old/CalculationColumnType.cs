﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    /// <summary>
    /// Mainly changed in "CalculationColumnType" table
    /// Salary column types, -ve so as to prevent for "IncomeId:Amount" type
    /// if changed, changes requires in sp "[CalcGetHeaderList]",function "[CalGetIncomeList]",
    /// DAL.CalcGetCalculationListResult.cs,BLL.CalculationManager.cs
    /// </summary>
    public enum CalculationColumnType
    {
        //OvertimePay = -1,
        Income = 1,
        IncomeUSD =100, //dollar income column for D2
        //IncomeSpecialEvent = 2,
        //IncomeResterospect = 3,
        IncomePF = 4, // Company Contribution
        IncomeLeaveEncasement = 5, //Income Leave Encasement


        




        
        IncomeGrossUSD = 101,//dollar income column for D2
        IncomeGross = 7,

        Deduction = 8,
        DeductionLoanInterest = 18,
        DeductionExcessLeave = 9, // LOP
        DeductionPF = 10, // Company Contribution + Employee Contribution from Optimum PF
        //DeductipnPFRetrospect = 21,
        DeductionCIT = 11,
        //DeductionAdjustment = 12,// Not used in latest version
        SST = 20,
        TDS = 13,
        DeductionInsurance = 17,
        DeductionTotal = 14,

        NetSalary = 15,
        NetSalaryUSD = 155,

        DeemedIncome = 25,//income type of caluclation "Deemed income" & used for display
        //but doesn't used in income or deduction calculation only in tax calculation 



        Increment = 16, //Used in CalculationIncluded table to hold the increments & Special event id,
        ReportPDays = 50,
        Insurance = 19,
        OtherFund = 200,
        Leave = 51, //Used in Employee Master case
        AddOnOrPartialIncome = 52,
        PastRegularIncomeAmount=54,
        TaxRemoteAreaDeduction = 55,
        TaxInsuranceDeduction = 56,
        Gratuity = 60,
        RetirementTDS = 61,
        Header = 300,
        HeaderHightlight = 301,
        ExternalCIT = 302
        //used in sp "GetLastIncludedIncrement", used in function "[CalcGetIncrementIncome]"
    }

    public static class TaxItem
    {

        public const string SumofCITAndPF = "Sum of CIT And PF";    
        public const string DeductionLimit = "Limit";
        public const string OneThird = "1/3rd of Taxable";
        public const string EligibleRetirementFund = "Min of a, b or c";
        public const string EligibleLifeInsuranceDeduction = "Insurance Premium";

        public const string AnnualTaxableAmount = "Annual Taxable Amount";

        public const string OnePercentSST = "SST 1% Tax";
        public const string FifteenPercentTDS = "TDS 15% Tax";
        public const string TwentyFivePercentTDS = "TDS 25% Tax";
        public const string Surchange = "Surcharge";
        public const string LessFemaleRebate="Less Female Rebate";

        public const string SSTAddonPaid = "SST Add-on Paid";
        public const string TDSAddonPaid = "TDS Add-on Paid";
        public const string TotalSSTfortheYear="Total SST for the Year";
        public const string TotalTDSfortheYear="Total TDS for the Year";
        public const string SSTPaidinPastMonth="SST Paid in Past Month";
        public const string TDSPaidinPastMonth="TDS Paid in Past Month";
        public const string RemainingSST="Remaining SST";
        public const string RemainingTDS="Remaining TDS";
        
        public const string SSTthisMonth="SST this Month";
        public const string TDSthisMonth="TDS this Month";

    }

    /// <summary>
    /// Enum used to categorise the income types in Tax Details Calculation List
    /// </summary>
    public enum TaxDetailHeaderEnum
    {
        DefaultIncome = -2,
        PastIncome = -1,
        CurrentIncome = 0,
        ForecastIncome = 1,
        OneTimeIncome = 2,
        DeemedIncome = 3
    }

    /// <summary>
    /// Represents the Value holding different information for each Cell of the Calculation
    /// </summary>
    public  class CalculationValue
    {
        public int Type { get; set; }

        public int SettlementId { get; set; }

        public CalculationColumnType ColumnType
        {
            get { return (CalculationColumnType) Type; }
        }

        public string ToolTip { get; set; }

        public CalculationErrorCode ErrorCode { get; set; } 
        public bool IsError { get; set; }
        public string ErrorMessage
        {
            get
            {
                switch (ErrorCode)
                {
                    case CalculationErrorCode.AdjustmentExistingAmountMisMatch:
                        return "Previous adjustment exists for different amount, please remove the adjustment and resave if needed.";
                    case CalculationErrorCode.DefaultValueNotSet:
                        return "Default value not set.";
                    default:
                        return "";
                }
            }
        }
        public string HeaderName { get; set; }
        public int SourceId { get; set; }
        public int ProjectId { get; set; }
        public bool HasCIT { get; set; }
        public decimal Amount { get; set; }
        public decimal FullAmount { get; set; }
        public decimal AmountWithOutAdjustment { get; set; }
        public decimal AdjustedAmount { get; set; }
        public decimal TDS { get; set; }
        public decimal NetPayable { get; set; }

        public decimal TDSAdjustment { get; set; }
        public bool ExcludeFromRetirement { get; set; }
        /// <summary>
        /// Adjustment Comment if column type is Adjustment
        /// </summary>
        public string AdjustmentComment { get; set; }
        /// <summary>
        /// List of Id if column type is Special event
        /// </summary>
        public string ListOfId { get; set; }

        /// <summary>
        /// Provides logic in application if the ColumnType is Income or Not
        /// </summary>
        public static bool IsColumTypeIncome(CalculationColumnType type)
        {
            if ((type >= CalculationColumnType.Income
                        && type <= CalculationColumnType.IncomeLeaveEncasement)
                //|| (type >= CalculationColumnType.IncomeVehicleAllowance && type <= CalculationColumnType.IncomeSiteAllowance
                //|| (type == CalculationColumnType.IncomeResterospectAllowance))
                //|| (type == CalculationColumnType.IncomePFRetrospect)
           //     || (type == CalculationColumnType.IncomeUSD)
           //     || (type == CalculationColumnType.IncomeGrossUSD)
                )
                return true;
            return false;
        }

        /// <summary>
        /// Provides logic in application if the ColumnType is Deduction or Not
        /// </summary>
        public static bool IsColumTypeDeduction(CalculationColumnType type)
        {
            if ((type >= CalculationColumnType.Deduction
                 && type <= CalculationColumnType.TDS)
                 || type == CalculationColumnType.DeductionInsurance
                 || type == CalculationColumnType.SST
                 || type==CalculationColumnType.DeductionLoanInterest
                 //|| type == CalculationColumnType.DeductipnPFRetrospect   
                )


                return true;
            return false;
        }

        public static bool IsColumTypeDeemedIncome(CalculationColumnType type)
        {
            if (type == CalculationColumnType.DeemedIncome)


                return true;
            return false;
        }

        /// <summary>
        /// If the column type is HPL unit type or not
        /// </summary>
        //public static bool IsHPLUnitColumn(CalculationColumnType type)
        //{
        //    if (type >= CalculationColumnType.IncomeOvertimePublicHolidayUnit && type <= CalculationColumnType.IncomeOnCallAllowanceUnit)
        //        return true;

        //    return false;
        //}

        /// <summary>
        /// If the column type is HPL unit type or not
        /// </summary>
        public static bool IsD2Column(CalculationColumnType type)
        {
            if (type == CalculationColumnType.IncomeUSD || type == CalculationColumnType.IncomeGrossUSD)
                return true;

            return false;
        }


        /// <summary>
        /// If the column type is HPL allowance or overtime column
        /// </summary>
        //public static bool IsHPLAllowanceOrOvertimeColumn(CalculationColumnType type)
        //{
        //    if (type >= CalculationColumnType.IncomeOvertimePublicHoliday && type <= CalculationColumnType.IncomeOnCallAllowance)
        //        return true;

        //    return false;
        //}


        private static List<CalcGetHeaderListResult> GetSortedIncomesOrDeduction(
            List<CalcGetHeaderListResult> headers, Dictionary<int, int> orders)
        {
            try
            {

                foreach (var calcGetHeaderListResult in headers)
                {


                    calcGetHeaderListResult.Order = orders[calcGetHeaderListResult.SourceId.Value];


                    //if( incomeOrders.ContainsKey(calcGetHeaderListResult.SourceId))
                }
            }
            catch (Exception)
            {
                
               
            }




            return headers.OrderBy(i => i.Order).ToList();
        }


        public static List<CalcGetHeaderListResult> SortHeaders(List<CalcGetHeaderListResult> headers,Dictionary<int,int> incomeOrders, Dictionary<int,int> deductionOrders )
        {
            List<CalcGetHeaderListResult> tempHeaders = new List<CalcGetHeaderListResult>();

            //d2 columns
            tempHeaders.AddRange(
                GetSortedIncomesOrDeduction(headers.Where(h => h.ColumnType == CalculationColumnType.IncomeUSD).ToList(),
                    incomeOrders));
            tempHeaders.AddRange(
               GetSortedIncomesOrDeduction(headers.Where(h => h.ColumnType == CalculationColumnType.IncomeGrossUSD).ToList(),
                   incomeOrders));


            List<CalcGetHeaderListResult> list =  
                GetSortedIncomesOrDeduction( headers.Where(h => h.ColumnType == CalculationColumnType.Income).ToList(),
                    incomeOrders);
            CalcGetHeaderListResult pfHeader = headers.Where(h => h.ColumnType == CalculationColumnType.IncomePF).OrderBy(h => h.SourceId)
                .FirstOrDefault();

            if(pfHeader != null)
            {
                Company comp =DAL.BaseBiz.PayrollDataContext.Companies.FirstOrDefault();
                int pfOrder = comp.PFOrder==null ? 0 : comp.PFOrder.Value;

                pfHeader.Order = pfOrder;

                list.Add(pfHeader);
            }

            tempHeaders.AddRange(list.OrderBy(x=>x.Order));
            
          
            tempHeaders.AddRange(
                headers.Where(h => h.ColumnType == CalculationColumnType.IncomeLeaveEncasement).OrderBy(h => h.SourceId));
            //tempHeaders.AddRange(
            //                    headers.Where(h => h.ColumnType == CalculationColumnType.IncomeInsurance).OrderBy(h => h.SourceId));

           


            tempHeaders.AddRange(
                headers.Where(h => h.ColumnType == CalculationColumnType.IncomeGross).OrderBy(h => h.SourceId));


            //Deemed incomes
            tempHeaders.AddRange(
                            headers.Where(h => h.ColumnType == CalculationColumnType.DeemedIncome).OrderBy(h => h.SourceId));

            //Deduction
            tempHeaders.AddRange(
               GetSortedIncomesOrDeduction(headers.Where(h => h.ColumnType == CalculationColumnType.Deduction).ToList(),
                   deductionOrders));

           
            tempHeaders.AddRange(
               headers.Where(h => h.ColumnType == CalculationColumnType.DeductionLoanInterest).OrderBy(h => h.SourceId));
            tempHeaders.AddRange(
                headers.Where(h => h.ColumnType == CalculationColumnType.DeductionExcessLeave).OrderBy(
                    h => h.SourceId));
            tempHeaders.AddRange(
                headers.Where(h => h.ColumnType == CalculationColumnType.DeductionPF).OrderBy(h => h.SourceId));
            //tempHeaders.AddRange(
            //    headers.Where(h => h.ColumnType == CalculationColumnType.DeductipnPFRetrospect).OrderBy(h => h.SourceId));
            tempHeaders.AddRange(
                headers.Where(h => h.ColumnType == CalculationColumnType.DeductionCIT).OrderBy(h => h.SourceId));

            tempHeaders.AddRange(
              headers.Where(h => h.ColumnType == CalculationColumnType.ExternalCIT).OrderBy(h => h.SourceId));
            //tempHeaders.AddRange(
            //    headers.Where(h => h.ColumnType == CalculationColumnType.DeductionAdjustment).OrderBy(
            //        h => h.SourceId));

            tempHeaders.AddRange(
                headers.Where(h => h.ColumnType == CalculationColumnType.DeductionInsurance).OrderBy(h => h.SourceId));

            tempHeaders.AddRange(
               headers.Where(h => h.ColumnType == CalculationColumnType.SST).OrderBy(h => h.SourceId));

            tempHeaders.AddRange(
                headers.Where(h => h.ColumnType == CalculationColumnType.TDS).OrderBy(h => h.SourceId));
            tempHeaders.AddRange(
               headers.Where(h => h.ColumnType == CalculationColumnType.DeductionTotal).OrderBy(h => h.SourceId));
            tempHeaders.AddRange(
               headers.Where(h => h.ColumnType == CalculationColumnType.NetSalary).OrderBy(h => h.SourceId));
            tempHeaders.AddRange(
              headers.Where(h => h.ColumnType == CalculationColumnType.NetSalaryUSD).OrderBy(h => h.SourceId));
            return tempHeaders;
        }
    }

    public enum CalculationErrorCode
    {
        AdjustmentExistingAmountMisMatch = 1,
        DefaultValueNotSet = 2 //when IsValid = false
    }
}
