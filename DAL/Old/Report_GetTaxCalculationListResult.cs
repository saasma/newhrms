﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
namespace DAL
{
    public partial class Report_GetTaxCalculationListResult
    {


        private Dictionary<string,string> list = null;

        public bool IsExists(CalcGetTaxDetailsHeaderListResult header)
        {
            if (this.TaxDetails == null)
                return false;

            if (list == null)
            {
                list = new Dictionary<string, string>();

                string[] values = this.TaxDetails.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
               
                foreach (string valueEach in values)
                {

                   
                    list.Add(valueEach,"");
                }
            }

            if (list.ContainsKey (header.Type + ":" + header.SourceId + ":" + header.GroupType))
                return true;
            return false;
        }

        public string GetValue(CalcGetTaxDetailsHeaderListResult header)
        {
            //happens due to new employee with no basic salary like 
            if (this.TaxDetails == null)
                return "";
            string value = "", description, past, current, future, amount;
            int type, sourceId;
            decimal futureAmount, futureRem;
            if (list == null)
            {
                list = new Dictionary<string, string>();


                string[] values = this.TaxDetails.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string valueEach in values)
                {
                    string[] items = valueEach.Split(new char[] { ':' });

                    value = "";

                    // if has more than 5 then only the line is valid as other has not Type/SourceId info
                    if (items.Length >= 5)
                    {
                        type = int.Parse(items[4]);
                        sourceId = int.Parse(items[5]);
                        amount = items[1];
                        description = items[2];

                        PIncome income = BaseBiz.PayrollDataContext.PIncomes.SingleOrDefault(x=>x.IncomeId == sourceId);

                        // for past Fixed/Percent of income used but disable now case
                        if (items.Length > 6 && items[6] == "Disabled" && (income.Calculation==IncomeCalculation.FIXED_AMOUNT || income.Calculation==IncomeCalculation.PERCENT_OF_INCOME))
                        {
                            if (list.ContainsKey(type + ":" + sourceId + ":-1") == false)
                            {
                                list.Add(type + ":" + sourceId + ":-1", amount);
                            }
                            else
                            {   //if already added like due to some logic dashain can come twice but only one can have value
                                decimal newAmount, oldAmount;
                                decimal.TryParse(amount, out newAmount);
                                string oldValue = list[type + ":" + sourceId + ":-1"];
                                decimal.TryParse(oldValue, out oldAmount);

                                list[type + ":" + sourceId + ":-1"] = (oldAmount + newAmount).ToString();
                            }
                        }
                        // for past Variable of income used but disable now case
                        else if (items.Length > 6 && items[6] == "Disabled" )
                        {
                            if (list.ContainsKey(type + ":" + sourceId + ":2") == false)
                            {
                                list.Add(type + ":" + sourceId + ":2", amount);
                            }
                            else
                            {   //if already added like due to some logic dashain can come twice but only one can have value
                                decimal newAmount, oldAmount;
                                decimal.TryParse(amount, out newAmount);
                                string oldValue = list[type + ":" + sourceId + ":2"];
                                decimal.TryParse(oldValue, out oldAmount);

                                list[type + ":" + sourceId + ":2"] = (oldAmount + newAmount).ToString();
                            }
                        }
                        // then is of Past/Current/Forecast income
                        else if (description.Contains(",") )
                        {
                            //Regular, 1323000.00 + 148500.00 + (148500.00*2)
                            if (description.Contains(","))
                                description = description.Substring(description.IndexOf(",") + 1);
                            
                               
                            string[] amounts = description.Split(new char[] { '+' }, StringSplitOptions.RemoveEmptyEntries);

                            string defaultAmount = "0";
                            decimal defaultAmountValue = 0;

                            if (items.Length > 6)
                            {
                                defaultAmount = items[6];
                                decimal.TryParse(defaultAmount, out defaultAmountValue);
                            }

                            past = amounts[0].Trim();
                            current = amounts[1].Trim();
                            //if has futur section only
                            if (amounts.Length >= 3)
                            {
                                future = amounts[2].Trim().Replace("(", "").Replace(")", "");

                                string[] futureSplited = future.Split(new char[] { '*' });

                                if (futureSplited.Length >= 1)
                                {
                                    decimal.TryParse(futureSplited[0], out futureAmount);
                                    decimal.TryParse(futureSplited[1], out futureRem);
                                }
                                else
                                {
                                    futureAmount = 0;
                                    futureRem = 0;
                                }
                            }
                            else
                            {
                                futureAmount = 0;
                                futureRem = 0;
                            }


                            if (list.ContainsKey(type + ":" + sourceId + ":0") == false)
                            {
                                list.Add(type + ":" + sourceId + ":-2", defaultAmount);
                                list.Add(type + ":" + sourceId + ":-1", past); // past        
                                list.Add(type + ":" + sourceId + ":0", current); // current 
                                list.Add(type + ":" + sourceId + ":1", (futureAmount * futureRem).ToString()); // future                                
                            }
                            else
                            {
                                //for income can have adjustment
                                list[type + ":" + sourceId + ":-2"] = (decimal.Parse(list[type + ":" + sourceId + ":-2"]) + Convert.ToDecimal( defaultAmountValue)).ToString();
                                list[type + ":" + sourceId + ":-1"] = (decimal.Parse(list[type + ":" + sourceId + ":-1"]) + decimal.Parse( past)).ToString();
                                list[type + ":" + sourceId + ":0"] = (decimal.Parse(list[type + ":" + sourceId + ":0"]) + decimal.Parse( current)).ToString();
                                list[type + ":" + sourceId + ":1"] = (decimal.Parse(list[type + ":" + sourceId + ":1"]) + (futureAmount * futureRem)).ToString();
                            }
                          
                        }
                        // or Deemed Income
                        else if (type == 25)
                        {
                            list.Add(type + ":" + sourceId + ":3", amount);
                        }
                        // for basic like Fixed having forecasting from Add-On
                        else if (income != null &&  income.Calculation == IncomeCalculation.FIXED_AMOUNT && items.Length > 6)
                        {
                            if (list.ContainsKey(type + ":" + sourceId + ":" + items[6]) == false)
                            {
                                list.Add(type + ":" + sourceId + ":" + items[6], amount);
                            }
                            else
                            {   //if already added like due to some logic dashain can come twice but only one can have value
                                decimal newAmount, oldAmount;
                                decimal.TryParse(amount, out newAmount);
                                string oldValue = list[type + ":" + sourceId + ":" + items[6]];
                                decimal.TryParse(oldValue, out oldAmount);

                                list[type + ":" + sourceId + ":" + items[6]] = (oldAmount + newAmount).ToString();
                            }
                        }
                        else if (type == 1)
                        {
                            // for basic like Fixed in add on time

                            //if (income.Calculation==IncomeCalculation.FIXED_AMOUNT)
                            //{
                            //    list.Add(type + ":" + sourceId + ":3", amount);
                            //}
                            if (list.ContainsKey(type + ":" + sourceId + ":2") == false)
                            {
                                list.Add(type + ":" + sourceId + ":2", amount);
                            }
                            else
                            {   //if already added like due to some logic dashain can come twice but only one can have value
                                decimal newAmount, oldAmount;
                                decimal.TryParse(amount, out newAmount);
                                string oldValue = list[type + ":" + sourceId + ":2"];
                                decimal.TryParse(oldValue, out oldAmount);

                                list[type + ":" + sourceId + ":2"] = (oldAmount + newAmount).ToString();
                            }
                            // varialble like one time income
                        }
                        //for the type like Leave Encash & other
                        else if (type == (int)CalculationColumnType.DeductionPF || type == (int)CalculationColumnType.DeductionCIT)
                        {
                            string[] amounts = description.Split(new char[] { '+' }, StringSplitOptions.RemoveEmptyEntries);


                            past = amounts[0].Trim();
                            current = amounts[1].Trim();
                            if (amounts.Length >= 3)
                            {
                                future = amounts[2].Trim().Replace("(", "").Replace(")", "");

                                string[] futureSplited = future.Split(new char[] { '*' });

                                if (futureSplited.Length >= 1)
                                {
                                    decimal.TryParse(futureSplited[0], out futureAmount);
                                    decimal.TryParse(futureSplited[1], out futureRem);
                                }
                                else
                                {
                                    futureAmount = 0;
                                    futureRem = 0;
                                }

                            }
                            else
                            {
                                futureAmount = 0;
                                futureRem = 0;
                            }

                            list.Add(type + ":" + sourceId + ":-1", past); // past        
                            list.Add(type + ":" + sourceId + ":0", current); // current 
                            list.Add(type + ":" + sourceId + ":1", (futureAmount * futureRem).ToString()); // future                                

                        }
                        else
                        {
                            if (list.ContainsKey(type + ":" + sourceId + ":2") == false)
                                list.Add(type + ":" + sourceId + ":2", amount);
                            else
                            {   //if already added like due to some logic dashain can come twice but only one can have value
                                decimal newAmount, oldAmount;
                                decimal.TryParse(amount, out newAmount);
                                string oldValue = list[type + ":" + sourceId + ":2"];
                                decimal.TryParse(oldValue, out oldAmount);

                                list[type + ":" + sourceId + ":2"] = (oldAmount + newAmount).ToString();
                            }
                        }
                    }

                    
                }
            }

            if (list.ContainsKey(header.Type + ":" + header.SourceId + ":" + header.GroupType))

                return list[header.Type + ":" + header.SourceId + ":" + header.GroupType];

            return "";
        }
    }
}
