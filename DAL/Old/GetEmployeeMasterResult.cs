﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
namespace DAL
{
    public partial class GetEmployeeMasterResult
    {


        private Dictionary<string,string> list = null;

        public bool IsExists(GetEmployeeMasterOtherHeadersResult header)
        {
            if (this.List == null)
                return false;

            if (list == null)
            {
                list = new Dictionary<string, string>();

                string[] values = this.List.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
               
                foreach (string valueEach in values)
                {

                   
                    list.Add(valueEach,"");
                }
            }

            if (list.ContainsKey (header.Type + ":" + header.SourceId))
                return true;
            return false;
        }

        public string GetValue(GetEmployeeMasterOtherHeadersResult header)
        {
            //happens due to new employee with no basic salary like 
            if (this.List == null)
                return "";

            if (list == null)
            {
                list = new Dictionary<string, string>();

                
                string[] values = this.List.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string valueEach in values)
                {
                    string[] items = valueEach.Split(new char[] { ':' });

                    string value = "";
                    if (items.Length >= 3)
                        value = items[2];
                    list.Add(items[0]  + ":" + items[1], value);
                }
            }

            if (list.ContainsKey(header.Type + ":" + header.SourceId))

                return list[header.Type + ":" + header.SourceId];
            return "";
        }
    


    




    }

    

}
