﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Base;


namespace DAL
{
    public partial class Report_Pay_SalarySummary_MultipleMonthsResult
    {

        private List<CalculationValue> list = null;

        private decimal totalIncome = 0;
        private decimal totalDeduciton = 0;
        private decimal netSalary = 0;

        public void SetList(List<Report_Pay_SalarySummary_MultipleMonthsDetailsResult> details, 
            List<CalcGetHeaderListResult> headerList, int decimalPlaces)
        {
            

            list = new List<CalculationValue>();


            string[] values = this.List.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            //incomeValues = new string[values.Length, 2];
            CalculationValue value = null;
            //foreach (string valueEach in values)
            foreach(var item in details)
            {
                CalculationValue listItem = list.FirstOrDefault(x => x.Type == item.Type && x.SourceId == item.SourceId);

                if (listItem != null)
                {
                    listItem.Amount += item.Total;
                }
                else
                {


                    value = new CalculationValue()
                    {
                        Type = item.Type.Value,
                        SourceId = item.SourceId.Value,
                        Amount = item.Total
                    };


                    list.Add(value);
                }

                //if ((value.ColumnType == CalculationColumnType.IncomeAdjustment ||
                //         value.ColumnType == CalculationColumnType.DeductionAdjustment)
                //        && cellvalues.Length > 3)
                //    value.AdjustmentComment = cellvalues[3];

                //For Special event & normal income, 4 value contains id of special event or increment id to
                //be saved in CCalculationIncluded table
                //if ((value.ColumnType == CalculationColumnType.Income
                //    //|| value.ColumnType == CalculationColumnType.IncomeInsurance
                //        || value.ColumnType == CalculationColumnType.DeductionInsurance) && cellvalues.Length > 3)
                //{
                //    value.ListOfId = cellvalues[3];
                //}

                

            }


            // set Gross Income,Total Deduction and Net Salary

            // If income total then generate total
            //if (columnType == CalculationColumnType.IncomeGross && totalIncome == 0)
            {
                foreach (var calculationValue in list)
                {
                    //if income type & its header exists then sum
                    if (
                        ( 
                        CalculationValue.IsColumTypeIncome(calculationValue.ColumnType)
                        ||
                        (calculationValue.Type == 0 && calculationValue.SourceId == 0) // for opening
                        )&&
                        ((headerList != null && headerList.Any(x=>x.Type == calculationValue.Type && x.SourceId == calculationValue.SourceId)) || headerList == null)
                        )
                    {
                        // skip deduction for income total
                        //if (calculationValue.Type != 8)
                        {

                            if (calculationValue.Amount != -1)
                                totalIncome +=
                                    decimal.Parse(BaseHelper.GetCurrency(calculationValue.Amount, decimalPlaces));
                        }
                    }
                }

            }
            // Deduction total
            //else if (columnType == CalculationColumnType.DeductionTotal && totalDeduciton == 0)
            {
                foreach (var calculationValue in list)
                {
                    if (CalculationValue.IsColumTypeDeduction(calculationValue.ColumnType) &&
                        ((headerList != null && headerList.Any(x => x.Type == calculationValue.Type && x.SourceId == calculationValue.SourceId)) || headerList == null)
                        )
                    {
                        if (calculationValue.Amount != -1)// -1 is Invalid case
                        {
                            //skip adding -ve tds in deduction
                            //if (
                            //    (calculationValue.ColumnType == CalculationColumnType.TDS || calculationValue.ColumnType == CalculationColumnType.SST)
                            //    && calculationValue.Amount <= 0) ;
                            //else
                            //    totalDeduciton += decimal.Parse(BaseHelper.GetCurrency(calculationValue.Amount, decimalPlaces));
                            totalDeduciton += decimal.Parse(BaseHelper.GetCurrency(calculationValue.Amount, decimalPlaces));
                        }
                    }
                }
                // return totalDeduciton;
            }




        }

        /// <summary>
        /// Returns the cell value
        /// </summary>
        public decimal? GetCellValue(int type, int sourceId, int decimalPlaces, Dictionary<string, string> headerList)
        {

            CalculationColumnType columnType = (CalculationColumnType)type;

            if (columnType == CalculationColumnType.IncomeGross)
                return totalIncome;
            else if (columnType == CalculationColumnType.DeductionTotal)
                return totalDeduciton;
            else if (columnType == CalculationColumnType.NetSalary)
                return totalIncome - totalDeduciton;

            if (this.list != null)
            {
               

                
                {

                    foreach (var calculationValue in list)
                    {
                        if (calculationValue.Type == type && calculationValue.SourceId == sourceId)
                        {
                            return calculationValue.Amount;
                        }
                    }
                }
            }
            return null;
        }

      

    }
}
