﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Utils;
using System.IO;
using System.Web;
using Utils.Calendar;
using Utils.Security;
using System.Data.Common;
using System.Data;
using System.Reflection;

namespace DAL
{

    public partial class  PayrollDataContext
    {
        partial void OnCreated()
        {
            //Put your desired timeout here.
            //reqd if morethan 350 employee sal being exported from pay page,then timeout coming so optimize it 
            
           
            this.CommandTimeout = this.CommandTimeout * 50; // increased to 50 from 20 for tax summary export like for prabhu case

            //If you do not want to hard code it, then take it 
            //from Application Settings / AppSettings
            //this.CommandTimeout = Settings.Default.CommandTimeout;
        }
    }

    public class InsertUpdateStatus
    {
        public string ErrorMessage { get; set; }
        public bool IsSuccess { get; set; }

        public InsertUpdateStatus()
        {
            IsSuccess = true;
            ErrorMessage = "";
        }
    }


    public  enum ResultType
    {
        Success,
        NotChanged,
        NoRecord
    }

    //http://blogs.vertigo.com/personal/keithc/Blog/Lists/Posts/Post.aspx?ID=11

    public abstract class BaseBiz
    {
        /// <summary>
        /// Dictionary key for the DataContext in HttpContext.Current.Items
        /// </summary>
        public const string DATACONTEXT_ITEMS_KEY = "LinqUtilDataContextKey";

        protected static void SetConnectionPwd(PayrollDataContext PayrollDataContext)
        {
            if (SecurityHelper.hasPwd != null && SecurityHelper.hasPwd.Value)
            {
                PayrollDataContext.Connection.Open();
                PayrollDataContext.ExecuteCommand(string.Format("OPEN SYMMETRIC KEY DBDX DECRYPTION BY PASSWORD='{0}'",
                    SecurityHelper.pwd.Trim()));
            }
        }

        public static void SetConnectionPwd(DbConnection connection)
        {
            //Set common Connection String
          
            if (connection.State != ConnectionState.Open)
            {
                connection.ConnectionString = Config.ConnectionString;
                connection.Open();             
            }

            if (SecurityHelper.hasPwd != null && SecurityHelper.hasPwd.Value)
            {
                DbCommand cmd = connection.CreateCommand();
                cmd.CommandText = string.Format("OPEN SYMMETRIC KEY DBDX DECRYPTION BY PASSWORD='{0}'",
                     SecurityHelper.pwd.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        //private PayrollDataContext _payrollDataContext;
        //private StreamWriter writer;
        public static PayrollDataContext PayrollDataContext
        {
            get
            {
                if (HttpContext.Current.Items[DATACONTEXT_ITEMS_KEY] != null)
                {
                    PayrollDataContext _payrollDataContext = HttpContext.Current.Items[DATACONTEXT_ITEMS_KEY] as PayrollDataContext;
                    //SetConnectionPwd(_payrollDataContext);
                    //_payrollDataContext.ExecuteCommand("OPEN SYMMETRIC KEY DBDX DECRYPTION BY PASSWORD='admin'");
                    return _payrollDataContext;
                }
                else
                {
                    PayrollDataContext _payrollDataContext = new PayrollDataContext(Config.ConnectionString);
                    //_payrollDataContext.Connection.Open();
                    // _payrollDataContext.ExecuteCommand("OPEN SYMMETRIC KEY DBDX DECRYPTION BY PASSWORD='admin'");
                    SetConnectionPwd(_payrollDataContext);

                    
                    HttpContext.Current.Items[DATACONTEXT_ITEMS_KEY] = _payrollDataContext;
                    return _payrollDataContext;
                }
            }
        }

        public  BaseBiz()
        {

            
            

            //_payrollDataContext.DeferredLoadingEnabled = true;


         //   writer = new StreamWriter(HttpContext.Current.Server.MapPath("~/App_Data/LinqLog.txt"));
            //_payrollDataContext.Log

          //  PayrollDataContext.Log = writ
        }


        /// <summary>
        /// Fixed columns to be skipped while updating
        /// </summary>
        private static string[] skipFixedColumns =
            new string[] { "CreatedOn", "CreatedBy" };

        /// <summary>
        /// Copy an object to destination object, only matching fields will be copied
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceObject">An object with matching fields of the destination object</param>
        /// <param name="destObject">Destination object, must already be created</param>
        /// <param name="skippingNames">Property names that don't need to be updated like ID, CreatedOn, CreatedBy</param> 
        public static void CopyObject<T>(object sourceObject, ref T destObject, params string[] skippingNames)
        {

            //	If either the source, or destination is null, return
            if (sourceObject == null || destObject == null)
                return;

            //	Get the type of each object
            Type sourceType = sourceObject.GetType();
            Type destType = destObject.GetType();

            //	Loop through the source properties
            foreach (PropertyInfo p in sourceType.GetProperties())
            {
                //	Get the matching property in the destination object
                PropertyInfo destObj = destType.GetProperty(p.Name);

                //	If there is none, skip
                if (destObj == null || destObj.CanWrite == false)
                    continue;

                // if type if not String & IsClass then skip
                if (destObj.PropertyType.IsClass && !destObj.PropertyType.FullName.Equals("System.String"))
                    continue;


                if (skippingNames != null && (skippingNames.Contains(p.Name) || skipFixedColumns.Contains(p.Name)))
                    continue;


                object value = p.GetValue(sourceObject, null);
                //	Set the value in the destination
                if (value != null)
                {
                    destObj.SetValue(destObject, value, null);
                }
            }
        }



       

        #region IDisposable Members

        public static void Dispose()
        {
            if (HttpContext.Current.Items[DATACONTEXT_ITEMS_KEY] != null)
            {
                PayrollDataContext _payrollDataContext = HttpContext.Current.Items[DATACONTEXT_ITEMS_KEY] as PayrollDataContext;

                using (_payrollDataContext)
                {
                    if (_payrollDataContext.Connection.State == System.Data.ConnectionState.Open)
                        _payrollDataContext.Connection.Close();
                    _payrollDataContext.Dispose();

                    HttpContext.Current.Items.Remove(DATACONTEXT_ITEMS_KEY);
                }  
                
            }
            
        }

        #endregion
        


        protected static bool SaveChangeSet()
        {
            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.SubmitChanges();
            return cs.Inserts.Count >= 1;
        }

        /// <summary>
        /// Don't use to show nothing happen while updated as  it returns true even after successfull update if no changes for all rows
        /// </summary>
        /// <returns></returns>
        //[Obsolete("As it returns true even after successfull update if no changes for all rows")]
        protected static bool UpdateChangeSet()
        {
            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.SubmitChanges();
            return cs.Updates.Count >= 1;
        }

        protected static bool DeleteChangeSet()
        {
            System.Data.Linq.ChangeSet cs = PayrollDataContext.GetChangeSet();
            PayrollDataContext.SubmitChanges();
            return cs.Deletes.Count >= 1;
        }


       
       
    }
}
