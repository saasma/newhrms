﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public partial class EEmployee 
    {
        public string DepartmentValue { get; set; }
        public string BranchValue { get; set; }
        public string CostCodeValue { get; set; }
        public string LevelDesignationValue { get; set; }
        public string LevelValue { get; set; }
        public string DesignationValue { get; set; }
        public string SubDepartmentValue { get; set; }
        public string IdCardNoValue { get; set; }
        public bool IsRetiredOrResignedValue { get; set; }
        public int StatusID { get; set; }
        public string StatusText { get; set; }
        public string URLPhoto { get; set; }
        public string FunctionalTitle { get; set; }
        public void SeparateCombinedName()
        {
            //if (string.IsNullOrEmpty(this.FirstName))
            {
                string[] splits = this.Name.Trim().Split(new char[] { ' ' });
                if (splits.Length == 0)
                {
                }
                else if (splits.Length == 1)
                {
                    this.FirstName = splits[0].Trim();
                }
                else if (splits.Length == 2)
                {
                    this.FirstName = splits[0].Trim();
                    this.LastName = splits[1].Trim();
                }
                else
                {
                    this.FirstName = splits[0].Trim();

                    this.MiddleName = splits[1].Trim();
                    for (int i = 2; i < splits.Length - 1; i++)
                        this.MiddleName += " " + splits[i].Trim();

                    this.LastName = splits[splits.Length - 1].Trim();
                }


            }

            if (this.FirstName == null)
                this.FirstName = "";
            if (this.MiddleName == null)
                this.MiddleName = "";
            if (this.LastName == null)
                this.LastName = "";
        }

        public string IDAndName
        {
            set { }
            get
            {
                return this.EmployeeId + " - " + this.Name;
            }
        }

        public string NameEIN
        {
            set { }
            get
            {
                return this.Name + " - " + this.EmployeeId;
            }
        }

        public static string GetCombinedName(string first, string middle, string last)
        {
            if (!string.IsNullOrEmpty(middle))
                return first + " " + middle + " " + last;

            return first + " " + last;
        }

        private int? _gradeIdLatest = null;
        public int? GradeIdLatest
        {
            get
            {
                if (_gradeIdLatest != null)
                    return _gradeIdLatest;

                EGradeHistory _gradeLatest =
                BaseBiz.PayrollDataContext.EGradeHistories
                    .Where(e => e.EmployeeId == this.EmployeeId)
                    .OrderByDescending(e => e.GradeHistoryId)
                    .Take(1).SingleOrDefault();

                if (_gradeLatest != null)
                    _gradeIdLatest = _gradeLatest.GradeId;




                return _gradeIdLatest;
            }
            set { }
        }

        private int? _workShiftIdLatest = null;
        public int? WorkShiftIdLatest
        {
            get
            {
                if (_workShiftIdLatest != null)
                    return _workShiftIdLatest;

                EWorkShiftHistory _latest =
                BaseBiz.PayrollDataContext.EWorkShiftHistories
                    .Where(e => e.EmployeeId == this.EmployeeId)
                    .OrderByDescending(e => e.WorkShiftHistoryId)
                    .Take(1).SingleOrDefault();

                if (_latest != null)
                    _workShiftIdLatest = _latest.WorkShiftId;




                return _workShiftIdLatest;
            }
            set { }
        }

        
        //public string FundName
        //{
        //    get
        //    {
        //        return PFRFName + "(" + PFRFAbbreviation + ")";
        //    }
        //}

        public bool IsValidImport { get; set; }
        public string ImportErrorMsg { get; set; }
    }
}
