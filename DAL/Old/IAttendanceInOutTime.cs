﻿using System;
namespace DAL
{


    public class AttendanceInOutTimeBO
    {
        int EmployeeId { get; set; }
        TimeSpan? OfficeFirstHalfInTime { get; set; }
        TimeSpan? OfficeInTime { get; set; }
        TimeSpan? OfficeOutTime { get; set; }
        TimeSpan? OfficeSecondHalfOutTime { get; set; }       
        int SettingId { get; set; }
        int? Threshold { get; set; }
    }


}
