﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Utils.Base;

namespace DAL
{
    public partial class EmployeeIncomeListResult
    {

        public string Value { get; set; }

        /// <summary>
        /// Access to rate or amount to be displayed in Deduction list for Employee
        /// </summary>
        public string RateAmount(int decimalPlaes)
        {
            if (this.IsValid.Value)
            {
                string cal = this.Calculation;


                if ((cal == IncomeCalculation.VARIABLE_AMOUNT || cal == IncomeCalculation.FIXED_AMOUNT) && this.Amount != null )
                {
                    return BaseHelper.GetCurrency(this.Amount.Value, decimalPlaes);
                }
                //else if (cal == IncomeCalculation.PERCENTAGE_OF_SALES && this.RatePercent != null)
                //{
                //    return Convert.ToInt32(this.RatePercent) + "%";
                //}
                //else if (cal == IncomeCalculation.DAILY || cal == IncomeCalculation.HOURLY)
                //{
                //    return this.Rate.Value;
                //}
                else if (cal == IncomeCalculation.DEEMED_INCOME )
                {
                    if(this.MarketValue != null && this.Amount!=null)
                        return BaseHelper.GetCurrency(this.MarketValue - this.Amount, decimalPlaes);
                    else if( this.RatePercent!=null)
                        return (this.RatePercent) + "%";

                    //return (this.MarketValue - this.Amount).Value;
                }
                else if (cal == IncomeCalculation.PERCENT_OF_INCOME && this.RatePercent != null)
                {
                    return (this.RatePercent) + "%";
                    //return Convert.ToDecimal(this.RatePercent.Value);
                }
                else if (cal == IncomeCalculation.Unit_Rate && this.RatePercent != null)
                {
                    return BaseHelper.GetCurrency(this.RatePercent, decimalPlaes);
                    //return Convert.ToDecimal(this.RatePercent.Value);
                }
                else if (cal == IncomeCalculation.FESTIVAL_ALLOWANCE)
                {

                    if (IsVariableAmount.Value && this.Amount != null)
                    {
                        return BaseHelper.GetCurrency(this.Amount.Value, decimalPlaes);
                    }
                    else if (IsCalculatedAmount.Value && this.RatePercent != null)
                    {
                        return Convert.ToInt32(this.RatePercent) + "%";
                    }
                }
            }

            return "";


        }

        /// <summary>
        /// Access to rate or amount to be displayed in Deduction list for Employee
        /// </summary>
        public string GetRate(int decimalPlaes)
        {
            if (this.IsValid.Value)
            {
                string cal = this.Calculation;

                                

                if (cal == IncomeCalculation.DEEMED_INCOME)
                {
                    if (this.RatePercent != null)
                        return (this.RatePercent) + "%";

                    //return (this.MarketValue - this.Amount).Value;
                }
                else if (cal == IncomeCalculation.PERCENT_OF_INCOME && this.RatePercent != null)
                {
                    return (this.RatePercent) + "%";
                    //return Convert.ToDecimal(this.RatePercent.Value);
                }
               
                else if (cal == IncomeCalculation.FESTIVAL_ALLOWANCE)
                {

                   if (IsCalculatedAmount.Value && this.RatePercent != null)
                    {
                        return Convert.ToInt32(this.RatePercent) + "%";
                    }
                }
            }

            return "";


        }

        /// <summary>
        /// Access to rate or amount to be displayed in Deduction list for Employee
        /// </summary>
        public string GetAmount(int decimalPlaes)
        {
            if (this.IsValid.Value)
            {
                string cal = this.Calculation;


                if ((cal == IncomeCalculation.VARIABLE_AMOUNT || cal == IncomeCalculation.FIXED_AMOUNT) && this.Amount != null )
                {
                    return BaseHelper.GetCurrency(this.Amount == null ? 0 : this.Amount.Value, decimalPlaes);
                }
                //else if (cal == IncomeCalculation.PERCENTAGE_OF_SALES && this.RatePercent != null)
                //{
                //    return Convert.ToInt32(this.RatePercent) + "%";
                //}
                //else if (cal == IncomeCalculation.DAILY || cal == IncomeCalculation.HOURLY)
                //{
                //    return this.Rate.Value;
                //}
                else if (cal == IncomeCalculation.DEEMED_INCOME )
                {
                    if (this.MarketValue != null && this.Amount != null)
                        return BaseHelper.GetCurrency(this.MarketValue - this.Amount, decimalPlaes);
                    else if (this.RatePercent != null)
                        return BaseHelper.GetCurrency(this.Amount,decimalPlaes);

                    //return (this.MarketValue - this.Amount).Value;
                }
                else if (cal == IncomeCalculation.PERCENT_OF_INCOME && this.RatePercent != null)
                {
                    return BaseHelper.GetCurrency(this.Amount, decimalPlaes);
                    //return Convert.ToDecimal(this.RatePercent.Value);
                }
                else if (cal == IncomeCalculation.Unit_Rate && this.RatePercent != null)
                {
                    return BaseHelper.GetCurrency(this.RatePercent, decimalPlaes);
                    //return Convert.ToDecimal(this.RatePercent.Value);
                }
                else if (cal == IncomeCalculation.FESTIVAL_ALLOWANCE)
                {

                    if (IsVariableAmount.Value && this.Amount != null)
                    {
                        return BaseHelper.GetCurrency(this.Amount.Value, decimalPlaes);
                    }
                    else if (IsCalculatedAmount.Value && this.RatePercent != null && this.Amount!=null)
                    {
                        return BaseHelper.GetCurrency(this.Amount, decimalPlaes);
                    }
                }
            }

            return "-";


        }
    }
}
