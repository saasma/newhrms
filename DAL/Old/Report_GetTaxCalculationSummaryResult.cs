﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
namespace DAL
{
    public partial class Report_GetTaxCalculationSummaryResult
    {
        private decimal _medical = 0;
        public decimal MedicalTax { set { _medical = value; } get { return _medical; } }
        public decimal AddOnSSTPaid { get; set; }
        public decimal AddOnTDSPaid { get; set; }
        public decimal SSTPaid { get; set; }
        public decimal TDSPaid { get; set; }
        public decimal ExcessTaxPaid { get; set; }
        public decimal ExternalCIT{get;set;}

        public decimal PastRegularIncomeAmount{get;set;}
        public decimal InsuranceAmount { get; set; }
        public decimal HealthInsurance { get; set; }
        public decimal ProvisionalCITDeduction { get; set; }
        private Dictionary<string,string> list = null;

        public bool IsExists(CalcGetTaxDetailsHeaderListResult header)
        {
            if (this.TaxDetails == null)
                return false;

            if (list == null)
            {
                list = new Dictionary<string, string>();

                string[] values = this.TaxDetails.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
               
                foreach (string valueEach in values)
                {

                   
                    list.Add(valueEach,"");
                }
            }

            if (list.ContainsKey (header.Type + ":" + header.SourceId + ":" + header.GroupType))
                return true;
            return false;
        }

        public string GetValue(CalcGetHeaderListResult header)
        {
            //happens due to new employee with no basic salary like 
            if (this.TaxDetails == null)
                return "";
            string value = "", description,  amount;
            int type, sourceId;
            decimal amountValue, amount1, amount2;
            if (list == null)
            {
                list = new Dictionary<string, string>();


                string[] values = this.TaxDetails.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string valueEach in values)
                {
                    string[] items = valueEach.Split(new char[] { ':' });

                    value = "";

                    // if has more than 5 then only the line is valid as other has not Type/SourceId info
                    if (items.Length >= 5)
                    {
                        type = int.Parse(items[4]);
                        sourceId = int.Parse(items[5]);
                        amount = items[1];
                        description = items[2];

                        PIncome income = BaseBiz.PayrollDataContext.PIncomes.SingleOrDefault(x => x.IncomeId == sourceId);
                        // if income is deemed set type = 25
                        if (type == 1 && income != null && income.Calculation == IncomeCalculation.DEEMED_INCOME)
                            type = 25;

                        if (type == (int)CalculationColumnType.PastRegularIncomeAmount && sourceId == (int)CalculationColumnType.PastRegularIncomeAmount)
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.PastRegularIncomeAmount = amountValue;
                            }
                        }
                        else if (type == (int)CalculationColumnType.DeductionPF && sourceId == (int)CalculationColumnType.DeductionPF)
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.PFDeduction = Convert.ToDecimal(this.PFDeduction) +  amountValue;
                            }
                        }
                        else if (type == (int)CalculationColumnType.DeductionCIT && sourceId == (int)CalculationColumnType.DeductionCIT)
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.CITDeduction = amountValue;
                            }
                        }
                        else if (valueEach.ToLower().Contains("insurance income") &&
                           valueEach.ToLower().Contains("none cash yearly") && type != (int)CalculationColumnType.Income)
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.InsuranceAmount = amountValue;
                            }
                        }
                        else if (valueEach.ToLower().Contains("insurance premium") && type != (int)CalculationColumnType.Income)
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.InsuranceDeduction = amountValue;
                            }
                        }
                        else if (valueEach.ToLower().Equals("remote area") && type != (int)CalculationColumnType.Income)
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.RemoteArea = amountValue;
                            }
                        }
                        else if (valueEach.ToLower().Contains("Annual Taxable Income".ToLower()))
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.YearlyGrossIncome = amountValue;
                            }
                        }
                        else if (valueEach.ToLower().Equals("health insurance") && type != (int)CalculationColumnType.Income)
                        {
                            if (decimal.TryParse(amount, out amountValue))
                            {
                                this.HealthInsurance = amountValue;
                            }
                        }
                       
                        else if (list.ContainsKey(type + ":" + sourceId) == false)
                        {
                            list.Add(type + ":" + sourceId, amount);
                        }
                       
                        else
                        {   //if already added like due to some logic dashain can come twice but only one can have value
                            decimal newAmount, oldAmount;
                            decimal.TryParse(amount, out newAmount);
                            string oldValue = list[type + ":" + sourceId];
                            decimal.TryParse(oldValue, out oldAmount);

                            list[type + ":" + sourceId] = (oldAmount + newAmount).ToString();
                        }


                    }
                    else
                    {

                        if (decimal.TryParse(items[1], out amountValue))
                        {
                            // Calculation for other fixed also
                            if (valueEach.ToLower().Contains("1% tax"))
                            {
                                value = items[2].Replace("on", "");
                                if (decimal.TryParse(value, out amount1))
                                    this.OnePercentAmount = amount1;
                                this.OnePercentTax = amountValue;

                                this.TotalTaxInTheYear = Convert.ToDecimal( this.OnePercentTax) + Convert.ToDecimal( this.FifteenPercentTax) + Convert.ToDecimal( this.TwentyFivePercentTax )+ Convert.ToDecimal( this.FourtyPercentTax)
                                    + Convert.ToDecimal(this.MedicalTax);
                            }
                            else if (valueEach.ToLower().Contains("Annual Taxable Income".ToLower()))
                            {
                                //if (decimal.TryParse(amount, out amountValue))
                                {
                                    this.YearlyGrossIncome = amountValue;
                                }
                            }
                            else if (valueEach.ToLower().Contains("15% tax"))
                            {
                                value = items[2].Replace("on", "");
                                if (decimal.TryParse(value, out amount1))
                                    this.FifteenPercentAmount= amount1;
                                this.FifteenPercentTax = amountValue;

                                this.TotalTaxInTheYear = Convert.ToDecimal(this.OnePercentTax) + Convert.ToDecimal(this.FifteenPercentTax) + Convert.ToDecimal(this.TwentyFivePercentTax) + Convert.ToDecimal(this.FourtyPercentTax)
                                    + Convert.ToDecimal(this.MedicalTax);
                            }
                            else if (valueEach.ToLower().Contains("25% tax"))
                            {
                                value = items[2].Replace("on", "");
                                if (decimal.TryParse(value, out amount1))
                                    this.TwentyFivePercentAmount = amount1;
                                this.TwentyFivePercentTax = amountValue;

                                this.TotalTaxInTheYear = Convert.ToDecimal(this.OnePercentTax) + Convert.ToDecimal(this.FifteenPercentTax) + Convert.ToDecimal(this.TwentyFivePercentTax) + Convert.ToDecimal(this.FourtyPercentTax)
                                    + Convert.ToDecimal(this.MedicalTax);
                            }
                            else if (valueEach.ToLower().Contains("surcharge"))
                            {
                                //value = items[2].Replace("on", "");
                                //if (decimal.TryParse(value, out amount1))
                                //    this.FourtyPercentAmount = amount1;
                                this.FourtyPercentAmount = ((decimal)2.5 * amountValue) * 4;

                                this.FourtyPercentTax = amountValue;

                                this.TotalTaxInTheYear = Convert.ToDecimal(this.OnePercentTax) + Convert.ToDecimal(this.FifteenPercentTax) + Convert.ToDecimal(this.TwentyFivePercentTax) + Convert.ToDecimal(this.FourtyPercentTax)
                                    + Convert.ToDecimal(this.MedicalTax);
                            }
                            else if (valueEach.ToLower().Contains("medical tax"))
                            {
                                this.MedicalTax = amountValue;

                                this.TotalTaxInTheYear = Convert.ToDecimal(this.OnePercentTax) + Convert.ToDecimal(this.FifteenPercentTax) + Convert.ToDecimal(this.TwentyFivePercentTax) + Convert.ToDecimal(this.FourtyPercentTax)
                                    + Convert.ToDecimal(this.MedicalTax);
                       
                            }
                                // old add on
                            else if (valueEach.ToLower().Contains("add-on tax") || valueEach.ToLower().Contains("add-on paid"))
                            {
                                if (valueEach.ToLower().Contains("sst"))
                                    this.AddOnSSTPaid += amountValue;
                                else
                                    this.AddOnTDSPaid += amountValue;
                            }
                            // sst/tds paid from regular salary
                            else if (valueEach.ToLower().Contains("sst paid in past"))
                            {
                                this.SSTPaid += amountValue;
                            }
                            else if (valueEach.ToLower().Contains("tds paid in past"))
                            {
                                this.TDSPaid += amountValue;
                            }
                            else if (valueEach.ToLower().Contains("sst this month"))
                            {
                                this.SSTPaid += amountValue;
                            }
                            else if (valueEach.ToLower().Contains("tds this month"))
                            {
                                this.TDSPaid += amountValue;
                            }  
                            else if (valueEach.ToLower().Contains("a, b or c"))
                            {
                                this.DeductForTax = amountValue;
                            }
                            else if (valueEach.ToLower().Contains("insurance premium"))
                            {
                                this.InsuranceDeduction = amountValue;
                            }
                            else if (valueEach.ToLower().Contains("health insurance"))
                            {
                                this.HealthInsurance = amountValue;
                            }
                            else if (valueEach.ToLower().Contains("insurance income") && 
                                valueEach.ToLower().Contains("none cash yearly"))
                            {
                                this.InsuranceAmount = amountValue;
                            }
                            else if (valueEach.ToLower().Contains("external cit"))
                            {
                                //this.CITDeduction = Convert.ToDecimal(this.CITDeduction) + Convert.ToDecimal(amountValue);
                                this.ExternalCIT = Convert.ToDecimal(amountValue);
                            }
                            else if (valueEach.ToLower().Contains("provisional cit"))
                            {
                                this.ProvisionalCITDeduction = Convert.ToDecimal(amountValue);
                            }
                            else if (valueEach.ToLower().Contains("cit adjustment"))
                            {
                                this.CITDeduction = Convert.ToDecimal(this.CITDeduction) + Convert.ToDecimal(amountValue);
                            }
                            else if (valueEach.ToLower().Contains("add-on pf"))
                            {
                                this.PFDeduction = Convert.ToDecimal(this.PFDeduction) + Convert.ToDecimal(amountValue);
                            }
                            else if (valueEach.ToLower().Contains("add-on cit"))
                            {
                                this.CITDeduction = Convert.ToDecimal(this.CITDeduction) + Convert.ToDecimal(amountValue);
                            }
                            else if (valueEach.ToLower().Contains("add-on employer pf"))
                            {
                                if (list.ContainsKey((int)CalculationColumnType.IncomePF + ":" + (int)CalculationColumnType.IncomePF) == false)
                                {
                                    list.Add((int)CalculationColumnType.IncomePF + ":" + (int)CalculationColumnType.IncomePF, items[1]);
                                }
                                else
                                {   //if already added like due to some logic dashain can come twice but only one can have value
                                    decimal oldAmount;

                                    string oldValue = list[(int)CalculationColumnType.IncomePF + ":" + (int)CalculationColumnType.IncomePF];
                                    decimal.TryParse(oldValue, out oldAmount);

                                    list[(int)CalculationColumnType.IncomePF + ":" + (int)CalculationColumnType.IncomePF] = (oldAmount + amountValue).ToString();
                                }

                            }
                        }
                    }
                }

                // set total Tax Payable sum
                this.TaxPayable = Convert.ToDecimal(this.TotalTaxInTheYear) 
                    + Convert.ToDecimal(this.Adjustment)

                    - Convert.ToDecimal(this.OnePercentTax)

                    - Math.Abs(Convert.ToDecimal(this.FemaleRebate))
                    
                    // femalte rebate of SST add to remove
                     +
                     (
                        Convert.ToDecimal(this.FemaleRebate) !=0 ?
                        (
                            (decimal)0.1 * Convert.ToDecimal(this.OnePercentTax)
                        )
                        : 0
                     )
                     
                    //- Math.Abs(Convert.ToDecimal(this.AddOnSSTPaid))
                     - Math.Abs(Convert.ToDecimal(this.AddOnTDSPaid))

                     //- Math.Abs(Convert.ToDecimal(this.SSTPaid))
                     - Math.Abs(Convert.ToDecimal(this.TDSPaid));

                if (this.TaxPayable < 0)
                    this.ExcessTaxPaid = Math.Abs(Convert.ToDecimal(this.TaxPayable));
             
            }

            if (list.ContainsKey(header.Type + ":" + header.SourceId))

                return list[header.Type + ":" + header.SourceId];



            return "";
        }
    }
}
