﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    /// <summary>
    /// Mainly changed in "CalculationColumnType" table
    /// Salary column types, -ve so as to prevent for "IncomeId:Amount" type
    /// if changed, changes requires in sp "[CalcGetHeaderList]",function "[CalGetIncomeList]",
    /// DAL.CalcGetCalculationListResult.cs,BLL.CalculationManager.cs
    /// </summary>
    public enum AttendaceColumnType
    {
        Leave = 1,
        WeeklyHoliday = 2,
        Holiday = 3
    }


    public partial class GetUserListResult
    {
        public string Password { get; set; }
    }

}
