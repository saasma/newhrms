﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public partial class AttendanceAllEmployeeReportResult
    {
        public string InRemarksText { get; set; }
        public string OutRemarksText { get; set; }
        //public string InIPAddress { get; set; }
        //public string OutIPAddress { get; set; }
    }
    public partial class EHumanResource 
    {
        private int? _locationIdLatest = null;
        public int? LocationIdLatest
        {
            get
            {
                if (_locationIdLatest != null)
                    return _locationIdLatest;

                ELocationHistory _latest =
                BaseBiz.PayrollDataContext.ELocationHistories
                    .Where(e => e.EmployeeId == this.EmployeeId)
                    .OrderByDescending(e => e.LocationHistoryId)
                    .Take(1).SingleOrDefault();

                if (_latest != null)
                    _locationIdLatest = _latest.LocationId;




                return _locationIdLatest;
            }
            set { }
        }

    }
}
