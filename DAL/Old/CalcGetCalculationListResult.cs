﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Base;


namespace DAL
{
    public partial class CalcGetCalculationListResult
    {
        public bool IsAddOn { get; set; }

        public SalaryType SalaryViewType
        {
            set{}
            get
            {
                return (SalaryType)this.SalaryType;
            }
        }
        private List<CalculationValue> list = null;

        public void ResetTotal()
        {
            this.totalIncome = 0;
            this.totalDeduciton = 0;
        }

        private List<CalculationValue> adjustmentList = null;

        private bool isSalaryGenerationUnsuccessfull = false;

        public bool IsSalaryGenerationUnsuccessfull
        {
            get { return isSalaryGenerationUnsuccessfull; }
            set
            {
                isSalaryGenerationUnsuccessfull = value;
            }
        }

        private decimal totalIncome = 0;
        private decimal totalDeduciton = 0;
        private bool isD2 = false;
        private int WhichCompany = 0;
        private bool IsCurrencyNotNepali = false;

        private Setting companySetting = null;

        /// <summary>
        /// Check if has -ve retrospect increment value for the emp
        /// </summary>
        //public bool IsRetrospectIncrementNegative(Dictionary<string, string> headerList)
        //{

        //    //retrospect increment value if it has one
        //    decimal? value = GetCellValue((int)CalculationColumnType.IncomeResterospect,
        //         (int)CalculationColumnType.IncomeResterospect, 0, headerList);

        //    if (value != null && value < 0)
        //        return true;

        //    return false;

        //}

        /// <summary>
        /// Returns the comment if Adjustment column
        /// </summary>
        public string GetComment(int type, int sourceId)
        {
            if (list != null)
            {
                foreach (var calculationValue in list)
                {
                    if (!string.IsNullOrEmpty(calculationValue.AdjustmentComment)
                        && calculationValue.Type == type && calculationValue.SourceId == sourceId)
                    {
                        return calculationValue.AdjustmentComment;
                    }
                }

            }
            return string.Empty;
        }

        public string GetList(int type, int sourceId)
        {
            if (list != null)
            {
                foreach (var calculationValue in list)
                {
                    if (calculationValue.Type == type && calculationValue.SourceId == sourceId)
                    {
                        return calculationValue.ListOfId;
                    }
                }

            }
            return string.Empty;
        }

        /// <summary>
        /// Return the different types of Salary, Combined(1), Regular(2) or Adjustment(3) depending upon the request
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        public CalculationValue GetCalculationValue(int type, int sourceId)
        {
            CalculationValue normalValue = null;
            CalculationValue adjustmentValue = null;

            if (list == null)
                return null;

            foreach (var calculationValue in list)
            {
                if (calculationValue.Type == type && calculationValue.SourceId == sourceId)
                {
                    normalValue =  calculationValue;
                    return normalValue;
                }
            }

            //if (this.SalaryViewType == DAL.SalaryType.Combined)
            //    return normalValue;

            //foreach (var calculationValue in adjustmentList)
            //{
            //    if (calculationValue.Type == type && calculationValue.SourceId == sourceId)
            //    {
            //        adjustmentValue = calculationValue;
            //        break;
            //    }
            //}

            //if (this.SalaryViewType == DAL.SalaryType.Adjustment && adjustmentValue != null)
            //    return adjustmentValue;


            //if (normalValue != null && adjustmentValue != null)
            //{
            //    normalValue.Amount -= adjustmentValue.Amount;
            //    return normalValue;
            //}

            return null;
        }


        /// <summary>
        /// Returns the cell value, if any things changed, always paste this in  "Report_Pay_SalarySummaryResult"
        /// </summary>
        public decimal? GetCellValue(int type,int sourceId,int decimalPlaces,Dictionary<string,string> headerList,
            ref bool isHeaderMissingError,ref int missingType,ref int missingSourceId)
        {

            isHeaderMissingError = false;
            missingType = 0;
            missingSourceId = 0;

            if (companySetting == null)
            {
                companySetting = BaseBiz.PayrollDataContext.Settings.FirstOrDefault();
            }

            if (!string.IsNullOrEmpty(this.IncomeList)) 
            {
                if (list == null)
                {
                    list = new List<CalculationValue>();
                    adjustmentList = new List<CalculationValue>();

                    //bool isD2 = false;
                    CompanySetting setting = BaseBiz.PayrollDataContext.CompanySettings.Where(x => x.Key == "IsD2").SingleOrDefault();
                    if (setting != null)
                    {
                       isD2= setting == null ? false : bool.Parse(setting.Value);
                    }
                    setting = BaseBiz.PayrollDataContext.CompanySettings.Where(x => x.Key == "Company").SingleOrDefault();
                    if (setting != null)
                    {
                        WhichCompany = setting.Value == "" ? 0 : int.Parse(setting.Value);
                    }
                    PPay pay = BaseBiz.PayrollDataContext.PPays.SingleOrDefault(x => x.EmployeeId == this.EmployeeId);
                    if (pay != null && pay.IsCurrencyNotNepali != null)
                        IsCurrencyNotNepali = pay.IsCurrencyNotNepali.Value;

                    string[] values = this.IncomeList.Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries);

                    // 1. Process & add adjustment type first also
                    if (this.SalaryViewType != DAL.SalaryType.Combined && !string.IsNullOrEmpty(this.AdjustmentList))
                    {
                        string[] adjustmentValues = this.AdjustmentList.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string adjValueEach in adjustmentValues)
                        {
                            string[] adjCellvalues = adjValueEach.Split(new char[] { ':' });
                            decimal amount = 0;
                            if (decimal.TryParse(adjCellvalues[2], out amount))
                            {
                                adjustmentList.Add(new CalculationValue()
                                            {
                                                Type = int.Parse(adjCellvalues[0]),
                                                SourceId = int.Parse(adjCellvalues[1]),
                                                Amount = amount,
                                                ToolTip = adjCellvalues.Length>=4 ?  adjCellvalues[3].Replace("+",":") : ""// Note help for Tooltip
                                            });
                            }
                        }
                    }

                    // 2. Normal or Combined Salary
                    CalculationValue value = null;
                    foreach (string valueEach in values)
                    {
                        //Generally cellvalues contains like 
                        // Type:SourceId:Value:[Adj comment if Adjustment type | List of Special Event if SpecialEvent type]
                        string[] cellvalues = valueEach.Split(new char[] {':'});

                        // if - comes from db then it is error or value is not set
                        if (cellvalues[2].Equals("-"))
                        {
                            value = new CalculationValue()
                                        {
                                            Type = int.Parse(cellvalues[0]),
                                            SourceId = int.Parse(cellvalues[1]),
                                            Amount = -1 //-1 for invalid error type
                                            ,
                                            IsError = true
                                            ,
                                            ErrorCode = CalculationErrorCode.DefaultValueNotSet
                                        };
                        }
                        else
                        {
                            if (cellvalues[1] == "")
                                continue;
                            value = new CalculationValue()
                                        {
                                            Type = int.Parse(cellvalues[0]),
                                            SourceId = int.Parse(cellvalues[1]),
                                            Amount = decimal.Parse(cellvalues[2])
                                        };

                            // validate if any head has amount but the header list does not contain that header then errro msg
                            if (value != null && value.Amount > 0 && headerList != null
                                && headerList.ContainsKey(value.Type + ":" + value.SourceId) == false)
                            {
                                isHeaderMissingError = true;
                                missingType = value.Type;
                                missingSourceId = value.SourceId;
                            }

                        }

                        // if [4] has integer value then Error Code 
                        if (cellvalues.Length >= 5 && cellvalues[4] != "")
                        {
                            string errorCodeStr = cellvalues[4];
                            int codeInt;
                            if (int.TryParse(errorCodeStr, out codeInt))
                            {
                                CalculationErrorCode errorCode = (CalculationErrorCode)codeInt;
                                switch (errorCode)
                                {
                                    case CalculationErrorCode.AdjustmentExistingAmountMisMatch:
                                        value.IsError = true;
                                        value.ErrorCode = CalculationErrorCode.AdjustmentExistingAmountMisMatch;
                                        break;
                                }
                            }
                        }

                        //if ((value.ColumnType == CalculationColumnType.IncomeAdjustment ||
                        //         value.ColumnType == CalculationColumnType.DeductionAdjustment)
                        //        && cellvalues.Length > 3)
                        //    value.AdjustmentComment = cellvalues[3];

                        //For Special event & normal income, 4 value contains id of special event or increment id to
                        //be saved in CCalculationIncluded table
                        if ((value.ColumnType == CalculationColumnType.Income
                                //|| value.ColumnType ==CalculationColumnType.IncomeInsurance
                                || value.ColumnType == CalculationColumnType.DeductionInsurance) && cellvalues.Length > 3)
                        {
                            value.ListOfId = cellvalues[3];
                        }


                        if (this.SalaryViewType == DAL.SalaryType.Regular)
                        {
                            bool exists = false;
                            foreach (var calculationValue in this.adjustmentList)
                            {
                                if (calculationValue.Type == value.Type && calculationValue.SourceId == value.SourceId)
                                {
                                    //if (this.SalaryViewType == DAL.SalaryType.Adjustment)
                                    //{
                                    //    value.ToolTip = calculationValue.ToolTip;
                                    //    value.Amount -= calculationValue.Amount;
                                    //}
                                    //else
                                    value.Amount -= Convert.ToDecimal(calculationValue.Amount);
                                    exists = true;
                                    //break;
                                }
                            }
                            //if (exists == false)
                            //{
                            //    if (this.SalaryViewType == DAL.SalaryType.Adjustment)
                            //        continue;
                            //}
                        }

                        list.Add(value);

                    }

                }


                CalculationColumnType columnType = (CalculationColumnType) type;

                if (columnType == CalculationColumnType.IncomeGrossUSD)
                {
                    decimal total = 0;
                    foreach (var calculationValue in list)
                    {
                        //if income type & its header exists then sum
                        if (calculationValue.ColumnType==CalculationColumnType.IncomeUSD &&
                            ((headerList != null && headerList.ContainsKey(calculationValue.Type + ":" + calculationValue.SourceId)) || headerList == null)
                            )
                        {
                            //if (calculationValue.Amount != -1)
                            total +=
                                    decimal.Parse(BaseHelper.GetCurrency(calculationValue.Amount, decimalPlaces));
                        }
                    }
                    return total;
                }
                // If income total then generate total
                else if (columnType == CalculationColumnType.IncomeGross && totalIncome == 0)
                {
                    foreach (var calculationValue in list)
                    {
                        //if income type & its header exists then sum
                        if (CalculationValue.IsColumTypeIncome(calculationValue.ColumnType) &&
                            ((headerList != null && headerList.ContainsKey(calculationValue.Type + ":" + calculationValue.SourceId)) || headerList == null)
                            )
                        {

                            //if (isD2==false || calculationValue.SourceId != 8)
                            {

                                if (calculationValue.Amount != -1)
                                    totalIncome +=
                                        decimal.Parse(BaseHelper.GetCurrency(calculationValue.Amount, decimalPlaces));
                            }
                        }
                    }
                    return totalIncome;
                }
                // Deduction total
                else if (columnType == CalculationColumnType.DeductionTotal && totalDeduciton == 0)
                {
                    foreach (var calculationValue in list)
                    {
                        if (CalculationValue.IsColumTypeDeduction(calculationValue.ColumnType) &&
                            ((headerList != null && headerList.ContainsKey(calculationValue.Type + ":" + calculationValue.SourceId)) || headerList == null)
                            )
                        {
                            // if Total Incomes or Gross Income is zero then make SST,TDS,CIT,Deduction PF also 0
                            if (totalIncome == 0)
                            {
                                if (calculationValue.ColumnType == CalculationColumnType.SST || calculationValue.ColumnType == CalculationColumnType.TDS
                                    || calculationValue.ColumnType == CalculationColumnType.DeductionCIT || calculationValue.ColumnType == CalculationColumnType.DeductionPF)
                                {
                                    calculationValue.Amount = 0;
                                }
                            }

                            if (calculationValue.Amount != -1)// -1 is Invalid case
                            {
                                totalDeduciton += decimal.Parse(BaseHelper.GetCurrency(calculationValue.Amount, decimalPlaces));
                            }
                        }
                    }
                    return totalDeduciton;
                }
                else if (columnType == CalculationColumnType.NetSalary)
                {
                    return totalIncome - totalDeduciton;
                }
                else if (columnType == CalculationColumnType.NetSalaryUSD)
                {
                    if (IsCurrencyNotNepali)
                    {
                        CCalculation calc  = BaseBiz.PayrollDataContext.CCalculations.SingleOrDefault(x=>x.CalculationId==this.CalculationId);
                        if ( calc != null &&  calc.FixedRate != null)
                            return (totalIncome - totalDeduciton) / Convert.ToDecimal(calc.FixedRate);
                        else
                        {
                            CurrencyRate rate = BaseBiz.PayrollDataContext.CurrencyRates.FirstOrDefault();
                            return (totalIncome - totalDeduciton) / Convert.ToDecimal(rate.FixedRateDollar.Value);
                        }
                    }
                    else
                        return null;
                }
                else
                {
                    // if Total Incomes or Gross Income is zero then make SST,TDS,CIT,Deduction PF also 0
                    if (totalIncome == 0 && IsAddOn == false)
                    {
                        if (columnType == CalculationColumnType.SST
                            || columnType == CalculationColumnType.TDS
                            || columnType == CalculationColumnType.DeductionCIT
                            || columnType == CalculationColumnType.DeductionPF)
                        {
                            return 0;
                        }
                    }

                    foreach (var calculationValue in list)
                    {
                        if (calculationValue.Type == type && calculationValue.SourceId == sourceId)
                        {
                            return calculationValue.Amount;
                        }
                    }
                }
            }
            return null;
        }

      

    }
}
