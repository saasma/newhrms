﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public partial class PEmployeeDeduction
    {

        public int RemainingInstallments
        {
            get
            {
                int remInstallment = 0;
                decimal paid = 0;
                GetPaidAndRemInstallmentForDeduction(this, ref remInstallment, ref paid);
                return remInstallment;
            }
        }

        public decimal Paid
        {
            get
            {
                int remInstallment = 0;
                decimal paid = 0;
                GetPaidAndRemInstallmentForDeduction(this, ref remInstallment, ref paid);
                return paid;
            }
        }

        public static void GetPaidAndRemInstallmentForDeduction(PEmployeeDeduction deduction, ref int remInstallment, ref decimal paid)
        {
            if (deduction.IsValid.Value)
            {
                if (deduction.PDeduction.Calculation == DeductionCalculation.LOAN
                    && deduction.PDeduction.IsEMI != null && deduction.PDeduction.IsEMI.Value)
                {

                    string uniqueId = GetUniqueId(deduction);

                    var query = BaseBiz.PayrollDataContext.CCalculationLoanAdvances.Where(
                        c => c.UniqueId == uniqueId &&
                        (
                            c.InstallmentAmount != 0
                        ));

                    paid = 0;
                    int paidInstallmentMonthCount = 0;
                    foreach (var cCalculationLoanAdvance in query)
                    {
                        paid += Convert.ToDecimal(cCalculationLoanAdvance.InstallmentAmount);
                        if (cCalculationLoanAdvance.InstallmentMonths == null)
                            paidInstallmentMonthCount += 1;
                        else
                            paidInstallmentMonthCount += cCalculationLoanAdvance.InstallmentMonths.Value;
                    }

                    if (deduction.ToBePaidOver == null)
                        remInstallment = 0;
                    else
                    {
                        remInstallment = deduction.ToBePaidOver.Value - paidInstallmentMonthCount;
                    }

                }
                else if (deduction.PDeduction.Calculation == DeductionCalculation.LOAN
                    || deduction.PDeduction.Calculation == DeductionCalculation.Advance)
                {
                    string uniqueId = GetUniqueId(deduction);

                    var query = BaseBiz.PayrollDataContext.CCalculationLoanAdvances.Where(
                        c => c.UniqueId == uniqueId &&
                        (
                            c.InstallmentAmount != 0
                        ));

                    paid = 0;
                    foreach (var cCalculationLoanAdvance in query)
                    {
                        paid += cCalculationLoanAdvance.InstallmentAmount;
                    }

                    if (BaseBiz.PayrollDataContext.IsEmployeeNotRetOrResigned(deduction.EmployeeId) == true)
                    { }
                    else
                    {
                        // add paid from other deduction in retirement from SettlementDetail table
                        SettlementDetail settlement = BaseBiz.PayrollDataContext.SettlementDetails
                            .FirstOrDefault(x => x.EmployeeId == deduction.EmployeeId && x.Type == 1 && x.SourceId ==
                            deduction.DeductionId);
                        if (settlement != null)
                        {
                            paid += Convert.ToDecimal(settlement.Value);
                        }
                    }

                    if (deduction.ToBePaidOver == null)
                        remInstallment = 0;
                    else
                    {
                        remInstallment = deduction.ToBePaidOver.Value - query.Count();    
                    }
                    
                }
            }

        }

        /// <summary>
        /// Generates UniqueId combining value of Amount+TakenOn+InterestRate+ToBePaidOver+
        /// PaymentTerms like for loan & similar for advance to idenfity or group in
        /// CCalculationLoanAdvance
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static string GetUniqueId(PEmployeeDeduction e)
        {
            PEmployeeDeduction dbEntity = DAL.BaseBiz.PayrollDataContext.PEmployeeDeductions
                .FirstOrDefault(x => x.EmployeeId == e.EmployeeId && x.DeductionId == e.DeductionId);

            if (dbEntity != null)
                return dbEntity.UniqueId;

            //if (e != null && e.IsValid != null && e.IsValid.Value)
            {
                //if changed in this form change in database GenerateUniqueId function also,vice versa
                if (e.PDeduction.Calculation == DeductionCalculation.LOAN)
                {
                    return
                       e.EmployeeDeductionId + "-" + ((int) e.AdvanceLoanAmount).ToString() //skip decimal portion as it may differ from database 
                        + e.TakenOn + e.InterestRate + e.MarketRate + e.ToBePaidOver + e.PaymentTerms + e.StartingFrom;
                }
                else if(e.PDeduction.Calculation==DeductionCalculation.Advance) // for advance
                { 
                    //when variable like is changed to advance then
                    if (e.AdvanceLoanAmount == null)
                    {
                        return "";
                    }
                    //skip decimal portion as it may differ from database 
                    
                    string value = e.EmployeeDeductionId + "-" + ((int)e.AdvanceLoanAmount).ToString() + e.ToBePaidOver.ToString() + e.StartingFrom;
                    return value;
                }
            }
            return "";
        }

        public static string GenerateUniqueId(PEmployeeDeduction e)
        {
            //if (e != null && e.IsValid != null && e.IsValid.Value)
            {
                //if changed in this form change in database GenerateUniqueId function also,vice versa
                if (e.PDeduction.Calculation == DeductionCalculation.LOAN)
                {
                    return
                       e.EmployeeDeductionId + "-" + ((int)e.AdvanceLoanAmount).ToString() //skip decimal portion as it may differ from database 
                        + e.TakenOn + e.InterestRate + e.MarketRate + e.ToBePaidOver + e.PaymentTerms + e.StartingFrom;
                }
                else if (e.PDeduction.Calculation == DeductionCalculation.Advance) // for advance
                {
                    //when variable like is changed to advance then
                    if (e.AdvanceLoanAmount == null)
                    {
                        return "";
                    }
                    //skip decimal portion as it may differ from database 

                    string value = e.EmployeeDeductionId + "-" + ((int)e.AdvanceLoanAmount).ToString() + e.ToBePaidOver.ToString() + e.StartingFrom;
                    return value;
                }
            }
            return "";
        }
    }
}
