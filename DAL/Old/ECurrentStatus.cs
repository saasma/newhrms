﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public partial class ECurrentStatus
    {
        public string CurrentStatusText
        {
            get
            {
                List<KeyValue> list = new JobStatus().GetMembers();


                KeyValue value = list.SingleOrDefault(k => k.Key == this.CurrentStatus.ToString());

                if (value == null)
                {
                    //JobStatusEnum status = (JobStatusEnum)this.CurrentStatus;
                    return "";// status.ToString();
                }
                else
                    return value.Value;
                    
                    
            }
        }


        /// <summary>
        /// Holds the from the of next status to make to date of current status,used in emp personal details page
        /// </summary>
        /// 
        public string ToDateValue { get; set; }
        public DateTime? TempToDate { get; set; }
        public string TimeElapsed { get; set; }

        public string LevelPosition { get; set; }
    }

}
