﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class KeyValue
    {
        public string Key { set; get; } //text to be displayed
        public string Value { get; set; } //value to be set

        public string KeyValueCombined
        {
            get
            {
                return Key + ":" + Value;
            }
        }

        public  KeyValue()
        {
           
        }
        public KeyValue(string key,string value)
        {
            Key = key;
            Value = value;
        }
    
    }

    //public class TextValue
    //{
    //    public string Text { set; get; } //text to be displayed
    //    public string Value { get; set; } //value to be set
    //     public string Other { set; get; } 
    //    public TextValue()
    //    {

    //    }
    //    public TextValue(string text, string value)
    //    {
    //        Text = text;
    //        Value = value;
    //    }

    //}
}
